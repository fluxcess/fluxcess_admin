<?php
use PHPUnit\Framework\TestCase;

class eventTest extends TestCase
{
	// ...

	public function testUserCanAccessEvent()
	{
		// Arrange
		$a = new event("de", "de_DE");

		// Act
		$_SESSION['user']['id'] = 1;
		$b = $a->checkUserEvent(1);

		// Assert
		$this->assertEquals(true, $b);
	}
	public function testUserCannotAccessEvent()
	{
		// Arrange
		$a = new event("de", "de_DE");
	
		// Act
		$_SESSION['user']['id'] = 2;
		$b = $a->checkUserEvent(1);
	
		// Assert
		$this->assertEquals(false, $b);
	}
	// ...
}