<?php

class ext_persno extends l5sys implements iExt_procman
{

    private $_myName = 'persno';

    public function __construct($lg, $locale)
    {}

    public function getWorkflowItem($regprocstepId)
    {
        $sm = new L5Smarty();
        $sm->addTemplateDir(BASEDIR . 'ext/' . $this->_myName . '/tpl', $this->_myName);
        $sm->assign('regprocstepId', $regprocstepId);
        $xout = $sm->fetch('[' . $this->_myName . ']regprocstep_details.tpl');
        return $xout;
    }

    public function getSettingsPage($regprocstepId)
    {
        $sm = new L5Smarty();
        $sm->addTemplateDir(BASEDIR . 'ext/' . $this->_myName . '/tpl', $this->_myName);
        $sm->assign('regprocstepId', $regprocstepId);
        $xout = $sm->fetch('[' . $this->_myName . ']regprocstep_settings.tpl');
        return $xout;
    }

    /**
     * adds column to the zguestX table
     *
     * @see iExt_procman::setupStep()
     */
    public function setupStep($eventId, $regprocstepId)
    {
        // get event id
        $this->addError('', gettext('No valid event id: ' . $eventId), '');
        if (! is_numeric($eventId) || $eventId < 1) {
            $this->addError('', gettext('No valid event id: ' . $eventId), '');
        } else {
            // add column to zguestX table
            // column name format:
            // x_<regprocstep_id>_<column_name>
            try {
                $sql = 'ALTER TABLE `zguest' . $eventId . '` ADD `x_' . $regprocstepId . '_persno` INT(0) NOT NULL AFTER `accesscode`;';
                $this->_pdoObj = dbconnection::getInstance();
                $pdoStatement = $this->_pdoObj->prepare($sql, array(
                    PDO::ATTR_CURSOR => PDO::CURSOR_FWDONLY
                ));
                $pdoStatement->execute(array());
                if ($pdoStatement->errorCode() * 1 != 0) {
                    $this->addError('', 'SQL Fehler', print_r($pdoStatement->errorInfo(), true), 1);
                }
            } catch (Exception $e) {
                $this->addError('', gettext('Error creating db column for ext_persno.'), print_r($pdoStatement->errorInfo(), true), 1);
            }
        }
    }

    public function removeStep($eventId, $regprocstepId)
    {
        // remove columns from zguestX table
        // get event id
        try {
            $sql = 'ALTER TABLE `zguest' . $eventId . '` DROP `x_' . $regprocstepId . '_persno`;';
            $this->_pdoObj = dbconnection::getInstance();
            $pdoStatement = $this->_pdoObj->prepare($sql, array(
                PDO::ATTR_CURSOR => PDO::CURSOR_FWDONLY
            ));
            $pdoStatement->execute(array());
            if ($pdoStatement->errorCode() * 1 != 0) {
                $this->addError('', 'SQL Fehler', print_r($pdoStatement->errorInfo(), true), 1);
            }
        } catch (Exception $e) {
            $this->addError('', gettext('Error removing db column for ext_persno.'), print_r($pdoStatement->errorInfo(), true), 1);
        }
    }
}