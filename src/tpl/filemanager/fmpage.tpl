<div class="filemanager filemanagerobject" data-folder="" data-fdselect="{$select}" data-selecttofield="{$selecttofield}" data-selected="{$selected}">
	<div class="filemanagermenu">
		<div class="menuicon newfolder" data-on="newfolderform">
			{"Create Folder"|gettext}
		</div>
		<div class="menuicon uploadfile" data-on="newfileform">
			{"Upload File"|gettext}
		</div>
	</div>
	<div class="clear"></div>
	<div class="newfolderform hidden fineform">
		<form class="apiform" action="" method="POST" enctype="multipart/form-data" data-url="/internalfiles">
                <input type="hidden" name="form" value="newfolder" />
                <input type="hidden" name="cb" value="reloadFileManager" />
            	<input type="hidden" name="foldername" value="" />
            
                {if count($err) > 0 && $ps.form == "newfolder"}
                    <div class="fehlermeldung">
                    	{"Fehler"|gettext}:
                        <ul>
                        {foreach from=$err item=er}
                            <li>{$er.meldung}</li>
                        {/foreach}
                        </ul>
                    </div>
                {/if}
                <label>{"Folder name"|gettext}<input name="newdirname" /></label>
                <input type="submit" value="{"Create!"|gettext}" class="submit" />
            </form>
	</div>
	<div class="newfileform hidden fineform">
           		<form class="apiform fileupload" action="" method="POST" enctype="multipart/form-data" data-url="/internalfiles">
                <input type="hidden" name="form" value="angebotdateineu" />
                <input type="hidden" name="angebot_id" value="{$angebot_id}" />
                <input type="hidden" name="foldername" value="" />
                <input type="hidden" name="cb" value="reloadFileManager" />
            
                {if count($err) > 0 && $ps.form == "angebotdateineu"}
                    <div class="fehlermeldung">
                    	Fehler:
                        <ul>
                        {foreach from=$err item=er}
                            <li>{$er.meldung}</li>
                        {/foreach}
                        </ul>
                    </div>
                {/if}
				<label>{"File"|gettext}: <input type="file" name="datei" class="jfileup" /></label>
                <input type="submit" value="{"Upload!"|gettext}" class="submit" />
                {"Files with the same name are overwritten without further notice!"|gettext}
            </form>

	</div>
	<div class="foldername clear">/</div>
	<table class="filelist" cellpadding="0" cellspacing="0" border="0">
    	<thead>
        	<tr>
            	<th> </th>
            	<th>{"Filename"|gettext}</th>
            	<th>{"Size"|gettext}</th>
            	<th>{"Date"|gettext}</th>
            	<th> </th>
        	</tr>
    	</thead>
    	<tbody class="filelistbody">
    		{if $parentallowed == 1}
    			<tr>
    				<td><a href="{$linkpfad}{$parentid}">..</a></td>
    				<td><a href="{$linkpfad}{$parentid}">{" - go to parent directory - "|gettext}</a></td>
    			</tr>
    		{/if}
        	{if count($internalfiles.directories) > 0 || count($internalfiles.files) > 0}
            {foreach from=$internalfiles.directories item=u}
                <tr data-subfolder="{$u[0]}">
                    <td class="folderlink"><img src="/pic/icons/folder.svg" alt="" title=""></td>
                    <td class="folderlink">{$u[0]}</td>
                    <td> </td>
                    <td>{$u[2]|date_format:"%d.%m.%Y %H:%M"}</td>
                    <td>
                        <img class="folderdelete" src="/pic/icons/delete.svg" alt="{"Delete directory!"|gettext}" title="{"Delete directory!"|gettext}">
                    </td>
                </tr>
                {assign var="i" value=$i+1}
            {/foreach}
            {foreach from=$internalfiles.files item=d}
                <tr data-file="{$d[0]}">
                    <td><img src="/pic/icons/file.svg" alt="" title=""></td>
                    <td>
                        <a href="{$d[3]}">
                        {$d[0]}
                        </a>
                    </td>
                    <td>{$d[1]}</td>
                    <td>{$d[2]|date_format:"%d.%m.%Y, %H:%M"}</td>
                    <td>
                        <img class="filedelete" src="/pic/icons/delete.svg" alt="{"Delete file!"|gettext}" title="{"Delete file!"|gettext}">
                    </td>
                </tr>
                {assign var="i" value=$i+1}
            {/foreach}
        {else}
            <tr>
                <td colspan="5">
                    <i>{"This directory is empty."|gettext}</i>
                </td>
            </tr>
        {/if}
    </tbody>
</table>
</div>