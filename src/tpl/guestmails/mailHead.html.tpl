<html>
<head>
<style>
* { font-family: 'Arial', sans-serif; }
body { width: 500px; 
	font-size: 13px;
	}
table {
	margin-left: 10px;
	margin-top: 10px;
}
.disclaimer { 
	font-size: 0.6em; 
	padding-top: 10px;
	margin-top: 10px;
	border-top: 1px solid #ccc;
	color: #888;
}
.head {
	font-size: 1.3em;
	font-weight: bold;
	padding-bottom: 10px;
}
.head a {
	color: #000;
}
hr {
	border-style: none;
	border-top: 1px dotted #ccc;
}

</style>
</head>
<body>
<table>
<tr><td class="head">{$ev.title}</td></tr>
<tr><td>{$ev.description}</td></tr>
<tr><td>