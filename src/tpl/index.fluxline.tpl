<!DOCTYPE html>
<html lang="{$lg}">
	<head>
	<meta charset="utf-8">
	<title>{$gsLocal['SITE_PAGETITLE'][$lg]} fluxline {"Visitor Management"|gettext} - {"The Digital Guest List"|gettext}</title>
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<meta name="description" content="{$gsLocal['SITE_PAGEDESC'][$lg]} ... The application for guest management at your event - guest lists, invitations, and the powerful fast-lane check-in.">
	<meta name="author" content="{$gsLocal['SITE_PAGEAUTHOR'][$lg]} Line5 e.K.">
	
	<link rel="stylesheet" href="/css/jquery-te-1.4.0.css">
	<link href="/css/visitormanagement.css" rel="stylesheet">
	<!-- HTML5 shim, for IE6-8 support of HTML5 elements -->
	<!--[if lt IE 9]>
      <script src="../assets/js/html5shiv.js"></script>
    <![endif]-->

	<link rel="apple-touch-icon-precomposed" sizes="144x144" href="{$gsLocal['SITE_FAVICONPATH'][$lg]}_144x144.png">
	<link rel="apple-touch-icon-precomposed" sizes="114x114" href="{$gsLocal['SITE_FAVICONPATH'][$lg]}_114x114.png">
	<link rel="apple-touch-icon-precomposed" sizes="72x72" href="{$gsLocal['SITE_FAVICONPATH'][$lg]}_72x72.png">
	<link rel="apple-touch-icon-precomposed" href="{$gsLocal['SITE_FAVICONPATH'][$lg]}_57x57.png">
	<link rel="shortcut icon" href="/pic/{$gsLocal['SITE_FAVICONPATH'][$lg]}_64x64.png">
	
	<script src="/js/jquery.min.1.10.2.js"></script>
	<script src="/js/jquery-te-1.4.0.min.js"></script>
	<script src="/js/visitormanagement.js"></script>
	{if $TESTING == true}
	<style type="text/css">
		html { border: 3px solid #080; }*/
	</style>
	{/if}
</head>

<body>
	{if $blank == true}
		<div data-role="page" id="pageLogin">
			<div data-role="header">
				<div class="headerbg"></div>
				<div style="height:130px;">
					<a href="/{$lg}"><img class="sitelogo" src="{$gsLocal['SITE_MINILOGO'][$lg]}" alt="{$gsLocal['SITE_NAME'][$lg]}" /></a>
					<span class="sitetitle">{$gsLocal['SITE_SUBTITLE'][$lg]}</span> 
				</div>
			</div>
			<div> 
				{$content} 
			</div>
			{include file="portal/backend.footer.fluxline.tpl"}
		</div>
	{else}
		{include file="portal/backend.fluxline.tpl"} 
	{/if}
	<div id="popupbackground" class="popupbackground"></div>
	
	{if $STATS !== false}
		{$smarty.const.SITE_TRACKINGSCRIPT}	
	{/if}
	{if $TESTING == true}<div id="testing">TESTING</div>{/if}
	{if $LIVEENV === true}<div id="testing">LIVE {$eventid}</div>{/if}
</body>
</html>
