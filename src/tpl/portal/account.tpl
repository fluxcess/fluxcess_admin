<div class="span9">
<div class="row-fluid">
<h2>{"Account Data"|gettext}</h2>
<div class="accountpage">
{if $accountsavesuccessful == true}
	<div class="alert alert-success">{"Saved!"|gettext}</div>
{/if}
<h3>{"Import Event from file"|gettext}</h3>
	<form action="" method="post" enctype="multipart/form-data" id="importeventsettingsform" data-ajax="false" class="nojssubmit">
		<input type="hidden" name="form" value="createEventFromTemplateImport" />
		<input type="hidden" name="class" value="event" />
		{"Title of your event"|gettext} <input name="title" type="text" class="input-block-level" placeholder="{"Title of your event"|gettext}" /><br />
		<input type="file" name="settingsfile" /><br />
		<input type="submit" value="{"Upload"|gettext}" />
	</form>
	
<h3>{"Import event from another server"|gettext}</h3>
{if $smarty.const.TRANSFER_IN_ALLOWED === true}
<div class="stepform">
	<div class="step">
		<form action="" method="post" class="specialjs" data-specialjs="handleDataTakeover|1">
			<div class="error hidden">Fehler:<div></div></div>
			<label>Server</label>
			<select name="url">
				{html_options options=$glob.TRANSFER_IN_SERVERS}	
			</select>
			<label>{"Username"|gettext}</label>
			<input name="username" />
			<label>{"Password"|gettext}</label>
			<input name="password" type="password" />
			<input type="submit" value="{"next..."|gettext}" />
		</form>
	</div>
	<div class="step hidden">
		{"One moment, please. Loading event list from the remote server ..."|gettext}
	</div>
	<div class="step hidden">
		<form action="" method="post" class="specialjs" data-specialjs="handleDataTakeover|2">
			<input type="hidden" name="url" value="" />
			<input type="hidden" name="username" value="" />
			<input type="hidden" name="password" value="" />
			<label>Event</label>
			<select name="fevent_id">
				
			</select>
			<br />
			{"Attention: This overrides all local data of the selected event!"|gettext}<br />
			{"The process can take several minutes."|gettext}<br />
			<input type="checkbox" required name="override" value="1" /> {"Override all local data of the selected event!"|gettext}<br />
			<input type="submit" value="{"next..."|gettext}" />
			
		</form>
		<div id="logstatusdisplay"></div>
	</div>
</div>
<a onclick="loadTransferList();$('.transferinlist').show();">Show Incoming Transfer List</a>
<div class="transferinlist hidden">
	<table>
		<thead>
			<tr>
				<th>Event</th>
				<th>Source</th>
				<th>Date / Time</th>
				<th>Code</th>
				<th>Status</th>
				<th></th>
			</tr>
		</thead>
		<tbody>
		</tbody>
	</table>
</div>
{else}
	<p>{"This feature is not available in this software version."|gettext}</p>
{/if}

<h3>{"Export event to another server"|gettext}</h3>
{if $smarty.const.TRANSFER_OUT_ALLOWED === true}
<div class="stepform">
	<div class="step">
		<form action="" method="post" class="specialjs" data-specialjs="handleDataPushover|1">
			<div class="error hidden">Fehler:<div></div></div>
			<label>Server</label>
			<select name="url">
				{html_options options=$glob.TRANSFER_OUT_SERVERS}	
			</select>
			<label>{"Username"|gettext}</label>
			<input name="username" />
			<label>{"Password"|gettext}</label>
			<input name="password" type="password" />
			<input type="submit" value="{"next..."|gettext}" />
		</form>
	</div>
	<div class="step hidden">
		{"One moment, please. Logging in at the remote server ..."|gettext}
	</div>
	<div class="step hidden">
		<form action="" method="post" class="specialjs" data-specialjs="handleDataPushover|2">
			<input type="hidden" name="url" value="" />
			<input type="hidden" name="username" value="" />
			<input type="hidden" name="password" value="" />
			<label>Event (lokal)</label>
			<select name="fevent_id_local">
			</select>
			<label>Event (remote)</label>
			<select name="fevent_id_remote" style="display: none;">
				
			</select>
			<br />
			{"Attention: This overrides all remote data of the selected event!"|gettext}<br />
			{"The process can take several minutes."|gettext}<br />
			<input type="checkbox" required name="override" value="1" /> {"Override all remote data of the selected event!"|gettext}<br />
			<input type="submit" value="{"next..."|gettext}" />
			
		</form>
		<div id="outlogstatusdisplay"></div>
	</div>
</div>
<a onclick="loadTransferList();$('.transferoutlist').show();">Show Outgoing Transfer List</a>
<div class="transferoutlist hidden">
	<table>
		<thead>
			<tr>
				<th>Event</th>
				<th>Destination</th>
				<th>Date / Time</th>
				<th>Code</th>
				<th>Status</th>
				<th></th>
			</tr>
		</thead>
		<tbody>
		</tbody>
	</table>
</div>
{else}
	<p>{"This feature is not available in this software version."|gettext}</p>
{/if}

<h3>{"Delete Account"|gettext}</h3>
	<div id="deleteaccount_feedback"></div>
		{if $deleteenabled == true}
			<p>{"Delete your account."|gettext}
			<strong>{"This action cannot be undone!"|gettext}</strong><br />
			{"If you are sure you want to DELETE your entire account, please type 'DELETE!!!' into the input area before pressing the Delete Account button."|gettext} 
			</p>
	
			<input id="fffaccountdeletesecure" name="fffdeletesecure" autocomplete="off" />
			<button class="btn btn-small btn-danger" id="accountdeletebutton" onclick="deleteAccount();">{"Delete Account"|gettext}</button>
		{else}
			<p>
			{"You still have some events assigned to your account."|gettext} 
			{"If you want to delete your account, please delete all events first."|gettext}
			</p>
		{/if}
</div>