<div class="pwform">

	<form method="post">

		<h3>{"Your new password"|gettext}</h3>
		{if count($err) > 0}
		<div class="error">
			{"Error"|gettext}:
			<ul>
				{foreach from=$err item=er}
				<li>{$er.note}</li> {/foreach}
			</ul>
		</div>
		{else}
		<input type="hidden" name="email" value="{$email}" />
		<input type="hidden" name="code" value="{$code}" />
		<input type="hidden" name="form" value="resetpw" />
		<input name="password" type="password" placeholder="{"Password"|gettext}">
		<input type="password" name="passwordrpt" placeholder="{"Repeat password"|gettext}">
		<button class="ui-btn ui-corner-all" type="submit">{"Save"|gettext}</button>
		{/if}
	</form>

</div>
