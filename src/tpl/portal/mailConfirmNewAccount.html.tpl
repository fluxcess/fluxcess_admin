{include file="portal/mailHead.html.tpl"}

<p>
{"Hi!"|gettext}
<br />
<br />
{"Please click on the following link to confirm your email address."|gettext}<br />
<br />
<a class="conflink" href="{$link}">{$link}</a><br />
<br />
{"This link is valid for eight hours. In case you are not able to confirm within this time span, please request a new email confirmation code on our website."|gettext}
<br />
<br />
</p>

{include file="portal/mailFoot.html.tpl"}