<div class="pwform">
<h3>{"Thank you..."|gettext}</h3>
<p>
	{"... your password has been changed. You are logged in now and can proceed with your work right now."|gettext}
	<br /> <br /> 
	<a href="/">{"Go to the guestlist start page..."|gettext}</a>
</p>
</div>