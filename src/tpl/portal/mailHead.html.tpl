<html>
<head>
<style>
* { font-family: 'Arial', sans-serif; }
body { width: 500px; 
	font-size: 13px;
	}
table {
	margin-left: 10px;
	margin-top: 10px;
}
.footer { font-size: 0.8em; 
padding-top: 10px;
margin-top: 1px dotted #ccc;
}
.head {
	font-size: 1.3em;
	font-weight: bold;
	padding-bottom: 10px;
}
.head a {
	color: #000;
}
.nice {
	padding: 10px;
	border: 4px solid #444;
	background: #ccc;
}
.nice h1 {
	font-size: 1em;
	font-weight: bold;
}
</style>
</head>
<body>
<table>
<tr><td class="head"><a href="{$gsLocal['SITE_LINK'][$lg]|gettext}">{$gsLocal['SITE_NAME'][$lg]|gettext}</a></td></tr>
<tr><td>