<div class="pwform">
	<form class="form-signin" method="post">
		<h3>{"Lost your password?"|gettext}</h3>
		<input type="hidden" name="form" value="reset" />
		<input name="email" type="text" class="input-block-level" placeholder="{"Email address"|gettext}">
		<button type="submit">{"Proceed"|gettext}...</button>
	</form>
</div>
<!-- /container -->
