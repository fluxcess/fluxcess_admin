<div class="pwform">
	<h3>{"Login"|gettext}</h3>
	<form action="https://{$sitedomain}/" method="post" data-ajax="false">
		<input type="hidden" name="form" value="login" />
		<input name="email" type="email" placeholder="{"Email address"|gettext}">
		<input type="password" name="password" placeholder="{"Password"|gettext}">
		<button class="ui-btn ui-corner-all" type="submit">{"Login"|gettext}</button>
		<div class="pwformsmalltext">
			<a href="/{$lg}/reset">{"Lost your password?"|gettext}</a><br />
			<a href="/{$lg}/register">{"No account yet? Register now!"|gettext}</a>
		</div>
	</form>
</div>