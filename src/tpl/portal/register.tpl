<div class="pwform">
	<form class="form-signin" method="post">
	<h3>{"Registration"|gettext}</h3>
			<input type="hidden" name="form" value="register" />
			<input name="email" type="email" placeholder="{"Email Address"|gettext}" required>
			<input name="password" type="password" placeholder="{"Password"|gettext}" required>
			<input type="password" name="passwordrpt" placeholder="{"Password (repeat)"|gettext}" required>
			<div class="pwformsmalltext">
			<label class="checklabel">
				<input type="checkbox" value="agb" name="agb" required> <span>{"Accept Terms and Conditions"|gettext} (<a href="https://www.line5.eu/de/impressum/allgemeine-geschaeftsbedingungen/" target="_blank">{"Terms and Conditions"|gettext}</a>)</span>
			</label> 
			<br />
			<label class="checklabel">
				<input type="checkbox" value="1" name="newsletter"> <span>{"I want to subscribe to the newsletter!"|gettext}</span>
			</label> 
			<label class="checklabel">
				<input type="checkbox" value="1" name="survey"> <span>{"You may invite me to surveys for improving the software!"|gettext}</span>
			</label> 
			<label class="checklabel">
				<input type="checkbox" value="1" name="contactme"> <span>{"Please contact me personally, I have got questions!"|gettext}</span>
			</label> 
			</div>
			<br />
			<button class="ui-btn ui-corner-all" type="submit">{"Register"|gettext}</button>
	</form>
</div>