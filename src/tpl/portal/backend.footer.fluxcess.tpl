<div class="footer">
	<div class="textfooter">&copy; <a href="{$gsLocal['SITE_PAGEAUTHORLINK'][$lg]}">{$gsLocal['SITE_PAGEAUTHOR'][$lg]}</a> 2015-2018. Build #{$smarty.const.FLUXLINE_VERSION} | <a href="#crm">CRM</a> | <a href="#filemanager">{"File Manager"|gettext}</a><br />
	{if ($gsLocal['SITE_PAGEFOOTDISCLAIMER'][$lg] != '')}
		{$gsLocal['SITE_PAGEFOOTDISCLAIMER'][$lg]}"
	{/if}
	</div>
	<div class="footerlinks">
		<ul>
			{if ($gsLocal['SITE_LINKABOUT'][$lg] != '')}
				<li><a target="_blank" rel="external" href="{$gsLocal['SITE_LINKABOUT'][$lg]}">{"About"|gettext}</a></li>
			{/if}
			{if ($gsLocal['SITE_LINKDOCU'][$lg] != '')}
				<li><a target="_blank" rel="external" href="{$gsLocal['SITE_LINKDOCU'][$lg]}">{"Documentation"|gettext}</a></li>
			{/if}
			{if ($gsLocal['SITE_LINKFORUM'][$lg] != '')}
				<li><a target="_blank" rel="external" href="{$gsLocal['SITE_LINKFORUM'][$lg]}">{"Support Forum"|gettext}</a></li>
			{/if}
			{if ($gsLocal['SITE_SOCIALLINK_FACEBOOK'][$lg] != '')}
				<li><a target="_blank" rel="external" href="{$gsLocal['SITE_SOCIALLINK_FACEBOOK'][$lg]}">{"facebook"|gettext}</a></li>
			{/if}
			{if ($gsLocal['SITE_SOCIALLINK_TWITTER'][$lg] != '')}
				<li><a target="_blank" rel="external" href="{$gsLocal['SITE_SOCIALLINK_TWITTER'][$lg]}">{"Twitter"|gettext}</a></li>
			{/if}
			{if ($gsLocal['SITE_LINKCONTACT'][$lg] != '')}
				<li><a target="_blank" rel="external" href="{$gsLocal['SITE_LINKCONTACT'][$lg]}">{"Contact"|gettext}</a></li>
			{/if}
			{if ($gsLocal['SITE_LINKPRIVACY'][$lg] != '')}
				<li><a target="_blank" rel="external" href="{$gsLocal['SITE_LINKPRIVACY'][$lg]}">{"Data Privacy"|gettext}</a></li>
			{/if}
		</ul>
	</div>
</div>