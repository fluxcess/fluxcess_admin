{"Hi!"|gettext} 

{"Please click on the following link to confirm your email address."|gettext}

{$link} 

{"This link is valid for eight hours. In case you are not able to confirm within this time span, please request a new email confirmation code on our website."|gettext}

