<!DOCTYPE html>
<html lang="en">
	<head>
	<meta charset="utf-8">
	<title>fluxline {"Visitor Management"|gettext} - {"The Digital Guest List"|gettext}</title>
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<meta name="description" content="The application for guest management at your event - guest lists, invitations, and the powerful fast-lane check-in.">
	<meta name="author" content="Line5 e.K.">
	
	<link href="/css/fast-lane-checkin.css" rel="stylesheet">
	<!-- HTML5 shim, for IE6-8 support of HTML5 elements -->
	<!--[if lt IE 9]>
      <script src="../assets/js/html5shiv.js"></script>
    <![endif]-->

	<link rel="apple-touch-icon-precomposed" sizes="144x144" href="/pic/fluxline_icon_144x144.png">
	<link rel="apple-touch-icon-precomposed" sizes="114x114" href="/pic/fluxline_icon_114x114.png">
	<link rel="apple-touch-icon-precomposed" sizes="72x72" href="/pic/fluxline_icon_72x72.png">
	<link rel="apple-touch-icon-precomposed" href="/pic/fluxline_icon_57x57.png">
	<link rel="shortcut icon" href="/pic/fluxline_icon_64x64.png">
	
	{if $TESTING == true}
	<style type="text/css">
		body { background-color: #cfc; }
	</style>
	{/if}
</head>
<body>
{$content} 
</body>
</html>
