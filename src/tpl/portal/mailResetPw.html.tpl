{include file="portal/mailHead.html.tpl"}

<p>{"Hi!"|gettext}
<br />
<br />
{"Please click on the following link, to change your password."|gettext}<br />
<br />
<a href="{$link}">{$link}</a><br />
<br />
{"This link is valid for eight hours. In case you are not able to change your password within that time span, please request a new link."|gettext}
<br />
<br /></p>

{include file="portal/mailFoot.html.tpl"}