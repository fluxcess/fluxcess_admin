<div class="pwform"><h3>{"Please check your mails now..."|gettext}</h3>
<p>
	{"Right now, we are sending an email to you. It contains a link, which will allow you to reset your password."|gettext}<br />
	{"See you in a minute!"|gettext}
</p>
</div>