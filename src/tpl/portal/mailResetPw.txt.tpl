{"Hi!"|gettext}
{"Please click on the following link, to change your password."|gettext}

{$link} 

{"This link is valid for eight hours. In case you are not able to change your password within that time span, please request a new link."|gettext}
