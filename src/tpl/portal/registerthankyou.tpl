<div class="pwform">
	<form class="form-signin" method="post">
	<h3>{"Thank you..."|gettext}</h3>
	{"... for your registration. You can now proceed to creating your first event:"|gettext}<br /> <br /> 
	<a href="/" data-ajax="false">{"Start now!"|gettext}</a>
	</form>
	<div id="registrationSuccessful"></div>
</div>