<div class="container">
	<form class="form-signin">
		{if count($err) > 0}
		<h2 class="form-register-heading">{"Error"|gettext}</h2>
		<div class="error">
			{"Error"|gettext}:
			<ul>
				{foreach from=$err item=er}
				<li>{$er.note}</li> {/foreach}
			</ul>
		</div>
		{else}
		<h2 class="form-register-heading">{"Thank you..."|gettext}</h2>
		<p class="confirmation-ok">
			{"... your email address has been confirmed. Now, you can use all features of the application."|gettext} 
			<br /> <br />
			<a data-ajax="false" href="/">{"Go to the guestlist start page..."|gettext}</a>
		</p>
		{/if}
	</form>
</div>
<!-- /container -->