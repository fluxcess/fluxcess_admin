{include file="portal/backend.header.fluxline.tpl"}

<nav class="topmenu">
	<ul>
		<li><a href="{$gsLocal['SITE_SOCIALLINK_FACEBOOK'][$lg]}">{"Provide Feedback on Facebook"|gettext}</a>
		<li><a href="/">{"Guest List"|gettext}</a></li>
		<li><a href="/#filemanager">{"Filemanager"|gettext}</a></li>
		<li><a>Account</a>
			<ul>
				<li><a href="/en{$link}" class="ui-btn" data-ajax="false">english</a></li>
				<li><a href="/de{$link}" class="ui-btn" data-ajax="false">deutsch</a></li>  
				<li><a href="/account" class="ui-btn">{if strlen($username) > 0}{$username}{else}***{/if}</a></li>
				<li><a href="/logout" class="ui-btn">Logout</a></li>
			</ul>
		</li>
	</ul>
</nav>

<div class="page" id="pageHome">
	{include file="portal/pageHome.tpl"}
</div>

<div class="page hidden" id="pageEvent">
	{include file="portal/pageEvent.tpl"}	
</div>

<div class="page hidden" id="pageCRM">
	{include file="portal/pageCRM.tpl"}
</div>

<div class="page hidden" id="pageFilemanager">
	{include file="portal/pageFilemanager.tpl"}
</div>

<div class="page hidden" id="pageEmailtemplate">
	{include file="portal/pageEmailtemplate.tpl"}	
</div>

{include file="portal/backend.footer.fluxline.tpl"}