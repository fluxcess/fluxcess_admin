{if count($eventlist) == 0}
<div class="pwform noevents">
{else}
<div class="pwform hide">
{/if}
	<h3>{"Step 1: Create your event!"|gettext}</h3>
			<h4>{"Welcome to fluxline!"|gettext}</h4>
    		<p>{"Think about a title of your event, fill it into the form field, and push the 'Create'-button. No worries, you can change the title later."|gettext}</p>
			{include file="event/neweventform.tpl"}
</div>


{if count($eventlist) == 0}
<div class="ui-grid-a twothirds multipleevents hide">
{else}
<div class="twothirds multipleevents">
{/if}
    <div class="ui-block-a" style="min-height:60px">
    	{if count($eventlist) == 0}
    		<h3>{"Events"|gettext}</h3>
    		<ul class="eventlist">
    		</ul>
    	{else}
    		<h3>{"Choose one of your events:"|gettext}</h3>
    		<ul class="eventlist">
    		{foreach from=$eventlist item=ev}
    			<li><a href="#fevent/{$ev.id}">{$ev.title}</a></li>
    		{/foreach}
    		</ul>
    	{/if}
    </div>
    <div class="ui-block-b" style="min-height:60px">
		<h3>{"Add an event"|gettext}</h2>
    	{include file="event/neweventform.tpl"}
		<p>{"Create an event and add guests."|gettext}</p>
		<br />
		<br />
		<h3>{"Suggestions? Criticism?"|gettext}</h3>
		<p>{"We need your feedback in order to improve. Please post your constructive suggestions on our facebook page!"|gettext}<br />
			<br />
			<a href="{$gsLocal['SITE_SOCIALLINK_FACEBOOK'][$lg]}" target="_blank" class="fbfeedbackbutton">
				{"Provide Feedback on Facebook"|gettext}
			</a>
		</p>
		
	</div>
</div>
