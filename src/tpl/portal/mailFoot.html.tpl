	</td></tr>
	<tr><td class="footer">
		Diese E-Mail wurde Ihnen durch die Website <a href="{$gsLocal['SITE_LINK'][$lg]}|gettext">{$gsLocal['SITE_NAME'][$lg]}</a> zugestellt. 
		Falls Sie sich dort nicht angemeldet haben, handelt es sich wahrscheinlich um einen Irrtum. 
		Bitte kontaktieren Sie uns in diesem Fall per E-Mail oder ignorieren alternativ diese Nachricht.<br /> 
		<br />
		<br />
		<strong>Impressum:</strong><br /> 
		{$smarty.const.MAIL_IMPRINT_HTML}<br />
	</td></tr>
	</table>
</body>
</html>