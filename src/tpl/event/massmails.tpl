<h2>{"Mass Mails"|gettext}</h2>

<nav class="vtabs" data-tabclass="settings">
	<ul>
		<li class="active"><a>{"New"|gettext}</a></li>
		<li><a>{"Drafts"|gettext}</a></li>
		<li><a>{"Templates"|gettext}</a></li>
		<li><a>{"Auto-Mails"|gettext}</a></li>
	</ul>
</nav>

<div data-tabclass="settings" class="vtabs vtabscontent vtabssettings">
	<div class="tab">
		{include file="event/massmailform.tpl"}
	</div>
			
	<div class="tab hidden">
		{include file="event/emaillist.tpl"}
	</div>
	
	<div class="tab hidden">
		{include file="event/emailtemplates.tpl"}
	</div>
	
	<div class="tab hidden">
		{include file="event/emailautomails.tpl"}
	</div>
</div>