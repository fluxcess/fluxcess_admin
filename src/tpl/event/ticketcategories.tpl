<h3>{"Ticket categories"|gettext}</h3>

<table class="multisetting" data-multisetting="ticketcategory">
	<thead>
		<tr>
			<th data-col="seqid">{"Id"|gettext}</th>
			<th data-col="ticketcategorycode">{"Ticketcategory Code"|gettext}</th>
			<th data-col="title">{"Title"|gettext}</th>
			<th data-col="desc">{"Description"|gettext}</th>
			<th data-col="price">{"Price"|gettext}</th>
			<th data-col="invoicetext">{"Invoice text"|gettext}</th>
			<th data-col="availablefrom">{"Available from"|gettext}</th>
			<th data-col="availableuntil">{"Available until"|gettext}</th>
			<th>{"Options"|gettext}</th>
		</tr>
	</thead>
	<tbody>
		{if count($ps.ticketcategory) == 0}
		<tr>
			<td colspan="5">---</td>
		</tr>
		{else} {foreach from=$ps.ticketcategory item=tc}
		<tr>
			<td>{$tc.no}</td>
			<td>{$tc.ticketcategorycode}</td>
		</tr>
		{/foreach} {/if}
	</tbody>
</table>
<h1>{"Add new ..."|gettext}</h1>
<a href="Javascript:multisettingNew('ticketcategory');">New</a>