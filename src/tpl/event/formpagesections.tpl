<h3>{"Form Page Sections"|gettext}</h3>

<table class="multisetting" data-multisetting="formpagesection">
	<thead>
		<tr>
			<th data-col="seqid">{"Id"|gettext}</th>
			<th data-col="id">{"Id"|gettext}</th>
			<th data-col="formpageid">{"Page Id"|gettext}</th>
			<th data-col="type">{"Type"|gettext}</th>
			<th>{"Options"|gettext}</th>
		</tr>
	</thead>
	<tbody>
		{if count($ps.formpagesection) == 0}
		<tr>
			<td colspan="4">---</td>
		</tr>
		{else} {foreach from=$ps.formpagesection item=cs}
		<tr>
			<td>{$cs.id}</td>
			<td>{$cs.id}</td>
			<td>{$cs.formpageid}</td>
			<td>{$cs.type}</td>
		</tr>
		{/foreach} {/if}
	</tbody>
</table>
<h1>{"Add new ..."|gettext}</h1>
<a href="Javascript:multisettingNew('formpagesection');">New</a>