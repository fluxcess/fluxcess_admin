<h3>{"Option collection values"|gettext}</h3>

<table class="multisetting" data-multisetting="optioncollectionvalue">
	<thead>
		<tr>
			<th data-col="seqid">{"Id"|gettext}</th>
			<th data-col="optioncollectionid">{"Option Collection"|gettext}</th>
			<th data-col="title">{"Title"|gettext}</th>
			<th data-col="val">{"Value"|gettext}</th>
			<th>{"Options"|gettext}</th>
		</tr>
	</thead>
	<tbody>
		{if count($ps.optioncollectionvalue) == 0}
		<tr>
			<td colspan="5">---</td>
		</tr>
		{else} {foreach from=$ps.optioncollectionvalue item=cs}
		<tr>
			<td>{$cs.seqid}</td>
			<td>{$cs.optioncollectionid}</td>
			<td>{$cs.title}</td>
			<td>{$cs.val}</td>
		</tr>
		{/foreach} {/if}
	</tbody>
</table>
<h1>{"Add new ..."|gettext}</h1>
<a href="Javascript:multisettingNew('optioncollectionvalue');">New</a>