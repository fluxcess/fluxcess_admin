<h3>{"Conference Sessions"|gettext} {$seqid}</h3>
<form action="" name="changemultisettingsform" class="changemultisettingsform verticalform apiform" method="post" data-url="/events/{$fevent_id}/multisettings">
	<input type="hidden" name="class" value="event" />
	<input type="hidden" name="cb" value="closeMultisettingsForm;reloadMultisettingsTables" />
	<input type="hidden" name="fevent_id" value="{$fevent_id}" />
	<input type="hidden" name="multisetting" value="{$multisettingtype}" />
	<input type="hidden" name="seqid" value="{$seqid}" />
	<input type="hidden" name="form" value="changemultisettingform" />

	<label for="type">{"Type"|gettext}</label>
	<select name="type">
		<option value="presentation">{"Presentation"|gettext}</option>
		<option value="break"{if $ps.type == "break"} selected{/if}>{"Break"|gettext}</option>
	</select>

	<label for="speaker">{"Speaker"|gettext}</label>
	<input name="speaker" type="text" placeholder="{"Prof. Dr. Müller"|gettext}" value="{$ps.speaker}" />

	<label for="title">{"Title"|gettext}</label>
	<input name="title" type="text" placeholder="{"Osnabrück and the climate change"|gettext}" value="{$ps.title}" />

	<label for="descr">{"Description"|gettext}</label>
	<textarea name="descr" placeholder="{"Does the sea level endanger Lower Saxony?"|gettext}">{$ps.descr}</textarea>

	<label for="speakerpic">{"Speaker Picture"|gettext}</label>
	<textarea name="speakerpic" type="text" placeholder="{"http://www.example.com/pic.jpg"|gettext}">{$ps.speakerpic}</textarea>

	<label for="no">{"Public Session Code / Number"|gettext}</label>
	<input name="no" type="text" placeholder="15" value="{$ps.no}" />

	<label for="id">{"ID"|gettext}</label>
	<input name="id" type="text" placeholder="{"AF1-019"|gettext}" value="{$ps.id}" />
	
	<label for="datetime">{"Start Time"|gettext}</label>
	<input name="datetime" type="text" placeholder="{"14.03.2018 14:00:00"|gettext}" value="{$ps.datetime}" />
	
	<label for="datetimeto">{"End Time"|gettext}</label>
	<input name="datetimeto" type="text" placeholder="{"14.03.2018 18:00:00"|gettext}" value="{$ps.datetimeto}" />

	<label for="linked">{"Linked Sessions"|gettext}</label>
	<input name="linked" type="text" placeholder="AF1-020" value="{$ps.linked}" />
	
	<label for="stream">{"Stream(s)"|gettext}</label>
	<input name="stream" type="text" placeholder="4" value="{$ps.stream}" />
	
	<label for="maxpax">{"Maximum number of attendees (0 for unlimited)"|gettext}</label>
	<input name="maxpax" type="text" placeholder="" value="{$ps.maxpax}" />
	
	<label for="bookedpax">{"Booked number of attendees (read only)"|gettext}</label>
	<input name="bookedpax" type="text" placeholder="" value="{$ps.bookedpax}" />
	
	<input type="submit" value="{" Save and close"|gettext}" name="submit" class="ui-btn ui-corner-all" />
</form>