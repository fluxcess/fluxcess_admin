<h2>{"Settings"|gettext}</h2>	
	<nav class="vtabs" data-tabclass="settings">
		<ul>
			<li class="active"><a>{"Basics"|gettext}</a></li>
			<li><a>{"Labels"|gettext}</a></li>
			<li><a>{"Public Listing"|gettext}</a></li>
			<li><a>{"Custom Texts"|gettext}</a></li>
			<li><a>{"Mail"|gettext}</a></li>
			<li><a>{"Organizer Mail"|gettext}</a></li>
			<li><a>{"Graphics"|gettext}</a></li>
			<li><a>{"Ticket categories"|gettext}</a></li>
			<li><a>{"Conference Sessions"|gettext}</a></li>
			<li><a>{"Conference Streams"|gettext}</a></li>
			<li><a>{"Autocond Emails"|gettext}</a></li>
			<li><a>{"Apple Wallet"|gettext}</a></li>
			<li><a>{"Miscellaneous"|gettext}</a></li>
			<li><a>{"Counters"|gettext}</a></li>
			<li><a>{"Fields"|gettext}</a></li>
			<li><a>{"Form Pages"|gettext}</a></li>
			<li><a>{"Page Sections"|gettext}</a></li>
			<li><a>{"Section Fields"|gettext}</a></li>
			<li><a>{"Option Collection"|gettext}</a></li>
			<li><a>{"Collection Values"|gettext}</a></li>
			<li><a>{"Booking Form"|gettext}</a></li>
			<li><a>{"Domains"|gettext}</a></li>
			<li><a>{"Check-In Form"|gettext}</a></li>
			<li><a>{"Room Sets"|gettext}</a></li>
			<li><a>{"Rooms"|gettext}</a></li>
			<li><a>{"Quick Add Form"|gettext}</a></li>
		</ul>
	</nav>

	<div data-tabclass="settings" class="vtabs vtabscontent vtabssettings">
		
		<div class="tab">
			<h3>{"Basics"|gettext}</h3>
			<form id="formeventsettings" action="" class="settingsform">
				<div class="alert alert-success hidden">{"Saved!"|gettext}</div>
				<input type="hidden" name="class" value="event" />
				<input type="hidden" name="cb" value="refreshEventTitle" />
				<input type="hidden" name="fevent_id" value="{$fevent_id}" />
				<input type="hidden" name="form" value="change" />
			
				<label for="title">{"Title"|gettext}:</label>
				<input name="title" type="text" class="input-block-level" placeholder="{"Title"|gettext}" value="{$ev.title}" />
	
				<label for="description">{"Description"|gettext}:</label>
				<textarea name="description" type="text" class="input-block-level" placeholder="{"Description"|gettext}">{$ev.description}</textarea>
	
				<label for="eventstarttime">{"Start Date & Time"|gettext}:</label>
				<input name="eventstarttime" type="text" class="input-block-level" placeholder="{"2018-05-04 10:00:00"|gettext}" value="{$ev.eventstarttime}" />
	
				<label for="registrationactive">{"Public booking page is active"|gettext}:</label>
				<input name="registrationactive" type="checkbox" value="1" {if $ev.registrationactive == 1}checked{/if} />
				<label for="checkinactive">{"Check-In page is active"|gettext}:</label>
				<input name="checkinactive" type="checkbox" value="1" {if $ev.checkinactive == 1}checked{/if} />
				
				
				<label for="eventinfo1">{"Additional Information 1"|gettext}:</label>
				<textarea name="eventinfo1" type="text" placeholder="{"..."|gettext}">{$ev.eventinfo1}</textarea>
				<label for="eventinfo2">{"Additional Information 2"|gettext}:</label>
				<textarea name="eventinfo2" type="text" placeholder="{"..."|gettext}">{$ev.eventinfo2}</textarea>
				<label for="eventinfo3">{"Additional Information 3"|gettext}:</label>
				<textarea name="eventinfo3" type="text" placeholder="{"..."|gettext}">{$ev.eventinfo3}</textarea>
				<label for="eventinfo4">{"Additional Information 4"|gettext}:</label>
				<textarea name="eventinfo4" type="text" placeholder="{"..."|gettext}">{$ev.eventinfo4}</textarea>
				<label for="eventinfo5">{"Additional Information 5"|gettext}:</label>
				<textarea name="eventinfo5" type="text" placeholder="{"..."|gettext}">{$ev.eventinfo5}</textarea>
				
				<button class="formsubmitbutton" type="submit">{"Save"|gettext}</button>
			</form>
		</div>
		
		<div class="tab hidden">
			<h3>{"Labels"|gettext}</h3>
			<form id="formeventsettings" action="" class="settingsform">
				<div class="alert alert-success hidden">{"Saved!"|gettext}</div>
				<input type="hidden" name="class" value="event" />
				<input type="hidden" name="cb" value="refreshEventTitle" />
				<input type="hidden" name="fevent_id" value="{$fevent_id}" />
				<input type="hidden" name="form" value="change" />
				
				<h4>Booking form</h4>
				<label for="accesscodelabel">{"Access code field label"|gettext}:</label>
				<input name="accesscodelabel" type="text" class="input-block-level" placeholder="{"Access Code Label"|gettext}" value="{$ev.accesscodelabel}" />
				<label for="registerbuttonlabel">{"Selfregistration Button label"|gettext}:</label>
				<input name="registerbuttonlabel" type="text" class="input-block-level" placeholder="{"Register"|gettext}" value="{$ev.registerbuttonlabel}" />
		
				<label for="welcometext">{"Welcometext, shown on the booking signin page"|gettext}:</label>
				<textarea name="welcometext" class="input-block-level jqte">{$ev.welcometext|escape:'htmlall'}</textarea>
				<label for="footerhtml">{"Footer, shown on each page"|gettext}:</label>
				<textarea name="footerhtml" class="input-block-level">{$ev.footerhtml|escape:'htmlall'}</textarea>
		
				<label for="introductiontext">{"Introduction text, shown on the booking response selection page"|gettext}:</label>
				<textarea name="introductiontext" class="input-block-level">{$ev.introductiontext|escape:'htmlall'}</textarea>
		
				<label for="accepttext">{"Accept text, ususally 'accept invitation'"|gettext}:</label>
				<textarea name="accepttext" class="input-block-level">{$ev.accepttext|escape:'htmlall'}</textarea>
				<label for="denytext">{"Deny text, usually 'deny invitation'"|gettext}:</label>
				<textarea name="denytext" class="input-block-level">{$ev.denytext|escape:'htmlall'}</textarea>
				
				
				<h4>Quick Ticket Printouts</h4>
				<label for="ticketprinthtml">{"HTML Code for printing tickets"|gettext}:</label>
				<textarea name="ticketprinthtml" class="input-block-level">{$ev.ticketprinthtml|escape:'htmlall'}</textarea>
				<label for="ticketprinthtmlpage">{"HTML Code for printing tickets (page code, must contain [tickets])"|gettext}:</label>
				<textarea name="ticketprinthtmlpage" class="input-block-level">{$ev.ticketprinthtmlpage|escape:'htmlall'}</textarea>
				
				<label for="ticketprintperpage">{"Number of tickets per page"|gettext}:</label>
				<input name="ticketprintperpage" type="text" class="input-block-level" placeholder="10" value="{$ev.ticketprintperpage}" />
				<label for="ticketprintpdfmargin">{"Margin for ticket print"|gettext}:</label>
				<input name="ticketprintpdfmargin" type="text" class="input-block-level" placeholder="10mm" value="{$ev.ticketprintpdfmargin}" />
				
				<h4>{"PDF Invoices/Tickets"|gettext}</h4>
				<label for="invoicehtmltemplate_id">{"Template for generating a PDF invoice"|gettext}:</label>
				<div>
				<select class="emailtemplateselection"><option>Test</option></select>
				<input name="invoicehtmltemplate_id" type="hidden" class="classSelect input-block-level" placeholder="1" value="{$ev.invoicehtmltemplate_id}" />
				</div>
				<label for="invoicehtmlcode">{"HTML Code for generating a PDF invoice"|gettext}:</label>
				<textarea name="invoicehtmlcode" class="input-block-level">{$ev.invoicehtmlcode|escape:'htmlall'}</textarea>
				<label for="invoicemailtemplate">{"Email template used for sending invoices"|gettext}:</label>
				<input name="invoicemailtemplate" type="text" class="input-block-level" placeholder="{"144"|gettext}" value="{$ev.invoicemailtemplate}" />
				
				<label for="fileinvoicebackground">{"Invoice background layer (PDF)"|gettext}:</label>
				<div class="forminternalfileselect">
					<input class="selectedfile" type="hidden" name="fileinvoicebackground" value="{$ev.fileinvoicebackground}" />
					<div class="selectedfiledisplay valEvfileinvoicebackground">{if $ev.fileinvoicebackground == ""}---{else}{$ev.fileinvoicebackground}{/if}</div>
					<div class="internalfileselector" data-belongsto="fileinvoicebackground"></div>
				</div>
				
								
				<button class="formsubmitbutton" type="submit">{"Save"|gettext}</button>
				
				
			</form>
		</div>
		
		<div class="tab hidden">
			<h3>{"Public Listing"|gettext}</h3>
			<form id="formeventsettings" action="" class="settingsform">
				<div class="alert alert-success hidden">{"Saved!"|gettext}</div>
				<input type="hidden" name="class" value="event" />
				<input type="hidden" name="cb" value="refreshEventTitle" />
				<input type="hidden" name="fevent_id" value="{$fevent_id}" />
				<input type="hidden" name="form" value="change" />
			
				<label for="eventpubliclylisted">{"Event is publicly listed"|gettext}:</label>
				<input name="eventpubliclylisted" type="checkbox" value="1" {if $ev.eventpubliclylisted == 1}checked{/if} />
				
				<label for="eventregpath">{"Registration Page Path"|gettext}:</label>
				<input name="eventregpath" type="text" class="input-block-level" placeholder="{"my_event_2018"|gettext}" value="{$ev.eventregpath}" />
	
				<label for="shortvenue">{"Venue name"|gettext}:</label>
				<input name="shortvenue" type="text" class="input-block-level" placeholder="{"Penthouse"|gettext}" value="{$ev.shortvenue}" />
				
				<label for="eventpublicdate">{"Event Date (from/to, as publicly shown in list)"|gettext}:</label>
				<input name="eventpublicdate" type="text" class="input-block-level" placeholder="{"Januar 27th-28th"|gettext}" value="{$ev.eventpublicdate}" />
				
				<label for="teaserbanner">{"Teaser image (url)"|gettext}:</label>
				<input name="teaserbanner" type="text" class="input-block-level" placeholder="{"http://..."|gettext}" value="{$ev.teaserbanner}" />
				
				<label for="shorttitle">{"Public Listing Title"|gettext}</label>
				<textarea name="shorttitle" placeholder="{"My Event"|gettext}">{$ev.shorttitle}</textarea>
				
				<label for="publicshopheadline">{"Public shop headline (for example 'Buy tickets for [ev.title]')"|gettext}</label>
				<input name="publicshopheadline" placeholder="{"Buy tickets for [ev.title]"|gettext}" value="{$ev.publicshopheadline}" />
				
				<label for="infolink">{"More information link (url)"|gettext}:</label>
				<input name="infolink" type="text" class="input-block-level" placeholder="{"http://..."|gettext}" value="{$ev.infolink}" />
				
				<button class="formsubmitbutton" type="submit">{"Save"|gettext}</button>
			</form>
		</div>
	
		<div class="tab hidden">
			<h3>{"Custom Texts"|gettext}</h3>
			<form id="formeventsettings" action="" class="settingsform">
				<div class="alert alert-success hidden">{"Saved!"|gettext}</div>
				<input type="hidden" name="class" value="event" />
				<input type="hidden" name="cb" value="refreshEventTitle" />
				<input type="hidden" name="fevent_id" value="{$fevent_id}" />
				<input type="hidden" name="form" value="change" />
			
				<label for="acceptthankyoutext">{"Accept 'Thank You' text"|gettext}:</label>
				<textarea name="acceptthankyoutext" class="input-block-level">{$ev.acceptthankyoutext|escape:'htmlall'}</textarea>
				<label for="denythankyoutext">{"Deny Thank You text"|gettext}:</label>
				<textarea name="denythankyoutext" class="input-block-level">{$ev.denythankyoutext|escape:'htmlall'}</textarea>
			
				<label for="acceptmailsubject">{"Accept mail subject"|gettext}:</label>
				<textarea name="acceptmailsubject" class="input-block-level">{$ev.acceptmailsubject|escape:'htmlall'}</textarea>
				<label for="acceptmailcontent">{"Accept mail content text"|gettext}:</label>
				<textarea name="acceptmailcontent" class="input-block-level">{$ev.acceptmailcontent|escape:'htmlall'}</textarea>
				<label for="acceptmailtemplate">{"Accept mail template"|gettext}:</label>
				<input name="acceptmailtemplate" class="input-block-level" value="{$ev.acceptmailtemplate}" />
				<label for="denymailsubject">{"Deny mail subject'"|gettext}:</label>
				<textarea name="denymailsubject" class="input-block-level">{$ev.denymailsubject|escape:'htmlall'}</textarea>
				<label for="denymailcontent">{"Deny mail content text"|gettext}:</label>
				<textarea name="denymailcontent" class="input-block-level">{$ev.denymailcontent|escape:'htmlall'}</textarea>
				<button class="formsubmitbutton" type="submit">{"Save"|gettext}</button>
			</form>
		</div>
				
		<div class="tab hidden">
			<h3>{"Mail"|gettext}</h3>
			<form id="formeventsettings" action="" class="settingsform">
				<div class="alert alert-success hidden">{"Saved!"|gettext}</div>
				<input type="hidden" name="class" value="event" />
				<input type="hidden" name="cb" value="refreshEventTitle" />
				<input type="hidden" name="fevent_id" value="{$fevent_id}" />
				<input type="hidden" name="form" value="change" />
				<label for="mailsender">{"Senders mail address"|gettext}:</label>
				<input name="mailsender" type="text" class="input-block-level" placeholder="{"my@email.com"|gettext}" value="{$ev.mailsender}" />
				<label for="mailsendername">{"Mail sender's name"|gettext}:</label>
				<input name="mailsendername" type="text" class="input-block-level" placeholder="{"Max Mustermann"|gettext}" value="{$ev.mailsendername}" />
				<label for="mailbcc">{"BCC Mail addresses"|gettext}:</label>
				<input name="mailbcc" type="text" class="input-block-level" placeholder="{"my@company.com"|gettext}" value="{$ev.mailbcc}" />
				
				<label for="outgoingmailredirection">{"Redirect all outgoing emails (0/1)"|gettext}:</label>
				<input name="outgoingmailredirection" type="text" class="input-block-level" placeholder="0" value="{$ev.outgoingmailredirection}" />
				<label for="outgoingmailredirectiontargets">{"Target email addresses for redirected outgoing emails (separated by commas)"|gettext}:</label>
				<textarea name="outgoingmailredirectiontargets" type="text" class="input-block-level" placeholder="{"my@company.com,info@example.org"|gettext}">{$ev.outgoingmailredirectiontargets}</textarea>
				
				<label for="smtphost">{"SMTP Servername"|gettext}:</label>
				<input name="smtphost" type="text" class="input-block-level" placeholder="smtp.gmail.com" value="{$ev.smtphost}" />
				<label for="smtpusername">{"SMTP Username"|gettext}:</label>
				<input name="smtpusername" type="text" class="input-block-level" placeholder="" value="{$ev.smtpusername}" />
				<label for="smtppassword">{"SMTP Password"|gettext}:</label>
				<input name="smtppassword" type="password" class="input-block-level" placeholder="*****" value="{$ev.smtppassword}" />
				<label for="smtpport">{"SMTP Port"|gettext}:</label>
				<input name="smtpport" type="text" class="input-block-level" placeholder="587" value="{$ev.smtpport}" />
				<label for="smtpsecure">{"SMTP Encryption"|gettext}: {$smtpsecure}</label>
				<select name="smtpsecure" class="input-block-level">{html_options selected=$ev.smtpsecure values=$smtpsecureval output=$smtpsecureout}</select>
				
				<button class="formsubmitbutton" type="submit">{"Save"|gettext}</button>
			</form>
		</div>
		
		<div class="tab hidden">
			<h3>{"Organizer Mail"|gettext}</h3>
			<form id="formeventsettings" action="" class="settingsform">
				<div class="alert alert-success hidden">{"Saved!"|gettext}</div>
				<input type="hidden" name="class" value="event" />
				<input type="hidden" name="cb" value="refreshEventTitle" />
				<input type="hidden" name="fevent_id" value="{$fevent_id}" />
				<input type="hidden" name="form" value="change" />
				<label for="organisermailcontent">{"Organiser mail content text"|gettext}:</label>
				<textarea name="organisermailcontent" class="input-block-level">{$ev.organisermailcontent|escape:'htmlall'}</textarea>
				<label for="organisermailsubject">{"Organiser mail subject'"|gettext}:</label>
				<textarea name="organisermailsubject" class="input-block-level">{$ev.organisermailsubject|escape:'htmlall'}</textarea>
				<label for="organisermailto">{"Organiser Email address (specify to receive a notification whenever a booking form is being submitted)"|gettext}:</label>
				<input name="organisermailto" type="text" class="input-block-level" placeholder="{"my@company.com"|gettext}" value="{$ev.organisermailto}" />
				<button class="formsubmitbutton" type="submit">{"Save"|gettext}</button>
			</form>
		</div>
		
		<div class="tab hidden">
			<h3>{"Graphics"|gettext}</h3>
			<form id="formeventsettings" action="" class="settingsform">
				<div class="alert alert-success hidden">{"Saved!"|gettext}</div>
				<input type="hidden" name="class" value="event" />
				<input type="hidden" name="cb" value="refreshEventTitle" />
				<input type="hidden" name="fevent_id" value="{$fevent_id}" />
				<input type="hidden" name="form" value="change" />
				<label for="fontcolor">{"Font Color"|gettext}:</label>
				<input name="fontcolor" type="text" class="input-block-level" placeholder="{"#000"|gettext}" value="{$ev.fontcolor}" />
				<label for="browserbgcolor">{"Browser background color"|gettext}:</label>
				<input name="browserbgcolor" type="text" class="input-block-level" placeholder="{"#fff"|gettext}" value="{$ev.browserbgcolor}" />
				<label for="browserbgimage">{"Browser background image"|gettext}:</label>
				<input name="browserbgimage" type="text" class="input-block-level" placeholder="{"https://yourdomain.com/yourimage.jpg"|gettext}" value="{$ev.browserbgimage}" />
				<label for="imageheader">{"Header image for mails and booking form"|gettext}:</label>
				<input name="imageheader" type="text" class="input-block-level" placeholder="{"https://yourdomain.com/yourimage.jpg"|gettext}" value="{$ev.imageheader}" />
				<label for="formbgcolor">{"Form background color"|gettext}:</label>
				<input name="formbgcolor" type="text" class="input-block-level" placeholder="{"#fff"|gettext}" value="{$ev.formbgcolor}" />
				<label for="inputbgcolor">{"Input background color"|gettext}:</label>
				<input name="inputbgcolor" type="text" class="input-block-level" placeholder="{"#fff"|gettext}" value="{$ev.inputbgcolor}" />
				<label for="fontfamily">{"Font family"|gettext}:</label>
				<select name="fontfamily" class="input-block-level">{html_options selected=$ev.fontfamily values=$ffamval output=$ffamout}</select>
				<label for="formfontsize">{"General Font Size"|gettext}:</label>
				<input name="formfontsize" type="text" class="input-block-level" placeholder="{"1em"|gettext}" value="{$ev.formfontsize}" />
				<button class="formsubmitbutton" type="submit">{"Save"|gettext}</button>
			</form>
		</div>
		<div class="tab hidden">
			{include file="event/ticketcategories.tpl"}
		</div>
		<div class="tab hidden">
			{include file="event/conferencesessions.tpl"}
		</div>
		<div class="tab hidden">
			{include file="event/conferencestreams.tpl"}
		</div>
		<div class="tab hidden">
			{include file="event/autocondemails.tpl"}
		</div>
		<div class="tab hidden">
			<h3>{"Apple Wallet"|gettext}</h3>
			<p>{"Pictures should be uploaded in three sizes each. However, a compromise with file size	of the pass needs to be found. In doubt, you may just upload the single-size picture."|gettext}</p>			
			<form id="formeventsettings" action="" class="settingsform">
				<div class="alert alert-success hidden">{"Saved!"|gettext}</div>
				<input type="hidden" name="class" value="event" />
				<input type="hidden" name="cb" value="refreshEventTitle" />
				<input type="hidden" name="fevent_id" value="{$fevent_id}" />
				<input type="hidden" name="form" value="change" />
				
				<label for="walletorg">{"Organisation Name"|gettext}:</label>
				<input name="walletorg" type="text" class="input-block-level" placeholder="{"fluxcess GmbH"|gettext}" value="{$ev.walletorg}" />
				
				<label for="walletserialno">{"Ticket Serial Number"|gettext}:</label>
				<input name="walletserialno" type="text" class="input-block-level" placeholder="{"[guest_id]"|gettext}" value="{$ev.walletserialno}" />
				
				<label for="walletbackgroundcolor">{"Background Color"|gettext}:</label>
				<input name="walletbackgroundcolor" type="text" class="input-block-level" placeholder="{"rgb(77,77,77)"|gettext}" value="{$ev.walletbackgroundcolor}" />
				<label for="walletforegroundcolor">{"Foreground Color"|gettext}:</label>
				<input name="walletforegroundcolor" type="text" class="input-block-level" placeholder="{"rgb(255,255,255)"|gettext}" value="{$ev.walletforegroundcolor}" />
				<label for="walletlabelcolor">{"Label Color"|gettext}:</label>
				<input name="walletlabelcolor" type="text" class="input-block-level" placeholder="{"rgb(77,77,77)"|gettext}" value="{$ev.walletlabelcolor}" />
				
				<label for="walletlogotext">{"Logo Text"|gettext}:</label>
				<input name="walletlogotext" type="text" placeholder="" value="{$ev.walletlogotext}" />
				<label for="walletdescription">{"Description"|gettext}:</label>
				<input name="walletdescription" type="text" placeholder="" value="{$ev.walletdescription}" />
				<label for="walletwebsite">{"Website URL, shown on backside"|gettext}:</label>
				<input name="walletwebsite" type="text" placeholder="" value="{$ev.walletwebsite}" />
				<label for="walletbacksidetxhl">{"Backside - Headline for long text"|gettext}:</label>
				<input name="walletbacksidetxhl" type="text" placeholder="" value="{$ev.walletbacksidetxhl}" />
				<label for="walletbacksidetext">{"Backside Long Text"|gettext}:</label>
				<textarea name="walletbacksidetext" type="text" placeholder="">{$ev.walletbacksidetext}</textarea>
				<label for="walletlocations">{"Relevant location(s), up to 10"|gettext}:</label>
				<input name="walletlocations" type="text" placeholder='"latitude": 52.312112, "longitude": 9.820341' value="{$ev.walletlocations}" />
				<label for="walletrelevantdate">{"Ticket relevant date (UTC Time)"|gettext}:</label>
				<input name="walletrelevantdate" type="text" placeholder="2016-05-31T11:00Z" value="{$ev.walletrelevantdate}" />
				
				<label for="walletcode">{"Wallet Code (take care, professional feature! json format, can use variables from above! Variables must be enclosed in brackets, like &lt;guest_id&gt;)"|gettext}:</label>
				<textarea name="walletcode" placeholder="">{$ev.walletcode}</textarea>
				
				
				<h4>Wallet Logo</h4>
				<p>The logo is displayed in the upper left corner of the wallet pass.</p>
				<label for="walletfilelogo1">{"Wallet logo (1x: 160x50px)"|gettext}:</label>
				<div class="forminternalfileselect">
					<input class="selectedfile" type="hidden" name="filewalletlogo1" value="{$ev.filewalletlogo1}" />
					<div class="selectedfiledisplay valEvfilewalletlogo1">{if $ev.filewalletlogo1 == ""}---{else}{$ev.filewalletlogo1}{/if}</div>
					<div class="internalfileselector" data-belongsto="filewalletlogo1"></div>
				</div>
				<label for="walletfilelogo2">{"Wallet logo (2x: 320x100px)"|gettext}:</label>
				<div class="forminternalfileselect">
					<input class="selectedfile" type="hidden" name="filewalletlogo2" value="{$ev.filewalletlogo2}" />
					<div class="selectedfiledisplay valEvfilewalletlogo2">{if $ev.filewalletlogo2 == ""}---{else}{$ev.filewalletlogo2}{/if}</div>
					<div class="internalfileselector" data-belongsto="filewalletlogo2"></div>
				</div>
				<label for="walletfilelogo3">{"Wallet logo (3x: 480x150px)"|gettext}:</label>
				<div class="forminternalfileselect">
					<input class="selectedfile" type="hidden" name="filewalletlogo3" value="{$ev.filewalletlogo3}" />
					<div class="selectedfiledisplay valEvfilewalletlogo3">{if $ev.filewalletlogo3 == ""}---{else}{$ev.filewalletlogo3}{/if}</div>
					<div class="internalfileselector" data-belongsto="filewalletlogo3"></div>
				</div>
				
				<h4>Wallet Icon</h4>
				<p>The icon is displayed in the email and on the lock screen.</p>
				
				<label for="walletfileicon1">{"Wallet icon (1x: 29x29px)"|gettext}:</label>
				<div class="forminternalfileselect">
					<input class="selectedfile" type="hidden" name="filewalleticon1" value="{$ev.filewalleticon1}" />
					<div class="selectedfiledisplay valEvfilewalleticon1">{if $ev.filewalleticon1 == ""}---{else}{$ev.filewalleticon1}{/if}</div>
					<div class="internalfileselector" data-belongsto="filewalleticon1"></div>
				</div>
				<label for="walletfileicon2">{"Wallet icon (2x: 58x58px)"|gettext}:</label>
				<div class="forminternalfileselect">
					<input class="selectedfile" type="hidden" name="filewalleticon2" value="{$ev.filewalleticon2}" />
					<div class="selectedfiledisplay valEvfilewalleticon2">{if $ev.filewalleticon2 == ""}---{else}{$ev.filewalleticon2}{/if}</div>
					<div class="internalfileselector" data-belongsto="filewalleticon2"></div>
				</div>
				<label for="walletfileicon3">{"Wallet icon (3x: 87x87px)"|gettext}:</label>
				<div class="forminternalfileselect">
					<input class="selectedfile" type="hidden" name="filewalleticon3" value="{$ev.filewalleticon3}" />
					<div class="selectedfiledisplay valEvfilewalleticon3">{if $ev.filewalleticon3 == ""}---{else}{$ev.filewalleticon3}{/if}</div>
					<div class="internalfileselector" data-belongsto="filewalleticon3"></div>
				</div>
				
				<h4>Wallet Strip</h4>
				<p>The strip picture is displayed at the top of the wallet pass.</p>
				
				<label for="walletfilestrip1">{"Wallet strip picture (1x: 375x98px)"|gettext}:</label>
				<div class="forminternalfileselect">
					<input class="selectedfile" type="hidden" name="filewalletstrip1" value="{$ev.filewalletstrip1}" />
					<div class="selectedfiledisplay valEvfilewalletstrip1">{if $ev.filewalletstrip1 == ""}---{else}{$ev.filewalletstrip1}{/if}</div>
					<div class="internalfileselector" data-belongsto="filewalletstrip1"></div>
				</div>
				<label for="walletfilestrip2">{"Wallet strip picture (2x: 750x196px)"|gettext}:</label>
				<div class="forminternalfileselect">
					<input class="selectedfile" type="hidden" name="filewalletstrip2" value="{$ev.filewalletstrip2}" />
					<div class="selectedfiledisplay valEvfilewalletstrip2">{if $ev.filewalletstrip2 == ""}---{else}{$ev.filewalletstrip2}{/if}</div>
					<div class="internalfileselector" data-belongsto="filewalletstrip2"></div>
				</div>
				<label for="walletfilestrip3">{"Wallet strip picture (3x: 1125x294px)"|gettext}:</label>
				<div class="forminternalfileselect">
					<input class="selectedfile" type="hidden" name="filewalletstrip3" value="{$ev.filewalletstrip3}" />
					<div class="selectedfiledisplay valEvfilewalletstrip3">{if $ev.filewalletstrip3 == ""}---{else}{$ev.filewalletstrip3}{/if}</div>
					<div class="internalfileselector" data-belongsto="filewalletstrip3"></div>
				</div>
				
				<button class="formsubmitbutton" type="submit">{"Save"|gettext}</button>
			</form>
			
			<h3>Wallet-Test</h3>
			<form class="apiform" action="" method="POST" enctype="multipart/form-data" data-url="/events/#/sendwallettestmail">
				<p>{"This feature sends a demo wallet pass to the given email address. Before generating such a pass, please save your settings above."|gettext}</p>
				<label>Email address: <input type="text" name="wallettestemailaddress" value="" /></label>
				<input type="submit" value="Send Now!"/>
			</form>
		</div>
		
		<div class="tab hidden">
			<h3>{"Miscellaneous"|gettext}</h3>
			<form id="formeventsettings" action="" class="settingsform">
				<div class="alert alert-success hidden">{"Saved!"|gettext}</div>
				<input type="hidden" name="class" value="event" />
				<input type="hidden" name="cb" value="refreshEventTitle" />
				<input type="hidden" name="fevent_id" value="{$fevent_id}" />
				<input type="hidden" name="form" value="change" />
				<label for="optionunsure">{"Show 'unsure' option to guests"|gettext}:</label>
				<input data-role="flipswitch" name="optionunsure" type="checkbox" class="input-block-level" value="1" {if $ev.optionunsure == 1}checked{/if} />
				<label for="optioncompanionnames">{"Ask guest to provide names of companions"|gettext}:</label>
				<input data-role="flipswitch" name="optioncompanionnames" type="checkbox" class="input-block-level" value="1" {if $ev.optioncompanionnames == 1}checked{/if} />
				<label for="domains">{"List the domains, separated by commas"|gettext}:</label>
				<input name="domains" type="text" class="input-block-level" placeholder="{"www.example.com"|gettext}" value="{$ev.domains}" />
				<label for="guestloginformat">{"Booking Login Form Format (0 or 1)"|gettext}:</label>
				<input name="guestloginformat" type="text" class="input-block-level" placeholder="{"0"|gettext}" value="{$ev.guestloginformat}" />
				<label for="selfregistrationallowed">{"Self-registration without invitation allowed"|gettext}:</label>
				<input data-role="flipswitch" name="selfregistrationallowed" type="checkbox" class="input-block-level" value="1" {if $ev.selfregistrationallowed == 1}checked{/if} />
				<label for="selfregistrationmaxcomp">{"Maximum number of companions per self-registration"|gettext}:</label>
				<input name="selfregistrationmaxcomp" type="text" class="input-block-level" placeholder="1" value="{$ev.selfregistrationmaxcomp}" />
				<label for="selfregistrationgenerateaccesscode">{"Auto-generate an accesscode for each self-registration"|gettext}:</label>
				<input data-role="flipswitch" name="selfregistrationgenerateaccesscode" type="checkbox" class="input-block-level" value="1" {if $ev.selfregistrationgenerateaccesscode == 1}checked{/if} />
				<label for="hideanswerform">{"Hide the default response part of the form"|gettext}:</label>
				<input name="hideanswerform" type="text" class="input-block-level" placeholder="1" value="{$ev.hideanswerform}" />
				<label for="checkindomain">{"Domain for the check-in app"|gettext}:</label>
				<input name="checkindomain" type="text" class="input-block-level" placeholder="{"www.example.com"|gettext}" value="{$ev.checkindomain}" />
				<label for="checkintype">{"Type of the check-in app"|gettext}:</label>
				<input name="checkintype" type="text" class="input-block-level" placeholder="{"0 or 1"|gettext}" value="{$ev.checkintype}" />
				<label for="unconfirmedwarn">{"Show a warning if a visitor is checked in with more persons than confirmed."|gettext}:</label>
				<input data-role="flipswitch" name="unconfirmedwarn" type="checkbox" class="input-block-level" value="1" {if $ev.unconfirmedwarn == 1}checked{/if} />
				<label for="checkinhardlimit">{"Only permit the registered number of guests per participant at the check-in"|gettext}:</label>
				<input name="checkinhardlimit" type="text" class="input-block-level" placeholder="{"0 or 1"|gettext}" value="{$ev.checkinhardlimit}" />
				<label for="checkinlimitsource">{"Field for determining the check-in limit"|gettext}:</label>
				<input name="checkinlimitsource" type="text" class="input-block-level" placeholder="{"0 or 1"|gettext}" value="{$ev.checkinlimitsource}" />
				<label for="showbrowserscanlink">{"Show browser scan link in check-in app"|gettext}:</label>
				<input name="showbrowserscanlink" type="text" class="input-block-level" placeholder="{"0 or 1"|gettext}" value="{$ev.showbrowserscanlink}" />
				<label for="showstatslink">{"Show stats link in check-in app"|gettext}:</label>
				<input name="showstatslink" type="text" class="input-block-level" placeholder="{"0 or 1"|gettext}" value="{$ev.showstatslink}" />
				<label for="showpresencelink">{"Show presence link in check-in app"|gettext}:</label>
				<input name="showpresencelink" type="text" class="input-block-level" placeholder="{"0 or 1"|gettext}" value="{$ev.showpresencelink}" />
				<label for="showtooltips">{"Show tool tips link in check-in app"|gettext}:</label>
				<input name="showtooltips" type="text" class="input-block-level" placeholder="{"0 or 1"|gettext}" value="{$ev.showtooltips}" />
				<label for="importexternalukey">{"Column name for unique key for imports"|gettext}:</label>
				<input name="importexternalukey" type="text" class="input-block-level" placeholder="{"externalid"|gettext}" value="{$ev.importexternalukey}" />
				<label for="massimportpassword">{"Password for mass imports via the API (auth-field)"|gettext}:</label>
				<input name="massimportpassword" type="text" class="input-block-level" placeholder="" value="{$ev.massimportpassword}" />
				<label for="invoicenumberprefix">{"Prefix for invoice numbers"|gettext}:</label>
				<input name="invoicenumberprefix" type="text" class="input-block-level" placeholder="{"INV-"|gettext}" value="{$ev.invoicenumberprefix}" />
				<label for="startpageguestlistcolumns">{"Columns in the guest list on the start page (json)"|gettext}:</label>
				<input name="startpageguestlistcolumns" type="text" class="input-block-level" placeholder="" value="{$ev.startpageguestlistcolumns}" />
				<label for="documentfilenameprefix">{"Prefix of the document filename (invoice or eticket, for example)"|gettext}:</label>
				<input name="documentfilenameprefix" type="text" class="input-block-level" placeholder="invoice" value="{$ev.documentfilenameprefix}" />
				<label for="autoinvoice">{"Sends an invoice / eticket automatically to everyone who registered."|gettext}:</label>
				<input data-role="flipswitch" name="autoinvoice" type="checkbox" class="input-block-level" value="1" {if $ev.autoinvoice == 1}checked{/if} />
				<label for="autowallet">{"Attaches an apple wallet automatically to each invoice mail."|gettext}:</label>
				<input data-role="flipswitch" name="autowallet" type="checkbox" class="input-block-level" value="1" {if $ev.autowallet == 1}checked{/if} />
				
				<button class="formsubmitbutton" type="submit">{"Save"|gettext}</button>
			</form>
		</div>
		
		<div class="tab hidden">
			<h3>{"Counters"|gettext}</h3>
			<form id="formeventsettings" action="" class="settingsform">
				<div class="alert alert-success hidden">{"Saved!"|gettext}</div>
				<input type="hidden" name="class" value="event" />
				<input type="hidden" name="fevent_id" value="{$fevent_id}" />
				<input type="hidden" name="form" value="change" />
				<label for="nextinvoicenumber">{"Next invoice number"|gettext}:</label>
				<input name="nextinvoicenumber" type="text" class="dset" placeholder="1" value="{$ev.nextinvoicenumber}" />
				<button class="formsubmitbutton" type="submit">{"Save"|gettext}</button>
			</form>
		</div>
		
		<div class="tab hidden">{include file="event/fieldform.tpl"}</div>
		<div class="tab hidden">{include file="event/formpages.tpl"}</div>
		<div class="tab hidden">{include file="event/formpagesections.tpl"}</div>
		<div class="tab hidden">{include file="event/formsectionfields.tpl"}</div>
			
		<div class="tab hidden">{include file="event/optioncollection.tpl"}</div>
		<div class="tab hidden">{include file="event/optioncollectionvalue.tpl"}</div>

		<div class="tab hidden">{include file="event/eventguestform.tpl"}</div>
		<div class="tab hidden">{include file="event/domainform.tpl"}</div>
		<div class="tab hidden">{include file="event/checkinformfields.tpl"}</div>
		<div class="tab hidden">{include file="event/roomset.tpl"}</div>
		<div class="tab hidden">
			{include file="event/rooms.tpl"}
		</div>
		<div class="tab hidden">{include file="event/quickaddformfields.tpl"}</div>
		<div class="tab hidden">
			<form action="" method="post">
				<input type="hidden" name="form" value="uploadImageHeader" />
				<input type="hidden" name="class" value="event" />
				<input type="hidden" name="fevent_id" value="{$fevent_id}" />
				<label for="imageheader">{"Header image (700 x 100 px, png/gif/jpg, max. 500kb)"|gettext}:</label>
				<input id="imageheader" type="file" name="files[]" multiple>
				<div id="progress" class="progress progress-success progress-striped">
        			<div class="bar"></div>
    			</div>
			</form>
			<div id="files" class="files"></div>
		</div>
		
	</div> 

<script>
/*jslint unparam: true */
/*global window, $ */
$(function () {
    'use strict';
    // Change this to the location of your server-side upload handler:
    var url = window.location.hostname === 'admin.xn--gsteliste-v2a.org' ?
                '//admin.xn--gsteliste-v2a.org/' : '/';
    /*$('#imageheader').fileupload({
        url: url,
        dataType: 'json',
        done: function (e, data) {
            $.each(data.result.files, function (index, file) {
                $('<p/>').text(file.name).appendTo('#files');
                $('<p/>').text(file.error).appendTo('#files');
                updateFileName(file.name);
            });
            
        },
        progressall: function (e, data) {
            var progress = parseInt(data.loaded / data.total * 100, 10);
            $('#progress .bar').css(
                'width',
                progress + '%'
            );
        }
    }).prop('disabled', !$.support.fileInput)
        .parent().addClass($.support.fileInput ? undefined : 'disabled');
    */
	//$('textarea').jqte();
	
	// settings of status
	var jqteStatus = true;
	/*
	$(".status").click(function(){
		jqteStatus = jqteStatus ? false : true;
		$('.l5jqte').jqte({
			"status" : jqteStatus
			})
	});*/
});
</script>