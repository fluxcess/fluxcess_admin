<h3>{"Room Set"|gettext} {$seqid}</h3>
<form action="" name="changemultisettingsform" class="changemultisettingsform verticalform apiform" method="post" data-url="/events/{$fevent_id}/multisettings">
	<input type="hidden" name="class" value="event" />
	<input type="hidden" name="cb" value="closeMultisettingsForm;reloadMultisettingsTables" />
	<input type="hidden" name="fevent_id" value="{$fevent_id}" />
	<input type="hidden" name="multisetting" value="{$multisettingtype}" />
	<input type="hidden" name="seqid" value="{$seqid}" />
	<input type="hidden" name="form" value="changemultisettingform" />

	<label for="id">{"Id"|gettext}</label>
	<input name="id" type="text" value="{$ps.id}" />
	
	<label for="title">{"Title"|gettext}</label>
	<input name="title" type="text" value="{$ps.title}"/>
		
	<input type="submit" value="{"Save and close"|gettext}" name="submit" class="ui-btn ui-corner-all" />
</form>

<h3>{"Rooms"|gettext}</h3>

<table data-multisetting="roomset" data-n1entity="n1roomtoset" data-parentid="{$ps.id}" data-parentseq="{$seqid}">
	<thead>
		<tr>
			<th>{"Room"|gettext}</th>
			<th>{"Options"|gettext}</th>
		</tr>
	</thead>
	{foreach from=$n1roomtoset item=r}
		<tr data-multiid="{$r.skey_multiid}">
			<td>
				{$dropdown_roomset_n1roomtoset[$r.sval_vch]}
			</td>
			<td>
				<img src="/pic/icons/delete.svg" alt="{"Remove room from room set"|gettext}" class="multisettingSubDelete" />
			</td>
		</tr>
	{/foreach}
</table>

<form action="" name="changemultisettingsform" class="changemultisettingsform verticalform apiform" method="post" data-url="/events/{$fevent_id}/multisettings">
	<input type="hidden" name="class" value="event" />
	<input type="hidden" name="cb" value="" />
	<input type="hidden" name="fevent_id" value="{$fevent_id}" />
	<input type="hidden" name="parentmultiname" value="roomset" />
	<input type="hidden" name="parentmultiid" value="{$ps.id}" />
	<input type="hidden" name="multiname" value="n1roomtoset" />
	
	<input type="hidden" name="form" value="addmultisetting1n" />
	<!-- <input type="hidden" name="form" value="changemultisettingform" /> -->

	<label for="val">{"Room"|gettext}</label>
	<select name="val">{html_options options=$dropdown_roomset_n1roomtoset selected=$ps.val}</select>
		
	<input type="submit" value="{"Add"|gettext}" name="submit" class="ui-btn ui-corner-all" />
</form>
