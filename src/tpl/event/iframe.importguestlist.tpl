<form action="/index.php?type=iframe&module=event&fevent_id=1&submodule=importfile" method="POST" enctype="multipart/form-data" id="importform" data-ajax="false" class="nojssubmit">
	{if $importsuccessful == true}
	<div class="alert alert-success">{"Imported!"|gettext}</div>
	<script type="text/javascript">parent.oTable.fnReloadAjax();</script>
	{/if}
	<input type="hidden" name="class" value="event" />
	<input type="hidden" name="fevent_id" value="{$fevent_id}" />
	<input type="hidden" name="form" value="importguestlist" />
	<input type="hidden" name="noajax" value="1" />
	<input type="checkbox" name="clearbefore" value="1" /> Gästeliste vor Import löschen<br />
	<input type="file" name="importfile" id="importfile" />
	
	<input type="hidden" name="fileinput" /><br />
	<button class="btn btn-small btn-primary" type="submit" id="uploadimportfilebutton">{"Import"|gettext}</button>
</form>