<h3>{"Option Collection Value"|gettext} {$seqid}</h3>
<form action="" name="changemultisettingsform" class="changemultisettingsform verticalform apiform" method="post" data-url="/events/{$fevent_id}/multisettings">
	<input type="hidden" name="class" value="event" />
	<input type="hidden" name="cb" value="closeMultisettingsForm;reloadMultisettingsTables" />
	<input type="hidden" name="fevent_id" value="{$fevent_id}" />
	<input type="hidden" name="multisetting" value="{$multisettingtype}" />
	<input type="hidden" name="seqid" value="{$seqid}" />
	<input type="hidden" name="form" value="changemultisettingform" />

	<label for="optioncollectionid">{"Option Collection"|gettext}</label>
	<select name="optioncollectionid">{html_options options=$dropdown_optioncollectionvalue_optioncollectionid selected=$ps.optioncollectionid}</select>
	
	<label for="title">{"Displayed Title"|gettext}</label>
	<input name="title" type="text" value="{$ps.title}" />
	
	<label for="val">{"Underlying Value"|gettext}</label>
	<input name="val" type="text" value="{$ps.val}" />
	
	<hr />
		
	<label for="preselected">{"Preselected"|gettext}</label>
	<input name="preselected" type="text" value="{$ps.preselected}" />
	
	<label for="hidesectionlist">{"Hide Section List"|gettext}</label>
	<input name="hidesectionlist" type="text" value="{$ps.hidesectionlist}" />
	
	<label for="showsectionlist">{"Show Section List"|gettext}</label>
	<input name="showsectionlist" type="text" value="{$ps.showsectionlist}" />
	
	<input type="submit" value="{" Save and close"|gettext}" name="submit" class="ui-btn ui-corner-all" />
</form>