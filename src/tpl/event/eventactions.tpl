<h2>{"Event-Actions"|gettext}</h2>

<h3>{"Access Code Generator"|gettext}</h3>
<div>
	
	<div id="generateaccesscodes_feedback"></div>
	<p>{"Generate a unique 6-character access code for each guest on the guestlist."|gettext}
	{"The guest will be able to use this code to login and confirm or cancel his participation."|gettext}
	{"Existing access codes will not be overwritten."|gettext}</p>
	
	<button class="btn btn-small btn-primary" onclick="generateAccessCodes();">{"Generate Access Codes"|gettext}</button>
	
</div>

<h3>{"Check-Out all guests"|gettext}</h3>
<div>
	
	<div id="checkoutallguests_feedback"></div>
	<p>{"Checks out all guests immediately."|gettext}</p>
	<button class="btn btn-small btn-primary" onclick="checkOutAllGuests();">{"Global Check-Out"|gettext}</button>
	
</div>

<h3>{"Print tickets"|gettext}</h3>
<div>
	
	<div id="printalltickets_feedback"></div>
	<p>{"Print all tickets immediately."|gettext}</p>
	<form action="" method="post" target="_blank" data-ajax="false" class="nojssubmit" onsubmit="printAllTickets();return false;">
		<input name="content" type="hidden" value="" />
		<button class="btn btn-small btn-primary" type="submit" >{"Generate PDF"|gettext}</button>
	</form>
	<div id="printselectedtickets_feedback"></div>
	<p>{"Print selected tickets immediately."|gettext}</p>
	<form action="" method="post" target="_blank" data-ajax="false" class="nojssubmit" onsubmit="printSelectedTickets();return false;">
		<input name="content" type="hidden" value="" />
		<button class="btn btn-small btn-primary" type="submit" >{"Generate PDF"|gettext}</button>
	</form>
	
</div>

<h3>{"Clear all guest data"|gettext}</h3>
<div class="deleteguestdataform">
	
	<div id="deleteguestdata_feedback"></div>
	<p>{"Delete all guests and all data (invoices, tickets, ...)"|gettext}
	<strong>{"This action cannot be undone!"|gettext}</strong><br />
	{"If you are sure you want to DELETE the entire guest list, please type 'CLEAR!!!' into the input area before pressing the Clear Guest List button."|gettext} 
	</p>
	
	<input id="fffdeleteguestlistsecure" name="fffdeleteguestlistsecure" autocomplete="off" />
	<button class="btn btn-small btn-danger" onclick="deleteGuestlist();">{"Clear Guest List"|gettext}</button>
</div>

<h3>{"Delete Event"|gettext}</h3>
<div class="deleteeventform">
	
	<div id="deleteevent_feedback"></div>
	<p>{"Delete this event, guestlist and all feedback."|gettext}
	<strong>{"This action cannot be undone!"|gettext}</strong><br />
	{"If you are sure you want to DELETE the entire event, please type 'DELETE!!!' into the input area before pressing the Delete Event button."|gettext} 
	</p>
	
	<input id="fffdeletesecure" name="fffdeletesecure" autocomplete="off" />
	<button class="btn btn-small btn-danger" onclick="deleteEvent();">{"Delete Event"|gettext}</button>
	
</div>