<h2>{"Guest list"|gettext}</h2>

<div class="right">
	<select data-inline="true" name="guesttableshowcount" id="guesttableshowcount" onchange="changeguesttableshowcount();">
		<option value="10">10</option>
		<option value="-1">all</option>
	</select>
	<a class="ui-btn ui-shadow ui-corner-all ui-btn-icon-notext ui-icon-plus ui-btn-inline" onclick="editGuest(0);" title="{"Add guest"|gettext}" name="butAddGuest"><img src="/pic/icons/add.svg" title="{"Add guest"|gettext}" alt="{"Add guest"|gettext}" /></a>
	<a class="ui-btn ui-shadow ui-corner-all ui-btn-icon-notext ui-icon-refresh ui-btn-inline" onclick="reloadGuestTable();" title="{"Reload table"|gettext}"><img src="/pic/icons/refresh.svg" title="{"Reload table"|gettext}" alt="{"Reload table"|gettext}" /></a>
	<a class="ui-btn ui-shadow ui-corner-all ui-btn-icon-notext ui-icon-arrow-l ui-btn-inline" onclick="showGuesttablePreviousPage();" title="{"Previous page"|gettext}"><img src="/pic/icons/previouspage.svg" title="{"Previous page"|gettext}" alt="{"Previous page"|gettext}" /></a>
	<a class="ui-btn ui-shadow ui-corner-all ui-btn-icon-notext ui-icon-arrow-r ui-btn-inline" onclick="showGuesttableNextPage();" title="{"Next page"|gettext}"><img src="/pic/icons/nextpage.svg" title="{"Next page"|gettext}" alt="{"Next page"|gettext}" /></a>
	<div style="width: 100px; display: inline; float: left;"><input type="search" id="gsearch" name="se" data-inline="true" /></div>
</div>

<table id="guesttable" cellpadding="0" cellspacing="0" border="0" class="table table-striped table-bordered datatable">
	<thead>
		<tr>
			<th data-cname="optionCheckbox"></th>
			<th data-cname="guest_id">{"Id"|gettext}</th>
			<th data-cname="salutation">{"Salutation"|gettext}</th>
			<th data-cname="firstname">{"First Name"|gettext}</th>
			<th data-cname="lastname">{"Last Name"|gettext}</th>
			<th data-cname="company">{"Company"|gettext}</th>
			<th data-cname="note">{"Comment"|gettext}</th>
			<th>{"Options"|gettext}</th>
		</tr>
	</thead>
	<tbody>
		<tr>
			<td></td>
			<td></td>
			<td></td>
			<td>{"Please wait..."|gettext}</td>
			<td></td>
			<td></td>
			<td></td>
		</tr>
	</tbody>
</table>
<div class="right">
<a class="ui-btn ui-shadow ui-corner-all ui-btn-icon-notext ui-icon-plus ui-btn-inline" onclick="editGuest(0);" title="{"Add guest"|gettext}" name="butAddGuest"><img src="/pic/icons/add.svg" title="{"Add guest"|gettext}" alt="{"Add guest"|gettext}" /></a>
	<a class="" onclick="reloadGuestTable();" title="{"Reload table"|gettext}"><img src="/pic/icons/refresh.svg" title="{"Reload table"|gettext}" alt="{"Reload table"|gettext}" /></a>
	<a class="ui-btn ui-shadow ui-corner-all ui-btn-icon-notext ui-icon-arrow-l ui-btn-inline" onclick="showGuesttablePreviousPage();" title="{"Previous page"|gettext}"><img src="/pic/icons/previouspage.svg" title="{"Previous page"|gettext}" alt="{"Previous page"|gettext}" /></a>
	<a class="ui-btn ui-shadow ui-corner-all ui-btn-icon-notext ui-icon-arrow-r ui-btn-inline" onclick="showGuesttableNextPage();" title="{"Next page"|gettext}"><img src="/pic/icons/nextpage.svg" title="{"Next page"|gettext}" alt="{"Next page"|gettext}" /></a>
	</div>