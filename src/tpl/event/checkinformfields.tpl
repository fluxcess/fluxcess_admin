<h3>{"Check-In Form Fields"|gettext}</h3>

<table class="multisetting" data-multisetting="checkinformfield">
	<thead>
		<tr>
			<th data-col="seqid">{"Id"|gettext}</th>
			<th data-col="id">{"Id"|gettext}</th>
			<th data-col="type">{"Type"|gettext}</th>
			<th>{"Options"|gettext}</th>
		</tr>
	</thead>
	<tbody>
		{if count($ps.checkinformfield) == 0}
		<tr>
			<td colspan="4">---</td>
		</tr>
		{else} {foreach from=$ps.checkinformfield item=cs}
		<tr>
			<td>{$cs.id}</td>
			<td>{$cs.title}</td>
			<td>{$cs.type}</td>
			<td>
				<img src="/pic/icon/arrowdown.svg" />
				<img src="/pic/icon/arrowup.svg" />
			</td>
		</tr>
		{/foreach} {/if}
	</tbody>
</table>
<h1>{"Add new ..."|gettext}</h1>
<a href="Javascript:multisettingNew('checkinformfield');">New</a>