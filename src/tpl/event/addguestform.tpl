<form action="">
	<h2>{"Add guest"|gettext}</h2>
	{if $guestcreatesuccessful == true}
	<div class="alert alert-success">{"Saved!"|gettext}</div>
	{/if}
	<input type="hidden" name="class" value="guest" />
	<input type="hidden" name="cb" value="oTable.fnReloadAjax" />
	<input type="hidden" name="fevent_id" value="{$fevent_id}" />
	<input type="hidden" name="form" value="create" />
	<input name="salutation" type="text" class="input-block-level" placeholder="{"Salutation"|gettext}" />
	<input name="firstname" type="text" class="input-block-level" placeholder="{"First Name"|gettext}" />
	<input name="lastname" type="text" class="input-block-level" placeholder="{"Last Name"|gettext}" />
	<input name="company" type="text" class="input-block-level" placeholder="{"Company"|gettext}" />
	<input name="emailaddress" type="text" class="input-block-level" placeholder="{"Email address"|gettext}" />
	<input name="note" type="text" class="input-block-level" placeholder="{"Comment"|gettext}" />

	{if strlen($customFields) > 0}
		<hr /> {"Custom Fields"|gettext} <hr />
		{$customFields}
	{/if}
	
	<button class="btn btn-small btn-primary" type="submit">{"Create"|gettext}</button>
</form>
