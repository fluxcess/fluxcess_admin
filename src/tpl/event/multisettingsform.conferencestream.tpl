<h3>{"Conference Stream"|gettext} {$seqid}</h3>
<form action="" name="changemultisettingsform" class="changemultisettingsform verticalform apiform" method="post" data-url="/events/{$fevent_id}/multisettings">
	<input type="hidden" name="class" value="event" />
	<input type="hidden" name="cb" value="closeMultisettingsForm;reloadMultisettingsTables" />
	<input type="hidden" name="fevent_id" value="{$fevent_id}" />
	<input type="hidden" name="multisetting" value="{$multisettingtype}" />
	<input type="hidden" name="seqid" value="{$seqid}" />
	<input type="hidden" name="form" value="changemultisettingform" />

	<label for="title">{"Title"|gettext}</label>
	<input name="title" type="text" placeholder="{"Research"|gettext}" value="{$ps.title}" />

	<label for="id">{"ID"|gettext}</label>
	<input name="id" type="text" placeholder="{"F1"|gettext}" value="{$ps.id}" />
		
	<input type="submit" value="{" Save and close"|gettext}" name="submit" class="ui-btn ui-corner-all" />
</form>