<h3>{if $guest_id == 0}{"Add guest"|gettext}{else}{"Guest"|gettext} #{$guest_id}{/if}</h3>
<div class="htabs" data-guest_id="{$guest_id}">
	<ul class="htabs_menu">
		{if ($guest_id == 0 || $guest_id == '') && count($ev.quickaddformfield) > 0}
		<li data-htab="quickaddform">{"Quick Add Form"|gettext}</li>
		{/if}
		<li data-htab="guestform">{"Guest Data"|gettext}</li>
		{if $guest_id != 0}
		<li data-htab="invoicing">{"Invoicing / Ticketing"|gettext}</li>
		<li data-htab="checkin">{"Check-In"|gettext}</li>
		<li data-htab="actionlog">{"Action Log"|gettext}</li>
		{/if}
	</ul>
	{if ($guest_id == 0 || $guest_id == '') && count($ev.quickaddformfield) > 0}
	<div class="htab htab_quickaddform" data-htab="quickaddform">
		{include file="event/changeguestform_quickadd.tpl"}
	</div>
	{/if}
	<div class="htab htab_guestform" data-htab="guestform">
		{include file="event/changeguestform_maindata.tpl"}
	</div>
	<div class="htab htab_checkin" data-htab="checkin">
		<h1>Check-In</h1>
		<div class="checkinouterror hidden error">{"Errors occured:"|gettext}<div></div></div>
		<table>
			<thead>
				<th>{"Room"|gettext}</th>
				<th>{"Room Name"|gettext}</th>
				<th>{"Checked In"|gettext}</th>
				<th>{"Options"|gettext}</th>
			</thead>
			<tbody>
				{if (count($ps.checkin) > 0)}
					{foreach from=$ps.checkin item=ci}
						<tr data-room_id="{$ci.room_id}">
							<td>{$ci.room_id}</td>
							<td>{$ci.room_name}</td>
							<td>{$ci.pax}</td>
							<td>
								<input type="button" value="{"check in"|gettext}" class="buttonCheckInOut buttonCheckin" />
								<input type="button" value="{"check out"|gettext}" class="buttonCheckInOut buttonCheckout" />
							</td>
						</tr>
					{/foreach}
				{else}
					<tr>
						<td colspan="4">{"No data available."|gettext}</td>
					</tr>
				{/if}
			</tbody>
		</table>
	</div>
	<div class="htab htab_actionlog" data-htab="actionlog">
		<h1>{"Guest Action Log"|gettext}</h1>
		<table class="guestactionloglist" data-guest_id="{$guest_id}">
			<thead>
				<tr>
					<th>{"Date"|gettext}</th>
					<th>{"Action"|gettext}</th>
					<th>{"Info"|gettext}</th>
					<th>{"Details"|gettext}</th>
				</tr>
			</thead>
			<tbody>
				{if (count($ps.actionlog) == 0)}
				<tr><td colspan="3"><em>{"No actions available yet."|gettext}</em></td></tr>
				{else}
					{foreach from=$ps.actionlog item=al}
					<tr>
						<td>{$al.ts|date_format:"d.m.Y H:i:s"}</td>
						<td>{$ps.actiontype[$al.actiontype]} ({$al.actiontype})</td>
						<td><a href="/api/downloads/{$smarty.session.customer.customer_id}/event/{$fevent_id}/invoices/{$inv.suppdata}">{$inv.suppdata}</a></td>
						<td>{if ($al.actiontype == 'inv' && $al.paymentstatus == null)}
							<img src="/pic/icons/exclamation_red.svg" alt="{"unpaid"|gettext}" />
							{elseif ($al.actiontype == 'inv' && $al.paymentstatus == 16)}
							<img src="/pic/icons/mark_green.svg" alt="{"paid"|gettext}" />
							{else}
								{if $al.actiontype == 'inv'}
									({$al.paymentcomment})
								{elseif $al.actiontype == 'pay'}
									{$ps.paymentstatus[$al.details1]}
								{elseif $al.actiontype == 'msg'}
									{"Message..."|gettext}
								{elseif $al.actiontype == 'prt'}
									{"Ticket print..."|gettext}
								{else}
									{$al.suppdata} 
								{/if}
							{/if}</td>
					</tr>
					{/foreach}
				{/if}
			</tbody>
		</table>
	</div>

	<div class="htab htab_invoicing" data-htab="invoicing">
<h1>{"Invoices"|gettext}</h1>
<table class="guestinvoiceslist smalltable" data-guest_id="{$guest_id}">
	<thead>
	<tr>
		<th>{"Date"|gettext}</th>
		<th>{"File"|gettext}</th>
		<th>{"Net"|gettext}</th>
		<th>{"Gross"|gettext}</th>
		<th>{"Payment status"|gettext}</th>
		<th>{"Options"|gettext}</th>
	</tr>
	</thead>
	<tbody>
	{if (count($ps.invoices) == 0)}
		<tr><td colspan="3"><em>{"No invoices available yet."|gettext}</em></td></tr>
	{else}
	{foreach from=$ps.invoices item=inv}
		<tr data-invoicefile="{$inv.suppdata}" data-actionid="{$inv.id}" {if $inv.flg01 == 1}
		class="invoicecancelled"
		{/if}
		>
			<td>{$inv.ts}</td>
			<td><a href="/api/downloads/{$smarty.session.customer.customer_id}/event/{$fevent_id}/invoices/{$inv.suppdata}">{$inv.suppdata}</a></td>
			<td class="price">{$inv.dec6|currencyd}</td>
			<td class="price">{$inv.dec8|currencyd}</td>
			<td>{if ($inv.paymentstatus == null)}
					<img src="/pic/icons/exclamation_red.svg" alt="{"unpaid"|gettext}" />
				{elseif ($inv.paymentstatus == 16)}
					<img src="/pic/icons/mark_green.svg" alt="{"paid"|gettext}" />
				{else}
					{$inv.paymentstatus} ({$inv.paymentcomment})
				{/if}</td>
			<td>
				<img src="/pic/icons/email.svg" alt="Re-Send" class="buttonResendInvoice" alt="{"Resend invoice via email"|gettext}" title="{"Resend invoice via email"|gettext}"/>
				<img src="/pic/icons/remoteprint.svg" alt="print remotely" class="buttonRemoteprintInvoice" alt="{"Resend invoice via email"|gettext}" title="{"Reprint invoice remotely"|gettext}"/>
				{if $inv.flg01 == 0}
				<img src="/pic/icons/delete.svg" alt="cancel" class="buttonCancelInvoice" alt="{"Cancel invoice"|gettext}" />
				{else}
				<img src="/pic/icons/delete.svg" alt="uncancel" class="buttonUncancelInvoice" alt="{"Uncancel invoice"|gettext}" />
				{/if}
			</td>
		</tr>
	{/foreach}
	{/if}
	</tbody>
</table>


{if (count($ps.invoices) == 0)}
<div class="buttonarea firstInvoiceCreation">

	{if $guestchangesuccessful == true}
	<div class="alert alert-success">{"The invoice has been successfully generated and stored."|gettext}</div>
	{/if}
	<div class="invoiceerror hidden error">{"Errors occured:"|gettext}<div></div></div>
	<img src="/pic/icons/view.svg" title="{"Show preview!"|gettext}" class="buttonInvoicePreview iconaction" />
	<img src="/pic/icons/x_invoice_save.svg" title="{"Generate invoice and save on server!"|gettext}" class="buttonInvoiceCreateSave iconaction" />
	<img src="/pic/icons/x_invoice_save_email.svg" title="{"Generate invoice, save on server and send per email!"|gettext}" class="buttonInvoiceCreateSaveSend iconaction"/>
	<img src="/pic/icons/x_invoice_save_remoteprint.svg" title="{"Generate invoice, save on server and print remotely!"|gettext}" class="buttonInvoiceCreateSaveRemoteprint iconaction" />
</div>	
{/if}
{if (count($ps.invoices) > 0)}


	{if $guestchangesuccessful == true}
	<div class="alert alert-success">{"The invoice has been successfully generated, stored and sent."|gettext}</div>
	{/if}
	<div class="invoiceerror hidden error">{"Errors occured:"|gettext}<div></div></div>	
	<img src="/pic/icons/view.svg" title="{"Show preview!"|gettext}" class="buttonInvoicePreview iconaction" />
	<img src="/pic/icons/x_invoice_save.svg" title="{"Generate follow-up invoice and save on server!"|gettext}" class="buttonInvoiceCreateSave iconaction" />
	<img src="/pic/icons/x_invoice_save_email.svg" title="{"Generate follow-up invoice, save on server and send per email!"|gettext}" class="buttonInvoiceCreateSaveSend iconaction"/>
	<img src="/pic/icons/x_invoice_save_remoteprint.svg" title="{"Generate follow-up invoice, save on server and print remotely!"|gettext}" class="buttonInvoiceCreateSaveRemoteprint iconaction" />

{/if}

<h1>{"Payments"|gettext}</h1>
<table class="guestpaymentlist smalltable" data-guest_id="{$guest_id}">
	<thead>
	<tr>
		<th>{"Date"|gettext}</th>
		<th>{"Invoice"|gettext}</th>
		<th>{"Comment"|gettext}</th>
		<th>{"Payment status"|gettext}</th>
	</tr>
	</thead>
	<tbody>
	{if (count($ps.payments) == 0)}
		<tr><td colspan="3"><em>{"No payment data available yet."|gettext}</em></td></tr>
	{else}
	{foreach from=$ps.payments item=pay}
		<tr>
			<td>{$pay.ts}</td>
			<td>{$pay.invoicename}</td>
			<td>{$pay.suppdata}</td>
			<td>{$pay.pstatus}</td>
		</tr>
	{/foreach}
	{/if}
	</tbody>
</table>

<form action="" method="post" name="addPaymentForm" class="addPaymentForm apiform noduplicatesubmissions" data-url="/events/#/guests/#/addPayment">
	<input type="hidden" name="class" value="guest" />
	<input type="hidden" name="cb" value="updateGuestInvoiceListTrigger;reloadGuestTable" />
	<input type="hidden" name="fevent_id" value="{$fevent_id}" />
	<input type="hidden" name="guest_id" value="{$guest_id}" />
	<input type="hidden" name="form" value="addPaymentForm" />
	
	<select name="parent_id" class="guestinvoiceselectbox">
		{foreach from=$ps.invoices item=inv}
			<option value="{$inv.id}">{$inv.suppdata}</option>
		{/foreach}
	</select>
	<label><input type="radio" name="room" value="16" /> {"Full Payment (Account balanced)"|gettext} Komplettzahlung (Konto ausgeglichen)</label>
	<label><input type="radio" name="room" value="2" /> {"Partial Payment (Invoice not fully paid yet, status 2)"|gettext} Teilzahlung (Rechnung noch offen, Status 2)</label>
	<label><input type="radio" name="room" value="0" /> {"Payment failed (Invoice not paid yet, status 0)"|gettext} Fehlgeschlagener Zahlungsversuch (Rechnung noch offen, Status 0)</label>
	<label><input type="radio" name="room" value="-1" /> {"Reset (reset invoice to unpaid, status -1)"|gettext} Zurücksetzen (Rechnung wieder offen, Status -1)</label>
	
	<label for="suppdata">{"Notes"|gettext}</label>
	<div class="alert hidden alert-success">{"The payment information has been successfully registered."|gettext}</div>
	<textarea name="suppdata"></textarea>
	
	<input type="submit" value="{"Save"|gettext}" />
</form>

		<h1>{"Tickets"|gettext}</h1>
		<div class="ticketerror hidden error">{"Errors occured:"|gettext}<div></div></div>
		<img src="/pic/icons/view.svg" title="{"Show preview!"|gettext}" class="buttonTicketPreview iconaction" />
		<img src="/pic/icons/x_invoice_save_remoteprint.svg" title="{"Generate ticket, save on server and print remotely!"|gettext}" class="buttonTicketCreateSaveRemoteprint iconaction" />
	</div>
</div>

<!-- 
<h3>Notes</h3>
<table>
	<tr>
		<th>{"Date"|gettext}</th>
		<th>{"Note"|gettext}</th>
	</tr>
	{if (count($ps.notes) == 0)}
		<tr><td colspan="2"><em>{"No notes taken for this guest yet."|gettext}</em></td></tr>
	{else}
	{foreach from=$ps.notes item=nt}
		<tr>
			<td>{$nt.ts}</td>
			<td>{$nt.suppdata}</td>
		</tr>
	{/foreach}
	{/if}
</table>

<form action="" name="addguestnote" class="guestnoteform">
	{if $guestnoteaddedsuccessful == true}
	<div class="alert alert-success">{"Saved!"|gettext}</div>
	{/if}
	<input type="hidden" name="class" value="guest" />
	<input type="hidden" name="cb" value="updateGuestNotes;refreshEventStats" />
	<input type="hidden" name="fevent_id" value="{$fevent_id}" />
	<input type="hidden" name="guest_id" value="{$guest_id}" />
	<input type="hidden" name="form" value="addGuestNoteForm" />
	<textarea name="suppdata"></textarea>
	<input type="submit" value="{"Register payment"|gettext}" />
</form>-->
</div>