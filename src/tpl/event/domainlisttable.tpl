<table class="table table-striped">
<thead>
<tr>
<th>{"Domainname"|gettext}</th>
<th>{"URL Path"|gettext}</th>
<th>{"Expiration date"|gettext}</th>
<th>{"Options"|gettext}</th>
</tr>
</thead>
<tbody>
	{foreach from=$domains item=r}
	<tr>
		<td>{$r.domainname}</td>
		<td><a href="http://{$r.domainname}/{$r.urlpath}" target="_blank">{$r.urlpath}</a></td>
		<td>{$r.expirationdate}</td>
		<td>
		 <a class="ui-btn ui-btn-inline ui-icon-delete ui-btn-icon-notext ui-corner-all" onclick="deleteDomain({$r.domain_id});" title="{"Delete Domain"|gettext}">delete domain</a>
		</td>
	</tr>
	{/foreach}
</tbody>
</table>

