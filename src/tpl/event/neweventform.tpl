<form action="" class="neweventform">
	{if $eventcreatesuccessful == true}
	<div class="alert alert-success">{"Saved!"|gettext}</div>
	{/if}
	<input type="hidden" name="class" value="event" />
	<input type="hidden" name="form" value="create" />
	<input type="hidden" name="cb" value="refreshEvents" />
	<input name="title" type="text" class="input-block-level" placeholder="{"Title of your event"|gettext}" />

	<button class="btn btn-small btn-primary" type="submit">{"Create"|gettext}</button>
</form>