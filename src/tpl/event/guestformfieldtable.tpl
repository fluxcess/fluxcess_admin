<h3>{"Shown Fields"|gettext}</h3>
<table class="table table-striped">
<thead>
<tr>
<th>{"Fieldname"|gettext}</th>
<th>{"Comment"|gettext}</th>
<th>{"Options"|gettext}</th>
</tr>
</thead>
<tbody>
	{foreach from=$fieldsInform item=r}
	<tr>
		<td>{if $r.formlabel != "" && $r.formlabel != $r.fieldname}{$r.formlabel} ({$r.fieldname}){else}{$r.fieldname}{/if}</td>
		<td>{$r.note}</td>
		<td><a onclick="moveField({$r.field_id}, 'down', 'formfield');" title="{"move down"|gettext}"><img src="/pic/icons/arrowdown.svg" alt="{"move down"|gettext}" title="{"move down"|gettext}" /></a>
		 <a onclick="moveField({$r.field_id}, 'up', 'formfield');" title="{"move up"|gettext}"><img src="/pic/icons/arrowup.svg" alt="{"move up"|gettext}" title="{"move up"|gettext}" /></a>
		 <a onclick="toggleFieldExport({$r.field_id}, 'off', 'formfield');" title="{"Remove field from booking form"|gettext}"><img src="/pic/icons/delete.svg" alt="{"Remove field from booking form"|gettext}" title="{"Remove field from booking form"|gettext}" /></a>
		 <a onclick="editField({$r.field_id});" title="{"edit field"|gettext}" ><img src="/pic/icons/edit.svg" alt="{"edit field"|gettext}" title="{"edit field"|gettext}" /></a>
		</td>
	</tr>
	{/foreach}
</tbody>
</table>

<h3>{"Hidden Fields"|gettext}</h3>
<table class="table table-striped">
<thead>
<tr>
<th>{"Fieldname"|gettext}</th>
<th>{"Comment"|gettext}</th>
<th>{"Options"|gettext}</th>
</tr>
</thead>
<tbody>
	{foreach from=$fieldsNotinform item=r}
	<tr>
		<td>{if $r.formlabel != "" && $r.formlabel != $r.fieldname}{$r.formlabel} ({$r.fieldname}){else}{$r.fieldname}{/if}</td>
		<td>{$r.note}</td>
		<td>
			<a onclick="toggleFieldExport({$r.field_id}, 'on', 'formfield');" title="{"Add field to form"|gettext}"><img src="/pic/icons/add.svg" alt="{"Add field to form"|gettext}" title="{"Add field to form"|gettext}" /></a> 
			{if $r.syscolumn == 0} 
				<a onclick="deleteField({$r.field_id});" title="{"Delete field"|gettext}"><img src="/pic/icons/delete.svg" alt="{"Delete field"|gettext}" title="{"Delete field"|gettext}" /></a>
			{/if}
			<a onclick="editField({$r.field_id});" title="{"edit field"|gettext}"><img src="/pic/icons/edit.svg" alt="{"edit field"|gettext}" title="{"edit field"|gettext}" /></a>
		</td>
	</tr>
	{/foreach}
</tbody>
</table> 
