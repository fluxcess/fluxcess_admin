<form action="" class="form-horizontal">
<h2></h2>
	{if $fieldchangesuccessful == true}
	<div class="alert alert-success">{"Saved!"|gettext}</div>
	{/if}
	<input type="hidden" name="class" value="event" />
	<input type="hidden" name="cb" value="closeFieldForm" />
	<input type="hidden" name="fevent_id" value="{$fevent_id}" />
	<input type="hidden" name="field_id" value="{$field_id}" />
	<input type="hidden" name="form" value="changeFieldForm" />
	<div class="control-group">
		<label class="control-label" for="fieldname">{"Field Name"|gettext}</label>
		<div class="controls">
			<input name="fieldname" type="text" class="input-block-level" placeholder="{"Field Name"|gettext}" value="{$ps.fieldname}" {if $ps.syscolumn}disabled{/if} />
			<span class="help-block">{"Title of the column"|gettext}</span>
		</div>
	</div>
	
	<div class="control-group">
		<label class="control-label" for="fieldtypeeasy">{"Field Type"|gettext}</label>
		<div class="controls">
			<select {if $ps.syscolumn}disabled{/if} class="input-block-level" id="fieldtypeeasyselect" name="fieldtypeeasy">{html_options values=$fteasyval output=$fteasy selected=$ps.fieldtypeeasy}</select>
			<span class="help-block">{"Type of the column"|gettext}</span>
		</div>
	</div>
	
	<div class="control-group" id="cgpfid" {if $ps.fieldtypeeasy == 11} style="display: block;"{else} style="display: none;"{/if}>
		<label class="control-label" for="parent_field_id">{"Parent Field"|gettext}</label>
		<div class="controls">
			<select class="input-block-level" id="parentfieldid" name="parent_field_id">{html_options values=$pfval output=$pfout selected=$ps.parent_field_id}</select>
			<span class="help-block">{"Parent Field Id"|gettext}</span>
		</div>
	</div>
	
	<div class="control-group" id="cgradio" {if $ps.fieldtypeeasy == 8 || $ps.fieldtypeeasy == 9 || $ps.fieldtypeeasy == 11} style="display: none;"{/if}>
		<label class="control-label" for="radio">{"Displaytype"|gettext}</label>
		<div class="controls">
			<select class="input-block-level" id="fielddisplaytype" name="fielddisplaytype">{html_options values=$fdtval output=$fdtout selected=$ps.fielddisplaytype}</select>
			<span class="help-block">{"Prepare this field for radiobutton / checkbox usage"|gettext}</span>
		</div>
	</div>
	
	<div class="control-group" id="cgformlabel" {if $ps.fieldtypeeasy == 8 || $ps.fieldtypeeasy == 9 || $ps.fieldtypeeasy == 11} style="display: none;"{/if}>
		<label class="control-label" for="formlabel">{"Label"|gettext}</label>
		<div class="controls">
			<input name="formlabel" type="text" class="input-block-level" placeholder="{"Label"|gettext}" value="{$ps.formlabel}" />
			<span class="help-block">{"Label of the column"|gettext}</span>
		</div>
	</div>
	
	<div class="control-group">
		<label class="control-label" for="note">{"Comment"|gettext}</label>
		<div class="controls">
			<textarea {if $ps.syscolumn}disabled{/if} class="input-block-level"  name="note">{$ps.note}</textarea>
			<span class="help-block">{"Comment"|gettext}</span>
		</div>
	</div>
	
	<div class="control-group" id="cglongcontent" {if $ps.fieldtypeeasy != 9 && $ps.fieldtypeeasy != 11} style="display: none;"{/if}>
		<label class="control-label" for="longcontent">{"Content (for field type HTML)"|gettext}</label>
		<div class="controls">
			<textarea {if $ps.syscolumn}disabled{/if} class="input-block-level"  name="longcontent">{$ps.longcontent}</textarea>
			<span class="help-block">{"HTML Content"|gettext}</span>
		</div>
	</div>
	
	<div class="control-group">
		<label class="control-label" for="radiocheckvalue">{"Value"|gettext}</label>
		<div class="controls">
			<input name="radiocheckvalue" type="text" class="input-block-level" placeholder="{"Value"|gettext}" value="{$ps.radiocheckvalue}" />
			<span class="help-block">{"Value (only for radiobuttons and checkboxes)"|gettext}</span>
		</div>
	</div>
	
	<div class="control-group">
		<label class="control-label" for="isdefaultvalue">{"Is a default value"|gettext}</label>
		<div class="controls">
			<input data-role="flipswitch" name="isdefaultvalue" type="checkbox" class="input-block-level" value="1" {if $ps.isdefaultvalue == true}checked{/if} />
			<span class="help-block">{"Only applicable for radiobuttons / checkboxes."|gettext}</span>
		</div>
	</div>
	
	<div class="control-group">
		<label class="control-label" for="editbyguest">{"Guests are allowed to edit this field"|gettext}</label>
		<div class="controls">
			<input data-role="flipswitch" name="editbyguest" type="checkbox" class="input-block-level" value="1" {if $ps.editbyguest == true}checked{/if} />
			<span class="help-block">{"If checked, this field and its content can be edited by the guest in the feedback form."|gettext}</span>
		</div>
	</div>
	
	<div class="control-group">
		<label class="control-label" for="editbymanager">{"Event managers are allowed to edit this field"|gettext}</label>
		<div class="controls">
			<input data-role="flipswitch" name="editbymanager" type="checkbox" class="input-block-level" value="1" {if $ps.editbymanager == true}checked{/if} />
			<span class="help-block">{"If checked, this field and its content are shown to the event manager in the online guest list."|gettext}</span>
		</div>
	</div>
	<input type="submit" value="{"Save"|gettext}" class="ui-btn ui-corner-all" />
</form>
<script type="text/javascript">
$("#fieldtypeeasyselect").change(function(){
	if ($(this).val() == '9' || $(this).val() == '11') {
		$('#cglongcontent').css('display', 'block');
	} else {
		$('#cglongcontent').css('display', 'none');
	}

	if ($(this).val() == '9' || $(this).val() == '8' || $(this).val() == '11') {
		$('#cgradio').css('display', 'none');
	} else {
		$('#cgradio').css('display', 'block');
	}

	if ($(this).val() == '9' || $(this).val() == '8' || $(this).val() == '11') {
		$('#cgformlabel').css('display', 'none');
	} else {
		$('#cgformlabel').css('display', 'block');
	}

	if ($(this).val() == '11') {
		$('#cgpfid').css('display', 'block');
	} else {
		$('#cgpfid').css('display', 'none');
	}
	
});
</script>