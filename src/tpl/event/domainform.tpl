<h3>{"Domains"|gettext}</h3>

<p class="help-block">
{"Manage your domain(s) here."|gettext}
{"If a guest types to your domain in his / her web browser, he / she can register for your event."|gettext}
</p>
<div id="domainlisttable">
{include file="event/domainlisttable.tpl"}
</div>



<button class="btn" onclick="editDomain(0);">{"Create new domain"|gettext}</button>