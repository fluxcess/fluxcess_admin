<h3>{"Form Page"|gettext} {$seqid}</h3>
<form action="" name="changemultisettingsform" class="changemultisettingsform verticalform apiform" method="post" data-url="/events/{$fevent_id}/multisettings">
	<input type="hidden" name="class" value="event" />
	<input type="hidden" name="cb" value="closeMultisettingsForm;reloadMultisettingsTables" />
	<input type="hidden" name="fevent_id" value="{$fevent_id}" />
	<input type="hidden" name="multisetting" value="{$multisettingtype}" />
	<input type="hidden" name="seqid" value="{$seqid}" />
	<input type="hidden" name="form" value="changemultisettingform" />

	<label for="id">{"Page Id"|gettext}</label>
	<input name="id" type="text" placeholder="{"0"|gettext}" value="{$ps.id}" />
	
	<label for="pagetitle">{"Title"|gettext}</label>
	<input name="pagetitle" type="text" placeholder="{"Personal Data"|gettext}" value="{$ps.pagetitle}" />

	<label for="directaccess">{"Direct Access"|gettext}</label>
	<input name="directaccess" type="text" placeholder="0 / 1" value="{$ps.directaccess}" />
		
	<label for="backallowed">{"Backwards navigation allowed"|gettext}</label>
	<input name="backallowed" type="text" placeholder="0 / 1" value="{$ps.backallowed}" />
	
	<label for="checkout">{"Page leads to checkout"|gettext}</label>
	<input name="checkout" type="text" placeholder="0 / 1" value="{$ps.checkout}" />
	
	<label for="buynow">{"Caption of the 'buy now' button"|gettext}</label>
	<input name="buynow" type="text" placeholder="{"Buy now!"|gettext}" value="{$ps.buynow}" />
	
	<input type="submit" value="{" Save and close"|gettext}" name="submit" class="ui-btn ui-corner-all" />
</form>

<h3>{"Form Page Sections"|gettext}</h3>

<table data-multisetting="formpage" data-n1entity="n1formpagesectiontoformpage" data-parentid="{$ps.id}" data-parentseq="{$seqid}">
	<thead>
		<tr>
			<th>{"Form Page Section"|gettext}</th>
			<th>{"Options"|gettext}</th>
		</tr>
	</thead>
	{foreach from=$n1formpagesectiontoformpage item=r}
		<tr data-multiid="{$r.skey_multiid}">
			<td>
				{$dropdown_formpage_n1formpagesectiontoformpage[$r.sval_vch]}
			</td>
			<td>
				<img src="/pic/icons/delete.svg" alt="{"Remove fieldset from form field page"|gettext}" class="multisettingSubDelete" />
			</td>
		</tr>
	{/foreach}
</table>

<form action="" name="changemultisettingsform" class="changemultisettingsform verticalform apiform" method="post" data-url="/events/{$fevent_id}/multisettings">
	<input type="hidden" name="class" value="event" />
	<input type="hidden" name="cb" value="" />
	<input type="hidden" name="fevent_id" value="{$fevent_id}" />
	<input type="hidden" name="parentmultiname" value="formpage" />
	<input type="hidden" name="parentmultiid" value="{$ps.id}" />
	<input type="hidden" name="multiname" value="n1formpagesectiontoformpage" />
	
	<input type="hidden" name="form" value="addmultisetting1n" />
	<!-- <input type="hidden" name="form" value="changemultisettingform" /> -->

	<label for="val">{"Form Page"|gettext}</label>
	<select name="val">{html_options options=$dropdown_formpage_n1formpagesectiontoformpage selected=$ps.val}</select>
		
	<input type="submit" value="{"Add"|gettext}" name="submit" class="ui-btn ui-corner-all" />
</form>
