<table>
	<thead>
		<tr>
			<th>{"Subject"|gettext}</th>
			<th></th>
		</tr>
	</thead>
	<tbody>
		{if count($emaillist) > 0}
		{foreach from=$emaillist item=em}
			<tr>
				<td><a href="javascript:loadEmail({$fevent_id}, {$em.email_id}, 0);">{$em.subject}</a></td>
			</tr>
		{/foreach} 
		{else}
			<tr><td colspan="2">{"No drafts stored."|gettext}</td>
		{/if}
	</tbody>
</table>