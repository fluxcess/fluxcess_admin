<h3>{"Form Pages"|gettext}</h3>

<table class="multisetting" data-multisetting="formpage">
	<thead>
		<tr>
			<th data-col="seqid">{"Id"|gettext}</th>
			<th data-col="id">{"Id"|gettext}</th>
			<th data-col="pagetitle">{"Title"|gettext}</th>
			<th>{"Options"|gettext}</th>
		</tr>
	</thead>
	<tbody>
		{if count($ps.formpage) == 0}
		<tr>
			<td colspan="4">---</td>
		</tr>
		{else} {foreach from=$ps.formpage item=cs}
		<tr>
			<td>{$cs.seqid}</td>
			<td>{$cs.id}</td>
			<td>{$cs.pagetitle}</td>
		</tr>
		{/foreach} {/if}
	</tbody>
</table>
<h1>{"Add new ..."|gettext}</h1>
<a href="Javascript:multisettingNew('formpage');">New</a>