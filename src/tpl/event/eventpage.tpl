<div class="hero-unit" id="eventtitle">
		<h1><span class="valEvtitle">{$ev.title}</span></h1>
		<p class="eventmicrodata">Event-Id: <span class="valEvfevent_id">{$fevent_id}</span> | {"Invited"|gettext}: <span class="valEvinvited">{$stat.invited}</span> | 
		{"Registered"|gettext}: <span class="valEvregistered">{$stat.registered}</span> | 
		{"Cancelled"|gettext}: <span class="valEvcancelled">{$stat.cancelled}</span>
		{if $ev.selfregistrationallowed == 1} | {"Self registered"|gettext}: <span class="valEvselfregistered">{$stat.selfregistered}</span>{/if}
		</p>
		<p class="valEvdescription">{$ev.description}</p>
</div>

<nav class="mmenu">
    <ul>
    	<li class="active"><a class="linkFevent" href="#fevent/{$fevent_id}/guestlist">{"Guest list"|gettext}</a></li>
		<li><a class="linkFevent" href="#fevent/{$fevent_id}/impexp">{"Import/Export"|gettext}</a></li>
		<li><a class="linkFevent" href="#fevent/{$fevent_id}/settings">{"Settings"|gettext}</a></li>
		<li><a class="linkFevent" href="#fevent/{$fevent_id}/actions">{"Actions"|gettext}</a></li>
		<li><a class="linkFevent" href="#fevent/{$fevent_id}/history">{"History"|gettext}</a></li>
		<li><a class="linkFevent" href="#fevent/{$fevent_id}/massmail">{"Mass Mails"|gettext}</a></li>
		<li><a class="linkFevent" href="#fevent/{$fevent_id}/checkin">{"Check-In"|gettext}</a></li>
    </ul>
</nav>
<div class="clear"></div>
<div class="subpage guestlist">{include file="event/guestlisttable.tpl"}</div>
<div class="subpage impexp hidden">{include file="event/importexport.tpl"}</div>
<div class="subpage settings hidden">{include file="event/eventform.tpl"}</div>
<div class="subpage actions hidden">{include file="event/eventactions.tpl"}</div>
<div class="subpage history hidden">{include file="event/rsvphistory.tpl"}</div>
<div class="subpage massmail hidden">{include file="event/massmails.tpl"}</div>
<div class="subpage checkin hidden">{include file="event/eventcheckinlink.tpl"}</div>
<div class="halfsubpage hidden">:-)<br />:-)</div>
<div class="clear"></div>
<div id="eventid">{$fevent_id}</div>


<div id="popupGuest" class="popup">
	<div class="closepopup"><a href="javascript:closePopup();">close</a></div>
	<div id="popupPanelGuest" class="panel">
		:-)
	</div>
</div>

<div id="popupField" class="popup">
	<div class="closepopup"><a href="javascript:closePopup();">close</a></div>
	<div id="popupPanelField" class="panel">
		:-)
	</div>
</div>

<div id="popupDomain" class="popup">
	<div class="closepopup"><a href="javascript:closePopup();">close</a></div>
	<div id="popupPanelDomain" class="panel">
		:-)
	</div>
</div>
<div id="popupMultisetting" class="popup">
	<div class="closepopup"><a href="javascript:closePopup();">close</a></div>
	<div id="popupPanelMultisetting" class="panel">
		:-)
	</div>
</div>