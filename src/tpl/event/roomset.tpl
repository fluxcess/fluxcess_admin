<h3>{"Room Sets"|gettext}</h3>

<table class="multisetting" data-multisetting="roomset">
	<thead>
		<tr>
			<th data-col="seqid">{"Id"|gettext}</th>
			<th data-col="id">{"Id"|gettext}</th>
			<th data-col="title">{"Title"|gettext}</th>
			<th>{"Options"|gettext}</th>
		</tr>
	</thead>
	<tbody>
		{if count($ps.roomset) == 0}
		<tr>
			<td colspan="4">---</td>
		</tr>
		{else} {foreach from=$ps.roomset item=cs}
		<tr>
			<td>{$cs.seqid}</td>
			<td>{$cs.id}</td>
			<td>{$cs.title}</td>
		</tr>
		{/foreach} {/if}
	</tbody>
</table>
<h1>{"Add new ..."|gettext}</h1>
<a href="Javascript:multisettingNew('roomset');">New</a>