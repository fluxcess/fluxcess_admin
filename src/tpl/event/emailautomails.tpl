<h3>{"Automatic emails"|gettext}</h3>
<table id="emailautomaticemailtable">
	<thead>
		<tr>
			<th>{"Id"|gettext}</th>
			<th>{"Title"|gettext}</th>
			<th>{"Description"|gettext}</th>
			<th>{"Options"|gettext}</th>
		</tr>
	</thead>
	<tbody>
		{foreach from=$mmtemplates item=tpl}
			<tr>
				<td>{$tpl.mailtemplate_id}</td>
				<td>{$tpl.title}</td>
				<td>{$tpl.description}</td>
				<td>
					
				</td>
			</tr>
		{/foreach}
	</tbody>
</table>
{if count($amtemplates) == 0}
	{"Here you can define automatically sent emails."|gettext}
{/if}