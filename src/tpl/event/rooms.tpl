<h3>{"Rooms"|gettext}</h3>

<table class="multisetting" data-multisetting="room">
	<thead>
		<tr>
			<th data-col="seqid">{"Id"|gettext}</th>
			<th data-col="id">{"Id"|gettext}</th>
			<th data-col="title">{"Name"|gettext}</th>
			<th data-col="maxpax">{"Maximum Capacity"|gettext}</th>
			<th>{"Options"|gettext}</th>
		</tr>
	</thead>
	<tbody>
		{if count($ps.room) == 0}
		<tr>
			<td colspan="5">---</td>
		</tr>
		{else} {foreach from=$ps.room item=r}
		<tr>
			<td>{$r.id}</td>
			<td>{$r.title}</td>
			<td>{$r.maxpax}</td>
		</tr>
		{/foreach} {/if}
	</tbody>
</table>
<h1>{"Add new ..."|gettext}</h1>
<a href="Javascript:multisettingNew('room');">New</a>