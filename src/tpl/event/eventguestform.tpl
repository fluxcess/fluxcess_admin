<h3>{"Fields"|gettext}</h3>

<p class="help-block">
{"Here you can determine the order of fields in the guest's booking form."|gettext}
{"In the details for each field, you can determine if the field content can be modified by your guest."|gettext}
{"The guest can access the booking form via a Domain."|gettext}
{"You can create a Domain under Settings > Domains."|gettext}
</p>

<div id="guestformfieldlisttables">
{include file="event/guestformfieldtable.tpl"}
</div>


<button class="btn" onclick="editField(0);">{"Create new field"|gettext}</button>