<h3>{"Option Collection"|gettext} {$seqid}</h3>
<form action="" name="changemultisettingsform" class="changemultisettingsform verticalform apiform" method="post" data-url="/events/{$fevent_id}/multisettings">
	<input type="hidden" name="class" value="event" />
	<input type="hidden" name="cb" value="closeMultisettingsForm;reloadMultisettingsTables" />
	<input type="hidden" name="fevent_id" value="{$fevent_id}" />
	<input type="hidden" name="multisetting" value="{$multisettingtype}" />
	<input type="hidden" name="seqid" value="{$seqid}" />
	<input type="hidden" name="form" value="changemultisettingform" />

	<label for="id">{"Id"|gettext}</label>
	<input name="id" type="text" value="{$ps.id}" />
	
	<label for="title">{"Title"|gettext}</label>
	<input name="title" type="text" value="{$ps.title}"/>
		
	<input type="submit" value="{"Save and close"|gettext}" name="submit" class="ui-btn ui-corner-all" />
</form>