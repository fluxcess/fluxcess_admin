<h3>{"Your email templates"|gettext}</h3>
<table id="emailtemplatetable" {if count($mmtemplates) == 0}class="hidde	"{/if} >
	<thead>
		<tr>
			<th>{"Id"|gettext}</th>
			<th>{"Title"|gettext}</th>
			<th>{"Description"|gettext}</th>
			<th>{"Options"|gettext}</th>
		</tr>
	</thead>
	<tbody>
		{foreach from=$mmtemplates item=tpl}
			<tr>
				<td>{$tpl.mailtemplate_id}</td>
				<td><a href="#emailtemplate/{$tpl.mailtemplate_id}">{$tpl.title}</a></td>
				<td>{$tpl.description}</td>
				<td>
					
				</td>
			</tr>
		{/foreach}
	</tbody>
</table>
{if count($mmtemplates) == 0}
	{"Here you can upload your own email templates."|gettext}
{/if}

<div class="togglesection">
	<div class="toggler">{"Add email template from code ..."|gettext}</div>
	<div class="toggled">
		<form action="" method="post" class="apiform" data-url="/emailtemplates">
			<input type="hidden" name="cb" value="reloadEmailtemplateTable" />
			<label for="title">{"Template Title"|gettext}</label>
			<input name="title" />
			<label for="emailsubject">{"Email Subject"|gettext}</label>
			<input name="emailsubject" />
			<label for="content">{"Template Code"|gettext}</label>
			<textarea name="content"></textarea>
			<label for="description">{"Description"|gettext}</label>
			<textarea name="description"></textarea>
			<input type="submit" value="{"Save!"|gettext}" />
		</form>
	</div>
</div>
