<table class="table table-striped">
<thead>
<tr>
<th>{"Sequence"|gettext}</th>
<th>{"Extension"|gettext}</th>
<th>{"Description"|gettext}</th>
<th>{"Details"|gettext}</th>
<th>{"Active"|gettext}</th>
<th>{"Options"|gettext}</th>
</tr>
</thead>
<tbody class="valBookingProcessSteps">
	{foreach from=$regprocsteps item=r}
	<tr>
		<td>{$r.sequence}</td>
		<td>{$r.title} ({$r.fname})</td>
		<td>{$r.description}</td>
		<td>{$r.stepdetails}</td>
		<td>{$r.active}</td>
		<td><a class="ui-btn ui-btn-inline ui-icon-carat-d ui-btn-icon-notext ui-corner-all" onclick="moveStep({$r.step_id}, 'down');"><i class="icon-arrow-down"></i></a>
		 <a class="ui-btn ui-btn-inline ui-icon-carat-u ui-btn-icon-notext ui-corner-all" onclick="moveStep({$r.step_id}, 'up');"><i class="icon-arrow-up"></i></a>
		 <a class="ui-btn ui-btn-inline ui-icon-minus ui-btn-icon-notext ui-corner-all" onclick="removeStep({$r.step_id});" title="{"Remove step from process"|gettext}"><i class="icon-minus-sign"></i></a>
		 <a class="ui-btn ui-btn-inline ui-icon-minus ui-btn-icon-notext ui-corner-all" onclick="disableStep({$r.step_id});" title="{"Disable step"|gettext}"><i class="icon-minus-sign"></i></a>
		 <a class="ui-btn ui-btn-inline ui-icon-edit ui-btn-icon-notext ui-corner-all" onclick="editStep({$r.step_id});">edit</a>
		</td>
	</tr>
	{/foreach}
</tbody>
</table>