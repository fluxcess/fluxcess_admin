<h2>{"Import/Export"|gettext}</h2>
<h3>{"Import"|gettext}</h3>
{"Import your spreadsheet full of guests into the digital guestlist software!"|gettext} 
{"For information about the import format, please have a look at the documentation."|gettext}
{"You can change the number and sequence of columns under Settings > Fields."|gettext}<br />
<a rel="external" target="_blank" href="http://doku.fast-lane.info/node/7">{"Documentation"|gettext}</a>
<iframe border="0" class="uploadiframe" src="/index.php?type=iframe&module=event&fevent_id={$fevent_id}&submodule=importfile"> </iframe>

<h3>{"Export"|gettext}</h3>
<table class="exportlinks">
	<thead>
		<tr>
			<th>{"Title"|gettext}</th>
			<th>{"Description"|gettext}</th>
			<th>{"File"|gettext}</th>
		</tr>
	</thead>
	<tbody>
	    {foreach from=$ev.exports item=ex}
	    	<tr>
	    		<td>{$ex[1]}</td>
	    		<td>{$ex[2]}</td>
	    		<td>
	    			{foreach from=$ex[3] key=filetype item=link}
	    				<a data-ajax="false" href="{$link}"><img src="/pic/{$filetype}-icon.png" alt="{"{$filetype}"|gettext}" /></a> 
	    			{/foreach}
	    		</td>
	    	</tr>
	    {/foreach}
	</tbody>
</table>
