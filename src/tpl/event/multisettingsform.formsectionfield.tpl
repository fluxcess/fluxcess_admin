<h3>{"Form Fields"|gettext} {$seqid}</h3>
<form action="" name="changemultisettingsform" class="changemultisettingsform verticalform apiform" method="post" data-url="/events/{$fevent_id}/multisettings">
	<input type="hidden" name="class" value="event" />
	<input type="hidden" name="cb" value="closeMultisettingsForm;reloadMultisettingsTables" />
	<input type="hidden" name="fevent_id" value="{$fevent_id}" />
	<input type="hidden" name="multisetting" value="{$multisettingtype}" />
	<input type="hidden" name="seqid" value="{$seqid}" />
	<input type="hidden" name="form" value="changemultisettingform" />

	<label for="id">{"Id"|gettext}</label>
	<input name="id" type="text" value="{$ps.id}" />
	
	<label for="formsectionid">{"Form Section Id"|gettext}</label>
	<select name="formsectionid">{html_options options=$dropdown_formsectionfield_formsectionid selected=$ps.formsectionid}</select>

	<label for="type">{"Type"|gettext}</label>
	<select name="type">{html_options options=$options_formsectionfield_type selected=$ps.type}</select>
	
	<label for="content">{"Content"|gettext}</label>
	<textarea name="content" type="text" placeholder="">{$ps.content}</textarea>
		
	<label for="minimumchoices">{"Minimum number of choices required"|gettext}</label>
	<input name="minimumchoices" type="text" value="{$ps.minimumchoices}"/>
	
	<label for="maximumchoices">{"Maximum number of allowed choices"|gettext}</label>
	<input name="maximumchoices" type="text" value="{$ps.maximumchoices}"/>
	
	<label for="name">{"Name"|gettext}</label>
	<input name="name" type="text" value="{$ps.name}"/>
	
	<label for="ticketchoice">{"Choice of tickets"|gettext}</label>
	<input name="ticketchoice" type="text" value="{$ps.ticketchoice}"/>
	
	<label for="caption">{"Caption"|gettext}</label>
	<input name="caption" type="text" value="{$ps.caption}"/>
	
	<label for="mandatory">{"Mandatory"|gettext}</label>
	<input name="mandatory" type="text" value="{$ps.mandatory}"/>
	
	<label for="optionset">{"Optionset"|gettext}</label>
	<input name="optionset" type="text" value="{$ps.optionset}"/>
	
	<label for="minlength">{"Minimum length"|gettext}</label>
	<input name="minlength" type="text" value="{$ps.minlength}"/>
	
	<label for="maxlength">{"Maximum length"|gettext}</label>
	<input name="maxlength" type="text" value="{$ps.maxlength}"/>
	
	<input type="submit" value="{" Save and close"|gettext}" name="submit" class="ui-btn ui-corner-all" />
</form>