<h3>{"Form Page Section"|gettext} {$seqid}</h3>
<form action="" name="changemultisettingsform" class="changemultisettingsform verticalform apiform" method="post" data-url="/events/{$fevent_id}/multisettings">
	<input type="hidden" name="class" value="event" />
	<input type="hidden" name="cb" value="closeMultisettingsForm;reloadMultisettingsTables" />
	<input type="hidden" name="fevent_id" value="{$fevent_id}" />
	<input type="hidden" name="multisetting" value="{$multisettingtype}" />
	<input type="hidden" name="seqid" value="{$seqid}" />
	<input type="hidden" name="form" value="changemultisettingform" />

	<label for="id">{"Section Id"|gettext}</label>
	<input name="id" type="text" placeholder="{"0"|gettext}" value="{$ps.id}" />
	
	<label for="formpageid">{"Page Id"|gettext}</label>
	<select name="formpageid">{html_options options=$dropdown_formpagesection_formpageid selected=$ps.formpageid}</select>
	
	<label for="type">{"Type"|gettext}</label>
	<select name="type">{html_options options=$options_formpagesection_type selected=$ps.type}</select>

	<label for="name">{"Name"|gettext}</label>
	<input name="name" type="text" value="{$ps.name}" />
	
	<label for="content">{"Content"|gettext}</label>
	<textarea name="content">{$ps.content}</textarea>
		
	<label for="maxwidth">{"Maximum width"|gettext}</label>
	<input name="maxwidth" type="text" value="{$ps.maxwidth}" />
	
	<label for="minimumchoices">{"Minimum number of choices"|gettext}</label>
	<input name="minimumchoices" type="text" value="{$ps.minimumchoices}" />
	
	<label for="maximumchoices">{"Maximum number of choices"|gettext}</label>
	<input name="maximumchoices" type="text" value="{$ps.maximumchoices}" />
	
	<label for="sessionlayout">{"Id of the session layout"|gettext}</label>
	<input name="sessionlayout" type="text" value="{$ps.sessionlayout}" />
	
	<label for="thbackgroundcolor">{"Headline background color"|gettext}</label>
	<input name="thbackgroundcolor" type="text" value="{$ps.thbackgroundcolor}" />
	
	<label for="thcolor">{"Headline font color"|gettext}</label>
	<input name="thcolor" type="text" value="{$ps.thcolor}" />
	
	<label for="invoiceparentfield">{"Invoice Parent Field"|gettext}</label>
	<input name="invoiceparentfield" type="text" value="{$ps.invoiceparentfield}"/>
	
	<label for="streamlist">{"List of streams"|gettext}</label>
	<input name="streamlist" type="text" value="{$ps.streamlist}"/>
	
	<label for="ticketcategorylist">{"List of ticket categories"|gettext}</label>
	<textarea name="ticketcategorylist">{$ps.ticketcategorylist}</textarea>
	
	<label for="groupname">{"Group Name"|gettext}</label>
	<input name="groupname" type="text" value="{$ps.groupname}"/>
	
	<label for="display">{"Display"|gettext}</label>
	<input name="display" type="text" value="{$ps.display}"/>
		
	<input type="submit" value="{" Save and close"|gettext}" name="submit" class="ui-btn ui-corner-all" />
</form>