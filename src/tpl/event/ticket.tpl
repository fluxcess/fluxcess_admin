<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<title>Line5 Fast Lane Check-In - {"The Digital Guest List"|gettext}</title>
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<meta name="description" content="">
	<meta name="author" content="">
	<link href="/css/fast-lane-checkin-ticket.css" rel="stylesheet">
	{if $TESTING == true}
	<style type="text/css">
		body { background-color: #cfc; }
	</style>
	{/if}
</head>
<body>
{$content} 
<script type="text/javascript">
print();
</script>
{if $TESTING == true}<div id="testing">TESTING</div>{/if}
</body>
</html>
