<h3>{"Conference Streams"|gettext}</h3>

<table class="multisetting" data-multisetting="conferencestream">
	<thead>
		<tr>
			<th data-col="seqid">{"Id"|gettext}</th>
			<th data-col="id">{"Id"|gettext}</th>
			<th data-col="title">{"Title"|gettext}</th>
			<th>{"Options"|gettext}</th>
		</tr>
	</thead>
	<tbody>
		{if count($ps.conferencesession) == 0}
		<tr>
			<td colspan="4">---</td>
		</tr>
		{else} {foreach from=$ps.conferencesession item=cs}
		<tr>
			<td>{$cs.id}</td>
			<td>{$cs.title}</td>
		</tr>
		{/foreach} {/if}
	</tbody>
</table>
<h1>{"Add new ..."|gettext}</h1>
<a href="Javascript:multisettingNew('conferencestream');">New</a>