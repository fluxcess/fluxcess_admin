		<form action="" name="changeguestform" class="guestform apiform" method="post" data-url="/events/{$fevent_id}/guests/{$guest_id}">
		
			{if $guestchangesuccessful == true}
				<div class="alert alert-success">{"Saved!"|gettext}</div>
			{/if}
			<input type="hidden" name="class" value="guest" />
			<input type="hidden" name="cb" value="saveclose:closeGuestForm;refreshEventStats|save:reloadGuestForm" />
			<input type="hidden" name="fevent_id" value="{$fevent_id}" />
			<input type="hidden" name="guest_id" value="{$guest_id}" />
			<input type="hidden" name="form" value="changeGuestForm" />
			{foreach from=$ev.quickaddformfield item=qaff}
				{if $qaff.type == 'html'}
					{$qaff.content}
				{elseif $qaff.type == 'input'}
					<label>{$qaff.caption}</label>
					<input name="{$qaff.name}" />
				{elseif $qaff.type == 'select'}
					<label>{$qaff.caption}</label>
					<select name="{$qaff.name}">
						{if strlen($qaff.optionset) > 0}
							{html_options options=$ev.optionsets[$qaff.optionset]}
						{elseif strlen($qaff.ticketchoice) > 0}
							{assign var=myticketchoices value=","|explode:$qaff.ticketchoice}
							{foreach from=$myticketchoices item=tc}
							{print_r($ticketcategory[$tc])}
								<option value="{$tc}">
									{$ticketcategory[$tc].title} {$ticketcategory[$tc].price|number_format:2:",":"."}
								</option>
							{/foreach}
						{/if}
					</select>
				{else}
					- {$qaff.type}
				{/if}
			{/foreach} 
			<div class="clear"><br /></div>
			
			<input type="submit" value="{"Save and close"|gettext}" name="btn_saveclose" />
			<div>
				<input type="submit" value="{"Save"|gettext}" name="btn_save" />
			</div>
		</form>