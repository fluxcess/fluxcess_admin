<h3>{"Form Section Fields"|gettext}</h3>

<table class="multisetting" data-multisetting="formsectionfield">
	<thead>
		<tr>
			<th data-col="seqid">{"Id"|gettext}</th>
			<th data-col="id">{"Id"|gettext}</th>
			<th data-col="type">{"Type"|gettext}</th>
			<th>{"Options"|gettext}</th>
		</tr>
	</thead>
	<tbody>
		{if count($ps.formsectionfield) == 0}
		<tr>
			<td colspan="4">---</td>
		</tr>
		{else} {foreach from=$ps.formsectionfield item=cs}
		<tr>
			<td>{$cs.id}</td>
			<td>{$cs.title}</td>
			<td>{$cs.type}</td>
			<td>
				<img src="/pic/icon/arrowdown.svg" />
				<img src="/pic/icon/arrowup.svg" />
			</td>
		</tr>
		{/foreach} {/if}
	</tbody>
</table>
<h1>{"Add new ..."|gettext}</h1>
<a href="Javascript:multisettingNew('formsectionfield');">New</a>