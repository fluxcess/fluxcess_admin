{if $massmailsentsuccessfully == 1}
	<div class="alert alert-success">{"Your mass email has been sent."|gettext}<br />
	{"Number of email recipients: "|gettext} {$massmailsentnumber}</div>
{else}
<form action="" method="post" class="noduplicatesubmissions" name="massmailsendform" id="massmailsendform">
	<div class="alert alert-success alert-hidden">{"The test email has been sent."|gettext}</div>
	{"Send a circular letter to your guests with this function."|gettext} 
	{"For security reasons, emails to more than 100 recipients at a time cannot be sent."|gettext} 
	{"There is a limit of 3 mass mailings per user per day."|gettext} 
	{"Please do not send confidential data via this tool. All data is transmitted unencrypted."|gettext}
	
	{if !isset($ev.mailsender) || $ev.mailsender == ''}
		<div class="errtext hideifset his_mailsender">{$ev.mailsender}
			{"Please add a mail sender address in the Settings > Email tab before sending mass mails."|gettext}
		</div>
	{/if} 
	<fieldset>
		<legend>{"Template"|gettext}</legend>
		<label for="emailtemplate_id">{"Email template"|gettext}</label>
		<select name="emailtemplate_id" class="emailtemplate_id_options">
		</select>
	</fieldset>
	
	<input type="hidden" name="class" value="event" />
	<!-- <input type="hidden" name="cb" value="refreshEventTitle" />-->
	<input type="hidden" name="fevent_id" class="valEvfevent_id" value="{$fevent_id}" />
	<input type="hidden" name="form" value="sendMassMail" />
	<input type="hidden" name="email_id" value="{$ps.email_id}" />
	
	<fieldset data-role="controlgroup">
		<legend>{"Recipients"|gettext}</legend>
		<input name="massmailrecipients" id="massmailrecipients_1" type="checkbox" value="1" {if (array_search('1', $ps.massmailrecipients) !== false)} checked{/if} />
		<label for="massmailrecipients_1">{"All"|gettext} (<span class="valEvMMR1">{$massmailcount[1]}</span>)</label> 
		<input name="massmailrecipients" id="massmailrecipients_2" type="checkbox" value="2" {if (array_search('2', $ps.massmailrecipients) !== false)} checked{/if} />
		<label for="massmailrecipients_2">{"Confirmed"|gettext} (<span class="valEvMMR2">{$massmailcount[2]}</span>)</label>
		<input name="massmailrecipients" id="massmailrecipients_3" type="checkbox" value="3" {if (array_search('3', $ps.massmailrecipients) !== false)} checked {/if}/>
		<label for="massmailrecipients_3">{"Cancelled"|gettext} (<span class="valEvMMR3">{$massmailcount[3]}</span>)</label>
	 	<input name="massmailrecipients" id="massmailrecipients_4" type="checkbox" value="4" {if (array_search('4', $ps.massmailrecipients) !== false)} checked{/if}/>
	 	<label for="massmailrecipients_4">{"Unsure"|gettext} (<span class="valEvMMR4">{$massmailcount[4]}</span>)</label>
	 	<input name="massmailrecipients" id="massmailrecipients_5" type="checkbox" value="5" {if (array_search('5', $ps.massmailrecipients) !== false)} checked{/if}/>
	 	<label for="massmailrecipients_5">{"Never logged in"|gettext} (<span class="valEvMMR5">{$massmailcount[5]}</span>)</label>

	 </fieldset>
	 
	 <fieldset>
	 <legend>{"Subject"|gettext}</legend>
	 <input name="massmailsubject" value="{$ps.massmailsubject}" placeholder="{"Please type the subject for your mass mail here!"|gettext}" />
	 </fieldset>
	 <h3>{"Content"|gettext}</h3>
	 	<label for="massmailcontent">{"Content of the mass mail"|gettext}:</label>
		<textarea name="massmailcontent" class="input-block-level htmlinput">{$ps.massmailcontent}</textarea>
	 
	 <fieldset data-role="controlgroup">
	 	<legend>{"Send"|gettext}</legend>
	 	<input type="radio" id="massmailsendingtype_1" name="massmailsendingtype" value="1" {if $ps.massmailsendingtype == 1 || !isset($ps.massmailsendingtype)} checked{/if} />
	 	<label for="massmailsendingtype_1">{"Send 1 test email with the data of a specific guest to a manually specified email address"|gettext}</label>
	 	<input type="radio" id="massmailsendingtype_3" name="massmailsendingtype" value="3" {if $ps.massmailsendingtype == 3} checked{/if} />
	 	<label for="massmailsendingtype_3">{"Send the entire set of emails as a test to a manually specified email address"|gettext} </label>
	 	<input type="radio" id="massmailsendingtype_4" name="massmailsendingtype" value="4" {if $ps.massmailsendingtype == 4} checked{/if} />
	 	<label for="massmailsendingtype_4">{"Send the email to the selected guests."|gettext}</label>
	 </fieldset>
	 
	 <div>
	 <div id="massmailsendingtypeoptions1">
	 	<label for="testmailguestid">Guest id for the test email:</label>
	 	<input name="testmailguestid" id="testmailguestid" value="1" /> {"to"|gettext}
	 	<label for="testmailrecipient1">Redirect the test email to:</label> 
	 	<input name="testmailrecipient1" id="testmailrecipient1" value="{$ps.testmailrecipient1}" placeholder="{"mail@example.org"|gettext}" />
	 </div>
	 <div class="hide" id="massmailsendingtypeoptions3">
	 	<label for="testmailrecipient3">Redirect all emails to:</label>
	 	<input name="testmailrecipient3" id="testmailrecipient3" value="{$ps.testmailrecipient3}" placeholder="{"mail@example.org"|gettext}" />
	 </div>
	 </div>
	 <button class="btn btn-small btn-primary onlyonce" type="submit">{"Send Mass Mail"|gettext}</button>
	 <button class="btn btn-small btn-primary onlyonce" onclick="saveemail();" type="button">{"Save Draft"|gettext}</button>
</form>
{/if}