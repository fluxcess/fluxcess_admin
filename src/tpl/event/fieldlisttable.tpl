<h3>{"Exported Fields"|gettext}</h3>
<table class="table table-striped">
<thead>
<tr>
<th>{"Fieldname"|gettext}</th>
<th>{"Comment"|gettext}</th>
<th>{"Options"|gettext}</th>
</tr>
</thead>
<tbody class="valExportedFields">
	{foreach from=$fieldsExported item=r}
	<tr>
		<td>{if $r.formlabel != "" && $r.formlabel != $r.fieldname}{$r.formlabel} ({$r.fieldname}){else}{$r.fieldname}{/if}</td>
		<td>{$r.note}</td>
		<td><a onclick="moveField({$r.field_id}, 'down', 'export');" title="{"move down"|gettext}"><img src="/pic/icons/arrowdown.svg" alt="{"move down"|gettext}" title="{"move down"|gettext}" /></a>
		 <a onclick="moveField({$r.field_id}, 'up', 'export');" title="{"move up"|gettext}"><img src="/pic/icons/arrowup.svg" alt="{"move up"|gettext}" title="{"move up"|gettext}" /></a>
		 <a onclick="toggleFieldExport({$r.field_id}, 'off', 'export');" title="{"Remove field from export"|gettext}"><img src="/pic/icons/delete.svg" alt="{"Remove field from export"|gettext}" title="{"Remove field from export"|gettext}" /></a>
		 <a onclick="editField({$r.field_id});" title="{"edit field"|gettext}"><img src="/pic/icons/edit.svg" alt="{"edit field"|gettext}" title="{"edit field"|gettext}" /></a>
		</td>
	</tr>
	{/foreach}
</tbody>
</table>

<h3>{"More Fields"|gettext}</h3>
<table class="table table-striped">
<thead>
<tr>
<th>{"Fieldname"|gettext}</th>
<th>{"Comment"|gettext}</th>
<th>{"Options"|gettext}</th>
</tr>
</thead>
<tbody>
	{foreach from=$fieldsNotexported item=r}
	<tr>
		<td>{if $r.formlabel != "" && $r.formlabel != $r.fieldname}{$r.formlabel} ({$r.fieldname}){else}{$r.fieldname}{/if}</td>
		<td>{$r.note}</td>
		<td>
			<a onclick="toggleFieldExport({$r.field_id}, 'on', 'export');" title="{"Add field to export"|gettext}"><img src="/pic/icons/add.svg" alt="{"Add field to export"|gettext}" title="{"Add field to export"|gettext}" /></a> 
			{if $r.syscolumn == 0} 
				<a alt="{"Delete field"|gettext}" title="{"Delete field"|gettext}" onclick="deleteField({$r.field_id});"><img src="/pic/icons/delete.svg" alt="{"Delete field"|gettext}" title="{"Delete field"|gettext}" /></a>
			{/if}
			<a onclick="editField({$r.field_id});"><img src="/pic/icons/edit.svg" alt="{"edit field"|gettext}" title="{"edit field"|gettext}" /></a>
		</td>
	</tr>
	{/foreach}
</tbody>
</table> 
