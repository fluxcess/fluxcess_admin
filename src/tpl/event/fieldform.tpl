<h3>{"Fields"|gettext}</h3>

<p class="help-block">
{"The list of fields determines which fields are available in the database."|gettext}
{"Their sequence determines in which sequence fields are expected from a data-import."|gettext}
{"In order to change the sequence of fields in the booking form, please check Settings > Booking form."|gettext}
</p>
<div id="fieldlisttables">
{include file="event/fieldlisttable.tpl"}
</div>



<button class="btn" onclick="editField(0);">{"Create new field"|gettext}</button>