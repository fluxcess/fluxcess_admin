<h3>{"Conference Session"|gettext}</h3>

<table class="multisetting" data-multisetting="conferencesession">
	<thead>
		<tr>
			<th data-col="seqid">{"Id"|gettext}</th>
			<th data-col="id">{"Id"|gettext}</th>
			<th data-col="datetime">{"Start Time"|gettext}</th>
			<th data-col="type">{"Type"|gettext}</th>
			<th data-col="title">{"Title"|gettext}</th>
			<th data-col="speaker">{"Speaker"|gettext}</th>
			<th>{"Options"|gettext}</th>
		</tr>
	</thead>
	<tbody>
		{if count($ps.conferencesession) == 0}
		<tr>
			<td colspan="7">---</td>
		</tr>
		{else} {foreach from=$ps.conferencesession item=cs}
		<tr>
			<td>{$cs.no}</td>
			<td>{$cs.ticketcategorycode}</td>
		</tr>
		{/foreach} {/if}
	</tbody>
</table>
<h1>{"Add new ..."|gettext}</h1>
<a href="Javascript:multisettingNew('conferencesession');">New</a>