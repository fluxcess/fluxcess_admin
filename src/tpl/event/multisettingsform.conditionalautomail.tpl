<h3>{"Conditional Automatic Emails"|gettext} {$seqid}</h3>
<form action="" name="changemultisettingsform" class="changemultisettingsform verticalform apiform" method="post" data-url="/events/{$fevent_id}/multisettings">
	<input type="hidden" name="class" value="event" />
	<input type="hidden" name="cb" value="closeMultisettingsForm;reloadMultisettingsTables" />
	<input type="hidden" name="fevent_id" value="{$fevent_id}" />
	<input type="hidden" name="multisetting" value="{$multisettingtype}" />
	<input type="hidden" name="seqid" value="{$seqid}" />
	<input type="hidden" name="form" value="changemultisettingform" />

	<label for="emailtemplateid">{"Email Template Id"|gettext}</label>
	<input name="emailtemplateid" type="text" placeholder="{"0"|gettext}" value="{$ps.emailtemplateid}" />
	
	<label for="emailtrigger">{"Email Trigger"|gettext}</label>
	<input name="emailtrigger" type="text" placeholder="{"booking"|gettext}" value="{$ps.emailtrigger}" />

	<label for="emailcondition">{"Email Condition"|gettext}</label>
	<textarea name="emailcondition" type="text" placeholder="">{$ps.emailcondition}</textarea>
		
	<input type="submit" value="{" Save and close"|gettext}" name="submit" class="ui-btn ui-corner-all" />
</form>