<h3>{"Option collections"|gettext}</h3>

<table class="multisetting" data-multisetting="optioncollection">
	<thead>
		<tr>
			<th data-col="seqid">{"Id"|gettext}</th>
			<th data-col="id">{"Id"|gettext}</th>
			<th data-col="title">{"Title"|gettext}</th>
			<th>{"Options"|gettext}</th>
		</tr>
	</thead>
	<tbody>
		{if count($ps.optioncollection) == 0}
		<tr>
			<td colspan="4">---</td>
		</tr>
		{else} {foreach from=$ps.optioncollection item=cs}
		<tr>
			<td>{$cs.seqid}</td>
			<td>{$cs.id}</td>
			<td>{$cs.title}</td>
		</tr>
		{/foreach} {/if}
	</tbody>
</table>
<h1>{"Add new ..."|gettext}</h1>
<a href="Javascript:multisettingNew('optioncollection');">New</a>