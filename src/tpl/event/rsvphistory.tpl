<h2>{"History"|gettext}</h2>
<p>{"This list contains changes in the number of registered participants."|gettext}</p>
{if count($rsvphistoryrecords) == 0}
{"No guests' bookings available."|gettext}
{else}
<div id="rsvphistorytable">
<table class="table table-striped">
<thead>
<tr>
<th>{"Time"|gettext}</th>
<th>{"Salutation"|gettext}</th>
<th>{"First Name"|gettext}</th>
<th>{"Last Name"|gettext}</th>
<th>{"Change"|gettext}</th>
<th>{"Registered"|gettext}</th>
</tr>
</thead>
<tbody>
	{foreach from=$rsvphistoryrecords item=hr}
	{if $hr.diff_registered <> 0}
	<tr>
		<td>{$hr.changetime}</td>
		<td>{$hr.salutation}</td>
		<td>{$hr.firstname}</td>
		<td>{$hr.lastname}</td>
		<td class="right">{$hr.diff_registered}</td>
		<td class="right">{$hr.registered}</td>
	</tr>
	{/if}
	{/foreach}
</tbody>
</table>
</div>
{/if}