<form action="" class="form-horizontal">
<h2></h2>
	{if $domainchangesuccessful == true}
	<div class="alert alert-success">{"Saved!"|gettext}</div>
	{/if}
	<input type="hidden" name="class" value="domain" />
	<input type="hidden" name="cb" value="closeDomainForm" />
	<input type="hidden" name="fevent_id" value="{$fevent_id}" />
	<input type="hidden" name="domain_id" value="{$domain_id}" />
	<input type="hidden" name="form" value="changeDomainForm" />
	<!-- <div class="control-group">
		<label class="control-label" for="domaintype">{"Domain Type"|gettext}</label>
		<div class="controls">
			<select class="input-block-level"  name="domaintype">{html_options values=$dtval output=$dtout selected=$ps.domaintype}</select>
			<span class="help-block">{"Type of the domain"|gettext}</span>
		</div>
	</div>-->
	<!-- 
	<div class="control-group">
		<label class="control-label" for="domainname">{"Domain Name"|gettext}</label>
		<div class="controls">
			<input name="domainname" type="text" class="input-block-level" placeholder="{"www.mydomain.com"|gettext}" value="{$ps.domainname}" />
			<span class="help-block"></span>
		</div>
	</div>
	-->
	<input type="hidden" name="domaintype" value="0" /> 
	
	<div class="control-group">
		<label class="control-label" for="urlpath">{"URI Path"|gettext}</label>
		<div class="controls">
			http://{html_options name=domainname values=$stdop output=$stdop selected=$ps.domainname}/<input name="urlpath" type="text" class="input-block-level" placeholder="{"myevent"|gettext}" value="{$ps.urlpath}" />
			<span class="help-block"></span>
		</div>
	</div>
	<input type="submit" value="{"Save"|gettext}" class="ui-btn ui-corner-all" />
</form>
