<h3>{"Check-In Form Fields"|gettext} {$seqid}</h3>
<form action="" name="changemultisettingsform" class="changemultisettingsform verticalform apiform" method="post" data-url="/events/{$fevent_id}/multisettings">
	<input type="hidden" name="class" value="event" />
	<input type="hidden" name="cb" value="closeMultisettingsForm;reloadMultisettingsTables" />
	<input type="hidden" name="fevent_id" value="{$fevent_id}" />
	<input type="hidden" name="multisetting" value="{$multisettingtype}" />
	<input type="hidden" name="seqid" value="{$seqid}" />
	<input type="hidden" name="form" value="changemultisettingform" />

	<label for="id">{"Id"|gettext}</label>
	<input name="id" type="text" value="{$ps.id}" />
	
	<label for="formsectionid">{"Form Section"|gettext}</label>
	<select name="formsectionid">{html_options options=$options_checkinformfield_formsectionid selected=$ps.formsectionid}</select>
		
	<label for="type">{"Type"|gettext}</label>
	<select name="type">{html_options options=$options_checkinformfield_type selected=$ps.type}</select>
	
	<label for="fieldname">{"Field Name"|gettext}</label>
	<input name="fieldname" type="text" value="{$ps.fieldname}"/>

	<label for="requiredcondition">{"Required Condition"|gettext}</label>
	<textarea name="requiredcondition" type="text" placeholder="">{$ps.requiredcondition}</textarea>
	
	
	<hr />
	<h3>{"For a room set choice only:"|gettext}</h3>
	<label for="roomsetid">{"Room Set"|gettext}</label>
	<select name="roomsetid">{html_options options=$dropdown_checkinformfield_roomsetid selected=$ps.roomsetid}</select>
	
	<label for="minimumchoices">{"Minimum number of choices required"|gettext}</label>
	<input name="minimumchoices" type="text" value="{$ps.minimumchoices}"/>
	
	<label for="maximumchoices">{"Maximum number of allowed choices"|gettext}</label>
	<input name="maximumchoices" type="text" value="{$ps.maximumchoices}"/>
	
	
	<hr />
	<h3>{"For a Display Field only:"|gettext}</h3>
	<label for="whattoshow">{"Display"|gettext}</label>
	<select name="whattoshow">{html_options options=$options_checkinformfield_whattoshow selected=$ps.whattoshow}</select>
	
	<label for="customtext">{"Custom text"|gettext}</label>
	<input name="customtext" type="text" value="{$ps.customtext}"/>
	
	<label for="color">{"Color"|gettext}</label>
	<input name="color" type="text" value="{$ps.color}"/>
	<label for="backgroundcolor">{"Background color"|gettext}</label>
	<input name="backgroundcolor" type="text" value="{$ps.backgroundcolor}"/>
	
	<hr />
	<h3>{"For an input field only:"|gettext}</h3>
	<label for="filledbyscanner">
		<input name="filledbyscanner" type="checkbox" value="1" {if $ps.filledbyscanner == 1}checked{/if} />
		{"Filled by scanner"|gettext}
	</label>
	
	<hr />
	<input type="submit" value="{" Save and close"|gettext}" name="submit" class="ui-btn ui-corner-all" />
</form>