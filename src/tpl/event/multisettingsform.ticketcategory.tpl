<h3>{"Ticket Category"|gettext} {$seqid}</h3>
<form action="" name="changemultisettingsform" class="changemultisettingsform verticalform apiform" method="post" data-url="/events/{$fevent_id}/multisettings">
	<input type="hidden" name="class" value="event" />
	<input type="hidden" name="cb" value="closeMultisettingsForm;reloadMultisettingsTables" />
	<input type="hidden" name="fevent_id" value="{$fevent_id}" />
	<input type="hidden" name="multisetting" value="{$multisettingtype}" />
	<input type="hidden" name="seqid" value="{$seqid}" />
	<input type="hidden" name="form" value="changemultisettingform" />

	<label for="title">{"Title"|gettext}</label>
	<input name="title" type="text" placeholder="{"VIP Ticket"|gettext}" value="{$ps.title}" />

	<label for="desc">{"Description"|gettext}</label>
	<textarea name="desc" placeholder="{"... with backstage tour and star dinner"|gettext}">{$ps.desc}</textarea>

	<label for="invoicetext">{"Invoice text"|gettext}</label>
	<textarea name="invoicetext" placeholder="{"VIP Ticket with backstage tour and star dinner"|gettext}">{$ps.invoicetext}</textarea>

	<label for="ticketcategorycode">{"Ticketcode"|gettext}</label>
	<input name="ticketcategorycode" type="text" placeholder="{"FXVIP"|gettext}" value="{$ps.ticketcategorycode}" />

	<label for="price">{"Price"|gettext}</label>
	<input name="price" type="text" placeholder="{"1389.00"|gettext}" value="{$ps.price}" />
	
	<label for="vatamount">{"VAT amount"|gettext}</label>
	<input name="vatamount" type="text" placeholder="{"221.77"|gettext}" value="{$ps.vatamount}" />
	
	<label for="vatrate">{"VAT Rate"|gettext}</label>
	<input name="vatrate" type="text" placeholder="{"19.00"|gettext}" value="{$ps.vatrate}" />%
	
	<label for="pricenet">{"Net Price"|gettext}</label>
	<input name="pricenet" type="text" placeholder="{"1167.23"|gettext}" value="{$ps.pricenet}" />
	
	<label for="availablefrom">{"Available from"|gettext}</label>
	<input name="availablefrom" type="text" placeholder="{"14.03.2018 14:00:00"|gettext}" value="{$ps.availablefrom}" />
	
	<label for="availableuntil">{"Available until"|gettext}</label>
	<input name="availableuntil" type="text" placeholder="{"14.03.2018 18:00:00"|gettext}" value="{$ps.availableuntil}" />
	
	<input type="submit" value="{" Save and close"|gettext}" name="submit" class="ui-btn ui-corner-all" />
</form>