<h3>{"Automatic conditional emails"|gettext}</h3>

<table class="multisetting" data-multisetting="conditionalautomail">
	<thead>
		<tr>
			<th data-col="seqid">{"Id"|gettext}</th>
			<th data-col="id">{"Id"|gettext}</th>
			<th data-col="emailtemplateid">{"Template Id"|gettext}</th>
			<th data-col="emailcondition">{"Condition"|gettext}</th>
			<th data-col="emailtrigger">{"Trigger"|gettext}</th>
			<th>{"Options"|gettext}</th>
		</tr>
	</thead>
	<tbody>
		{if count($ps.autoconditionemail) == 0}
		<tr>
			<td colspan="6">---</td>
		</tr>
		{else} {foreach from=$ps.autoconditionalemail item=cs}
		<tr>
			<td>{$cs.id}</td>
			<td>{$cs.title}</td>
		</tr>
		{/foreach} {/if}
	</tbody>
</table>
<h1>{"Add new ..."|gettext}</h1>
<a href="Javascript:multisettingNew('conditionalautomail');">New</a>