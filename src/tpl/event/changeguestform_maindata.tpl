		<form action="" name="changeguestform" class="guestform apiform" method="post" data-url="/events/{$fevent_id}/guests/{$guest_id}">
		
			{if $guestchangesuccessful == true}
				<div class="alert alert-success">{"Saved!"|gettext}</div>
			{/if}
			<input type="hidden" name="class" value="guest" />
			<input type="hidden" name="cb" value="saveclose:closeGuestForm;refreshEventStats|save:reloadGuestForm" />
			<input type="hidden" name="fevent_id" value="{$fevent_id}" />
			<input type="hidden" name="guest_id" value="{$guest_id}" />
			<input type="hidden" name="form" value="changeGuestForm" />

			<div class="lcol">
				<h2>{"Main fields"|gettext}</h2>
				<label for="guestformsalutation">{"Salutation"|gettext}</label>
				<input name="salutation" id="guestformsalutation" type="text" placeholder="{"Salutation"|gettext}" value="{$ps.salutation}" />
				
				<label for="guestformfirstname">{"First Name"|gettext}</label>
				<input name="firstname" id="guestformfirstname" type="text" placeholder="{"First Name"|gettext}" value="{$ps.firstname}" />
				
				<label class="control-label" for="lastname">{"Last Name"|gettext}</label>
				<input name="lastname" type="text" class="input-block-level" placeholder="{"Last Name"|gettext}" value="{$ps.lastname}" />
				
				<div class="control-group">
					<label class="control-label" for="company">{"Company"|gettext}</label>
					<div class="controls">
						<input name="company" type="text" class="input-block-level" placeholder="{"Company"|gettext}" value="{$ps.company}" />
					</div>
				</div>
				<div class="control-group">
					<label class="control-label" for="note">{"Comment"|gettext}</label>
					<div class="controls">
						<input name="note" type="text" class="input-block-level" placeholder="{"Comment"|gettext}" value="{$ps.note}" />
					</div>
				</div>

				<div class="control-group">
					<label class="control-label" for="invited">{"Invited"|gettext}</label>
					<div class="controls">
						<input name="invited" type="text" class="input-block-level" placeholder="{"Invited"|gettext}" value="{$ps.invited}" />
						<span class="help-block">{"Number of persons invited"|gettext}</span>
					</div>
				</div>
				
				<div class="control-group">
					<label class="control-label" for="cancelled">{"Cancelled"|gettext}</label>
					<div class="controls">
						<input name="cancelled" type="text" class="input-block-level" placeholder="{"Cancelled"|gettext}" value="{$ps.cancelled}" />
						<span class="help-block">{"Number of persons who refused to attend"|gettext}</span>
					</div>
				</div>
				
				<div class="control-group">
					<label class="control-label" for="registered">{"Registered"|gettext}</label>
					<div class="controls">
						<input name="registered" type="text" class="input-block-level" placeholder="{"Registered"|gettext}" value="{$ps.registered}" />
						<span class="help-block">{"Number of persons who confirmed to attend"|gettext}</span>
					</div>
				</div>
				
				<div class="control-group">
					<label class="control-label" for="accesscode">{"Access Code"|gettext}</label>
					<div class="controls">
						<input name="accesscode" type="text" class="input-block-level" placeholder="{"Access Code"|gettext}" value="{$ps.accesscode}" />
						<span class="help-block">{"The guest can use this code to register or cancel the event."|gettext}</span>
					</div>
				</div>
				
				<div class="control-group">
					<label class="control-label" for="emailaddress">{"Email address"|gettext}</label>
					<div class="controls">
						<input name="emailaddress" type="text" class="input-block-level" placeholder="{"max.mustermann@email.de"|gettext}" value="{$ps.emailaddress}" />
						<span class="help-block">{"Email address of the guest, used for automatic messages and mass mails."|gettext}</span>
					</div>
				</div>
			</div>
	
			<div class="rcol">
				{if strlen($customFields) > 0}
					<h2>{"Custom Fields"|gettext} </h2>
					{$customFields}
				{/if}
			</div>
			<div class="clear"><br /></div>
			
			<input type="submit" value="{"Save and close"|gettext}" name="btn_saveclose" />
			<div>
				<input type="submit" value="{"Save"|gettext}" name="btn_save" />
			</div>
		</form>