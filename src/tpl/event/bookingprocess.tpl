<h3>{"Booking Process"|gettext}</h3>
<div id="steplistforevent">
{include file="event/steplistforevent.tpl"}
</div>

<form action="" method="put" class="noduplicatesubmissions apiform" name="regprocstepaddform" id="regprocstepaddform" data-url="/events/#/regprocsteps">
<h2>{"Add process step"|gettext}</h2>
	<div class="alert alert-success alert-hidden">{"The step has been added."|gettext}</div>
	<input type="hidden" name="class" value="event" />
	<input type="hidden" name="cb" value="reloadRegProcStepTable" />
	<input type="hidden" name="fevent_id" class="valEvfevent_id" value="{$fevent_id}" />
	<input type="hidden" name="form" value="addRegProcStep" />
	<input type="hidden" name="resultdiv" value="#steplistforevent" />
	
	<label for="description">{"Description"|gettext}</label>
	<input name="description" placeholder="{"type your description here"|gettext}" />
	<select name="extension_id">
		{html_options options=$availableextensions}
	</select>
	<input type="submit" value="{"add"|gettext}" />
</form>