<h2>Email template #<span class="emt_emailtemplate_id"></span> (<span class="emt_title"></span>)</h2>
<form action="" method="PUT" class="emailtemplateform apiform" data-url="/emailtemplates/#">
	<input type="hidden" name="emailtemplate_id" value="" />
	<label for="title">{"Template Title"|gettext}</label>
	<input name="title" value="" placeholder="{"Ticket delivery email"|gettext}" />
	<label for="description">{"Description"|gettext}</label>
	<textarea name="description"></textarea>
	
	<label for="emailsubject">{"Subject"|gettext}</label>
	<input name="emailsubject" value="" placeholder="{"Your ticket arrived!"|gettext}" />
	<label for="content">{"Template Code"|gettext}</label>
	<textarea name="content" class="bightml"></textarea>
	<br />
	<input type="submit" value="{"Save"|gettext}" />
</form>