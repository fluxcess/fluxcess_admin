{if $crmenabled == "0"}
	<form action="" method="put" class="noduplicatesubmissions apiform" name="crmenableform" id="crmenableform" data-url="/crms/enableCRM">
		<div class="alert alert-success alert-hidden">{"The CRM module has been enabled."|gettext}</div>
		<input type="hidden" name="class" value="crms" />
		<input type="hidden" name="form" value="enablecrm" />
		<input type="submit" value="{"Enable CRM Module"|gettext}" />
		<input type="hidden" name="cb" value="reloadPage" />
	</form>
{else}
	<form action="" method="put" class="noduplicatesubmissions apiform" name="crmdisableform" id="crmdisableform" data-url="/crms/disableCRM">
		<div class="alert alert-success alert-hidden">{"The CRM module has been disabled."|gettext}</div>
		<input type="hidden" name="class" value="crms" />
		<input type="hidden" name="form" value="disablecrm" />
		<input type="hidden" name="cb" value="reloadPage" />
		<input type="submit" value="{"Disable CRM Module"|gettext}" />
	</form>

<h2>{"Guest list"|gettext}</h2>

<div class="right">
	<select data-inline="true" name="crmtableshowcount" id="crmtableshowcount" onchange="changecrmtableshowcount();">
		<option value="10">10</option>
		<option value="-1">all</option>
	</select>
	<a class="ui-btn ui-shadow ui-corner-all ui-btn-icon-notext ui-icon-plus ui-btn-inline" onclick="editCRMPerson(0);" title="{"Add person"|gettext}" name="butAddPerson"><img src="/pic/icons/add.svg" title="{"Add person"|gettext}" alt="{"Add person"|gettext}" /></a>
	<a class="ui-btn ui-shadow ui-corner-all ui-btn-icon-notext ui-icon-refresh ui-btn-inline" onclick="reloadCRMTable();" title="{"Reload table"|gettext}"><img src="/pic/icons/refresh.svg" title="{"Reload table"|gettext}" alt="{"Reload table"|gettext}" /></a>
	<a class="ui-btn ui-shadow ui-corner-all ui-btn-icon-notext ui-icon-arrow-l ui-btn-inline" onclick="showCRMTablePreviousPage();" title="{"Previous page"|gettext}"><img src="/pic/icons/previouspage.svg" title="{"Previous page"|gettext}" alt="{"Previous page"|gettext}" /></a>
	<a class="ui-btn ui-shadow ui-corner-all ui-btn-icon-notext ui-icon-arrow-r ui-btn-inline" onclick="showCRMTableNextPage();" title="{"Next page"|gettext}"><img src="/pic/icons/nextpage.svg" title="{"Next page"|gettext}" alt="{"Next page"|gettext}" /></a>
	<div style="width: 100px; display: inline; float: left;"><input type="search" id="gsearch" name="se" data-inline="true" /></div>
</div>	
<table class="crmtable table table-striped table-bordered datatable">
	<thead>
		{foreach from=$crmcols item=ccol}
			<th>
				{$ccol.title}
			</th>
		{/foreach}
	</thead>
	<tbody>
		{foreach from=$crmdata item=cdat}
			<tr>
				{foreach from=$crmcols item=ccol}
					<td>
						{$cdat[$ccol.name]}
					</td>
				{/foreach}
			</tr>
		{/foreach}
	</tbody>
</table>
{/if}
<div class="right">
	<form action="" method="post" class="formMoveCRMToEvent specialForm noduplicatesubmissions apiform" data-url="/crms/moveRecordsToEvent">
		<div class="alert alert-success alert-hidden">{"The records have been linked to the event."|gettext}</div>
		<input type="hidden" name="class" value="crm" />
		<input type="hidden" name="form" value="formMoveCRMToEvent" />
		{"Link selected persons to"|gettext}: <select name="selectevent">
	        		{$eventlistMenu}
	    		</select>
	    <input type="submit" value="Go" />
	</form>
	<a class="ui-btn ui-shadow ui-corner-all ui-btn-icon-notext ui-icon-plus ui-btn-inline" onclick="editCRMPerson(0);" title="{"Add person"|gettext}" name="butAddPerson"><img src="/pic/icons/add.svg" title="{"Add person"|gettext}" alt="{"Add person"|gettext}" /></a>
	<a class="ui-btn ui-shadow ui-corner-all ui-btn-icon-notext ui-icon-refresh ui-btn-inline" onclick="reloadCRMTable();" title="{"Reload table"|gettext}"><img src="/pic/icons/refresh.svg" title="{"Reload table"|gettext}" alt="{"Reload table"|gettext}" /></a>
	<a class="ui-btn ui-shadow ui-corner-all ui-btn-icon-notext ui-icon-arrow-l ui-btn-inline" onclick="showCRMTablePreviousPage();" title="{"Previous page"|gettext}"><img src="/pic/icons/previouspage.svg" title="{"Previous page"|gettext}" alt="{"Previous page"|gettext}" /></a>
	<a class="ui-btn ui-shadow ui-corner-all ui-btn-icon-notext ui-icon-arrow-r ui-btn-inline" onclick="showCRMTableNextPage();" title="{"Next page"|gettext}"><img src="/pic/icons/nextpage.svg" title="{"Next page"|gettext}" alt="{"Next page"|gettext}" /></a>
</div>	