<div class="hero-unit" id="crmtitle">
	<h1>CRM</h1>
</div>

<nav class="mmenu">
	<ul>
		<li class="active"><a class="linkCRM" href="#crm/persons">{"Persons"|gettext}</a></li>
		<li><a class="linkCRM" href="#crm/settings">{"Settings"|gettext}</a></li>
	</ul>
</nav>
<div class="clear"></div>



<div class="subpage persons">{include file="crm/personspage.tpl"}</div>
<div class="subpage settings hidden">{include file="crm/settingspage.tpl"}</div>
<div class="halfsubpage hidden">:-)<br />:-)</div>
<div class="clear"></div>


<div id="popupCRM" class="popup">
	<div class="closepopup"><a href="javascript:closePopup();">close</a></div>
	<div id="popupPanelCRM" class="panel">
		:-)
	</div>
</div>