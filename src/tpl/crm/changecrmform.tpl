<h3>{if $crm_id == 0}{"Add Person"|gettext}{else}{"Person"|gettext} #{$crm_id}{/if}</h3>
<form action="" name="changecrmform" class="crmform">
	{if $crmchangesuccessful == true}
	<div class="alert alert-success">{"Saved!"|gettext}</div>
	{/if}
	<input type="hidden" name="class" value="crm" />
	<input type="hidden" name="cb" value="closeCRMForm" />
	<input type="hidden" name="crm_id" value="{$crm_id}" />
	<input type="hidden" name="form" value="changeCRMForm" />

	<label for="guestformsalutation">{"Salutation"|gettext}</label>
	<input name="salutation" id="guestformsalutation" type="text" placeholder="{"Salutation"|gettext}" value="{$ps.salutation}" />
	
	<label for="guestformfirstname">{"First Name"|gettext}</label>
	<input name="firstname" id="guestformfirstname" type="text" placeholder="{"First Name"|gettext}" value="{$ps.firstname}" />

	<label class="control-label" for="lastname">{"Last Name"|gettext}</label>
	<input name="lastname" type="text" class="input-block-level" placeholder="{"Last Name"|gettext}" value="{$ps.lastname}" />

	{if strlen($customFields) > 0}
	<hr /> {"Custom Fields"|gettext} <hr />
	{$customFields}
	{/if}
	<input type="submit" value="{"Save"|gettext}" name="submit" class="ui-btn ui-corner-all" />
</form>
