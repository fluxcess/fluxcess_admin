#!/usr/bin/php
#!c:/apache24/php64/php.exe
#!/cygdrive/C/Apache24/PHP64/php.exe
<?php
/**
 * executes all sql updates or add tables initially
 *
 * @author Line5
 */
$dir = __dir__;
echo 'sqlupdate script in directory: ' . $dir . "\n";
if (strstr(__dir__, 'cygdrive')) {
	$dir = 'C:/Apache24/htdocs/gaestelisteadmin';
}

try {
	$localconf = $dir . '/../conf/config.local.php';
	echo "including local configuration from $localconf ...\n";
	if (is_file($localconf)) {
		echo "found!\n";
		include_once ($localconf);
	} else {
		echo "not found.\n";
	}
	echo "including global configuration ...\n";
	$globalconf = $dir . '/../conf/config.php';
	if (is_file($globalconf)) {
		echo "found!\n";
		include_once ($globalconf);
	} else {
		die("$globalconf not found.\n");
	}
} catch ( IOException $e ) {
	print_r($e);
}
echo "starting update ...\n";
try {
	// Update database scheme, up to the latest version
	if (tablesExist() !== true) {
		include_once ($dir . '/dbcreate/dbcreate.php');
	}
	if (tablesExist() === true) {
		$done = false;
		$lastUpdateTryFrom = 0;
		while ( $done == false ) {
			$currentDbVersion = getDbVersion();
			if ($lastUpdateTryFrom == $currentDbVersion) {
				$done = true;
			} else {
				$lastUpdateTryFrom = $currentDbVersion;
				$handle = opendir($dir . '/dbupdate');
				while ( false !== ($file = readdir($handle)) ) {
					if ($file != "." && $file != "..") {
						include ($dir . '/dbupdate/' . $file);
					}
				}
				closedir($handle);
			}
		}
	}
	$newDbVersion = getDbVersion();
} catch ( Exception $e ) {
	echo 'Datenbank-Fehler: ' . print_r($e->getMessage(), true);
}

/**
 * returns the current version of the database
 *
 * @return Ambigous <NULL, unknown>
 */
function getDbVersion() {
	$v = null;
	$sql = "SELECT sval FROM globsettings WHERE skey = 'dbversion';";
	$_pdoObj = dbconnection::getInstance();
	$pdoStatement = $_pdoObj->prepare($sql, array (
			PDO::ATTR_CURSOR => PDO::CURSOR_FWDONLY 
	));
	$pdoStatement->execute(Array ());
	if ($pdoStatement->rowCount() == 1) {
		$row = $pdoStatement->fetch();
		$v = $row['sval'];
	}
	if (is_numeric($v)) {
		echo 'Current database version: ' . $v . "\n";
	} else {
		echo 'Could not determine db version.' . "\n";
	}
	return $v;
}
function tablesExist() {
	$tableExists = false;
	$_pdoObj = dbconnection::getInstance();
	$rcount = $_pdoObj->query("SHOW TABLES LIKE 'globsettings'")->rowCount();
	if ($rcount > 0) {
		$tableExists = true;
	}
	return $tableExists;
}

/**
 * updates the database version counter in the database
 *
 * @param int $target
 *        	contains the new value for the version counter
 */
function updateDbVersion($target) {
	$sql = "UPDATE globsettings SET sval = :vers WHERE skey = 'dbversion';";
	$_pdoObj = dbconnection::getInstance();
	$pdoStatement = $_pdoObj->prepare($sql, array (
			PDO::ATTR_CURSOR => PDO::CURSOR_FWDONLY 
	));
	$pdoStatement->execute(Array (
			':vers' => $target 
	));
}

function sqlUpGenerateUniqueKey($length) {
	$k = '';
	$chars = 'ABCDEF1234567890';
	for($i = 0; $i < $length; $i++) {
		$k .= substr($chars, rand(0, strlen($chars)), 1);
	}
	return $k;
}
