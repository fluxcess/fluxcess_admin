#!/usr/bin/php
<?php
/**
  * THIS FILE IS AN EXAMPLE OF HOW TO CREATE
  * ONE BIG PHP FILE FROM ALL TEMPLATES IN THE 
  * TEMPLATE DIRECTORY
  * THE CREATED PHP FILE CAN BE USED WITH with e.g. xgettext (see comment in step DONE.)
  *
  */

$docRoot = getenv('DOCUMENT_ROOT');
if ($docRoot == '') {
	$docRoot = __DIR__;
}
if (substr($docRoot, -1) != '/') {
	$docRoot .= '/';
}
echo $docRoot."\n";
include_once($docRoot.'../conf/config.local.php');
include_once($docRoot.'../conf/config.php');
/*
if (strstr($docRoot, '/var/www/u2365gaesteadmt/') !== false) {
	echo '***** TESTING ENVIRONMENT.';
	define('TESTING', true);
} elseif (strstr($docRoot, '/var/www/u2365gaesteadm/') !== false) {
	echo '***** PRODUCTION ENVIRONMENT.';
	define('TESTING', false);
} else {
	die('+ - Path not found');
}


//DEFINE('BASE_PATH', __DIR__.DIRECTORY_SEPARATOR);
if (TESTING == true) {	
DEFINE('BASE_PATH', '/var/www/u2365gaesteadmt/home/u2365gaesteadmt/admintest.xn--gsteliste-v2a.org/');
define('BASEDIR', '/var/www/u2365gaesteadmt/home/u2365gaesteadmt/admintest.xn--gsteliste-v2a.org/');
} else {
DEFINE('BASE_PATH', '/var/www/u2365gaesteadm/home/u2365gaesteadm/admin.xn--gsteliste-v2a.org/');
define('BASEDIR', '/var/www/u2365gaesteadm/home/u2365gaesteadm/admin.xn--gsteliste-v2a.org/');
}
*/
include realpath(BASEDIR.'lib/Smarty/plugins/mebb_functions_glob_recursive.php');
include realpath(BASEDIR.'lib/Smarty/plugins/mebb_i18n_smarty.php');

if(file_exists(BASEDIR.'../../../../../.mebb')){//this is just for the our framework. just ignore this. the else block is relevant for you :)
  include_once BASEDIR.'../../../../smarty/Smarty.class.php';
  include_once BASEDIR.'../../../../../app/core/web/smarty/functions/locale.php';
}else{
  //print 'PLEASE INCLUDE YOUR SMARTY CLASS IN FILE '.__FILE__.' line '.__LINE__.' AND REMOVE THE exit() STATEMENT IN LINE '.(__LINE__ + 2).PHP_EOL;
  include_once(BASEDIR.'lib/Smarty/Smarty.class.php');
  include realpath(BASEDIR.'lib/Smarty/plugins/mebb_i18n_smarty_function_locale.php');
  //exit();
}

define('MEBB_IGNORE_ERRORS', true);
define('MEBB_TEMPLATE_EXTENSION', 'tpl');

//1.) SETUP SMARTY AS YOU USUALLY DO, i.e. DON'T JUST COPY THE BELOW, UNLESS IT FITS YOUR NEEDS
//    LOAD ALL THE MODIFIERS, FILTER, ETC. THAT YOU USE IN YOUR TEMPLATES, OTHERWISE, YOU'LL
//    ENCOUNTER A LOT OF COMPILE ERRORS :D
$smarty = new \Smarty();
$smarty->setTemplateDir(BASEDIR.'tpl');
$smarty->setCompileDir(BASEDIR.'tmp/compile');
$smarty->setCacheDir(BASEDIR.'tmp/compile');
$smarty->setConfigDir(BASEDIR.'tmp/compile');

//2.) INTEGRATE THE CUSTOM LOCALES FUNCTION
$smarty->registerPlugin('function', 'locale', '\\mebb\\app\\core\\web\\smarty\\functions\\locale');

//3.) we compile all the templates in the template directory
//    you can also set a custom directory/subdirectory and only consider files therein,
//    if you like to create multiple po/mo files
$info = array();//the passback array for error definitions (in case there are any)
$sources = \mebb\lib\i18n\smarty\compile($smarty, null, $info);

//4.) we're saving the files to the temporary directory. We've chosen to save them individually
//    because the po/mo files will at least contain an indication of the origin for the
//    message IDs, even if the line # will not be correct and the file-name will
//    be re-formatted; but hey, that's as good it gets. Feedback, ideas, suggestions, etc. 
//    more than welcome
$directory = \mebb\lib\i18n\smarty\save_individual($smarty, $sources, BASEDIR.'tmp/');

//DONE. The rest ist just for informational and playful purposes :
//You can no go to the directory and use any program to extract the message-IDs 
//If you are using xgettext, use the following command
// > cd /my/directory/with/translation/
// > xgettext -n *.tpl --language=PHP 

print 'The following templates have been compiled into '.$directory.':'.PHP_EOL;
foreach($sources as $source){
  print '  - '.$source['file_original'].PHP_EOL;
}

if(count($info['errors'])>0){
  print PHP_EOL.PHP_EOL.'The following errors have occured:'.PHP_EOL;
  foreach($info['errors'] as $error){
    $exception = $error['exception'];
    $file = $error['file'];
    print $file.' has the following error: '.$error['message'].PHP_EOL; 
  }
}

//exec("cd ".$directory);
echo $directory."\n";
$directory2 = BASEDIR."inc/";
echo $directory2."\n";
$directory3 = BASEDIR."conf/";
$cmd = 'xgettext --language=PHP --from-code=UTF-8 '.$directory.'*.tpl '.$directory2.'*.php '.$directory3.'*.php';
echo $cmd."\n";
exec($cmd);

// for new languages, do this once:
//$newlang = 'en_US.UTF-8';
//exec ('mkdir '.BASEDIR.'locale/'.$newlang.'/LC_MESSAGES -R');
//exec ('cp messages.po '.BASEDIR.'locale/'.$newlang.'/LC_MESSAGES/messages.po');

// language-specific merge & copy
//$langs = array('de_DE.UTF-8', 'en_US.UTF-8');
$langs = array('de_DE.UTF-8', 'en_US.UTF-8');
foreach ($langs as $lang) {
$cmd = 'msgmerge -N --no-wrap -o results.po '.BASEDIR.'locale/'.$lang.'/LC_MESSAGES/messages.po messages.po';
exec($cmd);
$cmd = 'mv results.po '.BASEDIR.'locale/'.$lang.'/LC_MESSAGES/messages.po';
exec($cmd);
}

$cmd = 'rm '.$directory.' -R';
exec($cmd);

?>
