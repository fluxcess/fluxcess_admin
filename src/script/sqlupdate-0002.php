#!/usr/bin/php
<?php
/**
 * Adds columns "lastselflogin" and "selfloginsfailed" to all zguest tables
 * 03.07.2013
 */

/**
 * not sure what goes here...
 */

include_once('../conf/config.php');


try {
	// get all existing event tables
	$sql = "SELECT fevent_id FROM fevent;";
	$_pdoObj = dbconnection::getInstance();
	$pdoStatement = $_pdoObj->prepare($sql, array(PDO::ATTR_CURSOR => PDO::CURSOR_FWDONLY));
	$pdoStatement->execute(Array());
	if ($pdoStatement->errorCode() != 0) {
		$this->addError('', 'SQL Fehler'.print_r($pdoStatement->errorInfo(), true), 1);
	} else {
		while ($row = $pdoStatement->fetch()) {
			echo 'Found event: '.$row['fevent_id']."\n";
			$sql = "ALTER TABLE `zguest".$row['fevent_id']."` ADD `lastselflogin` TIMESTAMP NULL DEFAULT NULL AFTER `accesscode` ,
ADD `selfloginsfailed` INT NOT NULL DEFAULT '0' AFTER `lastselflogin`;";
			$pdoStatement2 = $_pdoObj->prepare($sql, array(PDO::ATTR_CURSOR => PDO::CURSOR_FWDONLY));
			$pdoStatement2->execute(Array());
		}		
	}
} catch (Exception $e) {
	$this->addError('', 'Datenbank-Fehler', 1, print_r($e->getMessage(), true));
}

