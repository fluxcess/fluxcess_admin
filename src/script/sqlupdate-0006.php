#!/usr/bin/php
<?php
/**
 * Adds new table zcolumn<fevent_id> for each event
 * 07.07.2013
 */

/**
 * 
 */
include_once('../conf/config.php');


try {
	// get all existing event tables
	$sql = "SELECT fevent_id FROM fevent;";
	$_pdoObj = dbconnection::getInstance();
	$pdoStatement = $_pdoObj->prepare($sql, array(PDO::ATTR_CURSOR => PDO::CURSOR_FWDONLY));
	$pdoStatement->execute(Array());
	if ($pdoStatement->errorCode() != 0) {
		$this->addError('', 'SQL Fehler'.print_r($pdoStatement->errorInfo(), true), 1);
	} else {
		while ($row = $pdoStatement->fetch()) {
			echo 'Found event: '.$row['fevent_id']."\n";
			
			$sql = "ALTER TABLE `zcolumn".$row['fevent_id']."` 
					CHANGE `fieldname` `fieldname` VARCHAR( 64 ) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL
					;";
			$pdoStatement2 = $_pdoObj->prepare($sql, array(PDO::ATTR_CURSOR => PDO::CURSOR_FWDONLY));
			$pdoStatement2->execute(Array());
			
			$sql = "ALTER TABLE `zcolumn".$row['fevent_id']."`
					CHANGE `fielddefault` `fielddefault` VARCHAR( 64 ) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL
					;";
			$pdoStatement2 = $_pdoObj->prepare($sql, array(PDO::ATTR_CURSOR => PDO::CURSOR_FWDONLY));
			$pdoStatement2->execute(Array());
			
			$sql = "ALTER TABLE `zcolumn".$row['fevent_id']."`
					CHANGE `fieldtype` `fieldtype` VARCHAR( 64 ) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL
					;";
			$pdoStatement2 = $_pdoObj->prepare($sql, array(PDO::ATTR_CURSOR => PDO::CURSOR_FWDONLY));
			$pdoStatement2->execute(Array());
				
			$sql = "TRUNCATE TABLE `zcolumn".$row['fevent_id']."`;";
			$pdoStatement2 = $_pdoObj->prepare($sql, array(PDO::ATTR_CURSOR => PDO::CURSOR_FWDONLY));
			$pdoStatement2->execute(Array());
			
				
		}
	}
} catch (Exception $e) {
	$this->addError('', 'Datenbank-Fehler', 1, print_r($e->getMessage(), true));
}

