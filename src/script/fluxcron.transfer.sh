#!/bin/bash

# ensure the script runs only once:
[[ "$(pidof -x $(basename $0))" != $$ ]] && echo "Transfer script running already." && exit

# start the fluxcess cron job:
/usr/bin/php fluxcron.transfer.php
sleep 10
/usr/bin/php fluxcron.transfer.php
sleep 10
/usr/bin/php fluxcron.transfer.php
sleep 10
/usr/bin/php fluxcron.transfer.php
sleep 10
/usr/bin/php fluxcron.transfer.php