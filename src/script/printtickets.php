#!/usr/bin/php
#!c:/apache24/php64/php.exe
#!/cygdrive/C/Apache24/PHP64/php.exe
<?php
/**
 * prints tickets
 * 03.09.2014
 */

$dir = __dir__;
echo 'DIR: '.$dir;
if (strstr(__dir__, 'cygdrive')) {
	$dir = 'C:/Apache24/htdocs/gaestelisteadmin';
}

include_once ($dir.'/../conf/config.local.php');
include_once($dir.'/../conf/config.php');
session_start();
if (!is_numeric($argv[1]) || !is_numeric($argv[2]) || !is_numeric($argv[3])) {
	die("\n".'syntax: printtickets.php eventid guestfrom guestcount [printerid]'."\n");
}
$eventid = $argv[1];
$_SESSION['event']['id'] = $eventid;
$guestFrom = $argv[2];
$guestCount = $argv[3];
if (!isset($argv[4])) {
	$printerId = 1;
} else {
	$printerId = $argv[4];
}


echo "Hi - this is the printing job for event ".$_SESSION['event']['id'].". I will print on printer ".$printerId.".\n";

$sql = "SELECT guest_id FROM zguest".$eventid." 
		WHERE deleted = 0 AND archived = 0
		ORDER BY lastname DESC LIMIT ".$guestFrom.", ".$guestCount.";";
$_SESSION['user']['id'] = 1;
$_pdoObj = dbconnection::getInstance();
$pdoStatement = $_pdoObj->prepare($sql, array(PDO::ATTR_CURSOR => PDO::CURSOR_FWDONLY));
$pdoStatement->execute(Array());
$iCounter = 0;
if ($pdoStatement->rowCount() > 1) {
	while ($row = $pdoStatement->fetch()) {
		$i += 1;
		echo "Print ticket #".$i." (".($i+$guestFrom)."): ".$row['guest_id']."\n";
		$ev = new event();
		$gt['fevent_id'] = $eventid;
		$gt[3] = $row['guest_id'];
		$ev->printTicketPDF($gt, $ps, $err, $proceed, $printerId);
		sleep(10);
	}
}
