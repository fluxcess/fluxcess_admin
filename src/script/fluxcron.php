<?php
if (is_file('../conf/config.local.php')) {
	include_once ('../conf/config.local.php');
}
include_once ('../conf/config.php');

$lg = 'de';
$locale = 'de_DE';

// send invoices for those events,
// where automatic invoice sending is switched on
autoInvoiceSend();
setDirectoryOwner();
function setDirectoryOwner() {
	// this is important, because otherwise everything belongs to root and is not accessible at all
	if (strlen(BASEDIR) > 4) {
	$rightuser = fileowner(BASEDIR);
	$rightgroup = filegroup(BASEDIR);
	exec('chown -R '.$rightuser.':'.$rightgroup.' '.BASEDIR.'files');
	}
}
function autoInvoiceSend() {
	$sql = "SELECT e.fevent_id, e.fuser_id, fcfu.fcustomer_id 
			FROM fevent e 
			JOIN fcustomer_fuser fcfu ON e.fuser_id = fcfu.fuser_id
			WHERE e.deletedtime IS NULL
			;";
	
	$pdoObj = \dbconnection::getInstance();
	$pdoStatement = $pdoObj->prepare($sql, array (
			\PDO::ATTR_CURSOR => \PDO::CURSOR_FWDONLY 
	));
	$pdoStatement->execute($sqlArr);
	while ( $row = $pdoStatement->fetch(\PDO::FETCH_ASSOC) ) {
		$sql2 = "SELECT * FROM fevset_" . $row['fevent_id'] . " 
				WHERE skey = 'autoinvoice' AND sval_int = 1;";
		$pdoStatement2 = $pdoObj->prepare($sql2, array (
				\PDO::ATTR_CURSOR => \PDO::CURSOR_FWDONLY 
		));
		$pdoStatement2->execute(array ());
		if ($pdoStatement2->rowCount() == 1) {
			$eventlist[] = $row;
		}
	}
	if (is_array($eventlist) && count($eventlist) > 0) {
		foreach ( $eventlist as $evt ) {
			$fevent_id = $evt['fevent_id'];
			$_SESSION['fevent_id'] = $evt['fevent_id'];
			$_SESSION['user']['id'] = $evt['fuser_id'];
			$_SESSION['customer']['customer_id'] = $evt['fcustomer_id'];
			
			// first those customers who are not on a waiting list.
			// They need to be provided with an invoice or ticket.
			
			// ToDo: Limit number of mails per call!
			$sql = "SELECT guest_id, (SELECT IF(COUNT(*)>0,1,0) FROM xvl" . $fevent_id . " WHERE `inout` = 'inv' AND guest_id = g.guest_id) invoicesent
			FROM zguest" . $fevent_id . " g WHERE deleted = 0 AND archived = 0 AND waitinglist = 0
			AND (SELECT IF(COUNT(*)>0,1,0) FROM xvl" . $fevent_id . " WHERE `inout` = 'inv' AND guest_id = g.guest_id) = 0
			AND g.emailaddress != ''
			AND g.cancelled = 0;";
			$pdoStatement = $pdoObj->prepare($sql, array (
					\PDO::ATTR_CURSOR => \PDO::CURSOR_FWDONLY 
			));
			$pdoStatement->execute($sqlArr);
			$ev = new event($lg, $locale);
			$ev->setRecordId($fevent_id);
			$evdata = $ev->readRecord($fevent_id, true);
			while ( $row = $pdoStatement->fetch(\PDO::FETCH_ASSOC) ) {
				$g = new entityGuest($fevent_id, $row['guest_id'], $evdata);
				$guestdata = $g->getData(false);
				if (isset($evdata['outgoingmailredirection']) && $evdata['outgoingmailredirection'] == 1 && (!isset($evdata['outgoingmailredirectiontargets']) || $evdata['outgoingmailredirectiontargets'] == '')) {
				} else {
					if (!isset($guestdata['waitinglist']) || $guestdata['waitinglist'] == 0) {
						$pdoObj->beginTransaction();
						try {
							$invoiceno = $ev->getNextInvoiceNumberAndIncrease();
							echo "New invoice number: event " . $fevent_id . " / " . $invoiceno . "/ " . $guestdata['emailaddress'] . "\n";
							$data = $g->generateAndSendInvoice($invoiceno);
							$err = $g->getErrors();
							
							if (count($err) == 0) {
								$pdoObj->commit();
							} else {
								echo "***\nERRORS:\n";
								print_r($err);
								$pdoObj->rollBack();
							}
						} catch ( Exception $e ) {
							$pdoObj->rollBack();
							echo 'Invoice could not be created or sent: ' . print_r($e, true) . "\n";
						}
					} elseif (isset($guestdata['waitinglist']) && $guestdata['waitinglist'] == 1) {
						// $g->sendWaitingListMail();
					}
				}
			}
			
			// second those customers who are on a waiting list.
			// They need to be provided with an invoice or ticket.
			$sql = "SELECT guest_id, (SELECT IF(COUNT(*)>0,1,0) FROM xvl" . $fevent_id . " WHERE `inout` = 'wai' AND guest_id = g.guest_id) waitlistnotificationsent
			FROM zguest" . $fevent_id . " g WHERE deleted = 0 AND archived = 0 AND waitinglist = 1
			AND (SELECT IF(COUNT(*)>0,1,0) FROM xvl" . $fevent_id . " WHERE `inout` = 'wai' AND guest_id = g.guest_id) = 0;";
			$pdoStatement = $pdoObj->prepare($sql, array (
					\PDO::ATTR_CURSOR => \PDO::CURSOR_FWDONLY 
			));
			$pdoStatement->execute($sqlArr);
			while ( $row = $pdoStatement->fetch(\PDO::FETCH_ASSOC) ) {
				echo "waiting list for guest number $row[guest_id]...";
				$g = new entityGuest($fevent_id, $row['guest_id'], $evdata);
				$guestdata = $g->getData(false);
				
				$g->sendWaitingListMail();
			}
		}
	}
}