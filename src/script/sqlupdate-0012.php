#!/usr/bin/php
<?php
/**
 * Adds new columns "formlabel", "longcontent"
 * to table zcolumn<fevent_id> for each event
 * 08.08.2013
 */

/**
 * not sure what goes here...
 */


include_once('../conf/config.php');


try {
	// get all existing event tables
	$sql = "SELECT fevent_id FROM fevent;";
	$_pdoObj = dbconnection::getInstance();
	$pdoStatement = $_pdoObj->prepare($sql, array(PDO::ATTR_CURSOR => PDO::CURSOR_FWDONLY));
	$pdoStatement->execute(Array());
	if ($pdoStatement->errorCode() != 0) {
		echo 'SQL Fehler'.print_r($pdoStatement->errorInfo(), true);
	} else {
		while ($row = $pdoStatement->fetch()) {
			try {
				echo 'Found event: '.$row['fevent_id']."\n";
					
				$sql = "ALTER TABLE `zcolumn".$row['fevent_id']."`
						ADD `formlabel` VARCHAR( 60 ) NOT NULL DEFAULT '' AFTER `showtoguest`
						;";
				$pdoStatement2 = $_pdoObj->prepare($sql, array(PDO::ATTR_CURSOR => PDO::CURSOR_FWDONLY));
				$pdoStatement2->execute(Array());
				
				$sql = "ALTER TABLE `zcolumn".$row['fevent_id']."`
						CHANGE `formlabel` `formlabel` VARCHAR( 60 ) NOT NULL DEFAULT ''
						;";
				$pdoStatement2 = $_pdoObj->prepare($sql, array(PDO::ATTR_CURSOR => PDO::CURSOR_FWDONLY));
				$pdoStatement2->execute(Array());
					
				$sql = "ALTER TABLE `zcolumn".$row['fevent_id']."`
						ADD `longcontent` TEXT NOT NULL DEFAULT '' AFTER `fielddefault`
						;";
				$pdoStatement2 = $_pdoObj->prepare($sql, array(PDO::ATTR_CURSOR => PDO::CURSOR_FWDONLY));
				$pdoStatement2->execute(Array());
				
				$sql = "UPDATE `zcolumn".$row['fevent_id']."`
						SET formlabel = '' WHERE formlabel = '0'
						;";
				$pdoStatement2 = $_pdoObj->prepare($sql, array(PDO::ATTR_CURSOR => PDO::CURSOR_FWDONLY));
				$pdoStatement2->execute(Array());
				
			}catch (Exception $e) {
				echo 'Datenbank-Fehler: '.print_r($e->getMessage(), true);
			}
		}


	}
} catch (Exception $e) {
	echo 'Datenbank-Fehler: '.print_r($e->getMessage(), true);
}

