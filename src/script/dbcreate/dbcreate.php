<?php
/**
 * creates basic tables
 * for the database
 * 2015-03-03
 */
$thisScriptDbVersion = 29;

include_once ('../conf/config.php');

try {
	$errorCount = 0;
	// only update if this is the very next update.
	echo "Creating event table (script " . $thisScriptDbVersion . ")...\n";
	/**
	 * create event table
	 */
	
	$sql = "CREATE TABLE IF NOT EXISTS `fevent` (
`fevent_id` int(11) NOT NULL AUTO_INCREMENT,
`fuser_id` int(11) NOT NULL,
`eventtime` datetime DEFAULT NULL,
`title` varchar(128) NOT NULL,
`description` text,
`domains` text NOT NULL,
`guestloginformat` int(4) NOT NULL DEFAULT '0',
`imageHeader` varchar(40) NOT NULL,
`fontfamily` varchar(128) NOT NULL DEFAULT '',
`browserbgimage` varchar(128) NOT NULL DEFAULT '',
`optioncompanionnames` tinyint(1) NOT NULL DEFAULT '0',
`registrationactive` tinyint(1) NOT NULL DEFAULT '1',
`checkinactive` tinyint(1) NOT NULL DEFAULT '0',
`selfregistrationallowed` int(1) NOT NULL DEFAULT '0',
`selfregistrationmaxcomp` int(4) NOT NULL DEFAULT '1',
`ticketprinthtml` text,
`ticketprinthtmlpage` text NOT NULL,
`ticketprintperpage` INT(8) NOT NULL DEFAULT 0,
`ticketprintpdfmargin` VARCHAR(30) NOT NULL DEFAULT '10mm',
`registerbuttonlabel` varchar(50) NOT NULL DEFAULT 'Register',
`checkinhardlimit` int(1) NOT NULL DEFAULT '0',
`checkinlimitsource` int(1) NOT NULL DEFAULT '0',
`unconfirmedwarn` int(1) NOT NULL DEFAULT '1',
`showbrowserscanlink` int(1) NOT NULL DEFAULT '1',
`showtooltips` int(1) NOT NULL DEFAULT '1',
`formfontsize` varchar(30) NOT NULL DEFAULT '0',
`hideanswerform` int(1) NOT NULL DEFAULT '0',
`inputbgcolor` varchar(20) NOT NULL DEFAULT '',
`showpresencelink` int(1) NOT NULL DEFAULT '0',
`showstatslink` int(1) NOT NULL DEFAULT '0',
`checkindomain` varchar(128) NOT NULL DEFAULT '',
`checkintype` int(2) NOT NULL DEFAULT '0',
`accesscodelabel` varchar(20) NOT NULL DEFAULT '',
`footerhtml` text NOT NULL,
`imprintpagemenutitle` varchar(20) NOT NULL DEFAULT 'Impressum',
`imprintpagecontent` text NOT NULL,
`welcometext` text NOT NULL,
`introductiontext` text NOT NULL,
`accepttext` text NOT NULL,
`denytext` text NOT NULL,
`acceptthankyoutext` text NOT NULL,
`denythankyoutext` text NOT NULL,
`acceptmailcontent` text NOT NULL,
`denymailcontent` text NOT NULL,
`denymailsubject` varchar(256) NOT NULL DEFAULT '',
`acceptmailsubject` varchar(256) NOT NULL DEFAULT '',
`organisermailcontent` text,
`organisermailsubject` varchar(128) NOT NULL DEFAULT 'gästeliste.com changed data',
`organisermailto` varchar(128) NOT NULL DEFAULT '',
`mailsender` varchar(128) NOT NULL DEFAULT '',
`mailsendername` varchar(128) NOT NULL,
`mailbcc` text NOT NULL,
`optionunsure` tinyint(1) NOT NULL DEFAULT '1',
`fontcolor` varchar(10) NOT NULL,
`browserbgcolor` varchar(10) NOT NULL,
`formbgcolor` varchar(10) NOT NULL,
`createdtime` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
`deletedtime` datetime DEFAULT NULL,
PRIMARY KEY (`fevent_id`),
KEY `fuser_id` (`fuser_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 ;";
	
	$_pdoObj = dbconnection::getInstance();
	$pdoStatement = $_pdoObj->prepare($sql, array (
			PDO::ATTR_CURSOR => PDO::CURSOR_FWDONLY 
	));
	$pdoStatement->execute(Array ());
	
	if ($pdoStatement->errorCode() != 0) {
		echo 'SQL Fehler';
		print_r($pdoStatement->errorInfo());
		$errorCount += 1;
	}
	
	echo "Creating domain table (script " . $thisScriptDbVersion . ")...\n";
	/**
	 * create domain table
	 */
	
	$sql = "CREATE TABLE IF NOT EXISTS `domain` (
  `domain_id` int(11) NOT NULL AUTO_INCREMENT,
  `domaintype` tinyint(1) NOT NULL DEFAULT '0',
  `domainname` varchar(60) DEFAULT NULL,
  `urlpath` varchar(120) DEFAULT NULL,
  `fevent_id` int(11) NOT NULL,
  PRIMARY KEY (`domain_id`),
  KEY `fevent_id` (`fevent_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 ;
		";
	$_pdoObj = dbconnection::getInstance();
	$pdoStatement = $_pdoObj->prepare($sql, array (
			PDO::ATTR_CURSOR => PDO::CURSOR_FWDONLY 
	));
	$pdoStatement->execute(Array ());
	
	if ($pdoStatement->errorCode() != 0) {
		echo 'SQL Fehler';
		print_r($pdoStatement->errorInfo());
		$errorCount += 1;
	}
	
	echo "Creating user table (script " . $thisScriptDbVersion . ")...\n";
	/**
	 * create user table
	 */
	
	$sql = "CREATE TABLE IF NOT EXISTS `fuser` (
  `fuser_id` int(11) NOT NULL AUTO_INCREMENT,
  `email` varchar(128) NOT NULL,
  `password` varchar(255) NOT NULL,
  `salt` varchar(20) NOT NULL,
  `createdIp` varchar(45) NOT NULL,
  `createdTime` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `mailconfcode` varchar(128) NOT NULL,
  `mailconfcodetime` datetime DEFAULT NULL,
  `confirmedtime` datetime DEFAULT NULL,
  `confirmedip` varchar(46) DEFAULT NULL,
  `deleted` tinyint(1) NOT NULL DEFAULT '0',
  `lastlogin` datetime DEFAULT NULL,
  `ctloginsfailed` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`fuser_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 ;
		";
	$_pdoObj = dbconnection::getInstance();
	$pdoStatement = $_pdoObj->prepare($sql, array (
			PDO::ATTR_CURSOR => PDO::CURSOR_FWDONLY 
	));
	$pdoStatement->execute(Array ());
	
	if ($pdoStatement->errorCode() != 0) {
		echo 'SQL Fehler';
		print_r($pdoStatement->errorInfo());
		$errorCount += 1;
	}
	
	echo "Creating settings table (script " . $thisScriptDbVersion . ")...\n";
	/**
	 * create settings table
	 */
	$sql = "CREATE TABLE IF NOT EXISTS `globsettings` (
  `skey` varchar(128) NOT NULL,
  `sval` varchar(128) NOT NULL,
  PRIMARY KEY (`skey`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
		";
	$_pdoObj = dbconnection::getInstance();
	$pdoStatement = $_pdoObj->prepare($sql, array (
			PDO::ATTR_CURSOR => PDO::CURSOR_FWDONLY 
	));
	$pdoStatement->execute(Array ());
	
	if ($pdoStatement->errorCode() != 0) {
		echo 'SQL Fehler';
		print_r($pdoStatement->errorInfo());
		$errorCount += 1;
	}
	
	echo "Initially fill settings table (script " . $thisScriptDbVersion . ")...\n";
	/**
	 * initially fill settings table
	 */
	$sql = "INSERT INTO `globsettings` (`skey`, `sval`) VALUES
('dbversion', '" . $thisScriptDbVersion . "');
		;
		";
	$_pdoObj = dbconnection::getInstance();
	$pdoStatement = $_pdoObj->prepare($sql, array (
			PDO::ATTR_CURSOR => PDO::CURSOR_FWDONLY 
	));
	$pdoStatement->execute(Array ());
	
	if ($pdoStatement->errorCode() != 0) {
		echo 'SQL Fehler';
		print_r($pdoStatement->errorInfo());
		$errorCount += 1;
	}
	
	echo "Create room table (script " . $thisScriptDbVersion . ")...\n";
	/**
	 * create room table
	 */
	$sql = "CREATE TABLE IF NOT EXISTS `room` (
  `room_id` int(11) NOT NULL AUTO_INCREMENT,
  `fevent_id` int(11) NOT NULL,
  `roomname` varchar(60) NOT NULL,
  `capacity` int(11) NOT NULL,
  `active` tinyint(1) NOT NULL DEFAULT '1',
  PRIMARY KEY (`room_id`),
  KEY `fevent_id` (`fevent_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 ;
		";
	$_pdoObj = dbconnection::getInstance();
	$pdoStatement = $_pdoObj->prepare($sql, array (
			PDO::ATTR_CURSOR => PDO::CURSOR_FWDONLY 
	));
	$pdoStatement->execute(Array ());
	
	if ($pdoStatement->errorCode() != 0) {
		echo 'SQL Fehler';
		print_r($pdoStatement->errorInfo());
		$errorCount += 1;
	}
	
	echo "Create sessionlog table (script " . $thisScriptDbVersion . ")...\n";
	/**
	 * create sessionlog table
	 */
	$sql = "CREATE TABLE IF NOT EXISTS `sessionlog` (
  `sessionlog_id` bigint(20) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `session_id` varchar(64) CHARACTER SET latin1 NOT NULL,
  `postcontent` text CHARACTER SET latin1 NOT NULL,
  `getcontent` text CHARACTER SET latin1 NOT NULL,
  `sessionlog_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`sessionlog_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;
		";
	$_pdoObj = dbconnection::getInstance();
	$pdoStatement = $_pdoObj->prepare($sql, array (
			PDO::ATTR_CURSOR => PDO::CURSOR_FWDONLY 
	));
	$pdoStatement->execute(Array ());
	
	if ($pdoStatement->errorCode() != 0) {
		echo 'SQL Fehler';
		print_r($pdoStatement->errorInfo());
		$errorCount += 1;
	}
	
	
	if ($errorCount == 0) {
		// update db version
		updateDbVersion($thisScriptDbVersion);
	}
} catch (Exception $e) {
	echo 'Datenbank-Fehler';
	print_r($e->getMessage());
}