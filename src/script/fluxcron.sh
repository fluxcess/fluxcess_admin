#!/bin/bash

# ensure the script runs only once:
[[ "$(pidof -x $(basename $0))" != $$ ]] && echo "Script running already." && exit

# start the fluxcess cron job:
/usr/bin/php fluxcron.php