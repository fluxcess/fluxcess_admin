#!/usr/bin/php
<?php
/**
 * Adds new column "domains" 
 * to table fevent
 * 06.08.2013
 */

/**
 * not sure what goes here...
 */


include_once('../conf/config.php');


try {
	$_pdoObj = dbconnection::getInstance();
	$sql = "ALTER TABLE `fevent`
						ADD `domains` TEXT NOT NULL DEFAULT '' AFTER `description`
						;";
	$pdoStatement2 = $_pdoObj->prepare($sql, array(PDO::ATTR_CURSOR => PDO::CURSOR_FWDONLY));
	$pdoStatement2->execute(Array());
	
} catch (Exception $e) {
	echo 'Datenbank-Fehler: '.print_r($e->getMessage(), true);
}

