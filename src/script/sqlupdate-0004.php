#!/usr/bin/php
<?php
/**
 * Adds new table zcolumn<fevent_id> for each event
 * 07.07.2013
 */

/**
 * not sure what goes here...
 */


include_once('../conf/config.php');


try {
	// get all existing event tables
	$sql = "SELECT fevent_id FROM fevent;";
	$_pdoObj = dbconnection::getInstance();
	$pdoStatement = $_pdoObj->prepare($sql, array(PDO::ATTR_CURSOR => PDO::CURSOR_FWDONLY));
	$pdoStatement->execute(Array());
	if ($pdoStatement->errorCode() != 0) {
		$this->addError('', 'SQL Fehler'.print_r($pdoStatement->errorInfo(), true), 1);
	} else {
		while ($row = $pdoStatement->fetch()) {
			echo 'Found event: '.$row['fevent_id']."\n";
			
			// create the column table
			$sql = "CREATE TABLE IF NOT EXISTS `zcolumn".$row['fevent_id']."` (
					`field_id` int(11) NOT NULL AUTO_INCREMENT,
					`fieldorder` int(11) NOT NULL DEFAULT '0',
					`fieldname` varchar(16) NOT NULL,
					`fieldtype` int(2) NOT NULL DEFAULT '0',
					`fieldlength` int(2) NOT NULL DEFAULT '0',
					`fielddefault` varchar(16) NULL,
					`syscolumn` tinyint(1) NOT NULL DEFAULT '0',
					`note` TEXT NOT NULL,
					`deleted` tinyint(1) NOT NULL DEFAULT '0',
					PRIMARY KEY (`field_id`),
					UNIQUE KEY `fieldname` (`fieldname`)
					) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;
					;";
			$pdoStatement2 = $_pdoObj->prepare($sql, array(PDO::ATTR_CURSOR => PDO::CURSOR_FWDONLY));
			$pdoStatement2->execute(Array());
		}
	}
} catch (Exception $e) {
	$this->addError('', 'Datenbank-Fehler', 1, print_r($e->getMessage(), true));
}

