#!/usr/bin/php
<?php
/**
 * migrates from showtoguest to fieldorder
 * in table zcolumn<fevent_id> for each event
 * 08.08.2013
 */
/**
 * not sure what goes here...
 */

include_once('../conf/config.php');


try {
	// get all existing event tables
	$sql = "SELECT fevent_id FROM fevent;";
	$_pdoObj = dbconnection::getInstance();
	$pdoStatement = $_pdoObj->prepare($sql, array(PDO::ATTR_CURSOR => PDO::CURSOR_FWDONLY));
	$pdoStatement->execute(Array());
	if ($pdoStatement->errorCode() != 0) {
		echo 'SQL Fehler'.print_r($pdoStatement->errorInfo(), true);
	} else {
		while ($row = $pdoStatement->fetch()) {
			try {
				echo 'Found event: '.$row['fevent_id']."\n";
				echo "Allow NULL in fieldorder...\n";
				$sql = "ALTER TABLE `zcolumn".$row['fevent_id']."`
						CHANGE `fieldorder` `fieldorder` INT NULL DEFAULT NULL
						;";
				$pdoStatement2 = $_pdoObj->prepare($sql, array(PDO::ATTR_CURSOR => PDO::CURSOR_FWDONLY));
				$pdoStatement2->execute(Array());
							
				echo "Setting field order to NULL...\n";
				$sql = "UPDATE `zcolumn".$row['fevent_id']."`
						SET fieldorder = NULL
						;";
				$pdoStatement2 = $_pdoObj->prepare($sql, array(PDO::ATTR_CURSOR => PDO::CURSOR_FWDONLY));
				$pdoStatement2->execute(Array());
				
				echo "Searching showtoguest = 1 ...\n";
				$sql = "SELECT * FROM `zcolumn".$row['fevent_id']."`
						WHERE showtoguest = 1
						;";
				$pdoStatement2 = $_pdoObj->prepare($sql, array(PDO::ATTR_CURSOR => PDO::CURSOR_FWDONLY));
				$pdoStatement2->execute(Array());
				$i = 1;
				while ($rowguest = $pdoStatement2->fetch()) {
					echo "Field no: ".$rowguest['field_id']."\n";
					$sql = "UPDATE `zcolumn".$row['fevent_id']."` 
								SET fieldorder = $i
								WHERE field_id = ".$rowguest[field_id]."";
					$pdoStatement3 = $_pdoObj->prepare($sql, array(PDO::ATTR_CURSOR => PDO::CURSOR_FWDONLY));
					$pdoStatement3->execute(Array());
					$i++;
				}
				
			}catch (Exception $e) {
				echo 'Datenbank-Fehler: '.print_r($e->getMessage(), true);
			}
		}


	}
} catch (Exception $e) {
	echo 'Datenbank-Fehler: '.print_r($e->getMessage(), true);
}

