#!/usr/bin/php
<?php
/**
 * Adds new columns "guestloginformat",
 * "welcometext", "introductiontext", 
 * "accepttext", "denytext",
 * "acceptthankyoutext", "denythankyoutext",
 * "acceptmailcontent", "denymailcontent",
 * "acceptmailsubject", "denymailsubject",
 * "mailsender", "mailbcc"
 * to table fevent
 * 07.08.2013
 */

/**
 * not sure what goes here...
 */
include_once('../conf/config.php');


try {
	$_pdoObj = dbconnection::getInstance();
	$sql = "ALTER TABLE `fevent`
						ADD `guestloginformat` INT(4) NOT NULL DEFAULT '0' AFTER `domains`,
						ADD `welcometext` TEXT NOT NULL DEFAULT '' AFTER `guestloginformat`,
						ADD `introductiontext` TEXT NOT NULL DEFAULT '' AFTER `welcometext`,
						ADD `accepttext` TEXT NOT NULL DEFAULT '' AFTER `introductiontext`,
						ADD `denytext` TEXT NOT NULL DEFAULT '' AFTER `accepttext`,
						ADD `acceptthankyoutext` TEXT NOT NULL DEFAULT '' AFTER `denytext`,
						ADD `denythankyoutext` TEXT NOT NULL DEFAULT '' AFTER `acceptthankyoutext`,
						ADD `acceptmailcontent` TEXT NOT NULL DEFAULT '' AFTER `denythankyoutext`,
						ADD `denymailsubject` VARCHAR(256) NOT NULL DEFAULT '' AFTER `acceptmailcontent`,
						ADD `acceptmailsubject` VARCHAR(256) NOT NULL DEFAULT '' AFTER `denymailsubject`,
						ADD `mailsender` VARCHAR(128) NOT NULL DEFAULT '' AFTER `acceptmailsubject`,
						ADD `mailbcc` TEXT NOT NULL DEFAULT '' AFTER `mailsender`
						;";
	$pdoStatement2 = $_pdoObj->prepare($sql, array(PDO::ATTR_CURSOR => PDO::CURSOR_FWDONLY));
	$pdoStatement2->execute(Array());
	
} catch (Exception $e) {
	echo 'Datenbank-Fehler: '.print_r($e->getMessage(), true);
}

