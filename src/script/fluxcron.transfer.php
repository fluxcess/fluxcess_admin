<?php
if (is_file('../conf/config.local.php')) {
	include_once ('../conf/config.local.php');
}
include_once ('../conf/config.php');

$lg = 'de';
$locale = 'de_DE';

$eventTablesToBeTransferred = array (
		'ygr',
		'wemail',
		'zguest',
		'zcolumn',
		'fevset_',
		'zregprocstep',
		'xvl' 
);
$customerTablesToBeTransferred = array (
		'emailtemplate_' 
);

if (strlen($glob['DB_USER']) > 0) {
	createMissingMysqlDumps();
	if (TRANSFER_OUT_ALLOWED === true) {
		pushOutgoingTransfers();
	}
	if (TRANSFER_IN_ALLOWED === true || TRANSFER_FOREIGNTRIGGERED_IN_ALLOWED === true) {
		checkIncomingTransfers();
	}
} else {
	echo "No database username set.\n";
}
function checkIncomingTransfers() {
	global $glob;
	$transferDir = BASEDIR . 'tmp/transfer_in';
	if (!is_dir($transferDir)) {
		mkdir($transferDir);
	}
	$maxJobs = 1;
	$currentJob = 0;
	// go through all the files
	$files = scandir($transferDir);
	foreach ( $files as $f ) {
		// if ($f != '.' && $f != '..' && substr($f, -8) == '.tar.bz2' && $currentJob < $maxJobs) {
		if ($f != '.' && $f != '..' && substr($f, 0, 7) == 'status_' && $currentJob < $maxJobs) {
			$statusFileName = $f;
			$dataFileName = substr($f, 7) . '.tar.bz2';
			
			$statusFileContent = file_get_contents($transferDir . '/' . $statusFileName);
			$statusFileValues = explode(';', $statusFileContent);
			$currentStatus = $statusFileValues[0];
			$customerid = $statusFileValues[1];
			$remotecustomerid = $statusFileValues[2];
			if ($customerid < 1) {
				$customerid = 0;
			}
			if ($currentStatus == 'downloaded') {
				$fnparts = explode('_', $dataFileName);
				$remotedatafilename = 'c' . $remotecustomerid . '_' . $fnparts[1] . '_' . $fnparts[2] . '_' . $fnparts[3];
				
				// create directory for local customer
				$cmd0 = 'mkdir ' . $transferDir . '/' . $customerid;
				echo '#' . $cmd0 . "\n";
				passthru($cmd0);
				
				// unpack
				$cmd1 = 'cd ' . $transferDir . '/' . $customerid . ' && tar xvfj ' . $remotedatafilename . ' -C ' . $transferDir . '/' . $customerid . '/ ' . ' && rm ' . $remotedatafilename;
				echo '#' . $cmd1 . "\n";
				passthru($cmd1);
				
				//
				$cmd1b = 'cd ' . $transferDir . ' && echo "unpacked;' . $customerid . ';' . $remotecustomerid . '" > ' . $statusFileName;
				passthru($cmd1b);
				
				// get into db
				$fx = substr($remotedatafilename, 0, -8);
				$cmd2 = 'cd ' . $transferDir . '/' . $customerid . ' && mysql -u ' . $glob['DB_USER'] . ' -p' . $glob['DB_PW'] . ' ' . $glob['DB_DB'] . ' < ' . $fx . '.sql';
				echo '#' . $cmd2 . "\n";
				passthru($cmd2);
				
				$cmd3 = 'cd ' . $transferDir . '/' . $customerid . ' && mysql -u ' . $glob['DB_USER'] . ' -p' . $glob['DB_PW'] . ' ' . $glob['DB_DB'] . ' < ' . $fx . '.sql2';
				echo '#' . $cmd3 . "\n";
				passthru($cmd3);
				
				$cmd4 = 'cd ' . $transferDir . '/' . $customerid . ' && mysql -u ' . $glob['DB_USER'] . ' -p' . $glob['DB_PW'] . ' ' . $glob['DB_DB'] . ' < ' . $fx . '.sql3';
				echo '#' . $cmd4 . "\n";
				passthru($cmd4);
				
				$cmd4b = 'cd ' . $transferDir . ' && echo "loaded;' . $customerid . ';' . $remotecustomerid . '" > ' . $statusFileName;
				passthru($cmd4b);
				
				// remove files
				$cmdDel = 'cd ' . $transferDir . '/' . $customerid . ' && rm ' . $fx . '*';
				// echo '#' . $cmdDel . "\n";
				// passthru($cmdDel);
				
				// set status to "done"
				echo "setting status file " . $transferDir . '/' . $statusfilename . ' to done...';
				file_put_contents($transferDir . '/' . $statusFileName, 'done;' . $customerid . ';' . $remotecustomerid);
				
				$currentJob += 1;
			}
		}
	}
}
function createMissingMysqlDumps() {
	$triggerDir = BASEDIR . 'tmp/triggers';
	if (!is_dir($triggerDir)) {
		mkdir($triggerDir);
	}
	$maxJobs = 1;
	$currentJob = 0;
	// go through all the files
	$files = scandir($triggerDir);
	foreach ( $files as $f ) {
		if ($f != '.' && $f != '..' && $currentJob < $maxJobs) {
			// ToDo: if file is old, delete it
			// if file is not "done" yet, create dump
			$cts = file_get_contents($triggerDir . '/' . $f);
			
			$filenameParts = explode('_', $f);
			$customer_id = substr($filenameParts[1], 4);
			$fevent_id = substr($filenameParts[2], 2);
			$randX = $filenameParts[5];
			$ts = $filenameParts[4];
			
			// $data['file'] = $filename;
			if (substr($cts, 0, 4) !== 'done' && substr($ct, 0, 9) !== 'cancelled') {
				if (($filenameParts[0] == 'pushoverPrep' && strpos($cts, 'sqlgenerated=n') !== false) || $filenameParts[0] == 'takeoverPrep') {
					if ($currentJob < $maxJobs) {
						echo "C " . $customer_id . " | E " . $fevent_id . " | TS " . $ts . " | R " . $randX . "\n";
						$exportshortfilename = 'c' . $customer_id . '_e' . $fevent_id . '_ts' . $ts . '_r' . $randX;
						if (!is_dir(BASEDIR . 'tmp/transfer_out')) {
							mkdir(BASEDIR . 'tmp/transfer_out');
						}
						$exportfilename = BASEDIR . 'tmp/transfer_out/' . $exportshortfilename;
						createMysqlDump($customer_id, $fevent_id, $randX, $ts, $exportfilename . '.sql');
						echo "sql dump created, file size: " . filesize($exportfilename . '.sql') . "\n";
						compressMysqlDump($exportshortfilename);
						echo "sql dump bzipped, file size: " . filesize($exportfilename . '.tar.bz2') . "\n";
						setTriggerDone('sqldump', $customer_id, $fevent_id, $ts, $randX);
						$currentJob += 1;
					}
				}
			}
		}
	}
}
function setTriggerDone($triggertype, $customer_id, $fevent_id, $ts, $randX) {
	$fname = BASEDIR . 'tmp/triggers/takeoverPrep_cust' . $customer_id . '_ev' . $fevent_id . '__' . $ts . '_' . $randX;
	if (is_file($fname)) {
		file_put_contents($fname, 'done');
	} else {
		$fname = BASEDIR . 'tmp/triggers/pushoverPrep_cust' . $customer_id . '_ev' . $fevent_id . '__' . $ts . '_' . $randX;
		if (is_file($fname)) {
			$cts = file_get_contents($fname);
			if (substr($cts, 0, 3) == 'new') {
				$lines = explode("\r", $cts);
				// $newcts = "done\n";
				$lineNumber = 1;
				foreach ( $lines as $line ) {
					// if ($line != "new") {
					if ($triggertype == 'sqldump' && $line == 'sqlgenerated=n') {
						$newcts .= "sqlgenerated=y\r";
					} elseif ($triggertype == 'pushtransferred' && $line == 'pushtransferred=n') {
						$newcts .= "pushtransferred=y\r";
					} elseif ($triggertype == 'sqlimported' && $line == 'sqlimported=n') {
						$newcts .= "sqlimported=y\r";
					} elseif ($triggertype === null && $lineNumber == 1) {
						$newcts .= 'done' . "\r";
					} else {
						if (strlen(trim($line)) > 0) {
							$newcts .= $line . "\r";
						}
					}
					$lineNumber += 1;
				}
			}
			file_put_contents($fname, $newcts);
		}
	}
}
function createMysqlDump($customer_id, $fevent_id, $randX, $ts, $exportfilename) {
	global $eventTablesToBeTransferred, $customerTablesToBeTransferred, $glob;
	$outdir = BASEDIR . 'tmp/transfer_out';
	if (!is_dir($outdir)) {
		mkdir($outdir);
	}
	$cmd1 = 'mysqldump --complete-insert -u ' . $glob['DB_USER'] . ' -p' . $glob['DB_PW'] . ' ' . $glob['DB_DB'] . ' ';
	foreach ( $eventTablesToBeTransferred as $t ) {
		$cmd1 .= $t . $fevent_id . ' ';
	}
	foreach ( $customerTablesToBeTransferred as $t ) {
		$cmd1 .= $t . $customer_id . ' ';
	}
	$cmd1 .= ' > ' . $exportfilename;
	passthru($cmd1);
	
	$cmd2 = 'mysqldump --complete-insert -u ' . $glob['DB_USER'] . ' -p' . $glob['DB_PW'] . " --where='fevent_id=" . $fevent_id . "' -t --skip-add-drop-table --replace " . $glob['DB_DB'] . ' fevent > ' . $exportfilename . '2';
	passthru($cmd2);
	
	$cmd3 = 'mysqldump --complete-insert -u ' . $glob['DB_USER'] . ' -p' . $glob['DB_PW'] . " --where='fevent_id=" . $fevent_id . "' -t --skip-add-drop-table --replace " . $glob['DB_DB'] . ' room > ' . $exportfilename . '3';
	passthru($cmd3);
}
function compressMysqlDump($file) {
	echo "finding $file\n";
	$cmd = 'cd ' . BASEDIR . 'tmp/transfer_out && tar -cvjf ' . $file . '.tar.bz2 ' . $file . '* --remove-files';
	echo $cmd . PHP_EOL;
	passthru($cmd);
}
function pushOutgoingTransfers() {
	$triggerDir = BASEDIR . 'tmp/triggers';
	if (!is_dir($triggerDir)) {
		mkdir($triggerDir);
	}
	$maxJobs = 1;
	$currentJob = 0;
	// go through all the files
	$files = scandir($triggerDir);
	foreach ( $files as $f ) {
		if ($f != '.' && $f != '..' && substr($f, 0, strlen('pushoverPrep')) == 'pushoverPrep' && $currentJob < $maxJobs) {
			$cts = file_get_contents($triggerDir . '/' . $f);
			$fdata = explode("\r", $cts);
			foreach ( $fdata as $fdat ) {
				$fda = explode('=', $fdat);
				if (count($fda) == 2) {
					$parameter[$fda[0]] = $fda[1];
				}
			}
			$filenameParts = explode('_', $f);
			$customer_id = substr($filenameParts[1], 4);
			$fevent_id = substr($filenameParts[2], 2);
			$randX = $filenameParts[5];
			$ts = $filenameParts[4];
			$exportshortfilename = 'c' . $customer_id . '_e' . $fevent_id . '_ts' . $ts . '_r' . $randX . '.tar.bz2';
			$sqltargzfile = BASEDIR . 'tmp/transfer_out/' . $exportshortfilename;
			
			// if transfer not done yet, push data to remote server
			if (substr($cts, 0, 4) !== 'done' && substr($cts, 0, 9) !== 'cancelled') {	
				if ($parameter['pushtransferred'] == 'n') {
					if ($currentJob < $maxJobs) {
						
						echo "Trying to transfer file: " . $sqltargzfile . PHP_EOL;
						$cFile = curl_file_create($sqltargzfile);
						$parameter['form'] = 'pushevent';
						$parameter['file_contents'] = $cFile;
						$post = $parameter;
						$ch = curl_init();
						curl_setopt($ch, CURLOPT_URL, $parameter['url'] . '/api/remoteevents/pushevent');
						curl_setopt($ch, CURLOPT_USERPWD, $parameter['username'] . ":" . $parameter['password']);
						curl_setopt($ch, CURLOPT_POST, 1);
						curl_setopt($ch, CURLOPT_POSTFIELDS, $post);
						curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
						$result = curl_exec($ch);
						curl_close($ch);
						echo "CURL RESULT: " . $result . " ***| ";
						$successMsgExpected = 'Data stored on server.';
						if (strpos($result, $successMsgExpected) !== false) {
							setTriggerDone('pushtransferred', $customer_id, $fevent_id, $ts, $randX);
						}
						$currentJob += 1;
					}
				} elseif ($parameter['sqlimported'] == 'n') {
					$evt = new eventtransfer();
					$listOfTransfers = $evt->getRemoteTransferList($parameter['url'], $parameter['username'], $parameter['password']);
					foreach ( $listOfTransfers['transfers']['in'] as $transfer ) {
						if ($transfer['fevent'] == $fevent_id && $transfer['timestamp'] == $ts && $transfer['rand'] == $randX) {
							if ($transfer['status'] == 'done') {
								setTriggerDone('sqlimported', $customer_id, $fevent_id, $ts, $randX);
								setTriggerDone(null, $customer_id, $fevent_id, $ts, $randX);
							}
						}
					}
				}
			}
			
			$myTs = mktime(0, 0, 0, substr($ts, 4, 2), substr($ts, 6, 2), substr($ts, 0, 4));
			if ($myTs + 60 * 60 * 24 * 2 < time()) {
				if (is_file($sqltargzfile)) {
					unlink($sqltargzfile);
				}
				if (is_file($triggerDir . '/' . $f)) {
					unlink($triggerDir . '/' . $f);
				}
			}
		}
	}
}