#!/usr/bin/php
<?php
/**
 * creates translation files
 * @author Line5
 */

$docRoot = getenv('DOCUMENT_ROOT');
if ($docRoot == '') {
	$docRoot = __DIR__;
}
if (substr($docRoot, -1) != '/') {
	$docRoot .= '/';
}
echo $docRoot."\n";
include_once($docRoot.'../conf/config.local.php');
include_once($docRoot.'../conf/config.php');

$langs = array('de_DE.UTF-8', 'en_US.UTF-8');
foreach ($langs as $lang) {
$cmd = 'msgfmt -o '.BASEDIR.'locale/'.$lang.'/LC_MESSAGES/messages.mo '.BASEDIR.'locale/'.$lang.'/LC_MESSAGES/messages.po';
exec($cmd);
}