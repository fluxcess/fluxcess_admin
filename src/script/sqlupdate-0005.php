#!/usr/bin/php
<?php
/**
 * Adds new table zcolumn<fevent_id> for each event
 * 07.07.2013
 */

/**
 * not sure what goes here...
 */


include_once('../conf/config.php');

try {
	// get all existing event tables
	$sql = "SELECT fevent_id FROM fevent;";
	$_pdoObj = dbconnection::getInstance();
	$pdoStatement = $_pdoObj->prepare($sql, array(PDO::ATTR_CURSOR => PDO::CURSOR_FWDONLY));
	$pdoStatement->execute(Array());
	if ($pdoStatement->errorCode() != 0) {
		$this->addError('', 'SQL Fehler'.print_r($pdoStatement->errorInfo(), true), 1);
	} else {
		while ($row = $pdoStatement->fetch()) {
			echo 'Found event: '.$row['fevent_id']."\n";

			// create the column table
			$sql = "SHOW COLUMNS FROM `zguest".$row['fevent_id']."`;";
			$pdoStatement2 = $_pdoObj->prepare($sql, array(PDO::ATTR_CURSOR => PDO::CURSOR_FWDONLY));
			$pdoStatement2->execute(Array());
			$counter = 1;
			while ($row2 = $pdoStatement2->fetch()) {
				$bracketpos = strpos($row2['Type'], '(');
				if ($bracketpos !== false) {
					$ftype = substr($row2['Type'], 0, $bracketpos);
					$bracket2pos = strpos($row2['Type'], ')');
					$flen = substr($row2['Type'], $bracketpos+1, $bracket2pos-$bracketpos-1);
				} else {
					$ftype = $row2['Type'];
					$flen = 0;
				}
				if ($row2['Default'] == NULL) {
					$fdef = '';
				} else {
					$fdef = $row2['Default'];
				}
				$sql = "INSERT INTO `zcolumn".$row['fevent_id']."`
						SET fieldorder = :counter,
						fieldname = :fieldname, fieldtype = :fieldtype,
						fieldlength = :fieldlength, fielddefault = :fielddefault,
						syscolumn = 1, deleted = 0
						;";
				$pdoStatement3 = $_pdoObj->prepare($sql, array(PDO::ATTR_CURSOR => PDO::CURSOR_FWDONLY));
				$pdoStatement3->execute(Array(':counter' => $counter,
						':fieldname' => $row2['Field'], ':fieldtype' => $ftype,
						':fieldlength' => $flen, ':fielddefault' => $fdef));
				$counter += 1;
			}
		}
	}
} catch (Exception $e) {
	$this->addError('', 'Datenbank-Fehler', 1, print_r($e->getMessage(), true));
}

