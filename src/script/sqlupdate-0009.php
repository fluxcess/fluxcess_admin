#!/usr/bin/php
<?php

/**
 * Adds new columns "showtoguest", "editbyguest", "editbymanager" 
 * to table zcolumn<fevent_id> for each event
 * 14.07.2013
 */
/**
 * not sure what goes here...
 */

include_once('../conf/config.php');


try {
	// get all existing event tables
	$sql = "SELECT fevent_id FROM fevent;";
	$_pdoObj = dbconnection::getInstance();
	$pdoStatement = $_pdoObj->prepare($sql, array(PDO::ATTR_CURSOR => PDO::CURSOR_FWDONLY));
	$pdoStatement->execute(Array());
	if ($pdoStatement->errorCode() != 0) {
		echo 'SQL Fehler'.print_r($pdoStatement->errorInfo(), true);
	} else {
		while ($row = $pdoStatement->fetch()) {
			try {
				echo 'Found event: '.$row['fevent_id']."\n";
					
				$sql = "ALTER TABLE `zcolumn".$row['fevent_id']."`
						ADD `showtoguest` TINYINT( 1 ) NOT NULL DEFAULT '0' AFTER `syscolumn`
						;";
				$pdoStatement2 = $_pdoObj->prepare($sql, array(PDO::ATTR_CURSOR => PDO::CURSOR_FWDONLY));
				$pdoStatement2->execute(Array());
					
				$sql = "ALTER TABLE `zcolumn".$row['fevent_id']."`
						ADD `editbyguest` TINYINT( 1 ) NOT NULL DEFAULT '0' AFTER `showtoguest`
						;";
				$pdoStatement2 = $_pdoObj->prepare($sql, array(PDO::ATTR_CURSOR => PDO::CURSOR_FWDONLY));
				$pdoStatement2->execute(Array());
				
				$sql = "ALTER TABLE `zcolumn".$row['fevent_id']."`
						ADD `editbymanager` TINYINT( 1 ) NOT NULL DEFAULT '0' AFTER `editbyguest`
						;";
				$pdoStatement2 = $_pdoObj->prepare($sql, array(PDO::ATTR_CURSOR => PDO::CURSOR_FWDONLY));
				$pdoStatement2->execute(Array());
			}catch (Exception $e) {
				echo 'Datenbank-Fehler: '.print_r($e->getMessage(), true);
			}
		}


	}
} catch (Exception $e) {
	echo 'Datenbank-Fehler: '.print_r($e->getMessage(), true);
}

