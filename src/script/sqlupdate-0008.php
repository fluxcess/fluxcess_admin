#!/usr/bin/php
<?php
/**
 * fills new columns exportorder and syshidden for table zcolumn<fevent_id> for each event
 * 11.07.2013
 */

/**
 * not sure what goes here...
 */

include_once('../conf/config.php');

$standardcols = Array(
		// exportorder, syshidden
		'guest_id' => array(null, 0),
		'fevent_id' => array(null, 1),
		'salutation' => array(1, 0),
		'firstname' => array(2, 0),
		'lastname' => array(3, 0),
		'company' => array(4, 0),
		'emailaddress' => array(null, 0),
		'invited' => array(5, 0),
		'registered' => array(6, 0),
		'cancelled' => array(7, 0),
		'note' => array(8, 0),
		'accesscode' => array(9, 0),
		'lastselflogin' => array(null, 1),
		'selfloginsfailed' => array(null, 1),
		'deletedtime' => array(null, 1),
		'createdtime' => array(null, 1),
		'changetime' => array(null, 1)
);

try {
	// get all existing event tables
	$sql = "SELECT fevent_id FROM fevent;";
	$_pdoObj = dbconnection::getInstance();
	$pdoStatement = $_pdoObj->prepare($sql, array(PDO::ATTR_CURSOR => PDO::CURSOR_FWDONLY));
	$pdoStatement->execute(Array());
	if ($pdoStatement->errorCode() != 0) {
		$this->addError('', 'SQL Fehler'.print_r($pdoStatement->errorInfo(), true), 1);
	} else {
		while ($row = $pdoStatement->fetch()) {
			echo 'Found event: '.$row['fevent_id']."\n";

			// create the column table
			$sql = "SELECT * FROM `zcolumn".$row['fevent_id']."`;";
			$pdoStatement2 = $_pdoObj->prepare($sql, array(PDO::ATTR_CURSOR => PDO::CURSOR_FWDONLY));
			$pdoStatement2->execute(Array());
			$counter = 1;
			while ($row2 = $pdoStatement2->fetch()) {
				$fname = $row2['fieldname'];
				
				if (is_numeric($standardcols[$fname][0])) {
					$exportorder = $standardcols[$fname][0];
				} else {
					$exportorder = NULL;
				}
					
				if ($standardcols[$fname][1] == 1) {
					$syshidden = 1;
				} else {
					$syshidden = 0;
				}
				
				$sql = "UPDATE `zcolumn".$row['fevent_id']."`
						SET syshidden = :syshidden, exportorder = :exportorder 
						WHERE field_id = :fieldid
						;";
				$pdoStatement3 = $_pdoObj->prepare($sql, array(PDO::ATTR_CURSOR => PDO::CURSOR_FWDONLY));
				$pdoStatement3->execute(Array(':syshidden' => $syshidden,
						':exportorder' => $exportorder, ':fieldid' => $row2['field_id']));
			}
		}
	}
} catch (Exception $e) {
	$this->addError('', 'Datenbank-Fehler', 1, print_r($e->getMessage(), true));
}

