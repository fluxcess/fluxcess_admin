<?php
/**
 * Adds table "sessionlog"
 * 02.05.2015
 */
$thisScriptDbVersion = 28;

include_once('../conf/config.php');

try {
	$errorCount = 0;
	// only update if this is the very next update.
	if ($currentDbVersion == $thisScriptDbVersion - 1) {
		// change event table
		echo "Adding table sessionlog (script ".$thisScriptDbVersion.")...\n";
		$sql = "CREATE TABLE IF NOT EXISTS `sessionlog` (
  `sessionlog_id` bigint(20) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `session_id` varchar(64) CHARACTER SET latin1 NOT NULL,
  `postcontent` text CHARACTER SET latin1 NOT NULL,
  `getcontent` text CHARACTER SET latin1 NOT NULL,
  `sessionlog_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`sessionlog_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;
		;";

		$_pdoObj = dbconnection::getInstance();
		$pdoStatement = $_pdoObj->prepare($sql, array(PDO::ATTR_CURSOR => PDO::CURSOR_FWDONLY));
		$pdoStatement->execute(Array());
		if ($pdoStatement->errorCode() != 0) {
			echo 'SQL Fehler';
			print_r($pdoStatement->errorInfo());
			$errorCount += 1;
		}
		if ($errorCount == 0) {
			// update db version
			updateDbVersion($thisScriptDbVersion);
		} 
	}
} catch (Exception $e) {
	echo 'Datenbank-Fehler';
	print_r($e->getMessage());
}

