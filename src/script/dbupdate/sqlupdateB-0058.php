<?php
/**
 * adds a "type" field to every emailtemplate table
 * type 1 is email template, type 2 is pdf template
 * 22.02.2017
 */
$thisScriptDbVersion = 58;

include_once ('../conf/config.php');

try {
	$errorCount = 0;
	// only update if this is the very next update.
	if ($currentDbVersion == $thisScriptDbVersion - 1) {
		$_pdoObj = dbconnection::getInstance();
		
		$_pdoObj->beginTransaction();
		
		try {
			// find all guest tables
			$sql = "SELECT fcustomer_id FROM `fcustomer`
					ORDER BY fcustomer_id ASC;";
			$pdoStatementF = $_pdoObj->prepare($sql, array (
					PDO::ATTR_CURSOR => PDO::CURSOR_FWDONLY 
			));
			$pdoStatementF->execute(Array ());
			if ($pdoStatementF->errorCode() != 0) {
				echo 'SQL Fehler';
				print_r($pdoStatementF->errorInfo());
				$errorCount += 1;
			} else {
				while ( $row = $pdoStatementF->fetch() ) {
					$fcustomerid = $row['fcustomer_id'];
					
					// change emailtemplate table
					try {
						$result = $_pdoObj->query("SELECT 1 FROM `emailtemplate_" . $fcustomerid . "` LIMIT 1");
						try {
						
							echo "adding new column to emailtemplate table " . $fcustomerid . " (script " . $thisScriptDbVersion . ")...\n";
							$sql = "ALTER TABLE `emailtemplate_" . $fcustomerid . "` 
							ADD `tpl_type` INT NOT NULL DEFAULT '1' AFTER `fcustomer_id`;";
							try {
								$pdoStatement = $_pdoObj->prepare($sql, array (
										PDO::ATTR_CURSOR => PDO::CURSOR_FWDONLY 
								));
								$pdoStatement->execute(Array ());
								if ($pdoStatement->errorCode() != 0) {
									echo 'SQL Fehler';
									print_r($pdoStatement->errorInfo());
									$errorCount += 1;
								}
							} catch ( Exception $e ) {
							}
						} catch ( Exception $e ) {
							echo 'error describing table.' . PHP_EOL;
						}
					} catch ( Exception $e ) {
						// We got an exception == table not found
					}
				}
			}
			if ($errorCount == 0) {
				// update db version
				updateDbVersion($thisScriptDbVersion);
				$_pdoObj->commit();
			} else {
				$_pdoObj->rollBack();
			}
		} catch ( Exception $e ) {
			$_pdoObj->rollBack();
			echo "Innerer Datenbank-Fehler";
			print_r($e->getMessage());
		}
	}
} catch ( Exception $e ) {
	echo 'Datenbank-Fehler';
	print_r($e->getMessage());
}

