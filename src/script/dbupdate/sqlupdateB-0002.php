<?php
/**
 * Adds columns "imprintpagecontent" and "imprintpagemenutitle" to the event table
 * 11.08.2013
 */
$thisScriptDbVersion = 2;

include_once('../conf/config.php');

try {
	$errorCount = 0;
	// only update if this is the very next update.
	if ($currentDbVersion == $thisScriptDbVersion - 1) {
		// change event table
		echo "Adding columns to event table (script 2)...\n";
		$sql = "ALTER TABLE `fevent`
				ADD `imprintpagecontent` TEXT NOT NULL DEFAULT '' AFTER `imageheader` ,
				ADD `imprintpagemenutitle` VARCHAR(20) NOT NULL DEFAULT 'Impressum' AFTER `imageheader`
				;";

		$_pdoObj = dbconnection::getInstance();
		$pdoStatement = $_pdoObj->prepare($sql, array(PDO::ATTR_CURSOR => PDO::CURSOR_FWDONLY));
		$pdoStatement->execute(Array());
		if ($pdoStatement->errorCode() != 0) {
			$this->addError('', 'SQL Fehler'.print_r($pdoStatement->errorInfo(), true), 1);
			$errorCount += 1;
		}
		if ($errorCount == 0) {
			// update db version
			updateDbVersion($thisScriptDbVersion);
		} 
	}
} catch (Exception $e) {
	$this->addError('', 'Datenbank-Fehler', 1, print_r($e->getMessage(), true));
}

