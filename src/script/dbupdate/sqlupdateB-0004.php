<?php
/**
 * Adds columns "footerhtml", "accesscodelabel", and "fontfamily" to the event table
 * 11.08.2013
 */
$thisScriptDbVersion = 4;

include_once('../conf/config.php');

try {
	$errorCount = 0;
	// only update if this is the very next update.
	if ($currentDbVersion == $thisScriptDbVersion - 1) {
		// change event table
		echo "Adding columns to event table (script 4)...\n";
		$sql = "ALTER TABLE `fevent`
				ADD `footerhtml` TEXT NOT NULL DEFAULT '' AFTER `imageheader` ,
				ADD `accesscodelabel` VARCHAR(20) NOT NULL DEFAULT '' AFTER `imageheader`,
				ADD `fontfamily` VARCHAR(128) NOT NULL DEFAULT '' AFTER `imageheader`
				;";

		$_pdoObj = dbconnection::getInstance();
		$pdoStatement = $_pdoObj->prepare($sql, array(PDO::ATTR_CURSOR => PDO::CURSOR_FWDONLY));
		$pdoStatement->execute(Array());
		if ($pdoStatement->errorCode() != 0) {
			echo 'SQL Fehler';
			print_r($pdoStatement->errorInfo());
			$errorCount += 1;
		}
		if ($errorCount == 0) {
			// update db version
			updateDbVersion($thisScriptDbVersion);
		} 
	}
} catch (Exception $e) {
	echo 'Datenbank-Fehler';
	print_r($e->getMessage());
}

