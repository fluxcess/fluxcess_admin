<?php
/**
 * Adds columns "showbrowserscanlink", "checkinhardlimit", "checkinlimitsource" to the event table
 * 17.08.2014
 */
$thisScriptDbVersion = 16;

include_once('../conf/config.php');

try {
	$errorCount = 0;
	// only update if this is the very next update.
	if ($currentDbVersion == $thisScriptDbVersion - 1) {
		// change event table
		echo "Adding columns to event table (script ".$thisScriptDbVersion.")...\n";
		$sql = "ALTER TABLE `fevent`
				ADD `checkinhardlimit` INT(1) NOT NULL DEFAULT 0 AFTER `registerbuttonlabel`,
				ADD `checkinlimitsource` INT(1) NOT NULL DEFAULT 0 AFTER `checkinhardlimit`,
				ADD `showbrowserscanlink` INT(1) NOT NULL DEFAULT 1 AFTER `checkinlimitsource`
				;";

		$_pdoObj = dbconnection::getInstance();
		$pdoStatement = $_pdoObj->prepare($sql, array(PDO::ATTR_CURSOR => PDO::CURSOR_FWDONLY));
		$pdoStatement->execute(Array());
		if ($pdoStatement->errorCode() != 0) {
			echo 'SQL Fehler';
			print_r($pdoStatement->errorInfo());
			$errorCount += 1;
		}
		if ($errorCount == 0) {
			// update db version
			updateDbVersion($thisScriptDbVersion);
		} 
	}
} catch (Exception $e) {
	echo 'Datenbank-Fehler';
	print_r($e->getMessage());
}

