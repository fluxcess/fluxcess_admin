<?php
/**
 * add columns to the fevset_* table of each event:
 * - skey_multiname
 * - skey_multiseq
 * - skey_multikey
 * - skey_multiid
 * this is for easier sorting and deletion of multisetting entries 
 * 
 * 05.06.2016
 */
$thisScriptDbVersion = 44;

include_once ('../conf/config.php');

try {
	$errorCount = 0;
	// only update if this is the very next update.
	if ($currentDbVersion == $thisScriptDbVersion - 1) {
		$_pdoObj = dbconnection::getInstance();
		
		$_pdoObj->beginTransaction();
		
		try {
			// find all guest tables
			$sql = "SELECT fevent_id FROM `fevent`;";
			$pdoStatementF = $_pdoObj->prepare($sql, array (
					PDO::ATTR_CURSOR => PDO::CURSOR_FWDONLY 
			));
			$pdoStatementF->execute(Array ());
			if ($pdoStatementF->errorCode() != 0) {
				echo 'SQL Fehler (F)';
				print_r($pdoStatementF->errorInfo());
				$errorCount += 1;
			} else {
				while ( $row = $pdoStatementF->fetch() ) {
					$eventid = $row['fevent_id'];
					
					// change event table
					echo "Adding columns for event " . $eventid . " (script " . $thisScriptDbVersion . ")...\n";
					$sql = "ALTER TABLE `fevset_" . $eventid . "` 
							ADD `skey_multiname` VARCHAR(256) NOT NULL DEFAULT '' AFTER `skey`, 
							ADD `skey_multiseq` INT NOT NULL DEFAULT '0' AFTER `skey_multiname`, 
							ADD `skey_multikey` VARCHAR(256) NOT NULL DEFAULT '' AFTER `skey_multiseq`, 
							ADD `skey_multiid` VARCHAR(256) NOT NULL DEFAULT '' AFTER `skey_multikey`
							;";
					$pdoStatement = $_pdoObj->prepare($sql, array (
							PDO::ATTR_CURSOR => PDO::CURSOR_FWDONLY 
					));
					$pdoStatement->execute(Array ());
					if ($pdoStatement->errorCode() != 0) {
						echo 'SQL Fehler';
						print_r($pdoStatement->errorInfo());
						$errorCount += 1;
					}
					
					// fill new fields
					$sqlX = "SELECT * FROM fevset_" . $eventid . ";";
					$pdoStatementX = $_pdoObj->prepare($sqlX, array (
							PDO::ATTR_CURSOR => PDO::CURSOR_FWDONLY 
					));
					$pdoStatementX->execute(Array ());
					if ($pdoStatementX->errorCode() != 0) {
						echo 'SQL Fehler (X)';
						print_r($pdoStatementX->errorInfo());
						$errorCount += 1;
					}
					
					while ( $rowX = $pdoStatementX->fetch(PDO::FETCH_ASSOC) ) {
						$nmparts = explode('_', $rowX['skey']);
						if (count($nmparts) == 3) {
							// get id of current row
							$sqlG = "SELECT sval_vch FROM fevset_" . $eventid . " 
									WHERE skey = :skey;";
							switch ($nmparts[0]) {
								case 'optioncollection' :
								case 'optioncollectionvalue' :
								case 'conditionalautomail' :
								case 'conferencestream' :
								case 'conferencesession' :
								case 'ticketcategory' :
									$keyfield = 'id';
									break;
								case 'formpage' :
									$keyfield = 'pageid';
									break;
								case 'formpagesection' :
									$keyfield = 'sectionid';
									break;
								case 'formsectionfield' :
									$keyfield = 'fieldid';
									break;
							}
							$sqlArrG = array (
									':skey' => $nmparts[0] . '_' . $nmparts[1] . '_' . $keyfield 
							);
							$pdoStatementG = $_pdoObj->prepare($sqlG, array (
									PDO::ATTR_CURSOR => PDO::CURSOR_FWDONLY 
							));
							$pdoStatementG->execute($sqlArrG);
							if ($pdoStatementG->errorCode() != 0) {
								echo 'SQL Fehler (G)';
								print_r($pdoStatementG->errorInfo());
								$errorCount += 1;
							}
							$rowG = $pdoStatementG->fetch(PDO::FETCH_ASSOC);
							
							$key = $rowG['sval_vch'];
							while ( $key == '' ) {
								echo $sqlG;
								print_r($sqlArrG);
								print_r($rowG);
								
								echo "$rowX[skey] hat noch keine Id... generiere ";
								$key = sqlUpGenerateUniqueKey(8);
								echo "$key\n";
								// check if key exists already
								$sqlE = 'SELECT * FROM fevset_' . $eventid . ' 
										WHERE SUBSTRING(skey, 0, ' . (strlen($nmparts[0]) + 1) . ') = "' . $nmparts[0] . '_' . '"
										AND SUBSTRING(skey, ' . (strlen($nmparts[0]) + 2) . ') = "' . $keyfield . '"
										AND sval_vch = :key;';
								$pdoStatementE = $_pdoObj->prepare($sqlE, array (
										PDO::ATTR_CURSOR => PDO::CURSOR_FWDONLY 
								));
								$pdoStatementE->execute(Array (
										':key' => $key 
								));
								if ($pdoStatementE->errorCode() != 0) {
									echo 'SQL Fehler (E)';
									print_r($pdoStatementE->errorInfo());
									$errorCount += 1;
								} elseif ($pdoStatementE->rowCount() > 0) {
									$key = '';
								} else {
									// if it does not exist yet, we write it to the key column of 
									// this multisetting id
									echo "writing ".$key." to column ".$nmparts[0].'_'.$nmparts[1].'_'.$keyfield."\n";
									$sqlEU = 'INSERT INTO fevset_' . $eventid . ' (sval_vch, skey, skey_multiname, skey_multiseq, skey_multikey, skey_multiid)
											VALUES (:val, :keyname, :multiname, :multiseq, :multikey, :val)
											ON DUPLICATE KEY UPDATE sval_vch = :val;';
									$pdoStatementEU = $_pdoObj->prepare($sqlEU, array (
											PDO::ATTR_CURSOR => PDO::CURSOR_FWDONLY 
									));
									$pdoStatementEU->execute(Array (
											':val' => $key,
											':keyname' => $nmparts[0] . '_' . $nmparts[1] . '_' . $keyfield,
											':multiname' => $nmparts[0],
											':multiseq' => $nmparts[1],
											':multikey' => $nmparts[2]
									));
									if ($pdoStatementEU->errorCode() != 0) {
										echo 'SQL Fehler (EU)';
										print_r($pdoStatementEU->errorInfo());
										$errorCount += 1;
									}
								}
							}
							
							// update current row
							$sqlY = "UPDATE fevset_" . $eventid . "
							SET `skey_multiname` = :multiname,
							`skey_multiseq` = :multiseq,
							`skey_multikey` = :multikey,
							`skey_multiid` = :multiid
							WHERE skey = :skey
							;";
							$sqlArrY = array (
									':multiname' => $nmparts[0],
									':multiseq' => $nmparts[1],
									':multikey' => $nmparts[2],
									':multiid' => $key,
									':skey' => $nmparts[0] . '_' . $nmparts[1] . '_' . $nmparts[2] 
							);
							$pdoStatementY = $_pdoObj->prepare($sqlY, array (
									PDO::ATTR_CURSOR => PDO::CURSOR_FWDONLY 
							));
							$pdoStatementY->execute($sqlArrY);
							if ($pdoStatementY->errorCode() != 0) {
								echo 'SQL Fehler Y';
								print_r($pdoStatementY->errorInfo());
								$errorCount += 1;
							}
						}
					}
				}
			}
			if ($errorCount == 0) {
				// update db version
				updateDbVersion($thisScriptDbVersion);
				$_pdoObj->commit();
			} else {
				$_pdoObj->rollBack();
			}
		} catch ( Exception $e ) {
			$_pdoObj->rollBack();
			echo "Innerer Datenbank-Fehler";
			print_r($e->getMessage());
		}
	}
} catch ( Exception $e ) {
	echo 'Datenbank-Fehler';
	print_r($e->getMessage());
}
