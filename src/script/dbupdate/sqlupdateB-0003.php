<?php
/**
 * Adds table "domain" to the database
 * 14.-18.08.2013
 */
$thisScriptDbVersion = 3;

include_once('../conf/config.php');

try {
	$errorCount = 0;
	// only update if this is the very next update.
	if ($currentDbVersion == $thisScriptDbVersion - 1) {
		// change event table
		echo "Updating to db version $thisScriptDbVersion: Adding table 'domain' ...\n";
		$sql = "CREATE TABLE IF NOT EXISTS `domain` (
				`domain_id` int(11) NOT NULL AUTO_INCREMENT,
				`domaintype` tinyint(1) NOT NULL DEFAULT '0',
				`domainname` varchar(60) DEFAULT NULL,
				`urlpath` varchar(120) DEFAULT NULL,
				`fevent_id` int(11) NOT NULL,
				PRIMARY KEY (`domain_id`),
				KEY `fevent_id` (`fevent_id`)
				) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;";

		$_pdoObj = dbconnection::getInstance();
		$pdoStatement = $_pdoObj->prepare($sql, array(PDO::ATTR_CURSOR => PDO::CURSOR_FWDONLY));
		$pdoStatement->execute(Array());
		if ($pdoStatement->errorCode() != 0) {
			print_r($pdoStatement->errorInfo());
			echo 'FEHLER!';
			$errorCount += 1;
		}
		if ($errorCount == 0) {
			// update db version
			updateDbVersion($thisScriptDbVersion);
		}
	}
} catch (Exception $e) {
	echo 'Datenbank-Fehler';
	print_r($e->getMessage(), true);
}

