<?php
/**
 * adds massmail tables to all events (wemail)
 *
 * 26.11.2014
 */
$thisScriptDbVersion = 19;

include_once('../conf/config.php');

try {
	$errorCount = 0;
	// only update if this is the very next update.
	if ($currentDbVersion == $thisScriptDbVersion - 1) {
		$_pdoObj = dbconnection::getInstance();

		$_pdoObj->beginTransaction();

		try {
			// find all guest tables
			$sql = "SELECT fevent_id FROM `fevent`
					ORDER BY fevent_id ASC;";
			$pdoStatementF = $_pdoObj->prepare($sql, array(PDO::ATTR_CURSOR => PDO::CURSOR_FWDONLY));
			$pdoStatementF->execute(Array());
			if ($pdoStatementF->errorCode() != 0) {
				echo 'SQL Fehler';
				print_r($pdoStatementF->errorInfo());
				$errorCount += 1;
			} else {
				while ($row = $pdoStatementF->fetch()) {
					$eventid = $row['fevent_id'];

					// change event table
					echo "Adding table for event ".$eventid." (script ".$thisScriptDbVersion.")...\n";
					$sql = "CREATE TABLE IF NOT EXISTS `wemail".$eventid."` (
  							`email_id` int(11) NOT NULL AUTO_INCREMENT,
  							`subject` varchar(255) NOT NULL,
  							`mailtext` text NOT NULL,
  							`sendstatus` tinyint(1) NOT NULL DEFAULT '0',
  							`massmailrecipients` set('1','2','3','4','5') DEFAULT NULL,
  							`deleted` tinyint(1) NOT NULL DEFAULT '0',
  							PRIMARY KEY (`email_id`)
							) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;
							";

					$pdoStatement = $_pdoObj->prepare($sql, array(PDO::ATTR_CURSOR => PDO::CURSOR_FWDONLY));
					$pdoStatement->execute(Array());
					if ($pdoStatement->errorCode() != 0) {
						echo 'SQL Fehler';
						print_r($pdoStatement->errorInfo());
						$errorCount += 1;
					}
				}
			}
			if ($errorCount == 0) {
				// update db version
				updateDbVersion($thisScriptDbVersion);
				$_pdoObj->commit();
			} else {
				$_pdoObj->rollBack();
			}
		} catch (Exception $e) {
			$_pdoObj->rollBack();
			echo "Innerer Datenbank-Fehler";
			print_r($e->getMessage());
		}
	}
} catch (Exception $e) {
	echo 'Datenbank-Fehler';
	print_r($e->getMessage());
}

