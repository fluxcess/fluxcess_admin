<?php
/**
 * Deletes column "deletedtime" from those event column tables where it is still part of
 * Bugfix!
 * 02.05.2015
 */
$thisScriptDbVersion = 29;

include_once ('../conf/config.php');

try {
	$errorCount = 0;
	// only update if this is the very next update.
	if ($currentDbVersion == $thisScriptDbVersion - 1) {
		// change event table
		echo "Deletes column ~deletedtime~ from those event column tables where it is still part of (script " . $thisScriptDbVersion . ")...\n";
		
		$_pdoObj = dbconnection::getInstance();
		
		$_pdoObj->beginTransaction();
		
		// find all relevant column tables
		$sql = 'SELECT fevent_id FROM `fevent`
					WHERE createdtime < "2014-01-01"
					ORDER BY fevent_id ASC;';
		$pdoStatementF = $_pdoObj->prepare($sql, array (
				PDO::ATTR_CURSOR => PDO::CURSOR_FWDONLY 
		));
		$pdoStatementF->execute(Array ());
		if ($pdoStatementF->errorCode() != 0) {
			echo 'SQL Fehler';
			print_r($pdoStatementF->errorInfo());
			$errorCount += 1;
		} else {
			while ($row = $pdoStatementF->fetch()) {
				$eventid = $row['fevent_id'];
				
				// change event table
				echo "Deleting columns from zcolumn table for event " . $eventid . " (script " . $thisScriptDbVersion . ")...\n";
				$sql = "DELETE FROM `zcolumn" . $eventid . "`
							WHERE fieldname = 'deletedtime'
							;";
				
				$pdoStatement = $_pdoObj->prepare($sql, array (
						PDO::ATTR_CURSOR => PDO::CURSOR_FWDONLY 
				));
				$pdoStatement->execute(Array ());
				if ($pdoStatement->errorCode() != 0) {
					echo 'SQL Fehler';
					print_r($pdoStatement->errorInfo());
					$errorCount += 1;
				}
			}
		}
		
		if ($errorCount == 0) {
			// update db version
			updateDbVersion($thisScriptDbVersion);
			$_pdoObj->commit();
		} else {
			$_pdoObj->rollBack();
		}
	}
} catch (Exception $e) {
	echo 'Datenbank-Fehler';
	print_r($e->getMessage());
}

