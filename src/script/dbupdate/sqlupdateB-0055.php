<?php
/**
 * modify fevset table of each event:
 * add 3 parent columns, so that every 
 * setting can have multiple child settings
 * 09.11.2016
 */
$thisScriptDbVersion = 55;

include_once ('../conf/config.php');

try {
	$errorCount = 0;
	// only update if this is the very next update.
	if ($currentDbVersion == $thisScriptDbVersion - 1) {
		$_pdoObj = dbconnection::getInstance();
		
		$_pdoObj->beginTransaction();
		
		try {
			// find all guest tables
			$sql = "SELECT fevent_id FROM `fevent`
					ORDER BY fevent_id ASC;";
			$pdoStatementF = $_pdoObj->prepare($sql, array (
					PDO::ATTR_CURSOR => PDO::CURSOR_FWDONLY 
			));
			$pdoStatementF->execute(Array ());
			if ($pdoStatementF->errorCode() != 0) {
				echo 'SQL Fehler';
				print_r($pdoStatementF->errorInfo());
				$errorCount += 1;
			} else {
				while ( $row = $pdoStatementF->fetch() ) {
					$eventid = $row['fevent_id'];
					
					// change event table
					echo "adding new settings columns for event " . $eventid . " (script " . $thisScriptDbVersion . ")...\n";
					$sql = "ALTER TABLE `fevset_".$eventid."` 
							ADD `pskey_multiname` VARCHAR(256) NULL DEFAULT NULL AFTER `skey_multiid`, 
							ADD `pskey_multikey` VARCHAR(256) NULL DEFAULT NULL AFTER `pskey_multiname`, 
							ADD `pskey_multiid` VARCHAR(256) NULL DEFAULT NULL AFTER `pskey_multikey`;;";
					try {
					$pdoStatement = $_pdoObj->prepare($sql, array (
							PDO::ATTR_CURSOR => PDO::CURSOR_FWDONLY 
					));
					$pdoStatement->execute(Array ());
					if ($pdoStatement->errorCode() != 0) {
						echo 'SQL Fehler';
						print_r($pdoStatement->errorInfo());
						$errorCount += 1;
					}
					} catch (Exception $e) {
						
					}
				}
			}
			if ($errorCount == 0) {
				// update db version
				updateDbVersion($thisScriptDbVersion);
				$_pdoObj->commit();
			} else {
				$_pdoObj->rollBack();
			}
		} catch ( Exception $e ) {
			$_pdoObj->rollBack();
			echo "Innerer Datenbank-Fehler";
			print_r($e->getMessage());
		}
	}
} catch ( Exception $e ) {
	echo 'Datenbank-Fehler';
	print_r($e->getMessage());
}

