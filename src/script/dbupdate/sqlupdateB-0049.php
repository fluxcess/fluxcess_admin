<?php
/**
 * again, because it had been missing in the normal creation script:
 * change suppdata column of the xvl table of each event to MEDIUMTEXT
 * this column is intended for storing entire email message bodies now.
 * 16 MB is good for the start. 
 * 
 * 04.08.2016
 */
$thisScriptDbVersion = 49;

include_once ('../conf/config.php');

try {
	$errorCount = 0;
	// only update if this is the very next update.
	if ($currentDbVersion == $thisScriptDbVersion - 1) {
		$_pdoObj = dbconnection::getInstance();
		
		$_pdoObj->beginTransaction();
		
		try {
			// find all guest tables
			$sql = "SELECT fevent_id FROM `fevent`
					ORDER BY fevent_id ASC;";
			$pdoStatementF = $_pdoObj->prepare($sql, array (
					PDO::ATTR_CURSOR => PDO::CURSOR_FWDONLY 
			));
			$pdoStatementF->execute(Array ());
			if ($pdoStatementF->errorCode() != 0) {
				echo 'SQL Fehler';
				print_r($pdoStatementF->errorInfo());
				$errorCount += 1;
			} else {
				while ( $row = $pdoStatementF->fetch() ) {
					$eventid = $row['fevent_id'];
					
					// change event table
					echo "Modifying 'suppdata' column for event " . $eventid . " (script " . $thisScriptDbVersion . ")...\n";
					$sql = "ALTER TABLE `xvl" . $eventid . "` CHANGE `suppdata` `suppdata` MEDIUMTEXT CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL;";
					$pdoStatement = $_pdoObj->prepare($sql, array (
							PDO::ATTR_CURSOR => PDO::CURSOR_FWDONLY 
					));
					$pdoStatement->execute(Array ());
					if ($pdoStatement->errorCode() != 0) {
						echo 'SQL Fehler';
						print_r($pdoStatement->errorInfo());
						$errorCount += 1;
					}
				}
			}
			if ($errorCount == 0) {
				// update db version
				updateDbVersion($thisScriptDbVersion);
				$_pdoObj->commit();
			} else {
				$_pdoObj->rollBack();
			}
		} catch ( Exception $e ) {
			$_pdoObj->rollBack();
			echo "Innerer Datenbank-Fehler";
			print_r($e->getMessage());
		}
	}
} catch ( Exception $e ) {
	echo 'Datenbank-Fehler';
	print_r($e->getMessage());
}

