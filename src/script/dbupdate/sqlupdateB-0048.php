<?php
/**
 * Add fcustomer column to the fevent table
 * as we are going to have multiple users per customer (company) in the future,
 * it is necessary to specify a customer id for each event.  
 * 
 * We also add the customer id to each event, because events without 
 * customer id should not exist.
 * 
 * 02.08.2016
 */
$thisScriptDbVersion = 48;

include_once ('../conf/config.php');

try {
	$errorCount = 0;
	// only update if this is the very next update.
	if ($currentDbVersion == $thisScriptDbVersion - 1) {
		$_pdoObj = dbconnection::getInstance();
		
		$_pdoObj->beginTransaction();
		
		try {
			// change user table
			echo "Adding 'fcustomer' columns (script " . $thisScriptDbVersion . ")...\n";
			$sql = "ALTER TABLE `fevent` 
					ADD `fcustomer_id` INT(11) NOT NULL DEFAULT '0' AFTER `fuser_id`;";
			$pdoStatement = $_pdoObj->prepare($sql, array (
					PDO::ATTR_CURSOR => PDO::CURSOR_FWDONLY 
			));
			$pdoStatement->execute(Array ());
			if ($pdoStatement->errorCode() != 0) {
				echo 'SQL Fehler';
				print_r($pdoStatement->errorInfo());
				$errorCount += 1;
			}
			
			echo "Filling the new 'fcustomer' column (script " . $thisScriptDbVersion . ")...\n";
			$sql = "UPDATE fevent fev
					SET fev.fcustomer_id = (
						SELECT fcu.fcustomer_id
						FROM fcustomer_fuser fcu
						WHERE fuser_id = fev.fuser_id
					);";
			$pdoStatement = $_pdoObj->prepare($sql, array (
					PDO::ATTR_CURSOR => PDO::CURSOR_FWDONLY
			));
			$pdoStatement->execute(Array ());
			if ($pdoStatement->errorCode() != 0) {
				echo 'SQL Fehler';
				print_r($pdoStatement->errorInfo());
				$errorCount += 1;
			}
				
			if ($errorCount == 0) {
				// update db version
				updateDbVersion($thisScriptDbVersion);
				$_pdoObj->commit();
			} else {
				$_pdoObj->rollBack();
			}
		} catch ( Exception $e ) {
			$_pdoObj->rollBack();
			echo "Innerer Datenbank-Fehler";
			print_r($e->getMessage());
		}
	}
} catch ( Exception $e ) {
	echo 'Datenbank-Fehler';
	print_r($e->getMessage());
}

