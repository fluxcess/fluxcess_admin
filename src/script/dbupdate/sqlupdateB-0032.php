<?php
/**
 * adds exttype column to extension table
*
* 25.10.2015
*/
$thisScriptDbVersion = 32;

include_once('../conf/config.php');

try {
	$errorCount = 0;
	// only update if this is the very next update.
	if ($currentDbVersion == $thisScriptDbVersion - 1) {
		$_pdoObj = dbconnection::getInstance();

		$_pdoObj->beginTransaction();

		try {
			// find all guest tables
			$sql = "ALTER TABLE `extension` ADD `exttype` ENUM('regprocstep','auth') NULL DEFAULT NULL AFTER `extension_id`;";
			$pdoStatementF = $_pdoObj->prepare($sql, array(PDO::ATTR_CURSOR => PDO::CURSOR_FWDONLY));
			$pdoStatementF->execute(Array());
			if ($pdoStatementF->errorCode() != 0) {
				echo 'SQL Fehler';
				print_r($pdoStatementF->errorInfo());
				$errorCount += 1;
			} 
			if ($errorCount == 0) {
				// update db version
				updateDbVersion($thisScriptDbVersion);
				$_pdoObj->commit();
			} else {
				$_pdoObj->rollBack();
			}
		} catch (Exception $e) {
			$_pdoObj->rollBack();
			echo "Innerer Datenbank-Fehler";
			print_r($e->getMessage());
		}
	}
} catch (Exception $e) {
	echo 'Datenbank-Fehler';
	print_r($e->getMessage());
}

