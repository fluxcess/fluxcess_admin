<?php
/**
 * adds global customer table
 * adds global user_customer table
 * adds one customer entry for each user
 * 
 * later, it should be possible to add items n:m
 *
 * 23.02.2016
 */
$thisScriptDbVersion = 33;

include_once('../conf/config.php');

try {
	$errorCount = 0;
	// only update if this is the very next update.
	if ($currentDbVersion == $thisScriptDbVersion - 1) {
		$_pdoObj = dbconnection::getInstance();

		$_pdoObj->beginTransaction();

		try {
			// add customer table
			$sql = "CREATE TABLE IF NOT EXISTS `fcustomer` (
				`fcustomer_id` int(11) NOT NULL AUTO_INCREMENT,
  				`fcustomer_name` varchar(120) CHARACTER SET latin1 NOT NULL,
				PRIMARY KEY (`fcustomer_id`)
				) ENGINE=InnoDB DEFAULT CHARSET=utf8;";
			$pdoStatementF = $_pdoObj->prepare($sql, array(PDO::ATTR_CURSOR => PDO::CURSOR_FWDONLY));
			$pdoStatementF->execute(Array());
				
			// add n:m table
			$sql = "CREATE TABLE IF NOT EXISTS `fcustomer_fuser` (
  				`fcustomer_id` int(11) NOT NULL,
  				`fuser_id` int(11) NOT NULL,
				PRIMARY KEY (`fcustomer_id`, `fuser_id`)
				) ENGINE=InnoDB DEFAULT CHARSET=utf8;";
			$pdoStatementF = $_pdoObj->prepare($sql, array(PDO::ATTR_CURSOR => PDO::CURSOR_FWDONLY));
			$pdoStatementF->execute(Array());
				
			// find all users
			$sql = "SELECT * FROM `fuser`
					ORDER BY fuser_id ASC;";
			$pdoStatementF = $_pdoObj->prepare($sql, array(PDO::ATTR_CURSOR => PDO::CURSOR_FWDONLY));
			$pdoStatementF->execute(Array());
			if ($pdoStatementF->errorCode() != 0) {
				echo 'SQL Fehler';
				print_r($pdoStatementF->errorInfo());
				$errorCount += 1;
			} else {
				while ($row = $pdoStatementF->fetch()) {
					$userid = $row['fuser_id'];

					// add customer
					echo "Adding customer for user ".$userid ." (script ".$thisScriptDbVersion.")...\n";
					$sql = "INSERT INTO fcustomer SET fcustomer_id = ".$userid.", fcustomer_name = '".$row['email']."';";

					$pdoStatement = $_pdoObj->prepare($sql, array(PDO::ATTR_CURSOR => PDO::CURSOR_FWDONLY));
					$pdoStatement->execute(Array());
					if ($pdoStatement->errorCode() != 0) {
						echo 'SQL Fehler';
						print_r($pdoStatement->errorInfo());
						$errorCount += 1;
					}
					
					// add user to customer
					echo "Adding customer to user ".$userid ." (script ".$thisScriptDbVersion.")...\n";
					$sql = "INSERT INTO fcustomer_fuser SET fcustomer_id = ".$userid.", fuser_id = '".$userid."';";
					
					$pdoStatement = $_pdoObj->prepare($sql, array(PDO::ATTR_CURSOR => PDO::CURSOR_FWDONLY));
					$pdoStatement->execute(Array());
					if ($pdoStatement->errorCode() != 0) {
						echo 'SQL Fehler';
						print_r($pdoStatement->errorInfo());
						$errorCount += 1;
					}
				}
			}
			if ($errorCount == 0) {
				// update db version
				updateDbVersion($thisScriptDbVersion);
				$_pdoObj->commit();
			} else {
				$_pdoObj->rollBack();
			}
		} catch (Exception $e) {
			$_pdoObj->rollBack();
			echo "Innerer Datenbank-Fehler";
			print_r($e->getMessage());
		}
	}
} catch (Exception $e) {
	echo 'Datenbank-Fehler';
	print_r($e->getMessage());
}

