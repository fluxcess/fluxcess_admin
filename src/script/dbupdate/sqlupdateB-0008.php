<?php
/**
 * Adds columns "organisermailcontent", "organisermailto" and "organisermailsubject" to the event table
 * 21.11.2013
 */
$thisScriptDbVersion = 8;

include_once('../conf/config.php');

try {
	$errorCount = 0;
	// only update if this is the very next update.
	if ($currentDbVersion == $thisScriptDbVersion - 1) {
		// change event table
		echo "Adding columns to event table (script ".$thisScriptDbVersion.")...\n";
		$sql = "ALTER TABLE `fevent`
				ADD `organisermailcontent` TEXT AFTER `acceptmailsubject`,
				ADD `organisermailsubject` VARCHAR(128) NOT NULL DEFAULT '' AFTER `organisermailcontent`,
				ADD `organisermailto` VARCHAR(128) NOT NULL DEFAULT '' AFTER `organisermailsubject`
				;";

		$_pdoObj = dbconnection::getInstance();
		$pdoStatement = $_pdoObj->prepare($sql, array(PDO::ATTR_CURSOR => PDO::CURSOR_FWDONLY));
		$pdoStatement->execute(Array());
		if ($pdoStatement->errorCode() != 0) {
			echo 'SQL Fehler';
			print_r($pdoStatement->errorInfo());
			$errorCount += 1;
		}
		if ($errorCount == 0) {
			// update db version
			updateDbVersion($thisScriptDbVersion);
		} 
	}
} catch (Exception $e) {
	echo 'Datenbank-Fehler';
	print_r($e->getMessage());
}

