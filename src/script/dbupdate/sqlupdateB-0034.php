<?php
/**
 * adds global emailtemplate table
 * 
 * 02.03.2016
 */
$thisScriptDbVersion = 34;

include_once('../conf/config.php');

try {
	$errorCount = 0;
	// only update if this is the very next update.
	if ($currentDbVersion == $thisScriptDbVersion - 1) {
		$_pdoObj = dbconnection::getInstance();

		$_pdoObj->beginTransaction();

		try {
			// add emailtemplate table
			$sql = "CREATE TABLE `emailtemplate` (
  				`emailtemplate_id` int(11) NOT NULL AUTO_INCREMENT,
  				`fcustomer_id` int(11) NOT NULL DEFAULT '0',
  				`title` varchar(128) NOT NULL,
  				`description` text NOT NULL,
  				`content` mediumtext NOT NULL,
  				`created_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  				`created_fuser_id` int(11) NOT NULL,
  				`modified_time` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  				`modified_fuser_id` int(11) NOT NULL,
  				PRIMARY KEY (`emailtemplate_id`)
				) ENGINE=InnoDB DEFAULT CHARSET=utf8;";
			$pdoStatementF = $_pdoObj->prepare($sql, array(PDO::ATTR_CURSOR => PDO::CURSOR_FWDONLY));
			$pdoStatementF->execute(Array());
				
			if ($pdoStatementF->errorCode() != 0) {
				echo 'SQL Fehler';
				print_r($pdoStatementF->errorInfo());
				$errorCount += 1;
			} 
			if ($errorCount == 0) {
				// update db version
				updateDbVersion($thisScriptDbVersion);
				$_pdoObj->commit();
			} else {
				$_pdoObj->rollBack();
			}
		} catch (Exception $e) {
			$_pdoObj->rollBack();
			echo "Innerer Datenbank-Fehler";
			print_r($e->getMessage());
		}
	}
} catch (Exception $e) {
	echo 'Datenbank-Fehler';
	print_r($e->getMessage());
}
