<?php
/**
 * Changes all guest tables:
 * - removes auto-increment and unique properties
 * - adds column row_id with auto-increment
 * - adds column deleted, predefault value 0
 * - adds column archived, predefault value 0
 *
 * 22.12.2013
 */
$thisScriptDbVersion = 9;

include_once('../conf/config.php');

try {
	$errorCount = 0;
	// only update if this is the very next update.
	if ($currentDbVersion == $thisScriptDbVersion - 1) {
		$_pdoObj = dbconnection::getInstance();

		$_pdoObj->beginTransaction();

		try {
			// find all guest tables
			$sql = "SELECT fevent_id FROM `fevent`
					ORDER BY fevent_id ASC;";
			$pdoStatementF = $_pdoObj->prepare($sql, array(PDO::ATTR_CURSOR => PDO::CURSOR_FWDONLY));
			$pdoStatementF->execute(Array());
			if ($pdoStatementF->errorCode() != 0) {
				echo 'SQL Fehler';
				print_r($pdoStatementF->errorInfo());
				$errorCount += 1;
			} else {
				while ($row = $pdoStatementF->fetch()) {
					$eventid = $row['fevent_id'];
						
					// change event table
					echo "Changing table for event ".$eventid." (script ".$thisScriptDbVersion.")...\n";
					$sql = "ALTER TABLE `zguest".$eventid."`
							CHANGE `guest_id` `guest_id` INT( 11 ) NOT NULL,
							DROP PRIMARY KEY,
							ADD `row_id` INT NOT NULL AUTO_INCREMENT PRIMARY KEY FIRST,
							DROP `deletedtime`,
							ADD `deleted` TINYINT( 1 ) NOT NULL DEFAULT '0' AFTER `fevent_id` ,
							ADD `archived` TINYINT( 1 ) NOT NULL DEFAULT '0' AFTER `deleted` ,
							ADD INDEX ( `deleted` , `archived`)
							;";

					$pdoStatement = $_pdoObj->prepare($sql, array(PDO::ATTR_CURSOR => PDO::CURSOR_FWDONLY));
					$pdoStatement->execute(Array());
					if ($pdoStatement->errorCode() != 0) {
						echo 'SQL Fehler';
						print_r($pdoStatement->errorInfo());
						$errorCount += 1;
					}
				}
			}
			if ($errorCount == 0) {
				// update db version
				updateDbVersion($thisScriptDbVersion);
				$_pdoObj->commit();
			} else {
				$_pdoObj->rollBack();
			}
		} catch (Exception $e) {
			$_pdoObj->rollBack();
			echo "Innerer Datenbank-Fehler";
			print_r($e->getMessage());
		}
	}
} catch (Exception $e) {
	echo 'Datenbank-Fehler';
	print_r($e->getMessage());
}

