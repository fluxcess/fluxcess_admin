<?php
/**
 * re-attach forms to pages
 * from old way to new way
 * 12.04.2017
 */
$thisScriptDbVersion = 59;

include_once ('../conf/config.php');

try {
	$errorCount = 0;
	// only update if this is the very next update.
	if ($currentDbVersion == $thisScriptDbVersion - 1) {
		$_pdoObj = dbconnection::getInstance();
		
		$_pdoObj->beginTransaction();
		
		try {
			session_start();
			// find all event tables
			$sql = "SELECT fevent_id, fuser_id, fcustomer_id FROM `fevent`
					ORDER BY fevent_id ASC;;";
			$pdoStatementF = $_pdoObj->prepare($sql, array (
					PDO::ATTR_CURSOR => PDO::CURSOR_FWDONLY 
			));
			$pdoStatementF->execute(Array ());
			if ($pdoStatementF->errorCode() != 0) {
				echo 'SQL Fehler';
				print_r($pdoStatementF->errorInfo());
				$errorCount += 1;
			} else {
				while ( $row = $pdoStatementF->fetch() ) {
					$feventid = $row['fevent_id'];
					$_SESSION['user']['id'] = $row['fuser_id'];
					$_SESSION['customer']['customer_id'] = $row['fcustomer_id'];
					
					echo 'FEVENT_ID: ' . $feventid . PHP_EOL;
					
					// change multisettings...
					try {
						$sql = 'SELECT * FROM fevset_' . $feventid . '
								WHERE 
									skey_multiname = "formpagesection"
									AND skey_multikey = "formpageid"
									ORDER BY skey_multiseq;';
						echo $sql . PHP_EOL;
						try {
							$pdoStatementU = $_pdoObj->prepare($sql, array (
									PDO::ATTR_CURSOR => PDO::CURSOR_FWDONLY 
							));
							$pdoStatementU->execute(Array ());
							if ($pdoStatementU->errorCode() != 0) {
								echo 'SQL Fehler';
								print_r($pdoStatemenU->errorInfo());
								$errorCount += 1;
							} else {
								while ( $row = $pdoStatementU->fetch() ) {
									// read all occurences
									// one by one:
									// attach the right way
									$data = array();
									$data['parentmultiname'] = 'formpage';
									$data['parentmultiid'] = $row['sval_vch'];
									$data['multiname'] = 'n1formpagesectiontoformpage';
									$data['multisubsetting'] = ''; // not used in method
									$data['val'] = $row['skey_multiid'];
									$data['parentkey'] = ''; // n1formpagesectiontoformpage';
									echo "Daten einfügen:::";
									print_r($data);
									$m = new multisettings();
									$m->setRecordId($feventid);
									$m->addMultisettings1NRecord($feventid, $data);
								}
								// for all: delete the old way
								$sql = 'DELETE FROM fevset_' . $feventid . '
									WHERE
									skey_multiname = "formpagesection"
									AND skey_multikey = "formpageid";';
								echo $sql;
								$pdoStatementD = $_pdoObj->prepare($sql, array (
										PDO::ATTR_CURSOR => PDO::CURSOR_FWDONLY 
								));
								$pdoStatementD->execute(Array ());
							}
						} catch ( Exception $e ) {
							print_r($e);
						}
					} catch ( Exception $e ) {
						// We got an exception == table not found
						print_r($e->getMessage());
					}
				}
			}
			if ($errorCount == 0) {
				// update db version
				updateDbVersion($thisScriptDbVersion);
				$_pdoObj->commit();
			} else {
				$_pdoObj->rollBack();
			}
		} catch ( Exception $e ) {
			$_pdoObj->rollBack();
			echo "Innerer Datenbank-Fehler";
			print_r($e->getMessage());
		}
	}
} catch ( Exception $e ) {
	echo 'Datenbank-Fehler';
	print_r($e->getMessage());
}

