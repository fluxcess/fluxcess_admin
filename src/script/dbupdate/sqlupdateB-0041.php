<?php
/**
 * add "sval_dec" column to the evset_ table of each event
 * this column is intended for adding "decimal" values,
 * like prices
 * 
 * 07.05.2016
 */
$thisScriptDbVersion = 41;

include_once ('../conf/config.php');

try {
	$errorCount = 0;
	// only update if this is the very next update.
	if ($currentDbVersion == $thisScriptDbVersion - 1) {
		$_pdoObj = dbconnection::getInstance();
		
		$_pdoObj->beginTransaction();
		
		try {
			// find all guest tables
			$sql = "SELECT fevent_id FROM `fevent`
					ORDER BY fevent_id ASC;";
			$pdoStatementF = $_pdoObj->prepare($sql, array (
					PDO::ATTR_CURSOR => PDO::CURSOR_FWDONLY 
			));
			$pdoStatementF->execute(Array ());
			if ($pdoStatementF->errorCode() != 0) {
				echo 'SQL Fehler';
				print_r($pdoStatementF->errorInfo());
				$errorCount += 1;
			} else {
				while ( $row = $pdoStatementF->fetch() ) {
					$eventid = $row['fevent_id'];
					
					// change event table
					echo "Adding 'sval_dec' column for event " . $eventid . " (script " . $thisScriptDbVersion . ")...\n";
					$sql = "ALTER TABLE `fevset_" . $eventid . "` ADD `sval_dec` DECIMAL(10,2) NULL DEFAULT NULL ;";
					$pdoStatement = $_pdoObj->prepare($sql, array (
							PDO::ATTR_CURSOR => PDO::CURSOR_FWDONLY 
					));
					$pdoStatement->execute(Array ());
					if ($pdoStatement->errorCode() != 0) {
						echo 'SQL Fehler';
						print_r($pdoStatement->errorInfo());
						$errorCount += 1;
					}
				}
			}
			if ($errorCount == 0) {
				// update db version
				updateDbVersion($thisScriptDbVersion);
				$_pdoObj->commit();
			} else {
				$_pdoObj->rollBack();
			}
		} catch ( Exception $e ) {
			$_pdoObj->rollBack();
			echo "Innerer Datenbank-Fehler";
			print_r($e->getMessage());
		}
	}
} catch ( Exception $e ) {
	echo 'Datenbank-Fehler';
	print_r($e->getMessage());
}

