<?php
/**
 * adds email template table for each _customer_
 * copied from script number 38, because the creation script was 
 * missing for newly created customers/events
 * 01.08.2016
 */
$thisScriptDbVersion = 47;

include_once('../conf/config.php');

try {
	$errorCount = 0;
	// only update if this is the very next update.
	if ($currentDbVersion == $thisScriptDbVersion - 1) {
		$_pdoObj = dbconnection::getInstance();

		$_pdoObj->beginTransaction();

		try {
			// find all customers
			$sql = "SELECT fcustomer_id FROM `fcustomer`
					ORDER BY fcustomer_id ASC;";
			$pdoStatementF = $_pdoObj->prepare($sql, array(PDO::ATTR_CURSOR => PDO::CURSOR_FWDONLY));
			$pdoStatementF->execute(Array());
			if ($pdoStatementF->errorCode() != 0) {
				echo 'SQL Fehler';
				print_r($pdoStatementF->errorInfo());
				$errorCount += 1;
			} else {
				while ($row = $pdoStatementF->fetch()) {
					$customerid = $row['fcustomer_id'];

					// add emailtemplate table
					echo "Adding 'emailtemplate' table for customer ".$customerid." (script ".$thisScriptDbVersion.")...\n";
					$sql = "CREATE TABLE IF NOT EXISTS `emailtemplate_".$customerid."` (
  				`emailtemplate_id` int(11) NOT NULL AUTO_INCREMENT,
  				`fcustomer_id` int(11) NOT NULL DEFAULT '0',
  				`title` varchar(128) NOT NULL,
  				`description` text NOT NULL,
				`emailsubject` varchar(256) NOT NULL,
  				`content` mediumtext NOT NULL,
  				`created_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  				`created_fuser_id` int(11) NOT NULL,
  				`modified_time` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  				`modified_fuser_id` int(11) NOT NULL,
  				PRIMARY KEY (`emailtemplate_id`)
				) ENGINE=InnoDB DEFAULT CHARSET=utf8;";
					$pdoStatement = $_pdoObj->prepare($sql, array(PDO::ATTR_CURSOR => PDO::CURSOR_FWDONLY));
					$pdoStatement->execute(Array());
					if ($pdoStatement->errorCode() != 0) {
						echo 'SQL Fehler';
						print_r($pdoStatement->errorInfo());
						$errorCount += 1;
					}
				}
			}
			if ($errorCount == 0) {
				// update db version
				updateDbVersion($thisScriptDbVersion);
				$_pdoObj->commit();
			} else {
				$_pdoObj->rollBack();
			}
		} catch (Exception $e) {
			$_pdoObj->rollBack();
			echo "Innerer Datenbank-Fehler";
			print_r($e->getMessage());
		}
	}
} catch (Exception $e) {
	echo 'Datenbank-Fehler';
	print_r($e->getMessage());
}

