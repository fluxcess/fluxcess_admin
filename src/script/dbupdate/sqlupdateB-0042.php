<?php
/**
 * add "waitinglist" column to the zguest table of each event
 * this column is intended for storing information about if 
 * a guest is on the waiting list, or not.
 * 
 * 18.05.2016
 */
$thisScriptDbVersion = 42;

include_once ('../conf/config.php');

try {
	$errorCount = 0;
	// only update if this is the very next update.
	if ($currentDbVersion == $thisScriptDbVersion - 1) {
		$_pdoObj = dbconnection::getInstance();
		
		$_pdoObj->beginTransaction();
		
		try {
			// find all guest tables
			$sql = "SELECT fevent_id FROM `fevent`
					ORDER BY fevent_id ASC;";
			$pdoStatementF = $_pdoObj->prepare($sql, array (
					PDO::ATTR_CURSOR => PDO::CURSOR_FWDONLY 
			));
			$pdoStatementF->execute(Array ());
			if ($pdoStatementF->errorCode() != 0) {
				echo 'SQL Fehler';
				print_r($pdoStatementF->errorInfo());
				$errorCount += 1;
			} else {
				while ( $row = $pdoStatementF->fetch() ) {
					$eventid = $row['fevent_id'];
					try {
					// change event table
					echo "Adding 'waitinglist' column for event " . $eventid . " (script " . $thisScriptDbVersion . ")...\n";
					$sql = "ALTER TABLE `zguest" . $eventid . "` ADD `waitinglist` INT(1) NOT NULL DEFAULT '0' ;";
					$pdoStatement = $_pdoObj->prepare($sql, array (
							PDO::ATTR_CURSOR => PDO::CURSOR_FWDONLY 
					));
					$pdoStatement->execute(Array ());
					if ($pdoStatement->errorCode() != 0) {
						echo 'SQL Fehler';
						print_r($pdoStatement->errorInfo());
						$errorCount += 1;
					}
					echo "adding it to the zcol table ...\n";
					$sql = "INSERT INTO `zcolumn" . $eventid . "` 
							SET fieldname = 'waitinglist',
							fieldtype = 'int',
							fielddisplaytype = 0,
							editbymanager = 1,
							syscolumn = 1;";
					$pdoStatement = $_pdoObj->prepare($sql, array (
							PDO::ATTR_CURSOR => PDO::CURSOR_FWDONLY
					));
					$pdoStatement->execute(Array ());
					if ($pdoStatement->errorCode() != 0) {
						echo 'SQL Fehler';
						print_r($pdoStatement->errorInfo());
						$errorCount += 1;
					}

					} catch (Exception $e) {
						echo "SQL Catch Error ";
						print_r($pdoStatement->errorInfo());
						$errorCount += 1;
					}
						
				}
			}
			if ($errorCount == 0) {
				// update db version
				updateDbVersion($thisScriptDbVersion);
				$_pdoObj->commit();
			} else {
				$_pdoObj->rollBack();
			}
		} catch ( Exception $e ) {
			$_pdoObj->rollBack();
			echo "Innerer Datenbank-Fehler";
			print_r($e->getMessage());
		}
	}
} catch ( Exception $e ) {
	echo 'Datenbank-Fehler';
	print_r($e->getMessage());
}

