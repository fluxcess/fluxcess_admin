<?php
/**
 * Add some columns to the user table: newsletter, survey and contactme  
 * 
 * 31.07.2016
 */
$thisScriptDbVersion = 46;

include_once ('../conf/config.php');

try {
	$errorCount = 0;
	// only update if this is the very next update.
	if ($currentDbVersion == $thisScriptDbVersion - 1) {
		$_pdoObj = dbconnection::getInstance();
		
		$_pdoObj->beginTransaction();
		
		try {
			// change user table
			echo "Adding 'newsletter', 'survey' and 'contactme' columns (script " . $thisScriptDbVersion . ")...\n";
			$sql = "ALTER TABLE `fuser` 
					ADD `newsletter` TINYINT(1) NOT NULL DEFAULT '0' AFTER `confirmedip`, 
					ADD `survey` TINYINT(1) NOT NULL DEFAULT '0' AFTER `newsletter`, 
					ADD `contactme` TINYINT(1) NOT NULL DEFAULT '0' AFTER `survey`;";
			$pdoStatement = $_pdoObj->prepare($sql, array (
					PDO::ATTR_CURSOR => PDO::CURSOR_FWDONLY 
			));
			$pdoStatement->execute(Array ());
			if ($pdoStatement->errorCode() != 0) {
				echo 'SQL Fehler';
				print_r($pdoStatement->errorInfo());
				$errorCount += 1;
			}
			if ($errorCount == 0) {
				// update db version
				updateDbVersion($thisScriptDbVersion);
				$_pdoObj->commit();
			} else {
				$_pdoObj->rollBack();
			}
		} catch ( Exception $e ) {
			$_pdoObj->rollBack();
			echo "Innerer Datenbank-Fehler";
			print_r($e->getMessage());
		}
	}
} catch ( Exception $e ) {
	echo 'Datenbank-Fehler';
	print_r($e->getMessage());
}

