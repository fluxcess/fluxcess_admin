<?php
/**
 * Add a column to the session log in order to improve diagnosis of api calls:
 * api does not always send GET or POST, it might send other data.
 * Other data will be stored in the additional "othercontent" column.  
 * 
 * 06.06.2016
 */
$thisScriptDbVersion = 45;

include_once ('../conf/config.php');

try {
	$errorCount = 0;
	// only update if this is the very next update.
	if ($currentDbVersion == $thisScriptDbVersion - 1) {
		$_pdoObj = dbconnection::getInstance();
		
		$_pdoObj->beginTransaction();
		
		try {
			// change sessionlog table
			echo "Adding 'othercontent' column (script " . $thisScriptDbVersion . ")...\n";
			$sql = "ALTER TABLE `sessionlog` ADD `othercontent` TEXT NOT NULL DEFAULT '' AFTER `getcontent`;";
			$pdoStatement = $_pdoObj->prepare($sql, array (
					PDO::ATTR_CURSOR => PDO::CURSOR_FWDONLY 
			));
			$pdoStatement->execute(Array ());
			if ($pdoStatement->errorCode() != 0) {
				echo 'SQL Fehler';
				print_r($pdoStatement->errorInfo());
				$errorCount += 1;
			}
			if ($errorCount == 0) {
				// update db version
				updateDbVersion($thisScriptDbVersion);
				$_pdoObj->commit();
			} else {
				$_pdoObj->rollBack();
			}
		} catch ( Exception $e ) {
			$_pdoObj->rollBack();
			echo "Innerer Datenbank-Fehler";
			print_r($e->getMessage());
		}
	}
} catch ( Exception $e ) {
	echo 'Datenbank-Fehler';
	print_r($e->getMessage());
}

