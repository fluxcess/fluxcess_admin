<?php
/**
 * adds columns to field tables to all events (wemail)
 *
 * 08.12.2014
 */
$thisScriptDbVersion = 20;

include_once('../conf/config.php');

try {
	$errorCount = 0;
	// only update if this is the very next update.
	if ($currentDbVersion == $thisScriptDbVersion - 1) {
		$_pdoObj = dbconnection::getInstance();

		$_pdoObj->beginTransaction();

		try {
			// find all guest tables
			$sql = "SELECT fevent_id FROM `fevent`
					ORDER BY fevent_id ASC;";
			$pdoStatementF = $_pdoObj->prepare($sql, array(PDO::ATTR_CURSOR => PDO::CURSOR_FWDONLY));
			$pdoStatementF->execute(Array());
			if ($pdoStatementF->errorCode() != 0) {
				echo 'SQL Fehler';
				print_r($pdoStatementF->errorInfo());
				$errorCount += 1;
			} else {
				while ($row = $pdoStatementF->fetch()) {
					$eventid = $row['fevent_id'];

					// change event table
					echo "Adding columns to zcolumn table for event ".$eventid." (script ".$thisScriptDbVersion.")...\n";
					$sql = "ALTER TABLE `zcolumn".$eventid."`
							ADD `radiocheckvalue` VARCHAR(128) NOT NULL DEFAULT '' AFTER `longcontent`,
							ADD `fielddisplaytype` INT(2) NOT NULL DEFAULT 0 AFTER `fieldlength`,
							ADD `parent_field_id` INT(11) NOT NULL DEFAULT 0 AFTER `field_id`,
							ADD `isdefaultvalue` INT(1) NOT NULL DEFAULT 0 AFTER `fielddefault`
							;";

					$pdoStatement = $_pdoObj->prepare($sql, array(PDO::ATTR_CURSOR => PDO::CURSOR_FWDONLY));
					$pdoStatement->execute(Array());
					if ($pdoStatement->errorCode() != 0) {
						echo 'SQL Fehler';
						print_r($pdoStatement->errorInfo());
						$errorCount += 1;
					}
				}
			}
			if ($errorCount == 0) {
				// update db version
				updateDbVersion($thisScriptDbVersion);
				$_pdoObj->commit();
			} else {
				$_pdoObj->rollBack();
			}
		} catch (Exception $e) {
			$_pdoObj->rollBack();
			echo "Innerer Datenbank-Fehler";
			print_r($e->getMessage());
		}
	}
} catch (Exception $e) {
	echo 'Datenbank-Fehler';
	print_r($e->getMessage());
}

