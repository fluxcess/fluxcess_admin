<?php

class eventtransfer extends l5sys
{

    private $_triggerDir;

    private $_inTransferDir;

    private $_outTransferDir;

    /**
     * This is the constructor
     *
     * @param string $lg
     *            the language
     * @param string $locale
     *            the locale
     */
    function __construct($lg, $locale)
    {
        $this->_locale = $locale;
        $this->_lg = $lg;
        $this->_triggerDir = BASEDIR . 'tmp/triggers';
        $this->_inTransferDir = BASEDIR . 'tmp/transfer_in';
        $this->_outTransferDir = BASEDIR . 'tmp/transfer_out';
    }

    /**
     * returns a list of extensions for the api
     *
     * @return array contains events
     */
    public function apiGetList($entity = null, $params = null)
    {
        try {
            $res = null;
            $aparams = array();
            foreach ($params as $p) {
                $pParts = explode('=', $p);
                $aparams[$pParts[0]] = $pParts[1];
            }
            $res = $this->getTransferList();
        } catch (Exception $e) {
            $this->addError('', gettext('Problem reading remote event list.'), 1);
        }
        
        return $res;
    }

    /**
     * returns a list of all incoming and outgoing event transfers
     *
     * @return array[]|string[]|unknown[]
     */
    private function getTransferList()
    {
        $res = array(
            'transfers' => array(
                'in' => [],
                'out' => []
            ),
            'customer_id' => $_SESSION['customer']['customer_id']
        );
        
        // transfers out
        if (TRANSFER_OUT_ALLOWED === true) {
            if (is_dir($this->_inTransferDir)) {
                $files = scandir($this->_triggerDir);
                foreach ($files as $f) {
                    if ($f != '.' && $f != '..') {
                        $fnameParts = explode('_', $f);
                        if (substr($fnameParts[1], 4) == $_SESSION['customer']['customer_id']) {
                            $fc = file_get_contents($this->_triggerDir . '/' . $f);
                            $lines = explode("\r", $fc);
                            $t = array();
                            $t['server'] = '';
                            foreach ($lines as $line) {
                                if (substr($line, 0, 4) == 'url=') {
                                    $t['destination'] = substr($line, 4);
                                }
                            }
                            $t['fevent'] = substr($fnameParts[2], 2);
                            $t['timestamp'] = $fnameParts[4];
                            $t['rand'] = $fnameParts[5];
                            $t['status'] = $lines[0];
                            $res['transfers']['out'][] = $t;
                        }
                    }
                }
            }
        }
        
        // transfers in
        if (TRANSFER_IN_ALLOWED === true || TRANSFER_FOREIGNTRIGGERED_IN_ALLOWED === true) {
            if (is_dir($this->_inTransferDir)) {
                $files = scandir($this->_inTransferDir);
                foreach ($files as $f) {
                    if ($f != '.' && $f != '..') {
                        $fnameParts = explode('_', $f);
                        if (substr($fnameParts[1], 1) == $_SESSION['customer']['customer_id']) {
                            $fc = file_get_contents($this->_inTransferDir . '/' . $f);
                            $lines = explode("\r", $fc);
                            $parts = explode(';', $lines[0]);
                            
                            $t = array();
                            $t['fevent'] = substr($fnameParts[2], 1);
                            $t['timestamp'] = substr($fnameParts[3], 2);
                            $t['rand'] = substr($fnameParts[4], 1);
                            $t['status'] = $parts[0];
                            $res['transfers']['in'][] = $t;
                        }
                    }
                }
            }
        }
        
        return $res;
    }

    public function getRemoteTransferList($server, $username, $password)
    {
        $context = stream_context_create(array(
            'http' => array(
                'method' => 'PUT',
                'header' => "Authorization: Basic " . base64_encode($username . ":" . $password)
            )
        ));
        $eventtransferurl = $server . '/api/eventtransfers';
        
        $jsonObject = (file_get_contents($eventtransferurl, false, $context));
        $answerArray = json_decode($jsonObject, true);
        return $answerArray;
    }

    public function apiInvokeMethod($m)
    {
        if ($m == 'cancel') {
            $this->cancelTransfer($_POST['fevent_id'], $_POST['direction'], $_POST['server'], $_POST['code'], $_POST['ts']);
        } else {
            $this->addError('', gettext('API action does not exist:') . ' ' . $m);
        }
        return $res;
    }

    private function cancelTransfer($fevent_id, $direction, $server, $code, $ts)
    {
        if (is_numeric($fevent_id)) {
            $ev = new event($this->_lg, $this->_locale);
            if ($ev->checkUserEvent($fevent_id)) {
                // ToDo: verify filename fragments!
                if ($direction == 'in') {
                    $this->cancelIncomingTransfer($fevent_id, $server, $code, $ts);
                } elseif ($direction == 'out') {
                    $this->cancelOutgoingTransfer($fevent_id, $server, $code, $ts);
                }
            }
        }
    }

    private function cancelIncomingTransfer($fevent_id, $server, $code, $ts)
    {
        $dataFileName = 'c' . $_SESSION['customer']['customer_id'] . '_e' . $fevent_id . '_ts' . $ts . '_r' . $code;
        $dataFile = $this->_inTransferDir . '/' . $dataFileName;
        if (is_file($dataFile . '.sql')) {
            unlink($dataFile . '.sql');
        }
        if (is_file($dataFile . '.sql2')) {
            unlink($dataFile . '.sql2');
        }
        if (is_file($dataFile . '.sql3')) {
            unlink($dataFile . '.sql3');
        }
        if (is_file($dataFile . '.tar.bz2')) {
            unlink($dataFile . '.tar.bz2');
        }
        $triggerFileName = 'status_c' . $_SESSION['customer']['customer_id'] . '_e' . $fevent_id . '_ts' . $ts . '_r' . $code;
        $triggerFile = $this->_inTransferDir . '/' . $triggerFileName;
        if (is_file($triggerFile)) {
            $fc = file_get_contents($triggerFile);
            $firstSemicolonPos = strpos($fc, ';');
            $fc = 'cancelled' . substr($fc, $firstSemicolonPos);
            file_put_contents($triggerFile, $fc);
        }
    }

    private function cancelOutgoingTransfer($fevent_id, $server, $code, $ts)
    {
        $dataFileName = 'c' . $_SESSION['customer']['customer_id'] . '_e' . $fevent_id . '_ts' . $ts . '_r' . $code . '.tar.bz2';
        $dataFile = $this->_outTransferDir . '/' . $dataFileName;
        if (is_file($dataFile)) {
            unlink($dataFile);
        }
        $this->setTriggerDone('out', 'cancelled', $_SESSION['customer']['customer_id'], $fevent_id, $ts, $code);
    }

    private function setTriggerDone($direction, $triggertype, $customer_id, $fevent_id, $ts, $randX)
    {
        $fname = $this->_triggerDir . '/takeoverPrep_cust' . $customer_id . '_ev' . $fevent_id . '__' . $ts . '_' . $randX;
        if ($direction == 'in' && is_file($fname)) {
            file_put_contents($fname, 'done');
        } elseif ($direction == 'out') {
            $fname = BASEDIR . 'tmp/triggers/pushoverPrep_cust' . $customer_id . '_ev' . $fevent_id . '__' . $ts . '_' . $randX;
            if (is_file($fname)) {
                $cts = file_get_contents($fname);
                $lines = explode("\r", $cts);
                // $newcts = "done\n";
                $lineNumber = 1;
                $newcts = '';
                foreach ($lines as $line) {
                    if ($triggertype == 'sqldump' && $line == 'sqlgenerated=n') {
                        $newcts .= "sqlgenerated=y\r";
                    } elseif ($triggertype == 'pushtransferred' && $line == 'pushtransferred=n') {
                        $newcts .= "pushtransferred=y\r";
                    } elseif ($triggertype == 'sqlimported' && $line == 'sqlimported=n') {
                        $newcts .= "sqlimported=y\r";
                    } elseif ($triggertype === 'cancelled' && $lineNumber == 1) {
                        $newcts .= 'cancelled' . "\r";
                    } elseif ($triggertype === null && $lineNumber == 1) {
                        $newcts .= 'done' . "\r";
                    } else {
                        if (strlen(trim($line)) > 0) {
                            $newcts .= $line . "\r";
                        }
                    }
                    $lineNumber += 1;
                }
                file_put_contents($fname, $newcts);
            }
        }
    }
}