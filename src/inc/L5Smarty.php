<?php

/**
 * Custom Smarty class file for Line5 projects 
 * @author Line5
 *
 */

/**
 * Custom Smarty class for Line5 projects
 * 
 * @author Line5
 *        
 */
class L5Smarty extends Smarty
{

    /**
     * derived constructor
     * prepares some configuration directories.
     */
    function __construct()
    {
        $this->init();
    }

    /*
     * overloading not possible in php
     * function __construct($extensionName) {
     * init();
     * $this->addTemplateDir(BASEDIR.'ext/'.$extensionName.'/tpl', $extensionName);
     * }
     */
    function init()
    {
        parent::__construct();
        $this->template_dir = BASEDIR . 'tpl';
        $this->compile_dir = SMARTY_DIR2 . 'templates_c';
        if (! is_dir($this->compile_dir)) {
            mkdir($this->compile_dir);
        }
        $this->config_dir = SMARTY_DIR2 . 'configs';
        
        if (! is_dir($this->config_dir[0])) {
            mkdir($this->config_dir[0]);
        }
        $this->cache_dir = SMARTY_DIR2 . 'cache';
        if (! is_dir($this->cache_dir)) {
            mkdir($this->cache_dir);
        }
        // $this->plugins_dir[] = BASEDIR.'lib/smarty-plugins';
    }
}

?>