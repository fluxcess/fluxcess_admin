<?php

/**
 * contains the class guest
 */

/**
 * the guest class
 *
 * @author Line5
 *        
 */
class guest extends l5sys
{

    /**
     * contains the event id
     *
     * @var integer
     */
    private $_eventId = null;

    /**
     * entry point for GUI output
     * returns html code for the page
     *
     * @param array $gt
     * @param array $ps
     * @param array $err
     * @param boolean $proceed
     * @return Ambigous <string, void, string>
     */
    public function printPage(&$gt, &$ps, &$err, &$proceed)
    {
        $this->_gt = $gt;
        $this->_ps = $ps;
        $this->_err = $err;
        
        if (isset($_POST['form']) && $_POST['class'] == 'guest') {
            switch ($_POST['form']) {
                case 'create':
                    $xout = $this->handleCreate();
                    break;
                case 'deleteGuest':
                    $this->handleDelete();
                    $proceed = false;
                    break;
                case 'editGuestForm':
                    $xout .= $this->printChangeForm();
                    break;
                case 'changeGuestForm':
                    if (is_numeric($_POST['guest_id']) && $_POST['guest_id'] > 0) {
                        $this->saveData();
                    } else {
                        $xout = $this->handleCreate();
                    }
                    break;
            }
        } elseif ($this->_gt['type'] == 'editable' && $this->_gt['class'] == 'guest') {
            $this->changeDetail();
        } else {
            if (is_numeric($this->_gt[1])) {
                // $xout = $this->printGuestPage($this->_gt[1]);
            } else {
                switch ($this->_gt[1]) {
                    case 'listeJson':
                        $proceed = false;
                        $xout = $this->printGuestListJSON();
                        break;
                    default:
                        $sm = new L5Smarty();
                        $xout = $sm->fetch('guest/overview.tpl');
                }
            }
        }
        // pass back data to calling class
        $err = $this->_err;
        return $xout;
    }

    /**
     * handles user input from the form which is used to
     * create a new record
     *
     * @return Ambigous <string, void, string>
     */
    public function handleCreate($crm_id = null)
    {
        $guestid = null;
        $xout = '';
        // Check if event/table belongs to this user!
        $eventid = trim($_POST['fevent_id']);
        if (! is_numeric($eventid)) {
            $this->addError('', gettext('You cannot add guests to this event.') . '(' . $eventid . ')');
        }
        if (count($this->_err) == 0) {
            $fields = $this->readEditableFields($eventid);
            $ev = new event($this->_lg, $this->_locale);
            if (! $ev->checkUserEvent($eventid)) {
                $this->addError('', gettext('You cannot add guests to this event.') . ' (' . $eventid . ')');
            }
        }
        
        if ($crm_id == null) {
            $this->_ps['lastname'] = trim($this->_ps['lastname']);
            // check if lastname is long enough
            if (strlen($this->_ps['lastname']) < 2) {
                $this->addError('lastname', gettext('Please add the last name (at least 2 characters)'));
            }
            
            // check if email address is valid, if given
            if (isset($this->_ps['emailaddress']) && strlen($this->_ps['emailaddress']) > 0 && ! filter_var($this->_ps['emailaddress'], FILTER_VALIDATE_EMAIL)) {
                $this->addError('emailaddress', gettext('If you provide an email address, it needs to be a valid one.'));
            } elseif (! isset($this->_ps['emailaddress'])) {
                $this->_ps['emailaddress'] = '';
            }
        }
        if (count($this->_err) == 0) {
            $this->_pdoObj = dbconnection::getInstance();
            $this->_pdoObj->beginTransaction();
            try {
                $guestid = $this->generateUniqueGuestId($eventid);
                if ($crm_id == null) {
                    $sql = "INSERT INTO zguest" . $eventid . " SET ";
                    foreach ($this->_ps as $fieldname => $fieldvalue) {
                        if (! is_numeric($fieldname) && $fieldname != 'row_id' && $fieldname != 'guest_id' && array_search($fieldname, $fields) !== false) 
                        {
                            $sql .= $fieldname . " = :" . $fieldname . ", ";
                            $sqlArr[$fieldname] = $fieldvalue;
                        }
                    }
                    $sql .= " changetime = NOW(), guest_id = :guestid;";
                    $sqlArr[':guestid'] = $guestid;
                } else {
                    $sql = "INSERT INTO zguest" . $eventid . " SET
						changetime = NOW(),
						guest_id = :guestid,
						crm_id = :crm_id,
						fevent_id = :eventid
						;";
                    $sqlArr = array(
                        ':guestid' => $guestid,
                        ':eventid' => $eventid,
                        ':crm_id' => $crm_id
                    );
                }
                $pdoStatement = $this->_pdoObj->prepare($sql, array(
                    PDO::ATTR_CURSOR => PDO::CURSOR_FWDONLY
                ));
                // echo $sql;
                // print_r($sqlArr);
                $pdoStatement->execute($sqlArr);
                if ($pdoStatement->errorCode() != 0) {
                    $this->addError('', 'SQL Fehler' . print_r($pdoStatement->errorInfo(), true), 1);
                } elseif ($pdoStatement->rowCount() == 0) {
                    $this->addError('', 'SQL Fehler mit Errorcode 0: ' . print_r($pdoStatement->errorInfo(), true), 1);
                } else {
                    $guest_row_id = $this->_pdoObj->lastInsertId();
                }
                if (count($this->_err) == 0) {
                    $this->_pdoObj->commit();
                } else {
                    $this->_pdoObj->rollBack();
                }
                $ev = new event($this->_lg, $this->_locale);
                $ev->setRecordId($eventid);
                $ev->updateSessionGuestCounter();
            } catch (Exception $e) {
                $this->addError('', 'Datenbank-Fehler beim Anlegen eines Gastes.', 1, $sql . ' / ' . print_r($e->getMessage(), true), 1, print_r($e->getMessage(), true));
                $this->_pdoObj->rollBack();
            }
        }
        
        if (count($this->_err) == 0) {
            $sm = new L5Smarty();
            $sm->assign('fevent_id', $eventid);
            $sm->assign('guestcreatesuccessful', true);
            $xout = $sm->fetch('event/addguestform.tpl');
        }
        return $guestid;
    }

    /**
     * updates guest details in the database
     * requires $this->_ps to be filled
     */
    private function changeDetail()
    {
        try {
            $fieldname = $this->_ps['name'];
            $id = $this->_ps['pk'];
            $value = $this->_ps['value'];
            $eventid = $this->_gt['fevent_id'];
            
            $fields = array(
                'salutation',
                'firstname',
                'lastname',
                'company',
                'note'
            );
            if (array_search($fieldname, $fields) === false) {
                $this->addError('', 'Das Feld ' . $fieldname . ' kann nicht geändert werden.');
            }
            if (! is_numeric($id)) {
                $this->addError('', 'Ungültige Datensatz-Id: ' . $id);
            }
            
            $ev = new event($this->_lg, $this->_locale);
            if (! is_numeric($eventid) || ! $ev->checkUserEvent($eventid)) {
                $this->addError('', 'Ungültige Event-Id: ' . $eventid);
            }
            if (count($this->_err) == 0) {
                $sql = "UPDATE zguest" . $eventid . " SET " . $fieldname . " = :value WHERE guest_id = :id;";
                $this->_pdoObj = dbconnection::getInstance();
                $pdoStatement = $this->_pdoObj->prepare($sql, array(
                    PDO::ATTR_CURSOR => PDO::CURSOR_FWDONLY
                ));
                $pdoStatement->execute(array(
                    ':value' => $value,
                    ':id' => $id
                ));
                if ($pdoStatement->errorCode() != 0) {
                    $this->addError('', 'SQL Fehler' . print_r($pdoStatement->errorInfo(), true), 1);
                } elseif ($pdoStatement->rowCount() == 0) {
                    $this->addError('', 'SQL Fehler mit Errorcode 0: ' . print_r($pdoStatement->errorInfo(), true), 1);
                } else {
                    $guestid = $this->_pdoObj->lastInsertId();
                }
            }
        } catch (Exception $e) {
            $this->addError('', 'Datenbank-Fehler', 1, print_r($e->getMessage(), true));
        }
    }

    /**
     * checks submitted data for a guest deletion and invokes the
     * updaterecord functionality in order to delete the record.
     */
    private function handleDelete()
    {
        // check if guest id is numeric
        if (! is_numeric($this->_ps['guest_id'])) {
            $this->addError('', 'Fehlerhafte Gast-Id');
        }
        // check if event id is numeric
        if (! is_numeric($this->_ps['fevent_id'])) {
            $this->addError('', 'Fehlerhafte Event-Id');
        }
        // check if event belongs to user
        $ev = new event($this->_lg, $this->_locale);
        if (! $ev->checkUserEvent($this->_ps['fevent_id'])) {
            $this->addError('', 'Ungültige Event-Id: ' . $this->_ps['fevent_id']);
        }
        // delete guest
        $newData = array(
            'deleted' => 1
        );
        $this->updateRecord($this->_ps['fevent_id'], $this->_ps['guest_id'], $newData, array(
            'deleted'
        ));
        $ev = new event($this->_lg, $this->_locale);
        $ev->setRecordId($this->_ps['fevent_id']);
        $ev->updateSessionGuestCounter();
    }

    /**
     * returns html code for the form to modify guest data
     *
     * @return string
     */
    private function printChangeForm()
    {
        // check if guest id is numeric
        if (! is_numeric($this->_ps['guest_id'])) {
            $this->addError('', 'Fehlerhafte Gast-Id');
        }
        // check if event id is numeric
        if (! is_numeric($this->_ps['fevent_id'])) {
            $this->addError('', 'Fehlerhafte Event-Id');
        }
        // check if event belongs to user
        $ev = new event($this->_lg, $this->_locale);
        if (! $ev->checkUserEvent($this->_ps['fevent_id'])) {
            $this->addError('', 'Ungültige Event-Id: ' . $this->_ps['fevent_id']);
        } else {
            $eventdata = $ev->readRecord($this->_ps['fevent_id'], true);
        }
        
        if ($this->_ps['guest_id'] > 0) {
            $guestdata = $this->getGuestData($this->_ps['fevent_id'], $this->_ps['guest_id'], true, true, $eventdata);
        } else {
            $guestdata = array();
        }
        
        if ($guestdata['invited'] == '' || ! isset($guestdata['invited'])) {
            $guestdata['invited'] = 1;
        }
        if ($guestdata['cancelled'] == '') {
            $guestdata['cancelled'] = 0;
        }
        if ($guestdata['registered'] == '') {
            $guestdata['registered'] = 0;
        }
        if ($this->_ps['guest_id'] == 0) {
            $guestdata['accesscode'] = $this->generateAccessCode();
            $guestdata['webcode'] = '%' . $guestdata['accesscode'];
        }
        
        foreach ($eventdata['ticketcategory'] as $tc) {
            $ticketcategory[$tc['ticketcategorycode']] = $tc;
        }
        $sm = new L5Smarty();
        $sm->assign('ps', $guestdata);
        $sm->assign('fevent_id', $this->_ps['fevent_id']);
        $sm->assign('guest_id', $this->_ps['guest_id']);
        $sm->assign('ev', $eventdata);
        $sm->assign('ticketcategory', $ticketcategory);
        $sm->assign('customFields', $this->generateCustomFields($this->_ps['fevent_id'], $guestdata));
        $xout = $sm->fetch('event/changeguestform.tpl');
        
        return $xout;
    }

    /**
     * generates the html code for the custom fields in a booking form
     *
     * @param integer $eventid
     * @param array $guestdata
     *            contains all data from the guest
     * @return string contains html code with form fields
     */
    public function generateCustomFields($eventid, $guestdata)
    {
        $xout = '';
        try {
            $sql = "SELECT * FROM `zcolumn" . $eventid . "`
					WHERE deleted = 0
					AND syshidden = 0
					AND editbymanager = 1
					AND syscolumn = 0
					";
            $this->_pdoObj = dbconnection::getInstance();
            $pdoStatement = $this->_pdoObj->prepare($sql, array(
                PDO::ATTR_CURSOR => PDO::CURSOR_FWDONLY
            ));
            $pdoStatement->execute(array());
            if ($pdoStatement->errorCode() > 0) {
                $this->addError('', 'SQL Problem - Felder konnten nicht erstellt werden. Bitte kontaktieren Sie den Support.');
            } else {
                while ($row = $pdoStatement->fetch()) {
                    switch ($row['fieldtype']) {
                        case 'int':
                            $xout .= $this->createInputText($row, $guestdata);
                            break;
                        case 'varchar':
                            $xout .= $this->createInputText($row, $guestdata);
                            break;
                        case 'text':
                            $xout .= $this->createInputTextarea($row, $guestdata);
                    }
                }
            }
        } catch (Exception $e) {
            $this->addError('', 'Problem beim Erstellen der dynamischen Eingabefelder');
        }
        return $xout;
    }

    /**
     * returns a list of editable fields
     *
     * @param integer $eventid
     * @return array contains a list of editable fields
     */
    private function readEditableFields($eventid)
    {
        $f = array(
            'salutation',
            'firstname',
            'lastname',
            'company',
            'note',
            'invited',
            'registered',
            'cancelled',
            'accesscode',
            'emailaddress'
        );
        try {
            $sql = "SELECT fieldname FROM `zcolumn" . $eventid . "`
					WHERE deleted = 0
					AND syshidden = 0
					AND editbymanager = 1
					";
            $this->_pdoObj = dbconnection::getInstance();
            $pdoStatement = $this->_pdoObj->prepare($sql, array(
                PDO::ATTR_CURSOR => PDO::CURSOR_FWDONLY
            ));
            $pdoStatement->execute(array());
            if ($pdoStatement->errorCode() > 0) {
                $this->addError('', 'SQL Problem - Beschreibbare Felder nicht gefunden. Bitte kontaktieren Sie den Support.');
            } else {
                while ($row = $pdoStatement->fetch()) {
                    $f[] = $row['fieldname'];
                }
            }
        } catch (Exception $e) {
            $this->addError('', 'Problem beim Erstellen der dynamischen Eingabefelder');
        }
        return $f;
    }

    /**
     * returns html code for an input field with a label
     *
     * @param array $fielddata
     * @param array $guestdata
     * @return string
     */
    private function createInputText($fielddata, $guestdata)
    {
        $xout = '';
        $xout .= '<div class="control-group">
				<label class="control-label" for="' . $fielddata['fieldname'] . '">';
        if (strlen($fielddata['formlabel']) > 0) {
            $xout .= $fielddata['formlabel'];
        } else {
            $xout .= $fielddata['fieldname'];
        }
        $xout .= '</label>
						<div class="controls">
						<input name="' . $fielddata['fieldname'] . '" type="text" class="input-block-level" placeholder="" value="' . $guestdata[$fielddata['fieldname']] . '" ';
        if ($fielddata['editbymanager'] == 0) {
            $xout .= ' disabled';
        }
        $xout .= ' />';
        if ($fielddata['helptext'] != '') {
            $xout .= '<span class="help-block">Info: ' . $fielddata['helptext'] . '</span>';
        }
        $xout .= '</div>
				</div>';
        return $xout;
    }

    private function createInputTextarea($fielddata, $guestdata)
    {
        $xout = '';
        $xout .= '<div class="control-group">
				<label class="control-label" for="' . $fielddata['fieldname'] . '">';
        if (strlen($fielddata['formlabel']) > 0) {
            $xout .= $fielddata['formlabel'];
        } else {
            $xout .= $fielddata['fieldname'];
        }
        $xout .= '</label>
						<div class="controls">
						<textarea name="' . $fielddata['fieldname'] . '" placeholder="" ';
        if ($fielddata['editbymanager'] == 0) {
            $xout .= ' disabled';
        }
        $xout .= '>' . $guestdata[$fielddata['fieldname']] . '</textarea>';
        if ($fielddata['helptext'] != '') {
            $xout .= '<span class="help-block">Info: ' . $fielddata['helptext'] . '</span>';
        }
        $xout .= '</div>
				</div>';
        return $xout;
    }

    /**
     * returns guest data
     *
     * @param int $eventid
     * @param int $guestid
     * @return array
     */
    private function getGuestData($eventid, $guestid, $assoc = false, $includenmentities = true, &$eventdata = null, $checkinonly = false)
    {
        // check if guest id is numeric
        if (! is_numeric($guestid)) {
            $this->addError('', 'Fehlerhafte Gast-Id');
        }
        // check if event id is numeric
        if (! is_numeric($eventid)) {
            $this->addError('', 'Fehlerhafte Event-Id');
        }
        // check if event belongs to user
        $ev = new event($this->_lg, $this->_locale);
        if (! $ev->checkUserEvent($eventid)) {
            $this->addError('', 'Ungültige Event-Id: ' . $eventid);
        }
        $g = new entityGuest($eventid, $guestid, $eventdata, false, $checkinonly);
        $guestData = $g->getData($includenmentities);
        $this->_err = array_merge($this->_err, $g->getErrors());
        return $guestData;
    }

    /**
     * checks data in $this->_ps and invokes the updateRecord
     * method if successful
     */
    private function saveData()
    {
        try {
            // $fieldname = $this->_ps['name'];
            $id = $this->_ps['guest_id'];
            $value = $this->_ps['value'];
            $eventid = $this->_ps['fevent_id'];
            
            $fields = $this->readEditableFields($eventid);
            
            if (! is_numeric($id)) {
                $this->addError('', 'Ungültige Datensatz-Id: ' . $id);
            }
            
            // check access code
            // must be nothing or min 6 chars
            $this->_ps['accesscode'] = trim($this->_ps['accesscode']);
            if ($this->_ps['accesscode'] == '') {
                // empty is okay
            } elseif (strlen($this->_ps['accesscode']) < 6) {
                $this->addError('accesscode', 'Der Zugangscode muss aus mindestens 6 Zeichen bestehen oder leer sein.');
            } else {
                $this->checkIfAccessCodeIsUnique($eventid, $id, $this->_ps['accesscode']);
            }
            
            // never more registrations + cancellations than invitations
            // exception: self-registration allowed + invitations = 0,
            // but then, cancellations are also 0.
            if ($this->_ps['registered'] > $this->_ps['invited'] && $this->_ps['invited'] > 0) {
                $this->addError('registered', gettext('You cannot register more people than you invited.'));
            } elseif ($this->_ps['invited'] < 0) {
                $this->addError('invited', gettext('Please type a positive number or 0 in the "invited" field.'));
            } elseif ($this->_ps['cancelled'] > $this->_ps['invited']) {
                $this->addError('cancelled', gettext('You cannot cancel more people than you invited.'));
            } elseif ($this->_ps['registered'] + $this->_ps['cancelled'] > $this->_ps['invited'] && $this->_ps['invited'] > 0) {
                $this->addError('registered', gettext('You cannot register and cancel more people than you invited.'));
            } elseif (! is_numeric($this->_ps['invited'])) {
                $this->addError('invited', gettext('Please type a positive number or 0 in the "invited" field.'));
            }
            
            $ev = new event($this->_lg, $this->_locale);
            if (! is_numeric($eventid) || ! $ev->checkUserEvent($eventid)) {
                $this->addError('', 'Ungültige Event-Id: ' . $eventid);
            }
            if (count($this->_err) == 0) {
                $this->updateRecord($eventid, $id, $this->_ps, $fields);
                $ev = new event($this->_lg, $this->_locale);
                $ev->setRecordId($eventid);
                $ev->updateSessionGuestCounter();
            }
        } catch (Exception $e) {
            $this->addError('', 'Fehler beim Aktualisieren des Gastes', 1, __FILE__ . '/' . __LINE__ . ': ' . print_r($e->getMessage(), true));
        }
    }

    /**
     * sets the "archived" property of a record to "1",
     * if it finds an undeleted and unarchived record for a given guest id
     *
     * @param int $eventId
     * @param int $guestId
     */
    private function archiveRecord($eventId, $guestId)
    {
        $sql = "UPDATE zguest" . $eventId . "
				SET archived = 1
				WHERE guest_id = :gid
				AND archived = 0
				AND deleted = 0
				;
				";
        $this->_pdoObj = dbconnection::getInstance();
        $pdoStatement = $this->_pdoObj->prepare($sql, array(
            PDO::ATTR_CURSOR => PDO::CURSOR_FWDONLY
        ));
        $pdoStatement->execute(Array(
            ':gid' => $guestId
        ));
        if ($pdoStatement->errorCode() != 0) {
            $this->addError('', 'SQL Fehler', 1, print_r($pdoStatement->errorInfo(), true));
        }
        if ($pdoStatement->rowCount() == 0) {
            $this->addError('', 'SQL Fehler', 1, __FILE__ . '/' . __LINE__ . ': Not able to find unarchived and undeleted record for event ' . $eventId . ' guest ' . $guestId . ':');
        }
    }

    /**
     * returns the row id of the active record for the given guest
     *
     * @param int $eventid
     * @param int $guestid
     * @return int <NULL, int> // row_id
     */
    private function getCurrentRowId($eventid, $guestid)
    {
        $guestid = null;
        $this->_pdoObj = dbconnection::getInstance();
        $sql = "SELECT row_id FROM zguest" . $eventid . "
				WHERE guest_id = :gid
				AND archived = 0;";
        $sqlArr[':gid'] = $guestid;
        $pdoStatement = $this->_pdoObj->prepare($sql, array(
            PDO::ATTR_CURSOR => PDO::CURSOR_FWDONLY
        ));
        $pdoStatement->execute($sqlArr);
        if ($pdoStatement->errorCode() != 0) {
            $this->addError('', 'SQL Fehler' . print_r($pdoStatement->errorInfo(), true), 1);
        } else {
            $row = $pdoStatement->fetch();
            $guestid = $row['row_id'];
        }
        return $guestid;
    }

    /**
     * checks if the given accesscode is being used for any guest
     * at the giben already
     *
     * @param int $eventid
     * @param int $guestid
     *            // of the current guest - this is important for updates. this guestid will
     *            be excluded from checking.
     * @param string $accesscode
     */
    private function checkIfAccessCodeIsUnique($eventid, $guestid, $accesscode)
    {
        // must not exist for that event yet!
        $sql = "SELECT COUNT(guest_id) amount FROM zguest" . $eventid . "
				WHERE accesscode = :accesscode
				AND guest_id != :guestid;";
        try {
            $this->_pdoObj = dbconnection::getInstance();
            $pdoStatement = $this->_pdoObj->prepare($sql, array(
                PDO::ATTR_CURSOR => PDO::CURSOR_FWDONLY
            ));
            $pdoStatement->execute(Array(
                ':guestid' => $id,
                ':accesscode' => $accesscode
            ));
            if ($pdoStatement->errorCode() != 0) {
                $this->addError('', 'SQL Fehler' . print_r($pdoStatement->errorInfo(), true), 1);
            } else {
                $row = $pdoStatement->fetch();
                if ($row['amount'] > 0) {
                    $this->addError('accesscode', 'Dieser Zugangscode wurde bereits für einen anderen Gast vergeben.');
                }
            }
        } catch (PDOException $e) {
            $this->addError('accesscode', 'PDO Problem beim Prüfen des Zugangscodes.');
        } catch (Exception $e) {
            $this->addError('accesscode', 'Problem beim Prüfen des Zugangscodes.');
        }
    }

    /**
     * updates a guest record
     * this includes archiving the old record, creating a new one with the
     * new data, and is transaction safe.
     *
     * before calling this function, ensure this:
     * - all record data must be validated before
     * - check permissions of the user for this event id / guest / field data
     *
     * @param int $eventid
     * @param int $guestid
     * @param array $newData
     *            // usually a post var set - contains data in the form fieldname>fieldvalue
     */
    private function updateRecord($eventid, $guestid, $newData, $fields)
    {
        try {
            $recordsAreEqual = false;
            $this->_pdoObj = dbconnection::getInstance();
            $this->_pdoObj->beginTransaction();
            if ($guestid > 0) {
                // get current record row id
                $row_id = $this->getCurrentRowId($eventid, $guestid);
                // get current guest data
                $guestData = $this->getGuestData($eventid, $guestid, true, false);
                $changedGuestData = $guestData;
                // merge updated data into current record data array
                foreach ($newData as $fieldname => $fieldvalue) {
                    if (array_search($fieldname, $fields) !== false) {
                        $changedGuestData[$fieldname] = $fieldvalue;
                    }
                }
                
                // compare old and new record
                $unequalcounter = 0;
                
                foreach ($changedGuestData as $fieldname => $fieldvalue) {
                    if ($guestData[$fieldname] != $fieldvalue) {
                        $unequalcounter += 1;
                    }
                }
                if ($unequalcounter == 0) {
                    $recordsAreEqual = true;
                    // $this->addError('', "The new record contains the same data as the old record and will not be saved.", 1);
                }
                
                // remove old changetime field
                unset($changedGuestData['changetime']);
            } else {
                $recordsAreEqual = false;
                foreach ($newData as $fieldname => $fieldvalue) {
                    if (array_search($fieldname, $fields) !== false && $fieldname != 'guest_id') {
                        $changedGuestData[$fieldname] = $fieldvalue;
                    }
                }
                $changedGuestData['guest_id'] = $this->generateUniqueGuestId($eventid);
            }
            // TODO: fill editor data into new record set
            // $newData[''] = '';
            if ($recordsAreEqual == false) {
                if ($guestid > 0) {
                    // archive current record
                    $this->archiveRecord($eventid, $guestid);
                    // create new record with merged data
                }
                $sql = "INSERT INTO zguest" . $eventid . " SET guest_id = :guest_id, ";
                $sqlArr['guest_id'] = $guestid;
                foreach ($changedGuestData as $fieldname => $fieldvalue) {
                    if (! is_numeric($fieldname) && $fieldname != 'row_id' && $fieldname != 'guest_id') {
                        $sql .= $fieldname . " = :" . $fieldname . ", ";
                        $sqlArr[$fieldname] = $fieldvalue;
                    }
                }
                // $sql = substr($sql, 0, -2);
                $sql .= " changetime = NOW();";
                $pdoStatement = $this->_pdoObj->prepare($sql, array(
                    PDO::ATTR_CURSOR => PDO::CURSOR_FWDONLY
                ));
                $pdoStatement->execute($sqlArr);
                if ($pdoStatement->errorCode() != 0) {
                    $this->addError('', 'SQL Fehler' . print_r($pdoStatement->errorInfo(), true), 1);
                } else {
                    $row_id = $this->_pdoObj->lastInsertId();
                }
            }
            // In case of any error, roll back - otherwise commit.
            if (count($this->_err) > 0) {
                $this->_pdoObj->rollBack();
            } else {
                $this->_pdoObj->commit();
            }
        } catch (Exception $e) {
            $this->_pdoObj->rollBack();
            $this->addError('', 'Record update / creation unsuccessful. Contact administrator.', 1, __FILE__ . '/' . __LINE__ . ': ' . print_r($e->getMessage(), true) . ' ' . print_r($sqlArr, true) . ' ' . $sql);
        }
    }

    /**
     * returns a unique guest id for the given event
     * should be called by a function that opens and closes a transaction.
     *
     * @param int $eventid
     * @return int <NULL, number> // new, unique guest id
     */
    public function generateUniqueGuestId($eventid)
    {
        $row_id = null;
        $this->_pdoObj = dbconnection::getInstance();
        $sql = "SELECT guest_id FROM zguest" . $eventid . "
				ORDER BY guest_id DESC LIMIT 1;";
        $pdoStatement = $this->_pdoObj->prepare($sql, array(
            PDO::ATTR_CURSOR => PDO::CURSOR_FWDONLY
        ));
        $pdoStatement->execute();
        if ($pdoStatement->errorCode() != 0) {
            $this->addError('', 'SQL Fehler' . print_r($pdoStatement->errorInfo(), true), 1);
        } elseif ($pdoStatement->rowCount() == 0) {
            $row_id = 1;
        } else {
            $row = $pdoStatement->fetch();
            $row_id = ($row['guest_id'] * 1) + 1;
        }
        return $row_id;
    }

    public function apiGetRecord($params = null, $subentity = null, $subentityid = null)
    {
        if (isset($_GET['special']) && $_GET['special'] == 'checkin') {
            $e = array();
            $res = array(
                'guest' => $this->getGuestData($this->_eventId, $this->_recordId, true, true, $e, true)
            );
        } else {
            $res = array(
                'guest' => $this->getGuestData($this->_eventId, $this->_recordId, true, true)
            );
        }
        return $res;
    }

    public function setEventId($id)
    {
        $res = false;
        if ($this->_eventId == null) {
            $this->_eventId = $id;
            $res = true;
        }
        return $res;
    }

    private function generateAccessCode()
    {
        $blk = 'ABCDEFGHJKLMNPQRSTUVWXYZ23456789';
        $max = strlen($blk) - 1;
        $accesscode = '';
        for ($i = 0; $i < 6; $i ++) {
            $accesscode .= substr($blk, rand(0, $max), 1);
        }
        return $accesscode;
    }

    /**
     * Saves a record via the API
     */
    public function apiPutRecord($put)
    {
        $data = null;
        $ev = new event($this->_lg, $this->_locale);
        if ($ev->checkUserEvent($put['fevent_id']) == true) {
            $this->_eventId = $put['fevent_id'];
            $this->_ps = $put;
            if (is_numeric($put['guest_id']) && $put['guest_id'] > 0) {
                $guest_id = $put['guest_id'];
                $this->saveData();
            } else {
                $guest_id = $this->handleCreate(null);
            }
            $data = array(
                'guest_id' => $guest_id
            );
            // $this->ToEvent($put['fevent_id'], $put['extension_id'], $put['description']);
        } else {
            $this->addError('fevent_id', gettext('Invalid event id given.'));
        }
        return $data;
    }
}