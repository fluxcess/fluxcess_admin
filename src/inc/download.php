<?php
/**
 * Download-Class - handles the download of internal files,
 * that means, files the user has uploaded, are only visible when authenticated
 * via the admin area
 *
 * @author Line5, fluxcess GmbH
 *        
 */
class download extends l5sys
{    
    /**
     * These files are to be sent directly to the browser.
     * 
     * @param Array $data
     *              This array is directly from the GET request. Example:
     *              /api/downloads/1/hidden/Image%20abc.png or
     *              /api/downloads/1/hidden/directory%201/subdirectory/my_image.png
     */
    public function sendFileToBrowser($data)
    {
        $collectionStarted = false;
        $customeridFound = false;
        $directory = '';
        $filename = '';
        foreach ($data as $dir) {
            if ($dir == 'downloads') {
                // the path needs to begin with "downloads"
                $collectionStarted = true;
            } elseif ($collectionStarted === true && $customeridFound === false) {
                // the second part has to be the customer id.
                if (is_numeric($dir) && $_SESSION['customer']['customer_id'] == $dir) {
                    $directory .= '/' . $dir;
                    $customeridFound = true;
                } else {
                    // if it is not, the specified file does not belong to the right user.
                    header("HTTP/1.0 403 Forbidden");
                    echo '403 - ' . gettext('No access to this file or directory.');
                }
            } elseif ($collectionStarted === true && $customeridFound === true) {
                if ($dir != '..' && strlen($dir) > 0 && strlen($dir) < 100) {
                    $directory .= '/' . $dir;
                    $filename = $dir;
                }
            }
        }
        $downloadfile = BASEDIR . 'files' . $directory;
        if (is_file($downloadfile)) {
            header("Content-Type: application/octet-stream");
            header("Content-Disposition: attachment; filename=\"" . $filename . "\"");
            header("Content-Length: " . filesize($downloadfile));
            header("Pragma: cache");
            header("Expires: 0");
            readfile($downloadfile);
        } else {
            header("HTTP/1.0 404 Not Found");
            echo '404 - ' . gettext('File not found') . ': ' . $directory;
        }
    }
}