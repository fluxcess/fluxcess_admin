<?php
/**
 * This class is the backbone of the event exchange functionality.
 * Event data, including guest lists, can be pushed over to other servers, or pulled over 
 * from other servers.
 * 
 * This is useful, when the preparation for an event happens online, but during the event 
 * itself offline servers need to be used (maybe no internet at the event location). 
 * 
 * @author fluxcess GmbH
 *
 */
class remoteevent extends l5sys
{

    /**
     * Directory, in which the trigger file is to be stored. Example value: '/my/home/directory/tmp/triggers'
     * @var String
     */
    private $_triggerDir;

    /**
     * This is the constructor
     *
     * @param string $lg
     *            the language
     * @param string $locale
     *            the locale
     */
    function __construct($lg, $locale)
    {
        $this->_locale = $locale;
        $this->_lg = $lg;
        $this->_triggerDir = BASEDIR . 'tmp/triggers';
    }

    /**
     * returns a list of extensions for the api
     *
     * @return array contains events
     */
    public function apiGetList($entity = null, $params = null)
    {
        try {
            $res = null;
            $aparams = array();
            foreach ($params as $p) {
                $pParts = explode('=', $p);
                $aparams[$pParts[0]] = $pParts[1];
            }
            $res = $this->getRemoteEventList($aparams['server'], $aparams['username'], $aparams['password']);
        } catch (Exception $e) {
            $this->addError('', gettext('Problem reading remote event list.'), 1);
        }
        
        return $res;
    }

    /**
     * reads the list of events belonging to the given user from the given server
     * returns the list of events
     *
     * @param String $server
     * @param String $username
     * @param String $password
     * @return array|string|string[]|mixed
     */
    private function getRemoteEventList($server, $username, $password)
    {
        $res = $server . ' ' . $username;
        $listOfEventsUrl = $server . '/api/events/';
        
        // authenticate
        $context = stream_context_create(array(
            'http' => array(
                'method' => 'GET',
                'header' => "Authorization: Basic " . base64_encode($username . ":" . $password)
            )
        ));
        $jsonObject = (file_get_contents($listOfEventsUrl, false, $context));
        
        // read headers
        $serverResponse = $http_response_header;
        if (strpos($serverResponse[0], '200 OK') !== false) {
            $res = json_decode($jsonObject, true);
        } else if (strpos($serverResponse[0], '401 Unauthorized') !== false) {
            // authentication problem
            $this->addError('password', gettext('Username or password wrong.'));
            $res = array();
        } else {
            // unknown error
            $this->addError('url', gettext('Error while accessing the remote server: ') . $serverResponse[0], 1, print_r($serverResponse));
            $res = array();
        }
        
        return $res;
    }

    public function getList()
    {
        // return "123";
        // return $this->getRemoteEventList($server, $username, $password);
    }

    public function apiPutRecord($subentity = null, $subentityid = null, $put)
    {
        if ($subentity != null) {
            $classname = trim(substr($subentity, 0, - 1));
            $putAllowed = array();
            if ($subentity == 'takeoverfromserver') {
                $this->takeoverfromserver($_POST['ffevent_id'], $_POST['url'], $_POST['username'], $_POST['password']);
            } elseif ($subentity == 'triggerTakeoverPreparation') {
                $res['data'] = $this->triggerTakeoverPreparation($_GET['fevent_id']);
            } elseif ($subentity == 'checkTakeoverPreparation') {
                $res['data'] = $this->checkTakeoverPreparation($_GET['fevent_id'], $_GET['ts'], $_GET['randX']);
            } else {
                $this->addError('', gettext('API action does not exist:') . ' ' . $subentity);
            }
        }
        return $res;
    }

    public function apiInvokeMethod($m)
    {
        if ($m == 'takeoverfromserver') {
            $this->takeoverfromserver($_POST['ffevent_id'], $_POST['url'], $_POST['username'], $_POST['password']);
        } elseif ($m == 'triggerTakeoverPreparation') {
            $res['data'] = $this->triggerTakeoverPreparation($_GET['fevent_id']);
        } elseif ($m == 'checkTakeoverPreparation') {
            $res['data'] = $this->checkTakeoverPreparation($_GET['fevent_id'], $_GET['ts'], $_GET['randX']);
        } elseif ($m == 'pushovertoserver') {
            $this->pushovertoserver($_POST['event_id_local'], $_POST['event_id_remote'], $_POST['url'], $_POST['username'], $_POST['password']);
        } elseif ($m == 'pushevent') {
            echo "WELCOME, and thank you! Content:";
            print_r($_POST);
            print_r($_FILES['file_contents']);
            $fdir = BASEDIR . 'tmp/transfer_in/';
            if (! is_dir($fdir)) {
                mkdir($fdir);
            }
            $oldfilename = $_FILES['file_contents']['name'];
            $oldfilenameparts = explode('_', $oldfilename);
            // ToDo: Check permissions for this event
            $newfilename = 'c' . $_SESSION['customer']['customer_id'] . '_e' . $_POST['fevent_id_remote'] . '_';
            $newfilename .= $oldfilenameparts[2] . '_' . $oldfilenameparts[3];
            $newfilename = $oldfilename;
            $destination = $fdir . $_SESSION['customer']['customer_id'] . '/' . $newfilename;
            // $ev = new event($this->_lg, $this->_locale);
            $statusFileName = BASEDIR . 'tmp/transfer_in/status_' . substr($oldfilename, 0, - 8);
            $destDir = $fdir . $_SESSION['customer']['customer_id'];
            if (! is_dir($destDir)) {
                mkdir($destDir);
            }
            
            file_put_contents($statusFileName, 'downloading;' . $_SESSION['customer']['customer_id'] . ';' . $_POST['local_customer_id']);
            echo 'moving to ' . $destination . PHP_EOL;
            if (move_uploaded_file($_FILES['file_contents']['tmp_name'], $destination) == 1) {
                echo "Upload completed..." . PHP_EOL;
                file_put_contents($statusFileName, 'downloaded;' . $_SESSION['customer']['customer_id'] . ';' . $_POST['local_customer_id']);
                echo "Data stored on server.";
            } else {
                echo "file not written.";
                file_put_contents($statusFileName, 'downloadfailed;' . $_SESSION['customer']['customer_id'] . ';' . $_POST['local_customer_id']);
            }
        } else {
            $this->addError('', gettext('API action does not exist:') . ' ' . $m);
        }
        return $res;
    }

    private function takeoverfromserver($fevent_id, $url, $username, $password)
    {
        $errCounter = 0;
        header('Content-type: text/html; charset=utf-8');
        $context = stream_context_create(array(
            'http' => array(
                'method' => 'PUT',
                'header' => "Authorization: Basic " . base64_encode($username . ":" . $password)
            )
        ));
        $firsturl = $url . '/api/events/' . $fevent_id . '/triggerTakeoverPreparation?fevent_id=' . $fevent_id;
        
        $this->informUser(gettext('Asking remote server to prepare downloadable packages.') . '...');
        $jsonObject = (file_get_contents($firsturl, false, $context));
        $answerArray = json_decode($jsonObject, true);
        
        if ($jsonObject === false) {
            $this->informUser('[e] ' . gettext('Problem while triggering the package preparation.') . ' ' . $firsturl . '...');
        } elseif (isset($answerArray['err'])) {
            $ecnt = 0;
            foreach ($answerArray['err'] as $e) {
                $ecnt ++;
                $errtxt .= '(' . $ecnt . ') ' . $e['message'] . ' ';
                $errCounter ++;
            }
            $this->informUser('[e] ' . gettext('Error triggering the package preparation:') . ' ' . $errtxt);
        } else {
            $ts = $answerArray['data']['ts'];
            $randX = $answerArray['data']['rand'];
        }
        
        // great - now we need to check continuously, if the files have been generated on the server.
        if ($errCounter == 0) {
            $done = false;
            // $checkurl = 'http://' . $_POST['url'] . '/api/events/' . $fevent_id . '/triggerTakeoverPreparation';
            $checkurl = $url . '/api/events/' . $fevent_id . '/checkTakeoverPreparation?randX=' . $randX . '&ts=' . $ts . '&fevent_id=' . $fevent_id;
            $this->informUser(gettext('Asking remote server if packages are ready for download...'));
            $maxLoops = 24;
            $nowLoops = 0;
            while ($done === false && $nowLoops < $maxLoops) {
                $nowLoops ++;
                $jsonAnswer = file_get_contents($checkurl, false, $context);
                $answerArray = json_decode($jsonAnswer, true);
                if ($answerArray['data']['status'] === 'done') {
                    $done = true;
                } elseif ($answerArray['data']['status'] == 'error') {
                    $done = true;
                    $this->informUser('[e] ' . gettext('The remote server was not able to prepare the download packages.'));
                } else {
                    $this->informUser(gettext('waiting...') . print_r($jsonAnswer, true));
                }
                sleep(10);
            }
            if ($done === false) {
                $this->informUser('[e] ' . gettext('The remote server did not prepare the packages within an appropriate time span.'));
            } else {
                $this->informUser(gettext('The remote server has prepared the package, it is ready to be transferred here.'));
            }
        }
        
        $feventRecordDumpUrl = $url . '/api/events/' . $fevent_id . '/eventdump?fevent_id=' . $fevent_id . '&customer_id=' . $_SESSION['customer']['customer_id'] . '&randX=' . $randX . '&ts=' . $ts;
        $this->informUser(gettext('Starting to download data from server - local customer: ' . $_SESSION['customer']['customer_id']) . '...');
        
        $data['dump'] = $this->pullEventDataFromRemoteServer($_POST['url'], $dtp[0], $_POST['username'], $_POST['password'], $fevent_id, $_SESSION['customer']['customer_id'], $randX, $ts);
        flush();
        ob_end_flush();
        if ($data['dump']['err'] > 0) {
            $errCounter += 1;
        } else {
            $this->informUser(gettext('Data successfully downloaded.'));
        }

        // Next step: Processing and importing the loaded data...
        if ($errCounter == 0) {
            $done = false;
            // $checkurl = 'http://' . $_POST['url'] . '/api/events/' . $fevent_id . '/triggerTakeoverPreparation';
            $statusFile = BASEDIR . 'tmp/transfer_in/status_c' . $_SESSION['customer']['customer_id'] . '_e' . $fevent_id . '_ts' . $ts . '_r' . $randX;
            $this->informUser(gettext('Waiting for data to be processed and moved into the database ...'));
            $maxLoops = 32;
            $nowLoops = 0;
            while ($done === false && $nowLoops < $maxLoops) {
                $nowLoops ++;
                $contstr = file_get_contents($statusFile);
                $cont = explode(';', $contstr);
                if ($cont[0] === 'done') {
                    $done = true;
                } elseif ($answerArray['data']['status'] == 'error') {
                    $done = true;
                    $this->informUser('[e] ' . gettext('It was not possible to load the data into the database.'));
                } else {
                    $this->informUser(gettext('waiting...') . ' (' . $contstr . ')');
                }
                sleep(10);
            }
            if ($done === false) {
                $this->informUser('[e] ' . gettext('Data could not be loaded into the local database.'));
            } else {
                $this->informUser(gettext('The data seems to have been loaded into the local database.'));
            }
        }
        
        flush();
        ob_end_flush();
        sleep(1);
        $i ++;
        
        $this->informUser('changing event to belong to user ' . $_SESSION['user']['id'] . ', customer ' . $_SESSION['customer']['customer_id']);
        
        $sql = "UPDATE fevent SET fuser_id = :user_id, fcustomer_id = :customer_id
				WHERE fevent_id = :fevent_id;";
        $this->_pdoObj = dbconnection::getInstance();
        $pdoStatement = $this->_pdoObj->prepare($sql, array(
            PDO::ATTR_CURSOR => PDO::CURSOR_FWDONLY
        ));
        $sqlArr = array(
            ':user_id' => $_SESSION['user']['id'],
            'customer_id' => $_SESSION['customer']['customer_id'],
            'fevent_id' => $fevent_id
        );
        try {
            $pdoStatement->execute($sqlArr);
        } catch (Exception $e) {
            $this->informUser('ERROR' . $e->getMessage());
        }
        
        if ($pdoStatement->errorCode() != 0) {
            $this->informUser(gettext('We had a PDO problem importing the event data.') . ' ' . $pdoStatement->errorInfo());
        }
        
        if ($errCounter > 0) {
            $this->informUser('[e] ' . gettext('Event takeover cancelled.'));
        } else {
            $this->informUser(gettext('Done: The event has been taken over.'));
            // unlink($statusFile);
        }
    }

    private function pullEventDataFromRemoteServer($url, $datatype, $username, $password, $fevent_id, $customer_id, $randX, $ts)
    {
        $data = array(
            'err' => 0
        );
        $contextArr = array(
            'http' => array(
                'method' => 'GET',
                'header' => "Authorization: Basic " . base64_encode($username . ":" . $password)
            )
        );
        $context = stream_context_create($contextArr);
        
        $feventRecordDumpUrl = $url . '/api/events/' . $fevent_id . '/eventdump?fevent_id=' . $fevent_id . '&customer_id=' . $customer_id . '&randX=' . $randX . '&ts=' . $ts;
        if (! is_dir(BASEDIR . 'tmp/transfer_in')) {
            mkdir(BASEDIR . 'tmp/transfer_in');
        }
        if (! is_dir(BASEDIR . 'tmp/transfer_in/' . $_SESSION['customer']['customer_id'])) {
            mkdir(BASEDIR . 'tmp/transfer_in/' . $_SESSION['customer']['customer_id']);
        }
        $fn = 'c' . $customer_id . '_e' . $fevent_id . '_ts' . $ts . '_r' . $randX . '.tar.bz2';
        $localFileName = BASEDIR . 'tmp/transfer_in/' . $_SESSION['customer']['customer_id'] . '/' . $fn;
        $statusFileName = BASEDIR . 'tmp/transfer_in/status_' . substr($fn, 0, - 8);
        try {
            // echo "Headers:\n";
            stream_context_set_default($contextArr);
            $headers = get_headers($feventRecordDumpUrl);
            // $x = print_r($headers, true);
            foreach ($headers as $hdr) {
                if (substr($hdr, 0, 14) == 'Content-Dispos') {
                    $remotefilenameparts = explode('=', $hdr);
                    $remotefilename = $remotefilenameparts[1];
                }
            }
            $remoteFilenameXParts = explode('_', $remotefilenameparts[1]);
            $remoteCustomerId = substr($remoteFilenameXParts[0], 1);
            $myLocalRemoteFilename = BASEDIR . 'tmp/transfer_in/' . $_SESSION['customer']['customer_id'] . '/' . $remotefilename;
            file_put_contents($myLocalRemoteFilename, fopen($feventRecordDumpUrl, 'r', false, $context));
            file_put_contents($statusFileName, 'downloaded;' . $_SESSION['customer']['customer_id'] . ';' . $remoteCustomerId);
        } catch (Exception $e) {
            $this->informUser('[e] ' . gettext('Error pulling fevent record.'));
        }
        $data['localfilename'] = $myLocalRemoteFilename;
        if (is_file($myLocalRemoteFilename)) {
            $data['size'] = filesize($myLocalRemoteFilename);
        } else {
            $data['size'] = null;
            $data['err'] += 1;
        }
        return $data;
    }

    /**
     * Outputs a message to the browser via "echo" - and flushes the output cache.
     * This allows us to show status messages to the user while performing a very long running https request.
     * These long http requests occur, when the target server needs time to process data.
     * 
     * @param String $txt *** special: If the string begins with "[e] ", it is considered to be an error
     *                    and formated like an error.
     */
    private function informUser($txt)
    {
        if (substr($txt, 0, 4) == '[e] ') {
            echo '<span class="informUserError">' . date('d.m.Y H:i:s') . ' - ' . gettext('Error') . ': ' . substr($txt, 4) . '</span><br />' . "\n";
        } else {
            echo date('d.m.Y H:i:s') . ' - ' . $txt . '<br />' . "\n";
        }
        flush();
        ob_end_flush();
    }

    private function pushovertoserver($fevent_id_local, $fevent_id_remote, $url, $username, $password)
    {
        $errCounter = 0;
        header('Content-type: text/html; charset=utf-8');
        $this->informUser(gettext('Triggering the creation of the database dump.') . '...');
        // 1. trigger creation of a mysql dump (existing functionality?!)
        $triggerData = $this->triggerPushoverPreparation($fevent_id_local, $fevent_id_remote, $url, $username, $password);
        
        // 2. wait for sql dump to be created
        $maxLoops = 24;
        $nowLoops = 0;
        $done = false;
        $checkurl = $triggerData['checkfile'];
        while ($done === false && $nowLoops < $maxLoops) {
            $nowLoops ++;
            $checkFileContent = file_get_contents($checkurl, false, $context);
            if (strpos($checkFileContent, 'sqlgenerated=y') !== false) {
                $done = true;
                $this->informUser(gettext('Done.'));
            } else {
                $this->informUser(gettext('waiting...'));
            }
            if ($done === false) {
                sleep(10);
            }
        }
        
        // 3. wait for upload of that mysql dump
        $this->informUser(gettext('Uploading database dump to server ... ') . $url);
        $maxLoops = 24;
        $nowLoops = 0;
        $done = false;
        $checkurl = $triggerData['checkfile'];
        while ($done === false && $nowLoops < $maxLoops) {
            $nowLoops ++;
            $checkFileContent = file_get_contents($checkurl, false, $context);
            if (strpos($checkFileContent, 'pushtransferred=y') !== false) {
                $done = true;
                $this->informUser(gettext('Done.'));
            } else {
                $this->informUser(gettext('waiting...'));
            }
            if ($done === false) {
                sleep(10);
            }
        }
        
        // 4. wait for import of that mysql dump on the other server
        $this->informUser(gettext('Importing database dump to remote database ...') . ' ' . $url);
        $maxLoops = 24;
        $nowLoops = 0;
        $done = false;
        // checkurl is different now, because on the other server
        $checkurl = $url . '/api/events/' . $fevent_id_local . '/checkPushoverStatus?randX=' . $triggerData['rand'] . '&ts=' . $triggerData['ts'] . '&fevent_id=' . $fevent_id_local;
        echo $checkurl;
        $context = stream_context_create(array(
            'http' => array(
                'method' => 'PUT',
                'header' => "Authorization: Basic " . base64_encode($username . ":" . $password)
            )
        ));
        
        while ($done === false && $nowLoops < $maxLoops) {
            $nowLoops ++;
            $checkFileContent = file_get_contents($checkurl, false, $context);
            if (strpos($checkFileContent, 'loaded') !== false || strpos($checkFileContent, 'done') !== false) {
                $done = true;
                $this->informUser(gettext('Done.'));
            } else {
                $this->informUser(gettext('waiting...') . ' ' . $checkFileContent);
            }
            if ($done === false) {
                sleep(10);
            }
        }
        
        // 5. wait for success message from other server
        // 4. wait for import of that mysql dump on the other server
        $this->informUser(gettext('Importing database dump to remote database ...') . ' ' . $url);
        $maxLoops = 24;
        $nowLoops = 0;
        $done = false;
        while ($done === false && $nowLoops < $maxLoops) {
            $nowLoops ++;
            $checkFileContent = file_get_contents($checkurl, false, $context);
            if (strpos($checkFileContent, 'done') !== false) {
                $done = true;
                $this->informUser(gettext('The data transfer was most likely successful.'));
            } else {
                $this->informUser(gettext('waiting...'));
            }
            if ($done === false) {
                sleep(10);
            }
        }
    }

    private function triggerPushoverPreparation($fevent_id_local, $fevent_id_remote, $url, $username, $password)
    {
        $data = array();
        
        if (! is_dir($this->_triggerDir)) {
            mkdir($this->_triggerDir);
        }
        $randX = $this->generateRandomCode();
        $ts = date('YmdHis');
        $filename = 'pushoverPrep_cust' . $_SESSION['customer']['customer_id'] . '_ev' . $fevent_id_local . '__' . $ts . '_' . $randX;
        $filecontent = "new\r";
        $filecontent .= 'fevent_id_local=' . $fevent_id_local . "\r";
        $filecontent .= 'fevent_id_remote=' . $fevent_id_remote . "\r";
        $filecontent .= 'local_customer_id=' . $_SESSION['customer']['customer_id'] . "\r";
        $filecontent .= 'url=' . $url . "\r";
        $filecontent .= 'username=' . $username . "\r";
        $filecontent .= 'password=' . $password . "\r";
        $filecontent .= 'filesize=0' . "\r";
        $filecontent .= 'sqlgenerated=n' . "\r";
        $filecontent .= 'pushtransferred=n' . "\r";
        $filecontent .= 'sqlimported=n' . "\r";
        
        $checkfile = $this->_triggerDir . '/' . $filename;
        file_put_contents($checkfile, $filecontent);
        $data['ts'] = $ts;
        $data['rand'] = $randX;
        $data['fevent_id'] = $fevent_id;
        $data['checkfile'] = $checkfile;
        return $data;
    }

    /**
     * generates a random code, 6 characters long
     * 
     * @return string
     */
    private function generateRandomCode()
    {
        $blk = 'ABCDEFGHJKLMNPQRSTUVWXYZ23456789';
        $max = strlen($blk) - 1;
        $accesscode = '';
        for ($i = 0; $i < 6; $i ++) {
            $accesscode .= substr($blk, rand(0, $max), 1);
        }
        return $accesscode;
    }
}