<?php

class internalfile extends l5sys
{

    /**
     * This is the constructor
     *
     * @param string $lg
     *            the language
     * @param string $locale
     *            the locale
     */
    function __construct($lg, $locale)
    {
        $this->_locale = $locale;
        $this->_lg = $lg;
    }

    /**
     * returns a list of extensions for the api
     *
     * @return array contains events
     */
    public function apiGetList($entity = null, $params = null)
    {
        try {
            $res = null;
            $aparams = array();
            foreach ($params as $p) {
                $pParts = explode('=', $p);
                $aparams[$pParts[0]] = $pParts[1];
            }
            $res['content'] = $this->getFileList($aparams['folder']);
        } catch (Exception $e) {
            $this->addError('', 'Problem reading file list.', 1);
        }
        
        return $res;
    }

    private function getFileList($subDirectoryOfHidden = '')
    {
        $subDirectoryOfHidden = urldecode(trim($subDirectoryOfHidden));
        $subDirectoryOfHidden = str_replace('//', '', $subDirectoryOfHidden);
        if (strpos($subDirectoryOfHidden, '/..') === strlen($subDirectoryOfHidden) - 3) {
            /**
             * if there is a ".." at the end of the subdirectory, we need to cd up one level.
             * In that case, we have to remove the last directory part from the dir, in order
             * to change "/blabla/blubb/hi/.." to "/blabla/blubb/hi".
             */
            $subDirectoryOfHidden = substr($subDirectoryOfHidden, 0, - 3);
            $positionOfLastSlash = strrpos($subDirectoryOfHidden, '/');
            $subDirectoryOfHidden = substr($subDirectoryOfHidden, 0, $positionOfLastSlash);
        } elseif (strpos($subDirectoryOfHidden, '..') !== false) {
            $this->addError('', gettext('No access to the requested directory.'), 1);
        }
        
        if (substr($subDirectoryOfHidden, 0, 1) != '/') {
            $subDirectoryOfHidden = '/' . $subDirectoryOfHidden;
        }
        $dir = BASEDIR . 'files/' . $_SESSION['customer']['customer_id'] . '/hidden' . $subDirectoryOfHidden;
        $downloaddir = '/api/downloads/' . $_SESSION['customer']['customer_id'] . '/hidden';
        if ($subDirectoryOfHidden != '/') {
            $downloaddir .= $subDirectoryOfHidden;
        }
        if (! is_dir($dir)) {
            $this->addError('', gettext('Directory does not exist.') . ' (' . $subDirectoryOfHidden . ')');
        } else {
            $content = scandir($dir);
            $sortedContent = array(
                'path' => $subDirectoryOfHidden
            );
            if ($subDirectoryOfHidden != '/') {
                $sortedContent['parentDirectoryPermitted'] = '1';
            }
            foreach ($content as $c) {
                if ($c == '.') {} elseif ($c == '..') {} elseif (is_dir($dir . '/' . $c)) {
                    $sortedContent['directories'][] = array(
                        $c,
                        0,
                        filemtime($dir . '/' . $c)
                    );
                } elseif (is_file($dir . '/' . $c)) {
                    $sortedContent['files'][] = array(
                        $c,
                        filesize($dir . '/' . $c),
                        filemtime($dir . '/' . $c),
                        $downloaddir . '/' . $c
                    );
                }
            }
        }
        return $sortedContent;
    }

    public function getList()
    {
        return $this->getFileList();
    }

    public function getForOptionList()
    {
        $res = array();
        /*
         * $l = $this->getExtensionList();
         * foreach($l as $lx) {
         * $res[$lx['extension_id']] = $lx['fname'] . ' - ' . $lx['title'];
         * }
         */
        return $res;
    }

    /*
     * private function getExtensionList() {
     * $listOfExtensions = array ();
     * try {
     * $sql = "SELECT * FROM extension WHERE active = 1;";
     * $this->_pdoObj = dbconnection::getInstance();
     * $pdoStatement = $this->_pdoObj->prepare($sql, array (
     * PDO::ATTR_CURSOR => PDO::CURSOR_FWDONLY
     * ));
     * $pdoStatement->execute(Array ());
     * if ($pdoStatement->errorCode() * 1 != 0) {
     * $this->addError('', $pdoStatement->errorInfo(), 1);
     * } else {
     * while ($row = $pdoStatement->fetch(PDO::FETCH_ASSOC)) {
     * $listOfExtensions[] = $row;
     * }
     * }
     * } catch (Exception $e) {
     * $this->addError('', 'Error reading extension list', 1, $e->getMessage());
     * }
     * return $listOfExtensions;
     * }
     */
    public function getExtensionName($id)
    {
        $extensionName = "";
        try {
            $sql = "SELECT fname FROM extension WHERE extension_id = :id;";
            $this->_pdoObj = dbconnection::getInstance();
            $pdoStatement = $this->_pdoObj->prepare($sql, array(
                PDO::ATTR_CURSOR => PDO::CURSOR_FWDONLY
            ));
            $pdoStatement->execute(Array(
                'id' => $id
            ));
            if ($pdoStatement->errorCode() * 1 != 0) {
                $this->addError('', $pdoStatement->errorInfo(), 1);
            } else {
                $row = $pdoStatement->fetch(PDO::FETCH_ASSOC);
                $extensionName = $row['fname'];
            }
        } catch (Exception $e) {
            $this->addError('', 'Error reading extension name', 1, $e->getMessage());
        }
        return $extensionName;
    }

    /**
     * Saves a record via the API
     */
    public function apiPutRecord($put)
    {
        try {
            $dir = BASEDIR . 'files/' . $_SESSION['customer']['customer_id'] . '/hidden';
            if (! is_dir($dir)) {
                mkdir($dir);
            }
            if (isset($put['newdirname'])) {
                $newdirname = trim($put['newdirname']);
                $newdirname = str_replace('..', '', $newdirname);
                $foldername = str_replace('..', '', $put['foldername']);
                if (substr($foldername, 0, 1) != '/') {
                    $foldername = '/' . $foldername;
                }
                if (substr($newdirname, 0, 1) != '/') {
                    $newdirname = '/' . $newdirname;
                }
                if (strlen($newdirname) > 0 && substr($newdirname, 0, 1) != '.') {
                    if (is_dir($dir . '/' . $foldername)) {
                        $completeNewFolder = $dir . $foldername . $newdirname;
                        if ($foldername == '/') {
                            $completeNewFolder = $dir . $newdirname;
                        }
                        if (! @mkdir($completeNewFolder)) {
                            $error = error_get_last();
                            $this->addError('', gettext('Error creating new directory.'), $error['message']);
                        }
                    } else {
                        $this->addError('', gettext('The directory does not exist.'));
                    }
                }
            } elseif (isset($put['delete']) && $put['delete'] == 'folder') {
                $dfolder = trim($put['folder']);
                if (strpos('..', $dfolder) === false) {
                    if (substr($dfolder, 0, 1) != '/') {
                        $dfolder = '/' . $dfolder;
                    }
                    
                    if (! @rmdir($dir . $dfolder)) {
                        $error = error_get_last();
                        $this->addError('', $dfolder . ' ' . gettext('not deleted.') . ' ' . $error['message'], 1);
                    }
                }
            } elseif (isset($put['delete']) && $put['delete'] == 'file') {
                $dfile = trim($put['file']);
                if (strpos('..', $dfile) === false) {
                    if (substr($dfile, 0, 1) != '/') {
                        $dfile = '/' . $dfile;
                    }
                    if (is_file($dir . $dfile)) {
                        if (! @unlink($dir . $dfile)) {
                            $error = error_get_last();
                            $this->addError('', $dfile . ' ' . gettext('not deleted.') . ' ' . $error['message'], 1);
                        }
                    }
                }
            } else {
                $subfoldername = trim(urldecode($put['foldername']));
                if ($subfoldername == '' || strpos('..', $subfoldername) === false) {
                    $speicherOrtUndName = $dir . '/' . $_FILES['datei']['name'];
                    if (strlen($subfoldername) > 0 && $subfoldername != '/') {
                        if (substr($subfoldername, 0, 1) != '/') {
                            $subfoldername = '/' . $subfoldername;
                        }
                        $speicherOrtUndName = $dir . $subfoldername . '/' . $_FILES['datei']['name'];
                    }
                    if (move_uploaded_file($_FILES['datei']['tmp_name'], $speicherOrtUndName) == false) {
                        $this->addError('datei', gettext("Error saving the file."), 1);
                    }
                } else {
                    $this->addError('', gettext('Invalid folder name.'), 1, print_r($put));
                }
            }
            // $this->addFile($put['fevent_id'], $put['extension_id'], $put['description']);
        } catch (Exception $e) {
            $this->addError('', gettext('Unknown error.'), 1, print_r($e));
        }
    }

    /**
     *
     * @param String $select
     *            Values: 1 = select one file; 0 = do not select any file; n = select multiple files; d = select one directory; m = select multiple directories
     */
    private function printFileManager($select)
    {
        $internalfiles = $this->getFileList('');
        $sm = new L5Smarty();
        $sm->assign('internalfiles', $internalfiles);
        $sm->assign('select', $select);
        $xout = $sm->fetch('filemanager/fmpage.tpl');
        return $xout;
    }

    public function apiInvokeMethod($methodname)
    {
        switch ($methodname) {
            case 'filemanager':
                // $this->enableCRMModule();
                $xout = array(
                    'html' => $this->printFileManager($_POST['select'])
                );
                // echo $xout;
                
                break;
            default:
                $this->addError('', 'method ' . $methodname . ' has not been implemented yet. ' . $this->checkCRMModule());
        }
        return $xout;
    }
}