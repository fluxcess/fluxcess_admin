<?php

/**
 * 
 * @author fluxcess GmbH
 *
 */
class multisettings extends l5sys
{

    const ID_REGEXP = '/[^A-Za-z\d.]/i';

    private static $_multisettings = array(
        'optioncollection' => array(
            'fields' => array(
                array(
                    'name' => 'id',
                    'caption' => 'Id',
                    'type' => 'char'
                ),
                array(
                    'name' => 'title',
                    'caption' => 'Title',
                    'type' => 'char'
                )
            )
        ),
        'optioncollectionvalue' => array(
            'fields' => array(
                array(
                    'name' => 'id',
                    'caption' => 'Id',
                    'type' => 'char'
                ),
                array(
                    'name' => 'optioncollectionid',
                    'caption' => 'Option Collection',
                    'type' => 'char',
                    'dropdown' => array(
                        'keys' => 'optioncollection>id',
                        'values' => 'optioncollection>title'
                    )
                ),
                array(
                    'name' => 'title',
                    'caption' => 'Title',
                    'type' => 'text'
                ),
                array(
                    'name' => 'val',
                    'caption' => 'Value',
                    'type' => 'text'
                ),
                array(
                    'name' => 'preselected',
                    'caption' => 'Preselected',
                    'type' => 'int'
                ),
                array(
                    'name' => 'hidesectionlist',
                    'caption' => 'Hide Sections',
                    'type' => 'text'
                ),
                array(
                    'name' => 'showsectionlist',
                    'caption' => 'Show Sections',
                    'type' => 'text'
                )
            )
        ),
        'conditionalautomail' => array(
            'fields' => array(
                array(
                    'name' => 'id',
                    'caption' => 'Id',
                    'type' => 'char'
                ),
                array(
                    'name' => 'emailtemplateid',
                    'caption' => 'Email Template Id',
                    'type' => 'int'
                ),
                array(
                    'name' => 'emailtrigger',
                    'caption' => 'Email Trigger',
                    'type' => 'char'
                ),
                array(
                    'name' => 'emailcondition',
                    'caption' => 'Email Condition',
                    'type' => 'text'
                )
            )
        ),
        'formpage' => array(
            '1n' => array(
                array(
                    'multisettingname' => 'n1formpagesectiontoformpage',
                    'keyfield' => 'id'
                )
            ),
            'fields' => array(
                array(
                    'name' => 'id',
                    'caption' => 'Id',
                    'type' => 'char'
                ),
                array(
                    'name' => 'pagetitle',
                    'caption' => 'Page Title',
                    'type' => 'char'
                ),
                array(
                    'name' => 'directaccess',
                    'caption' => 'Direct Access',
                    'type' => 'int'
                ),
                array(
                    'name' => 'backallowed',
                    'caption' => 'Backwards Navigation Allowed',
                    'type' => 'int'
                ),
                array(
                    'name' => 'checkout',
                    'caption' => 'Checkout',
                    'type' => 'int'
                ),
                array(
                    'name' => 'buynow',
                    'caption' => 'Buy Now',
                    'type' => 'char'
                ),
                array(
                    'name' => 'n1formpagesectiontoformpage',
                    'caption' => 'Form Page Section',
                    'type' => 'char',
                    'n1' => 1,
                    'dropdown' => array(
                        'keys' => 'formpagesection>id',
                        'values' => 'formpagesection>type'
                    )
                )
            )
        ),
        'formpagesection' => array(
            // 'idcol' => 'sectionid',
            'fields' => array(
                array(
                    'name' => 'id',
                    'caption' => 'Section Id',
                    'type' => 'char',
                    'isid' => true
                ),
                array(
                    'name' => 'formpageid',
                    'caption' => 'Form Page',
                    'type' => 'char',
                    'dropdown' => array(
                        'keys' => 'formpage>id',
                        'values' => 'formpage>pagetitle'
                    )
                ),
                array(
                    'name' => 'type',
                    'caption' => 'Type',
                    'type' => 'char',
                    'options' => array(
                        'fields' => 'Fields',
                        'sessionchoice' => 'Session Choice',
                        'tcatchoice' => 'Ticket Category Choice',
                        'loginchoice' => 'Login Choice',
                        'summary' => 'Summary',
                        'beforebuyconfirmation' => 'Before Buy Confirmation',
                        'confirmation' => 'Confirmation'
                    )
                ),
                array(
                    'name' => 'content',
                    'caption' => 'Content',
                    'type' => 'text'
                ),
                array(
                    'name' => 'minimumchoices',
                    'caption' => 'Minimum number of choices',
                    'type' => 'int'
                ),
                array(
                    'name' => 'maximumchoices',
                    'caption' => 'Maximum number of choices',
                    'type' => 'int'
                ),
                array(
                    'name' => 'name',
                    'caption' => 'Name',
                    'type' => 'char'
                ),
                array(
                    'name' => 'invoiceparentfield',
                    'caption' => 'Parent field in invoice',
                    'type' => 'char'
                ),
                array(
                    'name' => 'streamlist',
                    'caption' => 'List of Streams',
                    'type' => 'text'
                ),
                array(
                    'name' => 'maxwidth',
                    'caption' => 'Maximum width',
                    'type' => 'char'
                ),
                array(
                    'name' => 'sessionlayout',
                    'caption' => 'Id of the session layout',
                    'type' => 'int'
                ),
                array(
                    'name' => 'thbackgroundcolor',
                    'caption' => 'Headline background color',
                    'type' => 'char'
                ),
                array(
                    'name' => 'thcolor',
                    'caption' => 'Headline font color',
                    'type' => 'char'
                ),
                array(
                    'name' => 'ticketcategorylist',
                    'caption' => 'List of ticket categories',
                    'type' => 'text'
                ),
                array(
                    'name' => 'groupname',
                    'caption' => 'Group Name',
                    'type' => 'char'
                ),
                array(
                    'name' => 'display',
                    'caption' => 'Display',
                    'type' => 'text'
                )
            )
        ),
        'formsectionfield' => array(
            // 'idcol' => 'fieldid',
            'fields' => array(
                array(
                    'name' => 'id',
                    'caption' => 'Field Id',
                    'type' => 'char',
                    'isid' => true
                ),
                array(
                    'name' => 'formsectionid',
                    'caption' => 'Form Section',
                    'type' => 'char',
                    'dropdown' => array(
                        'keys' => 'formpagesection>id',
                        'values' => 'formpagesection>id'
                    )
                ),
                array(
                    'name' => 'type',
                    'caption' => 'Type',
                    'type' => 'char',
                    'options' => array(
                        'html' => 'HTML',
                        'radio' => 'Radiobuttons',
                        'select' => 'Dropdown Box',
                        'input' => 'Text Input',
                        'textarea' => 'Textarea',
                        'checkbox' => 'Checkbox'
                    )
                ),
                array(
                    'name' => 'content',
                    'caption' => 'content',
                    'type' => 'text'
                ),
                array(
                    'name' => 'minimumchoices',
                    'caption' => 'Minimum Choices',
                    'type' => 'int'
                ),
                array(
                    'name' => 'maximumchoices',
                    'caption' => 'Maximum Choices',
                    'type' => 'int'
                ),
                array(
                    'name' => 'name',
                    'caption' => 'Name',
                    'type' => 'char'
                ),
                array(
                    'name' => 'ticketchoice',
                    'caption' => 'Ticket choice',
                    'type' => 'text'
                ),
                array(
                    'name' => 'caption',
                    'caption' => 'Caption',
                    'type' => 'text'
                ),
                array(
                    'name' => 'mandatory',
                    'caption' => 'Mandatory',
                    'type' => 'int'
                ),
                array(
                    'name' => 'optionset',
                    'caption' => 'Option Set',
                    'type' => 'char'
                ),
                array(
                    'name' => 'minlength',
                    'caption' => 'Minimum Length',
                    'type' => 'int'
                ),
                array(
                    'name' => 'maxlength',
                    'caption' => 'Maximum Length',
                    'type' => 'int'
                )
            )
        ),
        'conferencestream' => array(
            'fields' => array(
                array(
                    'name' => 'title',
                    'caption' => 'Title',
                    'type' => 'text'
                ),
                array(
                    'name' => 'id',
                    'caption' => 'Id',
                    'type' => 'char'
                )
            )
        ),
        'conferencesession' => array(
            'fields' => array(
                array(
                    'name' => 'datetime',
                    'caption' => 'Start time',
                    'type' => 'datetime'
                ),
                array(
                    'name' => 'datetimeto',
                    'caption' => 'End time',
                    'type' => 'datetime'
                ),
                array(
                    'name' => 'type',
                    'caption' => 'Type',
                    'type' => 'char'
                ),
                array(
                    'name' => 'speaker',
                    'caption' => 'Speaker',
                    'type' => 'text'
                ),
                array(
                    'name' => 'title',
                    'caption' => 'Title',
                    'type' => 'text'
                ),
                array(
                    'name' => 'descr',
                    'caption' => 'Description',
                    'type' => 'text'
                ),
                array(
                    'name' => 'speakerpic',
                    'caption' => 'Speaker Picture',
                    'type' => 'text'
                ),
                array(
                    'name' => 'no',
                    'caption' => 'Public Session Code / Number',
                    'type' => 'char'
                ),
                array(
                    'name' => 'id',
                    'caption' => 'ID',
                    'type' => 'char'
                ),
                array(
                    'name' => 'linked',
                    'caption' => 'Linked Sessions',
                    'type' => 'text'
                ),
                array(
                    'name' => 'stream',
                    'caption' => 'Stream(s)',
                    'type' => 'text'
                ),
                array(
                    'name' => 'maxpax',
                    'caption' => 'Maximum capacity',
                    'type' => 'int'
                ),
                array(
                    'name' => 'bookedpax',
                    'caption' => 'Booked capacity',
                    'type' => 'int'
                )
            )
        ),
        'quickaddformfield' => array(
            'fields' => array(
                array(
                    'name' => 'id',
                    'caption' => 'Field Id',
                    'type' => 'char',
                    'isid' => true
                ),
                array(
                    'name' => 'type',
                    'caption' => 'Type',
                    'type' => 'char',
                    'options' => array(
                        'html' => 'HTML',
                        'radio' => 'Radiobuttons',
                        'select' => 'Dropdown Box',
                        'input' => 'Text Input',
                        'textarea' => 'Textarea',
                        'checkbox' => 'Checkbox'
                    )
                ),
                array(
                    'name' => 'content',
                    'caption' => 'content',
                    'type' => 'text'
                ),
                array(
                    'name' => 'minimumchoices',
                    'caption' => 'Minimum Choices',
                    'type' => 'int'
                ),
                array(
                    'name' => 'maximumchoices',
                    'caption' => 'Maximum Choices',
                    'type' => 'int'
                ),
                array(
                    'name' => 'name',
                    'caption' => 'Name',
                    'type' => 'char'
                ),
                array(
                    'name' => 'ticketchoice',
                    'caption' => 'Ticket choice',
                    'type' => 'text'
                ),
                array(
                    'name' => 'caption',
                    'caption' => 'Caption',
                    'type' => 'text'
                ),
                array(
                    'name' => 'mandatory',
                    'caption' => 'Mandatory',
                    'type' => 'int'
                ),
                array(
                    'name' => 'optionset',
                    'caption' => 'Option Set',
                    'type' => 'char'
                ),
                array(
                    'name' => 'minlength',
                    'caption' => 'Minimum Length',
                    'type' => 'int'
                ),
                array(
                    'name' => 'maxlength',
                    'caption' => 'Maximum Length',
                    'type' => 'int'
                )
            )
        ),
        'roomset' => array(
            '1n' => array(
                array(
                    'multisettingname' => 'n1roomtoset',
                    'keyfield' => 'id'
                )
            ),
            'fields' => array(
                array(
                    'name' => 'id',
                    'caption' => 'Id',
                    'type' => 'char'
                ),
                array(
                    'name' => 'title',
                    'caption' => 'Title',
                    'type' => 'char'
                ),
                array(
                    'name' => 'n1roomtoset',
                    'caption' => 'Room',
                    'type' => 'char',
                    'n1' => 1,
                    'dropdown' => array(
                        'keys' => 'room>id',
                        'values' => 'room>title'
                    )
                )
            )
        ),
        
        'room' => array(
            'fields' => array(
                array(
                    'name' => 'title',
                    'caption' => 'Room name',
                    'type' => 'char'
                ),
                array(
                    'name' => 'maxpax',
                    'caption' => 'Maximum capacity',
                    'type' => 'int'
                ),
                array(
                    'name' => 'id',
                    'caption' => 'ID',
                    'type' => 'char'
                )
            )
        ),
        'ticketcategory' => array(
            'fields' => array(
                array(
                    'name' => 'id',
                    'caption' => 'Id',
                    'type' => 'char'
                ),
                array(
                    'name' => 'ticketcategorycode',
                    'caption' => 'Ticketcode',
                    'type' => 'text'
                ),
                array(
                    'name' => 'title',
                    'caption' => 'Title',
                    'type' => 'text'
                ),
                array(
                    'name' => 'price',
                    'caption' => 'Price',
                    'type' => 'decimal'
                ),
                array(
                    'name' => 'vatamount',
                    'caption' => 'VAT amount',
                    'type' => 'decimal'
                ),
                array(
                    'name' => 'vatrate',
                    'caption' => 'VAT rate',
                    'type' => 'decimal'
                ),
                array(
                    'name' => 'pricenet',
                    'caption' => 'Net Price',
                    'type' => 'decimal'
                ),
                array(
                    'name' => 'desc',
                    'caption' => 'Description (public)',
                    'type' => 'text'
                ),
                array(
                    'name' => 'invoicetext',
                    'caption' => 'Invoice text',
                    'type' => 'text'
                ),
                array(
                    'name' => 'availablefrom',
                    'caption' => 'verfügbar ab',
                    'type' => 'datetime'
                ),
                array(
                    'name' => 'availableuntil',
                    'caption' => 'verfügbar bis',
                    'type' => 'datetime'
                )
            )
        ),
        'checkinformfield' => array(
            // 'idcol' => 'fieldid',
            'fields' => array(
                array(
                    'name' => 'id',
                    'caption' => 'Field Id',
                    'type' => 'char',
                    'isid' => true
                ),
                array(
                    'name' => 'formsectionid',
                    'caption' => 'Form Section',
                    'type' => 'char',
                    'options' => array(
                        'area1' => 'Area 1',
                        'area2' => 'Area 2',
                        'area3' => 'Area 3'
                    )
                ),
                array(
                    'name' => 'type',
                    'caption' => 'Type',
                    'type' => 'char',
                    'options' => array(
                        'chosebuttons' => 'Buttons with counter',
                        'infodisplay' => 'Info Display',
                        'inputfield' => 'Input Field',
                        'dropdown' => 'Dropdown'
                    )
                ),
                array(
                    'name' => 'minimumchoices',
                    'caption' => 'Minimum Choices',
                    'type' => 'int'
                ),
                array(
                    'name' => 'maximumchoices',
                    'caption' => 'Maximum Choices',
                    'type' => 'int'
                ),
                array(
                    'name' => 'requiredcondition',
                    'caption' => 'Required Field',
                    'type' => 'text'
                ),
                array(
                    'name' => 'roomsetid',
                    'caption' => 'Room Set',
                    'type' => 'char',
                    'dropdown' => array(
                        'keys' => 'roomset>id',
                        'values' => 'roomset>title'
                    ),
                    'dropdownaddempty' => 1
                ),
                array(
                    'name' => 'fieldname',
                    'caption' => 'Field Name',
                    'type' => 'char'
                ),
                array(
                    'name' => 'whattoshow',
                    'caption' => 'What to display',
                    'type' => 'char',
                    'options' => array(
                        'fieldvalue' => 'Always show field value',
                        'customtext' => 'Show custom text, if field value equals 1'
                    )
                ),
                array(
                    'name' => 'customtext',
                    'caption' => 'Custom Text',
                    'type' => 'text'
                ),
                array(
                    'name' => 'backgroundcolor',
                    'caption' => 'Background Color',
                    'type' => 'char'
                ),
                array(
                    'name' => 'color',
                    'caption' => 'Color',
                    'type' => 'char'
                ),
                array(
                    'name' => 'filledbyscanner',
                    'caption' => 'Filled by Scanner',
                    'type' => 'char'
                )
            )
        )
    );

    private static $_n1types = array(
        'n1roomtoset',
        'n1formpagesectiontoformpage'
    );

    public static function getMultiSettingTypes()
    {
        return multisettings::$_multisettings;
    }

    public static function getMultiSettingSubTypes()
    {
        return multisettings::$_n1types;
    }

    public function printMultiSettingsForm($eventsettings, $params)
    {
        $res = array();
        try {
            $multisettingtype = $params['multisetting'];
            if (isset(multisettings::$_multisettings[$multisettingtype])) {
                // $eventsettings = $this->readRecord($this->_recordId, true);
                
                $sm = new L5Smarty();
                $sm->assign('fevent_id', $eventsettings['fevent_id']);
                $sm->assign('multisettingtype', $multisettingtype);
                $sm->assign('seqid', $params['seqid']);
                $thesettings = $eventsettings[$multisettingtype][$params['seqid']];
                $sm->assign('ps', $thesettings);
                // values for select/option fields
                foreach (multisettings::$_multisettings as $msname => $ms) {
                    foreach ($ms['fields'] as $field) {
                        if (is_array($field['options'])) {
                            $sm->assign('options_' . $msname . '_' . $field['name'], $field['options']);
                        }
                        if (is_array($field['dropdown'])) {
                            $key_parts = explode('>', $field['dropdown']['keys']);
                            $val_parts = explode('>', $field['dropdown']['values']);
                            $options = multisettings::getAllSettings($eventsettings['fevent_id'], $key_parts[0], $key_parts[1], $val_parts[0], $val_parts[1]);
                            if (isset($field['dropdownaddempty']) && $field['dropdownaddempty'] == 1) {
                                $addedNothing = array(
                                    '' => '-'
                                );
                                $options = array_merge($addedNothing, $options);
                            }
                            $sm->assign('dropdown_' . $msname . '_' . $field['name'], $options);
                        }
                        
                        if (isset($field['n1']) && $field['n1'] == true) {
                            $sm->assign($field['name'], $this->readSubSettings($eventsettings['fevent_id'], $msname, $thesettings['id'], $field['name']));
                        }
                    /**
                     * 'dropdown' => array(
                     * 'keys' => 'formpage>id',
                     * 'values' => 'formpage>title'
                     * )
                     */
                    }
                }
                $res['content'] = $sm->fetch('event/multisettingsform.' . $multisettingtype . '.tpl');
            } else {
                $this->addError('', gettext('Multisetting type not known.'), 1, print_r($params, true));
            }
        } catch (Exception $e) {
            $this->addError('', gettext('Sorry, there was a problem loading the settings form.') . ' ' . gettext('Please try again.'), 1, print_r($e, true));
        }
        if (count($this->_err) > 0) {
            $res['err'] = $this->getErrorsInApiFormat();
        }
        return $res;
    }

    /**
     * n1
     *
     * @param unknown $fevent_id
     * @param unknown $parent_multiname
     * @param unknown $parent_multiid
     * @param unknown $multiname
     * @return unknown[]
     */
    private function readSubSettings($fevent_id, $parent_multiname, $parent_multiid, $multiname)
    {
        $subSettings = array();
        try {
            $sql = "SELECT * FROM `fevset_" . $fevent_id . "`
					WHERE pskey_multiname = :parent_multiname
					AND pskey_multiid = :parent_multiid
					AND skey_multiname = :multiname
					ORDER BY skey_multiseq ASC
					;";
            $sqlArr = array(
                ':parent_multiname' => $parent_multiname,
                ':parent_multiid' => $parent_multiid,
                ':multiname' => $multiname
            );
            $this->_pdoObj = \dbconnection::getInstance();
            
            $pdoStatement = $this->_pdoObj->prepare($sql, array(
                \PDO::ATTR_CURSOR => \PDO::CURSOR_FWDONLY
            ));
            $pdoStatement->execute($sqlArr);
            if ($pdoStatement->errorCode() * 1 != 0) {
                $this->addError('', gettext('SQL Error setting ') . $f . print_r($pdoStatement->errorInfo(), true), 1);
            } else {
                while ($row = $pdoStatement->fetch(PDO::FETCH_ASSOC)) {
                    $subSettings[] = $row;
                }
            }
        } catch (Exception $e) {
            $this->addError('', gettext('There was a problem loading the subsetting.'), 1, print_r($e, true));
        }
        return $subSettings;
    }

    public function removeMultisettingsRecord($fevent_id, $multisettingtype, $multisettingid)
    {
        try {
            
            // start transaction
            $this->_pdoObj = \dbconnection::getInstance();
            $this->_pdoObj->beginTransaction();
            
            // get sequence of relevant record
            $sql = "SELECT skey_multiseq FROM fevset_" . $fevent_id . " WHERE skey_multiname = :multisettingtype
					AND skey_multiid = :multisettingid;";
            $pdoStatement = $this->_pdoObj->prepare($sql, array(
                \PDO::ATTR_CURSOR => \PDO::CURSOR_FWDONLY
            ));
            $sqlArr = array(
                ':multisettingtype' => $multisettingtype,
                ':multisettingid' => $multisettingid
            );
            $pdoStatement->execute($sqlArr);
            if ($pdoStatement->errorCode() * 1 != 0) {
                $this->addError('', gettext('SQL Error setting ') . $f . print_r($pdoStatement->errorInfo(), true), 1);
            } else {
                $relevantRecord = $pdoStatement->fetch(PDO::FETCH_ASSOC);
            }
            if ($relevantRecord > 0) {
                // delete relevant record
                $sql = "DELETE FROM fevset_" . $fevent_id . " WHERE skey_multiname = :multisettingtype
					AND skey_multiid = :multisettingid;";
                $sqlArr = array(
                    ':multisettingtype' => $multisettingtype,
                    ':multisettingid' => $multisettingid
                );
                $pdoStatement = $this->_pdoObj->prepare($sql, array(
                    \PDO::ATTR_CURSOR => \PDO::CURSOR_FWDONLY
                ));
                $pdoStatement->execute($sqlArr);
                if ($pdoStatement->errorCode() * 1 != 0) {
                    $this->addError('', gettext('SQL Error setting ') . $f . print_r($pdoStatement->errorInfo(), true), 1);
                }
                
                // modify all records with higher sequence number
                $sql = "UPDATE fevset_" . $fevent_id . " 
					SET skey_multiseq = (skey_multiseq - 1),
					skey = CONCAT(skey_multiname, '_', skey_multiseq, '_', skey_multikey)
					WHERE skey_multiname = :multisettingtype
					AND skey_multiseq > (:multisettingseq * 1)
					ORDER BY skey_multiseq ASC;";
                $sqlArr = array(
                    ':multisettingtype' => $multisettingtype,
                    ':multisettingseq' => $relevantRecord['skey_multiseq'] * 1
                );
                $pdoStatement = $this->_pdoObj->prepare($sql, array(
                    \PDO::ATTR_CURSOR => \PDO::CURSOR_FWDONLY
                ));
                $pdoStatement->execute($sqlArr);
                if ($pdoStatement->errorCode() * 1 != 0) {
                    $this->addError('', gettext('SQL Error setting ') . $f . print_r($pdoStatement->errorInfo(), true), 1);
                }
            } else {
                $this->addError('', 'Record not found!', 1, ':-(');
            }
            
            // delete all sub-settings
            $mstypes = multisettings::$_multisettings;
            foreach ($mstypes[$multisettingtype]['fields'] as $f) {
                if (isset($f['n1']) && $f['n1'] == 1) {
                    $sql = "DELETE FROM `fevset_" . $fevent_id . "`
							WHERE pskey_multiname = :multisettingtype
							AND pskey_multiid = :multiid;";
                    $sqlArr = array(
                        ':multisettingtype' => $multisettingtype,
                        ':multiid' => $multisettingid
                    );
                    $pdoStatement = $this->_pdoObj->prepare($sql, array(
                        \PDO::ATTR_CURSOR => \PDO::CURSOR_FWDONLY
                    ));
                    $pdoStatement->execute($sqlArr);
                    if ($pdoStatement->errorCode() * 1 != 0) {
                        $this->addError('', gettext('SQL Error setting ') . $f . print_r($pdoStatement->errorInfo(), true), 1);
                    }
                }
            }
            
            // end transaction
            if (count($this->_err) == 0) {
                $this->_pdoObj->commit();
            } else {
                $this->_pdoObj->rollBack();
            }
        } catch (Exception $e) {
            $this->_pdoObj->rollBack();
            $this->addError('', gettext('Problem deleting multisettings record.'), 1, print_r($e, true));
        }
    }

    public function saveMultisettingsRecord($fevent_id, $data)
    {
        $res = array();
        try {
            $changedId = false;
            $oldid = false;
            // ToDo: is this multisettings-type allowed?
            
            $mstype = $data['multisetting'];
            $keyno = $data['seqid'];
            
            $setting_id_field = multisettings::$_multisettings[$mstype]['idcol'];
            if (strlen($setting_id_field) == 0) {
                $setting_id_field = 'id';
            }
            $setting_id = $data[$setting_id_field];
            
            if (preg_match(self::ID_REGEXP, $setting_id)) {
                $this->addError('id', gettext('Only characters and numbers are allowed as Id.'));
            }
            
            if ($keyno == 0 || ! is_numeric($keyno)) {
                $ev = new event($this->_lg, $this->_locale);
                $ev->setRecordId($fevent_id);
                $keyno = $ev->getMultisettingHighestNumber($data['multisetting']) + 1;
            } else {
                // check if the id has been changed...?!
                $oldid = $this->getMultiId($fevent_id, $data['multisetting'], $keyno);
                if ($setting_id != $oldid) {
                    $changedId = true;
                }
            }
            
            if (! isset($setting_id) || strlen(trim($setting_id)) == 0) {
                do {
                    $setting_id = $this->generateUniqueKey(8);
                    $isUnique = $this->checkIfKeyIsUnique($mstype, $setting_id);
                    $data[$setting_id_field] = $setting_id;
                } while (! $isUnique);
            }
            
            // start transaction
            $this->_pdoObj = \dbconnection::getInstance();
            $this->_pdoObj->beginTransaction();
            
            // get settings-list for current multisettingtype
            $listOfSettings = multisettings::$_multisettings[$mstype];
            if (is_array($listOfSettings)) {
                // check given settings
                switch ($mstype) {
                    case 'conferencesession':
                        $this->checkConferencesession($data, $fevent_id);
                        break;
                    case 'conferencestream':
                        $this->checkConferencestream($data);
                        break;
                    case 'formpage':
                        $this->checkFormpage($data);
                        break;
                    case 'formpagesection':
                        $this->checkFormpagesection($data, $fevent_id);
                        break;
                    case 'ticketcategory':
                        $this->checkTicketcategory($data);
                        break;
                }
                
                if (count($this->_err) == 0) {
                    // save each setting individually
                    foreach ($listOfSettings['fields'] as $f) {
                        $skey = $mstype . '_' . $keyno . '_' . $f['name'];
                        $sqlArr = array(
                            'skey' => $skey,
                            'skey_multiname' => $mstype,
                            'skey_multiseq' => $keyno,
                            'skey_multikey' => $f['name'],
                            'skey_multiid' => $setting_id,
                            'sval_txt' => null,
                            'sval_int' => null,
                            'sval_vch' => null,
                            'sval_dtt' => null,
                            'sval_dec' => null
                        );
                        switch ($f['type']) {
                            case 'text':
                                $sqlArr['sval_txt'] = $data[$f['name']];
                                break;
                            case 'int':
                                $sqlArr['sval_int'] = $data[$f['name']];
                                break;
                            case 'char':
                                $sqlArr['sval_vch'] = $data[$f['name']];
                                break;
                            case 'datetime':
                                $sqlArr['sval_dtt'] = $data[$f['name']];
                                break;
                            case 'decimal':
                                $sqlArr['sval_dec'] = $data[$f['name']];
                                break;
                        }
                        
                        $sql = "INSERT INTO `fevset_" . $fevent_id . "` (skey, sval_txt, sval_int, sval_vch, sval_dtt, sval_dec, skey_multiname, skey_multiseq, skey_multikey, skey_multiid)
								VALUES (:skey, :sval_txt, :sval_int, :sval_vch, :sval_dtt, :sval_dec, 
								:skey_multiname, :skey_multiseq, :skey_multikey, :skey_multiid)
								ON DUPLICATE KEY UPDATE 
									sval_txt = :sval_txt, sval_int = :sval_int, 
								sval_vch = :sval_vch, sval_dtt = :sval_dtt, 
								sval_dec = :sval_dec,
								skey_multiname = :skey_multiname,
								skey_multiseq = :skey_multiseq,
								skey_multikey = :skey_multikey,
								skey_multiid = :skey_multiid;";
                        $pdoStatement = $this->_pdoObj->prepare($sql, array(
                            \PDO::ATTR_CURSOR => \PDO::CURSOR_FWDONLY
                        ));
                        $pdoStatement->execute($sqlArr);
                        if ($pdoStatement->errorCode() * 1 != 0) {
                            $this->addError('', gettext('SQL Error setting ') . $f . print_r($pdoStatement->errorInfo(), true), 1);
                        }
                    }
                    
                    // if the id changed, change ids of child records
                    if (count($this->_err) == 0 && $changedId === true) {
                        foreach ($listOfSettings['fields'] as $f) {
                            if (isset($f['n1']) && $f['n1'] == 1) {
                                $sql = "UPDATE `fevset_" . $fevent_id . "` 
										SET pskey_multiid = :newid
										WHERE pskey_multiid = :oldid
										AND pskey_multiname = :multiname
										;";
                                $sqlArr = array(
                                    ':newid' => $setting_id,
                                    ':oldid' => $oldid,
                                    ':multiname' => $mstype
                                );
                                $pdoStatement = $this->_pdoObj->prepare($sql, array(
                                    \PDO::ATTR_CURSOR => \PDO::CURSOR_FWDONLY
                                ));
                                $pdoStatement->execute($sqlArr);
                                if ($pdoStatement->errorCode() * 1 != 0) {
                                    $this->addError('', gettext('SQL Error setting ') . $f . print_r($pdoStatement->errorInfo(), true), 1);
                                }
                            }
                        }
                    }
                    
                    // end transaction
                    if (count($this->_err) > 0) {
                        $this->_pdoObj->rollBack();
                    } else {
                        $this->_pdoObj->commit();
                    }
                }
            } else {
                $this->addError('', 'Multisettings-Type does not exist.', 1, print_r($data, true));
            }
        } catch (Exception $e) {
            $this->_pdoObj->rollBack();
            $this->addError('', gettext('Error while saving the multisettings record.'), 1, print_r($e, true));
        }
        return $res;
    }

    private function getMultiId($fevent_id, $multiname, $multiseq)
    {
        $multiid = false;
        try {
            $sql = "SELECT skey_multiid FROM `fevset_" . $fevent_id . "`
					WHERE skey_multiname = :multiname
					AND skey_multiseq = :multiseq
					LIMIT 1
					;";
            $sqlArr = array(
                ':multiname' => $multiname,
                ':multiseq' => $multiseq
            );
            $this->_pdoObj = \dbconnection::getInstance();
            $pdoStatement = $this->_pdoObj->prepare($sql, array(
                \PDO::ATTR_CURSOR => \PDO::CURSOR_FWDONLY
            ));
            $pdoStatement->execute($sqlArr);
            if ($pdoStatement->errorCode() * 1 != 0) {
                $this->addError('', gettext('SQL Error setting ') . $f . print_r($pdoStatement->errorInfo(), true), 1);
            } else {
                if ($row = $pdoStatement->fetch(PDO::FETCH_ASSOC)) {
                    $multiid = $row['skey_multiid'];
                }
            }
        } catch (Exception $e) {
            $this->addError('', gettext('Error while getting the multiId for a record.'), 1, print_r($e, true));
        }
        return $multiid;
    }

    /**
     * n1 1n
     * 
     * @param unknown $fevent_id
     * @param unknown $data
     */
    public function addMultisettings1NRecord($fevent_id, $data, $transaction = true)
    {
        $res = array();
        try {
            // ToDo: is this multisettings-subtype allowed?
            
            $parentmultiname = $data['parentmultiname'];
            $parentmultikey = null;
            $parentmultiid = $data['parentmultiid'];
            
            $multiname = $data['multiname'];
            $multikey = '';
            
            $mssubtype = $data['multisubsetting'];
            $val = $data['val'];
            $parentkey = $data['parentkey'];
            
            $ev = new event($this->_lg, $this->_locale);
            $ev->setRecordId($fevent_id);
            if (isset($data['skey_multiseq']) && strlen($data['skey_multiseq']) > 2) {
                $multiseq = $data['skey_multiseq'];
            } else {
                $multiseq = $ev->getMultisettingHighestNumber($multiname, $parentmultiid) + 1;
            }
            if (isset($data['skey_multiid']) && strlen($data['skey_multiid']) > 2) {
                $multiid = $data['skey_multiid'];
            } else {
                do {
                    $multiid = $this->generateUniqueKey(8);
                    $isUnique = $this->checkIfKeyIsUnique($mstype, $setting_id);
                } while (! $isUnique);
            }
            if (count($this->_err) == 0) {
                // start transaction
                $this->_pdoObj = \dbconnection::getInstance();
                if ($transaction) {
                    $this->_pdoObj->beginTransaction();
                }
                $skey = $multiname . '_' . $parentmultiid . '_' . $multiseq;
                $sqlArr = array(
                    'skey' => $skey,
                    'skey_multiname' => $multiname,
                    'skey_multiseq' => $multiseq,
                    'skey_multikey' => $multikey,
                    'skey_multiid' => $multiid,
                    'pskey_multiname' => $parentmultiname,
                    'pskey_multikey' => $parentmultikey,
                    'pskey_multiid' => $parentmultiid,
                    'sval_txt' => null,
                    'sval_int' => null,
                    'sval_vch' => null,
                    'sval_dtt' => null,
                    'sval_dec' => null
                );
                // $ftype = multisettings::$_multisettings[$parentmultiname]['fields']
                $ftype = multisettings::getMultisettingsFieldType($parentmultiname, $multiname);
                
                switch ($ftype) {
                    case 'text':
                        $sqlArr['sval_txt'] = $data['val'];
                        break;
                    case 'int':
                        $sqlArr['sval_int'] = $data['val'];
                        break;
                    case 'char':
                        $sqlArr['sval_vch'] = $data['val'];
                        break;
                    case 'datetime':
                        $sqlArr['sval_dtt'] = $data['val'];
                        break;
                    case 'decimal':
                        $sqlArr['sval_dec'] = $data['val'];
                        break;
                }
                
                $sql = "INSERT INTO `fevset_" . $fevent_id . "` 
								(skey, sval_txt, sval_int, sval_vch, sval_dtt, sval_dec, 
								skey_multiname, skey_multiseq, skey_multikey, skey_multiid,
								pskey_multiname, pskey_multikey, pskey_multiid)
								VALUES (:skey, :sval_txt, :sval_int, :sval_vch, :sval_dtt, :sval_dec,
								:skey_multiname, :skey_multiseq, :skey_multikey, :skey_multiid,
								:pskey_multiname, :pskey_multikey, :pskey_multiid)
								;";
                $pdoStatement = $this->_pdoObj->prepare($sql, array(
                    \PDO::ATTR_CURSOR => \PDO::CURSOR_FWDONLY
                ));
                $pdoStatement->execute($sqlArr);
                if ($pdoStatement->errorCode() * 1 != 0) {
                    $this->addError('', gettext('SQL Error setting ') . $f . print_r($pdoStatement->errorInfo(), true), 1);
                }
            }
            // end transaction
            if ($transaction) {
                if (count($this->_err) > 0) {
                    $this->_pdoObj->rollBack();
                } else {
                    $this->_pdoObj->commit();
                }
            }
        } catch (Exception $e) {
            if ($transaction) {
                $this->_pdoObj->rollBack();
            }
            $this->addError('', gettext('Error while adding the submultisettings record.'), 1, print_r($e, true));
        }
        return $res;
    }

    public function removeMultisetting1n($eventid, $multiname, $multiid, $pmultiname, $pmultiid)
    {
        try {
            $deletedseq = false;
            // begin transaction
            $this->_pdoObj = \dbconnection::getInstance();
            $this->_pdoObj->beginTransaction();
            
            // get current sequence number
            $sqlArr = array(
                ':multiname' => $multiname,
                ':multiid' => $multiid,
                ':pmultiname' => $pmultiname,
                ':pmultiid' => $pmultiid
            );
            $sql = "SELECT skey_multiseq FROM `fevset_" . $eventid . "`
				WHERE skey_multiname = :multiname
				AND skey_multiid = :multiid
				AND pskey_multiname = :pmultiname
				AND pskey_multiid = :pmultiid;";
            $pdoStatement = $this->_pdoObj->prepare($sql, array(
                \PDO::ATTR_CURSOR => \PDO::CURSOR_FWDONLY
            ));
            $pdoStatement->execute($sqlArr);
            if ($pdoStatement->errorCode() * 1 != 0) {
                $this->addError('', gettext('SQL Error setting ') . $f . print_r($pdoStatement->errorInfo(), true), 1);
            } else {
                $row = $pdoStatement->fetch(PDO::FETCH_ASSOC);
                $deletedseq = $row['skey_multiseq'];
            }
            if (! is_numeric($deletedseq)) {
                $this->addError('', gettext('No sequence id for sub multisetting.'), 1);
            }
            
            if (count($this->_err) == 0) {
                // delete the record
                $sql = "DELETE FROM `fevset_" . $eventid . "` 
				WHERE skey_multiname = :multiname
				AND skey_multiid = :multiid
				AND pskey_multiname = :pmultiname
				AND pskey_multiid = :pmultiid;";
                $pdoStatement = $this->_pdoObj->prepare($sql, array(
                    \PDO::ATTR_CURSOR => \PDO::CURSOR_FWDONLY
                ));
                $pdoStatement->execute($sqlArr);
                if ($pdoStatement->errorCode() * 1 != 0) {
                    $this->addError('', gettext('SQL Error setting ') . $f . print_r($pdoStatement->errorInfo(), true), 1);
                }
            }
            
            if (count($this->_err) == 0) {
                // count down all other sequences
                $sqlArr = array(
                    ':multiname' => $multiname,
                    ':deletedseq' => $deletedseq,
                    ':pmultiname' => $pmultiname,
                    ':pmultiid' => $pmultiid
                );
                $sql = "UPDATE `fevset_" . $eventid . "` 
				SET skey_multiseq = skey_multiseq - 1
				WHERE skey_multiname = :multiname
				AND pskey_multiname = :pmultiname
				AND pskey_multiid = :pmultiid
				AND skey_multiseq > :deletedseq;";
                $pdoStatement = $this->_pdoObj->prepare($sql, array(
                    \PDO::ATTR_CURSOR => \PDO::CURSOR_FWDONLY
                ));
                $pdoStatement->execute($sqlArr);
                if ($pdoStatement->errorCode() * 1 != 0) {
                    $this->addError('', gettext('SQL Error setting ') . $f . print_r($pdoStatement->errorInfo(), true), 1);
                }
            }
            // end of transaction
            if (count($this->_err) > 0) {
                $this->_pdoObj->rollBack();
            } else {
                $this->_pdoObj->commit();
            }
        } catch (Exception $e) {
            $this->_pdoObj->rollBack();
            $this->addError('', gettext('Error while deleting a submultisettings record.'), 1, print_r($e, true));
        }
    }

    public function getErrorsInApiFormat()
    {
        $errs = array();
        foreach ($this->_err as $e) {
            $errs[] = array(
                'message' => $e['note'],
                'field' => $e['field']
            );
        }
        return $errs;
    }

    private function checkTicketcategory(&$data)
    {
        if ($data['availablefrom'] == '') {
            $data['availablefrom'] = null;
        }
        if ($data['availableuntil'] == '') {
            $data['availableuntil'] = null;
        }
        
        // avoid spelling mistakes:
        $data['ticketcategorycode'] = trim($data['ticketcategorycode']);
        $data['ticketcategorycode'] = str_replace(',', '', $data['ticketcategorycode']);
        
        // only certain combinations of price and vat are allowed:
        // replace commas, write points instead for prices
        if ($data['price'] != '') {
            $data['price'] = str_replace(',', '.', $data['price']);
            if (! is_numeric($data['price'])) {
                $this->addError('price', gettext('Please write only numbers for the price.'));
            }
        }
        if ($data['pricenet'] != '') {
            $data['pricenet'] = str_replace(',', '.', $data['pricenet']);
        }
        if ($data['pricenet'] == '' && $data['price'] == '') {
            if ($data['vatrate'] != '') {
                $this->addError('vatrate', gettext('It is not possible to add a VAT rate without filling the price or net price field.'));
            }
            if ($data['vatamount'] != '') {
                $this->addError('vatamount', gettext('It is not possible to add a VAT amount without a filling the price or net price field.'));
            }
        }
        
        if ($data['vatamount'] != '') {
            $data['vatamount'] = str_replace(',', '.', $data['vatamount']);
            if ($data['vatrate'] == '') {
                $this->addError('vatamount', gettext('It is not possible to add a VAT amount without defining the VAT rate.'));
            }
        }
        if ($data['vatrate'] != '') {
            $data['vatrate'] = str_replace(',', '.', $data['vatrate']);
        }
        
        // gross only: ok
        // vat amount must be combined with vat rate!
        // net price must be combined with at least vat rate
    }

    public function moveMultisettingEntry($fevent_id, $skey_multiname, $currentseq, $direction)
    {
        try {
            if (is_numeric($currentseq) && $currentseq > 0 && ($direction == 'down' || ($direction == 'up' && $currentseq > 1))) {
                $this->_pdoObj = \dbconnection::getInstance();
                $this->_pdoObj->beginTransaction();
                if ($direction == 'up') {
                    $newSeq = $currentseq - 1;
                } else {
                    $newSeq = $currentseq + 1;
                }
                
                // first, move & temporary store current record (prepend "x99x_")
                $sql = "UPDATE `fevset_" . $fevent_id . "` 
				SET 
					skey = CONCAT('x99x_',skey_multiname,'_',:newseq,'_',skey_multikey),
					skey_multiseq = :newseq
				WHERE skey_multiname = :skey_multiname
					AND skey_multiseq = :skey_multiseq
				;";
                $sqlArr = array(
                    ':skey_multiname' => $skey_multiname,
                    ':skey_multiseq' => $currentseq,
                    ':newseq' => $newSeq
                );
                $pdoStatement = $this->_pdoObj->prepare($sql, array(
                    \PDO::ATTR_CURSOR => \PDO::CURSOR_FWDONLY
                ));
                $pdoStatement->execute($sqlArr);
                if ($pdoStatement->errorCode() * 1 != 0) {
                    $this->addError('', gettext('SQL Error') . ' 44 ' . print_r($pdoStatement->errorInfo(), true), 1);
                }
                
                // move other record
                $sql = "UPDATE `fevset_" . $fevent_id . "`
				SET 
					skey = CONCAT(skey_multiname,'_',:skey_multiseqnew,'_',skey_multikey),
					skey_multiseq = :skey_multiseqnew
				WHERE skey_multiname = :skey_multiname
					AND skey_multiseq = :skey_multiseqold
					AND SUBSTRING(skey, 1, 5) <> 'x99x_'
				;";
                $sqlArr = array(
                    ':skey_multiname' => $skey_multiname,
                    ':skey_multiseqnew' => $currentseq,
                    ':skey_multiseqold' => $newSeq
                );
                $pdoStatement = $this->_pdoObj->prepare($sql, array(
                    \PDO::ATTR_CURSOR => \PDO::CURSOR_FWDONLY
                ));
                $pdoStatement->execute($sqlArr);
                if ($pdoStatement->errorCode() * 1 != 0) {
                    $this->addError('', gettext('SQL Error') . ' 45 ' . print_r($pdoStatement->errorInfo(), true), 1);
                } elseif ($pdoStatement->rowCount() < 1) {
                    $this->addError('', gettext('Record has the highest position already.'));
                }
                
                // remove 9999
                $sql = "UPDATE `fevset_" . $fevent_id . "`
				SET
					skey = CONCAT(skey_multiname,'_',:newseq,'_',skey_multikey)
				WHERE skey_multiname = :skey_multiname
					AND skey_multiseq = :newseq
					AND SUBSTRING(skey, 1, 5) = 'x99x_'
				;";
                $sqlArr = array(
                    ':skey_multiname' => $skey_multiname,
                    ':newseq' => $newSeq
                );
                $pdoStatement = $this->_pdoObj->prepare($sql, array(
                    \PDO::ATTR_CURSOR => \PDO::CURSOR_FWDONLY
                ));
                $pdoStatement->execute($sqlArr);
                if ($pdoStatement->errorCode() * 1 != 0) {
                    $this->addError('', gettext('SQL Error') . ' 46 ' . print_r($pdoStatement->errorInfo(), true), 1);
                }
                if (count($this->_err) == 0) {
                    $this->_pdoObj->commit();
                } else {
                    $this->_pdoObj->rollBack();
                }
            }
        } catch (Exception $e) {
            $this->_pdoObj->rollBack();
            $this->addError('', gettext('SQL Error') . ' 47 ' . print_r($pdoStatement->errorInfo(), true), 1);
        }
    }

    /**
     * checks the conference session data, before it is saved
     *
     * @param array $data
     *            // conference session data to be saved
     * @param int $fevent_id
     *            // unsafe
     */
    private function checkConferencesession(&$data, $fevent_id)
    {
        if ($data['datetime'] == '') {
            $data['datetime'] = null;
        }
        if ($data['datetimeto'] == '') {
            $data['datetimeto'] = null;
        }
        $data['id'] = trim($data['id']);
        $data['stream'] = trim($data['stream']);
        $data['no'] = trim($data['no']);
        $data['linked'] = trim($data['linked']);
        
        if (strlen($data['id']) < 1) {
            $this->addError('id', gettext('Please add an id. This can be a unique number or letter, for example.'));
        } else {
            // check if this conference session id exists already
            $sql = "SELECT * FROM fevset_" . $fevent_id . " 
				WHERE skey_multiname = 'conferencesession' 
				AND skey_multiid = :newid
				AND skey_multiname = :smultiname
				AND skey_multiseq <> :seq;";
            $pdoStatement = $this->_pdoObj->prepare($sql, array(
                \PDO::ATTR_CURSOR => \PDO::CURSOR_FWDONLY
            ));
            
            $sqlArr = array(
                ':newid' => $data['id'],
                ':smultiname' => $data['multisetting'],
                ':seq' => $data['seqid']
            );
            $pdoStatement->execute($sqlArr);
            if ($pdoStatement->errorCode() * 1 != 0) {
                print_r($pdoStatement->errorInfo());
            } else {
                if ($pdoStatement->rowCount() > 0) {
                    $this->addError('id', gettext('This ID exists already. Please choose a different one.'));
                }
            }
        }
        
        // let's auto-update the bookedpax number...
        // the number of guests who have actually booked this session
        $count = 0;
        $ev = new event($this->_lg, $this->_locale);
        $fevent_data = $ev->readRecord($fevent_id, true);
        
        // creating a list of all session choice fields
        $sessionFields = array();
        foreach ($fevent_data['formpagesection'] as $fps) {
            if ($fps['type'] == 'sessionchoice') {
                $sessionFields[] = $fps['name'];
            }
        }
        
        $sql = "SELECT * FROM zguest" . $fevent_id . " WHERE deleted = 0 AND archived = 0 ORDER BY guest_id ASC;";
        
        $pdoStatement = $this->_pdoObj->prepare($sql, array(
            \PDO::ATTR_CURSOR => \PDO::CURSOR_FWDONLY
        ));
        
        $sqlArr = array(
            ':newid' => $data['id'],
            ':smultiname' => $data['multisetting'],
            ':seq' => $data['seqid']
        );
        $pdoStatement->execute($sqlArr);
        if ($pdoStatement->errorCode() * 1 != 0) {
            print_r($pdoStatement->errorInfo());
        } else {
            while ($row = $pdoStatement->fetch()) {
                foreach ($sessionFields as $sessionField) {
                    $sessions = explode(',', $row[$sessionField]);
                    if (array_search($data['id'], $sessions) !== false) {
                        $count += 1;
                    }
                }
            }
        }
        $data['bookedpax'] = $count;
        
        if (strlen($data['stream']) < 1) {
            $this->addError('stream', gettext('Please select one or more conference streams.'));
        }
    }

    private function checkConferencestream(&$data)
    {
        $data['id'] = trim($data['id']);
        if (strlen($data['id']) < 1) {
            $this->addError('id', gettext('Please add an id. This can be a unique number or letter, for example.'));
        }
    }

    private function checkFormpage(&$data)
    {
        $data['id'] = trim($data['id']);
        $data['pagetitle'] = trim($data['pagetitle']);
        if (strlen($data['pagetitle']) < 1) {
            $this->addError('pagetitle', gettext('A page title is required.'));
        }
    }

    private function checkFormpagesection(&$data, $fevent_id)
    {
        
        // avoid typing mistakes in list of ticket categories
        $ticketcategorylist = explode(',', $data['ticketcategorylist']);
        $newlist = '';
        $ev = new event($this->_lg, $this->_locale);
        $fevent_data = $ev->readRecord($fevent_id, true);
        $foundAlready = array();
        // print_r($fevent_data);
        foreach ($ticketcategorylist as $ticketcategory) {
            $found = false;
            $ticketcategory = trim($ticketcategory);
            if (strlen($ticketcategory) > 0) {
                foreach ($fevent_data['ticketcategory'] as $availableCategory) {
                    if ($availableCategory['ticketcategorycode'] == $ticketcategory) {
                        $found = true;
                    }
                }
                if (! isset($foundAlready[$ticketcategory])) {
                    $foundAlready[$ticketcategory] = 1;
                    $newlist .= $ticketcategory . ',';
                }
                if ($found == false) {
                    $this->addError('ticketcategorylist', gettext('One of the ticket categories does not exist and should be removed.') . ' (' . trim($ticketcategory) . ')');
                }
            }
        }
        if (substr($newlist, - 1) == ',') {
            $newlist = substr($newlist, 0, - 1);
        }
        $data['ticketcategorylist'] = $newlist;
    }

    function generateUniqueKey($length)
    {
        $k = '';
        $chars = 'ABCDEFFHIJKLMNOPQRSTUVWXYZ1234567890';
        for ($i = 0; $i < $length; $i ++) {
            $k .= substr($chars, rand(0, strlen($chars)), 1);
        }
        return $k;
    }

    function checkIfKeyIsUnique($mstype, $setting_id)
    {
        $ok = true;
        try {
            // ToDo: implement!
        } catch (Exception $e) {
            $this->addError('', gettext('Could not check if newly generated key is unique.'), 1, print_r($e, true));
        }
        return $ok;
    }

    static function getAllSettings($fevent_id, $k_multisettings_name, $k_type, $v_multisettings_name, $v_type)
    {
        $options = null;
        try {
            // get type of the relevant field!
            $mst = multisettings::getMultiSettingTypes();
            foreach ($mst[$k_multisettings_name]['fields'] as $f) {
                if ($f['name'] == $k_type) {
                    $fieldtype = $f['type'];
                    switch ($f['type']) {
                        case 'text':
                            $k_sqlErg = 'sval_txt';
                            break;
                        case 'int':
                            $k_sqlErg = 'sval_int';
                            break;
                        case 'char':
                            $k_sqlErg = 'sval_vch';
                            break;
                        case 'datetime':
                            $k_sqlErg = 'sval_dtt';
                            break;
                        case 'decimal':
                            $k_sqlErg = 'sval_dec';
                            break;
                    }
                }
                if ($f['name'] == $v_type) {
                    switch ($f['type']) {
                        case 'text':
                            $v_sqlErg = 'sval_txt';
                            break;
                        case 'int':
                            $v_sqlErg = 'sval_int';
                            break;
                        case 'char':
                            $v_sqlErg = 'sval_vch';
                            break;
                        case 'datetime':
                            $v_sqlErg = 'sval_dtt';
                            break;
                        case 'decimal':
                            $v_sqlErg = 'sval_dec';
                            break;
                    }
                }
            }
            
            $sql = "SELECT v.* FROM `fevset_" . $fevent_id . "` k
							JOIN `fevset_" . $fevent_id . "` v 
									ON (k.skey_multiid = v.skey_multiid AND k.skey_multiid = v.skey_multiid)
								WHERE
								k.skey_multiname = :k_skey_multiname
								AND k.skey_multikey = :k_skey_multikey
								AND v.skey_multikey = :v_skey_multikey
						;";
            $pdoObj = \dbconnection::getInstance();
            $pdoStatement = $pdoObj->prepare($sql, array(
                \PDO::ATTR_CURSOR => \PDO::CURSOR_FWDONLY
            ));
            $sqlArr = array(
                ':k_skey_multiname' => $k_multisettings_name,
                ':k_skey_multikey' => $k_type,
                ':v_skey_multikey' => $v_type
            );
            $pdoStatement->execute($sqlArr);
            if ($pdoStatement->errorCode() * 1 != 0) {
                print_r($pdoStatement->errorInfo());
                // $this->addError('', gettext('SQL Error reading ') . $f . print_r($pdoStatement->errorInfo(), true), 1);
            } else {
                while ($row = $pdoStatement->fetch(PDO::FETCH_ASSOC)) {
                    // print_r($row);
                    $options[$row['skey_multiid']] = $row[$v_sqlErg] . ' (' . $row['skey_multiid'] . ')';
                }
            }
            // print_r($options);
        } catch (Exception $e) {
            // $this->addError('', gettext("Error reading sub settings from multisettings."), 1, print_r($e, true));
        }
        return $options;
    }

    public function __construct()
    {}

    static function getMultisettingsFieldType($multisettingsName, $fieldname)
    {
        $ftype = false;
        $mstypes = multisettings::getMultiSettingTypes();
        foreach ($mstypes[$multisettingsName]['fields'] as $f) {
            if ($f['name'] == $fieldname) {
                $ftype = $f['type'];
            }
        }
        return $ftype;
    }

    public function get1nRecords($fevent_id, $pskey_multiname)
    {
        $recs = array();
        $sql = "SELECT skey, skey_multiname, skey_multiseq,
				skey_multiid, pskey_multiname, pskey_multiid,
				IFNULL(sval_int, IFNULL(sval_vch, IFNULL(sval_txt, IFNULL(sval_dtt, IFNULL(sval_dec, ''))))) sval
				FROM fevset_" . $fevent_id . "
				WHERE pskey_multiname = :pskey_multiname
			;";
        $this->_pdoObj = dbconnection::getInstance();
        $pdoStatement = $this->_pdoObj->prepare($sql, array(
            PDO::ATTR_CURSOR => PDO::CURSOR_FWDONLY
        ));
        $pdoStatement->execute(array(
            ':pskey_multiname' => $pskey_multiname
        ));
        if ($pdoStatement->errorCode() * 1 != 0) {
            $this->addError('', gettext('SQL Error') . ': ' . print_r($pdoStatement->errorInfo(), true), 1);
            $nextNo = null;
        } else {
            while ($row = $pdoStatement->fetch(PDO::FETCH_ASSOC)) {
                $recs[$row['pskey_multiname']][$row['pskey_multiid']][] = $row;
            }
        }
        return $recs;
    }
}