<?php

/**
 * Contains the field class
 */

/**
 * Field object.
 * Used for mass mails and simple emails for sending.
 * 
 * @author Line5
 *        
 */
class email extends l5sys
{

    /**
     * Data of this email
     * 
     * @var array
     */
    private $_emaildata = array();

    /**
     * Id of the currently loaded email
     * 
     * @var int
     */
    private $_emailid = 0;

    /**
     * Id of the currently selected event
     * 
     * @var int
     */
    private $_eventId = 0;

    /**
     * Data of this event
     * 
     * @var array
     */
    private $_eventdata = array();

    /**
     * This is the constructor
     *
     * @param string $lg
     *            contains the language
     * @param string $locale
     *            contains the locale
     */
    function __construct($lg, $locale)
    {
        if (TESTING === true) {
            $this->_rsvpDomain = 'rsvptest.dc.gs';
        }
        $this->_locale = $locale;
        $this->_lg = $lg;
    }

    /**
     * returns the email record for the currently active email id
     * 
     * @return array:
     */
    public function getData()
    {
        return $this->_emaildata;
    }

    /**
     * replaces variable names in [<varname>] constructs.
     *
     * @param string $html
     * @param string $foremail
     *            determines if the returned html code is for emails
     * @return string
     */
    public function createCustomizedText($html, $foremail = true)
    {
        if ($foremail == false) {
            if (LIVEENV === true) {
                $codeurl = 'https://check-in.fast-lane.org/qr/' . $this->_eventId . '/' . $this->_guestdata['lastname'] . '/' . $this->_guestdata['accesscode'];
                $html = str_replace('[#qr_ticket_open]', '<img src="http://qr.line5.net/png.php?content=' . urlencode($codeurl) . '&qr.png" />', $html);
            } else {
                $codeurl = 'https://check-in.fast-lane.org/qr/' . $this->_eventId . '/' . $this->_guestdata['lastname'] . '/' . $this->_guestdata['accesscode'];
                $html = str_replace('[#qr_ticket_open]', '<img src="https://qr.line5.net/png.php?content=' . urlencode($codeurl) . '&qr.png" />', $html);
            }
        }
        $html = preg_replace_callback('/(\[{1})(.*?)(\]{1})/s', array(
            $this,
            'replaceCallback'
        ), $html);
        return $html;
    }

    /**
     * replaces matches with $data[$match]
     *
     * @param string $match
     * @param array $data
     * @return string
     */
    protected function replaceCallback($match)
    {
        $data = '';
        if (substr($match[2], 0, 1) == '#') {
            switch ($match[2]) {
                case '#qr_ticket_open':
                    // $this->_emailAttachedImages[] = 'qr_ticket_open';
                    $data = '<img src="cid:qr_ticket_open" alt="QR Code Ticket" />';
                    break;
            }
        } elseif (substr($match[2], 0, 3) == 'ev.') {
            $data = $this->_eventdata[substr($match[2], 3)];
        } else {
            $data = $this->_guestdata[$match[2]];
        }
        return ($data);
    }

    /**
     * reads all data for the currently active record from the database
     * and stores it in $this->_emaildata
     *
     * @return boolean determines if the action was successful
     */
    private function readEmailData()
    {
        $ok = false;
        $this->_pdoObj = dbconnection::getInstance();
        
        // read email data
        $tablename = 'wemail' . $this->_eventId;
        $sql = "SELECT * ";
        $sql .= " FROM " . $tablename . " WHERE 1 = 1
				AND deleted = 0
				;";
        $pdoStatement = $this->_pdoObj->prepare($sql, array(
            PDO::ATTR_CURSOR => PDO::CURSOR_FWDONLY
        ));
        $pdoStatement->execute(array());
        if ($pdoStatement->errorCode() != 0) {
            $this->addError('', gettext('SQL Error'), 1, __FILE__ . '/' . __LINE__ . ' ' . print_r($pdoStatement->errorInfo(), true));
        } elseif ($pdoStatement->rowCount() == 0) {
            $this->addError('', gettext('SQL Error'), 1, __FILE__ . '/' . __LINE__ . ' Errorcode 0: ' . print_r($pdoStatement->errorInfo(), true));
        } else {
            $this->_emaildata = $pdoStatement->fetch();
        }
        return $ok;
    }

    /**
     * saves an email to the database
     *
     * @param integer $id
     *            contains the email id
     * @param string $subject
     *            contains the email subject
     * @param text $mailtext
     *            contains the mailtext (assumed to be html)
     * @param integer $sendstatus
     *            contains the send status (0 = draft)
     * @param array<integer> $massmailrecipients
     *            contains the selection of massmail recipient categories (0-5)
     * @param array $err
     *            contains error codes
     */
    private function saveEmail($id, $subject, $mailtext, $sendstatus, $massmailrecipients, &$err)
    {
        $newOrUpdate = 'new';
        if (is_numeric($id) && $id > 0) {
            $newOrUpdate = 'update';
        }
        $sqlArr = array(
            'subject' => $subject,
            'mailtext' => $mailtext,
            'sendstatus' => $sendstatus
        );
        if ($newOrUpdate == 'new') {
            $sql = "INSERT INTO wemail" . $this->_eventId;
        } else {
            $sql = "UPDATE wemail" . $this->_eventId;
        }
        $sql .= " SET
				subject = :subject,
				mailtext = :mailtext,
				sendstatus = :sendstatus
				";
        if ($newOrUpdate == 'update') {
            $sql .= " WHERE email_id = :emailid";
            $sqlArr['emailid'] = $id;
        }
        $this->_pdoObj = dbconnection::getInstance();
        $pdoStatement = $this->_pdoObj->prepare($sql, array(
            PDO::ATTR_CURSOR => PDO::CURSOR_FWDONLY
        ));
        $pdoStatement->execute($sqlArr);
        if ($pdoStatement->errorCode() != 0) {
            $this->addError('', gettext('SQL Error'), 1, __FILE__ . '/' . __LINE__ . ' ' . print_r($pdoStatement->errorInfo(), true));
        } elseif ($pdoStatement->rowCount() == 0) {
            $this->addError('', gettext('SQL Error'), 1, __FILE__ . '/' . __LINE__ . ' Errorcode 0: ' . print_r($pdoStatement->errorInfo(), true));
        }
    }

    /**
     * Saves a record via the API
     */
    public function apiPutRecord()
    {
        $ev = new event($this->_lg, $this->_locale);
        if ($ev->checkUserEvent($_POST['fevent_id']) == true) {
            $this->_eventId = $_POST['fevent_id'];
            $this->saveEmail($_POST['email_id'], $_POST['massmailsubject'], $_POST['massmailcontent'], 0, array(), $err);
        } else {
            $this->addError('fevent_id', gettext('Invalid event id given.'));
            // ToDo: This error is not propagated via json answer yet!
        }
    }

    /**
     * returns data of an event for the api
     * record id must have been set before via setRecordId
     * 
     * @return array contains an array of events
     */
    public function apiGetRecord($params = null, $subentity = null, $subentityid = null)
    {
        if ($subentity == null) {
            $res = array(
                'email' => $this->readRecord($this->_recordId, true)
            );
        }
        return $res;
    }

    public function setRecordId($id)
    {
        $this->_recordId = $id;
    }

    /**
     * returns an array with the data of the record with the given id
     *
     * @param int $id
     *            contains the email id
     * @param boolean $assoc
     *            determines if only assoc array should be returned
     * @return null|array contains data of the email record
     */
    private function readRecord($id, $assoc = false)
    {
        try {
            $row = null;
            $sql = "SELECT * FROM wemail" . $this->_eventId . " 
					WHERE email_id = :id;";
            $this->_pdoObj = dbconnection::getInstance();
            $pdoStatement = $this->_pdoObj->prepare($sql, array(
                PDO::ATTR_CURSOR => PDO::CURSOR_FWDONLY
            ));
            $pdoStatement->execute(array(
                ':id' => $id
            ));
            if ($pdoStatement->errorCode() * 1 != 0) {
                $this->addError('', gettext('SQL Error') . ': ' . print_r($pdoStatement->errorInfo(), true), 1);
            } else {
                if ($assoc === true) {
                    $row = $pdoStatement->fetch(PDO::FETCH_ASSOC);
                } else {
                    $row = $pdoStatement->fetch();
                }
            }
        } catch (Exception $e) {
            $this->addError('', gettext('SQL Error'), 1, __FILE__ . '/' . __LINE__ . ': ' . print_r($e, true));
        }
        return $row;
    }

    public function setEventId($id)
    {
        $res = false;
        if ($this->_eventId == null) {
            $this->_eventId = $id;
            $res = true;
        }
        return $res;
    }
}