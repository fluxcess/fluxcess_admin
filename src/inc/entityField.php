<?php

/**
 * Contains the entityField class
 */

/**
 * Fild object.
 * Fields are columns in tables.
 *
 * @author Line5
 *        
 */
class entityEmail extends l5sys
{

    /**
     * Data of this field
     *
     * @var array
     */
    private $_fielddata = array();

    /**
     * Id of the currently loaded field
     *
     * @var int
     */
    private $_fieldid = 0;

    /**
     * Id of the currently selected event
     *
     * @var int
     */
    private $_eventid = 0;

    /**
     * Data of this event
     *
     * @var array
     */
    private $_eventdata = array();

    /**
     * instanciates an email object
     *
     * @param integer $eventid
     * @param integer $emailid
     * @param array $eventdata
     */
    function __construct($eventid, $emailid, $eventdata = null)
    {
        $this->_eventid = $eventid;
        $this->_emailid = $emailid;
        if ($this->readEmailData() !== true) {
            $this->_emailid = 0;
        }
        $this->_eventdata = $eventdata;
    }

    /**
     * returns the email record for the currently active email id
     *
     * @return array:
     */
    public function getData()
    {
        return $this->_emaildata;
    }

    /**
     * replaces variable names in [<varname>] constructs.
     *
     * @param string $html
     * @param string $foremail
     *            determines if the returned html code is for emails
     * @return string
     */
    public function createCustomizedText($html, $foremail = true)
    {
        if ($foremail == false) {
            if (LIVEENV === true) {
                $codeurl = 'https://check-in.fast-lane.org/qr/' . $this->_eventid . '/' . $this->_guestdata['lastname'] . '/' . $this->_guestdata['accesscode'];
                $html = str_replace('[#qr_ticket_open]', '<img src="http://qr.line5.net/png.php?content=' . urlencode($codeurl) . '&qr.png" />', $html);
            } else {
                $codeurl = 'https://check-in.fast-lane.org/qr/' . $this->_eventid . '/' . $this->_guestdata['lastname'] . '/' . $this->_guestdata['accesscode'];
                $html = str_replace('[#qr_ticket_open]', '<img src="https://qr.line5.net/png.php?content=' . urlencode($codeurl) . '&qr.png" />', $html);
            }
        }
        $html = preg_replace_callback('/(\[{1})(.*?)(\]{1})/s', array(
            $this,
            'replaceCallback'
        ), $html);
        return $html;
    }

    /**
     * replaces matches with $data[$match]
     *
     * @param string $match
     * @param array $data
     * @return string
     */
    protected function replaceCallback($match)
    {
        $data = '';
        if (substr($match[2], 0, 1) == '#') {
            switch ($match[2]) {
                case '#qr_ticket_open':
                    // $this->_emailAttachedImages[] = 'qr_ticket_open';
                    $data = '<img src="cid:qr_ticket_open" alt="QR Code Ticket" />';
                    break;
            }
        } elseif (substr($match[2], 0, 3) == 'ev.') {
            $data = $this->_eventdata[substr($match[2], 3)];
        } else {
            $data = $this->_guestdata[$match[2]];
        }
        return ($data);
    }

    /**
     * reads all data for the currently active record from the database
     * and stores it in $this->_emaildata
     *
     * @return boolean determines if the action was successful
     */
    private function readEmailData()
    {
        $ok = false;
        $this->_pdoObj = dbconnection::getInstance();
        
        // read email data
        $tablename = 'wemail' . $this->_eventid;
        $sql = "SELECT * ";
        $sql .= " FROM " . $tablename . " WHERE 1 = 1
				AND deleted = 0
				;";
        $pdoStatement = $this->_pdoObj->prepare($sql, array(
            PDO::ATTR_CURSOR => PDO::CURSOR_FWDONLY
        ));
        $pdoStatement->execute(array());
        if ($pdoStatement->errorCode() != 0) {
            $this->addError('', gettext('SQL Error'), 1, __FILE__ . '/' . __LINE__ . ' ' . print_r($pdoStatement->errorInfo(), true));
        } elseif ($pdoStatement->rowCount() == 0) {
            $this->addError('', gettext('SQL Error'), 1, __FILE__ . '/' . __LINE__ . ' Errorcode 0: ' . print_r($pdoStatement->errorInfo(), true));
        } else {
            $this->_emaildata = $pdoStatement->fetch();
        }
        return $ok;
    }

    /**
     * saves an email to the database
     *
     * @param integer $id
     *            contains the email id
     * @param string $subject
     *            contains the email subject
     * @param text $mailtext
     *            contains the mailtext (assumed to be html)
     * @param integer $sendstatus
     *            contains the send status (0 = draft)
     * @param array<integer> $massmailrecipients
     *            contains the selection of massmail recipient categories (0-5)
     * @param array $err
     *            contains error codes
     */
    private function saveEmail($id, $subject, $mailtext, $sendstatus, $massmailrecipients, &$err)
    {
        $newOrUpdate = 'new';
        if (is_numeric($id) && $id > 0) {
            $newOrUpdate = 'update';
        }
        $sqlArr = array(
            'subject' => $subject,
            'mailtext' => $mailtext,
            'sendstatus' => $sendstatus
        );
        if ($newOrUpdate == 'new') {
            $sql = "INSERT INTO wemail" . $this->_eventid;
        } else {
            $sql = "UPDATE wemail" . $this->_eventid;
        }
        $sql .= " SET 
  				subject = :subject,
  				mailtext = :mailtext,
  				sendstatus = :sendstatus
  				";
        if ($newOrUpdate == 'update') {
            $sql .= " WHERE email_id = :emailid";
            $sqlArr['emailid'] = $id;
        }
        
        $pdoStatement = $this->_pdoObj->prepare($sql, array(
            PDO::ATTR_CURSOR => PDO::CURSOR_FWDONLY
        ));
        $pdoStatement->execute($sqlArr);
        if ($pdoStatement->errorCode() != 0) {
            $this->addError('', gettext('SQL Error'), 1, __FILE__ . '/' . __LINE__ . ' ' . print_r($pdoStatement->errorInfo(), true));
        } elseif ($pdoStatement->rowCount() == 0) {
            $this->addError('', gettext('SQL Error'), 1, __FILE__ . '/' . __LINE__ . ' Errorcode 0: ' . print_r($pdoStatement->errorInfo(), true));
        }
    }

    public function apiPutRecord()
    {
        print_r($_GET);
        print_r($_POST);
    }
}