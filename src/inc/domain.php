<?php

class domain extends l5sys
{

    private $_dt = Array(
        0 => array(
            'standard domain with event-directory'
        )
    );

    /**
     * ToDo: This needs to be configurable!
     * 
     * List of domains for rsvp
     * @var array
     */
    private $_stdDomains = [];
    
    /**
     * list of paths, that can not be used, because they are reserved for some functionality
     * @var array
     */
    private $_blacklist = array(
        'qr',
        'logout'
    );

    function __construct($lg, $locale)
    {
        $this->_locale = $locale;
        $this->_lg = $lg;
        if (BOOKING_DOMAINS !== false) {
            $this->_stdDomains = BOOKING_DOMAINS;
        }
    }

    public function printPage(&$gt, &$ps, &$err, &$proceed)
    {
        $this->_gt = $gt;
        $this->_ps = $ps;
        $this->_err = $err;
        
        if (isset($_POST['form']) && $_POST['class'] == 'domain') {
            switch ($_POST['form']) {
                case 'deleteDomain':
                    $this->handleDeleteDomain();
                    $proceed = false;
                    break;
                case 'editDomainForm':
                    $xout .= $this->printChangeForm();
                    break;
                case 'changeDomainForm':
                    $xout .= $this->saveDomainData();
                    break;
            }
        } else {
            if (is_numeric($this->_gt[1]) || is_numeric($this->_gt['fevent_id'])) {
                if (is_numeric($this->_gt[1])) {
                    $eventid = $this->_gt[1];
                } else {
                    $eventid = $this->_gt['fevent_id'];
                }
                
                if ($this->_gt['submodule'] == 'printDomainListTable') {
                    $xout = $this->printDomainListTable($this->_gt['fevent_id']);
                }
            } else {
                if ($this->_gt['type'] == 'iframe') {
                    $sw = $this->_gt['submodule'];
                } else {
                    $sw = $this->_gt[1];
                }
            }
        }
        // pass back data to calling class
        $err = $this->_err;
        return $xout;
    }

    public function getDomains($eventid)
    {
        $data = array();
        // check if event id is numeric
        if (! is_numeric($eventid)) {
            $this->addError('', gettext('Event id contains errors.'));
        }
        // check if event belongs to user
        $ev = new event($this->_lg, $this->_locale);
        if (! $ev->checkUserEvent($eventid)) {
            $this->addError('', gettext('Invalid event id') . ': ' . $eventid);
        }
        try {
            $sql = "SELECT *
					FROM `domain`
					WHERE
					fevent_id = :eventid
					;";
            $this->_pdoObj = dbconnection::getInstance();
            $pdoStatement = $this->_pdoObj->prepare($sql, array(
                PDO::ATTR_CURSOR => PDO::CURSOR_FWDONLY
            ));
            $pdoStatement->execute(array(
                ':eventid' => $eventid
            ));
            if ($pdoStatement->errorCode() * 1 != 0) {
                $this->addError('', 'SQL Fehler', print_r($pdoStatement->errorInfo(), true), 1);
            } else {
                while ($row = $pdoStatement->fetch()) {
                    $data[] = $row;
                }
            }
        } catch (Exception $e) {
            $this->addError('', gettext('Error reading event domains.'), print_r($pdoStatement->errorInfo(), true), 1);
        }
        return $data;
    }

    private function saveDomainData()
    {
        $this->_pdoObj = dbconnection::getInstance();
        $this->_pdoObj->beginTransaction();
        try {
            // $fieldname = $this->_ps['name'];
            $id = $this->_ps['domain_id'];
            // $value = $this->_ps['value'];
            $eventid = $this->_ps['fevent_id'];
            
            $fields = array(
                'domaintype',
                'domainname',
                'urlpath'
            );
            
            if (! is_numeric($id)) {
                $this->addError('', 'Ungültige Datensatz-Id: ' . $id);
            }
            
            $ev = new event($this->_lg, $this->_locale);
            if (! is_numeric($eventid) || ! $ev->checkUserEvent($eventid)) {
                $this->addError('', 'Ungültige Event-Id: ' . $eventid);
            }
            
            if (array_search($this->_ps['domainname'], $this->_stdDomains) === false) {
                $this->addError('domainname', 'Ungültiger Domain-Name');
            }
            
            $this->_ps['urlpath'] = trim($this->_ps['urlpath']);
            if (strlen($this->_ps['urlpath']) < 3) {
                $this->addError('urlpath', 'Der URL-Pfad muss aus mindestens drei Zeichen bestehen.');
            } else {
                if (preg_match("/[^-a-z0-9\.]/i", $this->_ps['urlpath'])) {
                    $this->addError('urlpath', 'Der URL-Pfad enthält ungültige Zeichen.');
                }
            }
            
            // check if domain exists already
            if ($this->checkIfDomainExists($this->_ps['domainname'], $this->_ps['urlpath']) != 0) {
                $this->addError('urlpath', gettext('The domain exists already.'));
            }
            
            if (array_search($this->_ps['urlpath'], $this->_blacklist) !== false) {
                $this->addError('urlpath', gettext('Please choose a different URL path.'));
            }
            
            // check domain name
            $this->_ps['domainname'] = trim($this->_ps['domainname']);
            if (strlen($this->_ps['domainname']) < 3) {
                $this->addError('domainname', gettext('The domain name needs to consist of at least 3 characters.'));
            } else {
                if (array_search($this->_ps['domainname'], $this->_stdDomains) === false && preg_match("/[^-a-z0-9\.]/i", $this->_ps['domainname'])) {
                    $this->addError('domainname', gettext('The domain name contains invalid characters.'));
                }
            }
            
            // check domain type
            // TODO - this needs to be improved!
            $this->_ps['domaintype'] = trim($this->_ps['domaintype']);
            if (! is_numeric($this->_ps['domaintype']) || $this->_ps['domaintype'] != 0) {
                $this->addError('domaintype', gettext('Invalid domain type selected.'));
            }
            
            if (count($this->_err) == 0) {
                // set fields...
                $sqlF = '';
                
                // TODO: check if domain / path is valid
                // TODO: assist choosing domain type
                
                // new?
                if ($this->_ps['field_id'] == 0) {
                    $sql = "INSERT INTO `domain`
							SET fevent_id = :eventid, ";
                    $sqlArr['eventid'] = $eventid;
                    $isSysColumn = false;
                } else {
                    // TODO: multiple actions necessary if domain name or type is being changed!!!
                    // change?
                    $sql = "UPDATE domain SET ";
                }
                
                // Add contents
                foreach ($this->_ps as $fieldname => $fieldvalue) {
                    if (array_search($fieldname, $fields) !== false) {
                        $sqlF .= $fieldname . " = :" . $fieldname . ", ";
                        $sqlArr[$fieldname] = $fieldvalue;
                    }
                }
                
                if ($isSysColumn == false) {}
                
                $sql .= substr($sqlF, 0, - 2);
                if ($this->_ps['domain_id'] > 0) {
                    $sql .= " WHERE domain_id = :id;";
                    $sqlArr['id'] = $id;
                } else {
                    $sql .= ';';
                }
                
                if (count($this->_err) == 0) {
                    $pdoStatement = $this->_pdoObj->prepare($sql, array(
                        PDO::ATTR_CURSOR => PDO::CURSOR_FWDONLY
                    ));
                    $pdoStatement->execute($sqlArr);
                    if ($pdoStatement->errorCode() * 1 != 0) {
                        $this->addError('', 'SQL Fehler ' . __FILE__ . '/' . __LINE__ . ' - ' . print_r($pdoStatement->errorInfo(), true), 1);
                    } else {
                        // $fieldid = $this->_pdoObj->lastInsertId();
                    }
                }
                if (count($this->_err) == 0) {
                    $this->_pdoObj->commit();
                } else {
                    $this->_pdoObj->rollBack();
                }
            }
        } catch (Exception $e) {
            $this->_pdoObj->rollBack();
            $this->addError('', 'Datenbank-Fehler', 1, __FILE__ . '/' . __LINE__ . '/' . print_r($e->getMessage(), true));
        }
    }

    private function checkIfDomainExists($domainname, $urlpath)
    {
        $exists = - 1;
        $sql = "SELECT * FROM domain
					WHERE (domainname = :domainname
					AND urlpath = :urlpath)
					OR (domainname = :domainname
					AND urlpath = '')
				;";
        $pdoStatement = $this->_pdoObj->prepare($sql, array(
            PDO::ATTR_CURSOR => PDO::CURSOR_FWDONLY
        ));
        $pdoStatement->execute(array(
            ':domainname' => $domainname,
            ':urlpath' => $urlpath
        ));
        if ($pdoStatement->errorCode() * 1 != 0) {
            $this->addError('', 'SQL Fehler ' . __FILE__ . '/' . __LINE__ . ' - ' . print_r($pdoStatement->errorInfo(), true), 1);
        } else {
            if ($pdoStatement->rowCount() > 0) {
                $exists = 1;
            } else {
                $exists = 0;
            }
        }
        return $exists;
    }

    private function printChangeForm()
    {
        // check if domain id is numeric
        if (! is_numeric($this->_ps['domain_id'])) {
            $this->addError('', 'Fehlerhafte Domain-Id');
        }
        // check if event id is numeric
        if (! is_numeric($this->_ps['fevent_id'])) {
            $this->addError('', 'Fehlerhafte Event-Id');
        }
        // check if event belongs to user
        $ev = new event($this->_lg, $this->_locale);
        if (! $ev->checkUserEvent($this->_ps['fevent_id'])) {
            $this->addError('', 'Ungültige Event-Id: ' . $this->_ps['fevent_id']);
        }
        
        if ($this->_ps['domain_id'] != 0) {
            $domaindata = $this->getDomainData($this->_ps['fevent_id'], $this->_ps['domain_id']);
        } else {
            $domaindata = Array();
            $domaindata['domaintype'] = 1;
        }
        foreach ($this->_dt as $key => $val) {
            $dtval[] = $key;
            $dtout[] = $val[0];
        }
        foreach ($this->_stdDomains as $key => $val) {
            $stdop[] = $val;
        }
        $sm = new L5Smarty();
        $sm->assign('ps', $domaindata);
        $sm->assign('fevent_id', $this->_ps['fevent_id']);
        $sm->assign('domain_id', $this->_ps['domain_id']);
        $sm->assign('dtval', $dtval);
        $sm->assign('dtout', $dtout);
        $sm->assign('stdop', $stdop);
        $xout = $sm->fetch('event/changedomainform.tpl');
        
        return $xout;
    }

    private function getDomainData($eventid, $domainid)
    {
        // check if domain id is numeric
        if (! is_numeric($domainid)) {
            $this->addError('', 'Fehlerhafte Domain-Id');
        }
        // check if event id is numeric
        if (! is_numeric($eventid)) {
            $this->addError('', 'Fehlerhafte Event-Id');
        }
        // check if event belongs to user
        $ev = new event($this->_lg, $this->_locale);
        if (! $ev->checkUserEvent($eventid)) {
            $this->addError('', 'Ungültige Event-Id: ' . $eventid);
        }
        // read guest data
        $sql = "SELECT * FROM `domain` WHERE domain_id = :domainid;";
        $this->_pdoObj = dbconnection::getInstance();
        $pdoStatement = $this->_pdoObj->prepare($sql, array(
            PDO::ATTR_CURSOR => PDO::CURSOR_FWDONLY
        ));
        $pdoStatement->execute(array(
            ':domainid' => $domainid
        ));
        if ($pdoStatement->errorCode() * 1 != 0) {
            $this->addError('', 'SQL Fehler' . print_r($pdoStatement->errorInfo(), true), 1);
        } elseif ($pdoStatement->rowCount() == 0) {
            $this->addError('', __LINE__ . 'SQL Fehler mit Errorcode 0: ' . print_r($pdoStatement->errorInfo(), true), 1);
        } else {
            $domainData = $pdoStatement->fetch();
        }
        
        return $guestData;
    }

    private function handleDeleteDomain()
    {
        // check if field id is numeric
        if (! is_numeric($this->_ps['domain_id'])) {
            $this->addError('', 'Fehlerhafte Domain-Id');
        }
        // check if event id is numeric
        if (! is_numeric($this->_ps['fevent_id'])) {
            $this->addError('', 'Fehlerhafte Event-Id');
        }
        // check if event belongs to user
        $ev = new event($this->_lg, $this->_locale);
        if (! $ev->checkUserEvent($this->_ps['fevent_id'])) {
            $this->addError('', 'Ungültige Event-Id: ' . $this->_ps['fevent_id']);
        }
        
        $this->_pdoObj = dbconnection::getInstance();
        $this->_pdoObj->beginTransaction();
        try {
            
            if (count($this->_err) == 0) {
                $sql = "DELETE FROM domain
						WHERE domain_id = :domainid
						AND fevent_id = :eventid
						;";
                $pdoStatement = $this->_pdoObj->prepare($sql, array(
                    PDO::ATTR_CURSOR => PDO::CURSOR_FWDONLY
                ));
                $pdoStatement->execute(array(
                    ':domainid' => $this->_ps['domain_id'],
                    ':eventid' => $this->_ps['fevent_id']
                ));
                
                if ($pdoStatement->errorCode() * 1 != 0) {
                    $this->addError('', 'SQL Fehler ' . __LINE__ . print_r($pdoStatement->errorInfo(), true), 1);
                } elseif ($pdoStatement->rowCount() == 0) {
                    $this->addError('', 'Domain nicht gefunden.');
                }
            }
            if (count($this->_err) == 0) {
                $this->_pdoObj->commit();
            } else {
                $this->_pdoObj->rollBack();
            }
        } catch (Exception $e) {
            $this->addError('', 'Error deleting domain ' . $this->_ps['domain_id'], 1, print_r($e, true));
            $this->_pdoObj->rollBack();
        }
    }

    /**
     * returns the html code of a table with domains for the specified event
     * 
     * @param int $eventid
     * @return string
     */
    private function printDomainListTable($eventid)
    {
        $ev = new event($this->_lg, $this->_locale);
        if (! $ev->checkUserEvent($eventid)) {
            $this->addError('', 'Wrong event id.');
        } else {
            
            $sm = new L5Smarty();
            $sm->assign('err', $this->_err);
            $sm->assign('fevent_id', $eventid);
            $sm->assign('domains', $this->getDomains($eventid));
            $xout = $sm->fetch('event/domainlisttable.tpl');
        }
        return $xout;
    }
}