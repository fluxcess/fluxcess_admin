<?php

/**
 * Ping-Class for the API
 * 
 * The "ping" just returns a "pong" to requests. It can be used to test the fluxcess API,
 * including authentication.
 * 
 * @author fluxcess GmbH
 *
 */
class ping extends l5sys
{

    /**
     *
     * @param string $lg
     *            the language
     * @param string $locale
     *            the locale
     */
    function __construct($lg, $locale)
    {
        $this->_locale = $locale;
        $this->_lg = $lg;
    }

    /**
     * returns the "pong" for GET requests
     *
     * @return array with pong
     */
    public function apiGetList($entity = null, $params = null)
    {
        $res = array();
        $res['content'] = 'pong';
        return $res;
    }

    /**
     * returns the "pong" for POST requests
     *
     * @return array with pong
     */
    public function apiPutRecord($put)
    {
        $res = array();
        $res['content'] = 'pong';
        return $res;
    }
}