<?php

/**
 * Contains the API class
 */

/**
 * API class
 *
 * @author Line5
 *        
 */
class api
{

    /**
     * contains the answer to the client as an array
     *
     * @var array
     */
    private $_result;

    /**
     * contains the status code which is to be returned
     *
     * @var int
     */
    private $_statusCode = null;

    /**
     * determines if the api call has been successful
     *
     * @var boolean
     */
    private $_success = false;

    /**
     * contains array of get parameters
     *
     * @var array
     */
    private $_gt;

    /**
     * contains the api version, if given
     *
     * @var string|null
     */
    private $_version = null;

    /**
     * contains the language
     *
     * @var string
     */
    private $_lg;

    /**
     * contains the locale
     *
     * @var string
     */
    private $_locale;

    private $_err;

    const LOCALE_EN_EN = 'en_US.UTF-8';

    const LOCALE_DE_DE = 'de_DE.UTF-8';

    /**
     * constructor
     *
     * @param string|null $version
     *            contains the api version number
     * @param array $gt
     *            contains the get parameters, without "api" or version number
     * @param string $lg
     *            contains the language code
     * @param string $locale
     *            contains the locale code
     */
    function __construct($version = null, $gt, $lg, $locale)
    {
        // $this->_gt = $gt;
        foreach ($gt as $xgtkey => $xgtval) {
            $xgtvalxpl = explode('&', $xgtval);
            $this->_gt[$xgtkey] = $xgtvalxpl[0];
        }
        $this->_version = $version;
        $this->_lg = $lg;
        $this->_locale = $locale;
    }

    /**
     * Main function - entry point.
     */
    public function start()
    {
        $sendAnswer = true; // must not be sent, if a _file_ is returned for download
        $payload = null;
        $permittedClasses = array(
            'crm',
            'download',
            'email',
            'emailtemplate',
            'event',
            'eventtransfer',
            'extension',
            'field',
            'internalfile',
            'ping',
            'remoteevent'
        );
        // first, figure out what's requested
        for ($i = 0; $i < count($this->_gt); $i += 2) {
            if (trim($this->_gt[$i]) != '') {
                if (isset($this->_gt[$i + 1])) {
                    $r[] = array(
                        $this->_gt[$i],
                        $this->_gt[$i + 1]
                    );
                } else {
                    $r[] = array(
                        $this->_gt[$i],
                        null
                    );
                }
                $this->_result[] = $r;
            }
        }
        $requriParts = explode('?', $_SERVER['REQUEST_URI']);
        $paramsStr = $requriParts[1];
        $paramsArr = explode('&', $paramsStr);
        if (is_array($paramsArr)) {
            foreach ($paramsArr as $pa) {
                $paz = explode('=', $pa);
                if (strlen($paz[0]) > 0) {
                    $params[$paz[0]] = urldecode($paz[1]);
                }
            }
        }
        if (isset($params['lg'])) {
            if ($params['lg'] == 'de') {
                $this->_locale = self::LOCALE_DE_DE;
                $this->_lg = 'de';
            } else {
                $this->_locale = self::LOCALE_EN_EN;
                $this->_lg = 'en';
            }
        }
        
        try {
            // then, invoke classes and forward requests to them
            $myClassName = $r[0][0];
            $myClassEntityId = $r[0][1];
            // r0 = first level
            if ($myClassName != 'ping') {
                $theClassName = trim(substr($myClassName, 0, - 1));
            } else {
                $theClassName = $myClassName;
            }
            if (strlen($theClassName) == 0) {
                $this->addApiError(13, 'No entity given.');
            } elseif (array_search($theClassName, $permittedClasses) !== false) {
                if ($theClassName == 'download') {
                    $this->_result = '';
                    $dl = new download();
                    $dl->sendFileToBrowser($this->_gt);
                    $sendAnswer = false;
                } else {
                    $a = new $theClassName($this->_lg, $this->_locale);
                    $method = trim(strtoupper($_SERVER['REQUEST_METHOD']));
                    if ($method == 'PUT') {
                        $put = array();
                        print_r(parse_str(file_get_contents('php://input'), $put));
                    }
                    // myClassEntityId is (for example) the 22 in /events/22
                    if (is_numeric($myClassEntityId)) {
                        if ($a->setRecordId($myClassEntityId) === true) {
                            if (isset($r[1])) {
                                // r1 = second level, for example: /events/22/guests
                                if ($method == 'PUT') {
                                    $this->_result = $a->apiPutRecord($r[1][0], $r[1][1], $put);
                                    $this->addApiErrors($a->getErrors());
                                } elseif ($method == 'DELETE') {
                                    $this->_result = $a->apiDeleteRecord($r[1][0], $r[1][1]);
                                    $this->addApiErrors($a->getErrors());
                                } elseif ($method == 'POST' && ! isset($r[2][0])) {
                                    $this->_result = $a->apiPutRecord($r[1][0], $r[1][1], $_POST);
                                    $this->addApiErrors($a->getErrors());
                                } else {
                                    // r2 = third level, for example /events/22/guests/15/xxx(/yyy)
                                    if (! isset($r[2][1]) && isset($r[2][0]) && $method == 'POST') {
                                        // this is a /events/22/guest/15/sendhimanemail case
                                        $this->_result = $a->apiSpecialFunction($r[1][0], $r[1][1], $r[2][0]);
                                        $this->addApiErrors($a->getErrors());
                                    } elseif (isset($r[2][0]) && ! isset($r[2][1]) && $method == 'GET') {
                                        $this->_result = $a->apiGetGrandchildRecords($r[1][0], $r[1][1], $r[2][0]);
                                        $this->addApiErrors($a->getErrors());
                                    } else {
                                        if (is_numeric($r[1][1])) {
                                            $this->_result = $a->apiGetRecord(null, $r[1][0], $r[1][1]);
                                            $this->addApiErrors($a->getErrors());
                                        } else {
                                            $this->_result = $a->apiGetList($r[1][0], $params);
                                            $this->addApiErrors($a->getErrors());
                                        }
                                    }
                                }
                            } else {
                                if ($method == 'GET') {
                                    $this->_result = $a->apiGetRecord($params);
                                    $this->addApiErrors($a->getErrors());
                                } elseif ($method == 'PUT') {
                                    $this->_result = $a->apiPutRecord($put);
                                    $this->addApiErrors($a->getErrors());
                                } elseif ($method == 'POST') {
                                    $this->_result = $a->apiSpecialPostFunction($_POST);
                                    $this->addApiErrors($a->getErrors());
                                } else {
                                    $this->addApiError(10, 'unsupported method ' . $method);
                                }
                            }
                        } else {
                            $this->addApiErrors($a->getErrors());
                        }
                    } elseif ($method == 'POST' && $myClassEntityId == '') {
                        $this->_result = $a->apiPutRecord($_POST);
                        $this->addApiErrors($a->getErrors());
                    } elseif (($method == 'POST' || $method == 'PUT') && strlen($myClassEntityId) > 0) {
                        $this->_result = $a->apiInvokeMethod($myClassEntityId);
                        $this->addApiErrors($a->getErrors());
                    } else {
                        $this->_result = $a->apiGetList(null, $paramsArr);
                        $this->addApiErrors($a->getErrors());
                    }
                }
            } else {
                $this->addApiError(11, 'Entity ' . $theClassName . ' unknown.');
            }
        } catch (Exception $e) {
            $this->addApiError(9, 'Error ' . $e->getMessage());
        }
        // $this->_result[$payload[0]] = $payload;
        if ($sendAnswer === true && (! isset($this->_result['sendAnswer']) || (isset($this->_result['sendAnswer']) && $this->_result['sendAnswer'] == true))) {
            if (isset($this->_result['sendAnswer'])) {
                unset($this->_result['sendAnswer']);
            }
            if ($this->_statusCode === null) {
                if (count($this->_err) == 0) {
                    $this->_statusCode = 200;
                }
            }
            if ($this->_success === false) {
                if (count($this->_err) == 0) {
                    $this->_success = true;
                }
            }
            $meta['statusCode'] = $this->_statusCode;
            $meta['success'] = $this->_success;
            // array_unshift($this->_result, array('meta' => $meta));
            if (is_array($this->_result)) {
                $this->_result = array(
                    'meta' => $meta
                ) + $this->_result;
            } else {
                $this->_result = array(
                    'meta' => $meta
                );
            }
            
            if (! headers_sent()) {
                $answer = json_encode($this->_result, 128);
                header("Access-Control-Allow-Orgin: *");
                header("Access-Control-Allow-Methods: *");
                header("Content-Type: application/json; charset=UTF-8");
                echo $answer;
            }
        }
    }

    /**
     * adds an error to an api return message
     *
     * @param int $code
     *            contains the error code
     * @param string $message
     *            contains the error message
     * @param string $description
     *            contains the error description
     * @param string $field
     *            contains the field name, in case a form error occured
     */
    private function addApiError($code, $message, $description = null, $field = null)
    {
        $this->_statusCode = 500;
        $this->_result['code'] = $code;
        $this->_result['message'] = $message;
        if ($description != null) {
            $this->_result['description'] = $description;
        }
    }

    private function addApiErrors($errorArray)
    {
        if (count($errorArray) > 0) {
            $this->_statusCode = 500;
            foreach ($errorArray as $myError) {
                $e['code'] = $myError['code'];
                $e['field'] = $myError['field'];
                $e['message'] = '[' . $myError['field'] . '] ' . $myError['note'];
                if ($description != null) {
                    $e['description'] .= ' :: ' . $description;
                }
                $this->_result['err'][] = $e;
                /*
                 * if ($this->_result['code'] == null) {
                 * $this->_result['code'] = $myError['code'];
                 * $this->_result['message'] = '[' . $myError['field'] . '] ' . $myError['note'];
                 * }
                 * if ($description != null) {
                 * $this->_result['description'] .= ' :: ' . $description;
                 * }
                 */
            }
        }
    }
}