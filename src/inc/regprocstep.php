<?php

class regprocstep extends l5sys
{

    private $_eventId;

    /**
     * This is the constructor
     *
     * @param string $lg
     *            the language
     * @param string $locale
     *            the locale
     */
    function __construct($lg, $locale)
    {
        $this->_locale = $locale;
        $this->_lg = $lg;
    }

    /**
     * Saves a record via the API
     */
    public function apiPutRecord($put)
    {
        $ev = new event($this->_lg, $this->_locale);
        if ($ev->checkUserEvent($put['fevent_id']) == true) {
            $this->_eventId = $put['fevent_id'];
            $this->addStepToEvent($put['fevent_id'], $put['extension_id'], $put['description']);
        } else {
            $this->addError('fevent_id', gettext('Invalid event id given.'));
        }
    }

    /**
     * returns the list of registration process steps for the specified event to the api
     *
     * @return array contains events
     */
    public function apiGetListForEvent($eventid)
    {
        $res = null;
        $r = $this->readStepListForEvent($eventid);
        $res['steps'] = $r;
        
        return $res;
    }

    public function apiDeleteRecord($eventid, $recordid)
    {
        $this->deleteStepFromEvent($eventid, $recordid);
    }

    private function deleteStepFromEvent($eventid, $recordid)
    {
        $res = false;
        try {
            // ToDo: Should be a transaction
            
            $tablename = "zregprocstep" . $eventid;
            $this->_pdoObj = dbconnection::getInstance();
            // find out current sequence
            $sql = "SELECT sequence, extension_id, regprocstep_id FROM " . $tablename . " WHERE regprocstep_id = :id;";
            $pdoStatement = $this->_pdoObj->prepare($sql, array(
                PDO::ATTR_CURSOR => PDO::CURSOR_FWDONLY
            ));
            $pdoStatement->execute(Array(
                ':id' => $recordid
            ));
            if ($pdoStatement->errorCode() * 1 != 0) {
                $this->addError('', $pdoStatement->errorInfo(), 1);
            } else {
                $row = $pdoStatement->fetch();
            }
            $sequence = $row['sequence'];
            $extension_id = $row['extension_id'];
            $regprocstep_id = $row['regprocstep_id'];
            
            $sql = "DELETE FROM " . $tablename . "
					WHERE regprocstep_id = :id;";
            $pdoStatement = $this->_pdoObj->prepare($sql, array(
                PDO::ATTR_CURSOR => PDO::CURSOR_FWDONLY
            ));
            $pdoStatement->execute(Array(
                ':id' => $recordid
            ));
            if ($pdoStatement->errorCode() * 1 != 0) {
                $this->addError('', $pdoStatement->errorInfo(), 1);
            }
            
            if ($sequence > 0) {
                $sql = "UPDATE " . $tablename . " 
						SET sequence = sequence - 1 WHERE sequence > :seq;";
                $pdoStatement = $this->_pdoObj->prepare($sql, array(
                    PDO::ATTR_CURSOR => PDO::CURSOR_FWDONLY
                ));
                $pdoStatement->execute(Array(
                    ':seq' => $sequence
                ));
                if ($pdoStatement->errorCode() * 1 != 0) {
                    $this->addError('', $pdoStatement->errorInfo(), 1);
                }
            }
        } catch (Exception $e) {
            $this->addError('', 'Error removing registration process step from event', 1, $e->getMessage());
        }
        if (count($this->_err) == 0) {
            $ext = new extension($this->_lg, $this->_locale);
            $classname = 'ext_' . $ext->getExtensionName($extension_id);
            $c = new $classname($this->_lg, $this->_locale);
            $c->removeStep($eventid, $regprocstep_id);
        }
        
        if (count($this->_err) == 0) {
            $res = true;
        }
        return $res;
    }

    public function getStepListForEvent($eventid)
    {
        $res = $this->readStepListForEvent($eventid);
        return $res;
        ;
    }

    private function printStepListForEvent($eventid)
    {
        $slfe = $this->readStepListForEvent($eventid);
        $sm = new L5Smarty();
        $sm->assign('regprocsteps', $slfe);
        $xout = $sm->fetch('event/steplistforevent.tpl');
        return $xout;
    }

    private function readStepListForEvent($eventid)
    {
        $listOfSteps = array();
        if (is_numeric($eventid)) {
            try {
                $sql = "SELECT r.regprocstep_id, r.sequence, e.fname, e.title, r.description, r.active
					FROM zregprocstep" . $eventid . " r
						LEFT JOIN extension e ON r.extension_id = e.extension_id
					ORDER BY sequence ASC
					;";
                $this->_pdoObj = dbconnection::getInstance();
                $pdoStatement = $this->_pdoObj->prepare($sql, array(
                    PDO::ATTR_CURSOR => PDO::CURSOR_FWDONLY
                ));
                $pdoStatement->execute(Array());
                if ($pdoStatement->errorCode() * 1 != 0) {
                    $this->addError('', $pdoStatement->errorInfo(), 1);
                } else {
                    while ($row = $pdoStatement->fetch(PDO::FETCH_ASSOC)) {
                        try {
                            $classname = 'ext_' . $row['fname'];
                            $c = new $classname($this->_lg, $this->_locale);
                            $row['stepdetails'] = $c->getWorkflowItem($row['regprocstep_id']);
                        } catch (Exception $e) {
                            $this->addError('', 'Error getting step details (ext)', 1, $e->getMessage());
                        }
                        $listOfSteps[] = $row;
                    }
                }
            } catch (Exception $e) {
                $this->addError('', 'Error reading registration process step list', 1, $e->getMessage());
            }
        }
        return $listOfSteps;
    }

    public function addStepToEvent($event_id, $extension_id, $description = '')
    {
        $res = false;
        $insertId = false;
        try {
            $sql = "INSERT INTO zregprocstep" . $event_id . "
					(`sequence`, `active`, `description`, `extension_id`)
					SELECT IF(COUNT(*)=0,1,MAX(`sequence`) + 1), 1, :desc, :ext
					FROM zregprocstep" . $event_id . ";";
            
            $this->_pdoObj = dbconnection::getInstance();
            $pdoStatement = $this->_pdoObj->prepare($sql, array(
                PDO::ATTR_CURSOR => PDO::CURSOR_FWDONLY
            ));
            $sqlArr = array(
                ':ext' => $extension_id,
                ':desc' => $description
            );
            $pdoStatement->execute($sqlArr);
            if ($pdoStatement->errorCode() * 1 != 0) {
                $this->addError('', $pdoStatement->errorInfo(), 1);
            } else {
                $insertId = $this->_pdoObj->lastInsertId();
                $res = true;
            }
            
            /**
             * extension-specific "add" code needs to be executed
             * some extensions might need to add database columns,
             * for example.
             */
            if ($insertId != false) {
                $ext = new extension($this->_lg, $this->_locale);
                $classname = 'ext_' . $ext->getExtensionName($extension_id);
                $c = new $classname($this->_lg, $this->_locale);
                $c->setupStep($this->_eventId, $insertId);
            }
        } catch (Exception $e) {
            $this->addError('', 'Error adding step to event', 1, $e->getMessage());
        }
        return $res;
    }

    public function setEventId($id)
    {
        $res = false;
        if ($this->_eventId == null) {
            $this->_eventId = $id;
            $res = true;
        }
        return $res;
    }

    public function apiGetRecord($gt)
    {
        $res = array();
        $row = $this->readStep($this->_recordId);
        if (strlen($row['fname']) > 0) {
            if ($gt['action'] == 'editpopup') {
                // in this case, we need to return html code.
                // the html code is extension-specific.
                $classname = 'ext_' . $row['fname'];
                $c = new $classname($this->_lg, $this->_locale);
                $res['html'] = $c->getSettingsPage($row['regprocstep_id']);
            }
        } else {
            $this->addError('', 'Invalid fname ' . $row['fname'] . ' for record ' . $this->_recordId . '.');
        }
        return $res;
    }

    private function readStep($regprocstep_id)
    {
        $row = array();
        try {
            $sql = "SELECT r.regprocstep_id, r.sequence, e.fname, e.title, r.description, r.active
					FROM zregprocstep" . $this->_eventId . " r
						LEFT JOIN extension e ON r.extension_id = e.extension_id
					WHERE r.regprocstep_id = :id
					;";
            $this->_pdoObj = dbconnection::getInstance();
            $pdoStatement = $this->_pdoObj->prepare($sql, array(
                PDO::ATTR_CURSOR => PDO::CURSOR_FWDONLY
            ));
            $pdoStatement->execute(Array(
                'id' => $regprocstep_id
            ));
            if ($pdoStatement->errorCode() * 1 != 0) {
                $this->addError('', $pdoStatement->errorInfo(), 1);
            } else {
                $row = $pdoStatement->fetch(PDO::FETCH_ASSOC);
            }
        } catch (Exception $e) {
            $this->addError('', 'Error reading registration process step', 1, $e->getMessage());
        }
        return $row;
    }
}