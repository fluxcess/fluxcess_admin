<?php

class portal extends l5sys
{

    /**
     * determines if the user is logged in
     *
     * @var boolean
     */
    private $_loggedin = false;

    /**
     * link to the current page, excluding the language prefix
     *
     * @var string
     */
    private $_link = '';

    /**
     * is "api" if the request is an api request.
     *
     * @var string|null
     */
    private $_requestType = null;

    /**
     * contains the api version, if given.
     *
     * @var string|null
     */
    private $_apiVersion = null;

    /**
     * contains a list of available font names
     *
     * @var array
     */
    private $_fontfamilies = array(
        0 => array(
            'Arial, Helvetica, sans-serif'
        ),
        1 => array(
            '"Arial Black", Gadget, sans-serif'
        ),
        2 => array(
            '"Comic Sans MS", cursive, sans-serif'
        ),
        3 => array(
            'Impact, Charcoal, sans-serif'
        ),
        4 => array(
            '"Lucida Sans Unicode", "Lucida Grande", sans-serif'
        ),
        5 => array(
            'Tahoma, Geneva, sans-serif'
        ),
        6 => array(
            '"Trebuchet MS", Helvetica, sans-serif'
        ),
        7 => array(
            'Verdana, Geneva, sans-serif'
        ),
        8 => array(
            '"Courier New", Courier, monospace'
        ),
        9 => array(
            '"Lucida Console", Monaco, monospace'
        ),
        10 => array(
            'Georgia, serif'
        ),
        11 => array(
            '"Palatino Linotype", "Book Antiqua", Palatino, serif'
        ),
        12 => array(
            '"Times New Roman", Times, serif'
        )
    );

    const LOCALE_EN_EN = 'en_US.UTF-8';

    const LOCALE_DE_DE = 'de_DE.UTF-8';

    function __construct()
    {
        session_start();
        if ($_SESSION['loggedin'] == true || LIVEENV === true) {
            $this->_loggedin = true;
        } elseif (strlen(INTERFACE_REMOTE_IPS) > 0 && array_search(getenv('REMOTE_ADDR'), explode(',', INTERFACE_REMOTE_IPS)) !== false) {
            $_SESSION['loggedin'] = true;
            $this->_loggedin = true;
            $_SESSION['user']['id'] = INTERFACE_AUTOLOGIN_USERID;
            $_SESSION['user']['email'] = INTERFACE_AUTOLOGIN_EMAILADDRESS;
            // ToDo: set the real customer id here!
            $_SESSION['customer']['customer_id'] = INTERFACE_AUTOLOGIN_CUSTOMERID;
        }
        
        if (LIVEENV === true) {
            // $_SESSION['event']['id'] = LIVEEVENTID;
            if (PRESETUSERID) {
                $_SESSION['customer']['customer_id'] = 1;
                $_SESSION['user']['id'] = PRESETUSERID;
            }
        }
        $lgStoredInSession = false;
        if (isset($_SESSION['lg'])) {
            $this->_lg = $_SESSION['lg'];
            $lgStoredInSession = true;
        }
        if (isset($_SESSION['locale'])) {
            $this->_locale = $_SESSION['locale'];
        }
        if (isset($_GET['type'])) {
            if ($_GET['type'] != 'ajax' && $_GET['type'] != 'editable' && $_GET['type'] != 'iframe' && $_GET['type'] != 'download') {
                $this->parseInput();
            } else {
                $this->_gt = $_GET;
                
                if ($_GET['lg'] == 'de' || $_POST['lg'] == 'de') {
                    $locale = self::LOCALE_DE_DE;
                    $lg = 'de';
                } else {
                    if ($_GET['lg'] == 'en' || $_POST['lg'] == 'en') {
                        $locale = self::LOCALE_EN_EN;
                        $lg = 'en';
                    } elseif ($lgStoredInSession) {
                        $locale = $_SESSION['locale'];
                        $lg = $_SESSION['lg'];
                    }
                }
                $this->_locale = $locale;
                $this->_lg = $lg;
                $_SESSION['lg'] = $lg;
                $_SESSION['locale'] = $locale;
            }
        } else {
            $this->parseInput();
        }
        putenv("LC_ALL=" . $this->_locale);
        putenv("LANGUAGE=" . $this->_locale);
        setlocale(LC_ALL, $this->_locale);
        bindtextdomain("messages", BASEDIR . "/locale/"); // binds the messages domain to the locale folder
        bind_textdomain_codeset("messages", "UTF-8"); // ensures text returned is utf-8, quite often this is iso-8859-1 by default
        textdomain("messages");
    }

    public function start()
    {
        global $glob;
        if ($this->_loggedin == true) {
            $this->logRequest();
        }
        $proceed = true; // if proceed is true, proceed to full html output - otherwise don't
        $blank = false; // if blank is true, do not use the entire index.html backend template
        $content = '';
        if ($this->_requestType == 'api') {
            // collect credentials...
            $tryingUsername = '';
            $tryingPassword = '';
            $tryToLogin = false;
            if (isset($_SERVER['PHP_AUTH_USER']) && strlen($_SERVER['PHP_AUTH_USER']) > 1) {
                // http auth
                $tryingUsername = $_SERVER['PHP_AUTH_USER'];
                $tryingPassword = $_SERVER['PHP_AUTH_PW'];
                $tryToLogin = true;
            } elseif (isset($_GET['login'])) {
                // GET request parameters
                $tryingUsername = $_GET['login'];
                $tryingPassword = $_GET['pw'];
                $tryToLogin = true;
            } elseif (isset($_POST['login'])) {
                // POST request parameters
                $tryingUsername = $_POST['login'];
                $tryingPassword = $_POST['pw'];
                $tryToLogin = true;
            }
            
            if ($this->_loggedin == true && $tryToLogin === false) {
                // Option A: user is logged in already and has a session.
                $api = new api($this->_apiVersion, $this->_gt, $this->_lg, $this->_locale);
                $api->start();
            } elseif (strlen(INTERFACE_REMOTE_IPS) > 0 && array_search(getenv('REMOTE_ADDR'), explode(',', INTERFACE_REMOTE_IPS)) !== false) {
                // Option B: IP of this user is set for "no login necessary"
                $_SESSION['loggedin'] = true;
                $this->_loggedin = true;
                $_SESSION['user']['id'] = INTERFACE_AUTOLOGIN_USERID;
                $_SESSION['user']['email'] = INTERFACE_AUTOLOGIN_EMAILADDRESS;
                $_SESSION['customer']['customer_id'] = INTERFACE_AUTOLOGIN_CUSTOMERID;
                
                $api = new api($this->_apiVersion, $this->_gt, $this->_lg, $this->_locale);
                $api->start();
            } elseif ($tryToLogin === true) {
                // Option C: try to login with given credentials
                $this->handleLogin($tryingUsername, $tryingPassword);
                if ($this->_loggedin == true) {
                    $api = new api($this->_apiVersion, $this->_gt, $this->_lg, $this->_locale);
                    $api->start();
                } else {
                    header('HTTP/1.0 401 Unauthorized');
                    header('WWW-Authenticate: Basic realm="Authentication failed. Please try again."');
                    echo 'authentication failed';
                }
            } else {
                // If everything else fails: return an error message.
                header('WWW-Authenticate: Basic realm="Please identify."');
                header('HTTP/1.0 401 Unauthorized');
                echo 'Please identify.';
                exit();
            }
        } else {
            if (isset($_POST['form']) && isset($_POST['class'])) {
                
                /* cases within other classes */
                if ($this->_loggedin === true) {
                    switch ($_POST['class']) {
                        case 'account':
                            $xout = $this->handleDeleteAccount();
                            $proceed = false;
                            break;
                        case 'crm':
                            $cm = new crm();
                            $content = $cm->printPage($this->_gt, $_POST, $this->_err, $proceed);
                            $proceed = false;
                            break;
                        case 'domain':
                            $dm = new domain($this->_lg, $this->_locale);
                            $content = $dm->printPage($this->_gt, $_POST, $this->_err, $proceed);
                            $proceed = false;
                            break;
                        case 'event':
                            $ev = new event($this->_lg, $this->_locale);
                            $content = $ev->printPage($this->_gt, $_POST, $this->_err, $proceed);
                            $proceed = false;
                            break;
                        case 'guest':
                            $gs = new guest();
                            $content = $gs->printPage($this->_gt, $_POST, $this->_err, $proceed);
                            $proceed = false;
                            break;
                    }
                }
            } elseif (isset($this->_gt['type']) && $this->_gt['type'] == 'editable' && isset($this->_gt['class']) && $this->_loggedin === true) {
                switch ($_GET['class']) {
                    case 'guest':
                        $gs = new guest();
                        $gs->printPage($this->_gt, $_POST, $this->_err);
                        $proceed = false;
                        break;
                }
                $proceed = false;
            } elseif (isset($_POST['form'])) {
                /* cases within this class */
                $this->_ps = $_POST;
                switch ($_POST['form']) {
                    case 'login':
                        $proceed = false;
                        $this->handleLogin($this->_ps['email'], $this->_ps['password']);
                        if (count($this->_err) == 0) {
                            $resp['redirect'] = '/';
                        } else {
                            $blank = true;
                            $proceed = true;
                        }
                        break;
                    case 'register':
                        $content = $this->handleRegistration();
                        $proceed = false;
                        break;
                    case 'reset':
                        $content = $this->handleReset();
                        $proceed = false;
                        break;
                    case 'resetpw':
                        $content = $this->handleResetPw();
                        $proceed = false;
                        break;
                }
            }
            if ($proceed == true) {
                /* if logged in ... */
                if ($this->_loggedin == true) {
                    if (isset($this->_gt['type']) && ($this->_gt['type'] == 'iframe' || $this->_gt['type'] == 'download')) {
                        $sw = $this->_gt['module'];
                    } else {
                        $sw = $this->_gt[0];
                    }
                    switch ($sw) {
                        /* Cases within other classes: */
                        case 'account':
                            $content = $this->printAccountPage();
                            break;
                        case 'domain':
                            $dm = new domain($this->_lg, $this->_locale);
                            $content = $dm->printPage($this->_gt, $this->_ps, $this->_err, $proceed);
                            break;
                        case 'event':
                            $ev = new event($this->_lg, $this->_locale);
                            $content = $ev->printPage($this->_gt, $this->_ps, $this->_err, $proceed);
                            break;
                        case 'guest':
                            $gs = new guest();
                            $content = $gs->printPage($this->_gt, $this->_ps, $this->_err, $proceed);
                            break;
                        /* Cases within this class: */
                        case 'logout':
                            $this->logout();
                            $blank = true;
                            break;
                        case 'print':
                            $ev = new event($this->_lg, $this->_locale);
                            if ($_SESSION['event']['fevent_id'] == 284) {
                                $content = $ev->printTicketPDF($this->_gt, $this->_ps, $this->_err, $proceed);
                                die();
                            } else {
                                $content = $ev->printTicket($this->_gt, $this->_ps, $this->_err, $proceed);
                            }
                            break;
                        case 'printpdf':
                            $ev = new event($this->_lg, $this->_locale);
                            $content = $ev->printTicketPDF($this->_gt, $this->_ps, $this->_err, $proceed);
                            break;
                        case 'registerconfirm':
                            $content = $this->printRegistrationConfirmationPage();
                            $blank = true;
                            break;
                        default:
                            if (LIVEENV === true) {
                                $this->_lg = 'de';
                                $this->_locale = 'de_DE';
                                $ev = new event($this->_lg, $this->_locale);
                                $this->_gt[1] = $_SESSION['event']['id'];
                                // $content = $ev->printPage($this->_gt, $this->_ps, $this->_err, $proceed);
                                $content = $this->printHomePage();
                            } else {
                                $content = $this->printHomePage();
                            }
                    }
                } else {
                    switch ($this->_gt[0]) {
                        /* Cases within this class: */
                        case 'logout':
                            $this->logout();
                            $blank = true;
                            break;
                        case 'register':
                            $content = $this->printRegistrationForm();
                            $blank = true;
                            break;
                        case 'registerconfirm':
                            $content = $this->printRegistrationConfirmationPage();
                            $blank = true;
                            break;
                        case 'reset':
                            $content = $this->printResetForm();
                            $blank = true;
                            break;
                        case 'resetpw':
                            $content = $this->printResetPwForm();
                            $blank = true;
                            break;
                        default:
                            $content = $this->printSigninPage();
                            $blank = true;
                    }
                }
            }
            
            if (isset($this->_gt['type']) && $this->_gt['type'] == 'ajax' && $this->_gt['datatables'] == 1) {} elseif (isset($this->_gt['type']) && $this->_gt['type'] == 'editable' && $this->_loggedin === true) {
                if (count($this->_err) > 0) {
                    header($_SERVER['SERVER_PROTOCOL'] . ' 500 Internal Server Error', true, 500);
                    foreach ($this->_err as $e) {
                        echo $e['note'] . ' ';
                    }
                }
            } elseif (isset($this->_gt['type']) && $this->_gt['type'] == 'ajax') {
                $resp['err'] = $this->_err;
                $resp['cont'] = $content;
                echo json_encode($resp);
            } elseif (isset($this->_gt['type']) && $this->_gt['type'] == 'iframe' && $this->_loggedin === true) {
                echo $this->printIFrame($content);
            } elseif (isset($this->_gt['type']) && $this->_gt['type'] == 'download' && $this->_loggedin === true && $this->_gt['dltype'] == 'xlsx') {
                // $this->printExport($content, 'guests_'.$_SESSION['event']['fevent_id'].date('Y-m-d__H:i:s').'.csv');
            } elseif (isset($this->_gt['type']) && $this->_gt['type'] == 'download' && $this->_loggedin === true) {
                $this->printExport($content, 'guests_' . $_SESSION['event']['fevent_id'] . date('Y-m-d__H:i:s') . '.csv');
            } else {
                echo $this->printPage($content, $blank);
            }
        }
    }

    private function printExport($content, $filename)
    {
        header('Content-type: text/csv; charset=utf-8');
        header('Content-Disposition: attachment; filename="download.csv"');
        echo $content;
    }

    /**
     * returns html code of index.html
     *
     * @param string $content
     * @return Ambigous <string, void, string>
     */
    private function printPage($content, $blank = false)
    {
        global $gsLocal;
        if ($this->_gt[0] == 'print') {
            $sm = new L5Smarty();
            $sm->assign('content', $content);
            $sm->assign('blank', $blank);
            $sm->assign('username', $_SESSION['user']['email']);
            $sm->assign('link', $this->_link);
            $sm->assign('TESTING', TESTING);
            $xout = $sm->fetch('event/ticket.tpl');
        } else {
            $ev = new event($this->_lg, $this->_locale);
            $eventdata = null;
            if (LIVEENV === true) {
                $eventid = $_SESSION['event']['id'];
            } elseif (isset($_SESSION['fevent_id'])) {
                $eventid = $_SESSION['fevent_id'];
            }
            $eventlistMenu = $ev->printEventListMenu($_SESSION['event']['id']);
            $eventlist = $ev->getEventList();
            if (! isset($this->_ps['massmailrecipients']) || ! is_array($this->_ps['massmailrecipients'])) {
                $this->_ps['massmailrecipients'] = array();
            }
            $ex = new extension($this->_lg, $this->_locale);
            $rps = new regprocstep($this->_lg, $this->_locale);
            
            $crme = new crm($this->_lg, $this->_locale);
            $crmenabled = $crme->checkCRMModule();
            
            $internalfilex = new internalfile($this->_lg, $this->_locale);
            $internalfilelist = $internalfilex->getList();
            
            $showStats = true;
            if (DEVENV == true) {
                $showStats = false;
            }
            $sm = new L5Smarty();
            $sm->assign('content', $content);
            $sm->assign('blank', $blank);
            $sm->assign('eventlistMenu', $eventlistMenu);
            $sm->assign('eventlist', $eventlist);
            $sm->assign('username', $_SESSION['user']['email']);
            $sm->assign('link', $this->_link);
            $sm->assign('TESTING', TESTING);
            $sm->assign('LIVEENV', LIVEENV);
            $sm->assign('availableextensions', $ex->getForOptionList());
            $sm->assign('CHECKIN_APP', CHECKIN_APP);
            $sm->assign('eventid', $eventid);
            $sm->assign('fevent_id', $eventid);
            $sm->assign('STATS', $showStats);
            $sm->assign('internalfiles', $internalfilelist);
            $sm->assign('lg', $this->_lg);
            $sm->assign('gsLocal', $gsLocal);
            $sm->assign('ps', $this->_ps);
            $sm->assign('crmenabled', $crmenabled);
            $sm->assign('sitedomain', getenv('HTTP_HOST'));
            $sm->assign('smtpsecureval', $this->generateSmtpSecureVals());
            $sm->assign('smtpsecureout', $this->generateSmtpSecureVals());
            $sm->assign('ffamval', $this->generateFontFamilyOptions());
            $sm->assign('ffamout', $this->generateFontFamilyOptions());
            
            $sm->assign('regprocsteps', $rps->getStepListForEvent($eventid));
            
            $xout = $sm->fetch('index.' . BASETEMPLATE . '.tpl');
        }
        return $xout;
    }

    private function printIFrame($content, $blank = false)
    {
        $ev = new event($this->_lg, $this->_locale);
        $eventlist = $ev->printEventListMenu();
        $sm = new L5Smarty();
        $sm->assign('content', $content);
        $sm->assign('username', $_SESSION['user']['email']);
        $sm->assign('link', $this->_link);
        $sm->assign('TESTING', TESTING);
        $sm->assign('LIVEENV', LIVEENV);
        $xout = $sm->fetch('portal/iframe.tpl');
        return $xout;
    }

    /**
     * returns the HTML code of the start page
     *
     * @return Ambigous <string, void, string>
     */
    private function printHomePage()
    {
        $ev = new event($this->_lg, $this->_locale);
        $sm = new L5Smarty();
        $sm->assign('eventlist', $ev->getEventList());
        $content = $sm->fetch('portal/home.tpl');
        return $content;
    }

    /**
     * returns the HTML code of the signin page
     *
     * @return Ambigous <string, void, string>
     */
    private function printSigninPage()
    {
        $sm = new L5Smarty();
        $sm->assign('lg', $this->_lg);
        $sm->assign('sitedomain', getenv('HTTP_HOST'));
        $content = $sm->fetch('portal/signin.tpl');
        return $content;
    }

    /**
     * reads the query string (URL)
     * and fills the $this->_gt variable
     */
    private function parseInput()
    {
        $inp = getenv('QUERY_STRING');
        $inps = explode('/', $inp);
        $link = getenv('REQUEST_URI');
        $locale = '';
        $lg = '';
        if (substr($link, 0, 3) == '/en' || substr($link, 0, 3) == '/de') {
            $this->_lg = substr($link, 1, 2);
            if ($this->_lg == 'de') {
                $locale = self::LOCALE_DE_DE;
                $lg = 'de';
            } else {
                $locale = self::LOCALE_EN_EN;
                $lg = 'en';
            }
            $this->_link = substr($link, 3);
            array_shift($inps);
        } elseif (substr($link, 0, 4) == '/api') {
            $this->_requestType = 'api';
            array_shift($inps);
            if (substr($link, 4, 2) == '/V') {
                $this->_apiVersion = substr($inps[0], 1);
                array_shift($inps);
            }
            if ($_GET['lg'] == 'de' || $_POST['lg'] == 'de') {
                $locale = self::LOCALE_DE_DE;
                $lg = 'de';
            } elseif ($_GET['lg'] == 'en' || $_POST['lg'] == 'en') {
                $locale = self::LOCALE_EN_EN;
                $lg = 'en';
            } else {
                if (isset($_SESSION['lg']) && isset($_SESSION['locale'])) {
                    $locale = $_SESSION['locale'];
                    $lg = $_SESSION['lg'];
                } else {
                    $locale = self::LOCALE_EN_EN;
                    $lg = 'en';
                }
            }
        } else {
            if ($_GET['lg'] == 'de' || $_POST['lg'] == 'de') {
                $locale = self::LOCALE_DE_DE;
                $lg = 'de';
            } elseif ($_GET['lg'] == 'en' || $_POST['lg'] == 'en') {
                $locale = self::LOCALE_EN_EN;
                $lg = 'en';
            } else {
                if (isset($_SESSION['lg']) && isset($_SESSION['locale'])) {
                    $locale = $_SESSION['locale'];
                    $lg = $_SESSION['lg'];
                } else {
                    $locale = self::LOCALE_EN_EN;
                    $lg = 'en';
                }
            }
        }
        $this->_locale = $locale;
        $this->_lg = $lg;
        if (strlen($lg) > 0) {
            $_SESSION['lg'] = $lg;
            $_SESSION['locale'] = $locale;
        }
        $this->_gt = $inps;
    }

    /**
     * returns html code for the new user registration form
     *
     * @return Ambigous <string, void, string>
     */
    private function printRegistrationForm()
    {
        $sm = new L5Smarty();
        $xout = $sm->fetch('portal/register.tpl');
        return $xout;
    }

    /**
     * returns html code for the password reset form (email address form only)
     *
     * @return Ambigous <string, void, string>
     */
    private function printResetForm()
    {
        $sm = new L5Smarty();
        $xout = $sm->fetch('portal/reset.tpl');
        return $xout;
    }

    /**
     * returns html code for the page shown when the user confirms his email address
     * needs to check if the email address and confirmation link are valid and not expired
     *
     * @return Ambigous <string, void, string>
     */
    private function printRegistrationConfirmationPage()
    {
        // Check: valid email?
        $mail = filter_var($this->_gt[1], FILTER_VALIDATE_EMAIL);
        if ($mail === false) {
            $this->addError('', gettext('The link is invalid. Please request a new one.') . ' (E-Mail).');
        }
        
        // check if code is long enough
        if (strlen($this->_gt[2]) < 16) {
            $this->addError('', gettext('The link is invalid. Please request a new one.') . ' (Code).');
        }
        
        // Check if code is valid and not expired yet
        $userid = $this->checkMailCode($mail, $this->_gt[2]);
        
        if (count($this->_err) == 0) {
            // fill confirmedtime and ip if empty
            try {
                $sql = "UPDATE fuser SET
						confirmedtime = NOW(),
						confirmedip = :ip,
						mailconfcode = NULL,
						mailconfcodetime = NULL
						WHERE fuser_id = :userid
						AND confirmedtime IS NULL;";
                $pdoStatement = $this->_pdoObj->prepare($sql, array(
                    PDO::ATTR_CURSOR => PDO::CURSOR_FWDONLY
                ));
                $pdoStatement->execute(array(
                    ':userid' => $userid,
                    ':ip' => getenv('REMOTE_ADDR')
                ));
                if ($pdoStatement->errorCode() > 0) {
                    $this->addError('', 'SQL Fehler', 1, print_r($pdoStatement->errorInfo(), true));
                }
            } catch (Exception $e) {
                $this->addError('', 'DB Fehler', 1, print_r($e->getMessage(), true));
            }
        }
        
        $sm = new L5Smarty();
        $sm->assign('err', $this->_err);
        $xout = $sm->fetch('portal/registrationconfirmationpage.tpl');
        return $xout;
    }

    /**
     * returns the html code for the password reset form if
     * the user forgot his password.
     * User reaches this form by clicking a link in an email,
     * hence the data from this link has to be checked.
     *
     * @return Ambigous <string, void, string>
     */
    private function printResetPwForm()
    {
        // Check if email address is valid
        $mail = filter_var($this->_gt[1], FILTER_VALIDATE_EMAIL);
        if ($mail === false) {
            $this->addError('', gettext('The link is invalid. Please request a new one.') . ' (E-Mail).');
        }
        
        // check if code is long enough
        if (strlen($this->_gt[2]) < 16) {
            $this->addError('', gettext('The link is invalid. Please request a new one.') . ' (Code).');
        }
        
        // Check if code is valid and not expired yet
        $this->checkMailCode($mail, $this->_gt[2]);
        
        // print form
        $sm = new L5Smarty();
        $sm->assign('email', $this->_gt[1]);
        $sm->assign('code', $this->_gt[2]);
        $sm->assign('err', $this->_err);
        $xout = $sm->fetch('portal/resetpw.tpl');
        return $xout;
    }

    /**
     * Checks if a confirmation mail code and an email address
     * match to data in the db
     *
     * @param string $email
     * @param string $code
     * @return boolean
     */
    private function checkMailCode($email, $code)
    {
        $res = false;
        $sql = "SELECT UNIX_TIMESTAMP(mailconfcodetime) tm, fuser_id FROM fuser u WHERE
				email = :email AND mailconfcode = :code
				AND mailconfcode IS NOT NULL;";
        $this->_pdoObj = dbconnection::getInstance();
        $pdoStatement = $this->_pdoObj->prepare($sql, array(
            PDO::ATTR_CURSOR => PDO::CURSOR_FWDONLY
        ));
        $pdoStatement->execute(array(
            ':email' => $email,
            ':code' => $code
        ));
        if ($pdoStatement->errorCode() != 0) {
            $this->addError('', gettext('SQL Error'), 1, print_r($pdoStatement->errorInfo(), true));
        } elseif ($pdoStatement->rowCount() == 0) {
            $this->addError('', gettext('The code is invalid. Please request a new one.') . ' (Code DB)', 1, __FILE__ . '/' . __LINE__);
        } else {
            $row = $pdoStatement->fetch();
            if ($row['tm'] < time() - (60 * 60 * 8)) {
                $this->addError('', gettext('The link for resetting the password has expired. Please request a new one.'));
            } else {
                $res = $row['fuser_id'];
            }
        }
        return $res;
    }

    /**
     * handles user input from the form which is used to
     * register a new user.
     *
     * @return Ambigous <string, void, string>
     */
    private function handleRegistration()
    {
        $xout = '';
        // check if mail address is valid
        $mail = filter_var($this->_ps['email'], FILTER_VALIDATE_EMAIL);
        if ($mail === false) {
            $this->addError('email', gettext('Please provide a valid email address.'));
        } else {
            $this->_ps['email'] = $mail;
        }
        
        // check if mail address exists already
        if ($this->checkForExistingUser($this->_ps['email']) == true) {
            $this->addError('email', gettext('There is already an account for this email address.'));
        }
        
        // check if passwords are equal
        if ($this->_ps['password'] != $this->_ps['passwordrpt']) {
            $this->addError('passwordrpt', gettext('The two provided passwords do not match.'));
        }
        // check if passwords are strong enough
        $this->_ps['password'] = trim($this->_ps['password']);
        if (strlen($this->_ps['password']) < 6) {
            $this->addError('password', gettext('The password needs to consist of at least 6 characters.'));
        }
        
        // check if AGB are confirmed
        if ($this->_ps['agb'] != 'agb') {
            $this->addError('agb', gettext('Please confirm that you accept the TOC/AGB.'));
        }
        
        if ($this->_ps['newsletter'] != '1') {
            $this->_ps['newsletter'] = 0;
        }
        if ($this->_ps['contactme'] != '1') {
            $this->_ps['contactme'] = 0;
        }
        if ($this->_ps['survey'] != '1') {
            $this->_ps['survey'] = 0;
        }
        
        // check if registration is allowed on this instance
        if (NEW_USER_REGISTRATION_ALLOWED !== true) {
            $this->addError('', gettext('I am sorry, there seems to be no registration permission for your email address. Please contact the support.'));
        }
        
        // generate mailconfcode
        $mailconfcode = $this->genPass(33);
        $salt = $this->genPass(20);
        $pass = hash('sha512', $salt . $this->_ps['password']);
        
        if (count($this->_err) == 0) {
            try {
                $this->_pdoObj->beginTransaction();
                $sql = "INSERT INTO fuser SET
						email = :email, password = :password,
						createdIp = :createdIp, mailconfcode = :mailconfcode,
						newsletter = :newsletter, survey = :survey, contactme = :contactme,
						salt = :salt, mailconfcodetime = NOW()
						;";
                $this->_pdoObj = dbconnection::getInstance();
                $pdoStatement = $this->_pdoObj->prepare($sql, array(
                    PDO::ATTR_CURSOR => PDO::CURSOR_FWDONLY
                ));
                $pdoStatement->execute(array(
                    ':email' => $this->_ps['email'],
                    ':password' => $pass,
                    ':createdIp' => getenv('REMOTE_ADDR'),
                    ':mailconfcode' => $mailconfcode,
                    ':salt' => $salt,
                    ':newsletter' => $this->_ps['newsletter'],
                    ':survey' => $this->_ps['survey'],
                    ':contactme' => $this->_ps['contactme']
                ));
                if ($pdoStatement->errorCode() != 0) {
                    $this->addError('', 'SQL Fehler' . print_r($pdoStatement->errorInfo(), true), 1);
                } else {
                    $fuser_id = $this->_pdoObj->lastInsertId();
                    
                    $this->createCustomer($fuser_id, $this->_ps['email']);
                    
                    // send mail
                    $sm = new L5Smarty();
                    $confirmationlink = 'http';
                    if ($_SERVER['HTTPS'] == 'on') {
                        $confirmationlink .= 's';
                    }
                    $confirmationlink .= '://';
                    $confirmationlink .= getenv('HTTP_HOST');
                    
                    $confirmationlink .= '/registerconfirm/' . $this->_ps['email'] . '/' . $mailconfcode;
                    /*
                     * if (TESTING == true) {
                     * $confirmationlink = 'http://admintest.gästeliste.org/registerconfirm/' . $this->_ps['email'] . '/' . $mailconfcode;
                     * }
                     */
                    $sm->assign('link', $confirmationlink);
                    $txt = $sm->fetch('portal/mailConfirmNewAccount.txt.tpl');
                    $html = $sm->fetch('portal/mailConfirmNewAccount.html.tpl');
                    try {
                        $this->sendEmailToUser(gettext('Please confirm your email address.'), $html, $txt, $this->_ps['email']);
                    } catch (Exception $e) {
                        $this->addError('', gettext('Registration Email error. Please notify the customer service.'), - 1, __FILE__ . '/' . __LINE__ . ':' . print_r($e->getMessage(), true));
                    }
                    
                    // Login user
                    $this->loginUser($this->_ps['email'], $this->_ps['password']);
                }
                $this->_pdoObj->commit();
            } catch (Exception $e) {
                $this->addError('', gettext('Registration error. Please notify the customer service.'), 1, __FILE__ . '/' . __LINE__ . ':' . print_r($e->getMessage(), true));
                $this->_pdoObj->rollBack();
            }
        }
        
        if (count($this->_err) == 0) {
            $sm = new L5Smarty();
            $xout = $sm->fetch('portal/registerthankyou.tpl');
        }
        return $xout;
    }

    private function createCustomer($fuser_id, $customer_name)
    {
        try {
            $fcustomer_id = null;
            $sql = "INSERT INTO fcustomer SET
						fcustomer_name = :fcustomer_name
						;";
            $this->_pdoObj = dbconnection::getInstance();
            $pdoStatement = $this->_pdoObj->prepare($sql, array(
                PDO::ATTR_CURSOR => PDO::CURSOR_FWDONLY
            ));
            $pdoStatement->execute(array(
                ':fcustomer_name' => $customer_name
            ));
            if ($pdoStatement->errorCode() != 0) {
                $this->addError('', 'SQL Fehler' . print_r($pdoStatement->errorInfo(), true), 1);
            } else {
                $fcustomer_id = $this->_pdoObj->lastInsertId();
            }
            if (count($this->_err) == 0) {
                $sql = "INSERT INTO fcustomer_fuser SET
						fcustomer_id = :fcustomer_id,
						fuser_id = :fuser_id
						;";
                $this->_pdoObj = dbconnection::getInstance();
                $pdoStatement = $this->_pdoObj->prepare($sql, array(
                    PDO::ATTR_CURSOR => PDO::CURSOR_FWDONLY
                ));
                $pdoStatement->execute(array(
                    ':fcustomer_id' => $fcustomer_id,
                    ':fuser_id' => $fuser_id
                ));
                if ($pdoStatement->errorCode() != 0) {
                    $this->addError('', 'SQL Fehler' . print_r($pdoStatement->errorInfo(), true), 1);
                }
            }
        } catch (Exception $e) {
            $this->addError('', gettext('Customer creation error. Please notify the customer service.'), 1, __FILE__ . '/' . __LINE__ . ':' . print_r($e->getMessage(), true));
        }
    }

    /**
     * handles user input from the form where the user types his
     * email address if he forgot the password.
     *
     * @return Ambigous <string, void, string>
     */
    private function handleReset()
    {
        $xout = '';
        // check if email address is valid
        $mail = filter_var($this->_ps['email'], FILTER_VALIDATE_EMAIL);
        if ($mail === false) {
            $this->addError('email', gettext('Please provide a valid email address.'));
        } else {
            $this->_ps['email'] = $mail;
        }
        
        // check if email address exists
        if ($this->checkForExistingUser($this->_ps['email']) == false) {
            $this->addError('email', gettext('This email address is unknown to our systems.'));
        }
        
        // generate code
        $resetcode = $this->genPass(32);
        
        // UPDATE mailcode in fuser table
        if (count($this->_err) == 0) {
            $sql = "UPDATE fuser SET mailconfcode = :mailconfcode,
					mailconfcodetime = NOW() WHERE email = :email AND deleted = 0;";
            $pdoStatement = $this->_pdoObj->prepare($sql, array(
                PDO::ATTR_CURSOR => PDO::CURSOR_FWDONLY
            ));
            $pdoStatement->execute(array(
                ':email' => $this->_ps['email'],
                ':mailconfcode' => $resetcode
            ));
            if ($pdoStatement->errorCode() > 0) {
                $this->addError('', 'SQL Fehler', 1);
            }
        }
        
        // send mail
        if (count($this->_err) == 0) {
            $sm = new L5Smarty();
            if (TESTING === true) {
                $sm->assign('link', 'http://admintest.gästeliste.org/resetpw/' . $this->_ps['email'] . '/' . $resetcode);
            } else {
                $sm->assign('link', 'http://admin.gästeliste.org/resetpw/' . $this->_ps['email'] . '/' . $resetcode);
            }
            $html = $sm->fetch('portal/mailResetPw.html.tpl');
            $txt = $sm->fetch('portal/mailResetPw.txt.tpl');
            $this->sendEmailToUser(gettext('Reset your password'), $html, $txt, $this->_ps['email']);
        }
        
        // Show help text (announce email)
        if (count($this->_err) == 0) {
            $sm = new L5Smarty();
            $xout = $sm->fetch('portal/resetthankyou.tpl');
        }
        return $xout;
    }

    /**
     * handles the user input for the form which is used to change the
     * password when forgotten.
     * returns the thankyou page if successful.
     *
     * @return Ambigous <string, void, string>
     */
    private function handleResetPw()
    {
        $xout = '';
        $this->_ps['password'] = trim($this->_ps['password']);
        
        // Check if email address is valid
        $mail = filter_var($this->_ps['email'], FILTER_VALIDATE_EMAIL);
        if ($mail === false) {
            $this->addError('', gettext('The link is invalid. Please request a new one.') . ' (E)');
        }
        
        // check if code is long enough
        if (strlen($this->_ps['code']) < 16) {
            $this->addError('', gettext('The link is invalid. Please request a new one.') . ' (C)');
        }
        
        // Check if code is valid and not expired yet
        $userid = $this->checkMailCode($mail, $this->_ps['code']);
        
        // Encrypt password
        $salt = $this->genPass(20);
        $pass = hash('sha512', $salt . $this->_ps['password']);
        
        // check length of password
        if (strlen($this->_ps['password']) < 6) {
            $this->addError('password', gettext('The password needs to consist of at least 6 characters.'));
        }
        // check if both password field contents are equal
        if ($this->_ps['password'] != $this->_ps['passwordrpt']) {
            $this->addError('passwordrpt', gettext('The two provided passwords do not match.'));
        }
        
        // Store new password in DB
        if (count($this->_err) == 0) {
            $sql = "UPDATE fuser SET
					password = :pw, salt = :salt,
					mailconfcode = NULL, mailconfcodetime = NULL
					WHERE email = :email AND mailconfcode = :code
					AND deleted = 0;";
            $pdoStatement = $this->_pdoObj->prepare($sql, array(
                PDO::ATTR_CURSOR => PDO::CURSOR_FWDONLY
            ));
            $pdoStatement->execute(array(
                ':email' => $mail,
                ':code' => $this->_ps['code'],
                ':pw' => $pass,
                ':salt' => $salt
            ));
            if ($pdoStatement->errorCode() > 0) {
                $this->addError('', 'SQL Fehler', 1);
            }
        }
        
        // fill confirmedtime and ip if empty
        if (count($this->_err) == 0) {
            $sql = "UPDATE fuser SET
					confirmedtime = NOW(),
					confirmedip = :ip
					WHERE fuser_id = :userid
					AND confirmedtime IS NULL;";
            $pdoStatement = $this->_pdoObj->prepare($sql, array(
                PDO::ATTR_CURSOR => PDO::CURSOR_FWDONLY
            ));
            $pdoStatement->execute(array(
                ':userid' => $userid,
                ':ip' => getenv('REMOTE_ADDR')
            ));
        }
        
        // Show help text (proceed) + Login
        if (count($this->_err) == 0) {
            $sm = new L5Smarty();
            $xout = $sm->fetch('portal/resetpwthankyou.tpl');
            $this->loginUser($mail, $this->_ps['password']);
        }
        return $xout;
    }

    /**
     * checks username and password.
     * If they match, the user is logged in
     * by storing his data in the session.
     *
     * @param string $username
     * @param string $password
     */
    private function loginUser($username, $password)
    {
        $sql = "SELECT fu.fuser_id, fu.email, fu.salt, fu.password, fcu.fcustomer_id
				FROM fuser fu
				JOIN fcustomer_fuser fcu ON fu.fuser_id = fcu.fuser_id 
				WHERE fu.email = :email AND fu.deleted = 0;";
        $this->_pdoObj = dbconnection::getInstance();
        $pdoStatement = $this->_pdoObj->prepare($sql, array(
            PDO::ATTR_CURSOR => PDO::CURSOR_FWDONLY
        ));
        $pdoStatement->execute(array(
            ':email' => $username
        ));
        if ($pdoStatement->rowCount() > 0) {
            $row = $pdoStatement->fetch();
            if (hash('sha512', $row['salt'] . $password) == $row['password']) {
                $_SESSION['loggedin'] = true;
                $this->_loggedin = true;
                $_SESSION['user']['id'] = $row['fuser_id'];
                $_SESSION['user']['email'] = $row['email'];
                // ToDo: set the real customer id here!
                $_SESSION['PHP_AUTH_USER'] = $row['email'];
                $_SESSION['customer']['customer_id'] = $row['fcustomer_id'];
                $sql = "UPDATE fuser SET lastlogin = NOW(), ctloginsfailed = 0
						WHERE fuser_id = :fuser;";
                $pdoStatement = $this->_pdoObj->prepare($sql, array(
                    PDO::ATTR_CURSOR => PDO::CURSOR_FWDONLY
                ));
                $pdoStatement->execute(array(
                    ':fuser' => $row['fuser_id']
                ));
            } else {
                $this->_loggedin = false;
                unset($_SESSION['loggedin']);
                $this->addError('password', 'Das Passwort ist leider nicht richtig. ');
                $sql = "UPDATE fuser SET ctloginsfailed = ctloginsfailed + 1
						WHERE fuser_id = :fuser;";
                $pdoStatement = $this->_pdoObj->prepare($sql, array(
                    PDO::ATTR_CURSOR => PDO::CURSOR_FWDONLY
                ));
                $pdoStatement->execute(array(
                    ':fuser' => $row['fuser_id']
                ));
            }
        } else {
            $this->_loggedin = false;
            unset($_SESSION['loggedin']);
            $this->addError('email', gettext('User unknown: ') . $username . ' ' . $pdoStatement->rowCount());
        }
    }

    /**
     * returns true, if a given email address exists in the database already
     * returns false, if a given email address does not exist yet.
     *
     * @param string $email
     * @return boolean
     */
    private function checkForExistingUser($email)
    {
        $res = true;
        $sql = "SELECT COUNT(*) recs FROM fuser WHERE email = :email;";
        $this->_pdoObj = dbconnection::getInstance();
        $pdoStatement = $this->_pdoObj->prepare($sql, array(
            PDO::ATTR_CURSOR => PDO::CURSOR_FWDONLY
        ));
        $pdoStatement->execute(array(
            ':email' => $email
        ));
        $row = $pdoStatement->fetch();
        if ($row['recs'] == 0) {
            $res = false;
        }
        return $res;
    }

    /**
     * handles login for the login form.
     * checks some inputs.
     */
    private function handleLogin($email, $password)
    {
        $mail = filter_var($email, FILTER_VALIDATE_EMAIL);
        if ($mail === false) {
            $this->addError('email', gettext('Please provide a valid email address.'));
        } else {
            $this->_ps['email'] = $mail;
            $email = $mail;
        }
        
        $password = trim($password);
        if (strlen($password) < 6) {
            $this->addError('password', gettext('Please provide the right password.'));
        }
        
        if (count($this->_err) == 0) {
            $this->loginUser($email, $password);
        } else {
            $this->_loggedin = false;
            unset($_SESSION['loggedin']);
        }
    }

    /**
     * generates and returns a password of given length
     *
     * @param integer $length
     * @return string
     */
    private function genPass($length)
    {
        $chars = 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890';
        $pass = '';
        for ($i = 0; $i < $length; $i ++) {
            $pass .= substr($chars, rand(0, strlen($chars) - 1), 1);
        }
        return $pass;
    }

    /**
     * destroys the session and redirects the user to the start page.
     */
    private function logout()
    {
        session_destroy();
        header('Location: /');
    }

    private function printAccountPage()
    {
        global $glob;
        $ev = new event($this->_lg, $this->_locale);
        $numberOfEvents = $ev->getNumberOfEvents();
        $deleteenabled = false;
        if ($numberOfEvents == 0) {
            $deleteenabled = true;
        }
        $sm = new L5Smarty();
        $sm->assign('deleteenabled', $deleteenabled);
        $sm->assign('glob', $glob);
        $xout = $sm->fetch('portal/account.tpl');
        return $xout;
    }

    private function handleDeleteAccount()
    {
        $userid = $_SESSION['user']['id'];
        if (is_numeric($userid) && $userid > 0) {
            $this->_pdoObj = dbconnection::getInstance();
            $this->_pdoObj->beginTransaction();
            try {
                $sql = "DELETE FROM fuser WHERE fuser_id = :userid;";
                $pdoStatement = $this->_pdoObj->prepare($sql, array(
                    PDO::ATTR_CURSOR => PDO::CURSOR_FWDONLY
                ));
                $pdoStatement->execute(array(
                    ':userid' => $_SESSION['user']['id']
                ));
                
                if ($pdoStatement->errorCode() * 1 != 0) {
                    $this->addError('', 'SQL Fehler', 1, __FILE__ . '/' . __LINE__ . ': ' . print_r($pdoStatement->errorInfo(), true));
                }
                
                if (count($this->_err) == 0) {
                    $this->_pdoObj->commit();
                    session_destroy();
                } else {
                    $this->_pdoObj->rollBack();
                }
            } catch (Exception $e) {
                $this->addError('', gettext('Error deleting account') . ' ' . $_SESSION['user']['id'], 1, __FILE__ . '/' . __LINE__ . ': ' . print_r($e, true));
                $this->_pdoObj->rollBack();
            }
        } else {
            $this->addError('', gettext('Wrong user id given while deleting account') . ' ' . $_SESSION['user']['id'], 1, __FILE__ . '/' . __LINE__);
        }
    }

    /**
     * Log the whole http request to a database table
     * this is for debugging purposes and should not be activated in production.
     */
    private function logRequest()
    {
        try {
            if (LOG_ALL_REQUESTS == true) {
                $sql = "INSERT INTO sessionlog 
					    SET user_id = :userid, 
						session_id = :sid, 
						postcontent = :post, 
						getcontent = :get,
						othercontent = :other;";
                $sqlArr = array(
                    ':userid' => $_SESSION['user']['id'],
                    ':sid' => session_id(),
                    ':post' => print_r($_POST, true),
                    ':get' => print_r($_GET, true),
                    ':other' => file_get_contents('php://input')
                );
                $this->_pdoObj = dbconnection::getInstance();
                $pdoStatement = $this->_pdoObj->prepare($sql, array(
                    PDO::ATTR_CURSOR => PDO::CURSOR_FWDONLY
                ));
                $pdoStatement->execute($sqlArr);
            }
        } catch (Exception $e) {
            $this->addError('', 'general problem', 1, $e->getFile() . '/' . $e->getLine() . ': ' . $e->getMessage());
        }
    }

    private function generateSmtpSecureVals()
    {
        $op = array(
            0 => '',
            '1' => 'ssl',
            2 => 'tls'
        );
        return $op;
    }

    private function generateFontFamilyOptions()
    {
        $op = array();
        $i = 0;
        foreach ($this->_fontfamilies as $ff) {
            $op[$i] = $ff[0];
            $i ++;
        }
        return $op;
    }
}