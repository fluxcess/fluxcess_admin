<?php

class l5errlog
{

    /**
     * contains all errors
     *
     * @var array
     */
    protected $_err = array();

    private static $singletonInstance = null;

    public static function getInstance()
    {
        if (l5errlog::$singletonInstance === null) {
            l5errlog::$singletonInstance = new l5errlog();
        }
        return l5errlog::$singletonInstance;
    }

    /**
     * only singleton creations allowed
     */
    protected function __construct()
    {}

    /**
     * singleton, no clones allowed
     */
    private function __clone()
    {}

    /**
     * singleton, no deserializations allowed
     */
    private function __wakeup()
    {}

    /**
     * returns an array of errors
     */
    public function getErrors()
    {
        return $this->_err;
    }

    /**
     * adds an error to the $_err array
     *
     * @param string $field
     *            // determines the form field name, to which the error refers. Can be left empty for general errors.
     * @param string $note
     *            // the error description, shown to the GUI user
     * @param number $urgency
     *            // urgency:
     *            // 1 = very important 5 = debug
     *            // 1: sends mail to admin
     *            // -1: sends mail to admin, but does not display the error to the user
     */
    public function addError($field, $note, $urgency = 5, $details = null)
    {
        global $glob;
        try {
            $er['field'] = $field;
            $er['note'] = $note;
            if (DEVENV == true) {
                $er['note'] .= ' -- Details: ' . $details;
            }
            if ($urgency > - 1) {
                $this->_err[] = $er;
            }
            if ($urgency == 1) {
                $tracex = debug_backtrace();
                $trace = $tracex[1];
                
                $nl = "\n";
                $msgtext = date('Ymd-His') . ' - fluxline - ' . $note . '<br />';
                $msgtext .= ' File: ' . $trace['file'] . $nl . 'line ' . $trace['line'] . $nl;
                $msgtext .= ' Class: ' . $trace['class'] . ' -> ' . $trace['function'] . $nl;
                // $msgtext .= ' Object: ' . print_r($trace['object'], true) . $nl;
                $msgtext .= ' Args: ' . print_r($trace['args'], true) . $nl;
                $msgtext .= ' Details: ' . print_r($details, true) . $nl;
                $msgtext .= ' Session Data: ' . print_r($_SESSION, true) . $nl;
                $msgtext .= print_r($trace[1], true);
                error_log($msgtext);
                // error_log($msgtext, 3, 'C:\\Apache24_32\\logs\\phperrlog.txt');
                $this->sendEmailToUser('[fluxline ERR] Fehler (' . $glob['ENV_NAME'] . ')', nl2br($msgtext), 'HTML Mail', $glob['adminemail']);
            }
        } catch (Exception $e) {
            error_log('fehler? ' . print_r($e, true));
        }
    }

    /**
     * sends an email with the given content to the specified user.
     *
     * @param string $subject
     * @param string $html
     * @param string $txt
     * @param string $destination
     *            // email address of the recipient
     */
    protected function sendEmailToUser($subject, $html, $txt, $destination)
    {
        global $glob;
        $mail = new PHPMailer(true);
        if (DEVENV === true) {
            $mail->IsSMTP();
            $mail->Host = SMTP_HOST;
            $mail->SMTPAuth = true;
            $mail->Username = SMTP_USER;
            $mail->Password = SMTP_PASS;
            $mail->Port = 25;
        }
        $mail->From = $glob['mailsender'];
        $mail->FromName = $glob['mailsendername'];
        $mail->AddAddress($destination); // Name is optional
        $mail->AddReplyTo($glob['mailsender'], $glob['mailsendername']);
        $mail->AddBCC($glob['mailallbcc']);
        $mail->WordWrap = 50;
        $mail->IsHTML(true);
        $mail->Subject = $subject;
        $mail->Body = $html;
        $mail->AltBody = $txt;
        $mail->CharSet = 'UTF-8';
        if (! $mail->Send()) {
            $this->addError('', gettext('The email to a user could not be sent.' . $mail->ErrorInfo));
        }
    }
}