<?php

class extension extends l5sys
{

    /**
     * This is the constructor
     *
     * @param string $lg
     *            the language
     * @param string $locale
     *            the locale
     */
    function __construct($lg, $locale)
    {
        $this->_locale = $locale;
        $this->_lg = $lg;
    }

    /**
     * returns a list of extensions for the api
     *
     * @return array contains events
     */
    public function apiGetList($entity = null, $params = null)
    {
        $res = null;
        if ($entity == null) {
            $r = $this->getExtensionList();
            $res['extensions'] = $r;
        }
        return $res;
    }

    public function getList()
    {
        return $this->getExtensionList();
    }

    public function getForOptionList()
    {
        $res = array();
        $l = $this->getExtensionList();
        foreach ($l as $lx) {
            $res[$lx['extension_id']] = $lx['fname'] . ' - ' . $lx['title'];
        }
        return $res;
    }

    private function getExtensionList()
    {
        $listOfExtensions = array();
        try {
            $sql = "SELECT * FROM extension WHERE active = 1;";
            $this->_pdoObj = dbconnection::getInstance();
            $pdoStatement = $this->_pdoObj->prepare($sql, array(
                PDO::ATTR_CURSOR => PDO::CURSOR_FWDONLY
            ));
            $pdoStatement->execute(Array());
            if ($pdoStatement->errorCode() * 1 != 0) {
                $this->addError('', $pdoStatement->errorInfo(), 1);
            } else {
                while ($row = $pdoStatement->fetch(PDO::FETCH_ASSOC)) {
                    $listOfExtensions[] = $row;
                }
            }
        } catch (Exception $e) {
            $this->addError('', 'Error reading extension list', 1, $e->getMessage());
        }
        return $listOfExtensions;
    }

    public function getExtensionName($id)
    {
        $extensionName = "";
        try {
            $sql = "SELECT fname FROM extension WHERE extension_id = :id;";
            $this->_pdoObj = dbconnection::getInstance();
            $pdoStatement = $this->_pdoObj->prepare($sql, array(
                PDO::ATTR_CURSOR => PDO::CURSOR_FWDONLY
            ));
            $pdoStatement->execute(Array(
                'id' => $id
            ));
            if ($pdoStatement->errorCode() * 1 != 0) {
                $this->addError('', $pdoStatement->errorInfo(), 1);
            } else {
                $row = $pdoStatement->fetch(PDO::FETCH_ASSOC);
                $extensionName = $row['fname'];
            }
        } catch (Exception $e) {
            $this->addError('', 'Error reading extension name', 1, $e->getMessage());
        }
        return $extensionName;
    }
}