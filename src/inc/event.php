<?php

/**
 * Contains the class *event*
 * 
 * @author Line5
 */

/**
 * Everything about events (congresses, concerts,
 * ...)
 *
 * @author Line5
 */
class event extends l5sys
{

    /**
     * contains the domain name of the booking page
     *
     * @var string
     */
    private $_rsvpDomain = '';

    /**
     * contains events that belong to the user
     *
     * @var unknown
     */
    private $_usersEvents = array();

    /**
     * contains a list of field types
     *
     * @var array
     */
    private $_ft = array(
        0 => array(
            'Spezial'
        ),
        1 => array(
            'Text (max. 1 Zeichen)',
            'varchar',
            1
        ),
        2 => array(
            'Text (max. 10 Zeichen)',
            'varchar',
            10
        ),
        3 => array(
            'Text (max. 50 Zeichen)',
            'varchar',
            50
        ),
        4 => array(
            'Text (max. 100 Zeichen)',
            'varchar',
            100
        ),
        5 => array(
            'Text (max. 250 Zeichen)',
            'varchar',
            250
        ),
        6 => array(
            'Zahl',
            'int',
            11
        ),
        7 => array(
            'Ja/Nein',
            'int',
            1
        ),
        8 => array(
            'Feedback'
        ),
        9 => array(
            'HTML'
        ),
        10 => array(
            'Text (max. 64000 Zeichen)',
            'text',
            - 1
        ),
        11 => array(
            'Radiobuttons'
        )
    );

    private $_fdt = array(
        0 => array(
            'standard'
        ),
        1 => array(
            'Radiobuttons'
        )
    );

    /**
     * contains a list of available font names
     *
     * @var array
     */
    private $_fontfamilies = array(
        0 => array(
            'Arial, Helvetica, sans-serif'
        ),
        1 => array(
            '"Arial Black", Gadget, sans-serif'
        ),
        2 => array(
            '"Comic Sans MS", cursive, sans-serif'
        ),
        3 => array(
            'Impact, Charcoal, sans-serif'
        ),
        4 => array(
            '"Lucida Sans Unicode", "Lucida Grande", sans-serif'
        ),
        5 => array(
            'Tahoma, Geneva, sans-serif'
        ),
        6 => array(
            '"Trebuchet MS", Helvetica, sans-serif'
        ),
        7 => array(
            'Verdana, Geneva, sans-serif'
        ),
        8 => array(
            '"Courier New", Courier, monospace'
        ),
        9 => array(
            '"Lucida Console", Monaco, monospace'
        ),
        10 => array(
            'Georgia, serif'
        ),
        11 => array(
            '"Palatino Linotype", "Book Antiqua", Palatino, serif'
        ),
        12 => array(
            '"Times New Roman", Times, serif'
        )
    );

    private $_eventsettingfields = array(
        'invoicehtmlcode' => 'text',
        'invoicehtmltemplate_id' => 'int',
        'importexternalukey' => 'char',
        'invoicenumberprefix' => 'char',
        'massimportpassword' => 'char',
        'startpageguestlistcolumns' => 'text',
        'invoicemailtemplate' => 'int',
        'acceptmailtemplate' => 'int',
        'documentfilenameprefix' => 'text',
        'autoinvoice' => 'int',
        'autowallet' => 'int',
        'eventregpath' => 'text',
        'eventpubliclylisted' => 'int',
        'eventstarttime' => 'datetime',
        'teaserbanner' => 'text',
        'eventpublicdate' => 'char',
        'shortvenue' => 'char',
        'shorttitle' => 'text',
        'publicshopheadline' => 'text',
        'infolink' => 'text',
        'nextinvoicenumber' => 'int',
        'fileinvoicebackground' => 'text',
        'outgoingmailredirection' => 'int',
        'outgoingmailredirectiontargets' => 'text',
        'smtphost' => 'char',
        'smtppassword' => 'char',
        'smtpport' => 'char',
        'smtpsecure' => 'char',
        'smtpusername' => 'char',
        'walletorg' => 'char',
        'walletserialno' => 'text',
        'walletbackgroundcolor' => 'char',
        'walletforegroundcolor' => 'char',
        'walletlabelcolor' => 'char',
        'walletlogotext' => 'char',
        'walletdescription' => 'text',
        'walletlocations' => 'text',
        'walletrelevantdate' => 'char',
        'walletwebsite' => 'text',
        'walletbacksidetxhl' => 'text',
        'walletbacksidetext' => 'text',
        'walletcode' => 'text',
        'filewalleticon1' => 'text',
        'filewalleticon2' => 'text',
        'filewalleticon3' => 'text',
        'filewalletlogo1' => 'text',
        'filewalletlogo2' => 'text',
        'filewalletlogo3' => 'text',
        'filewalletstrip1' => 'text',
        'filewalletstrip2' => 'text',
        'filewalletstrip3' => 'text',
        'eventinfo1' => 'text',
        'eventinfo2' => 'text',
        'eventinfo3' => 'text',
        'eventinfo4' => 'text',
        'eventinfo5' => 'text',
        'imageheader' => 'text',
        'selfregistrationgenerateaccesscode' => 'int'
    );

    private $_multisettings = array();

    private $_specialsqlcolumns = array(
        'invoicesent' => array(
            'sql' => ' (SELECT IF(COUNT(*)>0,1,0) FROM ##xvl 
						WHERE `inout` = "inv" AND guest_id = g.guest_id) 
						invoicesent '
        ),
        'paymentstatus' => array(
            'sql' => ' (SELECT IF(COUNT(*)>0,0,1) FROM ##xvl i 
								WHERE `inout` = "inv" 
									AND flg01 = 0
									AND guest_id = g.guest_id 
									AND (IFNULL
										((SELECT room FROM ##xvl p
										WHERE parent_id = i.id 
										AND `inout` = "pay"
										ORDER BY ts DESC LIMIT 1), 0)) 
										< 16)
								paymentstatus '
        )
    );

    /**
     * This is the constructor
     *
     * @param string $lg
     *            the language
     * @param string $locale
     *            the locale
     */
    function __construct($lg, $locale)
    {
        if (BOOKING_DOMAINS != false) {
            $this->_rsvpDomain = BOOKING_DOMAINS[0];
        }
        
        $this->_multisettings = multisettings::getMultiSettingTypes();
        $this->_locale = $locale;
        $this->_lg = $lg;
    }

    /**
     * This is the print function
     * very nice.
     *
     * @param array $gt
     *            the GET parameters
     * @param array $ps
     *            the POST parameters
     * @param array $err
     *            an array of errors
     * @param boolean $proceed
     *            really clear...
     * @return Ambigous <string, void, string>
     */
    public function printPage(&$gt, &$ps, &$err, &$proceed)
    {
        $this->_gt = $gt;
        $this->_ps = $ps;
        if (! isset($ps)) {
            // ToDo: Corbe Interface! $arr = json_decode(file_get_contents('php://input'), true);
        }
        $this->_err = $err;
        
        if (isset($_POST['form']) && $_POST['class'] == 'event') {
            switch ($_POST['form']) {
                case 'create':
                    $xout = $this->handleCreate();
                    break;
                case 'change':
                    $xout = $this->handleSave();
                    break;
                case 'changeFieldForm':
                    $xout .= $this->saveFieldData();
                    break;
                case 'checkoutallguests':
                    $xout = $this->handleCheckOutAllGuests();
                    break;
                case 'deleteField':
                    $this->handleDeleteField();
                    $proceed = false;
                    break;
                case 'deleteEvent':
                    $this->handleDeleteEvent();
                    $proceed = false;
                    break;
                case 'editFieldForm':
                    $xout .= $this->printChangeForm();
                    break;
                case 'generateaccesscodes':
                    $xout = $this->handleGenerateAccessCodes();
                    break;
                case 'importguestlist':
                    $xout = $this->importGuestlist();
                    break;
                case 'createEventFromTemplateImport':
                    $xout = $this->importEventSettings();
                    if (count($this->_err) == 0) {
                        header('Location: /');
                    }
                    break;
                case 'moveField':
                    $this->handleMoveField();
                    $proceed = false;
                    break;
                case 'sendMassMail':
                    $xout = $this->handleMassMailSending();
                    break;
                case 'toggleFieldExport':
                    $this->handleToggleFieldExport();
                    $proceed = false;
                    break;
                case 'updateFileName':
                    $this->updateFileName($this->_ps['filename']);
                    $proceed = false;
                    break;
                case 'uploadImageHeader':
                    $this->handleUpload('imageHeader');
                    $proceed = false;
                    die();
                    break;
            }
        } else {
            if (is_numeric($this->_gt[1]) || is_numeric($this->_gt['fevent_id'])) {
                if (is_numeric($this->_gt[1])) {
                    $eventid = $this->_gt[1];
                } else {
                    $eventid = $this->_gt['fevent_id'];
                }
                $_SESSION['event']['id'] = $eventid;
                if ($this->checkUserEvent($eventid) === true) {
                    if (isset($this->_gt['type']) && $this->_gt['type'] == 'download') {
                        switch ($this->_gt['dl']) {
                            case 'newfeedback':
                                $xout = $this->exportGuestlistNewFeedback($eventid);
                                break;
                            case 'newfeedbackxlsx':
                                $xout = $this->exportGuestlistNewFeedbackXLSX($eventid);
                                break;
                            case 'liveexport':
                                $xout = $this->exportGuestlistWithLiveStatus($eventid);
                                break;
                            case 'liveexportxlsx':
                                $xout = $this->exportGuestlistWithLiveStatusXLSX($eventid);
                                break;
                            case 'washereexport':
                                $xout = $this->exportGuestlistWithWasHereStatus($eventid);
                                break;
                            case 'washereexportxlsx':
                                $xout = $this->exportGuestlistWithWasHereStatusXLSX($eventid);
                                break;
                            case 'visitorlogexportxlsx':
                                $xout = $this->exportVisitorLogXLSX($eventid);
                                break;
                            case 'guestlistxlsx':
                                $xout = $this->exportGuestlistXLSX($eventid);
                                break;
                            case 'conferencesessionsxlsx':
                                $xout = $this->exportConferenceSessionsXLSX($eventid);
                                break;
                            case 'settingsxml':
                                $xout = $this->exportSettingsXML($eventid);
                                break;
                            case 'invoicespayments':
                                $xout = $this->exportInvoicesPayments($eventid);
                                break;
                            default:
                                $xout = $this->exportGuestlist($eventid);
                                break;
                        }
                    } elseif (isset($this->_gt['submodule']) && $this->_gt['submodule'] == 'printFieldListTable') {
                        $xout = $this->printFieldListTable($this->_gt['fevent_id'], $this->_gt['ordertype']);
                    } else {
                        $xout = $this->printEventPage($eventid);
                    }
                } else {
                    $xout .= 'No access to this event id (' . $eventid . ')';
                }
            } else {
                if ($this->_gt['type'] == 'iframe') {
                    $sw = $this->_gt['submodule'];
                } else {
                    $sw = $this->_gt[1];
                }
                switch ($sw) {
                    case 'listeJson':
                        $proceed = false;
                        $xout = $this->getEventList();
                        break;
                    case 'detailsJson':
                        $proceed = false;
                        $xout = $this->printEventDetailsJSON($this->_gt['fevent_id']);
                        break;
                    default:
                        $sm = new L5Smarty();
                        $xout = $sm->fetch('event/overview.tpl');
                }
            }
        }
        // pass back data to calling class
        $err = $this->_err;
        return $xout;
    }

    /**
     * returns the html code for a ticket
     * public function: checks if user is permitted to access the given event
     *
     * @param array $gt
     *            contains $_GET values
     * @param array $ps
     *            contains $_POST values
     * @param array $err
     *            contains error messages
     * @param boolean $proceed
     *            tells, if should proceed or not
     * @return string <string, mixed> contains html code
     */
    public function printTicket(&$gt, &$ps, &$err, &$proceed)
    {
        $this->_gt = $gt;
        $this->_ps = $ps;
        $this->_err = $err;
        
        if (is_numeric($this->_gt[1]) || is_numeric($this->_gt['fevent_id'])) {
            if (is_numeric($this->_gt[1])) {
                $eventid = $this->_gt[1];
            } else {
                $eventid = $this->_gt['fevent_id'];
            }
            $guestid = $this->_gt[3];
            if ($this->checkUserEvent($eventid) === true) {
                /**
                 * ToDo: log ticket prints!
                 * (This is done in a subfunction!)
                 *
                 * @var unknown
                 */
                $xout = $this->printTicketX($eventid, $guestid);
            } else {
                $xout .= gettext('No access to this event id') . ' (' . $eventid . ')';
            }
        }
        // pass back data to calling class
        $err = $this->_err;
        return $xout;
    }

    /**
     * returns the html code for a ticket
     * public function: checks if user is permitted to access the given event
     *
     * @param array $eventid
     *            contains the current event id (must be checked!!!)
     * @param array $guestid
     *            contains the current guest id
     * @return string <string, mixed> contains html code
     */
    public function printTicketX($eventid, $guestid)
    {
        if ($this->checkUserEvent($eventid) === true) {
            $ev = $this->readRecord($eventid);
            $myGuest = new entityGuest($eventid, $guestid, $ev);
            if (! isset($ev['ticketprinthtml']) || strlen($ev['ticketprinthtml']) == 0) {
                $xhtml = '<p class="title">[ev.title]</p><p class="namefield">[firstname] [lastname]</p><p class="qr">[#qr_ticket_open]</p>';
            } else {
                $xhtml = $ev['ticketprinthtml'];
            }
            $xout = $myGuest->createCustomizedText($xhtml, false);
            $myGuest->logGuestAction('', 'prt', '', $xout);
            if (LIVEENV === true) {
                $xout = str_replace('https:', 'http:', $xout);
            }
        } else {
            $xout .= 'No access to this event id (' . $eventid . ')';
        }
        
        return $xout;
    }

    /**
     * writes pdf for ticket / badge to print queue directories
     * public function: checks if user is permitted to access the given event
     *
     * @param array $gt
     * @param array $ps
     * @param array $err
     * @param string $proceed
     *            has no function
     * @param string $printer
     */
    public function printTicketPDF(&$gt, &$ps, &$err, &$proceed, $printer = null)
    {
        $this->_gt = $gt;
        $this->_ps = $ps;
        $this->_err = $err;
        
        if (is_numeric($this->_gt[1]) || is_numeric($this->_gt['fevent_id'])) {
            if (is_numeric($this->_gt[1])) {
                $eventid = $this->_gt[1];
            } else {
                $eventid = $this->_gt['fevent_id'];
            }
            if ($this->checkUserEvent($eventid) === true) {
                $ev = $this->readRecord($eventid);
                $guestid = $this->_gt[3];
                $myGuest = new entityGuest($eventid, $guestid, $ev);
                if (! isset($ev['ticketprinthtml']) || strlen($ev['ticketprinthtml']) == 0) {
                    $xhtml = '<p class="title">[ev.title]</p><p class="namefield">[firstname] [lastname]</p><p class="qr">[#qr_ticket_open]</p>';
                } else {
                    $xhtml = $ev['ticketprinthtml'] . ' - ' . $eventid . ' / ' . $guestid;
                }
                $xout = $myGuest->createCustomizedText($xhtml, false);
                
                require_once (BASEDIR . 'lib/tcpdf/tcpdf.php');
                
                // create new PDF document
                $pdf = new TCPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);
                
                // set document information
                $pdf->SetCreator('fluxcess');
                $pdf->SetAuthor('fluxcess GmbH');
                $pdf->SetTitle('fluxcess Ticket');
                $pdf->SetSubject('Ticketing');
                $pdf->SetKeywords('Ticket');
                
                // set default header data
                // $pdf->SetHeaderData(PDF_HEADER_LOGO, PDF_HEADER_LOGO_WIDTH, PDF_HEADER_TITLE.' 006', PDF_HEADER_STRING);
                
                // set header and footer fonts
                // $pdf->setHeaderFont(Array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));
                // $pdf->setFooterFont(Array(PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA));
                $pdf->SetPrintHeader(false);
                $pdf->SetPrintFooter(false);
                
                // set default monospaced font
                $pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);
                
                // set margins
                $pdf->SetMargins(0, 0, 0, 0);
                // $pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
                // $pdf->SetFooterMargin(PDF_MARGIN_FOOTER);
                
                // set auto page breaks
                // $pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);
                $pdf->SetAutoPageBreak(FALSE);
                
                // set image scale factor
                $pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);
                // set some language-dependent strings (optional)
                if (@file_exists(dirname(__FILE__) . '/lang/eng.php')) {
                    require_once (dirname(__FILE__) . '/lang/eng.php');
                    $pdf->setLanguageArray($l);
                }
                // ---------------------------------------------------------
                
                // set font
                // $pdf->SetFont('dejavusans', '', 10);
                // add a page
                $pdf->AddPage();
                // writeHTML($html, $ln=true, $fill=false, $reseth=false, $cell=false, $align='')
                $gd = $myGuest->getData();
                // ingram micro
                if (1 == 0) {
                    $pdf->writeHTMLCell(0, 0, 3, 17, $html = '<strong style="font-size: 22px;">' . $gd['firstname'] . ' ' . $gd['lastname'] . '</strong>', $border = 0, $ln = 0, $fill = 0, $reseth = true, $align = '', $autopadding = true);
                    $pdf->writeHTMLCell(0, 0, 3, 25, $html = '<span style="font-size: 20px;">' . $gd['company'] . '</span>', $border = 0, $ln = 0, $fill = 0, $reseth = true, $align = '', $autopadding = true);
                }
                if (LIVEENV === true) {
                    if (1 == 0) {
                        // ingram micro
                        $pdf->writeHTMLCell(0, 0, 64, 34, $html = '<img width="60" src="http://qr/png.php?content=https://im.dc.gs/qy/' . $gd['guest_id'] . '&/x.png" />', $border = 0, $ln = 0, $fill = 0, $reseth = true, $align = '', $autopadding = true);
                    } else {
                        // line5
                        $pdf->writeHTMLCell(0, 0, 3, 3, $html = '<img width="40" src="http://qr/png.php?content=http://boe2015.fluxline.org/qy/' . $gd['guest_id'] . '&/x.png" />', $border = 0, $ln = 0, $fill = 0, $reseth = true, $align = '', $autopadding = true);
                    }
                } else {
                    $pdf->writeHTMLCell(0, 0, 3, 3, $html = '<img width="80" src="https://qr.line5.net/png.php?content=https://im.dc.gs/qy/' . $gd['guest_id'] . '&/x.png" />', $border = 0, $ln = 0, $fill = 0, $reseth = true, $align = '', $autopadding = true);
                }
                if (1 == 1) {
                    // line5
                    $pdf->writeHTMLCell(0, 0, 3, 20, $html = '<strong style="font-size: 22px;">' . $gd['firstname'] . ' ' . $gd['lastname'] . '</strong>', $border = 0, $ln = 0, $fill = 0, $reseth = true, $align = '', $autopadding = true);
                    $pdf->writeHTMLCell(0, 0, 3, 28, $html = '<span style="font-size: 20px;">' . $gd['company'] . '</span>', $border = 0, $ln = 0, $fill = 0, $reseth = true, $align = '', $autopadding = true);
                }
                // $pdf->writeHTML($xout, true, false, true, false, '');
                try {
                    // output to print queue directories
                    if (getenv('REMOTE_ADDR') == '10.115.51.51' || $printer == 1) {
                        $pdf->Output(BASEDIR . 'tmp/ticket1/ticket_' . date('YmdHis') . '-' . rand(10000, 99999) . '.pdf', 'F');
                    } elseif (getenv('REMOTE_ADDR') == '10.115.51.52' || $printer == 3) {
                        $pdf->Output(BASEDIR . 'tmp/ticket2/ticket_' . date('YmdHis') . '-' . rand(10000, 99999) . '.pdf', 'F');
                    } else {
                        $pdf->Output(BASEDIR . 'tmp/ticket3/ticket_' . date('YmdHis') . '-' . rand(10000, 99999) . '.pdf', 'F');
                    }
                } catch (Exception $e) {
                    print_r($e->getMessage());
                }
            } else {
                $xout .= 'No access to this event id (' . $eventid . ')';
            }
        }
        // pass back data to calling class
        $err = $this->_err;
        // return $xout;
    }

    private function printEventPage($eventid)
    {
        $ev = $this->readRecord($eventid);
        $stat = $this->getStats($eventid);
        $sm = new L5Smarty();
        $sm->assign('ev', $ev);
        $sm->assign('stat', $stat);
        $sm->assign('fevent_id', $eventid);
        if (isset($this->_gt['type']) && $this->_gt['type'] == 'iframe') {
            switch ($this->_gt['submodule']) {
                case 'importfile':
                    $sm->assign('err', $err);
                    $fn = 'event/iframe.importguestlist.tpl';
                    break;
            }
            $xout = $sm->fetch($fn);
        } else {
            if (! isset($this->_ps['massmailcontent'])) {
                $this->_ps['massmailcontent'] = gettext('Dear [salutation] [lastname], ...');
            }
            if (! isset($this->_ps['massmailrecipients'])) {
                $this->_ps['massmailrecipients'] = array();
            }
            if (! isset($this->_ps['testmailrecipient1'])) {
                $this->_ps['testmailrecipient1'] = $ev['mailsender'];
            }
            if (! isset($this->_ps['testmailrecipient3'])) {
                $this->_ps['testmailrecipient3'] = $ev['mailsender'];
            }
            
            $dm = new domain($this->_lg, $this->_locale);
            $ex = new extension($this->_lg, $this->_locale);
            $rps = new regprocstep($this->_lg, $this->_locale);
            
            $sm->assign('domains', $dm->getDomains($eventid));
            $sm->assign('fieldsExported', $this->getFields($eventid, 'Exported'));
            $sm->assign('fieldsNotexported', $this->getFields($eventid, 'Notexported'));
            $sm->assign('fieldsInform', $this->getFields($eventid, 'Inform'));
            $sm->assign('fieldsNotinform', $this->getFields($eventid, 'Notinform'));
            $gu = new guest();
            $customFields = $gu->generateCustomFields($eventid, array());
            $sm->assign('customFields', $customFields);
            $sm->assign('rsvphistoryrecords', $this->getRSVPHistory($eventid));
            $sm->assign('ffamval', $this->generateFontFamilyOptions());
            $sm->assign('ffamout', $this->generateFontFamilyOptions());
            $sm->assign('smtpsecureval', $this->generateSmtpSecureVals());
            $sm->assign('smtpsecureout', $this->generateSmtpSecureVals());
            $sm->assign('massmailcount', $this->countMassMailRecipients($eventid));
            $sm->assign('emaillist', $this->readEmails($eventid, 0));
            $sm->assign('availableextensions', $ex->getForOptionList());
            $sm->assign('regprocsteps', $rps->getStepListForEvent($eventid));
            $sm->assign('invoicehtmltemplate_ids', $this->generateEmailTemplateOptions());
            $sm->assign('LIVEENV', LIVEENV);
            $sm->assign('ps', $this->_ps);
            $xout = $sm->fetch('event/eventpage.tpl');
        }
        return $xout;
    }

    private function importXLSXToArray($filename, &$xout)
    {
        require_once (BASEDIR . 'lib/vendor/autoload.php');
        $spreadsheet = new \PhpOffice\PhpSpreadsheet\Spreadsheet();
        
        $spreadsheet = \PhpOffice\PhpSpreadsheet\IOFactory::load($filename);
        $data = array();
        
        // read data from excel sheet
        $worksheet = $spreadsheet->getActiveSheet();
        $data = $worksheet->toArray();
        $rowCounter = 0;
        $colCounter = 0;
        foreach ($data as $rowNo => $rowdata) {
            $colCounter = 0;
            foreach ($rowdata as $col) {
                $rows[$rowCounter][$colCounter] = $col;
                $colCounter += 1;
            }
            $rowCounter += 1;
        }
        return $rows;
    }

    private function importCSVToArray($filename, &$xout)
    {
        $rows = array();
        $handle = event::fopen_utf8($filename, "r");
        $contents = fread($handle, filesize($filename));
        $xout .= gettext('Importing') . ' ' . $_FILES['importfile']['name'] . ': Read ' . filesize($filename) . ' Bytes<br />';
        fclose($handle);
        if (mb_detect_encoding($contents) != 'UTF-8') {
            $err[] = gettext('The file is not UTF-8 encoded.');
        }
        // $contents = str_replace("\r\n", "", $contents);
        $unformattedRows = explode("\r\n", $contents);
        foreach ($unformattedRows as $r) {
            $cols = explode("\t", $r);
            $rows[] = $cols;
        }
        return $rows;
    }

    private function importEventSettings()
    {
        $this->_ps = $_POST;
        $rows = array();
        $filename = $_FILES['settingsfile']['tmp_name'];
        $handle = event::fopen_utf8($filename, "r");
        $contents = fread($handle, filesize($filename));
        
        // get list of multisettings
        $multis = new multisettings();
        $multistypes = $multis->getMultiSettingTypes();
        
        $sxml = simplexml_load_file($filename);
        foreach ($sxml->settings->children() as $setid => $setval) {
            if (! isset($multistypes[$setid])) {
                $eventSettings['setting'][$setid] = $setval->__toString();
            }
        }
        foreach ($sxml->multisettings->children() as $multisettingtype => $myMultisettings) {
            $ms = array();
            foreach ($myMultisettings->children() as $s) {
                $msq = array();
                foreach ($s->children() as $sxKey => $sxValue) {
                    if (substr($sxKey, 0, 2) == 'n1') {
                        $n1s = array();
                        foreach ($sxValue->children() as $sxsxKey => $sxsxValue) {
                            $n1 = array();
                            $n1['skey'] = $sxsxValue['skey']->__toString();
                            $n1['skey_multiseq'] = $sxsxValue['skey_multiseq']->__toString();
                            $n1['skey_multiid'] = $sxsxValue['skey_multiid']->__toString();
                            $n1['pkey_multiname'] = $multisettingtype;
                            $n1['pkey_multiid'] = $s['id']->__toString();
                            $n1['sval'] = $sxsxValue->__toString();
                            $n1s[] = $n1;
                        }
                        $msq[$sxKey] = $n1s;
                    } else {
                        $msq[$sxKey] = $sxValue->__toString();
                    }
                }
                $msq['seq'] = $s['seq']->__toString();
                $ms[] = $msq;
            }
            $eventSettings['multisettings'][$multisettingtype] = $ms;
        }
        foreach ($sxml->fields->children() as $fieldid => $setval) {
            $v = null;
            foreach ($setval->children() as $childname => $childval) {
                $v[$childname] = $childval->__toString();
            }
            $eventSettings['fields'][] = $v;
        }
        if (count($this->_err) > 0) {
            print_r($this->_err);
        }
        $this->handleCreate($eventSettings);
    }

    private function importGuestlist()
    {
        $eventid = $this->_ps['fevent_id'];
        $importErrors = array();
        if (is_numeric($eventid) && $this->checkUserEvent($eventid)) {
            
            if ($this->_ps['clearbefore'] == 1) {
                try {
                    // $xout .= count($sp).' Spalten: '.$d['salutation'].' '.$d['firstname']. ' '.$d['lastname'].' '.$d['company'].'<br />';
                    $sql = "TRUNCATE TABLE zguest" . $eventid . ";";
                    $this->_pdoObj = dbconnection::getInstance();
                    $pdoStatement = $this->_pdoObj->prepare($sql, array(
                        PDO::ATTR_CURSOR => PDO::CURSOR_FWDONLY
                    ));
                    $pdoStatement->execute(Array());
                    if ($pdoStatement->errorCode() * 1 != 0) {
                        $importErrors[] = 'SQL Fehler' . print_r($pdoStatement->errorInfo(), true);
                    } else {
                        $xout .= gettext('Cleared guestlist.') . '<br />';
                    }
                } catch (Exception $e) {
                    $importErrors[] = gettext('Database error') . ': ' . print_r($e->getMessage(), true);
                }
            }
            
            $importStructure = $this->getImportStructure($eventid);
            
            // check if guest ids are provided within export or if they need to be generated for each
            // imported guest
            $generateGuestIDs = true;
            foreach ($importStructure as $fld) {
                if ($fld['mysqlfield'] == 'guest_id') {
                    $generateGuestIDs = false;
                }
            }
            $xout .= gettext("Guest IDs are not automatically generated, because the column is available in the imported file.") . "<br />";
            if ($generateGuestIDs == true) {
                $g = new guest();
            }
            
            // global $importStructure;
            $ds = 0;
            $errn = 0;
            $xleer = 0;
            $duplicate = 0;
            $imported = 0;
            // move_uploaded_file($_FILES["file"]["tmp_name"], "import/testliste.txt");
            $filename = $_FILES["importfile"]["tmp_name"];
            if (strtolower(substr($_FILES['importfile']['name'], - 4)) == 'xlsx') {
                $table = $this->importXLSXToArray($filename, $xout);
            } else {
                $table = $this->importCSVToArray($filename, $xout);
            }
            $counter = 0;
            
            foreach ($table as $sp) {
                if ($counter > 0) {
                    foreach ($importStructure as $fld) {
                        $d[$fld['mysqlfield']] = str_replace("'", "", $sp[$fld['xlscol'] - 1]);
                        $d[$fld['mysqlfield']] = str_replace('"', '', $d[$fld['mysqlfield']]);
                    }
                    $insertThisRecord = true;
                    if ($generateGuestIDs === false) {
                        if ($this->isGuestAvailable($d['guest_id']) == 1) {
                            $insertThisRecord = false;
                        }
                    }
                    if ($insertThisRecord === false) {
                        $duplicate += 1;
                    } else {
                        if ((strlen($d['lastname']) > 0 || strlen($d['firstname']) > 0 || strlen($d['company']) > 0)) {
                            try {
                                // $xout .= count($sp).' Spalten: '.$d['salutation'].' '.$d['firstname']. ' '.$d['lastname'].' '.$d['company'].'<br />';
                                $sql = "INSERT INTO zguest" . $eventid . " SET fevent_id = :eventid, ";
                                $sqlArr[':eventid'] = $eventid;
                                
                                // eventually generate row id, if none is provided.
                                if ($generateGuestIDs == true) {
                                    $sql .= " guest_id = :guestid, ";
                                    $sqlArr[':guestid'] = $g->generateUniqueGuestId($eventid);
                                }
                                // end of generating row id
                                
                                foreach ($importStructure as $fld) {
                                    $sql .= " " . $fld['mysqlfield'] . " = :" . $fld['mysqlfield'] . ", ";
                                    $sqlArr[':' . $fld['mysqlfield']] = $d[$fld['mysqlfield']];
                                }
                                
                                $sql = substr($sql, 0, - 2) . ";";
                                $this->_pdoObj = dbconnection::getInstance();
                                $pdoStatement = $this->_pdoObj->prepare($sql, array(
                                    PDO::ATTR_CURSOR => PDO::CURSOR_FWDONLY
                                ));
                                $pdoStatement->execute($sqlArr);
                                if ($pdoStatement->errorCode() * 1 != 0) {
                                    $importErrors[] = 'SQL Fehler' . print_r($pdoStatement->errorInfo(), true);
                                } elseif ($pdoStatement->rowCount() == 0) {
                                    $importErrors[] = __LINE__ . ' SQL Fehler mit Errorcode 0: ' . print_r($pdoStatement->errorInfo(), true);
                                } else {
                                    $guestid = $this->_pdoObj->lastInsertId();
                                    $imported += 1;
                                }
                            } catch (Exception $e) {
                                $importErrors[] = 'Datenbank-Fehler' . print_r($e->getMessage(), true);
                            }
                        } else {
                            $xleer ++;
                        }
                    }
                    $ds ++;
                } else {
                    // $xout .= 'Kopfzeile übersprungen.<br />';
                }
                $counter += 1;
            }
            $xout .= gettext('Finished.') . '<br />' . $ds . ' ' . gettext('records found in your file') . '<br />' . $xleer . ' ' . gettext('empty records ignored') . '<br />';
            $xout .= $duplicate . ' ' . gettext('duplicate records ignored') . '<br />';
            $xout .= '<strong>' . $imported . ' ' . gettext('new records imported') . '</strong><br />';
            if ($errn > 0) {
                $xout .= '<div class="error">' . $errn . ' Error<br />' . gettext('Last error:') . ' ' . $xerror . '</div>';
            }
            
            $sm = new L5Smarty();
            if (count($this->_err) == 0) {
                $sm->assign('importsuccessful', true);
            }
            $sm->assign('fevent_id', $eventid);
            $xout .= $sm->fetch('event/iframe.importguestlist.tpl');
        } else {
            $xout = 'Wrong event id.';
        }
        
        if (count($importErrors) > 0) {
            $this->addError('', gettext('Import Errors'), 1, print_r($importErrors, true) . ' SQL: ' . $sql . ' PDO Array' . print_r($sqlArr, true));
        }
        return $xout;
    }

    private function isGuestAvailable($guest_id)
    {
        $result = - 1;
        try {
            $sql = "SELECT * FROM zguest" . $_SESSION['fevent_id'] . " g WHERE guest_id = :guestid;";
            $sqlArr = array(
                ':guestid' => $guest_id
            );
            $this->_pdoObj = dbconnection::getInstance();
            $pdoStatement = $this->_pdoObj->prepare($sql, array(
                PDO::ATTR_CURSOR => PDO::CURSOR_FWDONLY
            ));
            $pdoStatement->execute($sqlArr);
            if ($pdoStatement->errorCode() * 1 != 0) {
                $importErrors[] = 'SQL Fehler' . print_r($pdoStatement->errorInfo(), true);
            } elseif ($pdoStatement->rowCount() == 0) {
                $importErrors[] = __LINE__ . ' SQL Fehler mit Errorcode 0: ' . print_r($pdoStatement->errorInfo(), true);
            } else {
                if ($pdoStatement->rowCount() > 0) {
                    $result = 1;
                } else {
                    $result = 0;
                }
            }
        } catch (Exception $e) {
            $this->addError('', gettext('Error checking if guest id exists.'), 1, $e->getFile . '/' . $e->getLine() . ': ' . $e->getMessage(), true);
        }
        return $result;
    }

    /**
     * returns an array with the data of the record with the given id
     *
     * @param int $id
     *            contains the event id
     * @param boolean $assoc
     *            determines if only assoc array should be returned
     * @return null|array contains data of the event record
     */
    public function readRecord($id, $assoc = false)
    {
        try {
            $row = null;
            $sql = "SELECT * FROM fevent WHERE fevent_id = :id 
					AND (fuser_id = :userid OR fcustomer_id = :customerid);";
            $this->_pdoObj = dbconnection::getInstance();
            $pdoStatement = $this->_pdoObj->prepare($sql, array(
                PDO::ATTR_CURSOR => PDO::CURSOR_FWDONLY
            ));
            $pdoStatement->execute(array(
                ':id' => $id,
                ':userid' => $_SESSION['user']['id'],
                ':customerid' => $_SESSION['customer']['customer_id']
            ));
            if ($pdoStatement->errorCode() * 1 != 0) {
                $this->addError('', gettext('SQL Error') . ': ' . print_r($pdoStatement->errorInfo(), true), 1);
            } else {
                if ($assoc === true) {
                    $row = $pdoStatement->fetch(PDO::FETCH_ASSOC);
                } else {
                    $row = $pdoStatement->fetch();
                }
            }
            
            $sql = "SELECT * FROM fevset_" . $id . ";";
            $pdoStatement = $this->_pdoObj->prepare($sql, array(
                PDO::ATTR_CURSOR => PDO::CURSOR_FWDONLY
            ));
            $pdoStatement->execute(array(
                ':id' => $id,
                ':userid' => $_SESSION['user']['id']
            ));
            $multisettingsSubtypes = multisettings::getMultiSettingSubTypes();
            
            if ($pdoStatement->errorCode() * 1 != 0) {
                $this->addError('', gettext('SQL Error') . ': ' . print_r($pdoStatement->errorInfo(), true), 1);
            } else {
                while ($row2 = $pdoStatement->fetch(PDO::FETCH_ASSOC)) {
                    $part1 = explode('_', $row2['skey']);
                    if (isset($this->_multisettings[$part1[0]])) {
                        foreach ($this->_multisettings[$part1[0]]['fields'] as $f) {
                            if ($f['name'] == $part1[2]) {
                                $switcher = $f['type'];
                            }
                        }
                    } else {
                        if (array_search($part1[0], $multisettingsSubtypes) !== false) {
                            // for subtypes, it is always an alphanumeric key
                            $switcher = 'char';
                        } else {
                            $switcher = $this->_eventsettingfields[$row2['skey']];
                        }
                    }
                    switch ($switcher) {
                        case 'text':
                            $row[$row2['skey']] = $row2['sval_txt'];
                            break;
                        case 'int':
                            $row[$row2['skey']] = $row2['sval_int'];
                            break;
                        case 'char':
                            $row[$row2['skey']] = $row2['sval_vch'];
                            break;
                        case 'datetime':
                            $row[$row2['skey']] = $row2['sval_dtt'];
                            break;
                        case 'decimal':
                            $row[$row2['skey']] = $row2['sval_dec'];
                            break;
                    }
                }
            }
            $exports = $this->generateExportLinks($id);
            $stats = $this->getStats($id, $row);
            $massmailcount = $this->getMassMailRecipientCount($id);
            foreach ($this->_multisettings as $mskey => $msval) {
                // echo $mskey . ' | ';
                $row[$mskey] = $this->readMultiSetting($mskey, $row);
                $row[$mskey . '_' . columns] = json_encode($this->_multisettings[$mskey]);
            }
            $optionsets = array(
                'optionsets' => $this->getOptionSets($row['optioncollection'], $row['optioncollectionvalue'])
            );
            $result = array_merge($row, $massmailcount, $stats, $exports, $optionsets);
        } catch (Exception $e) {
            $this->addError('', gettext('SQL Error'), 1, __FILE__ . '/' . __LINE__ . ': ' . print_r($e, true));
        }
        return $result;
    }

    private function getOptionSets($optioncollections, $optioncollectionvalues)
    {
        $o = array();
        foreach ($optioncollectionvalues as $ocv) {
            $o[$ocv['optioncollectionid']][$ocv['val']] = $ocv['title'];
        }
        return $o;
    }

    private function generateExportLinks($eventid)
    {
        $exports = array(
            0 => array(
                1,
                gettext('EXPTITLE_GUESTLIST'),
                gettext('EXPDESC_GUESTLIST'),
                array(
                    'csv' => '/index.php?type=download&module=event&submodule=guestlist&fevent_id=' . $eventid . '&/download.csv',
                    'xlsx' => '/index.php?type=download&module=event&submodule=guestlist&fevent_id=' . $eventid . '&dltype=xlsx&dl=guestlistxlsx&/download.xlsx'
                )
            ),
            1 => array(
                2,
                gettext('EXPTITLE_NEWS'),
                gettext('EXPDESC_NEWS'),
                array(
                    'csv' => '/index.php?type=download&module=event&submodule=guestlist&fevent_id=' . $eventid . '&dl=newfeedback&/download.csv',
                    'xlsx' => '/index.php?type=download&module=event&submodule=guestlist&fevent_id=' . $feventid . '&dltype=xlsx&dl=newfeedbackxlsx&/download.xlsx'
                )
            ),
            2 => array(
                3,
                gettext('EXPTITLE_LIVE'),
                gettext('EXPDESC_LIVE'),
                array(
                    'csv' => '/index.php?type=download&module=event&submodule=guestlist&fevent_id=' . $eventid . '&dl=liveexport&/download.csv',
                    'xlsx' => '/index.php?type=download&module=event&submodule=guestlist&fevent_id=' . $eventid . '&dltype=xlsx&dl=liveexportxlsx&/download.xlsx'
                )
            ),
            3 => array(
                4,
                gettext('EXPTITLE_VLOG'),
                gettext('EXPDESC_VLOG'),
                array(
                    'xlsx' => '/index.php?type=download&module=event&submodule=guestlist&fevent_id=' . $eventid . '&dltype=xlsx&dl=visitorlogexportxlsx&/download.xlsx'
                )
            ),
            4 => array(
                5,
                gettext('EXPTITLE_CHECKEDIN'),
                gettext('EXPDESC_CHECKEDIN'),
                array(
                    'csv' => '/index.php?type=download&module=event&submodule=guestlist&fevent_id=' . $eventid . '&dltype=csv&dl=washereexport&/download.csv',
                    'xlsx' => '/index.php?type=download&module=event&submodule=guestlist&fevent_id=' . $eventid . '&dltype=xlsx&dl=washereexportxlsx&/download.xlsx'
                )
            ),
            5 => array(
                6,
                gettext('EXPTITLE_CONFERENCESESSIONS'),
                gettext('EXPDESC_CONFERENCESESSIONS'),
                array(
                    'xlsx' => '/index.php?type=download&module=event&submodule=guestlist&fevent_id=' . $eventid . '&dltype=xlsx&dl=conferencesessionsxlsx&/download.xlsx'
                )
            ),
            6 => array(
                7,
                gettext('EXPTITLE_EVSETTINGS'),
                gettext('EXPDESC_EVSETTINGS'),
                array(
                    'settings' => '/index.php?type=download&module=event&submodule=guestlist&fevent_id=' . $eventid . '&dltype=xlsx&dl=settingsxml&/event_settings.xml'
                )
            ),
            7 => array(
                8,
                gettext('EXPTITLE_INVOICESPAYMENTS'),
                gettext('EXPDESC_INVOICESPAYMENTS'),
                array(
                    'xlsx' => '/index.php?type=download&module=event&submodule=guestlist&fevent_id=' . $eventid . '&dltype=xlsx&dl=invoicespayments&/invoices_payments.xlsx'
                )
            )
        );
        return array(
            'exports' => $exports
        );
    }

    private function getStats($eventid, $ev = null)
    {
        $row = array();
        try {
            if (is_numeric($eventid) && $this->checkUserEvent($eventid)) {
                if ($ev == null) {
                    $ev = $this->readRecord($eventid);
                }
                if ($ev['selfregistrationallowed'] == 0) {
                    $sql = "SELECT
					IFNULL(SUM(invited),0) invited,
					IFNULL(SUM(registered),0) registered,
					IFNULL(SUM(cancelled),0) cancelled
					FROM `zguest$eventid`
					WHERE archived = 0 AND deleted = 0
					;";
                } else {
                    $sql = "SELECT
					IFNULL(SUM(invited),0) invited,
					IFNULL(SUM(registered),0) registered,
					IFNULL(SUM(cancelled),0) cancelled
					FROM `zguest$eventid`
					WHERE archived = 0 AND deleted = 0 AND invited > 0
					;";
                }
                $this->_pdoObj = dbconnection::getInstance();
                $pdoStatement = $this->_pdoObj->prepare($sql, array(
                    PDO::ATTR_CURSOR => PDO::CURSOR_FWDONLY
                ));
                $pdoStatement->execute(array());
                if ($pdoStatement->errorCode() * 1 != 0) {
                    $this->addError('', gettext('SQL Error') . ': ' . print_r($pdoStatement->errorInfo(), true), 1);
                } else {
                    $row = $pdoStatement->fetch(PDO::FETCH_ASSOC);
                }
                if ($ev['selfregistrationallowed'] == 1) {
                    $sql = "SELECT
					IFNULL(SUM(registered),0) selfregistered
					FROM `zguest$eventid`
					WHERE archived = 0 AND deleted = 0 AND invited = 0
					;";
                    $pdoStatement = $this->_pdoObj->prepare($sql, array(
                        PDO::ATTR_CURSOR => PDO::CURSOR_FWDONLY
                    ));
                    $pdoStatement->execute(array());
                    if ($pdoStatement->errorCode() * 1 != 0) {
                        $this->addError('', gettext('SQL Error') . ': ' . print_r($pdoStatement->errorInfo(), true), 1);
                    } else {
                        $row2 = $pdoStatement->fetch();
                        $row['selfregistered'] = $row2['selfregistered'];
                    }
                }
            } else {
                $this->addError('', gettext('Wrong event id') . ' ' . $eventid, 1, __FILE__ . '/' . __LINE__);
            }
        } catch (Exception $e) {
            $this->addError('', gettext('SQL Error'), 1, __FILE__ . '/' . __LINE__ . ': ' . print_r($e, true));
        }
        return $row;
    }

    private function printEventDetailsJSON($eventid)
    {
        $ev = $this->readRecord($eventid);
        return $ev;
    }

    private function handleSave($createFromTemplate = false)
    {
        if (is_numeric($this->_ps['fevent_id']) && $this->checkUserEvent($this->_ps['fevent_id'])) {
            
            // check if title is long enough
            if (isset($this->_ps['title']) && strlen($this->_ps['title']) < 3) {
                $this->addError('title', gettext('Please provide your new event with a name / title (min. 3 characters).'));
            }
            // check if value for guest login format is valid
            if (isset($this->_ps['guestloginformat']) && $this->_ps['guestloginformat'] != 0 && $this->_ps['guestloginformat'] != 1) {
                $this->addError('guestloginformat', gettext('The value for the RSVP login format must be 0 or 1.'));
            }
            
            $optionfields = array(
                'optionunsure',
                'registrationactive',
                'checkinactive',
                'unconfirmedwarn',
                'optioncompanionnames',
                'selfregistrationallowed'
            );
            foreach ($optionfields as $of) {
                if (isset($this->_ps[$of]) && $this->_ps[$of] != 1) {
                    $this->_ps[$of] = 0;
                }
            }
            
            if (isset($this->_ps['mailsender']) && $this->_ps['mailsender'] != '' && ! filter_var($this->_ps['mailsender'], FILTER_VALIDATE_EMAIL)) {
                $this->addError('mailsender', gettext('The mailsender address is invalid.'));
            }
            if (isset($this->_ps['mailsendername'])) {
                $this->_ps['mailsendername'] = trim($this->_ps['mailsendername']);
            }
            if (isset($this->_ps['mailsendername']) && $this->_ps['mailsendername'] != '' && strlen($this->_ps['mailsendername']) < 3) {
                $this->addError('mailsendername', gettext('If you decide for a mailsender name, it needs to be at least 3 characters long.'));
            }
            if (isset($this->_ps['mailbcc']) && $this->_ps['mailbcc'] != '') {
                $bccrecps = explode(',', $this->_ps['mailbcc']);
                foreach ($bccrecps as $bccrecp) {
                    if (! filter_var(trim($bccrecp), FILTER_VALIDATE_EMAIL)) {
                        $this->addError('mailbcc', gettext('One of the email bcc addresses is invalid.'));
                    }
                }
            }
            if (isset($this->_ps['selfregistrationmaxcomp']) && ! is_numeric($this->_ps['selfregistrationmaxcomp'])) {
                $this->addError('selfregistrationmaxcomp', gettext('The value for the field "Maximum number of companions per self-registration" must be a number.'));
            }
            if (isset($this->_ps['checkintype']) && (! is_numeric($this->_ps['checkintype']) || $this->_ps['checkintype'] < 0 || $this->_ps['checkintype'] > 3)) {
                $this->addError('checkintype', gettext('The value for check-in type must be 0, 1, 2, or 3.'));
            }
            if (isset($this->_ps['checkinhardlimit']) && ($this->_ps['checkinhardlimit'] != 0 && $this->_ps['checkinhardlimit'] != 1)) {
                $this->addError('', gettext('The value for checkinhardlimit must be 0 or 1.'));
            }
            if (isset($this->_ps['checkinlimitsource']) && ($this->_ps['checkinlimitsource'] != 0 && $this->_ps['checkinlimitsource'] != 1)) {
                $this->addError('checkinlimitsource', gettext('The value for checkinlimitsource must be 0 or 1.'));
            }
            if (isset($this->_ps['showbrowserscanlink']) && ($this->_ps['showbrowserscanlink'] != 0 && $this->_ps['showbrowserscanlink'] != 1)) {
                $this->addError('showbrowserscanlink', gettext('The value for showbrowserscanlink must be 0 or 1.'));
            }
            if (isset($this->_ps['showstatslink']) && ($this->_ps['showstatslink'] != 0 && $this->_ps['showstatslink'] != 1)) {
                $this->addError('showstatslink', gettext('The value for showstatslink must be 0 or 1.'));
            }
            if (isset($this->_ps['showpresencelink']) && ($this->_ps['showpresencelink'] != 0 && $this->_ps['showpresencelink'] != 1)) {
                $this->addError('showpresencelink', gettext('The value for showpresencelink must be 0 or 1.'));
            }
            if (isset($this->_ps['showtooltips']) && ($this->_ps['showtooltips'] != 0 && $this->_ps['showtooltips'] != 1)) {
                $this->addError('showtooltips', gettext('The value for showtooltips must be 0 or 1.'));
            }
            
            $allowedFields = array(
                'title',
                'description',
                'optionunsure',
                'domains',
                'fontfamily',
                'footerhtml',
                'accesscodelabel',
                'registerbuttonlabel',
                'checkintype',
                'checkindomain',
                'guestloginformat',
                'welcometext',
                'introductiontext',
                'accepttext',
                'denytext',
                'ticketprinthtml',
                'ticketprintperpage',
                'ticketprintpdfmargin',
                'ticketprinthtmlpage',
                'acceptthankyoutext',
                'denythankyoutext',
                'acceptmailcontent',
                'denymailcontent',
                'organisermailcontent',
                'acceptmailsubject',
                'denymailsubject',
                'organisermailsubject',
                'organisermailto',
                'mailsender',
                'mailsendername',
                'mailbcc',
                'fontcolor',
                'browserbgcolor',
                'browserbgimage',
                'inputbgcolor',
                'formbgcolor',
                'registrationactive',
                'checkinactive',
                'unconfirmedwarn',
                'optioncompanionnames',
                'checkinhardlimit',
                'checkinlimitsource',
                'showbrowserscanlink',
                'showstatslink',
                'showpresencelink',
                'showtooltips',
                'formfontsize',
                'hideanswerform',
                'selfregistrationallowed',
                'selfregistrationmaxcomp'
            );
            
            // save
            if (count($this->_err) == 0) {
                try {
                    $sql = "UPDATE fevent SET ";
                    $sqlArr = array();
                    $fieldcounter = 0;
                    foreach ($allowedFields as $f) {
                        if (isset($this->_ps[$f])) {
                            $fieldcounter ++;
                            $sql .= " " . $f . " = :" . $f . ", ";
                            $sqlArr[':' . $f] = $this->_ps[$f];
                        }
                    }
                    if ($fieldcounter > 0) {
                        $sql = substr($sql, 0, - 2);
                        $sql .= " WHERE
						fevent_id = :eventid AND
						(fuser_id = :userid OR fcustomer_id = :fcustomer_id)";
                        $sqlArr[':userid'] = $_SESSION['user']['id'];
                        $sqlArr[':eventid'] = $this->_ps['fevent_id'];
                        $sqlArr[':fcustomer_id'] = $_SESSION['customer']['customer_id'];
                        
                        $this->_pdoObj = dbconnection::getInstance();
                        $pdoStatement = $this->_pdoObj->prepare($sql, array(
                            PDO::ATTR_CURSOR => PDO::CURSOR_FWDONLY
                        ));
                        $pdoStatement->execute($sqlArr);
                        if ($pdoStatement->errorCode() * 1 != 0) {
                            $this->addError('', gettext('SQL Error') . print_r($pdoStatement->errorInfo(), true), 1);
                        } else {
                            $eventid = $this->_pdoObj->lastInsertId();
                        }
                    }
                    
                    $mss = multisettings::getMultiSettingTypes();
                    $mssType = array();
                    foreach ($mss as $msname => $ms) {
                        foreach ($ms['fields'] as $fld1) {
                            $mssType[$msname][$fld1['name']] = $fld1['type'];
                        }
                    }
                    foreach ($this->_ps as $allfieldname => $allfieldvalue) {
                        $parts = explode('_', $allfieldname);
                        if (count($parts) == 3) {
                            $ftype = $mssType[$parts[0]][$parts[2]];
                            
                            $sqlArr = array(
                                'skey' => $allfieldname,
                                'sval_txt' => null,
                                'sval_int' => null,
                                'sval_vch' => null,
                                'sval_dtt' => null,
                                'sval_dec' => null
                            );
                            switch ($ftype) {
                                case 'text':
                                    $sqlArr['sval_txt'] = $allfieldvalue;
                                    break;
                                case 'int':
                                    $sqlArr['sval_int'] = $allfieldvalue;
                                    break;
                                case 'char':
                                    $sqlArr['sval_vch'] = $allfieldvalue;
                                    break;
                                case 'datetime':
                                    $sqlArr['sval_dtt'] = $allfieldvalue;
                                    break;
                                case 'decimal':
                                    $sqlArr['sval_dec'] = $allfieldvalue;
                                    break;
                            }
                            
                            $sql = "INSERT INTO `fevset_" . $this->_ps['fevent_id'] . "` 
									(skey, sval_txt, sval_int, sval_vch, sval_dtt, sval_dec)
								VALUES (:skey, :sval_txt, :sval_int, :sval_vch, :sval_dtt, :sval_dec)
								ON DUPLICATE KEY UPDATE sval_txt = :sval_txt, 
									sval_int = :sval_int, sval_vch = :sval_vch, 
									sval_dtt = :sval_dtt, sval_dec = :sval_dec;";
                            $pdoStatement = $this->_pdoObj->prepare($sql, array(
                                PDO::ATTR_CURSOR => PDO::CURSOR_FWDONLY
                            ));
                            $pdoStatement->execute($sqlArr);
                            if ($pdoStatement->errorCode() * 1 != 0) {
                                $this->addError('', gettext('SQL Error') . print_r($pdoStatement->errorInfo(), true), 1);
                            }
                        }
                    }
                } catch (Exception $e) {
                    $this->addError('', gettext('Database Error'), 1, print_r($e->getMessage(), true) . ' / ' . $sql);
                }
                try {
                    foreach ($this->_eventsettingfields as $f => $ftype) {
                        if (isset($this->_ps[$f])) {
                            if ($f == 'documentfilenameprefix') {
                                if (strpos($this->_ps[$f], ' ') !== false) {
                                    $this->addError('documentfilenameprefix', gettext('The document filename prefix must not contain spaces.'));
                                }
                            }
                            $sqlArr = array(
                                'skey' => $f,
                                'sval_txt' => null,
                                'sval_int' => null,
                                'sval_vch' => null,
                                'sval_dtt' => null,
                                'sval_dec' => null
                            );
                            switch ($ftype) {
                                case 'text':
                                    $sqlArr['sval_txt'] = $this->_ps[$f];
                                    break;
                                case 'int':
                                    $sqlArr['sval_int'] = $this->_ps[$f];
                                    break;
                                case 'char':
                                    $sqlArr['sval_vch'] = $this->_ps[$f];
                                    break;
                                case 'datetime':
                                    /**
                                     * ToDo:
                                     * format automatically!
                                     */
                                    $sqlArr['sval_dtt'] = $this->_ps[$f];
                                    break;
                                case 'decimal':
                                    $sqlArr['sval_dec'] = $this->_ps[$f];
                                    break;
                            }
                            if (count($this->_err) == 0) {
                                $sql = "INSERT INTO `fevset_" . $this->_ps['fevent_id'] . "` (skey, sval_txt, sval_int, sval_vch, sval_dtt, sval_dec)
								VALUES (:skey, :sval_txt, :sval_int, :sval_vch, :sval_dtt, :sval_dec) 
								ON DUPLICATE KEY UPDATE sval_txt = :sval_txt, sval_int = :sval_int, sval_vch = :sval_vch, sval_dtt = :sval_dtt, sval_dec = :sval_dec;";
                                $this->_pdoObj = dbconnection::getInstance();
                                $pdoStatement = $this->_pdoObj->prepare($sql, array(
                                    PDO::ATTR_CURSOR => PDO::CURSOR_FWDONLY
                                ));
                                $pdoStatement->execute($sqlArr);
                                if ($pdoStatement->errorCode() * 1 != 0) {
                                    $this->addError('', gettext('SQL Error setting ') . $f . print_r($pdoStatement->errorInfo(), true), 1);
                                }
                            }
                        }
                    }
                } catch (Exception $e) {
                    $this->addError('', gettext('Database Error'), 1, print_r($e->getMessage(), true));
                }
                if (count($this->_err) == 0 && $createFromTemplate === false) {
                    $ev = $this->readRecord($this->_ps['fevent_id']);
                    $sm = new L5Smarty();
                    $sm->assign('eventsavesuccessful', true);
                    $sm->assign('ev', $ev);
                    $sm->assign('fevent_id', $this->_ps['fevent_id']);
                    $sm->assign('ffamval', $this->generateFontFamilyOptions());
                    $sm->assign('ffamout', $this->generateFontFamilyOptions());
                    $sm->assign('smtpsecureval', $this->generateSmtpSecureVals());
                    $sm->assign('smtpsecureout', $this->generateSmtpSecureVals());
                    $sm->assign('invoicehtmltemplate_ids', $this->generateEmailTemplateOptions());
                    // $xout = $sm->fetch ( 'event/eventform.tpl' );
                }
            }
        } else {
            $this->addError('', gettext('Invalid event id.'));
        }
        return $xout;
    }

    private function generateEmailTemplateOptions()
    {
        $tpls = new emailtemplate($this->_lg, $this->_locale);
        $tploptions = $tpls->getEmailtemplateList();
        // print_r($tploptions);
        // return $tploptions;
    }

    private function handleGenerateAccessCodes()
    {
        $successCounter = 0;
        $eventid = $this->_ps['fevent_id'];
        if (is_numeric($eventid) && $this->checkUserEvent($eventid)) {
            
            // $xout .= $sm->fetch('');
            
            $sql = "SELECT guest_id FROM zguest" . $eventid . " WHERE accesscode = '';";
            $this->_pdoObj = dbconnection::getInstance();
            $pdoStatement = $this->_pdoObj->prepare($sql, array(
                PDO::ATTR_CURSOR => PDO::CURSOR_FWDONLY
            ));
            $pdoStatement->execute(array());
            
            while ($row = $pdoStatement->fetch()) {
                if (count($this->_err) == 0) {
                    $tryCounter = 0;
                    $anotherTry = true;
                    while ($anotherTry == true) {
                        $accesscode = $this->generateAccessCode();
                        
                        $sqlC = "SELECT count(guest_id) FROM zguest" . $eventid . " WHERE accesscode = :accesscode;";
                        $pdoStatementC = $this->_pdoObj->prepare($sqlC, array(
                            PDO::ATTR_CURSOR => PDO::CURSOR_FWDONLY
                        ));
                        $pdoStatementC->execute(array(
                            ':accesscode' => $accesscode
                        ));
                        $rowC = $pdoStatementC->fetch();
                        if ($rowC[0] == 0) {
                            $anotherTry = false;
                        }
                        $tryCounter += 1;
                        if ($tryCounter > 999) {
                            $anotherTry = false;
                            $this->addError('', gettext('No free access codes available. Failed.') . ' (' . $accesscode . ')');
                        }
                    }
                    
                    if (count($this->_err) == 0) {
                        $sql2 = "UPDATE zguest" . $eventid . " SET accesscode = :accesscode
								WHERE guest_id = :guestid AND accesscode = '';";
                        $pdoStatement2 = $this->_pdoObj->prepare($sql2, array(
                            PDO::ATTR_CURSOR => PDO::CURSOR_FWDONLY
                        ));
                        $pdoStatement2->execute(array(
                            ':accesscode' => $accesscode,
                            ':guestid' => $row['guest_id']
                        ));
                        if ($pdoStatement2->rowCount() == 1) {
                            $successCounter += 1;
                        }
                    }
                }
            }
            if (count($this->_err) == 0) {
                $xout = '<div class="alert alert-success">' . $successCounter . ' access codes generated.</div>';
            }
        } else {
            $this->addError('', gettext('Wrong event id'));
        }
        return $xout;
    }

    private function handleCheckOutAllGuests()
    {
        $successCounter = 0;
        $eventid = $this->_ps['fevent_id'];
        if (is_numeric($eventid) && $this->checkUserEvent($eventid)) {
            
            // $xout .= $sm->fetch('');
            
            $sql = "SELECT guest_id, room_id FROM ygr" . $eventid . " WHERE pax > 0;";
            $this->_pdoObj = dbconnection::getInstance();
            $pdoStatement = $this->_pdoObj->prepare($sql, array(
                PDO::ATTR_CURSOR => PDO::CURSOR_FWDONLY
            ));
            $pdoStatement->execute(array());
            
            while ($row = $pdoStatement->fetch()) {
                if (count($this->_err) == 0) {
                    $tryCounter = 0;
                    if (count($this->_err) == 0) {
                        // check out guest
                        $this->removeGuest($row['guest_id'], $row['room_id']);
                    }
                }
            }
            if (count($this->_err) == 0) {
                $xout = '<div class="alert alert-success">' . $successCounter . ' access codes generated.</div>';
            }
        } else {
            $this->addError('', gettext('Wrong event id'));
        }
        return $xout;
    }

    private function generateAccessCode()
    {
        $blk = 'ABCDEFGHJKLMNPQRSTUVWXYZ23456789';
        $max = strlen($blk) - 1;
        $accesscode = '';
        for ($i = 0; $i < 6; $i ++) {
            $accesscode .= substr($blk, rand(0, $max), 1);
        }
        return $accesscode;
    }

    /**
     * handles user input from the form which is used to
     * create a new record
     *
     * @return Ambigous <string, void, string>
     */
    private function handleCreate($eventSettings = null)
    {
        $createFromTemplate = false;
        if ($eventSettings !== null) {
            $createFromTemplate = true;
        }
        $xout = '';
        $this->_ps['title'] = trim($this->_ps['title']);
        // check if title is long enough
        if (strlen($this->_ps['title']) < 3) {
            $this->addError('title', gettext('Please assign a title to your event (3 characters or more)'));
        }
        $this->_pdoObj = dbconnection::getInstance();
        // start transaction
        $this->_pdoObj->beginTransaction();
        
        if (count($this->_err) == 0) {
            try {
                if (! isset($eventSettings['setting']['registrationactive'])) {
                    $eventSettings['setting']['registrationactive'] = 0;
                }
                
                if (count($this->_err) == 0) {
                    // create event with basic settings
                    $sqlArr = array(
                        ':title' => $this->_ps['title'],
                        ':user' => $_SESSION['user']['id'],
                        ':fcustomer' => $_SESSION['customer']['customer_id']
                    );
                    $sql = "INSERT INTO fevent SET
						title = :title,
						fuser_id = :user, 
						fcustomer_id = :fcustomer, ";
                    foreach ($eventSettings['setting'] as $esKey => $esVal) {
                        if ($esKey != 'title' && ! isset($this->_eventsettingfields[$esKey]) && strpos($esKey, '_') === false) {
                            $sql .= $esKey . " = :" . $esKey . ", ";
                            $sqlArr[':' . $esKey] = $esVal;
                        }
                    }
                    $sql = substr($sql, 0, - 2);
                    $sql .= ";";
                    $pdoStatement = $this->_pdoObj->prepare($sql, array(
                        PDO::ATTR_CURSOR => PDO::CURSOR_FWDONLY
                    ));
                    $pdoStatement->execute($sqlArr);
                    if ($pdoStatement->errorCode() * 1 != 0) {
                        $this->addError('', gettext('SQL Error') . print_r($pdoStatement->errorInfo(), true), 1, $sql);
                    } elseif ($pdoStatement->rowCount() == 0) {
                        $this->addError('', 'Event has not ben created.', 1, $sql);
                    } else {
                        $eventid = $this->_pdoObj->lastInsertId();
                    }
                }
            } catch (Exception $e) {
                $this->addError('', gettext('Database Error'), 1, print_r($e->getMessage(), true));
            }
        }
        if (count($this->_err) == 0) {
            if ($createFromTemplate === false) {
                $this->createGuestTable($eventid);
            } else {
                $this->createEmptyGuestTable($eventid);
            }
        }
        
        if (count($this->_err) == 0) {
            $this->createEvSettingsTable($eventid);
        }
        if (count($this->_err) == 0) {
            $this->createCustomerEmailtemplateTable($_SESSION['customer']['customer_id']);
        }
        if (count($this->_err) == 0) {
            // $this->fillEvSettingsTable($eventid);
            foreach ($eventSettings['setting'] as $esKey => $esVal) {
                $fl['fevent_id'] = $eventid;
                if ($esKey != 'title' && isset($this->_eventsettingfields[$esKey]) || strpos($esKey, '_') !== false) {
                    $fl[$esKey] = $esVal;
                }
            }
            $this->_ps = $fl;
            $this->handleSave(true);
        }
        
        if (count($this->_err) == 0) {
            $this->createGuestRoomTable($eventid);
        }
        if (count($this->_err) == 0) {
            $this->createRegprocstepTable($eventid);
        }
        if (count($this->_err) == 0) {
            $roomdata = array(
                'fevent_id' => $eventid,
                'roomname' => gettext('Room 1'),
                'capacity' => 1000
            );
            $this->createEventRoom($roomdata);
        }
        if (count($this->_err) == 0) {
            $this->createGuestColumnTable($eventid);
            if ($createFromTemplate === false) {
                $this->fillGuestColumnTable($eventid);
            } else {
                $admincolumns = Array(
                    // exportorder, syshidden
                    'row_id' => array(
                        null,
                        1
                    ),
                    'guest_id' => array(
                        null,
                        0
                    ),
                    'fevent_id' => array(
                        null,
                        1
                    ),
                    'deletedtime' => array(
                        null,
                        1
                    ),
                    'archived' => array(
                        null,
                        0
                    )
                );
                $this->fillGuestColumnTable($eventid, $admincolumns);
            }
        }
        if (count($this->_err) == 0) {
            // print_r($eventSettings['fields']);
            $dont = array(
                'row_id',
                'guest_id',
                'fevent_id',
                'deleted',
                'archived'
            );
            if (isset($eventSettings['fields'])) {
                foreach ($eventSettings['fields'] as $fld) {
                    
                    if (array_search($fld['fieldname'], $dont) === false) {
                        $fld['fevent_id'] = $eventid;
                        $this->_ps = $fld;
                        $this->saveFieldData(true);
                        if (count($this->_err) > 0) {
                            break;
                        }
                    }
                }
            }
        }
        if (count($this->_err) == 0) {
            if (isset($eventSettings['multisettings']) && is_array($eventSettings['multisettings'])) {
                foreach ($eventSettings['multisettings'] as $multisettingtype => $m) {
                    foreach ($m as $mx) {
                        // seqs
                        $this->storeMultiSetting($eventid, $multisettingtype, $mx);
                    }
                }
            }
        }
        if (count($this->_err) == 0) {
            $this->createVisitorLogTable($eventid);
        }
        if (count($this->_err) == 0) {
            $this->createMassmailTable($eventid);
        }
        if (count($this->_err) == 0) {
            $sm = new L5Smarty();
            $sm->assign('eventcreatesuccessful', true);
            $xout = $sm->fetch('event/neweventform.tpl');
            // end transaction
            $this->_pdoObj->commit();
            $this->refreshEventListForUser();
        } else {
            // end transaction
            $this->_pdoObj->rollBack();
            $sql = "DELETE FROM fevent WHERE fevent_id = :eventid;";
            $pdoStatement = $this->_pdoObj->prepare($sql, array(
                PDO::ATTR_CURSOR => PDO::CURSOR_FWDONLY
            ));
            $pdoStatement->execute(array(
                ':eventid' => $eventid
            ));
            if ($pdoStatement->errorCode() * 1 != 0) {
                $this->addError('', gettext('SQL Error') . print_r($pdoStatement->errorInfo(), true), 1);
            }
            
            $sql = "DROP TABLE IF EXISTS zguest" . $eventid . ";";
            $pdoStatement = $this->_pdoObj->prepare($sql, array(
                PDO::ATTR_CURSOR => PDO::CURSOR_FWDONLY
            ));
            $pdoStatement->execute(array());
            if ($pdoStatement->errorCode() * 1 != 0) {
                $this->addError('', gettext('SQL Error') . print_r($pdoStatement->errorInfo(), true), 1);
            }
            
            $sql = "DROP TABLE IF EXISTS fevset_" . $eventid . ";";
            $pdoStatement = $this->_pdoObj->prepare($sql, array(
                PDO::ATTR_CURSOR => PDO::CURSOR_FWDONLY
            ));
            $pdoStatement->execute(array());
            if ($pdoStatement->errorCode() * 1 != 0) {
                $this->addError('', gettext('SQL Error') . print_r($pdoStatement->errorInfo(), true), 1);
            }
            
            $sql = "DROP TABLE IF EXISTS zregprocstep" . $eventid . ";";
            $pdoStatement = $this->_pdoObj->prepare($sql, array(
                PDO::ATTR_CURSOR => PDO::CURSOR_FWDONLY
            ));
            $pdoStatement->execute(array());
            if ($pdoStatement->errorCode() * 1 != 0) {
                $this->addError('', gettext('SQL Error') . print_r($pdoStatement->errorInfo(), true), 1);
            }
            
            $sql = "DROP TABLE IF EXISTS zcolumn" . $eventid . ";";
            $pdoStatement = $this->_pdoObj->prepare($sql, array(
                PDO::ATTR_CURSOR => PDO::CURSOR_FWDONLY
            ));
            $pdoStatement->execute(array());
            if ($pdoStatement->errorCode() * 1 != 0) {
                $this->addError('', gettext('SQL Error') . print_r($pdoStatement->errorInfo(), true), 1);
            }
            
            $sql = "DROP TABLE IF EXISTS ygr" . $eventid . ";";
            $pdoStatement = $this->_pdoObj->prepare($sql, array(
                PDO::ATTR_CURSOR => PDO::CURSOR_FWDONLY
            ));
            $pdoStatement->execute(array());
            if ($pdoStatement->errorCode() * 1 != 0) {
                $this->addError('', gettext('SQL Error') . print_r($pdoStatement->errorInfo(), true), 1);
            }
            // ToDo: Check! some of these tables seem not to be deleted, especially the xvl one.
            $sql = "DROP TABLE IF EXISTS xvl" . $eventid . ";";
            $pdoStatement = $this->_pdoObj->prepare($sql, array(
                PDO::ATTR_CURSOR => PDO::CURSOR_FWDONLY
            ));
            $pdoStatement->execute(array());
            if ($pdoStatement->errorCode() * 1 != 0) {
                $this->addError('', gettext('SQL Error') . print_r($pdoStatement->errorInfo(), true), 1);
            }
        }
        if (count($this->_err) > 0) {
            print_r($this->_err);
        }
        
        return $xout;
    }

    /**
     * returns an array of all events for the current user
     * this array can easily converted via json_encode
     *
     * @return multitype:array
     */
    public function getEventList()
    {
        $sql = "SELECT * FROM fevent 
				WHERE (fuser_id = :user OR fcustomer_id = :customer)
				ORDER BY createdtime ASC
				;";
        $this->_pdoObj = dbconnection::getInstance();
        $pdoStatement = $this->_pdoObj->prepare($sql, array(
            PDO::ATTR_CURSOR => PDO::CURSOR_FWDONLY
        ));
        $pdoStatement->execute(array(
            ':user' => $_SESSION['user']['id'],
            ':customer' => $_SESSION['customer']['customer_id']
        ));
        if ($pdoStatement->errorCode() > 0) {
            $this->addError('', gettext('Error reading the list of events.'), 1);
        } else {
            while ($row = $pdoStatement->fetch()) {
                $res[] = array(
                    'id' => $row['fevent_id'],
                    'title' => $row['title']
                );
            }
        }
        return $res;
    }

    public function printEventListMenu($currentid = null)
    {
        $xout = '<option value="0">' . gettext('Please choose an event.') . '</option>';
        $sql = "SELECT * FROM fevent 
				WHERE (fuser_id = :user OR fcustomer_id = :customer)
				ORDER BY createdtime DESC
				;";
        $this->_pdoObj = dbconnection::getInstance();
        $pdoStatement = $this->_pdoObj->prepare($sql, array(
            PDO::ATTR_CURSOR => PDO::CURSOR_FWDONLY
        ));
        $pdoStatement->execute(array(
            ':user' => $_SESSION['user']['id'],
            ':customer' => $_SESSION['customer']['customer_id']
        ));
        if ($pdoStatement->errorCode() > 0) {
            $this->addError('', gettext('Error reading the list of events.'), 1);
        } else {
            while ($row = $pdoStatement->fetch()) {
                // $xout .= '<li><a href="/'.$this->_lg.'/event/'.$row['fevent_id'].'">'.$row['title'].'</a>'."\n";
                $xout .= '<option value="' . $row['fevent_id'] . '"';
                if ($currentid == $row['fevent_id']) {
                    $xout .= ' selected ';
                }
                $xout .= '>' . $row['title'] . '</option>';
            }
        }
        return $xout;
    }

    /**
     * creates a database table with the name zguest<event_id>.
     * This table is
     * used to store the guest list.
     *
     * @param int $eventid
     *            the event id
     */
    private function createGuestTable($eventid)
    {
        try {
            $sql = "
					CREATE TABLE IF NOT EXISTS `zguest" . $eventid . "` (
							`row_id` int(11) NOT NULL AUTO_INCREMENT,
							`guest_id` int(11) NOT NULL,
							`fevent_id` int(11) NOT NULL,
							`deleted` int(1) NOT NULL DEFAULT '0',
							`archived` int(1) NOT NULL DEFAULT '0',
							`salutation` varchar(20) DEFAULT NULL,
							`firstname` varchar(128) DEFAULT NULL,
							`lastname` varchar(128) DEFAULT NULL,
							`company` varchar(128) DEFAULT NULL,
							`emailaddress` varchar(128) NOT NULL,
							`invited` int(11) NOT NULL DEFAULT '1',
							`registered` int(11) NOT NULL DEFAULT '0',
							`cancelled` int(11) NOT NULL DEFAULT '0',
							`companionnames` text,
							`note` text,
							`accesscode` varchar(60) NOT NULL,
							`lastselflogin` timestamp NULL DEFAULT NULL,
							`selfloginsfailed` int(11) NOT NULL DEFAULT '0',
							`createdtime` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
							`changetime` datetime DEFAULT NULL,
							PRIMARY KEY (`row_id`),
							KEY `fevent_id` (`fevent_id`),
							INDEX (`guest_id`),
							INDEX ( `deleted` , `archived`)
							) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;
							";
            $this->_pdoObj = dbconnection::getInstance();
            $pdoStatement = $this->_pdoObj->prepare($sql, array(
                PDO::ATTR_CURSOR => PDO::CURSOR_FWDONLY
            ));
            $pdoStatement->execute(array());
            if ($pdoStatement->errorCode() * 1 > 0) {
                $this->addError('', gettext('Error while creating the guest table'), 1, print_r($pdoStatement->errorInfo(), true));
            }
        } catch (Exception $e) {
            $this->addError('', 'PDO ' . gettext('Error while creating the guest table'), 1, print_r($e->getMessage(), true));
        }
    }

    /**
     * creates a database table with the name emailtemplate_<customer_id>.
     * This table is used to store customer specific email templates.
     * It is a per-customer table, not a per-user or per-event table!
     *
     * @param int $customerid
     *            the customer id
     */
    private function createCustomerEmailtemplateTable($customerid)
    {
        try {
            $sql = "
					CREATE TABLE IF NOT EXISTS `emailtemplate_" . $customerid . "` (
  				`emailtemplate_id` int(11) NOT NULL AUTO_INCREMENT,
  				`fcustomer_id` int(11) NOT NULL DEFAULT '0',
  				`title` varchar(128) NOT NULL,
  				`description` text NOT NULL,
				`emailsubject` varchar(256) NOT NULL,
  				`content` mediumtext NOT NULL,
  				`created_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  				`created_fuser_id` int(11) NOT NULL,
  				`modified_time` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  				`modified_fuser_id` int(11) NOT NULL,
  				PRIMARY KEY (`emailtemplate_id`)
				) ENGINE=InnoDB DEFAULT CHARSET=utf8;
							";
            $this->_pdoObj = dbconnection::getInstance();
            $pdoStatement = $this->_pdoObj->prepare($sql, array(
                PDO::ATTR_CURSOR => PDO::CURSOR_FWDONLY
            ));
            $pdoStatement->execute(array());
            if ($pdoStatement->errorCode() * 1 > 0) {
                $this->addError('', gettext('Error while creating the guest table'), 1, print_r($pdoStatement->errorInfo(), true));
            }
        } catch (Exception $e) {
            $this->addError('', 'PDO ' . gettext('Error while creating the guest table'), 1, print_r($e->getMessage(), true));
        }
    }

    /**
     * creates a database table with the name zguest<event_id>.
     * This table is
     * used to store the guest list.
     *
     * @param int $eventid
     *            the event id
     */
    private function createEmptyGuestTable($eventid)
    {
        try {
            $sql = "
					CREATE TABLE IF NOT EXISTS `zguest" . $eventid . "` (
							`row_id` int(11) NOT NULL AUTO_INCREMENT,
							`guest_id` int(11) NOT NULL,
							`fevent_id` int(11) NOT NULL,
							`deleted` int(1) NOT NULL DEFAULT '0',
							`archived` int(1) NOT NULL DEFAULT '0',
							PRIMARY KEY (`row_id`),
							KEY `fevent_id` (`fevent_id`),
							INDEX (`guest_id`),
							INDEX ( `deleted` , `archived`)
							) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;
							";
            // $this->_pdoObj = dbconnection::getInstance();
            $pdoStatement = $this->_pdoObj->prepare($sql, array(
                PDO::ATTR_CURSOR => PDO::CURSOR_FWDONLY
            ));
            $pdoStatement->execute(array());
            if ($pdoStatement->errorCode() * 1 > 0) {
                $this->addError('', gettext('Error while creating empty guest table'), 1, print_r($pdoStatement->errorInfo(), true));
            }
        } catch (Exception $e) {
            $this->addError('', 'PDO ' . gettext('Error while creating empty guest table'), 1, print_r($e->getMessage(), true));
        }
    }

    /**
     * creates a database table with the name zregprocstep<event_id>.
     * This table is
     * used to store the steps of the registration process.
     *
     * @param int $eventid
     *            the event id
     */
    private function createRegprocstepTable($eventid)
    {
        try {
            $sql = "
					CREATE TABLE IF NOT EXISTS `zregprocstep" . $eventid . "` (
  							`regprocstep_id` int(11) NOT NULL AUTO_INCREMENT,
							`sequence` int(11) NOT NULL DEFAULT '0',
							`extension_id` int(11) NOT NULL DEFAULT '0',
  							`description` text NOT NULL,
  							`active` tinyint(1) NOT NULL DEFAULT '1',
  							PRIMARY KEY (`regprocstep_id`)
							) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;
							";
            $this->_pdoObj = dbconnection::getInstance();
            $pdoStatement = $this->_pdoObj->prepare($sql, array(
                PDO::ATTR_CURSOR => PDO::CURSOR_FWDONLY
            ));
            $pdoStatement->execute(array());
            if ($pdoStatement->errorCode() * 1 > 0) {
                $this->addError('', gettext('Error while creating the regprocstep table'), 1, print_r($pdoStatement->errorInfo(), true));
            }
        } catch (Exception $e) {
            $this->addError('', 'PDO ' . gettext('Error while creating the regprocstep table'), 1, print_r($e->getMessage(), true));
        }
    }

    /**
     * creates a database table with the name fevset_<event_id>.
     * This table is
     * used to store settings of the event.
     *
     * @param int $eventid
     *            the event id
     */
    private function createEvSettingsTable($eventid)
    {
        try {
            $sql = "
					CREATE TABLE IF NOT EXISTS `fevset_" . $eventid . "` (
  							`skey` VARCHAR(128) NOT NULL,
							`skey_multiname` VARCHAR(256) NOT NULL DEFAULT '', 
							`skey_multiseq` INT NOT NULL DEFAULT '0', 
							`skey_multikey` VARCHAR(256) NOT NULL DEFAULT '', 
							`skey_multiid` VARCHAR(256) NOT NULL DEFAULT '',
							`pskey_multiname` VARCHAR(256) NULL DEFAULT NULL,
							`pskey_multikey` VARCHAR(256) NULL DEFAULT NULL,
							`pskey_multiid` VARCHAR(256) NULL DEFAULT NULL,
							`sval_int` INT(11) DEFAULT NULL,
							`sval_vch` VARCHAR(128) DEFAULT NULL,
							`sval_txt` TEXT DEFAULT NULL,
							`sval_dtt` DATETIME DEFAULT NULL,
							`sval_dec` DECIMAL(10,2) NULL DEFAULT NULL,
							PRIMARY KEY (`skey`)
							) ENGINE=InnoDB DEFAULT CHARSET=utf8;
							";
            $this->_pdoObj = dbconnection::getInstance();
            $pdoStatement = $this->_pdoObj->prepare($sql, array(
                PDO::ATTR_CURSOR => PDO::CURSOR_FWDONLY
            ));
            $pdoStatement->execute(array());
            if ($pdoStatement->errorCode() * 1 > 0) {
                $this->addError('', gettext('Error while creating the fevset table'), 1, print_r($pdoStatement->errorInfo(), true));
            }
        } catch (Exception $e) {
            $this->addError('', 'PDO ' . gettext('Error while creating the fevset table'), 1, print_r($e->getMessage(), true));
        }
    }

    /**
     * creates a database table with the name zguest<event_id>.
     * This table is
     * used to store which guest is in which room.
     *
     * @param int $eventid
     *            the event id
     */
    private function createGuestRoomTable($eventid)
    {
        try {
            $sql = "CREATE TABLE IF NOT EXISTS `ygr" . $eventid . "` (
					`guest_id` int(11) NOT NULL,
					`room_id` int(11) NOT NULL,
					`pax` int(11) NOT NULL,
					UNIQUE KEY `guest_id` (`guest_id`,`room_id`)
					) ENGINE=InnoDB DEFAULT CHARSET=utf8;
					";
            $this->_pdoObj = dbconnection::getInstance();
            $pdoStatement = $this->_pdoObj->prepare($sql, array(
                PDO::ATTR_CURSOR => PDO::CURSOR_FWDONLY
            ));
            $pdoStatement->execute(array());
            if ($pdoStatement->errorCode() * 1 > 0) {
                $this->addError('', gettext('Error while creating the guest/room table.'), 1, print_r($pdoStatement->errorInfo(), true));
            }
        } catch (Exception $e) {
            $this->addError('', 'PDO ' . gettext('Error while creating the guest/room table.'), 1, print_r($e->getMessage(), true));
        }
    }

    /**
     * creates a database table zcolumn<event_id> with the columns of the zguest<event_id>.
     * This table is
     * used to store which columns exist in which order.
     *
     * @param number $eventid
     */
    private function createGuestColumnTable($eventid)
    {
        try {
            $sql = "CREATE TABLE IF NOT EXISTS `zcolumn" . $eventid . "` (
					`field_id` int(11) NOT NULL AUTO_INCREMENT,
					`parent_field_id` INT(11) NOT NULL DEFAULT 0,
					`fieldorder` int(11) DEFAULT NULL,
					`pageno` INT NOT NULL DEFAULT '1', 
					`sectionno` INT NOT NULL DEFAULT '0',
					`exportorder` int(11) DEFAULT NULL,
					`fieldname` varchar(64) NOT NULL,
					`fieldtype` varchar(64) NOT NULL,
					`fieldlength` int(2) NOT NULL DEFAULT '0',
					`fielddisplaytype` INT(2) NOT NULL DEFAULT 0,
					`fielddefault` varchar(64) NOT NULL,
					`isdefaultvalue` INT(1) NOT NULL DEFAULT 0,
					`longcontent` text NOT NULL,
					`radiocheckvalue` VARCHAR(128) NOT NULL DEFAULT '',
					`syscolumn` tinyint(1) NOT NULL DEFAULT '0',
					`showtoguest` tinyint(1) NOT NULL DEFAULT '0',
					`formlabel` varchar(60) NOT NULL DEFAULT '',
					`editbyguest` tinyint(1) NOT NULL DEFAULT '0',
					`editbymanager` tinyint(1) NOT NULL DEFAULT '0',
					`syshidden` tinyint(1) NOT NULL DEFAULT '0',
					`note` text NOT NULL,
					`deleted` tinyint(1) NOT NULL DEFAULT '0',
					PRIMARY KEY (`field_id`),
					UNIQUE KEY `fieldname` (`fieldname`)
					) ENGINE=InnoDB DEFAULT CHARSET=utf8;
					";
            $this->_pdoObj = dbconnection::getInstance();
            $pdoStatement = $this->_pdoObj->prepare($sql, array(
                PDO::ATTR_CURSOR => PDO::CURSOR_FWDONLY
            ));
            $pdoStatement->execute(array());
            if ($pdoStatement->errorCode() * 1 > 0) {
                $this->addError('', gettext('Error while creating the guest/column table.'), 1, print_r($pdoStatement->errorInfo(), true));
            }
        } catch (Exception $e) {
            $this->addError('', 'PDO ' . gettext('Error while creating the guest/column table.'), 1, print_r($e->getMessage(), true));
        }
    }

    /**
     * creates a database table xvl<event_id> This table is
     * used to store who checks in / out and when.
     *
     * @param number $eventid
     */
    private function createVisitorLogTable($eventid)
    {
        try {
            $sql = "CREATE TABLE IF NOT EXISTS `xvl" . $eventid . "` (
					`id` bigint(20) NOT NULL AUTO_INCREMENT,
					`parent_id` int(11) DEFAULT NULL,
					`ts` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
					`guest_id` int(11) NOT NULL,
					`ip` varchar(48) NOT NULL,
					`inout` varchar(3) NOT NULL,
					`room` VARCHAR(16) NOT NULL,
					`suppdata` MEDIUMTEXT NULL DEFAULT NULL,
					`dec1` DECIMAL(10,2) NULL DEFAULT NULL , 
					`dec2` DECIMAL(10,2) NULL DEFAULT NULL ,
					`dec3` DECIMAL(10,2) NULL DEFAULT NULL , 
					`dec4` DECIMAL(10,2) NULL DEFAULT NULL , 
					`dec5` DECIMAL(10,2) NULL DEFAULT NULL , 
					`dec6` DECIMAL(10,2) NULL DEFAULT NULL ,
					`dec7` DECIMAL(10,2) NULL DEFAULT NULL ,
					`dec8` DECIMAL(10,2) NULL DEFAULT NULL ,
					`flg01` TINYINT(1) NOT NULL DEFAULT '0' ,
					`flg02` TINYINT(1) NOT NULL DEFAULT '0' ,
					`flg03` TINYINT(1) NOT NULL DEFAULT '0' ,
					`flg04` TINYINT(1) NOT NULL DEFAULT '0' ,
					`flg05` TINYINT(1) NOT NULL DEFAULT '0' , 
					PRIMARY KEY (`id`)
					) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1
					";
            $this->_pdoObj = dbconnection::getInstance();
            $pdoStatement = $this->_pdoObj->prepare($sql, array(
                PDO::ATTR_CURSOR => PDO::CURSOR_FWDONLY
            ));
            $pdoStatement->execute(array());
            if ($pdoStatement->errorCode() * 1 > 0) {
                $this->addError('', gettext('Error while creating the visitor log table.'), 1, print_r($pdoStatement->errorInfo(), true));
            }
            
            $sql = "ALTER TABLE `xvl" . $eventid . "`
 				ADD KEY `inout` (`inout`), ADD KEY `guestid` (`guest_id`);
			";
            $pdoStatement = $this->_pdoObj->prepare($sql, array(
                PDO::ATTR_CURSOR => PDO::CURSOR_FWDONLY
            ));
            $pdoStatement->execute(array());
            if ($pdoStatement->errorCode() * 1 > 0) {
                $this->addError('', gettext('Error while creating the visitor log table.'), 1, print_r($pdoStatement->errorInfo(), true));
            }
        } catch (Exception $e) {
            $this->addError('', 'PDO ' . gettext('Error while creating the visitor log table.'), 1, print_r($e->getMessage(), true));
        }
    }

    private function createMassmailTable($eventid)
    {
        try {
            $sql = "CREATE TABLE IF NOT EXISTS `wemail" . $eventid . "` (
  				`email_id` int(11) NOT NULL AUTO_INCREMENT,
  				`subject` varchar(255) NOT NULL,
  				`mailtext` text NOT NULL,
  				`sendstatus` tinyint(1) NOT NULL DEFAULT '0',
  				`massmailrecipients` set('1','2','3','4','5') DEFAULT NULL,
  				`deleted` tinyint(1) NOT NULL DEFAULT '0',
  				PRIMARY KEY (`email_id`)
				) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;
			";
            $this->_pdoObj = dbconnection::getInstance();
            $pdoStatement = $this->_pdoObj->prepare($sql, array(
                PDO::ATTR_CURSOR => PDO::CURSOR_FWDONLY
            ));
            $pdoStatement->execute(array());
            if ($pdoStatement->errorCode() * 1 > 0) {
                $this->addError('', gettext('Error while creating the email table.'), 1, print_r($pdoStatement->errorInfo(), true));
            }
        } catch (Exception $e) {
            $this->addError('', 'PDO ' . gettext('Error while creating the email table.'), 1, print_r($e->getMessage(), true));
        }
    }

    /**
     * fills the database table zcolumn<event_id>
     *
     * @param int $eventid
     *            an event id
     */
    private function fillGuestColumnTable($eventid, $cols = null)
    {
        global $glob;
        $lang = $_SESSION['lg'];
        try {
            if ($cols === null) {
                $cols = Array(
                    // exportorder, syshidden
                    'guest_id' => array(
                        null,
                        0
                    ),
                    'fevent_id' => array(
                        null,
                        1
                    ),
                    'salutation' => array(
                        1,
                        0
                    ),
                    'firstname' => array(
                        2,
                        0
                    ),
                    'lastname' => array(
                        3,
                        0
                    ),
                    'company' => array(
                        4,
                        0
                    ),
                    'emailaddress' => array(
                        10,
                        0
                    ),
                    'invited' => array(
                        5,
                        0
                    ),
                    'registered' => array(
                        6,
                        0
                    ),
                    'cancelled' => array(
                        7,
                        0
                    ),
                    'note' => array(
                        8,
                        0
                    ),
                    'accesscode' => array(
                        9,
                        0
                    ),
                    'lastselflogin' => array(
                        null,
                        1
                    ),
                    'selfloginsfailed' => array(
                        null,
                        1
                    ),
                    'deletedtime' => array(
                        null,
                        1
                    ),
                    'createdtime' => array(
                        null,
                        1
                    ),
                    'changetime' => array(
                        null,
                        1
                    )
                );
            }
            
            $this->_pdoObj = dbconnection::getInstance();
            $sql = "SHOW COLUMNS FROM `zguest" . $eventid . "`;";
            $pdoStatement2 = $this->_pdoObj->prepare($sql, array(
                PDO::ATTR_CURSOR => PDO::CURSOR_FWDONLY
            ));
            $pdoStatement2->execute(Array());
            
            if ($pdoStatement2->errorCode() > 0) {
                $this->addError('', gettext('Error while reading from zguest table.'), 1, print_r($pdoStatement2->errorInfo(), true));
            }
            $counter = 1;
            if (count($this->_err) == 0) {
                while ($row2 = $pdoStatement2->fetch()) {
                    $bracketpos = strpos($row2['Type'], '(');
                    if ($bracketpos !== false) {
                        $ftype = substr($row2['Type'], 0, $bracketpos);
                        $bracket2pos = strpos($row2['Type'], ')');
                        $flen = substr($row2['Type'], $bracketpos + 1, $bracket2pos - $bracketpos - 1);
                    } else {
                        $ftype = $row2['Type'];
                        $flen = - 1;
                    }
                    if ($row2['Default'] == NULL) {
                        $fdef = '';
                    } else {
                        $fdef = $row2['Default'];
                    }
                    if (is_numeric($cols[$row2['Field']][0])) {
                        $exportorder = $cols[$row2['Field']][0];
                    } else {
                        $exportorder = NULL;
                    }
                    
                    if ($cols[$row2['Field']][1] == 1) {
                        $syshidden = 1;
                    } else {
                        $syshidden = 0;
                    }
                    $sql9Arr = Array(
                        ':counter' => $counter,
                        ':fieldname' => $row2['Field'],
                        ':fieldtype' => $ftype,
                        ':fieldlength' => $flen,
                        ':fielddefault' => $fdef,
                        ':syshidden' => $syshidden,
                        ':exportorder' => $exportorder
                    );
                    $sql = "INSERT INTO `zcolumn" . $eventid . "`
							SET fieldorder = :counter,
							fieldname = :fieldname, fieldtype = :fieldtype,
							fieldlength = :fieldlength, fielddefault = :fielddefault,
							syscolumn = 1, syshidden = :syshidden,
							exportorder = :exportorder, deleted = 0 ";
                    if (isset($glob['columnlabels'][$lang][$row2['Field']]) && $glob['columnlabels'][$lang][$row2['Field']] != '') {
                        $sql .= ", formlabel = :formlabel ";
                        $sql9Arr[':formlabel'] = $glob['columnlabels'][$lang][$row2['Field']];
                    }
                    $sql .= ";";
                    $pdoStatement3 = $this->_pdoObj->prepare($sql, array(
                        PDO::ATTR_CURSOR => PDO::CURSOR_FWDONLY
                    ));
                    $pdoStatement3->execute($sql9Arr);
                    if ($pdoStatement3->errorCode() > 0) {
                        $this->addError('', gettext('Error while filling the column table.'), 1, print_r($pdoStatement3->errorInfo(), true));
                    }
                    $counter += 1;
                }
            }
        } catch (Exception $e) {
            $this->addError('', gettext('PDO Error while filling the guest / column table.'), 1, print_r($e->getMessage(), true));
        }
    }

    /**
     * checks if the given event belongs to the currently logged-in user
     * and returns true if yes, or false if no.
     *
     * @param int $eventid
     *            an event id
     * @return boolean if the given event belongs to the currently logged in user
     */
    public function checkUserEvent($eventid)
    {
        $res = false;
        if (LIVEENV === true) {
            $res = true;
        } else {
            if (is_numeric($eventid)) {
                if (count($this->_usersEvents) == 0) {
                    $this->refreshEventListForUser();
                }
                if ($this->_usersEvents[$eventid] === true) {
                    $res = true;
                }
            } else {
                $this->addError('', gettext('Event Id not numeric'), 1);
            }
        }
        return $res;
    }

    private function refreshEventListForUser()
    {
        try {
            $sql = "SELECT fevent_id FROM fevent 
					WHERE (fuser_id = :userid OR fcustomer_id = :customerid);";
            $this->_pdoObj = dbconnection::getInstance();
            $pdoStatement = $this->_pdoObj->prepare($sql, array(
                PDO::ATTR_CURSOR => PDO::CURSOR_FWDONLY
            ));
            $pdoStatement->execute(array(
                ':userid' => $_SESSION['user']['id'],
                ':customerid' => $_SESSION['customer']['customer_id']
            ));
            if ($pdoStatement->errorCode() * 1 > 0) {
                $this->addError('', gettext('Error reading the list of events.'), 1);
            } else {
                // $this->_usersEvents = array ();
                while ($row = $pdoStatement->fetch()) {
                    $this->_usersEvents[$row['fevent_id']] = true;
                }
            }
        } catch (Exception $e) {
            $this->addError('', 'Problem refreshing the list of events for the user.', 1, print_r($e, true));
        }
    }

    /**
     * creates a room for an event.
     * needed for creation of an event - each event should have at least one room...
     *
     * @param array $data
     *            format:
     *            $data['fevent_id'] = <int - event id>
     *            $data['roomname'] = <str - name of the room>
     *            $data['capacity'] = <int - capacity of the room (people)>
     */
    private function createEventRoom($data)
    {
        try {
            $sql = "INSERT INTO room
					SET fevent_id = :eventid, roomname = :roomname,
					capacity = :capacity;";
            $this->_pdoObj = dbconnection::getInstance();
            $pdoStatement = $this->_pdoObj->prepare($sql, array(
                PDO::ATTR_CURSOR => PDO::CURSOR_FWDONLY
            ));
            $pdoStatement->execute(array(
                ':eventid' => $data['fevent_id'],
                ':roomname' => $data['roomname'],
                ':capacity' => $data['capacity']
            ));
            if ($pdoStatement->errorCode() * 1 > 0) {
                $this->addError('', gettext('Error while creating a room.'), 1, print_r($pdoStatement->errorInfo(), true));
            }
        } catch (Exception $e) {
            $this->addError('', 'PDO ' . gettext('Error while creating a room.'), 1, print_r($e->getMessage(), true));
        }
    }

    static function fopen_utf8($filename)
    {
        $encoding = '';
        $handle = fopen($filename, 'r');
        $bom = fread($handle, 2);
        // fclose($handle);
        rewind($handle);
        
        if ($bom === chr(0xff) . chr(0xfe) || $bom === chr(0xfe) . chr(0xff)) {
            // UTF16 Byte Order Mark present
            $encoding = 'UTF-16';
        } else {
            $file_sample = fread($handle, 1000) + 'e'; // read first 1000 bytes
                                                       // + e is a workaround for mb_string bug
            rewind($handle);
            
            // $encoding = mb_detect_encoding($file_sample , 'UTF-8, UTF-7, ASCII, EUC-JP,SJIS, eucJP-win, SJIS-win, JIS, ISO-2022-JP');
            $encoding = false;
        }
        if ($encoding) {
            stream_filter_append($handle, 'convert.iconv.' . $encoding . '/UTF-8');
        }
        return ($handle);
    }

    private function exportGuestlist($eventid)
    {
        if ($this->checkUserEvent($eventid)) {
            $importStructure = $this->getImportStructure($eventid);
            $xout = "\xEF\xBB\xBF";
            foreach ($importStructure as $fields) {
                $xout .= $fields['mysqlfield'] . "\t";
            }
            $xout .= 'directlink' . "\t";
            $xout = substr($xout, 0, - 1) . "\r\n";
            $sql = "SELECT * FROM zguest" . $eventid . " WHERE deleted = 0 AND archived = 0 ORDER BY guest_id ASC;";
            $this->_pdoObj = dbconnection::getInstance();
            $pdoStatement = $this->_pdoObj->prepare($sql, array(
                PDO::ATTR_CURSOR => PDO::CURSOR_FWDONLY
            ));
            $pdoStatement->execute(array());
            while ($row = $pdoStatement->fetch()) {
                // $res[] = $rw;
                foreach ($importStructure as $fields) {
                    $xout .= str_replace("\n", " ", str_replace("\r", " ", str_replace("\t", "", $row[$fields['mysqlfield']]))) . "\t";
                }
                // add direct rsvp link
                $xout .= $this->generateDirectRSVPLink($eventid, $row['lastname'], $row['accesscode']);
                $xout .= "\t";
                $xout = substr($xout, 0, - 1) . "\r\n";
            }
        }
        return $xout;
    }

    private function exportSettingsXML($eventid)
    {
        // header('Content-Type: application/xml; charset=UTF-8');
        $ms = multisettings::getMultiSettingTypes();
        $blacklist = array(
            'invited',
            'registered',
            'cancelled',
            'exports',
            'fevent_id',
            'fuser_id',
            'deletedtime',
            'createdtime',
            'MMR1',
            'MMR2',
            'MMR3',
            'MMR4',
            'MMR5'
        );
        $evData = $this->readRecord($eventid, true);
        ob_end_clean();
        header("Content-Type: text/html/force-download");
        header("Content-Disposition: attachment; filename=event_settings_" . $eventid . ".xml");
        
        $writer = new XMLWriter();
        $writer->openUri('php://output');
        $writer->startDocument('1.0', 'utf-8');
        $writer->setIndent(4);
        $writer->startElement('fluxlineEvent');
        $writer->writeAttribute('version', '1.0');
        $writer->writeAttribute('exportdate', date('Y-m-d H:i:s'));
        $writer->writeAttribute('exportedby', getenv('REMOTE_ADDR'));
        $writer->writeAttribute('codeversion', FLUXLINE_VERSION);
        $writer->writeAttribute('originalid', $eventid);
        $writer->writeAttribute('originalcreatedtime', $ev['createdtime']);
        $writer->startElement('settings');
        
        foreach ($evData as $evKey => $evVal) {
            $isMsCheckParts = explode('_', $evKey);
            if (count($isMsCheckParts) == 3 && isset($ms[$isMsCheckParts[0]])) {} elseif (! is_array($evVal) && array_search($evKey, $blacklist) === false && substr($evKey, 0, 2) != 'n1') {
                $writer->startElement($evKey);
                $writer->text($evVal);
                $writer->endElement();
            }
        }
        
        $writer->endElement();
        $writer->startElement('multisettings');
        foreach ($ms as $mtype => $m) {
            $writer->startElement($mtype);
            foreach ($evData[$mtype] as $seq => $evVal) {
                // get all 1:n assignments
                $n1ndata = array();
                $ms = new multisettings();
                $n1ndata = $ms->get1nRecords($eventid, $mtype);
                
                // write the multisetting to xml
                $writer->startElement($mtype);
                $writer->writeAttribute('seq', $seq);
                $writer->writeAttribute('id', $evVal['id']);
                foreach ($evVal as $evValKey => $evValVal) {
                    $writer->startElement($evValKey);
                    if (substr($evValKey, 0, 2) == 'n1') {
                        $n1nd = $n1ndata[$mtype][$evVal['id']];
                        foreach ($n1nd as $rr) {
                            $writer->startElement($rr['skey_multiname']);
                            $writer->writeAttribute('skey_multiseq', $rr['skey_multiseq']);
                            $writer->writeAttribute('skey_multiid', $rr['skey_multiid']);
                            $writer->writeAttribute('skey', $rr['skey']);
                            $writer->text($rr['sval']);
                            $writer->endElement();
                        }
                    } else {
                        $writer->text($evValVal);
                    }
                    $writer->endElement();
                }
                $writer->endElement();
            }
            $writer->endElement();
        }
        $writer->endElement();
        
        $writer->startElement('fields');
        
        $listOfFields = $this->getFields($eventid, 'ALL');
        foreach ($listOfFields as $field) {
            $writer->startElement('field');
            foreach ($field as $propid => $propval) {
                $writer->startElement($propid);
                $writer->text($propval);
                $writer->endElement();
            }
            $writer->endElement();
        }
        
        $writer->endElement();
        $writer->endElement();
        $writer->endDocument();
        $writer->flush();
    }

    /**
     * This is the conference session export.
     * It exports a list of participants,
     * stating which conference sessions each participant has booked.
     *
     * The function sends the resulting xlsx file directly
     * to the browser.
     *
     * @param int $eventid
     *            // (safe)
     * @return string // does not return anything, exits before
     */
    private function exportConferenceSessionsXLSX($eventid)
    {
        $xout = '';
        // safe function, parameter is checked
        if ($this->checkUserEvent($eventid)) {
            
            // uses a wonderful external library
            require_once (BASEDIR . 'lib/vendor/autoload.php');
            $spreadsheet = new \PhpOffice\PhpSpreadsheet\Spreadsheet();
            
            // setting header data for the exported file
            // ToDo: These values should probably be dynamic...
            $spreadsheet->getProperties()
                ->setCreator("fluxcess")
                ->setLastModifiedBy("fluxcess")
                ->setTitle("Gaesteliste")
                ->setSubject("Gaesteliste")
                ->setDescription("Gaesteliste, Stand " . date('d.m.Y, H:i:s'))
                ->setKeywords("gaesteliste guestlist")
                ->setCategory("gaesteliste");
            
            $fevent_data = $this->readRecord($eventid, false);
            
            $conferencesessions = array();
            $sessionOccupancy = array();
            
            /*
             * The first two columns in the export contain the guest id and name
             */
            $counter = 2;
            // headLine will be converted to the top row in the exported spreadsheet
            $headLine = array(
                'Guest Id',
                'Guest Name'
            );
            
            /*
             * read data for the title row,
             * that's the names and ids of the conference sessions
             */
            foreach ($fevent_data['conferencesession'] as $cs) {
                // if this field is not filled, the column is not exported.
                if ($cs['no'] != '') {
                    $conferencesessions[trim($cs['id'])] = $counter;
                    $headLine[] = $cs['no'] . ' | ' . $cs['title'] . ' (' . $cs['id'] . ')';
                    // initialize session occupancy (0 participants)
                    $sessionOccupancy[trim($cs['id'])] = 0;
                    $counter ++;
                }
            }
            
            // creating a list of all session choice fields
            $sessionFields = array();
            foreach ($fevent_data['formpagesection'] as $fps) {
                if ($fps['type'] == 'sessionchoice') {
                    $sessionFields[] = $fps['name'];
                }
            }
            
            // set the cursor in the spreadsheet to the top left field
            $spreadsheet->getActiveSheet()->fromArray($headLine, NULL, 'A1');
            
            // Get a list of all non-deleted guests
            $sql = "SELECT * FROM zguest" . $eventid . " WHERE deleted = 0 AND archived = 0 ORDER BY guest_id ASC;";
            $this->_pdoObj = dbconnection::getInstance();
            $pdoStatement = $this->_pdoObj->prepare($sql, array(
                PDO::ATTR_CURSOR => PDO::CURSOR_FWDONLY
            ));
            $pdoStatement->execute(array());
            
            // $i is the row number now
            $i = 2;
            // walk through all guests...
            while ($row = $pdoStatement->fetch()) {
                $i += 1;
                $writeRow = array();
                // initialize 100 columns
                for ($ii = 0; $ii < 100; $ii ++) {
                    $writeRow[$ii] = 0;
                }
                $writeRow[0] = $row['guest_id'];
                $writeRow[1] = $row['firstname'] . ' ' . $row['lastname'];
                
                // the content of sessionFields is comma separated
                foreach ($sessionFields as $msf) {
                    $chosenSessions = explode(',', $row[$msf]);
                    foreach ($chosenSessions as $chosenSession) {
                        if (trim($chosenSession) != '') {
                            // mark the session as selected for this guest
                            $writeRow[$conferencesessions[trim($chosenSession)]] = 1;
                            // increment the counter for this session
                            $sessionOccupancy[trim($chosenSession)] += 1;
                        }
                    }
                }
                
                // in the last column, add the direct rsvp link
                $writeRow[] = $this->generateDirectRSVPLink($eventid, $row['lastname'], $row['accesscode']);
                
                // write the row to the spreadsheet
                $spreadsheet->getActiveSheet()->fromArray($writeRow, NULL, 'A' . $i);
            }
            
            /**
             * the sumLine (2nd row in the spreadsheet) shows how many
             * guests have booked a specific session
             */
            $sumLine = array(
                '',
                'Summe'
            );
            foreach ($sessionOccupancy as $so) {
                $sumLine[] = $so;
            }
            $spreadsheet->getActiveSheet()->fromArray($sumLine, NULL, 'A2');
            
            // sum line is graphically emphasized
            $styleArray = array(
                'font' => array(
                    'bold' => true
                )
            );
            $spreadsheet->getActiveSheet()
            ->getStyle('A2:' . \PhpOffice\PhpSpreadsheet\Cell\Coordinate::stringFromColumnIndex($counter) . '2')
                ->applyFromArray($styleArray);
            
            ob_end_clean();
            $this->outputSpreadsheet($spreadsheet, 'conferencesessionlist_event' . $eventid . '_' . date('Y-m-d--H-i-s') . '.xlsx');
        } else {
            echo "wrong event id";
        }
        exit();
        return $xout;
    }

    private function exportGuestlistXLSX($eventid)
    {
        $xout = '';
        if ($this->checkUserEvent($eventid)) {
            require_once (BASEDIR . 'lib/vendor/autoload.php');
            $spreadsheet = new \PhpOffice\PhpSpreadsheet\Spreadsheet();
            $spreadsheet->getProperties()
                ->setCreator("fluxcess")
                ->setLastModifiedBy("fluxcess")
                ->setTitle("Gaesteliste")
                ->setSubject("Gaesteliste")
                ->setDescription("Gaesteliste, Stand " . date('d.m.Y, H:i:s'))
                ->setKeywords("gaesteliste guestlist")
                ->setCategory("gaesteliste");
            
            $importStructure = $this->getImportStructure($eventid);
            foreach ($importStructure as $fields) {
                $headLine[] = $fields['mysqlfield'];
            }
            $headLine[] = 'registrationlink';
            
            $spreadsheet->getActiveSheet()->fromArray($headLine, NULL, 'A1');
            
            $sql = "SELECT * FROM zguest" . $eventid . " WHERE deleted = 0 AND archived = 0 ORDER BY guest_id ASC;";
            $this->_pdoObj = dbconnection::getInstance();
            $pdoStatement = $this->_pdoObj->prepare($sql, array(
                PDO::ATTR_CURSOR => PDO::CURSOR_FWDONLY
            ));
            $pdoStatement->execute(array());
            $i = 1;
            while ($row = $pdoStatement->fetch()) {
                $i += 1;
                $writeRow = array();
                foreach ($importStructure as $fields) {
                    $writeRow[] = str_replace("\n", " ", str_replace("\r", " ", str_replace("\t", "", $row[$fields['mysqlfield']])));
                }
                // add direct rsvp link
                $writeRow[] = $this->generateDirectRSVPLink($eventid, $row['lastname'], $row['accesscode']);
                $spreadsheet->getActiveSheet()->fromArray($writeRow, NULL, 'A' . $i);
            }
            ob_end_clean();
            
            $this->outputSpreadsheet($spreadsheet, 'guestlist_event' . $eventid . '_' . date('Y-m-d--H-i-s') . '.xlsx');
        } else {
            echo "wrong event id";
        }
        exit();
        return $xout;
    }
    
    private function outputSpreadsheet($spreadsheet, $filename) {
        header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
        header('Content-Disposition: attachment;filename="' . $filename . '"');
        header('Cache-Control: max-age=0');
        // If you're serving to IE 9, then the following may be needed
        header('Cache-Control: max-age=1');
        
        // If you're serving to IE over SSL, then the following may be needed
        header('Expires: Mon, 26 Jul 1997 05:00:00 GMT'); // Date in the past
        header('Last-Modified: ' . gmdate('D, d M Y H:i:s') . ' GMT'); // always modified
        header('Cache-Control: cache, must-revalidate'); // HTTP/1.1
        header('Pragma: public'); // HTTP/1.0
        $objWriter = \PhpOffice\PhpSpreadsheet\IOFactory::createWriter($spreadsheet, 'Xlsx');
        $this->SaveViaTempFile($objWriter);
    }
    
    
    /**
     * This is necessary, because PhpSpreadsheet writes to /tmp, which is a problem in some host configurations
     * @param object $objWriter
     */
    private function SaveViaTempFile($objWriter){
        $filePath = session_save_path() . "/" . rand(0, getrandmax()) . rand(0, getrandmax()) . ".tmp";
        $objWriter->save($filePath);
        readfile($filePath);
        unlink($filePath);
    }

    private function exportInvoicesPayments($eventid)
    {
        $xout = '';
        if ($this->checkUserEvent($eventid)) {
            require_once (BASEDIR . 'lib/vendor/autoload.php');
            $spreadsheet = new \PhpOffice\PhpSpreadsheet\Spreadsheet();
            $spreadsheet->getProperties()
                ->setCreator("fluxcess")
                ->setLastModifiedBy("fluxcess")
                ->setTitle("Gaesteliste")
                ->setSubject("Zahlungsliste")
                ->setDescription("Zahlungsliste, Stand " . date('d.m.Y, H:i:s'))
                ->setKeywords("gaesteliste guestlist")
                ->setCategory("gaesteliste");
            
            $importStructure = $this->getImportStructure($eventid);
            $headLine = array(
                gettext('Timestamp'),
                gettext('Guest Id'),
                gettext('Salutation'),
                gettext('First Name'),
                gettext('Last Name'),
                gettext('Invoice Amount'),
                gettext('Payment Status'),
                gettext('Payment Note'),
                gettext('Payment Note Timestamp'),
                gettext('Guest Deleted')
            );
            $spreadsheet->getActiveSheet()->fromArray($headLine, NULL, 'A1');
            
            $sql = "SELECT * FROM zguest" . $eventid . " WHERE archived = 0 ORDER BY guest_id ASC;";
            $this->_pdoObj = dbconnection::getInstance();
            $pdoStatement = $this->_pdoObj->prepare($sql, array(
                PDO::ATTR_CURSOR => PDO::CURSOR_FWDONLY
            ));
            $pdoStatement->execute(array());
            $i = 1;
            $pms = array();
            while ($row = $pdoStatement->fetch()) {
                
                $g = new entityGuest($eventid, $row['guest_id'], null, true);
                $gd = $g->getData(true);
                if (count($pms) == 0) {
                    $pms = $g->getPaymentStatusList();
                }
                foreach ($gd['invoices'] as $inv) {
                    $i += 1;
                    $writeRow = array();
                    $writeRow[] = $inv['ts'];
                    $writeRow[] = $inv['guest_id'];
                    $writeRow[] = $gd['salutation'];
                    $writeRow[] = $gd['firstname'];
                    $writeRow[] = $gd['lastname'];
                    $writeRow[] = $inv['room'];
                    $writeRow[] = $pms[$inv['paymentstatus']];
                    $writeRow[] = $inv['paymentcomment'];
                    $writeRow[] = $inv['paymenttimestamp'];
                    $writeRow[] = $gd['deleted'];
                    // $writeRow[] = str_replace("\n", " ", str_replace("\r", " ", str_replace("\t", "", $row[$fields['mysqlfield']])));
                    $spreadsheet->getActiveSheet()->fromArray($writeRow, NULL, 'A' . $i);
                }
                // add direct rsvp link
                // $writeRow[] = $this->generateDirectRSVPLink($eventid, $row['lastname'], $row['accesscode']);
            }
            ob_end_clean();
            $this->outputSpreadsheet($spreadsheet, 'invoicelist_event' . $eventid . '_' . date('Y-m-d--H-i-s') . '.xlsx');
        } else {
            echo "wrong event id";
        }
        exit();
        return $xout;
    }
    

    private function readVisitorStatistics($eventid)
    {
        /*
         * Format:
         * maximum number of concurrent guests:
         * $day['20141012']['00']['max'] = 51
         * minimum number of concurrent guests:
         * $day['20141012']['00']['min'] = 0
         * how many people entered during a certain hour
         * $day['20141012']['00']['entered'] = 51
         * how many people left during a certain hour
         * $day['20141012']['00']['left'] = 51
         */
        $day = array();
        if ($this->checkUserEvent($eventid)) {
            $days = $this->getDaysLogged($eventid);
            
            foreach ($days as $d) {
                $totalvisitors = 0;
                $newVisitors = array();
                for ($i = 0; $i < 24; $i ++) {
                    $day[$d][$i]['max'] = 0;
                    $day[$d][$i]['entered'] = 0;
                    $day[$d][$i]['left'] = 0;
                    $day[$d][$i]['new'] = 0;
                }
                $day[$d]['max'] = 0;
                $day[$d]['entered'] = 0;
                $day[$d]['new'] = 0;
                $sql = "SELECT
						guest_id,
						`inout`,
						DATE_FORMAT(vl.ts, '%k') hour
						FROM xvl" . $eventid . " vl
								WHERE
								DATE(vl.ts) = :day
								ORDER BY vl.ts ASC
								";
                $sqlArr = array(
                    ':day' => $d
                );
                $this->_pdoObj = dbconnection::getInstance();
                $pdoStatement = $this->_pdoObj->prepare($sql, array(
                    PDO::ATTR_CURSOR => PDO::CURSOR_FWDONLY
                ));
                $pdoStatement->execute($sqlArr);
                if ($pdoStatement->errorCode() * 1 > 0) {
                    $this->addError('', gettext('SQL Error'), 1, __LINE__ . print_r($pdoStatement->errorInfo(), true));
                }
                $myHour = 0;
                $lastHour = 0;
                while ($row = $pdoStatement->fetch()) {
                    $hour = $row['hour'];
                    
                    if ($hour != $myHour) {
                        while ($hour != $myHour) {
                            $myHour += 1;
                            $day[$d][$myHour]['entered'] = 0;
                            $day[$d][$myHour]['left'] = 0;
                            $day[$d][$myHour]['max'] = $totalvisitors;
                            $day[$d][$myHour]['min'] = $totalvisitors;
                            $day[$d][$myHour]['new'] = $day[$d]['new'];
                        }
                    }
                    if ($hour != $lastHour) {
                        $myHour = $hour;
                        $lastHour = $hour;
                        // for ($k = $lastHour+1; $k <= $hour; $k++) {
                        $day[$d][$hour]['new'] = $day[$d]['new'];
                        // }
                        // echo 'UP ';
                    }
                    if (! isset($day[$d][$hour]['min'])) {
                        $day[$d][$hour]['min'] = $totalvisitors;
                    }
                    if ($row['inout'] == 'in') {
                        $totalvisitors += 1;
                        $day[$d][$hour]['entered'] += 1;
                        $day[$d]['entered'] += 1;
                    } elseif ($row['inout'] == 'out') {
                        $totalvisitors -= 1;
                        $day[$d][$hour]['left'] += 1;
                    }
                    if (! isset($newVisitors[$row['guest_id']]) && $newVisitors[$row['guest_id']] != 1) {
                        $newVisitors[$row['guest_id']] = 1;
                        $day[$d][$hour]['new'] += 1;
                        $day[$d]['new'] += 1;
                    }
                    if ($day[$d][$hour]['max'] < $totalvisitors) {
                        $day[$d][$hour]['max'] = $totalvisitors;
                    }
                    if ($day[$d][$hour]['min'] > $totalvisitors) {
                        $day[$d][$hour]['min'] = $totalvisitors;
                    }
                    if ($day[$d]['max'] < $totalvisitors) {
                        $day[$d]['max'] = $totalvisitors;
                    }
                }
            }
        }
        
        echo '<style>td { text-align: right; vertical-align: top;}</style><table border="1"><tr><td></td>';
        for ($i = 0; $i < 24; $i ++) {
            echo '<th>';
            if ($i < 10) {
                $j = '0' . $i;
            } else {
                $j = $i;
            }
            echo $j . '</th>';
        }
        echo '<th>Total</th></tr>';
        if (is_array($day)) {
            foreach ($day as $d => $xd) {
                
                echo '<tr><th>' . $d . '</th>';
                if (is_array($xd)) {
                    foreach ($xd as $a => $f0) {
                        if (is_numeric($a)) {
                            echo '<td>' . $f0['max'] . '<br />' . $f0['min'] . '<br />' . $f0['new'] . '<br />+' . $f0['entered'] . '<br />-' . $f0['left'] . '</td>';
                        }
                    }
                    echo '<td>' . $xd['max'] . '<br /><br />' . $xd['new'] . '<br />+' . $xd['entered'] . '</td>' . '</tr>';
                } else {
                    echo 'Kein array:' . print_r($xd, true);
                }
            }
        }
        echo '</table>';
    }

    private function getDaysLogged($eventid)
    {
        $days = array();
        if ($this->checkUserEvent($eventid)) {
            $sql = "SELECT DATE_FORMAT(vl.ts, '%Y-%m-%d') checkinday
					FROM xvl" . $eventid . " vl
							GROUP BY checkinday
							ORDER BY vl.ts ASC
							;";
            $this->_pdoObj = dbconnection::getInstance();
            $pdoStatement = $this->_pdoObj->prepare($sql, array(
                PDO::ATTR_CURSOR => PDO::CURSOR_FWDONLY
            ));
            $pdoStatement->execute(array());
            if ($pdoStatement->errorCode() * 1 > 0) {
                $this->addError('', gettext('SQL Error'), 1, __LINE__ . print_r($pdoStatement->errorInfo(), true));
            }
            while ($row = $pdoStatement->fetch()) {
                $days[] = $row[checkinday];
            }
        }
        return $days;
    }

    private function exportVisitorLogXLSX($eventid)
    {
        $xout = '';
        if ($this->checkUserEvent($eventid)) {
            $this->readVisitorStatistics($eventid);
            require_once (BASEDIR . 'lib/vendor/autoload.php');
            $spreadsheet = new \PhpOffice\PhpSpreadsheet\Spreadsheet();
            
            $spreadsheet->getProperties()
                ->setCreator("fluxcess")
                ->setLastModifiedBy("fluxcess")
                ->setTitle("Visitor Log")
                ->setSubject("Visitor Log")
                ->setDescription("Visitor Log, Stand " . date('d.m.Y, H:i:s'))
                ->setKeywords("gaesteliste guestlist")
                ->setCategory("gaesteliste");
            
            $headLine = array(
                'time',
                'salutation',
                'firstname',
                'lastname',
                'company',
                'guest_id',
                'direction',
                'ip'
            );
            $spreadsheet->getActiveSheet()->fromArray($headLine, NULL, 'A1');
            
            $sql = "SELECT vl.ts, vl.inout, vl.ip,
					g.salutation, g.firstname, g.lastname, g.company, g.guest_id
					FROM xvl" . $eventid . " vl
							JOIN zguest" . $eventid . " g ON vl.guest_id = g.guest_id
									WHERE deleted = 0 AND archived = 0 ORDER BY vl.ts DESC;";
            $this->_pdoObj = dbconnection::getInstance();
            $pdoStatement = $this->_pdoObj->prepare($sql, array(
                PDO::ATTR_CURSOR => PDO::CURSOR_FWDONLY
            ));
            $pdoStatement->execute(array());
            $i = 1;
            while ($row = $pdoStatement->fetch()) {
                $i += 1;
                $writeRow = array(
                    $row['ts'],
                    $row['salutation'],
                    $row['firstname'],
                    $row['lastname'],
                    $row['company'],
                    $row['guest_id'],
                    $row['inout'],
                    $row['ip']
                );
                $spreadsheet->getActiveSheet()->fromArray($writeRow, NULL, 'A' . $i);
            }
            ob_end_clean();
            $this->outputSpreadsheet($spreadsheet, 'visitorlog_event' . $eventid . '_' . date('Y-m-d--H-i-s') . '.xlsx');
        } else {
            echo "wrong event id";
        }
        exit();
        return $xout;
    }

    private function getGuestlistNewFeedback($eventid)
    {
        $rows = array();
        $sql = "select zg.*,
					(SELECT row_id FROM zguest" . $eventid . " WHERE guest_id = zg.guest_id AND row_id < zg.row_id ORDER BY row_id DESC LIMIT 1) pre_row_id,
							registered-(SELECT registered FROM zguest" . $eventid . " WHERE guest_id = zg.guest_id AND row_id < zg.row_id ORDER BY row_id DESC LIMIT 1) diff_registered,
									cancelled-(SELECT cancelled FROM zguest" . $eventid . " WHERE guest_id = zg.guest_id AND row_id < zg.row_id ORDER BY row_id DESC LIMIT 1) diff_cancelled
											from zguest" . $eventid . " zg
													WHERE
													(
													registered <> (SELECT registered FROM zguest" . $eventid . " WHERE guest_id = zg.guest_id AND row_id < zg.row_id ORDER BY row_id DESC LIMIT 1)
															OR cancelled <> (SELECT cancelled FROM zguest" . $eventid . " WHERE guest_id = zg.guest_id AND row_id < zg.row_id ORDER BY row_id DESC LIMIT 1)
																	)
																	ORDER BY changetime DESC
																	";
        $this->_pdoObj = dbconnection::getInstance();
        $pdoStatement = $this->_pdoObj->prepare($sql, array(
            PDO::ATTR_CURSOR => PDO::CURSOR_FWDONLY
        ));
        $pdoStatement->execute(array());
        while ($row = $pdoStatement->fetch()) {
            $rows[] = $row;
        }
        return $rows;
    }

    private function exportGuestlistNewFeedback($eventid)
    {
        if ($this->checkUserEvent($eventid)) {
            $fb = $this->getNewFeedbackSince();
            $importStructure = $this->getImportStructure($eventid);
            $xout = "\xEF\xBB\xBF";
            $xout .= "changetime\tnewRegistered\tnewCancelled\t";
            foreach ($importStructure as $fields) {
                $xout .= $fields['mysqlfield'] . "\t";
            }
            $xout .= 'directlink' . "\t";
            $xout = substr($xout, 0, - 1) . "\r\n";
            $rows = $this->getGuestlistNewFeedback($eventid);
            foreach ($rows as $row) {
                $xout .= $row['changetime'] . "\t" . $row['diff_registered'] . "\t" . $row['diff_cancelled'] . "\t";
                foreach ($importStructure as $fields) {
                    $xout .= str_replace("\n", " ", str_replace("\r", " ", str_replace("\t", "", $row[$fields['mysqlfield']]))) . "\t";
                }
                // add direct rsvp link
                $xout .= $this->generateDirectRSVPLink($eventid, $row['lastname'], $row['accesscode']);
                $xout .= "\t";
                $xout = substr($xout, 0, - 1) . "\r\n";
            }
        }
        return $xout;
    }

    private function exportGuestlistNewFeedbackXLSX($eventid)
    {
        $xout = '';
        if ($this->checkUserEvent($eventid)) { 
            $importStructure = $this->getImportStructure($eventid);
            $this->getGuestlistNewFeedback($eventid);
            require_once (BASEDIR . 'lib/vendor/autoload.php');
            $spreadsheet = new \PhpOffice\PhpSpreadsheet\Spreadsheet();
            
            $spreadsheet->getProperties()
                ->setCreator("fluxcess")
                ->setLastModifiedBy("fluxcess")
                ->setTitle("Booking Log")
                ->setSubject("Booking Log")
                ->setDescription("Booking log, " . date('d.m.Y, H:i:s'))
                ->setKeywords("gaesteliste guestlist")
                ->setCategory("gaesteliste");
            
                       
            $headLine = array(
                'changetime',
                'newRegistered',
                'newCancelled'
            );
            
            foreach ($importStructure as $fields) {
                $headLine[] = $fields['mysqlfield'];
            }
            $headLine[] = 'directlink';
            $spreadsheet->getActiveSheet()->fromArray($headLine, NULL, 'A1');
            
            $rows = $this->getGuestlistNewFeedback($eventid);
            $i = 1;
            $writeRow = array();
            foreach ($rows as $row) {
                $i += 1;
                $writeRow[] = $row['changetime'];
                $writeRow[] = $row['diff_registered'];
                $writeRow[] = $row['diff_cancelled'];
                foreach ($importStructure as $fields) {
                    $writeRow[] = $row[$fields['mysqlfield']];
                }
                $writeRow[] = $this->generateDirectRSVPLink($eventid, $row['lastname'], $row['accesscode']);
                $spreadsheet->getActiveSheet()->fromArray($writeRow, NULL, 'A' . $i);
            }
            ob_end_clean();
            $this->outputSpreadsheet($spreadsheet, 'visitorlog_event' . $eventid . '_' . date('Y-m-d--H-i-s') . '.xlsx');
        } else {
            echo "wrong event id";
        }
        exit();
        return $xout;
    }

    private function getGuestlistWithLiveStatus($eventid)
    {
        $rows = array();
        $sql = "SELECT zg.*,
					(IF (ygr.pax IS NULL,0,ygr.pax)) room1
					FROM zguest" . $eventid . " zg
							JOIN room r ON zg.fevent_id = r.fevent_id
							LEFT JOIN `ygr" . $eventid . "` ygr ON r.room_id = ygr.room_id AND ygr.guest_id = zg.guest_id
									WHERE zg.deleted = 0 AND zg.archived = 0
									ORDER BY zg.guest_id ASC
									;";
        $this->_pdoObj = dbconnection::getInstance();
        $pdoStatement = $this->_pdoObj->prepare($sql, array(
            PDO::ATTR_CURSOR => PDO::CURSOR_FWDONLY
        ));
        $pdoStatement->execute(array());
        while ($row = $pdoStatement->fetch()) {
            $rows[] = $row;
        }
        return $rows;
    }

    private function exportGuestlistWithLiveStatus($eventid)
    {
        if ($this->checkUserEvent($eventid)) {
            $importStructure = $this->getImportStructure($eventid);
            $xout = "\xEF\xBB\xBF";
            foreach ($importStructure as $fields) {
                $xout .= $fields['mysqlfield'] . "\t";
            }
            $xout .= "room1\tdirectlink\t";
            $xout = substr($xout, 0, - 1) . "\r\n";
            $rows = $this->getGuestlistWithLiveStatus($eventid);
            foreach ($rows as $row) {
                // $res[] = $rw;
                foreach ($importStructure as $fields) {
                    $xout .= str_replace("\n", " ", str_replace("\r", " ", str_replace("\t", "", $row[$fields['mysqlfield']]))) . "\t";
                }
                $xout .= $row['room1'] . "\t";
                // add direct rsvp link
                $xout .= $this->generateDirectRSVPLink($eventid, $row['lastname'], $row['accesscode']);
                $xout .= "\t";
                $xout = substr($xout, 0, - 1) . "\r\n";
            }
        }
        return $xout;
    }

    private function exportGuestlistWithLiveStatusXLSX($eventid)
    {
        $xout = '';
        if ($this->checkUserEvent($eventid)) {
            $importStructure = $this->getImportStructure($eventid);
            
            require_once (BASEDIR . 'lib/vendor/autoload.php');
            $spreadsheet = new \PhpOffice\PhpSpreadsheet\Spreadsheet();
            
            $spreadsheet->getProperties()
                ->setCreator("fluxcess")
                ->setLastModifiedBy("fluxcess")
                ->setTitle("Live Export")
                ->setSubject("Live Export")
                ->setDescription("Booking log, " . date('d.m.Y, H:i:s'))
                ->setKeywords("gaesteliste guestlist")
                ->setCategory("gaesteliste");
            
            $headLine = array();
            
            foreach ($importStructure as $fields) {
                $headLine[] = $fields['mysqlfield'];
            }
            $headLine[] = 'room 1';
            $spreadsheet->getActiveSheet()->fromArray($headLine, NULL, 'A1');
            
            $rows = $this->getGuestlistWithLiveStatus($eventid);
            $i = 1;
            
            foreach ($rows as $row) {
                $writeRow = array();
                $i += 1;
                foreach ($importStructure as $fields) {
                    $writeRow[] = $row[$fields['mysqlfield']];
                }
                $writeRow[] = $row['room1'];
                $spreadsheet->getActiveSheet()->fromArray($writeRow, NULL, 'A' . $i);
            }
            ob_end_clean();
            $this->outputSpreadsheet($spreadsheet, 'live_event' . $eventid . '_' . date('Y-m-d--H-i-s') . '.xlsx');
        } else {
            echo "wrong event id";
        }
        exit();
        return $xout;
    }

    private function getGuestlistWithWasHereStatus($eventid)
    {
        $rows = array();
        $sql = "SELECT zg.*,
					IF(vl.guest_id IS NULL,0,1) washere
					FROM zguest" . $eventid . " zg
							LEFT JOIN `xvl" . $eventid . "` vl ON vl.guest_id = zg.guest_id
									WHERE zg.deleted = 0 AND zg.archived = 0
									GROUP BY zg.guest_id
									ORDER BY zg.guest_id ASC
									;";
        $this->_pdoObj = dbconnection::getInstance();
        $pdoStatement = $this->_pdoObj->prepare($sql, array(
            PDO::ATTR_CURSOR => PDO::CURSOR_FWDONLY
        ));
        $pdoStatement->execute(array());
        while ($row = $pdoStatement->fetch()) {
            $rows[] = $row;
        }
        return $rows;
    }

    private function exportGuestlistWithWasHereStatus($eventid)
    {
        if ($this->checkUserEvent($eventid)) {
            $importStructure = $this->getImportStructure($eventid);
            $xout = "\xEF\xBB\xBF";
            foreach ($importStructure as $fields) {
                $xout .= $fields['mysqlfield'] . "\t";
            }
            $xout .= "directlink\twashere\t";
            $xout = substr($xout, 0, - 1) . "\r\n";
            $rows = $this->getGuestlistWithWasHereStatus($eventid);
            foreach ($rows as $row) {
                // $res[] = $rw;
                foreach ($importStructure as $fields) {
                    $xout .= str_replace("\n", " ", str_replace("\r", " ", str_replace("\t", "", $row[$fields['mysqlfield']]))) . "\t";
                }
                // add direct rsvp link
                $xout .= $this->generateDirectRSVPLink($eventid, $row['lastname'], $row['accesscode']);
                $xout .= "\t" . $row['washere'] . "\t";
                $xout = substr($xout, 0, - 1) . "\r\n";
            }
        }
        return $xout;
    }

    private function exportGuestlistWithWasHereStatusXLSX($eventid)
    {
        $xout = '';
        if ($this->checkUserEvent($eventid)) {
            $importStructure = $this->getImportStructure($eventid);
            
            
            require_once (BASEDIR . 'lib/vendor/autoload.php');
            $spreadsheet = new \PhpOffice\PhpSpreadsheet\Spreadsheet();
            
            $spreadsheet->getProperties()
                ->setCreator("fluxcess")
                ->setLastModifiedBy("fluxcess")
                ->setTitle("Export")
                ->setSubject("Export")
                ->setDescription("Booking log, " . date('d.m.Y, H:i:s'))
                ->setKeywords("gaesteliste guestlist")
                ->setCategory("gaesteliste");
            
            $headLine = array();
            
            foreach ($importStructure as $fields) {
                $headLine[] = $fields['mysqlfield'];
            }
            $headLine[] = 'checked in';
            $spreadsheet->getActiveSheet()->fromArray($headLine, NULL, 'A1');
            
            $rows = $this->getGuestlistWithWasHereStatus($eventid);
            $i = 1;
            
            foreach ($rows as $row) {
                $writeRow = array();
                $i += 1;
                foreach ($importStructure as $fields) {
                    $writeRow[] = $row[$fields['mysqlfield']];
                }
                $writeRow[] = $row['washere'];
                $spreadsheet->getActiveSheet()->fromArray($writeRow, NULL, 'A' . $i);
            }
            ob_end_clean();
            $this->outputSpreadsheet($spreadsheet, 'checked_in_event' . $eventid . '_' . date('Y-m-d--H-i-s') . '.xlsx');
        } else {
            echo 'wrong event id';
        }
        exit();
        return $xout;
    }

    private function generateDirectRSVPLink($eventid, $lastname, $accesscode)
    {
        $lnk = '';
        if (strlen($lastname) > 1 && strlen($accesscode) > 3) {
            $lnk = 'https://' . $this->_rsvpDomain . '/qr/' . $eventid . '/' . $lastname . '/' . $accesscode;
        }
        return $lnk;
    }

    /**
     * Reads the import- and export structure from the columns table
     * enables the user to dynamically influence the import / export structure
     *
     * @param int $eventid
     */
    private function getImportStructure($eventid)
    {
        $importStructure = Array();
        if ($this->checkUserEvent($eventid)) {
            $sql = "SELECT fieldname FROM `zcolumn" . $eventid . "`
					WHERE exportorder IS NOT NULL AND deleted = 0
					ORDER BY exportorder ASC;";
            $this->_pdoObj = dbconnection::getInstance();
            $pdoStatement = $this->_pdoObj->prepare($sql, array(
                PDO::ATTR_CURSOR => PDO::CURSOR_FWDONLY
            ));
            $pdoStatement->execute(array());
            $counter = 1;
            while ($row = $pdoStatement->fetch()) {
                $f = array(
                    'xlscol' => $counter,
                    'mysqlfield' => $row['fieldname']
                );
                $importStructure[] = $f;
                $counter += 1;
            }
        }
        return $importStructure;
    }

    private function printChangeForm()
    {
        // check if guest id is numeric
        if (! is_numeric($this->_ps['field_id'])) {
            $this->addError('', gettext('Invalid field id.'));
        }
        // check if event id is numeric
        if (! is_numeric($this->_ps['fevent_id'])) {
            $this->addError('', gettext('Invalid event id') . '.');
        }
        // check if event belongs to user
        $ev = new event($this->_lg, $this->_locale);
        if (! $ev->checkUserEvent($this->_ps['fevent_id'])) {
            $this->addError('', gettext('Invalid event id') . ': ' . $this->_ps['fevent_id']);
        }
        
        if ($this->_ps['field_id'] != 0) {
            $fielddata = $this->getFieldData($this->_ps['fevent_id'], $this->_ps['field_id']);
        } else {
            $fielddata = Array();
            $fielddata['fieldtypeeasy'] = 1;
        }
        foreach ($this->_ft as $key => $val) {
            $ftval[] = $key;
            $ftout[] = $val[0];
        }
        
        foreach ($this->_fdt as $key => $val) {
            $fdtval[] = $key;
            $fdtout[] = $val[0];
        }
        
        $pfval[] = '0';
        $pfout[] = '-';
        $myFields = $this->getFields($this->_ps['fevent_id'], 'NOTstandard');
        foreach ($myFields as $f) {
            if ($f['field_id'] != $this->_ps['field_id']) {
                $pfval[] = $f['field_id'];
                $pfout[] = $f['fieldname'];
            }
        }
        
        $sm = new L5Smarty();
        $sm->assign('ps', $fielddata);
        $sm->assign('fevent_id', $this->_ps['fevent_id']);
        $sm->assign('field_id', $this->_ps['field_id']);
        $sm->assign('fteasyval', $ftval);
        $sm->assign('fteasy', $ftout);
        $sm->assign('fdtval', $fdtval);
        $sm->assign('fdtout', $fdtout);
        $sm->assign('pfval', $pfval);
        $sm->assign('pfout', $pfout);
        $xout = $sm->fetch('event/changefieldform.tpl');
        
        return $xout;
    }

    private function getFieldData($eventid, $fieldid)
    {
        // check if guest id is numeric
        if (! is_numeric($fieldid)) {
            $this->addError('', gettext('Invalid field id.'));
        }
        // check if event id is numeric
        if (! is_numeric($eventid)) {
            $this->addError('', gettext('Invalid event id') . '.');
        }
        // check if event belongs to user
        $ev = new event($this->_lg, $this->_locale);
        if (! $ev->checkUserEvent($eventid)) {
            $this->addError('', gettext('Invalid event id') . ': ' . $eventid);
        }
        // read guest data
        $tablename = 'zcolumn' . $eventid;
        $sql = "SELECT * FROM " . $tablename . " WHERE field_id = :fieldid;";
        $this->_pdoObj = dbconnection::getInstance();
        $pdoStatement = $this->_pdoObj->prepare($sql, array(
            PDO::ATTR_CURSOR => PDO::CURSOR_FWDONLY
        ));
        $pdoStatement->execute(array(
            ':fieldid' => $fieldid
        ));
        if ($pdoStatement->errorCode() * 1 != 0) {
            $this->addError('', gettext('SQL Error') . print_r($pdoStatement->errorInfo(), true), 1);
        } elseif ($pdoStatement->rowCount() == 0) {
            $this->addError('', gettext('SQL Error with error code 0'), 1, __LINE__ . 'SQL Fehler mit Errorcode 0: ' . print_r($pdoStatement->errorInfo(), true));
        } else {
            $guestData = $pdoStatement->fetch();
        }
        
        $guestData['fieldtypeeasy'] = 0;
        foreach ($this->_ft as $ftkey => $val) {
            if ($val[1] == $guestData['fieldtype'] && $val[2] == $guestData['fieldlength']) {
                $guestData['fieldtypeeasy'] = $ftkey;
            }
        }
        if ($guestData['fieldtypeeasy'] == 0 && is_numeric($guestData['fieldtype']) && $guestData['fieldtype'] > 0) {
            $guestData['fieldtypeeasy'] = $guestData['fieldtype'];
        }
        
        return $guestData;
    }

    private function saveFieldData($createFromTemplate = false)
    {
        $id = $this->_ps['field_id'];
        $eventid = $this->_ps['fevent_id'];
        // check if user wants to create an existing field
        if ($createFromTemplate === false) {
            $listOfFields = $this->getFields($eventid, 'ALL');
        } else {
            $listOfFields = array();
        }
        if ($this->_ps['field_id'] == 0) {
            foreach ($listOfFields as $f) {
                if ($f['fieldname'] == $this->_ps['fieldname']) {
                    if ($f['deleted'] == 1) {
                        $this->addError('fieldname', gettext('A field with this name has existed before for this event. Please choose another field name.'));
                    } else {
                        $this->addError('fieldname', gettext('A field with this name exists already for this event. Please choose another field name.'));
                    }
                }
            }
        }
        if (count($this->_err) == 0) {
            $this->_pdoObj = dbconnection::getInstance();
            if ($createFromTemplate === false) {
                $this->_pdoObj->beginTransaction();
            }
            try {
                /* Handle Checkbox Fields */
                if ($this->_ps['isdefaultvalue'] != 1) {
                    $this->_ps['isdefaultvalue'] = 0;
                }
                if ($this->_ps['showtoguest'] != 1) {
                    $this->_ps['showtoguest'] = 0;
                }
                if ($this->_ps['editbyguest'] != 1) {
                    $this->_ps['editbyguest'] = 0;
                }
                if ($this->_ps['editbymanager'] != 1) {
                    $this->_ps['editbymanager'] = 0;
                }
                /* End of handling checkbox fields */
                
                $fields = array(
                    'fieldname',
                    'fieldtype',
                    'fieldlength',
                    'note',
                    'editbyguest',
                    'showtoguest',
                    'editbymanager',
                    'longcontent',
                    'formlabel',
                    'radiocheckvalue',
                    'fielddisplaytype',
                    'parent_field_id',
                    'isdefaultvalue'
                );
                $templatefields = array(
                    'field_id',
                    'fieldorder',
                    'pageno',
                    'sectionno',
                    'exportorder',
                    'fielddefault',
                    'syscolumn',
                    'syshidden',
                    'deleted'
                );
                $fieldsSys = array(
                    'editbyguest',
                    'showtoguest',
                    'editbymanager',
                    'formlabel'
                );
                if (! is_numeric($id)) {
                    $this->addError('', gettext('Invalid record id') . ': ' . $id);
                }
                
                if (! is_numeric($eventid) || ! $this->checkUserEvent($eventid)) {
                    $this->addError('', gettext('Invalid event id') . ': ' . $eventid);
                }
                if (count($this->_err) == 0) {
                    // set fields...
                    $sqlF = '';
                    
                    // is this a syscolumn?
                    $isSysColumn = true;
                    // new?
                    if ($this->_ps['field_id'] == 0) {
                        $sql = "INSERT INTO `zcolumn" . $eventid . "`
								SET ";
                        $isSysColumn = false;
                    } elseif ($createFromTemplate === true) {
                        $sql = "INSERT INTO `zcolumn" . $eventid . "`
								SET ";
                        $isSysColumn = $this->_ps['syscolumn'];
                    } else {
                        // check if no syscol, and not deleted - and for field type
                        $sql = "SELECT syscolumn, deleted, fieldname, fieldtype FROM `zcolumn" . $eventid . "`
								WHERE field_id = :fieldid;";
                        $this->_pdoObj = dbconnection::getInstance();
                        $pdoStatement = $this->_pdoObj->prepare($sql, array(
                            PDO::ATTR_CURSOR => PDO::CURSOR_FWDONLY
                        ));
                        $pdoStatement->execute(array(
                            'fieldid' => $id
                        ));
                        if ($pdoStatement->errorCode() * 1 != 0) {
                            $this->addError('', gettext('SQL Error'), 1, __LINE__ . print_r($pdoStatement->errorInfo(), true));
                        } else {
                            $row = $pdoStatement->fetch();
                            $oldcolname = $row['fieldname'];
                            if ($row['syscolumn'] == 0) {
                                $isSysColumn = false;
                                // $this->addError('', 'This field is a system default field and cannot be changed.');
                            }
                            if ($row['deleted'] == 1) {
                                $this->addError('', gettext('This field has been deleted and does not exist anymore.'));
                            }
                            // check if the column is subject to be renamed. if yes...
                            // check, if the new column name exists already.
                            if ($oldcolname != $this->_ps['fieldname']) {
                                foreach ($listOfFields as $f) {
                                    if ($f['fieldname'] == $this->_ps['fieldname']) {
                                        if ($f['deleted'] == 1) {
                                            $this->addError('fieldname', gettext('You have tried to rename the field. A field with your new suggestion for the field name has existed already, and has been deleted. Please choose another field name.'));
                                        } else {
                                            $this->addError('fieldname', gettext('You have tried to rename the field. A field with your new suggestion for the field name exists already. Please choose another field name.'));
                                        }
                                    }
                                }
                            }
                        }
                        
                        if (count($this->_err) == 0) {
                            // change? (only if no syscolumn and not deleted)
                            $sql = "UPDATE zcolumn" . $eventid . " SET ";
                        }
                    }
                    
                    // Add contents
                    if ($createFromTemplate === true) {
                        $nullableCols = array(
                            'fieldorder',
                            'exportorder'
                        );
                        foreach ($this->_ps as $fieldname => $fieldvalue) {
                            if (array_search($fieldname, $fields) !== false || array_search($fieldname, $fieldsSys) !== false || array_search($fieldname, $templatefields) !== false) {
                                if ($fieldname != 'field_id') {
                                    if ($fieldvalue == '' && array_search($fieldname, $nullableCols) !== false) {
                                        $sqlF .= $fieldname . " = :" . $fieldname . ", ";
                                        $sqlArr[$fieldname] = null;
                                    } else {
                                        $sqlF .= $fieldname . " = :" . $fieldname . ", ";
                                        $sqlArr[$fieldname] = $fieldvalue;
                                    }
                                }
                            }
                        }
                    } elseif ($isSysColumn == false || $isSysColumn == 0) {
                        foreach ($this->_ps as $fieldname => $fieldvalue) {
                            if (array_search($fieldname, $fields) !== false) {
                                $sqlF .= $fieldname . " = :" . $fieldname . ", ";
                                $sqlArr[$fieldname] = $fieldvalue;
                            }
                        }
                    } else {
                        foreach ($this->_ps as $fieldname => $fieldvalue) {
                            if (array_search($fieldname, $fieldsSys) !== false) {
                                $sqlF .= $fieldname . " = :" . $fieldname . ", ";
                                $sqlArr[$fieldname] = $fieldvalue;
                            }
                        }
                    }
                    
                    if (($isSysColumn == false || $isSysColumn == 0) || $createFromTemplate === true) {
                        // check field name
                        $this->_ps['fieldname'] = trim($this->_ps['fieldname']);
                        if (strlen($this->_ps['fieldname']) < 3) {
                            $this->addError('fieldname', gettext('The field name needs to consist of at least 3 characters.'));
                        } else {
                            if (preg_match("/[^a-z0-9]/", $this->_ps['fieldname'])) {
                                $this->addError('fieldname', gettext('The fieldname contains invalid characters. Only lower capital letters and numbers are allowed.'));
                            }
                        }
                        
                        // check field type
                        $this->_ps['fieldtypeeasy'] = trim($this->_ps['fieldtypeeasy']);
                        if (! is_numeric($this->_ps['fieldtypeeasy']) && $createFromTemplate === false) {
                            $this->addError('fieldtypeeasy', gettext('Invalid field type selected'));
                        } elseif ($createFromTemplate === true) {
                            // $sqlArr['fieldtype'] = $this->_ps['fieldtype'];
                            // $sqlF .= " fieldtype = :fieldtype, ";
                            // $sqlF .= " fieldlength = :fieldlength, ";
                            // $sqlArr['fieldlength'] = $this->_ps['fieldlength'];
                        } else {
                            // convert
                            if ($this->_ps['fieldtypeeasy'] == 0) {
                                $this->addError('fieldtypeeasy', gettext('The type "Spezial" is only valid for system fields.'));
                            } elseif ($this->_ps['fieldtypeeasy'] > 0 && $this->_ps['fieldtypeeasy'] < count($this->_ft)) {
                                
                                $fieldtypeno = $this->_ps['fieldtypeeasy'];
                                if (isset($this->_ft[$fieldtypeno][1])) {
                                    $this->_ps['fieldtype'] = $this->_ft[$fieldtypeno][1];
                                    $sqlArr['fieldtype'] = $this->_ps['fieldtype'];
                                    $sqlF .= " fieldtype = :fieldtype, ";
                                    $this->_ps['fieldlength'] = $this->_ft[$fieldtypeno][2];
                                    $sqlF .= " fieldlength = :fieldlength, ";
                                    $sqlArr['fieldlength'] = $this->_ps['fieldlength'];
                                } else {
                                    $this->_ps['fieldtype'] = $fieldtypeno;
                                    $sqlArr['fieldtype'] = $fieldtypeno;
                                    $sqlF .= " fieldtype = :fieldtype, ";
                                }
                                $this->_ps['fieldlength'] = $this->_ft[$fieldtypeno][2];
                            }
                        }
                    }
                    
                    $sql .= substr($sqlF, 0, - 2);
                    if ($this->_ps['field_id'] > 0 && $createFromTemplate === false) {
                        $sql .= " WHERE field_id = :id;";
                        $sqlArr['id'] = $id;
                    } elseif ($this->_ps['field_id'] > 0 && $createFromTemplate === true) {
                        $sql .= ", field_id = :id;";
                        $sqlArr['id'] = $this->_ps['field_id'];
                    } else {
                        $sql .= ';';
                    }
                    if (count($this->_err) == 0) {
                        $pdoStatement = $this->_pdoObj->prepare($sql, array(
                            PDO::ATTR_CURSOR => PDO::CURSOR_FWDONLY
                        ));
                        $pdoStatement->execute($sqlArr);
                        if ($pdoStatement->errorCode() * 1 != 0) {
                            $this->addError('', 'SQL Fehler' . print_r($pdoStatement->errorInfo(), true), 1);
                        } else {
                            // $fieldid = $this->_pdoObj->lastInsertId();
                        }
                        // add column to guest table // change column in guest table
                        if (($isSysColumn == 0 && ! is_numeric($this->_ps['fieldtype'])) || $createFromTemplate === true) {
                            if ($this->_ps['field_id'] == 0 || $createFromTemplate === true) {
                                $sql = "ALTER TABLE `zguest" . $eventid . "`
										ADD `" . $this->_ps['fieldname'] . "` " . $this->_ps['fieldtype'];
                                if ($this->_ps['fieldlength'] != - 1) {
                                    $sql .= "( " . $this->_ps['fieldlength'] . " ) ";
                                }
                                $sql .= " NOT NULL ";
                                
                                if ($this->_ps['fieldtype'] == 'int') {
                                    $sql .= ' DEFAULT 0 ';
                                }
                                if ($createFromTemplate === false) {
                                    $sql .= " AFTER `changetime`;";
                                }
                            } else {
                                $sql = "ALTER TABLE `zguest" . $eventid . "`
										CHANGE `" . $oldcolname . "` `" . $this->_ps['fieldname'] . "`
												" . $this->_ps['fieldtype'];
                                if ($this->_ps['fieldlength'] != - 1) {
                                    $sql .= "( " . $this->_ps['fieldlength'] . " ) ";
                                }
                                if ($this->_ps['fieldtype'] == 'int') {
                                    $sql .= ' DEFAULT 0 ';
                                }
                                $sql .= " NOT NULL;";
                            }
                            $pdoStatement = $this->_pdoObj->prepare($sql, array(
                                PDO::ATTR_CURSOR => PDO::CURSOR_FWDONLY
                            ));
                            $pdoStatement->execute(array());
                            
                            if ($pdoStatement->errorCode() * 1 != 0) {
                                $this->addError('', gettext('SQL Error') . ' ' . __LINE__ . ': ' . print_r($pdoStatement->errorInfo(), true), 1);
                            }
                        }
                    }
                    if (count($this->_err) == 0) {
                        if ($createFromTemplate === false) {
                            $this->_pdoObj->commit();
                        }
                    } else {
                        if ($createFromTemplate === false) {
                            $this->_pdoObj->rollBack();
                        }
                    }
                }
            } catch (Exception $e) {
                if ($createFromTemplate === false) {
                    $this->_pdoObj->rollBack();
                }
                $this->addError('', gettext('Database Error'), 1, print_r($e->getMessage(), true));
            }
        }
    }

    private function handleDeleteField()
    {
        // check if field id is numeric
        if (! is_numeric($this->_ps['field_id'])) {
            $this->addError('', gettext('Field Id invalid'));
        }
        // check if event id is numeric
        if (! is_numeric($this->_ps['fevent_id'])) {
            $this->addError('', 'Fehlerhafte Event-Id');
        }
        // check if event belongs to user
        if (! $this->checkUserEvent($this->_ps['fevent_id'])) {
            $this->addError('', 'Ungültige Event-Id: ' . $this->_ps['fevent_id']);
        }
        $table1name = 'zcolumn' . $this->_ps['fevent_id'];
        $this->_pdoObj = dbconnection::getInstance();
        $this->_pdoObj->beginTransaction();
        try {
            // figure out the field name
            $sql = "SELECT fieldname FROM `" . $table1name . "`
					WHERE field_id = :fieldid
					AND syscolumn = 0
					AND deleted = 0
					;";
            $pdoStatement = $this->_pdoObj->prepare($sql, array(
                PDO::ATTR_CURSOR => PDO::CURSOR_FWDONLY
            ));
            $pdoStatement->execute(array(
                ':fieldid' => $this->_ps['field_id']
            ));
            
            if ($pdoStatement->errorCode() * 1 != 0) {
                $this->addError('', 'SQL Fehler' . print_r($pdoStatement->errorInfo(), true), 1);
            } elseif ($pdoStatement->rowCount() == 0) {
                $this->addError('', 'Feld nicht gefunden.');
            }
            // delete field from column table
            if (count($this->_err) == 0) {
                $sql = "UPDATE `" . $table1name . "` SET deleted = 1 WHERE field_id = :fieldid;";
                $pdoStatement = $this->_pdoObj->prepare($sql, array(
                    PDO::ATTR_CURSOR => PDO::CURSOR_FWDONLY
                ));
                $pdoStatement->execute(array(
                    ':fieldid' => $this->_ps['field_id']
                ));
                if ($pdoStatement->errorCode() * 1 != 0) {
                    $this->addError('', 'SQL Fehler' . print_r($pdoStatement->errorInfo(), true), 1);
                } elseif ($pdoStatement->rowCount() == 0) {
                    $this->addError('', __LINE__ . ' SQL Fehler mit Errorcode 0: ' . print_r($pdoStatement->errorInfo(), true), 1);
                }
            }
            if (count($this->_err) == 0) {
                $this->_pdoObj->commit();
            } else {
                $this->_pdoObj->rollBack();
            }
        } catch (Exception $e) {
            $this->addError('', 'Error deleting field ' . $this->_ps['field_id'], print_r($e, true));
            $this->_pdoObj->rollBack();
        }
    }

    private function handleDeleteEvent()
    {
        // check if event id is numeric
        if (! is_numeric($this->_ps['fevent_id'])) {
            $this->addError('', 'Fehlerhafte Event-Id');
        }
        // check if event belongs to user
        if (! $this->checkUserEvent($this->_ps['fevent_id'])) {
            $this->addError('', 'Ungültige Event-Id: ' . $this->_ps['fevent_id']);
        }
        
        $tables = array(
            'zcolumn' . $this->_ps['fevent_id'],
            'zguest' . $this->_ps['fevent_id'],
            'ygr' . $this->_ps['fevent_id'],
            'wemail' . $this->_ps['fevent_id'],
            'zregprocstep' . $this->_ps['fevent_id'],
            'fevset_' . $this->_ps['fevent_id']
        );
        $this->_pdoObj = dbconnection::getInstance();
        $this->_pdoObj->beginTransaction();
        try {
            foreach ($tables as $table) {
                $sql = "DROP TABLE IF EXISTS `" . $table . "`;";
                $pdoStatement = $this->_pdoObj->prepare($sql, array(
                    PDO::ATTR_CURSOR => PDO::CURSOR_FWDONLY
                ));
                $pdoStatement->execute(array());
                
                if ($pdoStatement->errorCode() * 1 != 0) {
                    $this->addError('', 'SQL Fehler', 1, __FILE__ . '/' . __LINE__ . ': ' . print_r($pdoStatement->errorInfo(), true));
                }
            }
            // delete record(s) from room table
            if (count($this->_err) == 0) {
                $sql = "DELETE FROM `room` WHERE fevent_id = :eventid;";
                $pdoStatement = $this->_pdoObj->prepare($sql, array(
                    PDO::ATTR_CURSOR => PDO::CURSOR_FWDONLY
                ));
                $pdoStatement->execute(array(
                    ':eventid' => $this->_ps['fevent_id']
                ));
                if ($pdoStatement->errorCode() * 1 != 0) {
                    $this->addError('', 'SQL Fehler' . print_r($pdoStatement->errorInfo(), true), 1);
                }
            }
            // delete record from event table
            if (count($this->_err) == 0) {
                $sql = "DELETE FROM `fevent` WHERE fevent_id = :eventid;";
                $pdoStatement = $this->_pdoObj->prepare($sql, array(
                    PDO::ATTR_CURSOR => PDO::CURSOR_FWDONLY
                ));
                $pdoStatement->execute(array(
                    ':eventid' => $this->_ps['fevent_id']
                ));
                if ($pdoStatement->errorCode() * 1 != 0) {
                    $this->addError('', 'SQL Fehler' . print_r($pdoStatement->errorInfo(), true), 1);
                }
            }
            if (count($this->_err) == 0) {
                $this->_pdoObj->commit();
                unset($_SESSION['event']);
                unset($_SESSION['fevent_id']);
                $this->refreshEventListForUser();
            } else {
                $this->_pdoObj->rollBack();
            }
        } catch (Exception $e) {
            $this->addError('', 'Error deleting event ' . $this->_ps['fevent_id'], 1, __FILE__ . '/' . __LINE__ . ': ' . print_r($e, true));
            $this->_pdoObj->rollBack();
        }
    }

    private function handleMoveField()
    {
        // check if field id is numeric
        if (! is_numeric($this->_ps['field_id'])) {
            $this->addError('', 'Fehlerhafte Feld-Id');
        }
        // check if event id is numeric
        if (! is_numeric($this->_ps['fevent_id'])) {
            $this->addError('', 'Fehlerhafte Event-Id');
        }
        // check if direction is ok
        if ($this->_ps['direction'] != 'up' && $this->_ps['direction'] != 'down') {
            $this->addError('', 'Wrong direction');
        }
        // check if event belongs to user
        if (! $this->checkUserEvent($this->_ps['fevent_id'])) {
            $this->addError('', 'Ungültige Event-Id: ' . $this->_ps['fevent_id']);
        }
        // check if ordertype is ok
        if ($this->_ps['ordertype'] != 'export' && $this->_ps['ordertype'] != 'formfield') {
            $this->addError('', 'Ungültiger Sortierungstyp.', 1, 'Invalid ordertype ' . $this->_ps['ordertype']);
        }
        $tablename = 'zcolumn' . $this->_ps['fevent_id'];
        $this->_pdoObj = dbconnection::getInstance();
        $this->_pdoObj->beginTransaction();
        try {
            // figure out current field position
            $fd = $this->getFieldData($this->_ps['fevent_id'], $this->_ps['field_id']);
            if ($this->_ps['ordertype'] == 'export') {
                $currentPosition = $fd['exportorder'];
            } elseif ($this->_ps['ordertype'] == 'formfield') {
                $currentPosition = $fd['fieldorder'];
            }
            // direction up?
            if ($this->_ps['direction'] == 'up') {
                // if current position = 1, do nothing
                // otherwise:
                if ($currentPosition > 1) {
                    // add one to the position of the field with pos of current - 1 ++
                    // subtract one from the field with the given id
                    $this->switchNeighbourFields($this->_ps['fevent_id'], $this->_ps['field_id'], $currentPosition, $currentPosition - 1, $this->_ps['ordertype']);
                }
            } elseif ($this->_ps['direction'] == 'down') {
                // direction down?
                // find highest position
                $highestPos = $this->returnHighestFieldExportOrder($this->_ps['fevent_id'], $this->_ps['ordertype']);
                // if current position = highest position, do nothing
                // otherwise:
                if ($currentPosition < $highestPos) {
                    $this->switchNeighbourFields($this->_ps['fevent_id'], $this->_ps['field_id'], $currentPosition, $currentPosition + 1, $this->_ps['ordertype']);
                }
            }
            
            if (count($this->_err) == 0) {
                $this->_pdoObj->commit();
            } else {
                $this->_pdoObj->rollBack();
            }
        } catch (Exception $e) {
            $this->addError('', 'Error moving field ' . $this->_ps['field_id'], print_r($e, true));
            $this->_pdoObj->rollBack();
        }
    }

    private function handleToggleFieldExport()
    {
        // check if field id is numeric
        if (! is_numeric($this->_ps['field_id'])) {
            $this->addError('', 'Fehlerhafte Feld-Id');
        }
        // check if event id is numeric
        if (! is_numeric($this->_ps['fevent_id'])) {
            $this->addError('', 'Fehlerhafte Event-Id');
        }
        // check if direction is ok
        if ($this->_ps['newStatus'] != 'on' && $this->_ps['newStatus'] != 'off') {
            $this->addError('', 'Wrong Status');
        }
        if ($this->_ps['ordertype'] != 'export' && $this->_ps['ordertype'] != 'formfield') {
            $this->addError('', 'Invalid order type.');
        }
        // check if event belongs to user
        if (! $this->checkUserEvent($this->_ps['fevent_id'])) {
            $this->addError('', 'Ungültige Event-Id: ' . $this->_ps['fevent_id']);
        }
        $tablename = 'zcolumn' . $this->_ps['fevent_id'];
        $this->_pdoObj = dbconnection::getInstance();
        $this->_pdoObj->beginTransaction();
        try {
            // if on:
            if ($this->_ps['newStatus'] == 'on') {
                // figure out highest current position number
                $highestPos = $this->returnHighestFieldExportOrder($this->_ps['fevent_id'], $this->_ps['ordertype']);
                // assign highest number + 1 to field id
                $this->assignExportOrderToField($this->_ps['fevent_id'], $this->_ps['field_id'], $highestPos + 1, $this->_ps['ordertype']);
            } else {
                // if off:
                // figure out current position number
                $fd = $this->getFieldData($this->_ps['fevent_id'], $this->_ps['field_id']);
                if ($this->_ps['ordertype'] == 'export') {
                    $currentPosition = $fd['exportorder'];
                } else {
                    $currentPosition = $fd['fieldorder'];
                }
                // subtract 1 from position of all fields with position > current position (ordered)
                $this->removeFieldFromExport($this->_ps['fevent_id'], $this->_ps['field_id'], $currentPosition, $this->_ps['ordertype']);
            }
            
            // figure out current field position
            
            if (count($this->_err) == 0) {
                $this->_pdoObj->commit();
            } else {
                $this->_pdoObj->rollBack();
            }
        } catch (Exception $e) {
            $this->addError('', 'Error toggling field export status ' . $this->_ps['field_id'], 1, print_r($e, true));
            $this->_pdoObj->rollBack();
        }
    }

    private function removeFieldFromExport($eventid, $fieldid, $currentorder, $ordertype)
    {
        // SET to null
        $this->assignExportOrderToField($eventid, $fieldid, null, $ordertype);
        if ($ordertype == 'formfield') {
            $fieldname = 'fieldorder';
        } else {
            $fieldname = 'exportorder';
        }
        // subtract 1 from all
        $sql = "UPDATE `zcolumn" . $eventid . "`
		SET $fieldname = $fieldname - 1
		WHERE $fieldname > :currentorder
		AND deleted = 0
		ORDER BY $fieldname ASC
		;";
        $pdoStatement = $this->_pdoObj->prepare($sql, array(
            PDO::ATTR_CURSOR => PDO::CURSOR_FWDONLY
        ));
        $pdoStatement->execute(array(
            ':currentorder' => $currentorder
        ));
        
        if ($pdoStatement->errorCode() * 1 != 0) {
            $this->addError('', 'SQL Fehler', 1, print_r($pdoStatement->errorInfo(), true));
        }
    }

    private function assignExportOrderToField($eventid, $fieldid, $exportorder, $ordertype)
    {
        try {
            if ($ordertype == 'export') {
                $fieldname = 'exportorder';
            } else {
                $fieldname = 'fieldorder';
            }
            $sql = "UPDATE `zcolumn" . $eventid . "`
			SET $fieldname = :exportorder
			WHERE field_id = :fieldid
			AND deleted = 0
			;";
            $pdoStatement = $this->_pdoObj->prepare($sql, array(
                PDO::ATTR_CURSOR => PDO::CURSOR_FWDONLY
            ));
            $pdoStatement->execute(array(
                ':exportorder' => $exportorder,
                ':fieldid' => $fieldid
            ));
            
            if ($pdoStatement->errorCode() * 1 != 0) {
                $this->addError('', 'SQL Fehler' . print_r($pdoStatement->errorInfo(), true), 1, print_r($pdoStatement->errorInfo(), true));
            } elseif ($pdoStatement->rowCount() == 0) {
                $this->addError('', 'Feld nicht gefunden.', 1, print_r($pdoStatement->errorInfo(), true) . 'Feld nicht gefunden ' . __LINE__);
            }
        } catch (Exception $e) {
            $this->addError('', 'Error assigning export order to field', 1, print_r($e, true) . __LINE__);
        }
    }

    private function switchNeighbourFields($eventid, $fieldId, $currentPos, $neighbourPos, $ordertype)
    {
        try {
            if ($ordertype == 'export') {
                $fieldname = 'exportorder';
            } elseif ($ordertype == 'formfield') {
                $fieldname = 'fieldorder';
            }
            $sql = "UPDATE `zcolumn" . $eventid . "`
			SET $fieldname = :currentpos
			WHERE $fieldname = :neighbourpos
			AND deleted = 0
			;";
            $pdoStatement = $this->_pdoObj->prepare($sql, array(
                PDO::ATTR_CURSOR => PDO::CURSOR_FWDONLY
            ));
            $pdoStatement->execute(array(
                ':currentpos' => $currentPos,
                ':neighbourpos' => $neighbourPos
            ));
            
            if ($pdoStatement->errorCode() * 1 != 0) {
                $this->addError('', 'SQL Fehler' . print_r($pdoStatement->errorInfo(), true), 1, print_r($pdoStatement->errorInfo(), true));
            } elseif ($pdoStatement->rowCount() == 0) {
                $this->addError('', 'Feld nicht gefunden.', 1, print_r($pdoStatement->errorInfo(), true));
            }
            
            $sql = "UPDATE `zcolumn" . $eventid . "`
			SET $fieldname = :neighbourpos
			WHERE field_id = :fieldid
			AND deleted = 0
			;";
            $pdoStatement = $this->_pdoObj->prepare($sql, array(
                PDO::ATTR_CURSOR => PDO::CURSOR_FWDONLY
            ));
            $pdoStatement->execute(array(
                ':fieldid' => $fieldId,
                ':neighbourpos' => $neighbourPos
            ));
            
            if ($pdoStatement->errorCode() * 1 != 0) {
                $this->addError('', 'SQL Fehler' . print_r($pdoStatement->errorInfo(), true), 1, print_r($pdoStatement->errorInfo(), true));
            } elseif ($pdoStatement->rowCount() == 0) {
                $this->addError('', 'Feld nicht gefunden.', 1, print_r($pdoStatement->errorInfo(), true));
            }
        } catch (Exception $e) {
            $this->addError('', 'Error switching fields', print_r($e, true));
        }
    }

    /**
     *
     * @param int $eventid
     * @param str $filter
     *            Exported, Notexported, Inform, Notinform, <empty>, ALL.
     *            <empty> Does not show deleted or syshidden fields.
     *            ALL - shows all fields.
     * @return multitype:unknown
     */
    private function getFields($eventid, $filter)
    {
        $data = array();
        // check if event id is numeric
        if (! is_numeric($eventid)) {
            $this->addError('', 'Fehlerhafte Event-Id');
        }
        // check if event belongs to user
        if (! $this->checkUserEvent($eventid)) {
            $this->addError('', 'Ungültige Event-Id: ' . $eventid);
        }
        if ($filter != 'Exported' && $filter != 'Notexported' && $filter != 'Inform' && $filter != 'Notinform' && $filter != 'NOTstandard' && $filter != 'ALL') {
            $this->addError('', 'Invalid Filter.');
        }
        try {
            $sql = "SELECT *
					FROM `zcolumn" . $eventid . "`
							WHERE
							1 = 1 ";
            if ($filter != 'ALL') {
                $sql .= ' AND deleted = 0
						AND syshidden = 0 ';
            }
            if ($filter == 'Exported') {
                $sql .= ' AND exportorder IS NOT NULL ';
                $sql .= ' ORDER BY exportorder ASC;';
            } elseif ($filter == 'Notexported') {
                $sql .= ' AND exportorder IS NULL ';
                $sql .= ' ORDER BY exportorder ASC;';
            } elseif ($filter == 'Inform') {
                $sql .= ' AND fieldorder IS NOT NULL ';
                $sql .= ' ORDER BY fieldorder ASC;';
            } elseif ($filter == 'Notinform') {
                $sql .= ' AND fieldorder IS NULL ';
                $sql .= ' ORDER BY fieldorder ASC;';
            } elseif ($filter == 'NOTstandard') {
                $sql .= ' AND fielddisplaytype > 0 ';
                $sql .= ' ORDER BY fieldorder ASC;';
            }
            $this->_pdoObj = dbconnection::getInstance();
            $pdoStatement = $this->_pdoObj->prepare($sql, array(
                PDO::ATTR_CURSOR => PDO::CURSOR_FWDONLY
            ));
            $pdoStatement->execute(array());
            if ($pdoStatement->errorCode() * 1 != 0) {
                $this->addError('', 'SQL Fehler', print_r($pdoStatement->errorInfo(), true), 1);
            }
            if (count($this->_err) == 0) {
                $pdoStatement = $this->_pdoObj->prepare($sql, array(
                    PDO::ATTR_CURSOR => PDO::CURSOR_FWDONLY
                ));
                $pdoStatement->execute(array());
                if ($pdoStatement->errorCode() * 1 != 0) {
                    $this->addError('', 'SQL Fehler' . print_r($pdoStatement->errorInfo(), true), 1);
                } else {
                    while ($row = $pdoStatement->fetch(\PDO::FETCH_ASSOC)) {
                        $data[] = $row;
                    }
                }
            }
        } catch (Exception $e) {
            $this->addError('', 'Fehler', print_r($pdoStatement->errorInfo(), true), 1);
        }
        return $data;
    }

    private function printFieldListTable($eventid, $ordertype)
    {
        // $ev = $this->readRecord($eventid);
        $sm = new L5Smarty();
        $sm->assign('err', $this->_err);
        $sm->assign('fevent_id', $eventid);
        if ($ordertype == 'export') {
            $sm->assign('fieldsExported', $this->getFields($eventid, 'Exported'));
            $sm->assign('fieldsNotexported', $this->getFields($eventid, 'Notexported'));
            $xout = $sm->fetch('event/fieldlisttable.tpl');
        } elseif ($ordertype == 'fieldorder') {
            $sm->assign('fieldsInform', $this->getFields($eventid, 'Inform'));
            $sm->assign('fieldsNotinform', $this->getFields($eventid, 'Notinform'));
            $xout = $sm->fetch('event/guestformfieldtable.tpl');
        }
        return $xout;
    }

    private function returnHighestFieldExportOrder($eventid, $ordertype)
    {
        $ret = null;
        if ($ordertype == 'export') {
            $fieldname = 'exportorder';
        } else {
            $fieldname = 'fieldorder';
        }
        $sql = "SELECT fieldname, $fieldname, field_id FROM `zcolumn" . $eventid . "`
		WHERE deleted = 0
		AND $fieldname IS NOT NULL
		ORDER BY $fieldname DESC
		LIMIT 1
		;";
        $pdoStatement = $this->_pdoObj->prepare($sql, array(
            PDO::ATTR_CURSOR => PDO::CURSOR_FWDONLY
        ));
        $pdoStatement->execute(array());
        
        if ($pdoStatement->errorCode() * 1 != 0) {
            $this->addError('', 'SQL Fehler' . print_r($pdoStatement->errorInfo(), true), 1);
        } elseif ($pdoStatement->rowCount() == 0) {
            $ret = 0;
        } else {
            $row = $pdoStatement->fetch();
            $ret = $row[$fieldname];
        }
        return $ret;
    }

    private function handleUpload($imageName)
    {
        $eventid = $this->_ps['fevent_id'];
        if ($this->checkUserEvent($eventid)) {
            $uploadOptions = array(
                'upload_dir' => BASEDIR . 'public/files/e' . $eventid . '/',
                'accept_file_types' => '/\.(gif|jpe?g|png)$/i',
                'max_file_size' => 500000,
                'max_width' => 700,
                'max_height' => 100,
                'filename' => $imageName
            );
            require_once BASEDIR . 'lib/jqueryUploader/UploadHandler.php';
            $uh = new UploadHandler($uploadOptions);
        } else {
            $this->addError('', 'No file upload permission for event ' . $eventid);
        }
    }

    private function updateFileName($filename)
    {
        $eventid = $this->_ps['fevent_id'];
        $allowedFileNames = array(
            'imageHeader'
        );
        $allowedFileExtensions = array(
            'jpg',
            'jpeg',
            'gif',
            'png'
        );
        if ($this->checkUserEvent($eventid)) {
            $fnparts = explode('.', $filename);
            if (count($fnparts) == 2 && array_search($fnparts[0], $allowedFileNames) !== false && array_search($fnparts[1], $allowedFileExtensions) !== false) {
                $sql = "UPDATE `fevent` SET " . $fnparts[0] . " = :filename
						WHERE fevent_id = :eventid;";
                $pdoStatement = $this->_pdoObj->prepare($sql, array(
                    PDO::ATTR_CURSOR => PDO::CURSOR_FWDONLY
                ));
                $pdoStatement->execute(array(
                    ':filename' => $filename,
                    ':eventid' => $eventid
                ));
                
                if ($pdoStatement->errorCode() * 1 != 0) {
                    $this->addError('', 'SQL Fehler while updating a filename ', 1, print_r($pdoStatement->errorInfo(), true));
                }
            } else {
                $this->addError('filename', 'Filename not allowed.');
            }
        } else {
            $this->addError('', 'No file update permission for event ' . $eventid);
        }
    }

    private function generateFontFamilyOptions()
    {
        $op = array();
        $i = 0;
        foreach ($this->_fontfamilies as $ff) {
            $op[$i] = $ff[0];
            $i ++;
        }
        return $op;
    }

    private function generateSmtpSecureVals()
    {
        $op = array(
            0 => '',
            '1' => 'ssl',
            2 => 'tls'
        );
        return $op;
    }

    private function getNewFeedbackSince()
    {
        $sql = "select zg.guest_id, zg.row_id, zg.registered, zg.cancelled,
				(SELECT row_id FROM zguest" . $eventid . " WHERE guest_id = zg.guest_id AND row_id < zg.row_id ORDER BY row_id DESC LIMIT 1) pre_row_id,
						(SELECT registered FROM zguest" . $eventid . " WHERE guest_id = zg.guest_id AND row_id < zg.row_id ORDER BY row_id DESC LIMIT 1) pre_registered,
								(SELECT cancelled FROM zguest" . $eventid . " WHERE guest_id = zg.guest_id AND row_id < zg.row_id ORDER BY row_id DESC LIMIT 1) pre_cancelled,
										changetime
										from zguest" . $eventid . " zg
												WHERE
												(
												registered <> (SELECT registered FROM zguest" . $eventid . " WHERE guest_id = zg.guest_id AND row_id < zg.row_id ORDER BY row_id DESC LIMIT 1)
														OR cancelled <> (SELECT cancelled FROM zguest" . $eventid . " WHERE guest_id = zg.guest_id AND row_id < zg.row_id ORDER BY row_id DESC LIMIT 1)
																)
																ORDER BY guest_id DESC, row_id DESC
																";
    }

    public function getNumberOfEvents()
    {
        $numberOfEvents = 0;
        $sql = "SELECT COUNT(*) FROM fevent WHERE fuser_id = :user
				ORDER BY createdtime DESC
				;";
        $this->_pdoObj = dbconnection::getInstance();
        $pdoStatement = $this->_pdoObj->prepare($sql, array(
            PDO::ATTR_CURSOR => PDO::CURSOR_FWDONLY
        ));
        $pdoStatement->execute(array(
            ':user' => $_SESSION['user']['id']
        ));
        $row = $pdoStatement->fetch();
        $numberOfEvents = $row[0];
        return $numberOfEvents;
    }

    private function getRSVPHistory($eventid, $records = null, $hours = 48)
    {
        $history = array();
        $sql = "select zg.*,
				(SELECT row_id FROM zguest" . $eventid . " WHERE guest_id = zg.guest_id AND row_id < zg.row_id ORDER BY row_id DESC LIMIT 1) pre_row_id,
						IF((SELECT COUNT(*) FROM zguest" . $eventid . " WHERE guest_id = zg.guest_id) > 1,
								registered-(SELECT registered FROM zguest" . $eventid . " WHERE guest_id = zg.guest_id AND row_id < zg.row_id ORDER BY row_id DESC LIMIT 1),registered)
										diff_registered,
										IF((SELECT COUNT(*) FROM zguest" . $eventid . " WHERE guest_id = zg.guest_id) > 1,
												cancelled-(SELECT cancelled FROM zguest" . $eventid . " WHERE guest_id = zg.guest_id AND row_id < zg.row_id ORDER BY row_id DESC LIMIT 1),
														cancelled) diff_cancelled
														from zguest" . $eventid . " zg
																WHERE
																(
																registered <> (SELECT registered FROM zguest" . $eventid . " WHERE guest_id = zg.guest_id AND row_id < zg.row_id ORDER BY row_id DESC LIMIT 1)
																		OR cancelled <> (SELECT cancelled FROM zguest" . $eventid . " WHERE guest_id = zg.guest_id AND row_id < zg.row_id ORDER BY row_id DESC LIMIT 1)
																				)
																				OR
																				(
																				((SELECT COUNT(*) FROM zguest" . $eventid . " WHERE guest_id = zg.guest_id) = 1)
																						AND
																						registered > 0
																						)
																						ORDER BY changetime DESC
																						";
        
        $this->_pdoObj = dbconnection::getInstance();
        $pdoStatement = $this->_pdoObj->prepare($sql, array(
            PDO::ATTR_CURSOR => PDO::CURSOR_FWDONLY
        ));
        $pdoStatement->execute(array());
        while ($row = $pdoStatement->fetch()) {
            $history[] = $row;
        }
        return $history;
    }

    private function handleMassMailSending()
    {
        $eventid = $this->_ps['fevent_id'];
        if ($this->checkUserEvent($eventid) === true) {
            $massmailcount = $this->countMassMailRecipients($eventid);
            $numberOfEmails = $this->sendMassMail();
            if (count($this->_err) == 0) {
                $xout = $this->printMassMailForm($eventid);
            }
        }
        return $xout;
    }

    public function printMassMailForm($eventid, $numberofemails = -1, $massmailcount = array())
    {
        if ($this->checkUserEvent($eventid) === true) {
            $sm = new L5Smarty();
            if (isset($this->_ps['massmailrecipients'])) {
                if (! is_array($this->_ps['massmailrecipients'])) {
                    $f = $this->_ps['massmailrecipients'];
                    $this->_ps['massmailrecipients'] = array();
                    $this->_ps['massmailrecipients'][0] = $f;
                }
            }
            if ($this->_ps['form'] == 'sendMassMail' && count($this->_err) == 0 && $this->_ps['massmailsendingtype'] == 4 && $numberOfEmails > 0) {
                $sm->assign('massmailsentsuccessfully', 1);
                $sm->assign('massmailsentnumber', $numberOfEmails);
            }
            $sm->assign('err', $this->_err);
            $sm->assign('ps', $this->_ps);
            $sm->assign('fevent_id', $eventid);
            $sm->assign('ev', $this->readRecord($eventid));
            $sm->assign('massmailcount', $massmailcount);
            $xout = $sm->fetch('event/massmailform.tpl');
        } else {
            $xout .= 'No access to this event id (' . $eventid . ')';
        }
        return $xout;
    }

    /**
     * sends an email
     *
     * @return number
     */
    private function sendMassMail()
    {
        $numberOfEmails = 0;
        $ev = $this->readRecord($this->_ps['fevent_id']);
        if (! filter_var($ev['mailsender'], FILTER_VALIDATE_EMAIL)) {
            $this->addError('', gettext('Please add a mail sender address in the Settings > Email tab before sending mass mails.'));
        }
        
        if (strlen(trim($ev['mailsendername'])) < 3) {
            $this->addError('', gettext('Please add a mail sender name in the Settings > Email tab (min. 3 characters) before sending mass mails.'));
        }
        
        if (! isset($this->_ps['massmailrecipients'])) {
            $this->addError('', gettext('Please select recipients for your mass mail.'));
        }
        $this->_ps['massmailsubject'] = trim($this->_ps['massmailsubject']);
        if (strlen($this->_ps['massmailsubject']) < 3) {
            $this->addError('massmailsubject', gettext('The subject of the mail needs to consist of at least 3 characters.'));
        }
        $this->_ps['massmailcontent'] = trim($this->_ps['massmailcontent']);
        if (strlen($this->_ps['massmailcontent']) < 3) {
            $this->addError('massmailsubject', gettext('The content of the mail needs to consist of at least 3 characters.'));
        }
        if ($this->_ps['massmailsendingtype'] == 1) {
            // type 1: send one email
            if (! is_numeric($this->_ps['testmailguestid'])) {
                $this->addError('testmailguestid', gettext('Invalid guest id.'));
            }
            $guests = array(
                'list' => array(
                    0 => array(
                        'guest_id' => $this->_ps['testmailguestid'],
                        1 => $this->_ps['testmailrecipient1']
                    )
                )
            );
        } elseif ($this->_ps['massmailsendingtype'] == 3) {
            // type 3: send all mails to one address
            $guests = $this->getGuestList($this->_ps['fevent_id'], array(
                'onlyWithEmail' => 1
            ));
            $destination = $this->_ps['testmailrecipient3'];
        } elseif ($this->_ps['massmailsendingtype'] == 4) {
            // type 4: production / live: send to all
            $guests = $this->getGuestList($this->_ps['fevent_id'], array(
                'onlyWithEmail' => 1
            ));
            // email addresses are determined in the getGuestList funciton from guest data in this case.
            if (count($guests) > 100) {
                $this->addError('massmailsendingtype', gettext('If you need to send a mass mailing to more than 100 recipients, please contact the support first.'), 2, 'Mass mailing to ' . count($guests) . ' recipients requested.');
            }
        }
        if (count($this->_err) == 0) {
            foreach ($guests['list'] as $guest) {
                try {
                    $myGuest = new entityGuest($this->_ps['fevent_id'], $guest['guest_id']);
                    $subject = $myGuest->createCustomizedText($this->_ps['massmailsubject']);
                    
                    $sm = new L5Smarty();
                    $sm->assign('content', $myGuest->createCustomizedText($this->_ps['massmailcontent']));
                    $sm->assign('ev', $ev);
                    
                    if ($this->_ps['emailtemplate_id'] == '0') {
                        $contentHTML = $sm->fetch('guestmails/massmailstd.html.tpl');
                    } else {
                        $emt = new emailtemplate($this->_lg, $this->_locale);
                        $emt->setRecordId($this->_ps['emailtemplate_id']);
                        $tpl = $emt->getData();
                        $mailtpl = $tpl['content'];
                        
                        $dom = new DOMDocument();
                        $dom->loadHTML($mailtpl);
                        $xpath = new DOMXPath($dom);
                        
                        $nodes = $xpath->query("//@*[name()='mc:edit']");
                        foreach ($nodes as $node) {
                            if ($node->nodeValue == 'std_content00') {
                                $node->parentNode->nodeValue = $node->nodeValue;
                            }
                        }
                        
                        $contentHTML = $dom->saveHTML();
                    }
                    $contentTXT = htmlspecialchars_decode($contentHTML);
                    if (strpos($contentHTML, 'cid:qr_ticket_open')) {
                        $this->_emailAttachedImages[] = 'qr_ticket_open';
                    }
                    if (strpos($contentHTML, 'cid:qr_ticket_vcard')) {
                        $this->_emailAttachedImages[] = 'qr_ticket_vcard';
                    }
                    // determine recipient
                    if (! isset($guest[1])) {
                        if (isset($destination)) {
                            $guest[1] = $destination;
                        } else {
                            $guest[1] = $myGuest->getEmailAddress();
                        }
                    }
                    $this->sendEmailToRecipient($subject, $contentHTML, $contentTXT, $guest[1], $this->readRecord($this->_ps['fevent_id']), $myGuest->getData());
                    $numberOfEmails += 1;
                } catch (Exception $e) {
                    $this->addError('', gettext('Unknown error.'), 1, __FILE__ . ' :: ' . __LINE__ . ' ' . print_r($e, true));
                }
            }
        }
        return $numberOfEmails;
    }

    /**
     * returns a list of emails for the given event with the given sendstatus
     *
     * @param int $eventid
     *            contains the event id
     * @param int $sendstatus
     *            contains the sendstatus: 0 = draft
     * @return multitype:unknown
     */
    private function readEmails($eventid, $sendstatus)
    {
        $emails = array();
        $sql = "SELECT email_id, subject
					FROM wemail" . $eventid . " 
					WHERE sendstatus = :sendstatus;";
        $this->_pdoObj = dbconnection::getInstance();
        $pdoStatement = $this->_pdoObj->prepare($sql, array(
            PDO::ATTR_CURSOR => PDO::CURSOR_FWDONLY
        ));
        $pdoStatement->execute(array(
            'sendstatus' => $sendstatus
        ));
        while ($row = $pdoStatement->fetch()) {
            $emails[] = $row;
        }
        return $emails;
    }

    /**
     * returns a list of guest ids for the current event,
     * considering the applied filter(s)
     *
     * @param array $filter
     *            // onlyWithEmail (default = 0) only records with emailaddress field filled are returned
     * @return multitype:multitype:unknown // array ( list => array(
     *         0 => array(0 => row_id, 1 => emailaddress)
     *         1 => array(0 => row_id, 1 => emailaddress)
     *         n => array(0 => row_id, 1 => emailaddress)
     *         )
     *         count => 24
     *         )
     */
    private function getGuestList($eventid, $filter, $countonly = false, $columns = array('guest_id', 'emailaddress'), $sort = null, $se = null, $start = 0, $cnt = -1, $records = null, $subentities = null)
    {
        try {
            if (is_numeric($eventid)) {
                $crmOn = false;
                if ($start < 0) {
                    $start = 0;
                }
                $unionloops = 1;
                if ($this->checkCRMModule($eventid) != 0) {
                    // echo "IS ON";
                    $crmOn = true;
                    $unionloops = 2;
                }
                $guestlist = array();
                
                $myFields = $this->getFields($eventid, 'ALL');
                foreach ($columns as $c) {
                    $theField = array();
                    foreach ($myFields as $mf) {
                        if ($mf['fieldname'] == $c) {
                            $guestlist['cols'][] = array(
                                'name' => $mf['fieldname'],
                                'caption' => $mf['formlabel']
                            );
                        }
                    }
                    if ($c == 'paymentstatus') {
                        $guestlist['cols'][] = array(
                            'name' => $c,
                            'caption' => gettext('Payment')
                        );
                    } elseif ($c == 'invoicesent') {
                        $guestlist['cols'][] = array(
                            'name' => $c,
                            'caption' => gettext('Invoice sent')
                        );
                    }
                }
                $guestlist['cols'][] = array(
                    'type' => 'options',
                    'caption' => gettext('Options')
                );
                
                $sqlArr = array();
                $CRMColumns = array(
                    'lastname',
                    'firstname',
                    'salutation'
                );
                $colg = '';
                $colc = '';
                foreach ($columns as $column) {
                    if (isset($this->_specialsqlcolumns[$column])) {
                        $colg .= str_replace('##xvl', 'xvl' . $eventid, $this->_specialsqlcolumns[$column]['sql']) . ', ';
                        $colc .= ' "0" invoicesent,';
                    } else {
                        $colg .= 'g.' . $column . ', ';
                        if (array_search($column, $CRMColumns) !== false) {
                            $colc .= 'c.' . $column . ', ';
                        } else {
                            $colc .= 'g.' . $column . ', ';
                        }
                    }
                }
                if ($colg == '') {
                    $colg = 'g.*';
                } else {
                    $colg = substr($colg, 0, - 2);
                }
                if ($colc == '') {
                    $colc = 'c.*';
                } else {
                    $colc = substr($colc, 0, - 2);
                }
                $sql = '(';
                
                for ($i = 1; $i < $unionloops + 1; $i ++) {
                    if ($countonly == true) {
                        $sql .= "SELECT COUNT(*) amount FROM zguest" . $eventid . " g ";
                    } else {
                        if ($i == 1) {
                            $sql .= "SELECT " . $colg . " FROM zguest" . $eventid . " g ";
                        } else {
                            $sql .= "SELECT " . $colc . " FROM zguest" . $eventid . " g 
									LEFT JOIN crm_" . $_SESSION['customer']['customer_id'] . " c 
											ON g.crm_id = c.crm_id ";
                        }
                    }
                    $sql .= " WHERE g.archived = 0 AND g.deleted = 0 ";
                    if ($i == 1 && $crmOn == true) {
                        $sql .= ' AND g.crm_id IS NULL ';
                    } elseif ($crmOn == true) {
                        $sql .= ' AND g.crm_id IS NOT NULL AND c.archived = 0 AND c.deleted = 0 ';
                    }
                    if (isset($filter['allConfirmed']) || isset($filter['allCancelled']) || isset($filter['allUnsure']) || isset($filter['allNeverloggedin'])) {
                        $sqlFilter = ' AND ';
                        if (isset($filter['allConfirmed'])) {
                            $sqlFilter .= " registered > 0 OR ";
                        }
                        if (isset($filter['allCancelled'])) {
                            $sqlFilter .= " (cancelled > 0 AND registered = 0) OR ";
                        }
                        if (isset($filter['allUnsure'])) {
                            $sqlFilter .= " (cancelled = 0 AND registered = 0 AND lastselflogin IS NOT NULL) OR ";
                        }
                        if (isset($filter['allNeverloggedin'])) {
                            $sqlFilter .= " (cancelled = 0 AND registered = 0 AND lastselflogin IS NULL) OR ";
                        }
                        $sql .= substr($sqlFilter, 0, - 3);
                    }
                    if ($records != null) {
                        $recs = explode(";", $records);
                        if (count($recs) > 0) {
                            $sqlFilter .= ' AND (';
                            foreach ($recs as $r) {
                                if (strlen($r) > 0 && is_numeric($r)) {
                                    $sqlFilter .= ' guest_id = ' . $r . ' OR ';
                                }
                            }
                            $sqlFilter = substr($sqlFilter, 0, - 4) . ') ';
                        }
                        $sql .= $sqlFilter;
                    }
                    if ($se != null) {
                        $seteile = explode(' ', $se);
                        $i = 0;
                        foreach ($seteile as $steil) {
                            $i ++;
                            $sql .= " AND (firstname LIKE :steil" . $i . " OR lastname LIKE :steil" . $i . " OR company LIKE :steil" . $i . " OR accesscode LIKE :steil" . $i . " OR guest_id like :steil" . $i . " ) ";
                            $sqlArr[':steil' . $i] = '%' . $steil . '%';
                        }
                        
                        // $sql .= " AND (lastname LIKE :se or company LIKE :se) ";
                        // $sqlArr[':se'] = '%' . $se . '%';
                    }
                    if (isset($filter['onlyWithEmail']) && $filter['onlyWithEmail'] == 1) {
                        $sql .= " AND emailaddress != '' ";
                    }
                    $sqlLMT = '';
                    if (isset($cnt) && $cnt > 0) {
                        $sqlLMT = 'LIMIT ' . $start . ',' . $cnt;
                    }
                    $sqlSort = '';
                    if (isset($sort)) {
                        $sortFieldsTMP = explode(',', $sort);
                        $sqlSort = 'ORDER BY ';
                        foreach ($sortFieldsTMP as $sft) {
                            $xtmp = explode('|', $sft);
                            $sortFields[] = array(
                                $xtmp[0],
                                $xtmp[1]
                            );
                            if ($xtmp[0] != 'undefined') {
                                $sqlSort .= $xtmp[0] . ' ';
                                if ($xtmp[1] == 'A') {
                                    $sqlSort .= 'ASC';
                                } elseif ($xtmp[1] == 'D') {
                                    $sqlSort .= 'DESC';
                                }
                                $sqlSort .= ', ';
                            }
                        }
                        if (strlen($sqlSort) > 10) {
                            $sqlSort = substr($sqlSort, 0, - 2);
                        } else {
                            $sqlSort = '';
                        }
                    } else {
                        $sqlSort = ' ORDER BY guest_id ASC ';
                    }
                    
                    if ($i == 1 && $crmOn == true) {
                        $sql .= ') UNION (';
                    }
                }
                $sql .= ')';
                $sql .= $sqlSort . ' ' . $sqlLMT;
                
                $this->_pdoObj = dbconnection::getInstance();
                $pdoStatement = $this->_pdoObj->prepare($sql, array(
                    PDO::ATTR_CURSOR => PDO::CURSOR_FWDONLY
                ));
                $pdoStatement->execute($sqlArr);
                if ($countonly == true) {
                    $row = $pdoStatement->fetch();
                    $guestlist['count'] = $row['amount'];
                } else {
                    while ($row = $pdoStatement->fetch()) {
                        $m = array();
                        foreach ($columns as $column) {
                            if ($row[$column] == null) {
                                $m[$column] = '';
                            } else {
                                $m[$column] = $row[$column];
                            }
                        }
                        if ($subentities !== null) {
                            foreach ($subentities as $se) {
                                $m['subentities'][$se] = $this->getLogEntriesForGuest($this->_recordId, $row['guest_id']);
                            }
                        }
                        $guestlist['list'][] = $m;
                    }
                }
            } else {
                $this->addError('', gettext('Event') . ' ' . $eventid . ' ' . gettext('not available.'));
            }
        } catch (Exception $e) {
            $this->addError('', '', 1, $e->getFile() . '/' . $e->getLine() . ': ' . $e->getMessage() . ' (' . $sql . ')');
        }
        
        return $guestlist;
    }

    private function getLogEntriesForGuest($eventid, $guestid)
    {
        $data = array(
            'event' => $eventid,
            'guest' => $guestid
        );
        $g = new entityGuest($eventid, $guestid, null, false, true);
        $data = $g->getGuestLog();
        return $data;
    }

    private function countMassMailRecipients($eventid)
    {
        $massmailcount = array();
        $massmailcountTmp = $this->getGuestList($eventid, array(
            'onlyWithEmail' => 1
        ), true);
        $massmailcount[1] = $massmailcountTmp['count'];
        $massmailcountTmp = $this->getGuestList($eventid, array(
            'onlyWithEmail' => 1,
            'allConfirmed' => 1
        ), true);
        $massmailcount[2] = $massmailcountTmp['count'];
        $massmailcountTmp = $this->getGuestList($eventid, array(
            'onlyWithEmail' => 1,
            'allCancelled' => 1
        ), true);
        $massmailcount[3] = $massmailcountTmp['count'];
        $massmailcountTmp = $this->getGuestList($eventid, array(
            'onlyWithEmail' => 1,
            'allUnsure' => 1
        ), true);
        $massmailcount[4] = $massmailcountTmp['count'];
        $massmailcountTmp = $this->getGuestList($eventid, array(
            'onlyWithEmail' => 1,
            'allNeverloggedin' => 1
        ), true);
        $massmailcount[5] = $massmailcountTmp['count'];
        
        return $massmailcount;
    }

    public function getMassMailRecipientCount($eventid)
    {
        $myRes = array();
        if ($this->checkUserEvent($eventid) === true) {
            $c = $this->countMassMailRecipients($eventid);
            foreach ($c as $ckey => $cvalue) {
                $myRes['MMR' . $ckey] = $cvalue;
            }
        } else {
            $xout .= 'No access to this event id (' . $eventid . ')';
        }
        return $myRes;
    }

    /**
     * checks out the given guest from the given event
     *
     * @param int $id
     *            contains the guest id
     * @param int $room
     *            contains the room id
     * @return array|null an array of errors, or null, if action was successful
     */
    private function removeGuest($id, $room)
    {
        $err = null;
        if (is_numeric($id) && is_numeric($room)) {
            $eventid = $_SESSION['event']['id'];
            $now = time();
            $ip = getenv("REMOTE_ADDR");
            $_pdoObj = dbconnection::getInstance();
            $sql = "SELECT pax FROM ygr" . $eventid . "
					WHERE room_id = :roomid AND guest_id = :guestid;";
            $pdoStatement = $_pdoObj->prepare($sql, array(
                PDO::ATTR_CURSOR => PDO::CURSOR_FWDONLY
            ));
            $pdoStatement->execute(array(
                ':roomid' => $room,
                ':guestid' => $id
            ));
            if ($pdoStatement->rowCount() > 0) {
                $row = $pdoStatement->fetch();
                if ($row['pax'] > 0) {
                    $sql = "UPDATE ygr" . $eventid . " SET pax = pax - 1
							WHERE room_id = :roomid AND guest_id = :guestid
							;";
                    $pdoStatement2 = $_pdoObj->prepare($sql, array(
                        PDO::ATTR_CURSOR => PDO::CURSOR_FWDONLY
                    ));
                    $pdoStatement2->execute(array(
                        ':roomid' => $room,
                        ':guestid' => $id
                    ));
                    if (LIVEENV) {
                        // $sql = "INSERT INTO besucherlog SET besucher = '$id', zeit = '$now', ip = '$ip', `inout` = 'out', room = '$room';";
                        // $pdoStatement2->execute(array());
                    }
                    $sql = "INSERT INTO xvl" . $eventid . " SET guest_id = :id, ip = '$ip', `inout` = 'out', room = :room;";
                    $pdoStatement2 = $_pdoObj->prepare($sql, array(
                        PDO::ATTR_CURSOR => PDO::CURSOR_FWDONLY
                    ));
                    $pdoStatement2->execute(array(
                        ':id' => $id,
                        ':room' => $room
                    ));
                }
            }
        } else
            $err[] = 'Fehler: Besucher-ID ' . $id . ' oder Raum-ID ' . $room . ' nicht vorhanden';
        return $err;
    }

    /**
     * returns data of an event for the api
     * record id must have been set before via setRecordId
     *
     * @return array contains an array of events
     */
    public function apiGetRecord($params = null, $subentity = null, $subentityid = null)
    {
        if ($this->checkUserEvent($this->_recordId) === true) {
            if ($subentity == null) {
                if ($params['form'] == 'editMultisettingsForm') {
                    $eventsettings = $this->readRecord($this->_recordId, true);
                    $ms = new multisettings();
                    $res = $ms->printMultiSettingsForm($eventsettings, $params);
                } else {
                    $res = array(
                        'event' => $this->readRecord($this->_recordId, true)
                    );
                }
            } elseif ($subentity == 'guests') {
                $g = new guest();
                $g->setEventId($this->_recordId);
                $g->setRecordId($subentityid);
                $res = $g->apiGetRecord();
            } elseif ($subentity == 'fields') {
                $g = new field();
                $g->setEventId($this->_recordId);
                $g->setRecordId($subentityid);
                $res = $g->apiGetRecord();
            } elseif ($subentity == 'emails') {
                $g = new email($this->_lg, $this->_locale);
                $g->setEventId($this->_recordId);
                $g->setRecordId($subentityid);
                $res = $g->apiGetRecord();
            } elseif ($subentity == 'regprocsteps') {
                $g = new regprocstep($this->_lg, $this->_locale);
                $g->setEventId($this->_recordId);
                $g->setRecordId($subentityid);
                $res = $g->apiGetRecord($_GET);
            } else {
                $this->addError('api', 'Entity ' . $subentity . ' not allowed.');
            }
        } else {
            $xout .= 'No access to this event id (' . $eventid . ')';
        }
        
        return $res;
    }

    /**
     * returns a list of events for the api
     *
     * @return array contains events
     */
    public function apiGetList($entity = null, $params = null)
    {
        $res = null;
        if ($entity == null) {
            $r = $this->getEventList();
            $res['events'] = $r;
        } elseif ($entity == 'regprocsteps') {
            if ($this->checkUserEvent($this->_recordId) === true) {
                $st = new regprocstep($this->_lg, $this->_locale);
                $res = $st->apiGetListForEvent($this->_recordId);
            }
        } elseif ($entity == 'guests') {
            if ($this->checkUserEvent($this->_recordId) === true) {
                $cols = array(
                    'guest_id',
                    'salutation',
                    'firstname',
                    'lastname',
                    'company',
                    'note'
                );
                if (isset($params['cols'])) {
                    $colsarr = json_decode($params['cols'], true);
                    if (count($colsarr['cols']) > 0) {
                        $cols = array();
                        foreach ($colsarr['cols'] as $c) {
                            $cols[] = $c['name'];
                        }
                    }
                }
                $ents = null;
                if (isset($params['subentities'])) {
                    $entsarr = json_decode($params['subentities'], true);
                    if (count($entsarr['entities']) > 0) {
                        $ents = array();
                        foreach ($entsarr['entities'] as $c) {
                            $ents[] = $c['entity'];
                        }
                    }
                    // $cols = array();
                }
                
                $res = $this->getGuestList($this->_recordId, null, false, $cols, $params['sort'], $params['se'], $params['start'], $params['count'], urldecode($params['records']), $ents);
                
                if ($params['format'] != '') {
                    $res = array_merge($this->formatGuestList($res, $params['format']), $res);
                }
            } else {
                $xout .= 'No access to this event id (' . $this->_recordId . ')';
            }
        } elseif ($entity == 'fields') {
            $res = $this->getFields($this->_recordId, 'ALL');
        } elseif ($entity == 'multisettings') {
            $ms = new multisettings();
            $this->addError('', gettext('Entity') . ' ' . $entity . ' ' . gettext('not implemented yet.'));
        } elseif ($entity == 'checkinstats') {
            $res = $this->getCheckinDetails();
        } elseif ($entity == 'eventdump') {
            $this->sendEventDump($_GET);
            $res['sendAnswer'] = false;
        } else {
            $this->addError('', gettext('Entity') . ' ' . $entity . ' ' . gettext('not available.'));
        }
        return $res;
    }

    private function sendEventDump($gt)
    {
        $fn = 'c' . $_SESSION['customer']['customer_id'] . '_e' . $_GET['fevent_id'] . '_ts' . $_GET['ts'] . '_r' . $_GET['randX'] . '.tar.bz2';
        $filename = BASEDIR . 'tmp/transfer_out/' . $fn;
        header("Content-Type: application/gzip");
        header("Content-Length: " . filesize($filename));
        // header('Content-Transfer-Encoding: binary');
        header('Content-Disposition: attachment; filename=' . $fn);
        // fpassthru($filename);
        ob_clean();
        flush();
        readfile($filename);
        exit();
    }

    /**
     * returns a list of those database fields in the guest list,
     * where room check-in data is stored in.
     *
     * It therefore goes through the dynamic check-in forms and
     * puts together a list of check-in form fields with type "chosebuttons"
     *
     * @return unknown[]
     */
    private function getFieldsWithRoomCheckinData()
    {
        $fieldsWithRoomCheckinData = array();
        // check if there are room sets in the check-in form - and get their field name
        $sql = "SELECT fnt.sval_vch roomfieldname FROM `fevset_" . $this->_recordId . "` ftt
				JOIN `fevset_" . $this->_recordId . "` fnt
						ON (ftt.skey_multiid = fnt.skey_multiid
						AND ftt.skey_multiname = fnt.skey_multiname
						AND fnt.skey_multikey = 'fieldname')
				WHERE ftt.skey_multiname = 'checkinformfield'
				AND ftt.skey_multikey = 'type'
				AND ftt.sval_vch = 'chosebuttons'
				;";
        $this->_pdoObj = dbconnection::getInstance();
        $pdoStatement = $this->_pdoObj->prepare($sql, array(
            PDO::ATTR_CURSOR => PDO::CURSOR_FWDONLY
        ));
        $pdoStatement->execute(array());
        while ($row = $pdoStatement->fetch(PDO::FETCH_ASSOC)) {
            if ($row['roomfieldname'] != '') {
                $fieldsWithRoomCheckinData[] = $row['roomfieldname'];
            }
        }
        return $fieldsWithRoomCheckinData;
    }

    private function getCheckinStatusFromFields($fieldsWithRoomCheckinData)
    {
        $roomFullness = array();
        $fieldlist = implode(', ', $fieldsWithRoomCheckinData);
        $sql = "SELECT " . $fieldlist . " FROM `zguest" . $this->_recordId . "` 
				WHERE deleted = 0 AND archived = 0;";
        $this->_pdoObj = dbconnection::getInstance();
        
        $pdoStatement = $this->_pdoObj->prepare($sql, array(
            PDO::ATTR_CURSOR => PDO::CURSOR_FWDONLY
        ));
        $pdoStatement->execute(array());
        while ($row = $pdoStatement->fetch(PDO::FETCH_ASSOC)) {
            foreach ($row as $f) {
                $rooms = explode(',', $f);
                foreach ($rooms as $room) {
                    if (trim($room) != '') {
                        if (! isset($roomFullness[$room])) {
                            $roomFullness[$room] = 0;
                        }
                        $roomFullness[$room] += 1;
                    }
                }
            }
        }
        return $roomFullness;
    }

    private function getCheckinDetails()
    {
        $eventdata = $this->readRecord($this->_recordId, false);
        $rooms = array();
        
        $fieldsWithRoomCheckinData = $this->getFieldsWithRoomCheckinData();
        if (count($fieldsWithRoomCheckinData) > 0) {
            $checkinRoomList = $this->getCheckinStatusFromFields($fieldsWithRoomCheckinData);
        }
        foreach ($eventdata['room'] as $r) {
            $rooms[$r['id']] = $r;
        }
        $res['fevent']['title'] = $eventdata['title'];
        $res['fevent']['fevent_id'] = $this->_recordId;
        $this->_recordId;
        $sql = "SELECT 
    					yg.room_id, 
    					SUM(yg.pax) pax,
    					(SELECT COUNT(DISTINCT(guest_id)) 
							FROM xvl" . $this->_recordId . " WHERE room = yg.room_id AND `inout` = 'in') spax
						FROM ygr" . $this->_recordId . " yg
						GROUP BY yg.room_id
				";
        // $sql = "SELECT room_id, SUM(pax) pax FROM ygr".$this->_recordId."
        // GROUP BY room_id;";
        $this->_pdoObj = dbconnection::getInstance();
        $pdoStatement = $this->_pdoObj->prepare($sql, array(
            PDO::ATTR_CURSOR => PDO::CURSOR_FWDONLY
        ));
        $pdoStatement->execute(array()); // .$_SESSION['customer_id']));
        $done = array();
        if ($pdoStatement->errorCode() * 1 != 0) {
            $this->addError('', gettext('SQL Error') . ': ' . print_r($pdoStatement->errorInfo(), true), 1);
        } else {
            $no = $pdoStatement->rowCount();
            while ($row = $pdoStatement->fetch(PDO::FETCH_ASSOC)) {
                $done[$row['room_id']] = 1;
                // print_r($row);
                // print_r($rooms[$row['id']]);
                $row['room_name'] = $rooms[$row['room_id']]['title'];
                $res['room'][] = $row;
            }
        }
        foreach ($rooms as $r) {
            if (! isset($done[$r['id']])) {
                $checkinRoomCount = $checkinRoomList[$r['id']];
                if (! is_numeric($checkinRoomCount)) {
                    $checkinRoomCount = 0;
                }
                $row = array(
                    'room_id' => $r['id'],
                    'pax' => 0,
                    'spax' => 0,
                    'bpax' => $checkinRoomCount,
                    'room_name' => $r['title']
                );
                $res['room'][$r['id']] = $row;
            }
        }
        return $res;
    }

    public function apiPutRecord($subentity = null, $subentityid = null, $put)
    {
        if ($subentity != null) {
            $classname = trim(substr($subentity, 0, - 1));
            $putAllowed = array(
                'regprocstep',
                'guest'
            );
            if ($classname == 'multisetting') {
                $ms = new multisettings();
                if ($_POST['form'] == 'deleteMultisettings') {
                    // $this->addError('', 'multisetting deletion not implemented yet');
                    $res = $ms->removeMultisettingsRecord($this->_recordId, $_POST['multisetting'], $_POST['rowid']);
                } elseif ($_POST['form'] == 'moveMultisettings') {
                    $res = $ms->moveMultisettingEntry($this->_recordId, $_POST['multisetting'], $_POST['seqid'], $_POST['direction']);
                } elseif ($_POST['form'] == 'addmultisetting1n') {
                    $res = $ms->addMultisettings1NRecord($this->_recordId, $put);
                } elseif ($_POST['form'] == 'removemultisetting1n') {
                    $res = $ms->removeMultisetting1n($this->_recordId, $_POST['multiname'], $_POST['multiid'], $_POST['pmultiname'], $_POST['pmultiid']);
                } else {
                    $res = $ms->saveMultisettingsRecord($this->_recordId, $put);
                }
                $this->_err = $ms->getErrors(); // getErrorsInApiFormat();// + $this->_err;
            } elseif ($subentity == 'sendwallettestmail') {
                $res['data'] = $this->sendWalletTestMail($_POST['wallettestemailaddress']);
                // $this->_err = $xx->getErrors();
                
                // jaaaa
            } elseif ($subentity == 'pullremoteprintinvoices') {
                // $g = new entityGuest($this->_recordId, $subentityid, $this->readRecord($_SESSION['fevent_id'], true));
                $res['data'] = $this->pullRemotePrintInvoices($_POST['filter']);
            } elseif ($subentity == 'takeoverfromserver') {
                $this->takeoverfromserver($_POST['ffevent_id'], $_POST['url'], $_POST['username'], $_POST['password']);
            } elseif ($subentity == 'triggerTakeoverPreparation') {
                $res['data'] = $this->triggerTakeoverPreparation($_GET['fevent_id']);
            } elseif ($subentity == 'checkTakeoverPreparation') {
                $res['data'] = $this->checkTakeoverPreparation($_GET['fevent_id'], $_GET['ts'], $_GET['randX']);
            } elseif ($subentity == 'checkPushoverStatus') {
                $res['data'] = $this->checkPushoverStatus($_GET['fevent_id'], $_GET['ts'], $_GET['randX']);
            } elseif ($subentity == 'pushovertoserver') {
                $this->pushovertoserver($_POST['event_id_local'], $_POST['event_id_remote'], $_POST['url'], $_POST['username'], $_POST['password']);
            } elseif ($subentity == 'clearguestlist') {
                $res['data'] = $this->deleteAllGuestData($_POST['fevent_id']);
            } elseif (array_search($classname, $putAllowed) !== false) {
                $xx = new $classname($this->_lg, $this->_locale);
                $res['data'] = $xx->apiPutRecord($put);
                $this->_err = $xx->getErrors();
            } else {
                $this->addError('', gettext('API action does not exist:') . ' ' . $subentity);
            }
        }
        return $res;
    }

    private function deleteAllGuestData($fevent_id)
    {
        try {
            $tablesToTruncate = array(
                'zguest' . $fevent_id,
                'xvl' . $fevent_id,
                'ygr' . $fevent_id
            );
            $this->_pdoObj = dbconnection::getInstance();
            foreach ($tablesToTruncate as $ttt) {
                $sql = "TRUNCATE TABLE `" . $ttt . "`;";
                $pdoStatement = $this->_pdoObj->prepare($sql, array(
                    PDO::ATTR_CURSOR => PDO::CURSOR_FWDONLY
                ));
                $pdoStatement->execute(array());
                if ($pdoStatement->errorCode() * 1 != 0) {
                    $this->addError('', gettext('SQL Error') . ': ' . print_r($pdoStatement->errorInfo(), true), 1);
                } else {
                    $no = $pdoStatement->rowCount();
                }
            }
        } catch (Exception $e) {
            $this->addError('', gettext('Error deleting guest data:') . ' ' . $e->getMessage());
        }
    }

    private function checkTakeoverPreparation($fevent_id, $ts, $randX)
    {
        $data = array();
        $triggerDir = BASEDIR . 'tmp/triggers';
        if (! is_dir($triggerDir)) {
            mkdir($triggerDir);
        }
        $filename = 'takeoverPrep_cust' . $_SESSION['customer']['customer_id'] . '_ev' . $fevent_id . '__' . $ts . '_' . $randX;
        $cts = file_get_contents($triggerDir . '/' . $filename);
        $data['file'] = $filename;
        if (substr($cts, 0, 4) !== 'done') {
            $data['status'] = 'in progress';
        } else {
            $data['status'] = 'done';
        }
        return $data;
    }

    private function checkPushoverStatus($fevent_id, $ts, $randX)
    {
        $data = array();
        $triggerDir = BASEDIR . 'tmp/transfer_in';
        if (! is_dir($triggerDir)) {
            mkdir($triggerDir);
        }
        $filename = 'status_c' . $_SESSION['customer']['customer_id'] . '_e' . $fevent_id . '_ts' . $ts . '_r' . $randX;
        $cts = file_get_contents($triggerDir . '/' . $filename);
        $data['file'] = $filename;
        $d = explode(';', $cts);
        $data['cts'] = $cts;
        $data['status'] = $d[0];
        return $data;
    }

    private function triggerTakeoverPreparation($fevent_id)
    {
        $data = array();
        $triggerDir = BASEDIR . 'tmp/triggers';
        if (! is_dir($triggerDir)) {
            mkdir($triggerDir);
        }
        $randX = $this->generateAccessCode();
        $ts = date('YmdHis');
        $filename = 'takeoverPrep_cust' . $_SESSION['customer']['customer_id'] . '_ev' . $fevent_id . '__' . $ts . '_' . $randX;
        file_put_contents($triggerDir . '/' . $filename, '');
        $data['ts'] = $ts;
        $data['rand'] = $randX;
        $data['fevent_id'] = $fevent_id;
        return $data;
    }

    public function apiDeleteRecord($subentity = null, $subentityid = null)
    {
        if ($subentity != null) {
            $classname = trim(substr($subentity, 0, - 1));
            $deleteAllowed = array(
                'regprocstep'
            );
            if (array_search($classname, $deleteAllowed) !== false) {
                $xx = new $classname($this->_lg, $this->_locale);
                $xx->apiDeleteRecord($this->_recordId, $subentityid);
                $this->_err = $xx->getErrors();
            }
        }
    }

    /**
     * formats the given guest list and stores it as a file in a
     * temporary folder.
     * Returns path of that file.
     *
     * @param array $data
     *            contains guest list data
     * @param string $format
     *            values: ticketsstore
     * @return string
     */
    private function formatGuestList($data, $format)
    {
        switch ($format) {
            case 'ticketsstore':
                /**
                 * generate html code for all tickets and store the html file
                 * in a dedicated, temporary folder
                 */
                $ev = $this->readRecord($_SESSION['fevent_id']);
                $pagehtml = $ev['ticketprinthtmlpage'];
                if ($ev['ticketprinthtmlpage'] == '') {
                    $pagehtml = '[tickets]';
                }
                $counter = 0;
                $firstRow = 1;
                foreach ($data['list'] as $t) {
                    if ($ev['ticketprintperpage'] > 0) {
                        if (($counter) % $ev['ticketprintperpage'] == 0) {
                            if ($firstRow != 1) {
                                $html .= '</div>';
                            } else {
                                $firstRow = 0;
                            }
                            $html .= '<div class="onePage">';
                        }
                    }
                    $html .= $this->printTicketX($_SESSION['fevent_id'], $t['guest_id']);
                    $counter += 1;
                }
                if ($ev['ticketprintperpage'] > 1) {
                    $html .= '</div>';
                }
                $html = str_replace('[tickets]', $html, $pagehtml);
                $fname = date('Ymdhis') . '_' . $_SESSION['fevent_id'] . '_' . rand(100000000000000, 999999999999999) . '.html';
                $handle = fopen(DIR_STORAGE_PRE_PDF . $fname, 'w');
                fwrite($handle, $html);
                fclose($handle);
                $res['tempurl'] = HTTP_STORAGE_PRE_PDF . $fname;
                $res['html2pdfurl'] = HTML2PDF_SERVER;
                $res['pdfmargin'] = $ev['ticketprintpdfmargin'];
                break;
            default:
                $res = 'No no, this format is unknown.';
                break;
        }
        return $res;
    }

    /**
     * sets the record id
     *
     * @param int|null $id
     */
    public function setRecordId($id)
    {
        $res = false;
        if ($this->checkUserEvent($id) === true) {
            $this->_recordId = $id;
            $_SESSION['fevent_id'] = $id;
            $_SESSION['event']['id'] = $id;
            $res = true;
        } else {
            // echo "*************".$_SESSION[user][id]." ".$_SESSION[customer][customer_id]."********************$id*";
            $this->addError('event_id', 'No access to this event id (' . $id . ')');
        }
        return $res;
    }

    public function checkCRMModule($eventid)
    {
        $no = 0;
        $sql = "SHOW COLUMNS FROM `zguest" . $eventid . "` LIKE 'crm_id';";
        $this->_pdoObj = dbconnection::getInstance();
        $pdoStatement = $this->_pdoObj->prepare($sql, array(
            PDO::ATTR_CURSOR => PDO::CURSOR_FWDONLY
        ));
        $pdoStatement->execute(array()); // .$_SESSION['customer_id']));
        if ($pdoStatement->errorCode() * 1 != 0) {
            $this->addError('', gettext('SQL Error') . ': ' . print_r($pdoStatement->errorInfo(), true), 1);
        } else {
            $no = $pdoStatement->rowCount();
        }
        return $no;
    }

    public function apiSpecialFunction($subentity, $subentityid, $specialFunction)
    {
        $data = array();
        if ($subentity == 'guests' && is_numeric($subentityid) && $specialFunction == 'sendPDFInvoice') {
            // invoice generation method should be called here.
            $g = new entityGuest($_SESSION['fevent_id'], $subentityid, $this->readRecord($_SESSION['fevent_id'], true));
            $data = $g->generateInvoicePreview();
        } elseif ($subentity == 'guests' && is_numeric($subentityid) && $specialFunction == 'resendinvoice') {
            $g = new entityGuest($_SESSION['fevent_id'], $subentityid, $this->readRecord($_SESSION['fevent_id'], true));
            $g->resendInvoice($_POST['invoicefile']);
            $this->_err = array_merge($this->_err, $g->getErrors());
        } elseif ($subentity == 'guests' && is_numeric($subentityid) && $specialFunction == 'remoteprintinvoice') {
            $g = new entityGuest($_SESSION['fevent_id'], $subentityid, $this->readRecord($_SESSION['fevent_id'], true));
            $g->remoteprintInvoice($_POST['invoicefile']);
            $this->_err = array_merge($this->_err, $g->getErrors());
        } elseif ($subentity == 'guests' && is_numeric($subentityid) && $specialFunction == 'cancelinvoice') {
            $g = new entityGuest($_SESSION['fevent_id'], $subentityid, $this->readRecord($_SESSION['fevent_id'], true));
            $g->cancelInvoice($_POST['invoiceid'], '---');
            $this->_err = array_merge($this->_err, $g->getErrors());
        } elseif ($subentity == 'guests' && is_numeric($subentityid) && $specialFunction == 'uncancelinvoice') {
            $g = new entityGuest($_SESSION['fevent_id'], $subentityid, $this->readRecord($_SESSION['fevent_id'], true));
            $g->cancelInvoice($_POST['invoiceid'], '---', true);
            $this->_err = array_merge($this->_err, $g->getErrors());
        } elseif ($subentity == 'guests' && is_numeric($subentityid) && $specialFunction == 'generateAndRemotePrintPDFInvoice') {
            $g = new entityGuest($_SESSION['fevent_id'], $subentityid, $this->readRecord($_SESSION['fevent_id'], true));
            $d = $this->generateInvoice($subentityid);
            $data = $g->remoteprintInvoice($d['filename']['filename']);
            $this->_err = array_merge($this->_err, $g->getErrors());
        } elseif ($subentity == 'guests' && is_numeric($subentityid) && $specialFunction == 'serverstoreticket') {
            // generate & save html code
            $ev = $this->readRecord($this->_recordId, true);
            $html = $this->printTicketX($_SESSION['fevent_id'], $subentityid); // $guestid)
            
            $fname = date('Ymdhis') . '_' . $_SESSION['fevent_id'] . '_' . rand(100000000000000, 999999999999999) . '.html';
            $handle = fopen(DIR_STORAGE_PRE_PDF . $fname, 'w');
            fwrite($handle, $html);
            fclose($handle);
            $res['tempurl'] = HTTP_STORAGE_PRE_PDF . $fname;
            $res['html2pdfurl'] = HTML2PDF_SERVER;
            $res['pdfmargin'] = $ev['ticketprintpdfmargin'];
            
            // send to pdf converter
            $longpath = '/' . $_SESSION['customer']['customer_id'] . '/event/' . $_SESSION['fevent_id'] . '/tickets/';
            $path = BASEDIR . 'files' . $longpath;
            $filenameprefix = 'ticket';
            /*
             * if (isset($this->_eventdata['documentfilenameprefix']) && strlen($this->_eventdata['documentfilenameprefix']) > 0) {
             * $filenameprefix = $this->_eventdata['documentfilenameprefix'];
             * }
             */
            $invoiceFilename = $filenameprefix . '_' . $subentityid . '_' . date('Ymd-His') . '.pdf';
            
            $savefilename = $path . $invoiceFilename;
            if (! is_dir($path)) {
                mkdir($path, 0700, true);
            }
            $res['allurl'] = HTML2PDF_SERVER . '?filename=invoice.pdf&url=' . urlencode($res['tempurl']) . '&size=A4&margin=' . urlencode($res['pdfmargin']);
            $ch = curl_init($res['allurl']);
            // echo $res['allurl'];
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            
            $contents = curl_exec($ch);
            if (curl_error($ch)) {
                $this->addError('', 'Error creating PDF from HTML', 1, curl_error($ch));
            } elseif ($contents === FALSE) {
                $this->addError('', 'PDF file not downloaded from PDF generation server.', 1, $res['allurl']);
            }
            curl_close($ch);
            file_put_contents($savefilename, $contents);
            if (! is_file($savefilename) || filesize($savefilename) == 0) {
                $this->addError('', 'Invoice not saved / downloaded from PDF generation server.', 1, 'filename: ' . $savefilename . ' size: ' . filesize($savefilename));
            }
            
            if (filesize($savefilename) < 100) {
                $this->addError('', gettext('Invoice could not be generated.'), 1);
            }
            if (count($this->_err) == 0) {
                $g = new entityGuest($_SESSION['fevent_id'], $subentityid, $this->readRecord($_SESSION['fevent_id'], true));
                $g->logGuestAction(getenv('REMOTE_ADDR'), 'tik', '', $longpath . $invoiceFilename);
                $g->logGuestAction(getenv('REMOTE_ADDR'), 'rpr', '0', $longpath . $invoiceFilename);
            }
            // store file on server
            // log ticket creation
            
            // $g->serverStoreTicket();
            $this->_err = array_merge($this->_err, $g->getErrors());
        } elseif ($subentity == 'guests' && is_numeric($subentityid) && $specialFunction == 'checkinout') {
            $g = new entityGuest($_SESSION['fevent_id'], $subentityid, $this->readRecord($_SESSION['fevent_id'], true));
            $g->guestCheckinCheckout($_POST['guest_id'], $_POST['room_id'], $_POST['checkinout']);
            $this->_err = array_merge($this->_err, $g->getErrors());
        } elseif ($subentity == 'guests' && is_numeric($subentityid) && $specialFunction == 'generateAndSavePDFInvoice') {
            // invoice generation method should be called here.
            $data = $this->generateInvoice($subentityid);
        } elseif ($subentity == 'guests' && is_numeric($subentityid) && $specialFunction == 'generatePDFInvoice') {
            // invoice generation method should be called here.
            $g = new entityGuest($_SESSION['fevent_id'], $subentityid, $this->readRecord($_SESSION['fevent_id'], true));
            /**
             * transaction: ensure that each invoice number is only created once,
             * while at the same time preventing the invoice number to be increased if
             * an invoice cannot be created for any reason.
             */
            $this->_pdoObj->beginTransaction();
            try {
                $data = $g->generateAndSendInvoice($this->getNextInvoiceNumberAndIncrease());
                $this->_err = array_merge($this->_err, $g->getErrors());
                if (count($this->_err) == 0) {
                    $this->_pdoObj->commit();
                } else {
                    $this->_pdoObj->rollBack();
                }
            } catch (Exception $e) {
                $this->_pdoObj->rollBack();
                $this->addError('', gettext('Invoice could not be created or sent.'), 1, print_r($e, true));
            }
        } elseif ($subentity == 'guests' && is_numeric($subentityid) && $specialFunction == 'savePDFInvoice') {
            // invoice generation method should be called here.
            $g = new entityGuest($_SESSION['fevent_id'], $subentityid, $this->readRecord($_SESSION['fevent_id'], true));
            /**
             * transaction: ensure that each invoice number is only created once,
             * while at the same time preventing the invoice number to be increased if
             * an invoice cannot be created for any reason.
             */
            $this->_pdoObj->beginTransaction();
            try {
                $data = $g->generateAndSendInvoice($this->getNextInvoiceNumberAndIncrease(), false);
                $this->_err = array_merge($this->_err, $g->getErrors());
                if (count($this->_err) == 0) {
                    $this->_pdoObj->commit();
                } else {
                    $this->_pdoObj->rollBack();
                }
            } catch (Exception $e) {
                $this->_pdoObj->rollBack();
                $this->addError('', gettext('Invoice could not be created or sent.'), 1, print_r($e, true));
            }
        } elseif ($subentity == 'guests' && is_numeric($subentityid) && $specialFunction == 'addPayment') {
            $g = new entityGuest($_SESSION['fevent_id'], $subentityid, $this->readRecord($_SESSION['fevent_id'], true));
            $data = $g->addPayment();
            $this->_err = array_merge($this->_err, $g->getErrors());
        }
        return $data;
    }

    private function generateInvoice($guest_id)
    {
        $g = new entityGuest($_SESSION['fevent_id'], $guest_id, $this->readRecord($_SESSION['fevent_id'], true));
        /**
         * transaction: ensure that each invoice number is only created once,
         * while at the same time preventing the invoice number to be increased if
         * an invoice cannot be created for any reason.
         */
        $this->_pdoObj->beginTransaction();
        try {
            $invoiceno = $this->getNextInvoiceNumberAndIncrease();
            $pricing = null;
            $data = $g->generateInvoice($invoiceno, $pricing);
            $filename = $g->saveInvoice($data, $invoiceno, $pricing);
            $data['filename'] = $filename;
            $data['fullfilename'] = SITE_LINK . '/api/downloads/1/event/' . $_SESSION['fevent_id'] . '/invoices/' . $filename;
            $this->_err = array_merge($this->_err, $g->getErrors());
            if (count($this->_err) == 0) {
                $this->_pdoObj->commit();
            } else {
                $this->_pdoObj->rollBack();
            }
        } catch (Exception $e) {
            $this->_pdoObj->rollBack();
            $this->addError('', gettext('Invoice could not be created or sent.'), 1, print_r($e, true));
        }
        return $data;
    }

    public function getNextInvoiceNumberAndIncrease()
    {
        $nextNo = null;
        $settingExists = false;
        $sql = "SELECT sval_int FROM fevset_" . $this->_recordId . " WHERE skey = 'nextinvoicenumber';";
        $this->_pdoObj = dbconnection::getInstance();
        
        $pdoStatement = $this->_pdoObj->prepare($sql, array(
            PDO::ATTR_CURSOR => PDO::CURSOR_FWDONLY
        ));
        $pdoStatement->execute(array()); // .$_SESSION['customer_id']));
        if ($pdoStatement->errorCode() * 1 != 0) {
            $this->addError('', gettext('SQL Error') . ': ' . print_r($pdoStatement->errorInfo(), true), 1);
        } elseif ($pdoStatement->rowCount() == 0) {
            $nextNo = 1;
        } else {
            $settingExists = true;
            $row = $pdoStatement->fetch();
            $nextNo = $row['sval_int'];
        }
        
        if (count($this->_err) == 0) {
            // Todo: insert setting, if not available yet!
            if ($settingExists == true) {
                $sql = "UPDATE fevset_" . $this->_recordId . " SET sval_int = IF(sval_int IS NULL,1,sval_int + 1) WHERE skey = 'nextinvoicenumber';";
            } else {
                $sql = "INSERT INTO fevset_" . $this->_recordId . " SET sval_int = 2, skey = 'nextinvoicenumber';";
            }
            $this->_pdoObj = dbconnection::getInstance();
            $pdoStatement = $this->_pdoObj->prepare($sql, array(
                PDO::ATTR_CURSOR => PDO::CURSOR_FWDONLY
            ));
            $pdoStatement->execute(array()); // .$_SESSION['customer_id']));
            if ($pdoStatement->errorCode() * 1 != 0) {
                $this->addError('', gettext('SQL Error') . ': ' . print_r($pdoStatement->errorInfo(), true), 1);
                $nextNo = null;
            }
        }
        return $nextNo;
    }

    /**
     * returns the registration form field sequence
     */
    public function ff($fevent_id, $fevent_data)
    {
        if (is_array($fevent_data['formpage']) && is_array($fevent_data['formsectionfield']) && is_array($fevent_data['formpagesection'])) {
            $data = array();
            $optioncollection = array();
            
            foreach ($fevent_data['formsectionfield'] as $fsf) {
                $sectionfields[$fsf['formsectionid']]['fields'][$fsf['id']] = $fsf;
            }
            
            foreach ($fevent_data['formpagesection'] as $fps) {
                if (! is_array($sections[$fps['id']])) {
                    $sections[$fps['id']] = array();
                }
                foreach ($fps as $fpspropertykey => $fpspropertyval) {
                    if (substr($fpspropertykey, - 4) == 'list') {
                        $fps[$fpspropertykey] = explode(',', $fpspropertyval);
                    }
                }
                $sections[$fps['id']] = $fps;
                $sections[$fps['id']]['fields'] = $sectionfields[$fps['id']];
                // $pages[$fps['formpageid']]['sections'][$fps['id']] = array_merge($fps, $sections[$fps['id']]);
            }
            foreach ($fevent_data['formpage'] as $fp) {
                foreach ($fp['n1formpagesectiontoformpage'] as $fps) {
                    $pages[$fp['id']]['sections'][$fps] = $sections[$fps];
                }
                $data['pages'][$fp['id']] = array_merge($fp, $pages[$fp['id']]);
            }
        }
        if (is_array($data['pages'])) {
            // add stream data
            foreach ($data['pages'] as $pageid => $p) {
                foreach ($p['sections'] as $sectionid => $s) {
                    if ($s['type'] == 'sessionchoice' && $s['streamlist'] && is_array($s['streamlist'])) {
                        $myStreams = array();
                        $mySessions = array();
                        $sessionSorter = array();
                        $sessionDoneList = array();
                        foreach ($s['streamlist'] as $stream) {
                            // $stream is the stream id (for example F1 might stand for Friday, stream 1)
                            // stream name is replaced in the $s['streams'] variable
                            $myTitle = '';
                            foreach ($fevent_data['conferencestream'] as $confstream) {
                                if ($confstream['id'] == $stream) {
                                    $myTitle = $confstream['title'];
                                    $myId = $confstream['id'];
                                    break;
                                }
                            }
                            $myStreams[] = array(
                                'title' => $myTitle,
                                'id' => $myId
                            );
                            $mySession = array();
                            // find all matching sessions now!
                            foreach ($fevent_data['conferencesession'] as $confsession) {
                                if (! isset($sessionDoneList[$confsession['id']])) {
                                    $confsessionStreams = explode(',', $confsession['stream']);
                                    if (array_search($stream, $confsessionStreams) !== false) {
                                        // take over session
                                        $confsessionLinked = explode(',', $confsession['linked']);
                                        if ($confsessionLinked && is_array($confsessionLinked) && strlen($confsessionLinked[0]) > 0) {
                                            $mySession['linked'] = $confsessionLinked;
                                        }
                                        $mySession['stream'] = $confsessionStreams;
                                        $mySession['title'] = $confsession['title'];
                                        $mySession['id'] = $confsession['id'];
                                        $mySession['type'] = $confsession['type'];
                                        $mySession['datetime'] = $this->datedbtofl($confsession['datetime']);
                                        $mySession['datetimeto'] = $this->datedbtofl($confsession['datetimeto']);
                                        $mySession['speaker'] = $confsession['speaker'];
                                        $mySession['speakerpic'] = $confsession['speakerpic'];
                                        $mySession['descr'] = $confsession['descr'];
                                        $mySession['no'] = $confsession['no'];
                                        $mySessions[] = $mySession;
                                        $sessionSorter[count($mySessions) - 1] = $mySession['datetime'];
                                        $sessionDoneList[$mySession['id']] = 1;
                                    }
                                }
                            }
                        }
                        if (count($myStreams) > 0) {
                            $data['pages'][$pageid]['sections'][$sectionid]['streams'] = $myStreams;
                        }
                        if (count($mySessions) > 0) {
                            asort($sessionSorter);
                            $sessionList = array();
                            foreach ($sessionSorter as $mySessionId => $sdatetime) {
                                $sessionList[] = $mySessions[$mySessionId];
                            }
                            $data['pages'][$pageid]['sections'][$sectionid]['sessions'] = $sessionList;
                        }
                    }
                }
            }
        }
        
        $tcs = array();
        if (is_array($fevent_data['ticketcategory'])) {
            foreach ($fevent_data['ticketcategory'] as $tc) {
                $tcs[] = $tc;
            }
        }
        $data['ticketcategory'] = $tcs;
        
        return $data;
    }

    public function apiGetGrandchildRecords($childentityname, $childentityid, $grandchildentity)
    {
        $data = array(
            ''
        );
        if ($childentityname == 'guests' && $grandchildentity == 'invoices') {
            $g = new entityGuest($this->_recordId, $childentityid);
            $data['records'] = $g->getGuestInvoiceList();
        } elseif ($childentityname == 'guests' && $grandchildentity == 'payments') {
            $g = new entityGuest($this->_recordId, $childentityid);
            $data['records'] = $g->getGuestPaymentList();
        } else {
            $this->addError('', gettext('Method or entity not available.') . ' ' . $childentityname, 1, $childentityname . ', ' . $childentityid . ', ' . $grandchildentity);
        }
        return $data;
    }

    public function apiSpecialPostFunction($ps)
    {
        $data = array();
        
        switch ($ps['form']) {
            case 'addmultisetting':
                $this->addMultiSetting($ps);
                break;
        }
        return $data;
    }

    private function readMultiSetting($multisettingname, $evdata)
    {
        $data = array();
        try {
            $assignments = array();
            foreach ($evdata as $dkey => $dval) {
                if (substr($dkey, 0, strlen($multisettingname) + 1) == $multisettingname . '_') {
                    $extract = explode('_', $dkey);
                    $data[$extract[1]][$extract[2]] = $dval;
                    if ($extract[2] == 'id') {
                        $assignments[$dval] = $extract[1];
                    }
                }
            }
            if ($this->_multisettings[$multisettingname]['1n']) {
                foreach ($this->_multisettings[$multisettingname]['1n'] as $n1) {
                    foreach ($evdata as $dkey => $dval) {
                        if (substr($dkey, 0, strlen($n1['multisettingname']) + 1) == $n1['multisettingname'] . '_') {
                            $extract = explode('_', $dkey);
                            $data[$assignments[$extract[1]]][$n1['multisettingname']][$extract[2]] = $dval;
                        }
                    }
                }
            }
            if (is_array($data)) {
                ksort($data);
            }
        } catch (Exception $e) {
            $this->addError('', gettext('Problem reading multisetting field') . ' ' . $multisettingname, 1);
        }
        return $data;
    }

    private function addMultiSetting($ps)
    {
        
        // which multisetting?
        $multisettingname = $ps['multisetting'];
        
        // check highest multisetting number
        $nextNumber = $this->getMultisettingHighestNumber($multisettingname) + 1;
        // echo "next " . $nextNumber;
        // insert multisetting
        $tablename = 'fevset_' . $this->_recordId;
        for ($i = 0; $i < count($this->_multisettings[$multisettingname]['fields']); $i ++) {
            $purefieldname = $this->_multisettings[$multisettingname]['fields'][$i]['name'];
            $fieldname = $multisettingname . '_' . $nextNumber . '_' . $purefieldname;
            $fieldtype = $this->_multisettings[$multisettingname]['fields'][$i]['type'];
            
            $sql = "INSERT INTO " . $tablename . " SET skey = '" . $fieldname . "', ";
            if ($fieldtype == 'char') {
                $sql .= "sval_vch = ";
            } elseif ($fieldtype == 'int') {
                $sql .= "sval_int = ";
            } elseif ($fieldtype == 'text') {
                $sql .= "sval_txt = ";
            } elseif ($fieldtype == 'datetime') {
                $sql .= "sval_dtt = ";
            } elseif ($fieldtype == 'decimal') {
                $sql .= "sval_dec = ";
            }
            $sql .= ":" . $purefieldname . ";";
            $sqlArr = array(
                ':' . $purefieldname => $ps[$purefieldname]
            );
            $pdoStatement = $this->_pdoObj->prepare($sql, array(
                PDO::ATTR_CURSOR => PDO::CURSOR_FWDONLY
            ));
            $pdoStatement->execute($sqlArr);
            if ($pdoStatement->errorCode() * 1 != 0) {
                $importErrors[] = 'SQL Fehler' . print_r($pdoStatement->errorInfo(), true);
            } else {
                // echo "OK";
            }
        }
    }

    public function getMultisettingHighestNumber($multisettingname, $parentid = NULL)
    {
        $no = 0;
        // echo "my event id is set to: " . $this->_recordId . PHP_EOL;
        try {
            $sqlArr = array(
                ':multiname' => $multisettingname
            );
            
            $sql = "SELECT skey_multiseq FROM fevset_" . $this->_recordId . " 
					WHERE skey_multiname = :multiname ";
            if ($parentid === NULL) {
                $sql .= " AND pskey_multiid IS NULL ";
            } else {
                $sql .= " AND pskey_multiid = :parent_multiid ";
                $sqlArr[':parent_multiid'] = $parentid;
            }
            $sql .= "
					ORDER BY skey_multiseq DESC
					LIMIT 1;";
            $this->_pdoObj = dbconnection::getInstance();
            $pdoStatement = $this->_pdoObj->prepare($sql, array(
                PDO::ATTR_CURSOR => PDO::CURSOR_FWDONLY
            ));
            // echo $sql; print_r($sqlArr);
            $pdoStatement->execute($sqlArr);
            if ($pdoStatement->errorCode() * 1 != 0) {
                // echo "ERR!!!";
                $this->addError('', gettext('Error reading multisettings from database.'), 1, $sql);
            } elseif ($pdoStatement->rowCount() > 0) {
                // echo "OK!";
                $row = $pdoStatement->fetch(\PDO::FETCH_ASSOC);
                $no = $row['skey_multiseq'] * 1;
            } else {
                // echo "ERR2";
                // $this->addError('x', $sql . ', ' . print_r($sqlArr, true));
                $no = 0;
            }
        } catch (Exception $e) {
            $this->addError('', gettext('Error reading highest multisetting number.'), 1, print_r($e, true));
        }
        return $no;
    }

    private function datedbtofl($dbdate)
    {
        $dbdate = str_replace(' ', '', $dbdate);
        $dbdate = str_replace('-', '', $dbdate);
        $dbdate = str_replace(':', '', $dbdate);
        $dbdate = substr($dbdate, 0, - 2);
        return $dbdate;
    }

    /**
     * written for the xml settings import when creating a new event / cloning an event .
     *
     *
     * ..
     *
     * @param unknown $fevent_id
     * @param unknown $multisettingtype
     * @param unknown $multisettingsettings
     */
    private function storeMultiSetting($fevent_id, $multisettingtype, $multisettingsettings)
    {
        $mss = multisettings::getMultiSettingTypes();
        $mssType = array();
        foreach ($mss as $msname => $ms) {
            foreach ($ms['fields'] as $fld1) {
                $mssType[$msname][$fld1['name']] = $fld1['type'];
            }
        }
        
        foreach ($multisettingsettings as $mssKey => $allfieldvalue) {
            if (substr($mssKey, 0, 2) == 'n1') {
                $ms = new multisettings();
                foreach ($allfieldvalue as $n1recid => $n1rec) {
                    $data = array();
                    $data['skey'] = $n1rec['skey'];
                    $data['skey_multiid'] = $n1rec['skey_multiid'];
                    $data['skey_multiseq'] = $n1rec['skey_multiseq'];
                    $data['multiname'] = $mssKey;
                    $data['parentmultiid'] = $n1rec['pkey_multiid'];
                    $data['parentmultiname'] = $n1rec['pkey_multiname'];
                    $data['val'] = $n1rec['sval'];
                    $ms->addMultisettings1NRecord($fevent_id, $data, false);
                }
            } elseif ($mssKey != 'seq') {
                
                $ftype = $mssType[$multisettingtype][$mssKey];
                $sqlArr = array(
                    'skey' => $multisettingtype . '_' . $multisettingsettings['seq'] . '_' . $mssKey,
                    'sval_txt' => null,
                    'sval_int' => null,
                    'sval_vch' => null,
                    'sval_dtt' => null,
                    'sval_dec' => null
                );
                switch ($ftype) {
                    case 'text':
                        $sqlArr['sval_txt'] = $allfieldvalue;
                        break;
                    case 'int':
                        $sqlArr['sval_int'] = $allfieldvalue;
                        break;
                    case 'char':
                        $sqlArr['sval_vch'] = $allfieldvalue;
                        break;
                    case 'datetime':
                        $sqlArr['sval_dtt'] = $allfieldvalue;
                        break;
                    case 'decimal':
                        $sqlArr['sval_dec'] = $allfieldvalue;
                        break;
                }
                
                $sql = "INSERT INTO `fevset_" . $fevent_id . "`
									(skey, sval_txt, sval_int, sval_vch, sval_dtt, sval_dec, 
									skey_multiname, skey_multiseq, skey_multikey, skey_multiid)
								VALUES (:skey, :sval_txt, :sval_int, :sval_vch, :sval_dtt, :sval_dec,
									:skey_multiname, :skey_multiseq, :skey_multikey, :skey_multiid)
								ON DUPLICATE KEY UPDATE sval_txt = :sval_txt,
									sval_int = :sval_int, sval_vch = :sval_vch,
									sval_dtt = :sval_dtt, sval_dec = :sval_dec,
									skey_multiname = :skey_multiname, skey_multiseq = :skey_multiseq,
									skey_multikey = :skey_multikey, skey_multiid = :skey_multiid;";
                $pdoStatement = $this->_pdoObj->prepare($sql, array(
                    PDO::ATTR_CURSOR => PDO::CURSOR_FWDONLY
                ));
                $sqlArr[':skey_multiname'] = $multisettingtype;
                $sqlArr[':skey_multiseq'] = $multisettingsettings['seq'];
                $sqlArr[':skey_multikey'] = $mssKey;
                $sqlArr[':skey_multiid'] = $multisettingsettings['id'];
                $pdoStatement->execute($sqlArr);
                if ($pdoStatement->errorCode() * 1 != 0) {
                    $this->addError('', gettext('SQL Error') . print_r($pdoStatement->errorInfo(), true), 1);
                }
            }
        }
    }

    private function sendWalletTestMail($emailaddress)
    {
        $eventdata = $this->readRecord($this->_recordId);
        $g = new entityGuest($this->_recordId, 0, $eventdata);
        
        $fictionalguestdata = array(
            'firstname' => 'Max',
            'lastname' => 'Mustermann',
            'company' => 'Musterfirma GmbH',
            'guest_id' => '19280850923'
        );
        $walletpass = $g->generateWalletpass($fictionalguestdata);
        
        if (strlen($walletpass) > 0) {
            $res3att = array(
                'content' => $walletpass,
                'filename' => 'pass.pkpass',
                'encoding' => 'base64',
                'type' => 'application/vnd.apple.pkpass'
            );
        } else {
            $res3att = null;
            $this->addError('', 'Wallet Pass could not be generated.', 1);
        }
        
        $subject = 'Wallet Test Mail';
        $mailcontenthtml = 'See attachment';
        
        $mimemesg = $this->sendEmailToRecipient($subject, $mailcontenthtml, gettext('This is an html email.'), $emailaddress, $eventdata, array(), array(), array(
            $res3att
        ));
        // $this->logMessage($mimemesg);
        
        return $res;
    }

    public function pullRemotePrintInvoices($filter)
    {
        $invlist = array();
        try {
            $invlist = $this->readAllUnprintedRemotePrintInvoices($filter);
            /*
             * $invoicefname = '/' . $_SESSION['customer']['customer_id'] . '/event/' . $_SESSION['fevent_id'] . '/invoices/'.$invoicefilename;
             * $path = BASEDIR . 'files' . $invoicefilename;
             * $invoicefilename = $path;
             * if (!is_file($invoicefilename)) {
             * $this->addError('', gettext('The invoice cannot be found and not printed.'));
             * } elseif (filesize($invoicefilename) == 0) {
             * $this->addError('', gettext('There was a problem reading the generated invoice. Please send it manually.'));
             * } else {
             * $this->logGuestAction(getenv('REMOTE_ADDR'), 'rprtwait', '', $invoicefname);
             * }
             */
        } catch (Exception $e) {
            $this->addError('', 'Error resending invoice.', 1, print_r($e, true));
        }
        $invlist = array(
            'documentlist' => $invlist
        );
        return $invlist;
    }

    private function readAllUnprintedRemotePrintInvoices($filter = null)
    {
        $inv = array();
        
        $sqlFilt = "";
        if ($filter != null) {
            if ($filter == 'inv') {
                $sqlFilt = " AND suppdata LIKE '%invoices%' ";
            } elseif ($filter = 'tik') {
                $sqlFilt = " AND suppdata LIKE '%tickets%' ";
            }
        }
        
        $sql = "SELECT * FROM xvl" . $this->_recordId . " 
				WHERE `inout` = 'rpr' " . $sqlFilt . " 
				AND room = '0';";
        $pdoStatement = $this->_pdoObj->prepare($sql, array(
            PDO::ATTR_CURSOR => PDO::CURSOR_FWDONLY
        ));
        $sqlArr = array();
        $pdoStatement->execute($sqlArr);
        if ($pdoStatement->errorCode() * 1 != 0) {
            $this->addError('', gettext('SQL Error') . print_r($pdoStatement->errorInfo(), true), 1);
        } else {
            while ($row = $pdoStatement->fetch(PDO::FETCH_ASSOC)) {
                $inv[] = '/api/downloads' . $row['suppdata'];
            }
        }
        $sql = "UPDATE xvl" . $this->_recordId . "
					SET room = '1' 
					WHERE `inout` = 'rpr' " . $sqlFilt . " 
					AND room = '0';";
        $sqlArr = array();
        $pdoStatement = $this->_pdoObj->prepare($sql, array(
            PDO::ATTR_CURSOR => PDO::CURSOR_FWDONLY
        ));
        $pdoStatement->execute($sqlArr);
        if ($pdoStatement->errorCode() * 1 != 0) {
            $inv = array();
            $this->addError('', 'xx', 1, print_r($pdoStatement->errorInfo()));
        } elseif ($pdoStatement->rowCount() == 0) {
            $inv = array();
        }
        
        return $inv;
    }

    public function updateSessionGuestCounter()
    {
        $count = 0;
        $fevent_data = $this->readRecord($this->_recordId, true);
        
        // creating a list of all session choice fields
        $sessionFields = array();
        foreach ($fevent_data['formpagesection'] as $fps) {
            if ($fps['type'] == 'sessionchoice') {
                $sessionFields[] = $fps['name'];
            }
        }
        
        $sql = "SELECT * FROM zguest" . $this->_recordId . " 
				WHERE deleted = 0 AND archived = 0 ORDER BY guest_id ASC;";
        
        $pdoStatement = $this->_pdoObj->prepare($sql, array(
            \PDO::ATTR_CURSOR => \PDO::CURSOR_FWDONLY
        ));
        
        $sqlArr = array();
        $pdoStatement->execute($sqlArr);
        if ($pdoStatement->errorCode() * 1 != 0) {
            print_r($pdoStatement->errorInfo());
        } else {
            $sessionOccupancy = array();
            while ($row = $pdoStatement->fetch()) {
                foreach ($sessionFields as $sessionField) {
                    $sessions = explode(',', $row[$sessionField]);
                    foreach ($sessions as $sess) {
                        $sessionOccupancy[$sess] += 1;
                    }
                }
            }
            foreach ($sessionOccupancy as $sessid => $so) {
                $sql = "UPDATE fevset_" . $this->_recordId . " SET sval_int = :occupancy
					WHERE skey_multiname = 'conferencesession'
					AND skey_multikey = 'bookedpax'
					AND skey_multiid = :sessid;";
                $pdoStatement = $this->_pdoObj->prepare($sql, array(
                    PDO::ATTR_CURSOR => PDO::CURSOR_FWDONLY
                ));
                $sqlArr = array(
                    ':sessid' => $sessid,
                    ':occupancy' => $so
                );
                $pdoStatement->execute($sqlArr);
                if ($pdoStatement->errorCode() * 1 != 0) {
                    $this->addError('', gettext('SQL Error') . print_r($pdoStatement->errorInfo(), true), 1);
                }
            }
        }
    }
}
