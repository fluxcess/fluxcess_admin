<?php
use PKPass\PKPass;

/**
 * contains the class entityGuest
 *
 * @author Line5
 *        
 */

/**
 * guest object
 *
 * @author Line5
 *        
 */
class entityGuest extends l5sys
{

    /**
     * Data of this guest
     *
     * @var array
     */
    private $_guestdata = array();

    /**
     * Id of the currently loaded guest
     *
     * @var int
     */
    private $_guestid = 0;

    /**
     * Id of the currently selected event
     *
     * @var int
     */
    private $_eventid = 0;

    /**
     * Data of this event
     *
     * @var array
     */
    private $_eventdata = array();

    private $_paymentstatus = array();

    private $_actiontype = array();

    /**
     * constructor for the guest object
     *
     * @param integer $eventid
     * @param integer $guestid
     * @param array $eventdata
     */
    function __construct($eventid, $guestid, $eventdata = null, $evenIfDeleted = false, $checkinDataOnly = false)
    {
        $this->_eventid = $eventid;
        $this->_guestid = $guestid;
        $this->_paymentstatus = array(
            - 1 => gettext('Reset (reset invoice to unpaid, status -1)'),
            0 => gettext('Payment failed (Invoice not paid yet, status 0)'),
            2 => gettext('Partial Payment (Invoice not fully paid yet, status 2)'),
            16 => gettext('Full Payment (Account balanced)'),
            22 => gettext('Not relevant')
        );
        $this->_actiontype = array(
            'act' => gettext('Registered at Checkpoint'),
            'in' => gettext('Checked in'),
            'inv' => gettext('Invoice printed.'),
            'out' => gettext('Checked out'),
            'msg' => gettext('Email sent'),
            'pay' => gettext('Payment information'),
            'prt' => gettext('Ticket printed.')
        );
        $this->_eventdata = $eventdata;
        if ($guestid != 0) {
            if ($this->readGuestData($evenIfDeleted, $checkinDataOnly) !== true) {
                $this->_guestid = 0;
            }
        }
        $this->_eventdata = $eventdata;
    }

    /**
     * returns the record data of the guest as an array
     * usually, n:m entities (invoices, for example) are included in the result.
     * with the first parameter set to "false", these are not included.
     *
     * @return array
     */
    public function getData($returnnmentities = true)
    {
        $gd = $this->_guestdata;
        if ($returnnmentities === false) {
            unset($gd['invoices']);
            unset($gd['invoicesent']);
            unset($gd['actionlog']);
            unset($gd['payments']);
            unset($gd['paymentstatus']);
            unset($gd['paymenttype']);
            unset($gd['actiontype']);
            unset($gd['checkin']);
        }
        return $gd;
    }

    /**
     * replaces variable names in [<varname>] constructs.
     *
     * @param string $html
     * @param string $foremail
     *            determines if the returned html code is for emails
     * @return string
     */
    public function createCustomizedText($html, $foremail = true, $leftdelimit = '[', $rightdelimit = ']')
    {
        if ($foremail == false) {
            if (LIVEENV === true) {
                $codeurl = 'https://check-in.fast-lane.org/qr/' . $this->_eventid . '/' . $this->_guestdata['lastname'] . '/' . $this->_guestdata['accesscode'];
                $html = str_replace('[#qr_ticket_open]', '<img src="http://qr.line5.net/png.php?content=' . urlencode($codeurl) . '&qr.png" />', $html);
            } else {
                $codeurl = 'https://check-in.fast-lane.org/qr/' . $this->_eventid . '/' . $this->_guestdata['lastname'] . '/' . $this->_guestdata['accesscode'];
                $html = str_replace('[#qr_ticket_open]', '<img src="https://qr.line5.net/png.php?content=' . urlencode($codeurl) . '&qr.png" />', $html);
            }
        }
        $html = preg_replace_callback('/(\\' . $leftdelimit . '{1})(.*?)(\\' . $rightdelimit . '{1})/s', array(
            $this,
            'replaceCallback'
        ), $html);
        return $html;
    }

    /**
     * returns the QR Code content for the currently chosen event + guest
     *
     * @return string
     */
    public function getCheckInQRCode()
    {
        $codeurl = 'https://check-in.fast-lane.org/qr/' . $this->_eventid . '/' . $this->_guestdata['lastname'] . '/' . $this->_guestdata['accesscode'];
        if (LIVEENV === true) {
            $html = '<img src="http://qr.line5.net/png.php?content=' . urlencode($codeurl) . '&qr.png" />';
        } else {
            $html = '<img src="https://qr.line5.net/png.php?content=' . urlencode($codeurl) . '&qr.png" />';
        }
        return $html;
    }

    /**
     * replaces matches with $data[$match]
     *
     * @param string $match
     * @param array $data
     * @return string
     */
    protected function replaceCallback($match)
    {
        $data = '';
        if (substr($match[2], 0, 1) == '#') {
            $matchparams = explode('|', $match[2]);
            $match[2] = $matchparams[0];
            switch ($match[2]) {
                case '#if':
                    $combinations = explode('&&', $matchparams[1]);
                    $isTrue = true;
                    foreach ($combinations as $combination) {
                        $equality = explode('=', $combination);
                        if ($this->_guestdata[$equality[0]] != $equality[1]) {
                            $isTrue = false;
                        }
                    }
                    if ($isTrue == true) {
                        $data = $matchparams[2];
                    }
                    break;
                case '#img':
                    srand();
                    $piccounter = rand(0, 999999);
                    $mycid = 'pic_' . $piccounter;
                    // $data = '<img src="cid:' + $cid + '" alt="" />';// + $matchparams[2] + '" />';
                    $data = '<img src="cid:' . $mycid . '" alt="' . $matchparams[2] . '" />';
                    $this->_emailAttachedImages[] = array(
                        'url' => $matchparams[1],
                        'cid' => $mycid
                    );
                    break;
                case '#sessionlist':
                    $data = $this->generateSessionOverview();
                    break;
                case '#sessionlist2':
                    $data = $this->generateSessionOverview2();
                    break;
                case '#today':
                    $data = date('d.m.Y');
                    break;
                case '#qr_ticket_open':
                    // $this->_emailAttachedImages[] = 'qr_ticket_open';
                    $data = '<img src="cid:qr_ticket_open" alt="QR Code Ticket" />';
                    break;
                case "#qr_ticket_vcard":
                    $data = '<img src="cid:qr_ticket_vcard" alt="QR Code Ticket" />';
                    break;
            }
        } elseif (substr($match[2], 0, 3) == 'ev.') {
            $data = $this->_eventdata[substr($match[2], 3)];
            if (count($this->_eventdata) == 0) {
                $this->addError('', 'Error: empty event data', 1, __FILE__ . '/' . __LINE__ . '  ');
            }
        } else {
            $data = $this->_guestdata[$match[2]];
        }
        return ($data);
    }

    /**
     * reads the guest data from the database and stores it in the
     * $this->_guest variable
     *
     * @return boolean determines if the operation was successful
     */
    private function readGuestData($evenIfDeleted = false, $checkinDataOnly = false)
    {
        $ok = false;
        $this->_pdoObj = dbconnection::getInstance();
        // get columns
        $tablename = 'zcolumn' . $this->_eventid;
        $sql = "SELECT fieldname FROM " . $tablename . " WHERE 1 = 1 ";
        $sql .= " AND fieldtype NOT IN (8, 9) "; // admin area must not load html fields
        $sql .= " AND parent_field_id = 0;";
        $pdoStatement = $this->_pdoObj->prepare($sql, array(
            PDO::ATTR_CURSOR => PDO::CURSOR_FWDONLY
        ));
        $pdoStatement->execute(array());
        if ($pdoStatement->errorCode() != 0) {
            $this->addError('', gettext('SQL Error'), 1, __FILE__ . '/' . __LINE__ . ' ' . print_r($pdoStatement->errorInfo(), true), 1, $sql);
        } elseif ($pdoStatement->rowCount() == 0) {
            $this->addError('', gettext('SQL Error'), 1, __FILE__ . '/' . __LINE__ . ' Errorcode 0: ' . print_r($pdoStatement->errorInfo(), true) . $sql);
        } else {
            while ($row = $pdoStatement->fetch()) {
                $columns[] = $row['fieldname'];
            }
        }
        // read guest data
        $tablename = 'zguest' . $this->_eventid;
        $sql = "SELECT deleted, ";
        if (is_array($columns)) {
            foreach ($columns as $col) {
                $sql .= $col . ", ";
            }
            $sql = substr($sql, 0, - 2);
        }
        try {
            
            $sql .= " FROM " . $tablename . " WHERE guest_id = :guest ";
            if ($evenIfDeleted === false) {
                $sql .= " AND deleted = 0 ";
            }
            $sql .= " AND archived = 0;";
            $pdoStatement = $this->_pdoObj->prepare($sql, array(
                PDO::ATTR_CURSOR => PDO::CURSOR_FWDONLY
            ));
            $pdoStatement->execute(array(
                ':guest' => $this->_guestid
            ));
            if ($pdoStatement->errorCode() != 0) {
                $this->addError('', gettext('SQL Error'), 1, __FILE__ . '/' . __LINE__ . ' ' . print_r($pdoStatement->errorInfo(), true), 1, 'SQL: ' . $sql . print_r($sqlArr, true));
            } elseif ($pdoStatement->rowCount() == 0) {
                $this->addError('', gettext('The guest with this id does not exist in the database. The record has probably been deleted.') . ' (' . $this->_guestid . ')', 1, __FILE__ . '/' . __LINE__ . ' Errorcode 0: ' . print_r($pdoStatement->errorInfo(), true));
            } else {
                $this->_guestdata = $pdoStatement->fetch(\PDO::FETCH_ASSOC);
                $ok = true;
            }
        } catch (PDOException $e) {
            $this->addError('', 'DB Error', 1, $e->getMessage() . $sql);
        }
        if ($checkinDataOnly === false) {
            $this->_guestdata['invoices'] = $this->getGuestInvoiceList();
        }
        $this->_guestdata['actionlog'] = $this->getGuestActionLog($checkinDataOnly);
        if ($checkinDataOnly === false) {
            $this->_guestdata['payments'] = $this->getGuestPaymentList();
        }
        $this->_guestdata['checkin'] = $this->getGuestCheckinInformation();
        $this->_guestdata['paymentstatus'] = $this->_paymentstatus;
        $this->_guestdata['actiontype'] = $this->_actiontype;
        $this->_guestdata['invoicesent'] = 0;
        if ($this->_guestdata['invoices'] > 0) {
            $this->_guestdata['invoicesent'] = 1;
        }
        return $ok;
    }

    /**
     * returns the email address of the currently selected guest
     *
     * @return string contains the email address
     */
    public function getEmailAddress()
    {
        return $this->_guestdata['emailaddress'];
    }

    /**
     * returns an array with a list of invoices for the current guest
     * result[<rowno>]['suppdata'] contains the actual file name of the invoice
     * if the returned array is empty, no invoices have been created for the customer yet
     */
    public function getGuestInvoiceList()
    {
        $invoices = array();
        try {
            $tablename = 'xvl' . $this->_eventid;
            $sql = "SELECT i.*,
						IF (flg01=0,
						(SELECT room FROM `" . $tablename . "` p
								WHERE parent_id = i.id 
								AND `inout` = 'pay'
						ORDER BY ts DESC LIMIT 1),22) paymentstatus,
						(SELECT suppdata FROM `" . $tablename . "` p
								WHERE parent_id = i.id 
								AND `inout` = 'pay'
						ORDER BY ts DESC LIMIT 1) paymentcomment,
						(SELECT ts FROM `" . $tablename . "` p
								WHERE parent_id = i.id 
								AND `inout` = 'pay'
						ORDER BY ts DESC LIMIT 1) paymenttimestamp
					FROM `" . $tablename . "` i
					WHERE guest_id = :guest_id
					AND `inout` = :inout;";
            $pdoStatement = $this->_pdoObj->prepare($sql, array(
                \PDO::ATTR_CURSOR => \PDO::CURSOR_FWDONLY
            ));
            $sqlArr = array(
                ':guest_id' => $this->_guestdata['guest_id'],
                ':inout' => 'inv'
            );
            $pdoStatement->execute($sqlArr);
            if ($pdoStatement->errorCode() != 0) {
                $this->addError('', gettext('SQL Error'), 1, __FILE__ . '/' . __LINE__ . ' ' . print_r($pdoStatement->errorInfo(), true));
            } elseif ($pdoStatement->rowCount() > 0) {
                while ($row = $pdoStatement->fetch(\PDO::FETCH_ASSOC)) {
                    for ($i = 1; $i < 9; $i ++) {
                        $row['fdec' . $i] = number_format($row['dec' . $i], 2, ',', '');
                    }
                    $invoices[] = $row;
                }
            }
        } catch (Exception $e) {
            $this->addError('', gettext('Error reading invoices from the database.'), 1, print_r($e, true));
        }
        return $invoices;
    }

    /**
     * returns an array with a list of invoices for the current guest
     * result[<rowno>]['suppdata'] contains the actual file name of the invoice
     * if the returned array is empty, no invoices have been created for the customer yet
     */
    public function getGuestPaymentList()
    {
        $payments = array();
        try {
            $tablename = 'xvl' . $this->_eventid;
            $sql = "SELECT i.*, 
					inv.suppdata invoicename
					FROM `" . $tablename . "` i
					JOIN `" . $tablename . "` inv ON i.parent_id = inv.id 
					WHERE i.guest_id = :guest_id
					AND `i`.`inout` = :inout;";
            $pdoStatement = $this->_pdoObj->prepare($sql, array(
                \PDO::ATTR_CURSOR => \PDO::CURSOR_FWDONLY
            ));
            $sqlArr = array(
                ':guest_id' => $this->_guestdata['guest_id'],
                ':inout' => 'pay'
            );
            $pdoStatement->execute($sqlArr);
            if ($pdoStatement->errorCode() != 0) {
                $this->addError('', gettext('SQL Error'), 1, __FILE__ . '/' . __LINE__ . ' ' . print_r($pdoStatement->errorInfo(), true));
            } elseif ($pdoStatement->rowCount() > 0) {
                while ($row = $pdoStatement->fetch(\PDO::FETCH_ASSOC)) {
                    $row['pstatus'] = gettext($this->_paymentstatus[$row['room']]);
                    $payments[] = $row;
                }
            }
        } catch (Exception $e) {
            $this->addError('', gettext('Error reading payment list from the database.'), 1, print_r($e, true));
        }
        return $payments;
    }

    /**
     * returns an array with a list of invoices for the current guest
     * result[<rowno>]['suppdata'] contains the actual file name of the invoice
     * if the returned array is empty, no invoices have been created for the customer yet
     */
    public function getGuestActionLog($checkinDataOnly = false)
    {
        $actions = array();
        try {
            $tablename = 'xvl' . $this->_eventid;
            $sql = "SELECT i.*,
						(SELECT room FROM `" . $tablename . "` p
								WHERE parent_id = i.id
								AND `inout` = 'pay'
						ORDER BY ts DESC LIMIT 1) paymentstatus,
						(SELECT suppdata FROM `" . $tablename . "` p
								WHERE parent_id = i.id
								AND `inout` = 'pay'
						ORDER BY ts DESC LIMIT 1) paymentcomment, 
						`inout` actiontype,
						`room` details1,
						`suppdata` suppdata
					FROM `" . $tablename . "` i
					WHERE guest_id = :guest_id
					";
            if ($checkinDataOnly === true) {
                $sql .= " AND (`inout` = 'in' OR `inout` = 'out') ";
            }
            $pdoStatement = $this->_pdoObj->prepare($sql, array(
                \PDO::ATTR_CURSOR => \PDO::CURSOR_FWDONLY
            ));
            $sqlArr = array(
                ':guest_id' => $this->_guestdata['guest_id']
            );
            $pdoStatement->execute($sqlArr);
            if ($pdoStatement->errorCode() != 0) {
                $this->addError('', gettext('SQL Error'), 1, __FILE__ . '/' . __LINE__ . ' ' . print_r($pdoStatement->errorInfo(), true));
            } elseif ($pdoStatement->rowCount() > 0) {
                while ($row = $pdoStatement->fetch(\PDO::FETCH_ASSOC)) {
                    $actions[] = $row;
                }
            }
        } catch (Exception $e) {
            $this->addError('', gettext('Error reading guest action log from the database.'), 1, print_r($e, true));
        }
        return $actions;
    }

    public function generateInvoicePreview()
    {
        try {
            $res = $this->generateInvoice(null);
        } catch (Exception $e) {
            $this->addError('', gettext('Invoice creation was not successful.'), 1, print_r($e, true));
        }
        $res['err'] = $this->_err;
        return $res;
    }

    public function generateInvoice($invoiceno = null, &$pricing)
    {
        $pricing = null;
        $this->_guestdata['invoiceno'] = $invoiceno;
        $this->_eventdata['shoppingcarthtml'] = $this->generateInvoiceShoppingcart1($pricing);
        $tpl = $this->_eventdata['invoicehtmlcode'];
        if ($this->_eventdata['invoicehtmlcode'] == '') {
            if (! isset($this->_eventdata['invoicehtmltemplate_id']) || $this->_eventdata['invoicehtmltemplate_id'] < 1) {
                $invh = gettext('Invoice / Ticket for') . ' [firstname] [lastname]<br /><br />';
                $invh .= gettext('Please customize this text in the settings.');
                // $this->_eventdata['invoicehtmlcode'] = $invh;
                $tpl = $invh;
            } else {
                $emtpl = new emailtemplate($this->_lg, $this->_locale);
                $emtpl->setRecordId($this->_eventdata['invoicehtmltemplate_id']);
                $emtpldata = $emtpl->getData();
                $tpl = $emtpldata['content'];
            }
        }
        $html = $this->createCustomizedText($tpl);
        $fname = date('Ymdhis') . '_' . $_SESSION['fevent_id'] . '_' . rand(100000000000000, 999999999999999) . '.html';
        $handle = fopen(DIR_STORAGE_PRE_PDF . $fname, 'w');
        fwrite($handle, $html);
        fclose($handle);
        $res['tempurl'] = HTTP_STORAGE_PRE_PDF . $fname;
        $res['html2pdfurl'] = HTML2PDF_SERVER;
        $res['pdfmargin'] = '0cm'; // $ev['ticketprintpdfmargin'];
        return $res;
    }

    /**
     *
     * @param array $exceptionalguestdata
     *            this overrides real guest data, useful for testing wallets
     */
    public function generateWalletpass($exceptionalguestdata = null)
    {
        $walletpath = BASEDIR . 'files/' . $_SESSION['customer']['customer_id'] . '/hidden';
        if ($exceptionalguestdata !== null) {
            $this->_guestdata = $exceptionalguestdata;
        }
        $this->_guestdata['invoiceno'] = $invoiceno;
        $this->_eventdata['shoppingcarthtml'] = $this->generateInvoiceShoppingcart1();
        
        require_once (BASEDIR . 'lib/pkpass/pkpass.php');
        
        // Set certifivate and path in the constructor
        $pass = new \PKPass\PKPass(BASEDIR . 'wallet/gaesteliste.pk.p12');
        // Add the WWDR certificate
        $pass->setWWDRcertPath(BASEDIR . 'wallet/AppleWWDRCA.pem');
        // Check if an error occured within the constructor
        if ($pass->checkError($error) == true) {
            exit('An error occured: ' . $error);
        }
        if ($this->_eventdata['walletorg'] == '') {
            $this->_eventdata['walletorg'] = 'fluxcess';
        }
        if ($this->_eventdata['walletbackgroundcolor'] == '') {
            $this->_eventdata['walletbackgroundcolor'] = 'rgb(77,77,77)';
        }
        if ($this->_eventdata['walletforegroundcolor'] == '') {
            $this->_eventdata['walletforegroundcolor'] = 'rgb(255,255,255)';
        }
        if ($this->_eventdata['walletlabelcolor'] == '') {
            $this->_eventdata['walletlabelcolor'] = 'rgb(200,200,200)';
        }
        if ($this->_eventdata['walletlogotext'] == '') {
            $this->_eventdata['walletlogotext'] = 'fluxcess Wallet Ticket';
        }
        if ($this->_eventdata['walletdescription'] == '') {
            $this->_eventdata['walletdescription'] = gettext('Description of the wallet pass');
        }
        if ($this->_eventdata['walletrelevantdate'] == '') {
            $this->_eventdata['walletrelevantdate'] = date('Y-m-d\T20:00\Z');
        }
        if ($this->_eventdata['walletcode'] == '') {
            $this->_eventdata['walletcode'] = '{
        "passTypeIdentifier": "pass.org.gaesteliste.www",
        "formatVersion": 1,
        "organizationName": "<ev.walletorg>",
        "serialNumber": "<guest_id>",
        "teamIdentifier": "F9T2BQJ56T",
        "backgroundColor": "<ev.walletbackgroundcolor>",
        "foregroundColor": "<ev.walletforegroundcolor>",
        "labelColor": "<ev.walletlabelcolor>",
        "logoText": "<ev.walletlogotext>",
        "description": "<ev.walletdescription>",
		';
            if ($this->_eventdata['walletlocations'] != '') {
                $this->_eventdata['walletcode'] .= '"locations": [
				<ev.walletlocations>
				],';
            }
            $this->_eventdata['walletcode'] .= '
		"relevantDate" : "<ev.walletrelevantdate>",
        "eventTicket": {
        "primaryFields": [
            {
                "key" : "name",
                "label" : "Teilnehmer",
                "value" : "<title> <firstname> <lastname>"
            }
        ],
        "secondaryFields": [
					
            {
                "key" : "company",
                "label" : "Firma",
                "value" : "<company>"
            }
        ],
                "auxiliaryFields": [
            {
                "key": "date",
                "label": "Datum",
                                "dateStyle": "PKDateStyleMedium",
                                "timeStyle": "PKDateStyleMedium",
                "value": "<ev.walletrelevantdate>"
            }
        ],
        "backFields": [';
            if ($this->_eventdata['walletwebsite'] != '') {
                $this->_eventdata['walletcode'] .= '{	"key": "website",
				"label": "Website",
				"value": "<walletwebsite>",
				}';
            }
            if ($this->_eventdata['walletwebsite'] != '' && $this->_eventdata['walletbacksidetxhl'] != '' && $this->_eventdata['walletbacksidetext'] != '') {
                $this->_eventdata['walletcode'] .= ', ';
            }
            if ($this->_eventdata['walletbacksidetxhl'] != '' && $this->_eventdata['walletbacksidetext'] != '') {
                $this->_eventdata['walletcode'] .= '{
                "key": "terms",
                "label": "<walletbacksidetxhl>",
                "value": "<walletbacksidetext>"
            	}';
            }
            $this->_eventdata['walletcode'] .= ']
},
    "barcode": {
        "format": "PKBarcodeFormatQR",
        "message": "<webcode>",
        "messageEncoding": "iso-8859-1"
    }
    }';
        }
        $walletJson = $this->createCustomizedText($this->_eventdata['walletcode'], false, '<', '>');
        $pass->setJSON($walletJson);
        
        if ($pass->checkError($error) == true) {
            exit('An error occured: ' . $error);
        }
        // add files to the PKPass package
        $files['icon.png'] = $this->_eventdata['filewalleticon1'];
        if (! is_file($walletpath . $files['icon.png'])) {
            $files['icon.png'] = BASEDIR . 'public/pic/wallet_default_icon.png';
        }
        
        $files['icon@2x.png'] = $this->_eventdata['filewalleticon2'];
        $files['icon@3x.png'] = $this->_eventdata['filewalleticon3'];
        
        $files['logo.png'] = $this->_eventdata['filewalletlogo1'];
        $files['logo@2x.png'] = $this->_eventdata['filewalletlogo2'];
        $files['logo@3x.png'] = $this->_eventdata['filewalletlogo3'];
        
        $files['strip.png'] = $this->_eventdata['filewalletstrip1'];
        $files['strip@2x.png'] = $this->_eventdata['filewalletstrip2'];
        $files['strip@3x.png'] = $this->_eventdata['filewalletstrip3'];
        
        $iconAdded = false;
        foreach ($files as $newname => $f) {
            if (is_file($walletpath . $f)) {
                $pass->addFile($walletpath . $f, $newname);
                if ($newname == 'icon.png') {
                    $iconAdded = true;
                }
            }
        }
        if ($iconAdded === false) {
            $pass->addFile($files['icon.png'], 'icon.png');
        }
        
        // specify english and french localizations
        $pass->addFile(BASEDIR . 'wallet/en.strings', 'en.lproj/pass.strings');
        $pass->addFile(BASEDIR . 'wallet/fr.strings', 'fr.lproj/pass.strings');
        if ($pass->checkError($error) == true) {
            exit('An error occured: ' . $error);
        }
        
        // If you pass true, the class will output the zip into the browser.
        $res = $pass->create(false);
        if ($res === false) {
            $this->addError('', 'wallet pass creation problem', 1, $pass->getError());
        }
        /*
         * $html = $this->createCustomizedText($this->_eventdata['invoicehtmlcode']);
         * $fname = date('Ymdhis') . '_' . $_SESSION['fevent_id'] . '_' . rand(100000000000000, 999999999999999) . '.html';
         * $handle = fopen(DIR_STORAGE_PRE_PDF . $fname, 'w');
         * fwrite($handle, $html);
         * fclose($handle);
         * $res['tempurl'] = HTTP_STORAGE_PRE_PDF . $fname;
         * $res['html2pdfurl'] = HTML2PDF_SERVER;
         * $res['pdfmargin'] = '0cm'; // $ev['ticketprintpdfmargin'];
         */
        return $res;
    }

    public function saveInvoice($res, $invoiceno, $pricing)
    {
        $path = BASEDIR . 'files/' . $_SESSION['customer']['customer_id'] . '/event/' . $_SESSION['fevent_id'] . '/invoices/';
        $filenameprefix = 'invoice';
        if (isset($this->_eventdata['documentfilenameprefix']) && strlen($this->_eventdata['documentfilenameprefix']) > 0) {
            $filenameprefix = $this->_eventdata['documentfilenameprefix'];
        }
        $invoiceFilename = $filenameprefix . '_' . $invoiceno . '_' . date('Ymd-His') . '_' . $this->_guestdata['guest_id'] . '.pdf';
        
        $savefilename = $path . $invoiceFilename;
        if (! is_dir($path)) {
            mkdir($path, 0700, true);
        }
        $res['allurl'] = HTML2PDF_SERVER . '?filename=invoice.pdf&url=' . urlencode($res['tempurl']) . '&size=A4&margin=0cm';
        $ch = curl_init($res['allurl']);
        // echo $res['allurl'];
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        
        $contents = curl_exec($ch);
        if (curl_error($ch)) {
            $this->addError('', 'Error creating PDF from HTML', 1, curl_error($ch));
        } elseif ($contents === FALSE) {
            $this->addError('', 'PDF file not downloaded from PDF generation server.', 1, $res['allurl']);
        }
        curl_close($ch);
        file_put_contents($savefilename, $contents);
        if (! is_file($savefilename) || filesize($savefilename) == 0) {
            $this->addError('', 'Invoice not saved / downloaded from PDF generation server.', 1, 'filename: ' . $savefilename . ' size: ' . filesize($savefilename));
        }
        // add a pdf as background
        if ($this->_eventdata['fileinvoicebackground'] != '') {
            $backgroundfilename = BASEDIR . 'files/' . $_SESSION['customer']['customer_id'] . '/hidden' . $this->_eventdata['fileinvoicebackground'];
            if (is_file($backgroundfilename)) {
                $output = array();
                $command = '/usr/bin/pdftk ' . $savefilename . ' background ' . $backgroundfilename . ' output ' . $savefilename . 'x';
                exec($command, $output);
                $command = 'mv ' . $savefilename . 'x ' . $savefilename;
                exec($command, $output);
            }
        }
        
        if (filesize($savefilename) < 100) {
            $this->addError('', gettext('Invoice could not be generated.'), 1);
        }
        if (count($this->_err) == 0) {
            $invoiceRowId = $this->logInvoiceCreation($invoiceFilename, $pricing);
            $this->saveInvoiceItems($invoiceRowId, $pricing);
            $this->saveInvoiceVATInformation($invoiceRowId, $pricing);
            $res['filename'] = $invoiceFilename;
            $res['fullfilename'] = $savefilename;
        } else {
            $res = false;
        }
        
        return $res;
    }

    private function saveInvoiceVATInformation($invoiceRowId, $pricing)
    {}

    private function saveInvoiceVATLine($line)
    {}

    private function saveInvoiceItems($invoiceRowId, $pricing)
    {
        if (count($pricing['items']) > 0) {
            foreach ($pricing['items'] as $item) {
                $this->saveInvoiceLine($invoiceRowId, $item);
            }
        } /*
           * for those customers who use invoices as tickets, it is normal to have no invoice items.
           * ToDo: create an option "invoices without items allowed
           *
           * else {
           * $this->addError('', gettext('Invoice cannot be created: No items are available to be invoiced.'));
           * }
           */
    }

    private function saveInvoiceLine($invoiceRowId, $line)
    {
        $invoiceItemRowId = false;
        try {
            $tablename = 'xvl' . $this->_eventid;
            $sql = "INSERT INTO `" . $tablename . "` SET guest_id = :guest_id,
					parent_id = :parent_id,
					ip = :ip, `inout` = :inout, room = :pricegross, suppdata = :suppdata,
					dec1 = :quantity, dec2 = :singlepricenet,
					dec3 = :singlevatamount, dec4 = :singlepricegross,
					dec5 = :vatrate,
					dec6 = :pricenet, dec7 = :vatamount, dec8 = :pricegross;";
            $pdoStatement = $this->_pdoObj->prepare($sql, array(
                PDO::ATTR_CURSOR => PDO::CURSOR_FWDONLY
            ));
            $sqlArr = array(
                ':guest_id' => $this->_guestdata['guest_id'],
                ':parent_id' => $invoiceRowId,
                ':ip' => '',
                ':inout' => 'ini',
                ':quantity' => number_format($line['quantity'], 2, '.', ''),
                ':singlepricenet' => number_format($line['singlepricenet'], 2, '.', ''),
                ':singlevatamount' => number_format($line['singlevatamount'], 2, '.', ''),
                ':singlepricegross' => number_format($line['singlepricegross'], 2, '.', ''),
                ':vatrate' => number_format($line['vatrate'], 2, '.', ''),
                ':pricenet' => number_format($line['pricenet'], 2, '.', ''),
                ':vatamount' => number_format($line['vatamount'], 2, '.', ''),
                ':pricegross' => number_format($line['pricegross'], 2, '.', ''),
                ':suppdata' => $line['description']
            );
            
            $pdoStatement->execute($sqlArr);
            if ($pdoStatement->errorCode() * 1 != 0) {
                $this->addError('', gettext('SQL Error'), 1, __FILE__ . '/' . __LINE__ . ' ' . print_r($pdoStatement->errorInfo(), true));
            } elseif ($pdoStatement->rowCount() == 0) {
                $this->addError('', gettext('SQL Error - log record has not been created.'), 1, __FILE__ . '/' . __LINE__ . ' Errorcode 0: ' . print_r($pdoStatement->errorInfo(), true));
            } else {
                $invoiceItemRowId = $this->_pdoObj->lastInsertId();
            }
        } catch (Exception $e) {
            $this->addError('', gettext('Problem writing an invoice item.'), 1, print_r($e, true));
        }
        return $invoiceItemRowId;
    }

    public function generateAndSendInvoice($invoiceno, $sendmail = true)
    {
        $pricing = null;
        $res = $this->generateInvoice($invoiceno, $pricing);
        $res2 = $this->saveInvoice($res, $invoiceno, $pricing);
        $savefilename = $res2['fullfilename'];
        if (! is_file($savefilename)) {
            $this->addError('', gettext('The invoice cannot be found and attached to the email.'));
        } elseif (filesize($savefilename) == 0) {
            $this->addError('', gettext('There was a problem reading the generated invoice. Please send it manually.'));
        }
        if (isset($this->_eventdata['autowallet']) && $this->_eventdata['autowallet'] == 1) {
            $walletpass = $this->generateWalletpass();
            if (strlen($walletpass) > 0) {
                $res3att = array(
                    'content' => $walletpass,
                    'filename' => 'pass.pkpass',
                    'encoding' => 'base64',
                    'type' => 'application/vnd.apple.pkpass'
                );
            } else {
                $this->addError('', 'Wallet Pass could not be generated.', 1);
            }
        } else {
            $res3att = null;
        }
        if (count($this->_err) == 0 && $sendmail === true) {
            $mt = new emailtemplate($this->_lg, $this->_locale);
            $templateid = $this->_eventdata['invoicemailtemplate'];
            if (is_array($this->_eventdata['conditionalautomail'])) {
                foreach ($this->_eventdata['conditionalautomail'] as $ecam) {
                    if ($ecam['emailtrigger'] == 'booking') {
                        $g = $this->_guestdata;
                        eval('$y = ' . $ecam['emailcondition'] . ';');
                        if ($y === true) {
                            $templateid = $ecam['emailtemplateid'];
                            break;
                        }
                    }
                }
            }
            $mt->setRecordId($templateid);
            $mtdata = $mt->getData();
            if (count($mtdata) < 2) {
                $mtdata = emailtemplate::returnEmptyTemplate();
            }
            $subject = $this->createCustomizedText($mtdata['emailsubject']);
            $mailcontenthtml = $this->createCustomizedText($mtdata['content']);
            $mimemesg = $this->sendEmailToRecipient($subject, $mailcontenthtml, gettext('This is an html email.'), $this->_guestdata['emailaddress'], $this->_eventdata, array(), array(
                $savefilename
            ), array(
                $res3att
            ));
            $this->logMessage($mimemesg);
        }
        if (count($this->_err) > 0) {
            // if created file has size 0, it can be removed.
            if (filesize($savefilename) == 0 && is_file($savefilename)) {
                unlink($savefilename);
            }
        }
        return $res;
    }

    public function generateInvoiceShoppingcart1(&$pricing = null)
    {
        $pricing = $this->generateInvoicePricing();
        $html = '<table cellpadding="0" cellspacing="0" width="100%" class="invtable top">
			<tr><td><strong>Bezeichnung</strong></td><td style="width: 2cm;" class="price"><strong>Betrag/EUR</strong></td><td style="width: 2cm;">&nbsp;</td></tr>
			</table>
			<table cellpadding="0" cellspacing="0" width="100%" class="invtable">
				';
        foreach ($pricing['items'] as $item) {
            $html .= '<tr><td style="width: 1cm;">' . $item['quantity'] . '</td><td>';
            $html .= $item['description'] . '</td>';
            $html .= '<td style="width: 2cm;" class="price">' . number_format($item['pricegross'], 2, ',', '.') . ' EUR</td>';
            $html .= '<td style="width: 2cm;">&nbsp;</td></tr>';
            // $html .= '<tr><td style="width: 1cm;">1</td><td>Nr/Seminar<br />1 | Test<br />2 | Test 2</td><td style="width: 2cm;" class="price">35,00 EUR</td><td style="width: 2cm;">&nbsp;</td></tr>';
        }
        $html .= '
			</table>
			<table cellpadding="0" cellspacing="0" width="100%" class="invtable">
			<tr class="sum">
			<td><strong>Rechnungsbetrag</strong></td><td style="width: 2cm;" class="price">' . number_format($pricing['sumgross'], 2, ',', '.') . ' EUR</td><td style="width: 2cm;">&nbsp;</td>
			</tr>
			</table>
		';
        return $html;
    }

    public function generateSessionOverview()
    {
        $ev = new event($this->_lg, $this->_locale);
        $ev->setRecordId($this->_eventid);
        $this->initWeekdays();
        $xxx = $ev->ff($this->_eventid, $this->_eventdata);
        $html = '';
        if (is_array($xxx['pages'])) {
            foreach ($xxx['pages'] as $p) {
                foreach ($p['sections'] as $s) {
                    switch ($s['type']) {
                        case 'sessionchoice':
                            $allsessions = array();
                            foreach ($s['sessions'] as $mses) {
                                $allsessions[$mses['id']] = $mses;
                            }
                            $choices = explode(',', $this->_guestdata[$s['name']]);
                            foreach ($choices as $choice) {
                                if (strlen($choice) > 0) {
                                    $sesdata = $allsessions[$choice];
                                    $begin = $this->datefltophp($sesdata['datetime']);
                                    $end = $this->datefltophp($sesdata['datetimeto']);
                                    $childtext[$s['invoiceparentfield']] .= '<div class="sessiontime">' . $this->_weekdaysLong[date('w', $begin)] . ', ' . date('H:i', $begin) . ' - ' . date('H:i', $end) . ' Uhr</div><div class="sessiondata">' . $sesdata['no'] . ' | ' . $sesdata['title'] . '</div>';
                                    // $invoicesum += $tcdata['price'];
                                }
                            }
                            break;
                    }
                }
            }
        }
        foreach ($childtext as $ctid => $ct) {
            $html .= $ct;
        }
        return $html;
    }

    public function generateSessionOverview2()
    {
        $ev = new event($this->_lg, $this->_locale);
        $ev->setRecordId($this->_eventid);
        $this->initWeekdays();
        $xxx = $ev->ff($this->_eventid, $this->_eventdata);
        $html = '<table class="sessionlist2">';
        if (is_array($xxx['pages'])) {
            foreach ($xxx['pages'] as $p) {
                foreach ($p['sections'] as $s) {
                    switch ($s['type']) {
                        case 'sessionchoice':
                            $allsessions = array();
                            foreach ($s['sessions'] as $mses) {
                                $allsessions[$mses['id']] = $mses;
                            }
                            $choices = explode(',', $this->_guestdata[$s['name']]);
                            foreach ($choices as $choice) {
                                if (strlen($choice) > 0) {
                                    $sesdata = $allsessions[$choice];
                                    $begin = $this->datefltophp($sesdata['datetime']);
                                    $end = $this->datefltophp($sesdata['datetimeto']);
                                    $childtext[$s['invoiceparentfield']] .= '<tr><td class="sessiontime">' . $this->_weekdaysLong[date('w', $begin)] . ', ' . date('H:i', $begin) . ' - ' . date('H:i', $end) . ' Uhr</td><td class="sessiondata">' . $sesdata['no'] . ' | ' . $sesdata['title'] . '</td></tr>';
                                    // $invoicesum += $tcdata['price'];
                                }
                            }
                            break;
                    }
                }
            }
        }
        foreach ($childtext as $ctid => $ct) {
            $html .= $ct;
        }
        return $html . '</table>';
    }

    /**
     * returns the "pricing" array.
     *
     * format:
     * $pricing
     * * items
     * # [ array of items ]
     * # * quantity
     * # * description
     * # * singlepricenet
     * # * singlevatamount
     * # * singlepricegross
     * # * vatrate
     * # * vatamount
     * # * pricenet
     * # * pricegross
     * * sumnet
     * * sumvat
     * * sumgross
     *
     * @return string[]|number[][][]|string[][][]|unknown[][][]|number[]|unknown[]
     */
    public function generateInvoicePricing()
    {
        $pricing = array();
        $tcs = array();
        $shoppingcartitems = array();
        $quantity = 1;
        $invoicesumnet = 0;
        $invoicesumgross = 0;
        $invoicesumvat = 0;
        $childtext = array();
        $namenomap = array();
        $ev = new event($this->_lg, $this->_locale);
        $xxx = $ev->ff($this->_eventid, $this->_eventdata);
        foreach ($xxx['ticketcategory'] as $tc) {
            $tcs[$tc['ticketcategorycode']] = $tc;
        }
        if (is_array($xxx['pages'])) {
            foreach ($xxx['pages'] as $p) {
                foreach ($p['sections'] as $s) {
                    switch ($s['type']) {
                        case 'sessionchoice':
                            $allsessions = array();
                            foreach ($s['sessions'] as $mses) {
                                $allsessions[$mses['id']] = $mses;
                            }
                            $choices = explode(',', $this->_guestdata[$s['name']]);
                            foreach ($choices as $choice) {
                                if (strlen($choice) > 0) {
                                    $sesdata = $allsessions[$choice];
                                    $childtext[$s['invoiceparentfield']] .= $sesdata['no'] . ' | ' . $sesdata['title'] . '<br />';
                                    // $invoicesum += $tcdata['price'];
                                }
                            }
                            break;
                        case 'tcatchoice':
                            $choices = explode(',', $this->_guestdata[$s['name']]);
                            foreach ($choices as $choice) {
                                if (strlen($choice) > 0 && isset($tcs[$choice]['price'])) {
                                    $tcdata = $tcs[$choice];
                                    if (! isset($tcdata['invoicetext']) || $tcdata['invoicetext'] == '') {
                                        $tcdata['invoicetext'] = $tcdata['title'];
                                    }
                                    $itemprice = $this->calculateItemPrice($tcdata, $quantity);
                                    // ToDo: Error Handling: If the price is Zero,
                                    // there might be a problem.
                                    $shoppingcartitems[] = array(
                                        'quantity' => 1,
                                        'description' => $tcdata['invoicetext'] . '<br />',
                                        'singlepricenet' => $itemprice['singlepricenet'],
                                        'singlepricegross' => $itemprice['singlepricegross'],
                                        'singlevatamount' => $itemprice['singlevatamount'],
                                        'vatrate' => $itemprice['vatrate'],
                                        'vatamount' => $itemprice['vatamount'],
                                        'pricenet' => $itemprice['pricenet'],
                                        'pricegross' => $itemprice['pricegross']
                                    );
                                    $namenomap[$s['name']] = count($shoppingcartitems) - 1;
                                    $invoicesumnet += $itemprice['pricenet'];
                                    $invoicesumgross += $itemprice['pricegross'];
                                    $invoicesumvat += $itemprice['vatrate'];
                                    if (! isset($invoiceVAT[$itemprice['vatrate']])) {
                                        $invoiceVAT[$itemprice['vatrate']] = $itemprice['vatamount'];
                                    } else {
                                        $invoiceVAT[$itemprice['vatrate']] += $itemprice['vatamount'];
                                    }
                                }
                            }
                            break;
                    }
                }
            }
        }
        foreach ($childtext as $parid => $cht) {
            // echo "parid: $parid, namenomapparid: $namenomap[$parid].\n";;
            $shoppingcartitems[$namenomap[$parid]]['description'] .= $cht . '<br />';
        }
        $pricing['items'] = $shoppingcartitems;
        $pricing['sumgross'] = $invoicesumgross;
        $pricing['sumnet'] = $invoicesumnet;
        $pricing['sumvat'] = $invoicesumvat;
        return $pricing;
    }

    /**
     * returns the item price row for an invoice - or false, if data is not correct.
     *
     * @param unknown $tcdata
     * @param number $quantity
     */
    private function calculateItemPrice($tcdata, $quantity = 1)
    {
        $itemprice = array();
        if (is_numeric($tcdata['price']) && is_numeric($tcdata['pricenet']) && is_numeric($tcdata['vatrate']) && is_numeric($tcdata['vatamount'])) {
            // given: net price, gross price, vat rate, vat amount
            $itemprice['singlepricenet'] = $tcdata['pricenet'];
            $itemprice['singlepricegross'] = $tcdata['price'];
            $itemprice['singlevatamount'] = $tcdata['vatamount'];
            $itemprice['pricenet'] = $quantity * $itemprice['singlepricenet'];
            $itemprice['pricegross'] = $quantity * $itemprice['singlepricegross'];
            $itemprice['vatrate'] = $tcdata['vatrate'];
            $itemprice['vatamount'] = $quantity * $itemprice['singlevatamount'];
        } elseif (is_numeric($tcdata['price']) && is_numeric($tcdata['pricenet']) && is_numeric($tcdata['vatrate'])) {
            // given: net price, gross price, vat rate
            $itemprice['singlepricenet'] = $tcdata['pricenet'];
            $itemprice['singlepricegross'] = $tcdata['price'];
            $itemprice['singlevatamount'] = $itemprice['singlepricegross'] - $itemprice['singlepricenet'];
            $itemprice['pricenet'] = $quantity * $itemprice['pricenet'];
            $itemprice['pricegross'] = $quantity * $itemprice['pricegross'];
            $itemprice['vatrate'] = $tcdata['vatrate'];
            $itemprice['vatamount'] = $quantity * $itemprice['singlevatamount'];
        } elseif (is_numeric($tcdata['price']) && is_numeric($tcdata['pricenet']) && is_numeric($tcdata['vatamount'])) {
            // given: net price, gross price, vat amount
            $this->addError('', gettext('There is an invalid price combination for one item: The value for VAT rate is missing, while the VAT amount is given.'));
            $itemprice = false;
        } elseif (is_numeric($tcdata['vatamount']) && is_numeric($tcdata['pricenet']) && is_numeric($tcdata['vatrate'])) {
            // given: net price, vat amount, vat rate
            $itemprice['singlepricenet'] = $tcdata['pricenet'];
            $itemprice['singlepricegross'] = $tcdata['pricenet'] + $tcdata['vatamount'];
            $itemprice['singlevatamount'] = $tcdata['vatamount'];
            $itemprice['pricenet'] = $quantity * $itemprice['pricenet'];
            $itemprice['pricegross'] = $quantity * $itemprice['pricegross'];
            $itemprice['vatrate'] = $tcdata['vatrate'];
            $itemprice['vatamount'] = $quantity * $itemprice['singlevatamount'];
        } elseif (is_numeric($tcdata['price']) && is_numeric($tcdata['vatamount']) && is_numeric($tcdata['vatrate'])) {
            // given: gross price, vat amount, vat rate
            $itemprice['singlepricenet'] = $tcdata['pricen'] - $tcdata['vatamount'];
            $itemprice['singlepricegross'] = $tcdata['price'];
            $itemprice['singlevatamount'] = $tcdata['vatamount'];
            $itemprice['pricenet'] = $quantity * $itemprice['pricenet'];
            $itemprice['pricegross'] = $quantity * $itemprice['pricegross'];
            $itemprice['vatrate'] = $tcdata['vatrate'];
            $itemprice['vatamount'] = $quantity * $itemprice['singlevatamount'];
        } elseif (is_numeric($tcdata['price']) && is_numeric($tcdata['vatrate'])) {
            // given: gross price, vat rate
            $itemprice['singlepricenet'] = $this->vatNetFromGross($tcdata['price'], $tcdata['vatrate']);
            $itemprice['singlepricegross'] = $tcdata['price'];
            $itemprice['singlevatamount'] = $itemprice['singlepricegross'] - $itemprice['singlepricenet'];
            $itemprice['pricenet'] = $quantity * $itemprice['singlepricenet'];
            $itemprice['pricegross'] = $quantity * $itemprice['singlepricegross'];
            $itemprice['vatrate'] = $tcdata['vatrate'];
            $itemprice['vatamount'] = $quantity * $itemprice['singlevatamount'];
        } elseif (is_numeric($tcdata['price']) && is_numeric($tcdata['vatamount'])) {
            // given: gross price, vat amount
            $this->addError('', gettext('There is an invalid price combination for one item: The value for VAT rate is missing, while the gross price and VAT amount are given.'));
            $itemprice = false;
        } elseif (is_numeric($tcdata['price']) && is_numeric($tcdata['pricenet'])) {
            // given: gross price, net price
            $this->addError('', gettext('There is an invalid price combination for one item: The value for VAT rate is missing, while the net price and the gross price are given.'));
            $itemprice = false;
        } elseif (is_numeric($tcdata['pricenet']) && is_numeric($tcdata['vatrate'])) {
            // given: net price, vat rate
            $itemprice['singlepricenet'] = $tcdata['price'];
            $itemprice['singlepricegross'] = $this->vatGrossFromNet($tcdata['price'], $tcdata['vatrate']);
            $itemprice['singlevatamount'] = $itemprice['singlepricegross'] - $itemprice['singlepricenet'];
            $itemprice['pricenet'] = $quantity * $itemprice['singlepricenet'];
            $itemprice['pricegross'] = $quantity * $itemprice['singlepricegross'];
            $itemprice['vatrate'] = $tcdata['vatrate'];
            $itemprice['vatamount'] = $quantity * $itemprice['singlevatamount'];
        } elseif (is_numeric($tcdata['pricenet']) && is_numeric($tcdata['vatamount'])) {
            // given: net price, vat amount
            $this->addError('', gettext('There is an invalid price combination for one item: The value for VAT rate is missing, while the net price and the VAT amount are provided.'));
            $itemprice = false;
        } elseif (is_numeric($tcdata['vatamount']) && is_numeric($tcdata['vatrate'])) {
            // given: vat amount, vat rate
            $this->addError('', gettext('There is an invalid price combination for one item: Neither a net price, nor a gross price are given.'));
            $itemprice = false;
        } elseif (is_numeric($tcdata['price'])) {
            // given: gross price
            $itemprice['singlepricenet'] = $tcdata['price'];
            $itemprice['singlepricegross'] = $tcdata['price'];
            $itemprice['singlevatamount'] = 0;
            $itemprice['pricenet'] = $quantity * $tcdata['price'];
            $itemprice['pricegross'] = $quantity * $tcdata['price'];
            $itemprice['vatrate'] = 0;
            $itemprice['vatamount'] = 0;
        } elseif (is_numeric($tcdata['pricenet'])) {
            $this->addError('', gettext('There is an invalid price combination for one item: Only the net price is given.'));
            $itemprice = false;
        } elseif (is_numeric($tcdata['vatamount'])) {
            $this->addError('', gettext('There is an invalid price combination for one item: Only the VAT amount is given.'));
            $itemprice = false;
        } elseif (is_numeric($tcdata['vatrate'])) {
            $this->addError('', gettext('There is an invalid price combination for one item: Only the VAT rate is given.'));
            $itemprice = false;
        }
        return $itemprice;
    }

    private function vatNetFromGross($gross, $rate)
    {
        $netprice = false;
        $netprice = $gross / (1 + ($rate / 100));
        return $netprice;
    }

    private function vatGrossFromNet($net, $rate)
    {
        $grossprice = false;
        $grossprice = $net * (1 + ($rate / 100));
        return $grossprice;
    }

    public function logGuestAction($ip, $actiontype, $info1, $suppdata)
    {
        try {
            $tablename = 'xvl' . $this->_eventid;
            $sql = "INSERT INTO `" . $tablename . "` SET guest_id = :guest_id,
					ip = :ip, `inout` = :actiontype, room = :info1, suppdata = :suppdata;";
            $pdoStatement = $this->_pdoObj->prepare($sql, array(
                PDO::ATTR_CURSOR => PDO::CURSOR_FWDONLY
            ));
            $sqlArr = array(
                ':guest_id' => $this->_guestdata['guest_id'],
                ':ip' => $ip,
                ':actiontype' => $actiontype,
                ':info1' => $info1,
                ':suppdata' => $suppdata
            );
            $pdoStatement->execute($sqlArr);
            if ($pdoStatement->errorCode() * 1 != 0) {
                $this->addError('', gettext('SQL Error'), 1, __FILE__ . '/' . __LINE__ . ' ' . print_r($pdoStatement->errorInfo(), true));
            } elseif ($pdoStatement->rowCount() == 0) {
                $this->addError('', gettext('SQL Error - guest action log record has not been created.'), 1, __FILE__ . '/' . __LINE__ . ' Errorcode 0: ' . print_r($pdoStatement->errorInfo(), true));
            }
        } catch (Exception $e) {
            $this->addError('', gettext('Problem logging the guest action.'), 1, print_r($e, true));
        }
    }

    /**
     * logs the invoice head creation to the xvl table
     * invokes invoice item logging and invoice vat rate logging
     *
     * @param unknown $invoiceFilename
     * @param unknown $pricing
     */
    protected function logInvoiceCreation($invoiceFilename, $pricing)
    {
        $invoiceRowId = false;
        try {
            $tablename = 'xvl' . $this->_eventid;
            $sql = "INSERT INTO `" . $tablename . "` SET guest_id = :guest_id,
					ip = :ip, `inout` = :inout, room = :sumgross, suppdata = :suppdata,
					dec6 = :sumnet, dec7 = :sumvat, dec8 = :sumgross;";
            $pdoStatement = $this->_pdoObj->prepare($sql, array(
                PDO::ATTR_CURSOR => PDO::CURSOR_FWDONLY
            ));
            $sqlArr = array(
                ':guest_id' => $this->_guestdata['guest_id'],
                ':ip' => '',
                ':inout' => 'inv',
                ':sumgross' => number_format($pricing['sumgross'], 2, '.', ''),
                ':sumnet' => number_format($pricing['sumnet'], 2, '.', ''),
                ':sumvat' => number_format($pricing['sumvat'], 2, '.', ''),
                ':suppdata' => $invoiceFilename
            );
            $pdoStatement->execute($sqlArr);
            if ($pdoStatement->errorCode() * 1 != 0) {
                $this->addError('', gettext('SQL Error'), 1, __FILE__ . '/' . __LINE__ . ' ' . print_r($pdoStatement->errorInfo(), true));
            } elseif ($pdoStatement->rowCount() == 0) {
                $this->addError('', gettext('SQL Error - log record has not been created.'), 1, __FILE__ . '/' . __LINE__ . ' Errorcode 0: ' . print_r($pdoStatement->errorInfo(), true));
            } else {
                $invoiceRowId = $this->_pdoObj->lastInsertId();
            }
        } catch (Exception $e) {
            $this->addError('', gettext('Problem logging the invoice creation.'), 1, print_r($e, true));
        }
        return $invoiceRowId;
    }

    protected function logMessage($mimeMessage)
    {
        try {
            $tablename = 'xvl' . $this->_eventid;
            $sql = "INSERT INTO `" . $tablename . "` SET guest_id = :guest_id,
					ip = :ip, `inout` = :inout, room = 0, suppdata = :suppdata;";
            $pdoStatement = $this->_pdoObj->prepare($sql, array(
                PDO::ATTR_CURSOR => PDO::CURSOR_FWDONLY
            ));
            $sqlArr = array(
                ':guest_id' => $this->_guestdata['guest_id'],
                ':ip' => '',
                ':inout' => 'msg',
                ':suppdata' => $mimeMessage
            );
            $pdoStatement->execute($sqlArr);
            if ($pdoStatement->errorCode() * 1 != 0) {
                $this->addError('', gettext('SQL Error'), 1, __FILE__ . '/' . __LINE__ . ' ' . print_r($pdoStatement->errorInfo(), true));
            } elseif ($pdoStatement->rowCount() == 0) {
                $this->addError('', gettext('SQL Error - log record has not been created.'), 1, __FILE__ . '/' . __LINE__ . ' Errorcode 0: ' . print_r($pdoStatement->errorInfo(), true));
            }
        } catch (Exception $e) {
            $this->addError('', gettext('Problem logging the message.'), 1, print_r($e, true));
        }
    }

    public function addPayment()
    {
        try {
            $tablename = 'xvl' . $this->_eventid;
            $sql = "INSERT INTO `" . $tablename . "` SET guest_id = :guest_id,
					parent_id = :parent_id,
					ip = :ip, `inout` = :inout, room = :room, suppdata = :suppdata;";
            $pdoStatement = $this->_pdoObj->prepare($sql, array(
                PDO::ATTR_CURSOR => PDO::CURSOR_FWDONLY
            ));
            $sqlArr = array(
                ':guest_id' => $this->_guestdata['guest_id'],
                ':parent_id' => $_POST['parent_id'],
                ':ip' => '',
                ':inout' => 'pay',
                ':room' => $_POST['room'],
                ':suppdata' => $_POST['suppdata']
            );
            $pdoStatement->execute($sqlArr);
            if ($pdoStatement->errorCode() != 0) {
                $this->addError('', gettext('SQL Error'), 1, __FILE__ . '/' . __LINE__ . ' ' . print_r($pdoStatement->errorInfo(), true));
            } elseif ($pdoStatement->rowCount() == 0) {
                $this->addError('', gettext('SQL Error - payment log record has not been created.'), 1, __FILE__ . '/' . __LINE__ . ' Errorcode 0: ' . print_r($pdoStatement->errorInfo(), true));
            }
        } catch (Exception $e) {
            $this->addError('', gettext('Problem logging the payment.'), 1, print_r($e, true));
        }
        
        return $data;
    }

    public function sendWaitingListMail()
    {
        $templateid = null;
        if (is_array($this->_eventdata['conditionalautomail'])) {
            foreach ($this->_eventdata['conditionalautomail'] as $ecam) {
                if ($ecam['emailtrigger'] == 'waitinglist') {
                    $g = $this->_guestdata;
                    eval('$y = ' . $ecam['emailcondition'] . ';');
                    if ($y === true) {
                        $templateid = $ecam['emailtemplateid'];
                        // echo "guest ".$this->_guestid." is on waiting list and receives template #".$templateid;
                        break;
                    }
                }
            }
        }
        if ($templateid === null) {
            // echo "guest ".$this->_guestid." is on waiting list and does not receive an invoice, because no waiting list mail template exists.\n";
        } else {
            $mt = new emailtemplate($this->_lg, $this->_locale);
            $mt->setRecordId($templateid);
            $mtdata = $mt->getData();
            $subject = $this->createCustomizedText($mtdata['emailsubject']);
            $mailcontenthtml = $this->createCustomizedText($mtdata['content']);
            $this->sendEmailToRecipient($subject, $mailcontenthtml, gettext('This is an html email.'), $this->_guestdata['emailaddress'], $this->_eventdata, array(), array(
                $savefilename
            ));
            $this->logWaitinglistInformed('email ' . $this->_guestdata['emailaddress']);
        }
    }

    private function logWaitinglistInformed($suppdata)
    {
        try {
            $sql = "INSERT INTO xvl" . $_SESSION['fevent_id'] . " SET ip = :ip,
					guest_id = :guest_id, `inout` = 'wai', room = 0, suppdata = :suppdata;";
            $pdoStatement = $this->_pdoObj->prepare($sql, array(
                PDO::ATTR_CURSOR => PDO::CURSOR_FWDONLY
            ));
            $sqlArr = array(
                ':guest_id' => $this->_guestdata['guest_id'],
                ':ip' => '',
                ':suppdata' => $suppdata
            );
            $pdoStatement->execute($sqlArr);
            if ($pdoStatement->errorCode() * 1 != 0) {
                $this->addError('', gettext('SQL problem inserting a waitinglist information log note into the database.', 1, print_r($pdoStatement->errorInfo(), true) . $sql));
            }
        } catch (Exception $e) {
            $this->addError('', gettext('Problem logging waiting list informed status.'), 1, print_r($e, true) . $sql);
        }
    }

    public function getGuestLog()
    {
        $logentries = array();
        try {
            $tablename = 'xvl' . $this->_eventid;
            $sql = "SELECT id log_id, parent_id, ts, guest_id, ip info1, `inout` actiontype, room info2, suppdata longdata FROM `" . $tablename . "` 
					WHERE guest_id = :guest_id
					AND `inout` != 'msg'
					UNION
					SELECT id log_id, parent_id, ts, guest_id, ip info1, `inout` actiontype, room info2, CONCAT(LEFT(suppdata, LOCATE('\n\n', suppdata)), '...') longdata FROM `" . $tablename . "` 
					WHERE guest_id = :guest_id
					AND `inout` = 'msg'
					;";
            $pdoStatement = $this->_pdoObj->prepare($sql, array(
                \PDO::ATTR_CURSOR => \PDO::CURSOR_FWDONLY
            ));
            $sqlArr = array(
                ':guest_id' => $this->_guestid
            );
            $pdoStatement->execute($sqlArr);
            if ($pdoStatement->errorCode() != 0) {
                $this->addError('', gettext('SQL Error'), 1, __FILE__ . '/' . __LINE__ . ' ' . print_r($pdoStatement->errorInfo(), true));
            } elseif ($pdoStatement->rowCount() > 0) {
                while ($row = $pdoStatement->fetch(\PDO::FETCH_ASSOC)) {
                    $logentries[] = $row;
                }
            }
        } catch (Exception $e) {
            $this->addError('', gettext('Error reading log entries for guest from the database.'), 1, print_r($e, true));
        }
        return $logentries;
    }

    public function resendInvoice($invoicefilename)
    {
        try {
            $path = BASEDIR . 'files/' . $_SESSION['customer']['customer_id'] . '/event/' . $_SESSION['fevent_id'] . '/invoices/';
            $invoicefilename = $path . $invoicefilename;
            if (! is_file($invoicefilename)) {
                $this->addError('', gettext('The invoice cannot be found and attached to the email.'));
            } elseif (filesize($invoicefilename) == 0) {
                $this->addError('', gettext('There was a problem reading the generated invoice. Please send it manually.'));
            }
            if (isset($this->_eventdata['autowallet']) && $this->_eventdata['autowallet'] == 1) {
                $walletpass = $this->generateWalletpass();
                if (strlen($walletpass) > 0) {
                    $res3att = array(
                        'content' => $walletpass,
                        'filename' => 'pass.pkpass',
                        'encoding' => 'base64',
                        'type' => 'application/vnd.apple.pkpass'
                    );
                } else {
                    $this->addError('', 'Wallet Pass could not be generated.', 1);
                }
            } else {
                $res3att = null;
            }
            if (count($this->_err) == 0) {
                $mt = new emailtemplate($this->_lg, $this->_locale);
                $templateid = $this->_eventdata['invoicemailtemplate'];
                if (is_array($this->_eventdata['conditionalautomail'])) {
                    foreach ($this->_eventdata['conditionalautomail'] as $ecam) {
                        if ($ecam['emailtrigger'] == 'booking') {
                            $g = $this->_guestdata;
                            eval('$y = ' . $ecam['emailcondition'] . ';');
                            if ($y === true) {
                                $templateid = $ecam['emailtemplateid'];
                                break;
                            }
                        }
                    }
                }
                $mt->setRecordId($templateid);
                $mtdata = $mt->getData();
                if (count($mtdata) < 2) {
                    $mtdata = emailtemplate::returnEmptyTemplate();
                }
                $subject = $this->createCustomizedText($mtdata['emailsubject']);
                $mailcontenthtml = $this->createCustomizedText($mtdata['content']);
                $mimemesg = $this->sendEmailToRecipient($subject, $mailcontenthtml, gettext('This is an html email.'), $this->_guestdata['emailaddress'], $this->_eventdata, array(), array(
                    $invoicefilename
                ), array(
                    $res3att
                ));
                $this->logMessage($mimemesg);
            }
        } catch (Exception $e) {
            $this->addError('', 'Error resending invoice.', 1, print_r($e, true));
        }
    }

    public function remoteprintInvoice($invoicefilename)
    {
        try {
            $invoicefname = '/' . $_SESSION['customer']['customer_id'] . '/event/' . $_SESSION['fevent_id'] . '/invoices/' . $invoicefilename;
            $path = BASEDIR . 'files' . $invoicefname;
            $invoicefilename = $path;
            if (! is_file($invoicefilename)) {
                $this->addError('', gettext('The invoice cannot be found and not printed.') . $path);
            } elseif (filesize($invoicefilename) == 0) {
                $this->addError('', gettext('There was a problem reading the generated invoice. Please send it manually.'));
            } else {
                $this->logGuestAction(getenv('REMOTE_ADDR'), 'rprtwait', '0', $invoicefname);
            }
        } catch (Exception $e) {
            $this->addError('', 'Error resending invoice.', 1, print_r($e, true));
        }
    }

    private function getGuestCheckinInformation()
    {
        $checkInInformation = false;
        if (is_array($this->_eventdata['room'])) {
            foreach ($this->_eventdata['room'] as $r) {
                $checkInInformation[$r['id']] = array(
                    'room_id' => $r['id'],
                    'room_name' => $r['title'],
                    'pax' => 0
                );
            }
        }
        if ($this->_guestid !== null) {
            $sql = "SELECT * FROM ygr" . $this->_eventid . " 
					WHERE guest_id = :guest_id
					;";
            $sqlArr = array(
                ':guest_id' => $this->_guestid
            );
            $pdoStatement = $this->_pdoObj->prepare($sql, array(
                \PDO::ATTR_CURSOR => \PDO::CURSOR_FWDONLY
            ));
            $pdoStatement->execute($sqlArr);
            if ($pdoStatement->errorCode() != 0) {
                $this->addError('', gettext('SQL Error'), 1, __FILE__ . '/' . __LINE__ . ' ' . print_r($pdoStatement->errorInfo(), true));
            } elseif ($pdoStatement->rowCount() > 0) {
                while ($row = $pdoStatement->fetch(\PDO::FETCH_ASSOC)) {
                    $row['room_name'] = $checkInInformation[$row['room_id']]['room_name'];
                    $checkInInformation[$row['room_id']] = $row;
                }
            }
        } else {
            $this->addError('guest_id', gettext('No guest selected.'), 1);
        }
        return $checkInInformation;
    }

    private function addGuestToRoom($room_id)
    {
        $sql = "INSERT INTO ygr" . $this->_eventid . " (pax, room_id, guest_id) VALUES (1, :roomid, :guestid)
							ON DUPLICATE KEY UPDATE pax = pax + 1
							;";
        // $_pdoObj = dbconnection::getInstance();
        $pdoStatement = $this->_pdoObj->prepare($sql, array(
            PDO::ATTR_CURSOR => PDO::CURSOR_FWDONLY
        ));
        $pdoStatement->execute(array(
            ':roomid' => $room_id,
            ':guestid' => $this->_guestid
        ));
        if ($pdoStatement->errorCode() > 0) {
            $this->addError('', gettext('Error adding guest to room.'), 1, print_r($pdoStatement->errorInfo()));
        }
    }

    private function removeGuestFromRoom($room_id)
    {
        $sql = "UPDATE ygr" . $this->_eventid . " SET pax = pax - 1
				WHERE room_id = :roomid AND guest_id = :guestid
				AND pax > 0
							;";
        // $_pdoObj = dbconnection::getInstance();
        $pdoStatement = $this->_pdoObj->prepare($sql, array(
            PDO::ATTR_CURSOR => PDO::CURSOR_FWDONLY
        ));
        $pdoStatement->execute(array(
            ':roomid' => $room_id,
            ':guestid' => $this->_guestid
        ));
        if ($pdoStatement->errorCode() > 0) {
            $this->addError('', gettext('Error adding guest to room.'), 1, print_r($pdoStatement->errorInfo()));
        }
    }

    public function guestCheckinCheckout($id, $room, $checkinout = 'checkin')
    {
        if (is_numeric($id) && strlen($room) > 0 && strlen($room) < 32 && ($checkinout == 'checkin' || $checkinout == 'checkout')) {
            $ip = getenv("REMOTE_ADDR");
            
            // how often is the guest checked in in this room now?
            $checkInInformation = $this->getGuestCheckinInformation();
            
            if ($checkinout == 'checkin') {
                $this->addGuestToRoom($room);
                $actiontype = 'in';
            } else {
                $this->removeGuestFromRoom($room);
                $actiontype = 'out';
            }
            
            $this->logGuestAction($ip, $actiontype, $room, '');
        } else {
            $err[] = 'Fehler: Besucher-ID ' . $id . ' oder Raum-ID ' . $room . ' nicht vorhanden';
        }
        // }
        return $err;
    }

    public function serverstoreticket()
    {
        
        // send to pdf converter
        // store file on server
        // log ticket creation
    }

    public function getPaymentStatusList()
    {
        return $this->_paymentstatus;
    }

    public function cancelInvoice($invoiceId, $comment, $uncancel = false)
    {
        try {
            // ToDo: check if invoice is cancelled already
            // if status is new status already, return an error message
            
            // ToDo: check if invoice has been paid.
            $paid = false;
            // check if invoice is NOT marked as paid
            if ($paid) {
                $this->addError('', gettext('Invoices which are marked as "paid" cannot be cancelled.'));
            }
            // check if comment field is filled
            $comment = trim($comment);
            if (strlen($comment) < 3) {
                $this->addError('comment', gettext('For cancelling an invoice, it is necessary to provide a comment.'));
            }
            
            // begin transaction
            // log invoice cancellation
            // $this->logGuestActionWithParent('', 'inc', '', $comment);
            // mark invoice as cancelled
            $this->markInvoiceAsCancelled($invoiceId, $uncancel);
            // end transaction
        } catch (Exception $e) {
            $this->addError('', gettext('Error cancelling invoice.'), 1, print_r($e->getMessage()));
        }
    }

    private function markInvoiceAsCancelled($invRowId, $uncancel = false)
    {
        $sql = "UPDATE `xvl" . $this->_eventid . "` 
				SET flg01 = :newstatus
				WHERE
				id = :invrowid
				;";
        // $_pdoObj = dbconnection::getInstance();
        $pdoStatement = $this->_pdoObj->prepare($sql, array(
            PDO::ATTR_CURSOR => PDO::CURSOR_FWDONLY
        ));
        $newStatus = 1;
        if ($uncancel === true) {
            $newStatus = 0;
        }
        $pdoStatement->execute(array(
            ':invrowid' => $invRowId,
            ':newstatus' => $newStatus
        ));
        if ($pdoStatement->errorCode() > 0) {
            $this->addError('', gettext('Error setting invoice cancelled flag.'), 1, print_r($pdoStatement->errorInfo()));
        }
    }
}