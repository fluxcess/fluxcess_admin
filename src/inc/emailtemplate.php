<?php

/**
 * Contains the entityEmailtemplate class
 */

/**
 * Emailtemplate object.
 *
 * @author Line5
 *        
 */
class emailtemplate extends l5sys
{

    /**
     * Data of this emailtemplate
     *
     * @var array
     */
    private $_emailtemplatedata = array();

    /**
     * Id of the currently loaded emailtemplate
     *
     * @var int
     */
    private $_emailtemplateid = 0;

    /**
     * This is the constructor
     *
     * @param string $lg
     *            contains the language
     * @param string $locale
     *            contains the locale
     */
    function __construct($lg, $locale)
    {
        if (TESTING === true) {
            $this->_rsvpDomain = 'rsvptest.dc.gs';
        }
        $this->_locale = $locale;
        $this->_lg = $lg;
    }

    /**
     * returns the email record for the currently active email id
     *
     * @return array:
     */
    public function getData()
    {
        return $this->_emailtemplatedata;
    }

    /**
     * replaces variable names in [<varname>] constructs.
     *
     * @param string $html
     * @param string $foremail
     *            determines if the returned html code is for emails
     * @return string
     */
    public function createCustomizedText($html, $foremail = true)
    {
        if ($foremail == false) {
            if (LIVEENV === true) {
                $codeurl = 'https://check-in.fast-lane.org/qr/' . $this->_eventId . '/' . $this->_guestdata['lastname'] . '/' . $this->_guestdata['accesscode'];
                $html = str_replace('[#qr_ticket_open]', '<img src="http://qr.line5.net/png.php?content=' . urlencode($codeurl) . '&qr.png" />', $html);
            } else {
                $codeurl = 'https://check-in.fast-lane.org/qr/' . $this->_eventId . '/' . $this->_guestdata['lastname'] . '/' . $this->_guestdata['accesscode'];
                $html = str_replace('[#qr_ticket_open]', '<img src="https://qr.line5.net/png.php?content=' . urlencode($codeurl) . '&qr.png" />', $html);
            }
        }
        $html = preg_replace_callback('/(\[{1})(.*?)(\]{1})/s', array(
            $this,
            'replaceCallback'
        ), $html);
        return $html;
    }

    /**
     * replaces matches with $data[$match]
     *
     * @param string $match
     * @param array $data
     * @return string
     */
    protected function replaceCallback($match)
    {
        $data = '';
        if (substr($match[2], 0, 1) == '#') {
            switch ($match[2]) {
                case '#qr_ticket_open':
                    // $this->_emailAttachedImages[] = 'qr_ticket_open';
                    $data = '<img src="cid:qr_ticket_open" alt="QR Code Ticket" />';
                    break;
            }
        } elseif (substr($match[2], 0, 3) == 'ev.') {
            $data = $this->_eventdata[substr($match[2], 3)];
        } else {
            $data = $this->_guestdata[$match[2]];
        }
        return ($data);
    }

    /**
     * reads all data for the currently active record from the database
     * and stores it in $this->_emailtemplatedata
     *
     * @return boolean determines if the action was successful
     */
    private function readEmailtemplateData()
    {
        $ok = false;
        $this->_pdoObj = dbconnection::getInstance();
        
        // read email data
        $tablename = 'emailtemplate_' . $_SESSION['customer']['customer_id'];
        $sql = "SELECT * ";
        $sql .= " FROM " . $tablename . " WHERE 1 = 1
				AND deleted = 0
				;";
        $pdoStatement = $this->_pdoObj->prepare($sql, array(
            PDO::ATTR_CURSOR => PDO::CURSOR_FWDONLY
        ));
        $pdoStatement->execute(array());
        if ($pdoStatement->errorCode() != 0) {
            $this->addError('', gettext('SQL Error'), 1, __FILE__ . '/' . __LINE__ . ' ' . print_r($pdoStatement->errorInfo(), true));
        } elseif ($pdoStatement->rowCount() == 0) {
            $this->addError('', gettext('SQL Error'), 1, __FILE__ . '/' . __LINE__ . ' Errorcode 0: ' . print_r($pdoStatement->errorInfo(), true));
        } else {
            $this->_emaildata = $pdoStatement->fetch();
        }
        return $ok;
    }

    /**
     * saves an emailtemplate to the database
     * updated 02.03.2016
     *
     * @param integer $id
     *            contains the emailtemplate id
     * @param string $title
     *            contains the emailtemplate title
     * @param text $description
     *            contains the description of the emailtemplate
     * @param text $content
     *            contains the template (= html code)
     */
    private function saveEmailtemplate($id, $title, $description, $content, $subject)
    {
        // ToDo: we need the real customer id here!
        $customer_id = $_SESSION['customer']['customer_id'];
        $newOrUpdate = 'new';
        if (is_numeric($id) && $id > 0) {
            $newOrUpdate = 'update';
        }
        $sqlArr = array(
            'title' => $title,
            'description' => $description,
            'content' => $content,
            'fcustomer_id' => $customer_id,
            'emailsubject' => $subject
        );
        if ($newOrUpdate == 'new') {
            $sql = "INSERT INTO emailtemplate_" . $_SESSION['customer']['customer_id'] . " ";
        } else {
            $sql = "UPDATE emailtemplate_" . $_SESSION['customer']['customer_id'] . " ";
        }
        $sql .= " SET
				title = :title,
				description = :description,
				content = :content,
				fcustomer_id = :fcustomer_id,
				emailsubject = :emailsubject
				";
        if ($newOrUpdate == 'update') {
            $sql .= " WHERE emailtemplate_id = :emailtemplate_id";
            $sqlArr['emailtemplate_id'] = $id;
        }
        $this->_pdoObj = dbconnection::getInstance();
        $pdoStatement = $this->_pdoObj->prepare($sql, array(
            PDO::ATTR_CURSOR => PDO::CURSOR_FWDONLY
        ));
        $pdoStatement->execute($sqlArr);
        if ($pdoStatement->errorCode() * 1 != 0) {
            $this->addError('', gettext('SQL Error'), 1, __FILE__ . '/' . __LINE__ . ' ' . print_r($pdoStatement->errorInfo(), true));
        } elseif ($pdoStatement->rowCount() == 0) {
            $this->addError('', gettext('SQL Error'), 1, __FILE__ . '/' . __LINE__ . ' Errorcode 0: ' . print_r($pdoStatement->errorInfo(), true));
        }
    }

    /**
     * Saves a record via the API
     */
    public function apiPutRecord($pt)
    {
        $this->checkEmailtemplate($pt);
        $this->saveEmailtemplate($pt['emailtemplate_id'], $pt['title'], $pt['description'], $pt['content'], $pt['emailsubject']);
    }

    private function checkEmailtemplate(&$ps)
    {
        if (strlen($ps['title']) < 3) {
            $this->addError('title', gettext('The email template title must consist of at least 3 characters.'));
        }
    }

    /**
     * returns data of an event for the api
     * record id must have been set before via setRecordId
     *
     * @return array contains an array of events
     */
    public function apiGetRecord($subentity = null, $subentityid = null)
    {
        if ($subentity == null || count($subentity) == 0) {
            $res = array(
                'emailtemplate' => $this->readRecord($this->_recordId, true)
            );
        }
        return $res;
    }

    public function setRecordId($id)
    {
        $res = false;
        $this->_recordId = $id;
        $this->_emailtemplatedata = $this->readRecord($this->_recordId, true);
        if ($this->_emailtemplatedata != null) {
            $res = true;
        }
        return $res;
    }

    /**
     * returns an array with the data of the record with the given id
     *
     * @param int $id
     *            contains the email id
     * @param boolean $assoc
     *            determines if only assoc array should be returned
     * @return null|array contains data of the emailtemplate record
     */
    private function readRecord($id, $assoc = false)
    {
        try {
            $row = null;
            $sql = "SELECT * FROM emailtemplate_" . $_SESSION['customer']['customer_id'] . "  
					WHERE emailtemplate_id = :id;";
            // ToDo: protect by checking for customer id
            $this->_pdoObj = \dbconnection::getInstance();
            $pdoStatement = $this->_pdoObj->prepare($sql, array(
                \PDO::ATTR_CURSOR => \PDO::CURSOR_FWDONLY
            ));
            $pdoStatement->execute(array(
                ':id' => $id
            ));
            if ($pdoStatement->errorCode() * 1 != 0) {
                $this->addError('', gettext('SQL Error') . ': ' . print_r($pdoStatement->errorInfo(), true), 1);
            } else {
                if ($assoc === true) {
                    $row = $pdoStatement->fetch(\PDO::FETCH_ASSOC);
                } else {
                    $row = $pdoStatement->fetch();
                }
            }
        } catch (Exception $e) {
            $this->addError('', gettext('SQL Error'), 1, __FILE__ . '/' . __LINE__ . ': ' . print_r($e, true));
        }
        return $row;
    }

    public function setEventId($id)
    {
        $res = false;
        if ($this->_eventId == null) {
            $this->_eventId = $id;
            $res = true;
        }
        return $res;
    }

    /**
     * returns a list of emailtemplates for the api
     *
     * @return array contains emailtemplates
     */
    public function apiGetList($entity = null, $params = null)
    {
        $res = null;
        if ($entity == null) {
            $r = $this->getEmailtemplateList();
            $res['emailtemplates'] = $r;
        } else {
            $this->addError('', 'entity ' . $entity . 'not available');
        }
        return $res;
    }

    /**
     * returns an array of all emailtemplates for the current customer
     * this array can easily converted via json_encode
     *
     * @return multitype:array
     */
    public function getEmailtemplateList()
    {
        $sql = "SELECT * FROM emailtemplate_" . $_SESSION['customer']['customer_id'] . " 
				WHERE fcustomer_id = :fcustomer_id
				ORDER BY created_time DESC
				;";
        $this->_pdoObj = dbconnection::getInstance();
        $pdoStatement = $this->_pdoObj->prepare($sql, array(
            PDO::ATTR_CURSOR => PDO::CURSOR_FWDONLY
        ));
        $pdoStatement->execute(array(
            ':fcustomer_id' => $_SESSION['customer']['customer_id']
        ));
        if ($pdoStatement->errorCode() > 0) {
            $this->addError('', gettext('Error reading the list of emailtemplates.'), 1);
        } else {
            while ($row = $pdoStatement->fetch()) {
                $res[] = array(
                    'id' => $row['emailtemplate_id'],
                    'title' => $row['title'],
                    'description' => $row['description']
                );
            }
        }
        return $res;
    }

    static function returnEmptyTemplate()
    {
        $tpl = array(
            'title' => 'Demo Template',
            'description' => 'This template is used, if you have not created your own email template yet.',
            'content' => 'This template needs to be adapted. It is for [firstname] [lastname].',
            'fcustomer_id' => $_SESSION['customer']['customer_id'],
            'emailsubject' => 'Demo Email'
        );
        return $tpl;
    }
}