<?php

/**
 *
 * @author Line5
 *        
 */
class l5sys
{

    /**
     * contains the url parameters
     *
     * @var array
     */
    protected $_gt = array();

    /**
     * contains the record id, or null if no record is selected
     *
     * @var int|null
     */
    protected $_recordId = null;

    /**
     * contains POST parameters
     *
     * @var array
     */
    protected $_ps = array();

    /**
     * contains all errors
     *
     * @var array
     */
    protected $_err = array();

    /**
     * PDO object for database access
     *
     * @var object
     */
    protected $_pdoObj = false;

    /**
     * language
     *
     * @var string
     */
    public $_lg = 'de';

    public $_locale = 'de_DE';

    public $_weekdaysLong = array();

    /**
     * Stores information about which images need to be added to an email
     *
     * @var array
     */
    protected $_emailAttachedImages = array();

    /**
     * adds an error to the $_err array
     *
     * @param string $field
     *            // determines the form field name, to which the error refers. Can be left empty for general errors.
     * @param string $note
     *            // the error description, shown to the GUI user
     * @param number $urgency
     *            // urgency:
     *            // 1 = very important 5 = debug
     *            // 1: sends mail to admin
     *            // -1: sends mail to admin, but does not display the error to the user
     */
    protected function addError($field, $note, $urgency = 5, $details = null)
    {
        global $glob;
        try {
            $er['field'] = $field;
            $er['note'] = $note;
            if (DEVENV == true) {
                $er['note'] .= ' -- Details: ' . $details;
            }
            if ($urgency > - 1) {
                $this->_err[] = $er;
            }
            if ($urgency == 1) {
                $tracex = debug_backtrace();
                $trace = $tracex[1];
                
                $nl = "\n";
                $msgtext = date('Ymd-His') . ' - fluxline - ' . $note . '<br />';
                $msgtext .= ' File: ' . $trace['file'] . $nl . 'line ' . $trace['line'] . $nl;
                $msgtext .= ' Class: ' . $trace['class'] . ' -> ' . $trace['function'] . $nl;
                $msgtext .= ' Details: ' . print_r($details, true) . $nl;
                $msgtext .= ' Args: ' . print_r($trace['args'], true) . $nl;
                $msgtext .= ' Session Data: ' . print_r($_SESSION, true) . $nl;
                $msgtext .= print_r($trace[1], true);
                error_log($message);
                $this->sendEmailToUser('[fluxline ERR] Fehler (' . ENV_NAME . ' / ' . FLUXLINE_VERSION . ')', nl2br($msgtext), 'HTML Mail', $glob['adminemail']);
            }
        } catch (Exception $e) {
            error_log('fehler? ' . print_r($e, true));
        }
    }

    /**
     * sends an email with the given content to the specified user.
     *
     * @param string $subject
     * @param string $html
     * @param string $txt
     * @param string $destination
     *            // email address of the recipient
     */
    protected function sendEmailToUser($subject, $html, $txt, $destination)
    {
        global $glob;
        $mail = new PHPMailer(true);
        /*
         * if (DEVENV === true) {
         * $mail->IsSMTP();
         * $mail->Host = SMTP_HOST;
         * $mail->SMTPAuth = true;
         * $mail->Username = SMTP_USER;
         * $mail->Password = SMTP_PASS;
         * $mail->Port = 25;
         * }
         */
        $mail->From = $glob['mailsender'];
        $mail->FromName = $glob['mailsendername'];
        $mail->AddAddress($destination); // Name is optional
        $mail->AddReplyTo($glob['mailsender'], $glob['mailsendername']);
        $mail->AddBCC($glob['mailallbcc']);
        $mail->WordWrap = 50;
        $mail->IsHTML(true);
        $mail->Subject = $subject;
        $mail->Body = $html;
        $mail->AltBody = $txt;
        $mail->CharSet = 'UTF-8';
        if (! $mail->Send()) {
            $this->addError('', gettext('The email to a user could not be sent. EI: ' . $mail->ErrorInfo));
        }
    }

    /**
     * sends an email in the name of the customer
     * with the given content to the specified recipient.
     *
     * @param string $subject
     * @param string $html
     * @param string $txt
     * @param string $destination
     *            // email address of the recipient
     * @param array $eventdata
     * @param array $guestdata
     */
    protected function sendEmailToRecipient($subject, $html, $txt, $destination, $eventdata, $guestdata, $attachments = null, $attachmentsByString = null)
    {
        global $glob;
        try {
            $ret = '';
            if (isset($eventdata['outgoingmailredirection']) && $eventdata['outgoingmailredirection'] == 1 && (! isset($eventdata['outgoingmailredirectiontargets']) || $eventdata['outgoingmailredirectiontargets'] == '')) {} else {
                $mail = new PHPMailer(true);
                if (DEVENV === true) {
                    $mail->IsSMTP();
                    $mail->Host = SMTP_HOST;
                    $mail->SMTPAuth = true;
                    $mail->Username = SMTP_USER;
                    $mail->Password = SMTP_PASS;
                }
                $mail->From = $eventdata['mailsender'];
                $mail->Sender = $eventdata['mailsender'];
                $mail->FromName = $eventdata['mailsendername'];
                if (isset($eventdata['outgoingmailredirection']) && $eventdata['outgoingmailredirection'] == 1) {
                    $destination = '';
                    if (isset($eventdata['outgoingmailredirectiontargets']) && $eventdata['outgoingmailredirectiontargets'] != '') {
                        $recipients = explode(',', $eventdata['outgoingmailredirectiontargets']);
                        foreach ($recipients as $recipient) {
                            $mail->addAddress($recipient);
                        }
                    }
                } else {
                    if (strlen($destination) < 3) {
                        $this->addError('', gettext('No recipient email address given.'), 1);
                    } else {
                        $mail->AddAddress($destination); // Name is optional
                    }
                }
                $mail->AddReplyTo($eventdata['mailsender'], $eventdata['mailsendername']);
                // ToDo: Should be an array / list of email addresses, comma separated
                $bccrecps = explode(',', $eventdata['mailbcc']);
                if (is_array($bccrecps)) {
                    foreach ($bccrecps as $bccrecp) {
                        $mail->AddBCC(trim($bccrecp));
                    }
                }
                $mail->WordWrap = 50;
                $mail->IsHTML(true);
                $mail->Subject = $subject;
                $mail->Body = $html;
                $mail->AltBody = $txt;
                $mail->CharSet = 'UTF-8';
                if (count($this->_emailAttachedImages) > 0) {
                    foreach ($this->_emailAttachedImages as $eai) {
                        if ($eai == 'qr_ticket_open') {
                            $codeurl = 'https://check-in.fast-lane.org/qr/' . str_replace('/', '', $eventdata['fevent_id']) . '/' . $guestdata['lastname'] . '/' . $guestdata['accesscode'];
                            $c = $this->getQrCode($codeurl);
                            $mail->AddStringEmbeddedImage($c, 'qr_ticket_open', 'qr_ticket_open_' . $eventdata['fevent_id'] . '_' . $guestdata['accesscode'] . '.png', 'base64', 'image/png', 'attachment');
                        } elseif ($eai == 'qr_ticket_vcard') {
                            $codeurl = "BEGIN:VCARD\nFN:";
                            if (strlen($guestdata['firstname']) > 0) {
                                $codeurl .= $guestdata['firstname'] . " ";
                            }
                            $codeurl .= $guestdata['lastname'] . "\n";
                            $codeurl .= "ORG:" . $guestdata['company'] . "\n";
                            $codeurl .= "TITLE:" . $guestdata['accesscode'] . "\n";
                            $codeurl .= "END:VCARD";
                            $c = $this->getQrCode($codeurl, true);
                            $mail->AddStringEmbeddedImage($c, 'qr_ticket_vcard', 'qr_ticket_vcard_' . $eventdata['fevent_id'] . '_' . $guestdata['accesscode'] . '.png', 'base64', 'image/png', 'attachment');
                        } else {
                            // it is a URL
                            $c = $this->getImage($eai['url']);
                            $parts = explode('/', $eai['url']);
                            $fname = $parts[count($parts) - 1];
                            $fnameparts = explode('.', $fname);
                            $ext = strtolower($fnameparts[count($fnameparts) - 1]);
                            $ftype = 'image/png';
                            if ($ext == 'png' || $ext == 'jpg' || $ext == 'gif') {
                                $ftype = 'image/' . $ext;
                            }
                            $mail->AddStringEmbeddedImage($c, $eai['cid'], $fname, 'base64', $ftype, 'inline');
                        }
                    }
                }
                foreach ($attachments as $att) {
                    $mail->addAttachment($att);
                }
                if (is_array($attachmentsByString)) {
                    foreach ($attachmentsByString as $att) {
                        if ($att !== null) {
                            if (! isset($att['disposition'])) {
                                $att['disposition'] = null;
                            }
                            $mail->addStringAttachment($att['content'], $att['filename'], $att['encoding'], $att['type']);
                        }
                    }
                }
                if (USESMTPRELAY === true || !empty($eventdata['smtphost'])) {
                    require_once BASEDIR . 'lib/phpmailer/class.smtp.php';
                    $mail->IsSMTP();
                    if (!empty($eventdata['smtphost'])) {
                        $mail->Host = $eventdata['smtphost'];
                    } else {
                        $mail->Host = SMTP_HOST;
                    }
                    
                    if (!empty($eventdata['smtpusername']) && !empty($eventdata['smtppassword'])) {
                        $mail->SMTPAuth = true;
                        $mail->Username = $eventdata['smtpusername'];
                        $mail->Password = $eventdata['smtppassword'];
                    } else {
                        $mail->SMTPAuth = false;
                    }
                    if (!empty($eventdata['smtpsecure'])) {
                        $mail->SMTPSecure = $eventdata['smtpsecure'];
                    }
                    $mail->SMTPDebug = 0;
                }
                if (! $mail->Send()) {
                    $this->addError('', 'Die Nachricht an ' . print_r($destination, true) . ' konnte nicht gesendet werden.', 1, __FILE__ . '/' . __LINE__ . ': Problem beim Mailversand Event ' . $_SESSION['fevent_id'] . $mail->ErrorInfo . '. - Subject: ' . $subject . ' Inhalt: ' . $contentHtml);
                } else {
                    // ToDo: log email!
                    $sql = "";
                }
                $ret = $mail->getSentMIMEMessage();
            }
        } catch (Exception $e) {
            $this->addError('', gettext('Email could not be sent.'), 1, __FILE__ . '/' . __LINE__ . ': Problem beim Mailversand Event ' . $_SESSION['fevent_id'] . $mail->ErrorInfo . '. - Subject: ' . $subject . ' Inhalt: ' . $contentHtml);
        }
        return $ret;
    }

    private function getQRCode($content, $withSpace = false)
    {
        if ($withSpace == false) {
            $content = str_replace(' ', '_', $content);
        }
        $c = file_get_contents('http://qr.line5.net/png.php?content=' . urlencode($content));
        return $c;
    }

    /**
     * sets the record id
     *
     * @param int|null $id
     */
    public function setRecordId($id)
    {
        $this->_recordId = $id;
    }

    /**
     * returns the record id
     */
    public function getRecordId()
    {
        return $this->_recordId;
    }

    /**
     * returns an array of errors
     */
    public function getErrors()
    {
        return $this->_err;
    }

    protected function getImage($url)
    {
        $c = null;
        try {
            $c = @file_get_contents($url);
            if ($c === false) {
                $this->addError('', gettext('Problem downloading file:') . ' ' . $url);
            }
        } catch (Exception $e) {
            $this->addError('', gettext('Problem downloading file:') . ' ' . $url);
        }
        return $c;
    }

    public function datefltophp($fldate)
    {
        $year = substr($fldate, 0, 4);
        $month = substr($fldate, 4, 2);
        $day = substr($fldate, 6, 2);
        $hour = substr($fldate, 8, 2);
        $mint = substr($fldate, 10, 2);
        $sec = 0;
        $phpdate = mktime($hour, $mint, $sec, $month, $day, $year);
        return $phpdate;
    }

    public function initWeekdays()
    {
        $this->_weekdaysLong = array(
            gettext('WKD_SUNDAY_LONG'),
            gettext('WKD_MONDAY_LONG'),
            gettext('WKD_TUESDAY_LONG'),
            gettext('WKD_WEDNESDAY_LONG'),
            gettext('WKD_THURSDAY_LONG'),
            gettext('WKD_FRIDAY_LONG'),
            gettext('WKD_SATURDAY_LONG')
        );
    }
}