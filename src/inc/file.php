<?php

/**
 * handles "internal files" - these are files, that are only accessible after authentication via the backend.
 * @author fluxcess GmbH
 *
 */
class internalfile extends l5sys
{

    /**
     * This is the constructor
     *
     * @param string $lg
     *            the language
     * @param string $locale
     *            the locale
     */
    function __construct($lg, $locale)
    {
        $this->_locale = $locale;
        $this->_lg = $lg;
    }

    /**
     * returns a list of extensions for the api
     *
     * @return array contains events
     */
    public function apiGetList($entity = null, $params = null)
    {
        $res = null;
        if ($entity == null) {
            $r = $this->getExtensionList();
            $res['extensions'] = $r;
        }
        return $res;
    }

    public function getList()
    {
        return $this->getExtensionList();
    }

    public function getForOptionList()
    {
        $res = array();
        return $res;
    }

    public function getExtensionName($id)
    {
        $extensionName = "";
        try {
            $sql = "SELECT fname FROM extension WHERE extension_id = :id;";
            $this->_pdoObj = dbconnection::getInstance();
            $pdoStatement = $this->_pdoObj->prepare($sql, array(
                PDO::ATTR_CURSOR => PDO::CURSOR_FWDONLY
            ));
            $pdoStatement->execute(Array(
                'id' => $id
            ));
            if ($pdoStatement->errorCode() * 1 != 0) {
                $this->addError('', $pdoStatement->errorInfo(), 1);
            } else {
                $row = $pdoStatement->fetch(PDO::FETCH_ASSOC);
                $extensionName = $row['fname'];
            }
        } catch (Exception $e) {
            $this->addError('', 'Error reading extension name', 1, $e->getMessage());
        }
        return $extensionName;
    }

    /**
     * Saves a record via the API
     */
    public function apiPutRecord($put)
    {
        $ev = new event($this->_lg, $this->_locale);
        print_r($_FILES);
        $dir = BASEDIR . 'files/' . $_SESSION['customer']['customer_id'] . '/hidden/';
        if (! is_dir($dir)) {
            mkdir($dir);
        }
        $speicherOrtUndName = $dir . $_FILES['datei']['name'];
        if (move_uploaded_file($_FILES['datei']['tmp_name'], $speicherOrtUndName) == false) {
            $this->addError('datei', gettext("Error saving the file."), 1);
        }
        // $this->addFile($put['fevent_id'], $put['extension_id'], $put['description']);
    }
    
}