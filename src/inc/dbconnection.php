<?php

/**
 * contains the database class
 */

/**
 * singleton class for the database connection
 * 
 * @author Line5
 *        
 */
class dbconnection
{

    /**
     * contains the instance for singleton
     * 
     * @var dbconnection
     */
    private static $instance = false;

    /**
     * contains the database connection handle
     * 
     * @var handle
     */
    private $_connectionHandle = false;

    /**
     * empty constructor for singleton enforcement
     */
    private function __construct()
    {}

    /**
     * empty clone function for singleton enforcement
     */
    private function __clone()
    {}

    /**
     * instanciator
     * 
     * @return dbconnection
     */
    public static function getInstance()
    {
        global $glob;
        if (! dbconnection::$instance) {
            dbconnection::$instance = new PDO("mysql:host=" . $glob['DB_HOST'] . ";dbname=" . $glob['DB_DB'], $glob['DB_USER'], $glob['DB_PW']);
            dbconnection::$instance->query("SET NAMES 'utf8'");
            dbconnection::$instance->query("SET CHARACTER SET 'utf8'");
            dbconnection::$instance->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
        }
        return dbconnection::$instance;
    }

    /**
     * opens the connection to the database
     */
    public function openConnection()
    {
        global $glob;
        $this->_connectionHandle = new PDO("mysql:host=" . $glob['DB_HOST'] . ";dbname=" . $glob['DB_DB'], $glob['DB_USER'], $glob['DB_PW']);
        $this->query("SET NAMES 'utf8'");
        $this->query("SET CHARACTER SET 'utf8'");
        $this->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
    }
}
?>