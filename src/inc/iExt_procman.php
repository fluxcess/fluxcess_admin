<?php

interface iExt_procman
{

    public function getWorkflowItem($regprocstepId);

    public function getSettingsPage($regprocstepId);

    /**
     * adds column(s) and/or table(s)
     * 
     * @param int $eventId
     * @param int $regprocstepId
     */
    public function setupStep($eventId, $regprocstepId);

    /**
     * removes column(s) and/or table(s)
     * 
     * @param int $regprocstepid
     */
    public function removeStep($eventId, $regprocstepId);
}