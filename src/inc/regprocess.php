<?php

interface itf_regprocess
{
}

class regprocess extends l5sys implements itf_regprocess
{

    private $_extensions = array();

    private function getAvailableExtensions()
    {
        $this->readAvailableExtensions(1);
        return $this->_extensions;
    }

    private function readAvailableExtensions($type = 1)
    {
        $this->_extensions = array();
        $sql = "SELECT * FROM extensions WHERE type = :type;";
        $this->_pdoObj = dbconnection::getInstance();
        $pdoStatement = $this->_pdoObj->prepare($sql, array(
            PDO::ATTR_CURSOR => PDO::CURSOR_FWDONLY
        ));
        $pdoStatement->execute(Array(
            ':type' => $type
        ));
        if ($pdoStatement->errorCode() * 1 != 0) {
            while ($row = $pdoStatement->fetch()) {
                $this->_extensions[] = $row;
            }
        } else {
            $this->addError('sql', gettext('SQL problem reading extension list'), 1, print_r($pdoStatement->errorInfo(), 1));
        }
    }
}