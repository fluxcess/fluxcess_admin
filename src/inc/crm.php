<?php

/**
 * Contains the entityCRM class
 */

/**
 * CRM object.
 * Only for enabling / disabling the CRM module.
 *
 * @author Line5
 *        
 */
class crm extends l5sys
{

    /**
     * Data of this email
     *
     * @var array
     */
    // private $_emaildata = array();
    
    /**
     * Id of the currently loaded email
     *
     * @var int
     */
    // private $_emailid = 0;
    
    /**
     * Id of the currently selected event
     *
     * @var int
     */
    // private $_eventId = 0;
    
    /**
     * Data of this event
     *
     * @var array
     */
    // private $_eventdata = array();
    
    /**
     * This is the constructor
     *
     * @param string $lg
     *            contains the language
     * @param string $locale
     *            contains the locale
     */
    /*
     * function __construct($lg, $locale) {
     * $this->_locale = $locale;
     * $this->_lg = $lg;
     * }
     */
    
    /**
     * entry point for GUI output
     * returns html code for the page
     *
     * @param array $gt
     * @param array $ps
     * @param array $err
     * @param boolean $proceed
     * @return Ambigous <string, void, string>
     */
    public function printPage(&$gt, &$ps, &$err, &$proceed)
    {
        $this->_gt = $gt;
        $this->_ps = $ps;
        $this->_err = $err;
        if (isset($_POST['form']) && $_POST['class'] == 'crm') {
            switch ($_POST['form']) {
                case 'create':
                    $xout = $this->handleCreate();
                    break;
                case 'deleteCRM':
                    $this->handleDelete();
                    $proceed = false;
                    break;
                case 'editCRMForm':
                    $xout .= $this->printChangeForm();
                    break;
                case 'changeCRMForm':
                    $this->saveData();
                    break;
            }
        } elseif ($this->_gt['type'] == 'editable' && $this->_gt['class'] == 'CRM') {
            $this->changeDetail();
        } else {
            if (is_numeric($this->_gt[1])) {
                // $xout = $this->printGuestPage($this->_gt[1]);
            } else {
                switch ($this->_gt[1]) {
                    case 'listeJson':
                        $proceed = false;
                        $xout = $this->printCRMListJSON();
                        break;
                    default:
                        $sm = new L5Smarty();
                        $xout = $sm->fetch('crm/overview.tpl');
                }
            }
        }
        // pass back data to calling class
        $err = $this->_err;
        return $xout;
    }

    /**
     * returns the email record for the currently active email id
     *
     * @return array:
     */
    public function getData()
    {
        // return $this->_emaildata;
    }

    /**
     * reads all data for the currently active record from the database
     * and stores it in $this->_emaildata
     *
     * @return boolean determines if the action was successful
     */
    private function readCRMData()
    {
        $ok = false;
        $this->_pdoObj = dbconnection::getInstance();
        
        // read email data
        $tablename = 'crm_' . $_SESSION['customer']['customer_id'];
        $sql = "SELECT * ";
        $sql .= " FROM " . $tablename . " WHERE 1 = 1
				AND deleted = 0
				AND archived = 0
				AND active = 1
				;";
        $pdoStatement = $this->_pdoObj->prepare($sql, array(
            PDO::ATTR_CURSOR => PDO::CURSOR_FWDONLY
        ));
        $pdoStatement->execute(array());
        if ($pdoStatement->errorCode() != 0) {
            $this->addError('', gettext('SQL Error'), 1, __FILE__ . '/' . __LINE__ . ' ' . print_r($pdoStatement->errorInfo(), true));
        } elseif ($pdoStatement->rowCount() == 0) {
            $this->addError('', gettext('SQL Error'), 1, __FILE__ . '/' . __LINE__ . ' Errorcode 0: ' . print_r($pdoStatement->errorInfo(), true));
        } else {
            $this->_emaildata = $pdoStatement->fetch();
        }
        return $ok;
    }

    /**
     * saves an email to the database
     *
     * @param integer $id
     *            contains the email id
     * @param string $subject
     *            contains the email subject
     * @param text $mailtext
     *            contains the mailtext (assumed to be html)
     * @param integer $sendstatus
     *            contains the send status (0 = draft)
     * @param array<integer> $massmailrecipients
     *            contains the selection of massmail recipient categories (0-5)
     * @param array $err
     *            contains error codes
     */
    private function saveEmail($id, $subject, $mailtext, $sendstatus, $massmailrecipients, &$err)
    {
        $newOrUpdate = 'new';
        $mid = null;
        if (is_numeric($id) && $id > 0) {
            $newOrUpdate = 'update';
            $mid = $id;
        }
        $sqlArr = array(
            'subject' => $subject,
            'mailtext' => $mailtext,
            'sendstatus' => $sendstatus
        );
        if ($newOrUpdate == 'new') {
            $sql = "INSERT INTO wemail" . $this->_eventId;
        } else {
            $sql = "UPDATE wemail" . $this->_eventId;
        }
        $sql .= " SET
				subject = :subject,
				mailtext = :mailtext,
				sendstatus = :sendstatus
				";
        if ($newOrUpdate == 'update') {
            $sql .= " WHERE email_id = :emailid";
            $sqlArr['emailid'] = $id;
        }
        $this->_pdoObj = dbconnection::getInstance();
        $pdoStatement = $this->_pdoObj->prepare($sql, array(
            PDO::ATTR_CURSOR => PDO::CURSOR_FWDONLY
        ));
        $pdoStatement->execute($sqlArr);
        if ($pdoStatement->errorCode() != 0) {
            $this->addError('', gettext('SQL Error'), 1, __FILE__ . '/' . __LINE__ . ' ' . print_r($pdoStatement->errorInfo(), true));
        } elseif ($pdoStatement->rowCount() == 0) {
            $this->addError('', gettext('SQL Error'), 1, __FILE__ . '/' . __LINE__ . ' Errorcode 0: ' . print_r($pdoStatement->errorInfo(), true));
        } else {
            if ($mid == null) {
                $mid = $pdoStatement->lastInsertId();
            }
        }
        return $mid;
    }

    /**
     * Saves a record via the API
     */
    public function apiPutRecord()
    {
        $ev = new event($this->_lg, $this->_locale);
        if ($ev->checkUserEvent($_POST['fevent_id']) == true) {
            $this->_eventId = $_POST['fevent_id'];
            $emailid = $this->saveEmail($_POST['email_id'], $_POST['massmailsubject'], $_POST['massmailcontent'], 0, array(), $err);
        } else {
            $this->addError('fevent_id', gettext('Invalid event id given.'));
            // ToDo: This error is not propagated via json answer yet!
        }
    }

    /**
     * returns data of an event for the api
     * record id must have been set before via setRecordId
     *
     * @return array contains an array of events
     */
    public function apiGetRecord($subentity = null, $subentityid = null)
    {
        if ($subentity == null) {
            $res = array(
                'email' => $this->readRecord($this->_recordId, true)
            );
        }
        return $res;
    }

    public function setRecordId($id)
    {
        $this->_recordId = $id;
    }

    /**
     * returns a list of crm objects for the api
     *
     * @return array contains crm objects
     */
    public function apiGetList($entity = null, $params = null)
    {
        $res = null;
        if ($entity == null) {
            $res['crm'] = $this->getCRMRecordList();
            $res['col'] = $this->getCRMColumnList();
        } else {
            $this->addError('', 'entity ' . $entity . 'not available');
        }
        return $res;
    }

    /**
     * returns an array with the data of the record with the given id
     *
     * @param int $id
     *            contains the email id
     * @param boolean $assoc
     *            determines if only assoc array should be returned
     * @return null|array contains data of the email record
     */
    private function readRecord($id, $assoc = false)
    {
        try {
            $row = null;
            $sql = "SELECT * FROM wemail" . $this->_eventId . " 
					WHERE email_id = :id;";
            $this->_pdoObj = dbconnection::getInstance();
            $pdoStatement = $this->_pdoObj->prepare($sql, array(
                PDO::ATTR_CURSOR => PDO::CURSOR_FWDONLY
            ));
            $pdoStatement->execute(array(
                ':id' => $id
            ));
            if ($pdoStatement->errorCode() * 1 != 0) {
                $this->addError('', gettext('SQL Error') . ': ' . print_r($pdoStatement->errorInfo(), true), 1);
            } else {
                if ($assoc === true) {
                    $row = $pdoStatement->fetch(PDO::FETCH_ASSOC);
                } else {
                    $row = $pdoStatement->fetch();
                }
            }
        } catch (Exception $e) {
            $this->addError('', gettext('SQL Error'), 1, __FILE__ . '/' . __LINE__ . ': ' . print_r($e, true));
        }
        return $row;
    }

    public function setEventId($id)
    {
        $res = false;
        if ($this->_eventId == null) {
            $this->_eventId = $id;
            $res = true;
        }
        return $res;
    }

    public function apiInvokeMethod($methodname)
    {
        switch ($methodname) {
            case 'enableCRM':
                $this->enableCRMModule();
                break;
            case 'disableCRM':
                $this->disableCRMModule();
                break;
            case 'moveRecordsToEvent':
                $this->moveRecordsToEvent();
                break;
            default:
                $this->addError('', 'method ' . $methodname . ' has not been implemented yet. ' . $this->checkCRMModule());
        }
    }

    private function enableCRMModule()
    {
        $customerid = $_SESSION['customer']['customer_id'];
        $sql = "CREATE TABLE IF NOT EXISTS `crm_" . $customerid . "` (
  				`row_id` int(11) NOT NULL AUTO_INCREMENT,
				`crm_id` int(11) NOT NULL,
  				`firstname` varchar(128) NOT NULL,
  				`lastname` varchar(128) NOT NULL,
  				`salutation` varchar(30) NOT NULL,
				`active` int(1) NOT NULL DEFAULT 1,
				`changetime` datetime,
				`archived` int(1) NOT NULL DEFAULT 0,
				`deleted` int(1) NOT NULL DEFAULT 0,
  				PRIMARY KEY (`row_id`)
				) ENGINE=InnoDB DEFAULT CHARSET=utf8;
				";
        $this->_pdoObj = dbconnection::getInstance();
        $pdoStatement = $this->_pdoObj->prepare($sql, array(
            PDO::ATTR_CURSOR => PDO::CURSOR_FWDONLY
        ));
        $pdoStatement->execute(array());
        if ($pdoStatement->errorCode() * 1 != 0) {
            $this->addError('', gettext('SQL Error') . ': ' . print_r($pdoStatement->errorInfo(), true), 1);
        } else {
            $no = $pdoStatement->rowCount();
        }
    }

    public function checkCRMModule()
    {
        $no = 0;
        $sql = "SHOW TABLES LIKE :myTableName;";
        $this->_pdoObj = dbconnection::getInstance();
        $pdoStatement = $this->_pdoObj->prepare($sql, array(
            PDO::ATTR_CURSOR => PDO::CURSOR_FWDONLY
        ));
        $pdoStatement->execute(array(
            ':myTableName' => 'crm_' . $_SESSION['customer']['customer_id']
        )); // .$_SESSION['customer_id']));
        if ($pdoStatement->errorCode() * 1 != 0) {
            $this->addError('', gettext('SQL Error') . ': ' . print_r($pdoStatement->errorInfo(), true), 1);
        } else {
            $no = $pdoStatement->rowCount();
        }
        return $no;
    }

    private function disableCRMModule()
    {
        $sql = "DROP TABLE `crm_" . $_SESSION['customer']['customer_id'] . "`;";
        $this->_pdoObj = dbconnection::getInstance();
        $pdoStatement = $this->_pdoObj->prepare($sql, array(
            PDO::ATTR_CURSOR => PDO::CURSOR_FWDONLY
        ));
        $pdoStatement->execute(array());
        if ($pdoStatement->errorCode() * 1 != 0) {
            $this->addError('', gettext('SQL Error') . ': ' . print_r($pdoStatement->errorInfo(), true), 1);
        } else {
            $no = $pdoStatement->rowCount();
        }
    }

    /**
     * returns an array of all emailtemplates for the current customer
     * this array can easily converted via json_encode
     *
     * @return multitype:array
     */
    public function getCRMRecordList()
    {
        $sql = "SELECT * FROM crm_" . $_SESSION['customer']['customer_id'] . " WHERE 1 = 1
		AND deleted = 0
				AND active = 1
				AND archived = 0
				;";
        $this->_pdoObj = dbconnection::getInstance();
        $pdoStatement = $this->_pdoObj->prepare($sql, array(
            PDO::ATTR_CURSOR => PDO::CURSOR_FWDONLY
        ));
        $pdoStatement->execute(array());
        if ($pdoStatement->errorCode() > 0) {
            $this->addError('', gettext('Error reading the list of crm objects.'), 1);
        } else {
            while ($row = $pdoStatement->fetch()) {
                $res[] = $row;
            }
        }
        return $res;
    }

    /**
     * returns an array of all crm columns for the current customer
     * this array can easily be converted via json_encode
     *
     * @return multitype:array
     */
    public function getCRMColumnList()
    {
        $sql = "SHOW COLUMNS FROM crm_" . $_SESSION['customer']['customer_id'] . " WHERE 1 = 1
				;";
        $this->_pdoObj = dbconnection::getInstance();
        $pdoStatement = $this->_pdoObj->prepare($sql, array(
            PDO::ATTR_CURSOR => PDO::CURSOR_FWDONLY
        ));
        $pdoStatement->execute(array());
        if ($pdoStatement->errorCode() > 0) {
            $this->addError('', gettext('Error reading the columns of the crm entity.'), 1);
        } else {
            while ($row = $pdoStatement->fetch()) {
                $res[] = $row;
            }
        }
        return $res;
    }

    /**
     * returns html code for the form to modify crm object data
     *
     * @return string
     */
    private function printChangeForm()
    {
        // check if guest id is numeric
        if (! is_numeric($this->_ps['crm_id'])) {
            $this->addError('', gettext('CRM Object id not numeric'));
        }
        
        if ($this->_ps['crm_id'] > 0) {
            $crmdata = $this->getCRMData($this->_ps['crm_id']);
        } else {
            $crmdata = array();
        }
        
        $sm = new L5Smarty();
        $sm->assign('ps', $crmdata);
        $sm->assign('crm_id', $this->_ps['crm_id']);
        // $sm->assign('customFields', $this->generateCustomFields($this->_ps['fevent_id'], $guestdata));
        $xout = $sm->fetch('crm/changecrmform.tpl');
        
        return $xout;
    }

    private function readEditableFields()
    {
        $f = array(
            'salutation',
            'firstname',
            'lastname'
        );
        return $f;
    }

    /**
     * checks data in $this->_ps and invokes the updateRecord
     * method if successful
     */
    private function saveData()
    {
        try {
            $id = $this->_ps['crm_id'];
            $value = $this->_ps['value'];
            $eventid = $this->_ps['fevent_id'];
            
            $fields = $this->readEditableFields();
            
            if (! is_numeric($id)) {
                $this->addError('', gettext('Invalid record id') . ': ' . $id);
            }
            
            if (count($this->_err) == 0) {
                $this->updateRecord($id, $this->_ps, $fields);
            }
        } catch (Exception $e) {
            $this->addError('', gettext('Error updating the CRM record.'), 1, __FILE__ . '/' . __LINE__ . ': ' . print_r($e->getMessage(), true));
        }
    }

    /**
     * updates a crm record
     *
     * @param int $crmid
     * @param array $newData
     *            // usually a post var set - contains data in the form fieldname>fieldvalue
     */
    private function updateRecord($crmid, $newData, $fields)
    {
        try {
            $recordsAreEqual = false;
            $this->_pdoObj = dbconnection::getInstance();
            $this->_pdoObj->beginTransaction();
            if ($crmid > 0) {
                // get current record row id
                $row_id = $this->getCurrentRowId($crmid);
                // get current crm data
                $crmData = $this->getCRMData($crmid);
                $changedCRMData = $crmData;
                // merge updated data into current record data array
                foreach ($newData as $fieldname => $fieldvalue) {
                    if (array_search($fieldname, $fields) !== false) {
                        $changedCRMData[$fieldname] = $fieldvalue;
                    }
                }
                
                // compare old and new record
                $unequalcounter = 0;
                
                foreach ($changedCRMData as $fieldname => $fieldvalue) {
                    if ($crmData[$fieldname] != $fieldvalue) {
                        $unequalcounter += 1;
                    }
                }
                if ($unequalcounter == 0) {
                    $recordsAreEqual = true;
                    // $this->addError('', "The new record contains the same data as the old record and will not be saved.", 1);
                }
                
                // remove old changetime field
                unset($changedCRMData['changetime']);
            } else {
                $recordsAreEqual = false;
                foreach ($newData as $fieldname => $fieldvalue) {
                    if (array_search($fieldname, $fields) !== false && $fieldname != 'crm_id') {
                        $changedCRMData[$fieldname] = $fieldvalue;
                    }
                }
                $changedCRMData['crm_id'] = $this->generateUniqueCRMId();
            }
            // TODO: fill editor data into new record set
            // $newData[''] = '';
            if ($recordsAreEqual == false) {
                if ($crmid > 0) {
                    // archive current record
                    $this->archiveRecord($crmid);
                    // create new record with merged data
                }
                $sql = "INSERT INTO crm_" . $_SESSION['customer']['customer_id'] . " SET ";
                foreach ($changedCRMData as $fieldname => $fieldvalue) {
                    if (! is_numeric($fieldname) && $fieldname != 'row_id') {
                        $sql .= $fieldname . " = :" . $fieldname . ", ";
                        $sqlArr[$fieldname] = $fieldvalue;
                    }
                }
                
                // $sql = substr($sql, 0, -2);
                $sql .= " changetime = NOW();";
                $pdoStatement = $this->_pdoObj->prepare($sql, array(
                    PDO::ATTR_CURSOR => PDO::CURSOR_FWDONLY
                ));
                $pdoStatement->execute($sqlArr);
                if ($pdoStatement->errorCode() != 0) {
                    $this->addError('', 'SQL Fehler' . print_r($pdoStatement->errorInfo(), true), 1);
                } else {
                    $row_id = $this->_pdoObj->lastInsertId();
                }
            }
            // In case of any error, roll back - otherwise commit.
            if (count($this->_err) > 0) {
                $this->_pdoObj->rollBack();
            } else {
                $this->_pdoObj->commit();
            }
        } catch (Exception $e) {
            $this->_pdoObj->rollBack();
            $this->addError('', 'CRM Record update / creation unsuccessful. Contact administrator.', 1, __FILE__ . '/' . __LINE__ . ': ' . print_r($e->getMessage(), true) . ' ' . print_r($sqlArr, true) . ' ' . $sql);
        }
    }

    /**
     * returns the row id of the active record for the given crm record
     *
     * @param int $crmid
     * @return int <NULL, int> // row_id
     */
    private function getCurrentRowId($crmid)
    {
        $object_id = null;
        $this->_pdoObj = dbconnection::getInstance();
        $sql = "SELECT row_id FROM crm_" . $_SESSION['customer']['customer_id'] . "
				WHERE crm_id = :gid
				AND archived = 0;";
        $sqlArr[':gid'] = $crmid;
        $pdoStatement = $this->_pdoObj->prepare($sql, array(
            PDO::ATTR_CURSOR => PDO::CURSOR_FWDONLY
        ));
        $pdoStatement->execute($sqlArr);
        if ($pdoStatement->errorCode() != 0) {
            $this->addError('', 'SQL Fehler' . print_r($pdoStatement->errorInfo(), true), 1);
        } else {
            $row = $pdoStatement->fetch();
            $object_id = $row['row_id'];
        }
        return $object_id;
    }

    /**
     * returns a unique crm object id
     * should be called by a function that opens and closes a transaction.
     *
     * @return int <NULL, number> // new, unique guest id
     */
    public function generateUniqueCRMId()
    {
        $row_id = null;
        $this->_pdoObj = dbconnection::getInstance();
        $sql = "SELECT crm_id FROM crm_" . $_SESSION['customer']['customer_id'] . "
				ORDER BY crm_id DESC LIMIT 1;";
        $pdoStatement = $this->_pdoObj->prepare($sql, array(
            PDO::ATTR_CURSOR => PDO::CURSOR_FWDONLY
        ));
        $pdoStatement->execute();
        if ($pdoStatement->errorCode() != 0) {
            $this->addError('', 'SQL Fehler' . print_r($pdoStatement->errorInfo(), true), 1);
        } elseif ($pdoStatement->rowCount() == 0) {
            $row_id = 1;
        } else {
            $row = $pdoStatement->fetch();
            $row_id = ($row['crm_id'] * 1) + 1;
        }
        return $row_id;
    }

    /**
     * returns guest data
     *
     * @param int $eventid
     * @param int $guestid
     * @return array
     */
    private function getCRMData($crm_id, $assoc = false)
    {
        $crmData = array();
        // check if crm id is numeric
        if (! is_numeric($crm_id)) {
            $this->addError('', gettext('Wrong CRM Object Id'));
        }
        
        $this->_pdoObj = dbconnection::getInstance();
        // get columns
        $tablename = 'crm_' . $_SESSION['customer']['customer_id'];
        $sql = "SELECT * FROM " . $tablename . " WHERE crm_id = :crm_id
				AND deleted = 0
				AND archived = 0
				AND active = 1;";
        
        $pdoStatement = $this->_pdoObj->prepare($sql, array(
            PDO::ATTR_CURSOR => PDO::CURSOR_FWDONLY
        ));
        $pdoStatement->execute(array(
            ':crm_id' => $crm_id
        ));
        if ($pdoStatement->errorCode() != 0) {
            $this->addError('', 'SQL Fehler' . print_r($pdoStatement->errorInfo(), true), 1);
        } elseif ($pdoStatement->rowCount() == 0) {
            $this->addError('', 'SQL Fehler mit Errorcode 0: ' . print_r($pdoStatement->errorInfo(), true), 1);
        } else {
            $n = null;
            if ($assoc == true) {
                $n = PDO::FETCH_ASSOC;
            }
            $crmData = $pdoStatement->fetch($n);
        }
        return $crmData;
    }

    /**
     * sets the "archived" property of a record to "1",
     * if it finds an undeleted and unarchived record for a given guest id
     *
     * @param int $guestId
     */
    private function archiveRecord($crm_id)
    {
        $sql = "UPDATE crm_" . $_SESSION['customer']['customer_id'] . "
				SET archived = 1,
				active = 0
				WHERE crm_id = :crm_id
				AND archived = 0
				AND deleted = 0
				;
				";
        $this->_pdoObj = dbconnection::getInstance();
        $pdoStatement = $this->_pdoObj->prepare($sql, array(
            PDO::ATTR_CURSOR => PDO::CURSOR_FWDONLY
        ));
        $pdoStatement->execute(Array(
            ':crm_id' => $crm_id
        ));
        if ($pdoStatement->errorCode() != 0) {
            $this->addError('', 'SQL Fehler', 1, print_r($pdoStatement->errorInfo(), true));
        }
        if ($pdoStatement->rowCount() == 0) {
            $this->addError('', 'SQL Fehler', 1, __FILE__ . '/' . __LINE__ . ': Not able to find unarchived and undeleted record for event ' . $eventId . ' guest ' . $guestId . ':');
        }
    }

    private function moveRecordsToEvent()
    {
        $fevent_id = $_POST['selectevent'];
        $_POST['fevent_id'] = $fevent_id;
        
        $crm_ids = explode(',', $_POST['crm_ids']);
        // ToDo: check column existence (crm_id) in zguest table
        $this->checkZguestCrmIdColumn($fevent_id);
        
        // ToDo: Write these lines to the guest table :-)
        
        $gs = new guest($this->_lg, $this->_locale);
        foreach ($crm_ids as $crm_id) {
            $gs->handleCreate($crm_id);
        }
    }

    private function checkZguestCrmIdColumn($fevent_id)
    {
        $sql = "SHOW COLUMNS FROM `zguest" . $fevent_id . "` LIKE 'crm_id';";
        $this->_pdoObj = dbconnection::getInstance();
        $pdoStatement = $this->_pdoObj->prepare($sql, array(
            PDO::ATTR_CURSOR => PDO::CURSOR_FWDONLY
        ));
        $pdoStatement->execute(Array());
        if ($pdoStatement->errorCode() != 0) {
            $this->addError('', 'SQL Fehler', 1, print_r($pdoStatement->errorInfo(), true));
        }
        if ($pdoStatement->rowCount() == 0) {
            // if the column does not exist yet, we add it.
            $sql = "ALTER TABLE `zguest" . $fevent_id . "` ADD `crm_id` INT NULL DEFAULT NULL 
					AFTER `row_id`, ADD UNIQUE (`crm_id`) ; ";
            $pdoStatement = $this->_pdoObj->prepare($sql, array(
                PDO::ATTR_CURSOR => PDO::CURSOR_FWDONLY
            ));
            $pdoStatement->execute(Array());
            if ($pdoStatement->errorCode() != 0) {
                $this->addError('', 'SQL Fehler', 1, print_r($pdoStatement->errorInfo(), true));
            }
        }
        
        // ToDo: the crm_id column should be removed when disabling the crm module
    }
}