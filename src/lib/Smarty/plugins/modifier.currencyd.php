<?php
/*
 * Smarty plugin
 * ---------------------------
 * Type:	modifier
 * Name:	currencyd
 * Purpose:	formats numbers for european visitors
 * 
 * @param unknown_type $sString
 * @return unknown
 */
function smarty_modifier_currencyd($string)
{
	return number_format(round($string, 2), 2, ',', '.') . '&nbsp;&euro;';
}

?>