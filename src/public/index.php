<?php
/**
 * This is the start file.
 * Every http request starts here.
 * 
 * @author Line5
 */

/*
 * configuration is stored in conf/config.local.php
 * This file is only loaded if it exists
 */
if (is_file('../conf/config.local.php')) {
	include_once ('../conf/config.local.php');
}

// load default configuration 
include_once ('../conf/config.php');

// initialize and launch the application
$p = new portal();
$p->start();