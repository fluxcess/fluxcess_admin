var guestCols = new Array('guest_id', 'salutation', 'firstname', 'lastname',
		'company', 'note');

/*
 * The language variable contains the currently chosen language, and is filled
 * correctly once the "ready" event has triggered.
 */
var lg = 'en';

var loadingCounter = 0;
var eventLoaded = false;
var oTable = null;
var nEditing = null;
var lastWindowSize = $(window).width();
var guestTableSort = '';
var guestTableSortLastCol = '';
var guestTableSortLastSortDirection = '';
var guestTableShowCount = 10;
var guestTableShowStart = 0;
var guestTableSE = '';
var guestTableCols = null;
var selectedGuests = new Array();
var fevent_id = 0;
var fevent_data = null;
var loggedin = 0;

var nextGuestId = null;

var currentEntity = '';
var currentEntityId = '';
var currentSubEntity = '';
var currentSubEntityId = '';

var currentPopup = '';

var uploadfiles;

/**
 * if a form is blocked for prevention of duplicate submissions, the name of the
 * form is stored in this variable
 */
var blockedForm = null;

// catch all form submits and submit data via AJAX
$(document).on('submit', 'form', function() {
	var ret = false;
	if (!$(this).hasClass('specialForm')) {
		return handleFormSubmit(this)
	}
	if ($(this).hasClass('realsubmit')) {
		ret = true;
	}
	return ret;
});

$(document).on('change', 'select', function() {
	if ($(this).attr("id") == "selectevent") {
		goToEvent($(this).val());
	}
});

function startLoading() {
	if (loadingCounter == 0) {
		loading("show");
	}
	loadingCounter += 1;
}

function endLoading() {
	loadingCounter -= 1;
	if (loadingCounter == 0) {
		loading("hide");
	}
}

function goToEvent(id, additionalpart) {
	var urlEvent = "/api/events/" + id;
	var dataType = "json";
	startLoading();
	clearEventData();
	// if (changePage == true) {
	// navigate('#pageEvent');
	// }
	if (additionalpart) {
		$('#pageEvent').find('.subpage').hide();
		$('#pageEvent').find('.' + additionalpart).show();
	}
	$.ajax({
		type : "GET",
		url : urlEvent,
		dataType : dataType
	}).done(function(data) {
		fillEventData(data);
		eventLoaded = true;
	}).always(function(data) {
		endLoading();
	});
	loadEmailTemplateList();
}

function loadEmailTemplateList() {
	var urlEvent = "/api/emailtemplates/";
	var dataType = "json";
	startLoading();
	// clearEData();
	// if (changePage == true) {
	// navigate('#pageEvent');
	// }
	/*
	 * if (additionalpart) { $('#pageEvent').find('.subpage').hide();
	 * $('#pageEvent').find('.' + additionalpart).show(); }
	 */
	$.ajax({
		type : "GET",
		url : urlEvent,
		dataType : dataType
	}).done(function(data) {
		fillEmailTemplateLists(data);
		eventLoaded = true;
	}).always(function(data) {
		endLoading();
	});
}

function goToEmailtemplate(id) {
	var urlEmailtemplate = "/api/emailtemplates/" + id;
	var dataType = "json";
	startLoading();
	clearEmailtemplateData();
	$.ajax({
		type : "GET",
		url : urlEmailtemplate,
		dataType : dataType
	}).done(function(data) {
		fillEmailtemplateData(data);
		// eventLoaded = true;
	}).always(function(data) {
		endLoading();
	});
}
function clearEmailtemplateData() {
	// $('.emailtemplateform')
}
function fillEmailtemplateData(data) {
	for (i = 0; i < Object.keys(data.emailtemplate).length; i++) {
		var key = Object.keys(data.emailtemplate)[i];
		var val = data.emailtemplate[key];
		$('.emailtemplateform').find('[name="' + key + '"]').val(val);
		$('.emt_' + key).html(val);
	}
	;
	$('.emailtemplateform').data('url',
			'/emailtemplates/' + data.emailtemplate.emailtemplate_id);
}
function goToCRM(id, additionalpart) {
	// var urlCRM = "/api/crm/" + id;
	var dataType = "json";
	startLoading();
	// clearCRMData();
	// if (changePage == true) {
	// navigate('#pageEvent');
	// }
	if (id) {
		$('#pageCRM').find('.subpage').hide();
		$('#pageCRM').find('.' + id).show();
	}/*
		 * $.ajax({ type : "GET", url : urlEvent, dataType : dataType
		 * }).done(function(data) { fillEventData(data); eventLoaded = true;
		 * }).always(function(data) { endLoading(); });
		 */
}

function goToEventSubpart(additionalpart) {
	if (additionalpart) {
		$('#pageEvent').find('.subpage').hide();
		$('#pageEvent').find('.' + additionalpart).show();
	}
}

function clearEventData() {
	$('#guesttable tbody').html('');
	selectedGuests = new Array();
	// reloadGuestTable();

	$('.valEvtitle').html('Loading event...');
	$('#eventid').html('');
	$('.valEvinvited').html('');
	$('.valEvregistered').html('');
	$('.valEvcancelled').html('');
}

function fillEmailTemplateLists(data) {
	$('.emailtemplateselection').html('');
	var html = '';
	$.each(data.emailtemplates, function(key, value) {
		html += '<option value="' + value.id + '">' + value.id + " | " + value.title + '</option>';
	});
	$('.emailtemplateselection').html(html);
}
$(document).on('change', '.emailtemplateselection', function() {
	$(this).parent().find('input').val($(this).val());
});
function fillEventData(data) {
	$('#eventid').html(data.event.fevent_id);
	fevent_id = data.event.fevent_id;
	$('.uploadiframe').attr(
			'src',
			'/index.php?type=iframe&module=event&fevent_id='
					+ data.event.fevent_id + '&submodule=importfile');
	guestTableSort = '';
	guestTableSortLastCol = '';
	guestTableSortLastSortDirection = '';
	guestTableShowCount = 10;
	guestTableShowStart = 0;
	if (data.event.startpageguestlistcolumns != undefined) {
		guestTableCols = data.event.startpageguestlistcolumns;
	}
	reloadGuestTable();
	$('.hideifset').show();
	$.each(data.event, function(key, value) {
		// alert("#" + key + " = " + value);
		if ($('.settingsform').find('[name="' + key + '"]')) {
			var myObjects = $('.settingsform').find('[name="' + key + '"]');
			if (myObjects.length > 1 && key != 'fevent_id') {
				alert(">1: " + key);
			} else {
				if (myObjects.attr("type") == "checkbox") {
					if (myObjects.attr("value") == value) {
						myObjects.prop("checked", true);
					} else {
						myObjects.prop("checked", false);
					}
					try {
						myObjects.flipswitch('refresh');
					} catch (err) {
					}
				} else if ($(myObjects).hasClass('classSelect')) {
					$(myObjects).parent().find('select').val(value);
				} else {
					myObjects.val(value);
				}
			}
		}
		if ($('.valEv' + key)) {
			if (key.substring(0, 4) == 'file' && value == '') {
				$('.valEv' + key).html('---');
			} else {
				$('.valEv' + key).html(value);
			}
			$('input.valEv' + key).val(value);
		}
		// hide those error messages which are shown only
		// if a specific value is not set yet
		if (value && value != '') {
			$('.his_' + key).hide();
		}
	});

	var rows = "";
	$.each(data.event.exports, function(key, value) {
		rows += "<tr><td>" + value[1] + "</td><td>" + value[2] + "</td><td>";
		$.each(value[3], function(mkey, mvalue) {
			rows += '<a data-ajax="false" href="' + mvalue
					+ '"><img src="/pic/' + mkey + '-icon.png" alt="{"' + mkey
					+ '"|gettext}" /></a>';
		});
		rows += "</td></tr>";
	});
	$('.exportlinks tbody').html(rows);

	$('.linkFevent').each(
			function() {
				var currentLink = $(this).attr('href');
				var firstSlashPos = currentLink.indexOf("/");
				var secondSlashPos = firstSlashPos
						+ currentLink.substring(firstSlashPos + 1).indexOf('/')
						+ 1;
				var newLink = '#fevent/' + fevent_id
						+ currentLink.substr(secondSlashPos);
				$(this).attr('href', newLink);
			});

	reloadDomainTable();
	reloadFieldTable();
	reloadRegProcStepTable();
	reloadEmailtemplateTable();
	fillMultisettingsTables(data);
}

function eventpage(id) {
	$('#navtabs').tabs('option', 'active', id);
}

function handleFormSubmit(thi) {
	var useapi = false;
	var specialjs = false;
	if ($(thi).hasClass('nojssubmit')) {
		return true;
	}
	if ($(thi).hasClass('justsubmitted')) {
		return false;
	}
	if ($(thi).hasClass('noduplicatesubmissions')) {
		$(thi).addClass('justsubmitted');
	}
	if ($(thi).hasClass('apiform')) {
		useapi = true;
	}
	if ($(thi).hasClass("specialjs")) {
		specialjs = true;
	}
	var url = '/index.php?type=ajax&lg=' + lg;

	if ($(thi).data("url")) {
		url = $(thi).data("url").replace("/events/#/",
				"/events/" + fevent_id + "/");
		var guest_id = $(thi).find('[name="guest_id"]').val();
		if (guest_id != undefined) {
			url = url.replace("/guests/#/", "/guests/" + guest_id + "/");
		}
	}
	loading("show");

	if ($(thi).data("resultdiv")) {
		data.resultdiv = $(thi).data("resultdiv");
	}

	var data = $(thi).serializeArray();

	// integrate unchecked checkboxes
	var serialized = $(thi).find('input:checkbox').each(function(key, val) {
		if (!val.checked) {
			data.push({
				name : $(val).attr('name'),
				value : false
			});
		}
	});

	var method = '';

	// figure out from the form, if a special callback should be
	// executed
	// after the return value is being delivered
	var callbck = null;
	var noajax = 0;
	for (i = 0; i < data.length; i++) {
		if (data[i].name == 'cb') {
			var callbckbutt = data[i].value.split("|");
			for (ii = 0; ii < callbckbutt.length; ii++) {
				var butttypeval = callbckbutt[ii].split(":");
				if (butttypeval.length == 2) {
					if ($(thi).find('[name="formsubmittedby"]').val()
							.substring(4) == butttypeval[0]) {
						callbck = butttypeval[1];
					}
				} else {
					callbck = butttypeval[0];
				}
			}
		}
		if (data[i].name == 'noajax') {
			noajax = 1;
		}
	}
	if (specialjs == true) {
		var specialjsfunction = $(thi).data('specialjs');
		var spparts = specialjsfunction.split('|');
		if (spparts[0] == 'handleDataTakeover') {
			handleDataTakeover(thi, spparts[1]);
		} else if (spparts[0] == 'handleDataPushover') {
			handleDataPushover(thi, spparts[1]);
		}
		return false;
	} else if (useapi == true) {
		url = "/api" + url;

		// send request via official api
		method = $(thi).attr("method");
		var dataType = 'json';
		// remove all error messages
		$('.errtext').remove();
		// send data to server
		var that = thi;

		var ajaxParams = {
			type : method,
			url : url,
			data : data,
			dataType : dataType
		};
		if ($(thi).hasClass('fileupload')) {
			ajaxParams.contentType = false;
			ajaxParams.processData = false;
			var data = new FormData(thi);
			/*
			 * $.each(uploadfiles, function(key, value) { //data.append('file-' +
			 * key, value); });
			 */
			/*
			 * $.each(uploadfiles, function(key, value) { alert("J");
			 * data.append(key, value); });
			 */
			ajaxParams.data = data;
		}
		;
		$.ajax(ajaxParams).done(function(data) {
			handleFormResponse(data, that, callbck);
		}).always(function(data) {
			$(that).removeClass('justsubmitted');
			loading("hide");
		});
		$(thi).find('[name="formsubmittedby"]').val('enter');
		return false;
	} else if (noajax == 0) {
		var dataType = 'json';
		// remove all error messages
		$('.errtext').remove();
		// send data to server
		var that = thi;
		$.ajax({
			type : "POST",
			url : url,
			data : data,
			dataType : dataType
		}).done(function(data) {
			handleFormResponse(data, that, callbck);
		}).always(function(data) {
			$(that).removeClass('justsubmitted');
			loading("hide");
		});
		$(thi).find('[name="formsubmittedby"]').val('enter');
		return false;
	} else {
		return true;
	}
}

function handleFormResponse(data, that, callbck) {
	if (data.redirect && data.redirect.length > 0) {
		location.href = data.redirect;
	} else if (data.err && data.err.length > 0) {
		for (i = 0; i < data.err.length; i++) {
			var note = data.err[i].note;
			if (note == undefined || note == "") {
				note = data.err[i].message;
			}
			if ($(that).find('input[name="' + data.err[i].field + '"]').size() > 0) {
				$(that).find('input[name="' + data.err[i].field + '"]')
						.toggleClass('errorfield', true);
				$(that).find('input[name="' + data.err[i].field + '"]').before(
						"<div class='errtext' id='err" + data.err[i].field
								+ "'>" + note + "</div>");
			} else {
				if ($(that).find('h2').length > 0) {
					$(that).find('h2').after(
							"<div class='errtext' id='err" + data.err[i].field
									+ "'>" + note + "</div>");
				} else {
					$(that).prepend(
							"<div class='errtext' id='err" + data.err[i].field
									+ "'>" + note + "</div>");
				}
			}
		}
	} else {
		var those = $(that).parent();
		if (data.cont) {
			if (data.cont.length > 0) {
				$(those).html(data.cont);
			}
		}
		if (data.data) {
			if (data.data.guest_id) {
				$('#popupGuest').data('guest_id', data.data.guest_id);
			}
		}
		$(those).find('.alert').css('display', 'block');
		$('html, body').animate({
			scrollTop : 0
		}, 50);
		$(those).find('.alert').fadeOut(2000);
		if (callbck != null && callbck != "") {
			var multibutcallbck = callbck.split('|');
			for (ii = 0; ii < multibutcallbck.length; ii++) {
				var callbcks = callbck.split(";");
				for (i = 0; i < callbcks.length; i++) {
					if (callbcks[i] == 'reloadFileManager') {
						eval(callbcks[i] + "(those);");
					} else {
						eval(callbcks[i] + "();");
					}
				}
			}
		}
		$('.htmlinput').jqte();
	}
}

function deleteGuest(id) {
	if (confirm('Gast wirklich löschen?')) {
		var url = '/index.php?type=ajax&lg=' + lg + '&class=guest';
		var data = 'fevent_id=' + $('#eventid').html() + '&guest_id=' + id
				+ '&form=deleteGuest&class=guest&type=ajax&lg=' + lg;
		$.ajax({
			type : "POST",
			url : url,
			data : data,
			success : function(data) {
				if (data.err.length > 0) {
					var errtxt = '';
					for (i = 0; i < data.err.length; i++) {
						errtxt += data.err[i].note + " ";
					}
					alert(errtxt);
				} else {
					reloadGuestTable();
				}
			},
			dataType : 'json'
		});
	}
}

function deleteDomain(id) {
	if (confirm('Domain wirklich löschen?')) {
		var url = '/index.php?type=ajax&lg=' + lg + '&class=domain';
		var data = 'fevent_id=' + $('#eventid').html() + '&domain_id=' + id
				+ '&form=deleteDomain&class=domain&type=ajax&lg=' + lg;
		$.ajax({
			type : "POST",
			url : url,
			data : data,
			success : function(data) {
				if (data.err.length > 0) {
					var errtxt = '';
					for (i = 0; i < data.err.length; i++) {
						errtxt += data.err[i].note + " ";
					}
					alert(errtxt);
				} else {
					reloadDomainTable();
				}
			},
			dataType : 'json'
		});
	}
}

function deleteField(id) {
	if (confirm('Feld wirklich löschen?')) {
		var url = '/index.php?type=ajax&lg=' + lg + '&class=event';
		var data = 'fevent_id=' + $('#eventid').html() + '&field_id=' + id
				+ '&form=deleteField&class=event&lg=' + lg + '&type=ajax';
		$.ajax({
			type : "POST",
			url : url,
			data : data,
			success : function(data) {
				if (data.err.length > 0) {
					var errtxt = '';
					for (i = 0; i < data.err.length; i++) {
						errtxt += data.err[i].note + " ";
					}
					alert(errtxt);
				} else {
					// oFieldTable.fnReloadAjax();
					reloadFieldTable();
				}
			},
			dataType : 'json'
		});
	}
}

function deleteEvent(id) {
	if ($('#fffdeletesecure').val() == 'DELETE!!!'
			|| $('#fffdeletesecure').val() == 'LÖSCHEN!!!') {
		if (confirm('Event wirklich löschen?')) {
			var url = '/index.php?type=ajax&lg=' + lg + '&class=event';
			var data = 'fevent_id=' + $('#eventid').html()
					+ '&form=deleteEvent&class=event&type=ajax&lg=' + lg;
			$.ajax({
				type : "POST",
				url : url,
				data : data,
				success : function(data) {
					if (data.err.length > 0) {
						var errtxt = '';
						for (i = 0; i < data.err.length; i++) {
							errtxt += data.err[i].note + " ";
						}
						$('#deleteevent_feedback').html(errtxt);
					} else {
						location.href = '/';
					}
				},
				dataType : 'json'
			});
		}
	}
}
function deleteGuestlist(id) {
	if ($('#fffdeleteguestlistsecure').val() == 'CLEAR!!!'
			|| $('#fffdeleteguestlistsecure').val() == 'LEEREN!!!') {
		if (confirm('Gästeliste wirklich löschen?')) {
			var url = '/api/events/' + $('#eventid').html() + '/clearguestlist';
			var data = 'fevent_id=' + $('#eventid').html();
			$.ajax({
				type : "POST",
				url : url,
				data : data,
				success : function(data) {
					if (data.err && data.err.length > 0) {
						var errtxt = '';
						for (i = 0; i < data.err.length; i++) {
							errtxt += data.err[i].message + " ";
						}
						$('#deleteguestdata_feedback').html(errtxt);
					} else {
						reloadGuestTable();
						location.href = '/#fevent/' + $('#eventid').html();
					}
				},
				dataType : 'json'
			});
		}
	}
}

function deleteAccount() {
	if ($('#fffaccountdeletesecure').val() == 'DELETE!!!'
			|| $('#fffaccountdeletesecure').val() == 'LÖSCHEN!!!') {
		if (confirm('Account wirklich löschen?')) {
			var url = '/index.php?type=ajax&lg=' + lg + '&class=account';
			var data = '&form=deleteAccount&class=account&type=ajax&lg=' + lg;
			$.ajax({
				type : "POST",
				url : url,
				data : data,
				success : function(data) {
					if (data.err.length > 0) {
						var errtxt = '';
						for (i = 0; i < data.err.length; i++) {
							errtxt += data.err[i].note + " ";
						}
						$('#deleteaccount_feedback').html(errtxt);
					} else {
						location.href = '/';
					}
				},
				dataType : 'json'
			});
		}
	}
}

function moveField(id, direction, ordertype) {
	var url = '/index.php?type=ajax&lg=' + lg + '&class=event';
	var data = 'fevent_id=' + $('#eventid').html() + '&field_id=' + id
			+ '&form=moveField&class=event&ordertype=' + ordertype
			+ '&type=ajax&lg=' + lg + '&direction=' + direction;
	$.ajax({
		type : "POST",
		url : url,
		data : data,
		success : function(data) {
			if (data.err.length > 0) {
				var errtxt = '';
				for (i = 0; i < data.err.length; i++) {
					errtxt += data.err[i].note + " ";
				}
				alert(errtxt);
			} else {
				// oFieldTable.fnReloadAjax();
				reloadFieldTable();
			}
		},
		dataType : 'json'
	});
}

function toggleFieldExport(id, newStatus, ordertype) {
	var url = '/index.php?type=ajax&lg=' + lg + '&class=event';
	var data = 'fevent_id=' + $('#eventid').html() + '&field_id=' + id
			+ '&form=toggleFieldExport&class=event&type=ajax&lg=' + lg
			+ '&ordertype=' + ordertype + '&newStatus=' + newStatus;
	$.ajax({
		type : "POST",
		url : url,
		data : data,
		success : function(data) {
			if (data.err.length > 0) {
				var errtxt = '';
				for (i = 0; i < data.err.length; i++) {
					errtxt += data.err[i].note + " ";
				}
				alert(errtxt);
			} else {
				// oFieldTable.fnReloadAjax();
				reloadFieldTable();
			}
		},
		dataType : 'json'
	});
}

function generateAccessCodes() {
	$('#generateaccesscodes_feedback').html(
			'<div class="alert">... in progress ...</div>');
	var url = '/index.php?type=ajax&lg=' + lg + '&class=event';
	var data = 'fevent_id=' + $('#eventid').html()
			+ '&form=generateaccesscodes&class=event&type=ajax&lg=' + lg;
	$
			.ajax({
				type : "POST",
				url : url,
				data : data,
				success : function(data) {
					if (data.err.length > 0) {
						var errtxt = '';
						for (i = 0; i < data.err.length; i++) {
							errtxt += data.err[i].note + " ";
						}
						$('#generateaccesscodes_feedback').html(
								'<div class="alert alert-error">' + errtxt
										+ '</div>');
						$('#generateaccesscodes_feedback').find('.alert')
								.fadeOut(4000);
					} else {
						$('#generateaccesscodes_feedback').html(data.cont);
						$('#generateaccesscodes_feedback').find('.alert')
								.fadeOut(2000);
					}
				},
				dataType : 'json'
			});
}

function checkOutAllGuests() {
	$('#checkoutallguests_feedback').html(
			'<div class="alert">... in progress ...</div>');
	var url = '/index.php?type=ajax&lg=' + lg + '&class=event';
	var data = 'fevent_id=' + $('#eventid').html()
			+ '&form=checkoutallguests&class=event&type=ajax&lg=' + lg;
	$.ajax({
		type : "POST",
		url : url,
		data : data,
		success : function(data) {
			if (data.err.length > 0) {
				var errtxt = '';
				for (i = 0; i < data.err.length; i++) {
					errtxt += data.err[i].note + " ";
				}
				$('#checkoutallguests_feedback').html(
						'<div class="alert alert-error">' + errtxt + '</div>');
				$('#checkoutallguests_feedback').find('.alert').fadeOut(4000);
			} else {
				$('#checkoutallguests_feedback').html(data.cont);
				$('#checkoutallguests_feedback').find('.alert').fadeOut(2000);
			}
		},
		dataType : 'json'
	});
}

function editGuest(id) {
	$('#popupPanelGuest').html('Moment...');
	showPopup('Guest');

	var url = '/index.php?type=ajax&lg=' + lg + '&class=guest';
	var data = 'fevent_id=' + $('#eventid').html() + '&guest_id=' + id
			+ '&form=editGuestForm&class=guest&type=ajax&lg=' + lg;

	$.ajax({
		type : "POST",
		url : url,
		data : data,
		success : function(data) {
			if (data.err.length > 0) {
				var errtxt = '';
				for (i = 0; i < data.err.length; i++) {
					errtxt += data.err[i].note + " ";
				}
				alert(errtxt);
			} else {
				$('#popupPanelGuest').html(data.cont);
				/*
				 * $("#pageEvent").css("minHeight",
				 * $("#popupPanelGuest").innerHeight() + "px");
				 */
			}
		},
		dataType : 'json'
	});
}
function editCRMPerson(id) {
	$('#popupPanelCRM').html('Moment...');
	showPopup('CRM');

	var url = '/index.php?type=ajax&lg=' + lg + '&class=crm';
	var data = 'fevent_id=' + $('#eventid').html() + '&crm_id=' + id
			+ '&form=editCRMForm&class=crm&type=ajax&lg=' + lg;

	$.ajax({
		type : "POST",
		url : url,
		data : data,
		success : function(data) {
			if (data.err.length > 0) {
				var errtxt = '';
				for (i = 0; i < data.err.length; i++) {
					errtxt += data.err[i].note + " ";
				}
				alert(errtxt);
			} else {
				$('#popupPanelCRM').html(data.cont);
			}
		},
		dataType : 'json'
	});
}

function closeGuestForm() {
	closePopup();
	reloadGuestTable();
}
function closeMultisettingsForm() {
	closePopup();
}
function closeCRMForm() {
	closePopup();
	reloadCRMTable();
}
function closeFieldForm() {
	closePopup();
	reloadFieldTable();
}
function closeDomainForm() {
	closePopup();
	reloadDomainTable();
}
function showPopup(name) {
	currentPopup = name;
	$('#popupbackground').show();
	$('#popup' + name).show();
}
function closePopup() {
	$('#popupbackground').hide();
	$('#popup' + currentPopup).hide();
}
function reloadFieldTable() {
	var url = '/index.php?type=ajax&0=event&lg=' + lg + '&fevent_id='
			+ $('#eventid').html()
			+ '&submodule=printFieldListTable&ordertype=export';
	var data = 'fevent_id=' + $('#eventid').html()
			+ '&form=editGuestForm&class=guest&lg=' + lg + '&type=ajax';
	$.ajax({
		type : "GET",
		url : url,
		success : function(data) {
			if (data.err.length > 0) {
				var errtxt = '';
				for (i = 0; i < data.err.length; i++) {
					errtxt += data.err[i].note + " ";
				}
				alert(errtxt);
			} else {
				$('#fieldlisttables').html(data.cont);
			}
		},
		dataType : 'json'
	});
	var url = '/index.php?type=ajax&0=event&fevent_id=' + $('#eventid').html()
			+ '&submodule=printFieldListTable&lg=' + lg
			+ '&ordertype=fieldorder';
	var data = 'fevent_id=' + $('#eventid').html() + '&form=editGuestForm&lg='
			+ lg + '&class=guest&type=ajax';
	$.ajax({
		type : "GET",
		url : url,
		success : function(data) {
			if (data.err.length > 0) {
				var errtxt = '';
				for (i = 0; i < data.err.length; i++) {
					errtxt += data.err[i].note + " ";
				}
				alert(errtxt);
			} else {
				$('#guestformfieldlisttables').html(data.cont);
			}
		},
		dataType : 'json'
	});

	$("#guestformfieldlisttables").trigger("create");
	$("#guestformfieldlisttables").trigger("updatelayout");
	$("#pageEvent").css("minHeight",
			$("#guestformfieldlisttables").innerHeight() + "px");
	updatePageHeight();
}
function reloadEmailtemplateTable() {
	var url = '/api/emailtemplates';
	$.ajax({
		type : "GET",
		url : url,
		success : function(data) {
			if (data.err && data.err.length > 0) {
				var errtxt = '';
				for (i = 0; i < data.err.length; i++) {
					errtxt += data.err[i].note + " ";
				}
				alert(errtxt);
			} else {
				var ht = '';
				var op = '<option value="0">-</option>';
				$(data.emailtemplates).each(
						function(key, val) {
							ht += '<tr><td>' + val.id;
							ht += '<td><a href="#emailtemplate/' + val.id
									+ '">' + val.title + '</a></td>';
							ht += '<td>' + val.description + '</td>';
							ht += '<td></td>';
							ht += '</td></tr>';
							op += '<option value="' + val.id + '">' + val.title
									+ '</option>';
						});
				$('#emailtemplatetable tbody').html(ht);
				$('.emailtemplate_id_options').html(op);
			}
		},
		dataType : 'json'
	});
}

function reloadCRMTable() {
	var url = '/api/crms';
	$
			.ajax({
				type : "GET",
				url : url,
				success : function(data) {
					if (data.err && data.err.length > 0) {
						var errtxt = '';
						for (i = 0; i < data.err.length; i++) {
							errtxt += data.err[i].note + " ";
						}
						alert(errtxt);
					} else {
						var ht = '';
						var thd = '<th></th>';
						$(data.col).each(function(key, val) {
							thd += '<th>' + val.Field + '</th>';
						});
						thd += '<th></th>';
						$(data.crm)
								.each(
										function(key, val) {
											ht += '<tr data-crm_id="'
													+ val.crm_id + '">';
											ht += '<td><input type="checkbox" class="crmchck" /></td>';
											$(data.col).each(
													function(keyI, valI) {
														ht += '<td>';
														ht += val[valI.Field];
														ht += '</td>';
													});
											ht += '<td><img src="/pic/icons/delete.svg" alt="delete" class="deleteIcon" />';
											ht += '<img src="/pic/icons/edit.svg" alt="delete" class="editIcon" /></td>';
											ht += '</tr>';
										});
						$('.crmtable thead').html(thd);
						$('.crmtable tbody').html(ht);
					}
				},
				dataType : 'json'
			});
}

function updatePageHeight() {
	if ($("#pageEvent").css("minHeight") < $("#guestformfieldlisttables")
			.innerHeight()) {
		$("#pageEvent").css("minHeight",
				$("#guestformfieldlisttables").innerHeight() + "px");
	}
}
function reloadDomainTable() {
	var url = '/index.php?type=ajax&lg=' + lg + '&0=domain&fevent_id='
			+ $('#eventid').html() + '&submodule=printDomainListTable';
	var data = 'fevent_id=' + $('#eventid').html()
			+ '&form=editDomainForm&class=domain&type=ajax&lg=' + lg;
	$.ajax({
		type : "GET",
		url : url,
		success : function(data) {
			if (data.err.length > 0) {
				var errtxt = '';
				for (i = 0; i < data.err.length; i++) {
					errtxt += data.err[i].note + " ";
				}
				alert(errtxt);
			} else {
				$('#domainlisttable').html(data.cont);
			}
		},
		dataType : 'json'
	});
}

function editField(id) {
	showPopup('Field');
	$('#popupPanelField').text('Moment...');
	var url = '/index.php?type=ajax&lg=' + lg + '&class=event';
	var data = 'fevent_id=' + $('#eventid').html() + '&field_id=' + id
			+ '&form=editFieldForm&class=event&type=ajax&lg=' + lg;
	$.ajax({
		type : "POST",
		url : url,
		data : data,
		success : function(data) {
			if (data.err.length > 0) {
				var errtxt = '';
				for (i = 0; i < data.err.length; i++) {
					errtxt += data.err[i].note + " ";
				}
				alert(errtxt);
			} else {
				reloadGuestTable();
				$('#popupPanelField').html(data.cont);
				/*
				 * $("#pageEvent").css("minHeight",
				 * $("#panelfield").innerHeight() + "px");
				 */
			}
		},
		dataType : 'json'
	});
}

/**
 * remove error class from fields which are marked for having errors once
 * someone clicks them
 */
$(document).on('focus', '.errorfield', function() {
	$(this).toggleClass('errorfield', false);
	var fieldname = this.name;
	$('#err' + fieldname).remove();
})
$(document).on('keyup', '.errorfield', function() {
	$(this).toggleClass('errorfield', false);
	var fieldname = this.name;
	$('#err' + fieldname).remove();
})
$(document).keyup(function(e) {
	if (e.keyCode == 27) { // escape key
		closePopup();
	}
});

function refreshEvents() {
	var url = '/index.php?type=ajax&lg=' + lg + '&0=event&1=listeJson';
	$.ajax({
		type : "GET",
		url : url,
		success : function(data) {
			// remove all event links
			var stop = false;
			var oldEventListLength = $('.eventlist').html().length;
			var numberOfEvents = 0;
			$('.eventlist').html('');
			if (data.cont.length > 0) {
				// add event links
				for (i = 0; i < data.cont.length; i++) {
					inh = '<li><a href="#fevent/' + data.cont[i].id + '">'
							+ data.cont[i].title + '</a></li>'
					numberOfEvents += 1;
					$('.eventlist').html($('.eventlist').html() + inh);
				}
			} else {
				$(that).parent().html(data.cont);
			}
			if (oldEventListLength < 10 && numberOfEvents > 0) {
				$('.noevents').addClass('hide');
				$('.multipleevents').removeClass('hide');
			} else if (oldEventListLength > 10 && numberOfEvents == 0) {
				$('.noevents').removeClass('hide');
				$('.multipleevents').addClass('hide');
			}
		},
		dataType : 'json'

	});
}

function refreshEventTitle() {
	goToEvent(fevent_id, 'settings');
	refreshEvents();
}

function updateFileName(filename) {
	var url = '/index.php?type=ajax&lg=' + lg + '&class=event';
	var data = 'fevent_id=' + $('#eventid').html() + '&filename=' + filename
			+ '&form=updateFileName&class=event&type=ajax&lg=' + lg;
	$.ajax({
		type : "POST",
		url : url,
		data : data,
		success : function(data) {
			if (data.err.length > 0) {
				var errtxt = '';
				for (i = 0; i < data.err.length; i++) {
					errtxt += data.err[i].note + " ";
				}
				alert(errtxt);
			} else {

			}
		},
		dataType : 'json'
	});
}

function editDomain(id) {
	$('#popupPanelDomain').text('Moment...');
	var url = '/index.php?type=ajax&lg=' + lg + '&class=domain';
	var data = 'fevent_id=' + $('#eventid').html() + '&domain_id=' + id
			+ '&form=editDomainForm&class=domain&type=ajax&lg=' + lg;
	showPopup('Domain');
	$.ajax({
		type : "POST",
		url : url,
		data : data,
		success : function(data) {
			if (data.err.length > 0) {
				var errtxt = '';
				for (i = 0; i < data.err.length; i++) {
					errtxt += data.err[i].note + " ";
				}
				alert(errtxt);
			} else {
				$('#popupPanelDomain').html(data.cont);
			}
		},
		dataType : 'json'
	});
}

function createGuestTable() {
	reloadGuestTable();
}

function reloadGuestTable() {
	var id = $('#eventid').html();
	var urlEvent = "/api/events/" + id + "/guests?";
	if (guestTableSort != '') {
		urlEvent += 'sort=' + guestTableSort + '&';
	}
	if (guestTableShowCount != -1) {
		urlEvent += 'count=' + guestTableShowCount + '&';
	}
	if (guestTableShowStart != -1) {
		urlEvent += 'start=' + guestTableShowStart + '&';
	}
	if (guestTableSE != '') {
		urlEvent += 'se=' + encodeURIComponent(guestTableSE) + '&';
	}
	if (guestTableCols != null) {
		urlEvent += 'cols=' + guestTableCols + '&';
	}
	var dataType = "json";
	startLoading();
	$.ajax({
		type : "GET",
		url : urlEvent,
		dataType : dataType
	}).done(function(data) {
		fillGuestTable(data);
		eventLoaded = true;
	}).always(function(data) {
		endLoading();
	});
}

function fillGuestTable(data) {
	var newRow = '';
	var newTable = '';
	// empty table:
	if (guestTableShowStart > 1 && (!data.list || data.list.length == 0)) {
		guestTableShowStart -= guestTableShowCount;
	} else {
		// head
		if (data.cols && data.cols.length > 0) {
			var thead = "<tr><th></th>";
			for (i = 0; i < data.cols.length; i++) {
				var capt = data.cols[i].caption;
				if (data.cols[i].name == 'guest_id') {
					capt = 'Id';
				}
				thead += '<th data-cname="' + data.cols[i].name + '">' + capt
						+ '</th>';
			}
			thead += '<th></th></tr>';
			$('#guesttable thead').html(thead);
		}
		// body
		$('#guesttable tbody').html('');
		// load step by step
		if (data.list && data.list.length > 0) {
			for (yy = 0; yy < data.list.length; yy++) {
				var value = data.list[yy];
				newRow = '<tr class="nrow" data-guest_id="' + value['guest_id']
						+ '" ';
				if (jQuery.inArray(value.guest_id, selectedGuests) > -1) {
					newRow += ' class="selected" ';
				}
				newRow += '><td><input type="checkbox" class="cb" name="v" value="1" ';
				if (jQuery.inArray(value.guest_id, selectedGuests) > -1) {
					newRow += ' checked ';
				}
				newRow += '/></td>';
				for (y = 0; y < Object.keys(value).length; y++) {

					if (Object.keys(value)[y] == 'paymentstatus') {
						newRow += '<td class="center">';
						if (value[Object.keys(value)[y]] == 0) {
							newRow += '<img src="/pic/icons/exclamation_red.svg" alt="not paid yet" />';
						}
					} else if (Object.keys(value)[y] == 'invoicesent') {
						newRow += '<td class="center">';
						if (value[Object.keys(value)[y]] == 1) {
							newRow += '<img src="/pic/icons/mark_green.svg" alt="sent" />';
						}
					} else {
						newRow += '<td>';
						newRow += value[Object.keys(value)[y]];
					}
					newRow += '</td>';
				}
				newRow += '<td><a class="ui-btn ui-shadow ui-corner-all ui-btn-icon-notext ui-icon-delete ui-btn-inline" onclick="deleteGuest('
						+ value.guest_id
						+ '); event.stopPropagation();"><img src="/pic/icons/delete.svg" alt="delete" title="delete"/></a>'
						+ '<a class="iconbutton iconedit" onclick="editGuest('
						+ value.guest_id
						+ '); event.stopPropagation();" title="edit"><img src="/pic/icons/edit.svg" alt="edit" title="edit" /></a>'
						+ '<a class="ui-btn ui-shadow ui-corner-all ui-btn-icon-notext ui-icon-shop ui-btn-inline" onclick="printTicket('
						+ value.guest_id
						+ '); event.stopPropagation();" title="print ticket"><img src="/pic/icons/print.svg" alt="print ticket" title="print"/></a></td>';
				newRow += '</tr>';
				newTable += newRow;

			}

			$('#guesttable tbody').html(newTable);
		} else {
			newTable = '<tr><td class="emptyguestlist" colspan="'
					+ (guestCols.length + 2)
					+ '">Keine Gäste vorhanden. Fügen Sie jederzeit Gäste mit dem <a class="" onclick="editGuest(0);" title="Add guest"><img src="/pic/icons/add.svg" alt="Add guest" title="Add guest" /></a> Button hinzu.</td></tr>';
			$('#guesttable tbody').html(newTable);
		}
		resizeGuestTable(true);
	}
}

function printTicket(id) {
	if ($('#eventid').html() == 4) {
		var url = '/index.php?type=ajax&lg=' + lg + '&0=printpdf&1='
				+ $('#eventid').html() + '&2=ticket&3=' + id;
		// var url = '/print/'+$('#eventid').html()+'/ticket/'+id;
		$.ajax({
			type : "GET",
			url : url,
			success : function(data) {
				if (data.err.length > 0) {
					var errtxt = '';
					for (i = 0; i < data.err.length; i++) {
						errtxt += data.err[i].note + " ";
					}
					alert(errtxt);
				} else {
					alert("Druckauftrag gestartet.");
				}
			},
			dataType : 'json'
		});
	} else {
		window.open('/print/' + $('#eventid').html() + '/ticket/' + id);
	}
}
function printTicketPDF(id) {
	window.open('/printpdf/' + $('#eventid').html() + '/ticket/' + id);
}
function loadEmail(eventid, id) {
	$.ajax({
		url : '/api/events/' + eventid + '/emails/' + id,
		type : 'GET',
		success : function(response) {
			$('#massmailsendform').find('input[name=massmailsubject]').val(
					response.email.subject);
			$('#massmailsendform').find('textarea[name=massmailcontent]')
					.jqteVal(response.email.mailtext);
			$('#massmailsendform').find('input[name=email_id]').val(
					response.email.email_id);
			$('a[href=#massmailtab1]').click();
		}
	});
}

$(document).ready(function() {
	$('.htmlinput').jqte();
	lg = $('html').attr('lang');
});

$(document)
		.on(
				'click',
				'#guesttable > tbody > tr.nrow',
				function() {
					$(".opened").remove();
					$(".open").removeClass("open");
					if ($('#guesttable thead').find(':hidden').length > 0) {
						firstHiddenElement = $('#guesttable thead').find(
								':hidden').index() + 1;
						var rowno = $(this).index() + 1;
						$(this).addClass("open");
						var droppedcontent = '<table cellspacing="0" width="100%" id="tdropped'
								+ rowno + '"><tbody>';
						for (i = firstHiddenElement; i < $('#guesttable thead tr th').length + 1; i++) {
							droppedcontent += '<tr><th>'
									+ $(
											'#guesttable thead tr :nth-child('
													+ i + ')').html()
									+ '</th><td>'
									+ $(
											'#guesttable tbody :nth-child('
													+ rowno + ') :nth-child('
													+ i + ')').html()
									+ '</td></tr>';
						}
						droppedcontent += '</tbody></table>';
						$(this).after(
								'<tr class="opened"><td colspan="'
										+ firstHiddenElement + '">'
										+ droppedcontent + '</td></tr>');
					}
				});
$(document).on(
		'click',
		'#guesttable > tbody > tr.nrow > td > input.cb',
		function() {
			var selectedGuestId = $(this).parent().parent().attr(
					'data-guest_id');
			if ($(this).is(':checked') == true) {
				if (jQuery.inArray(selectedGuestId, selectedGuests) == -1) {
					selectedGuests.push(selectedGuestId);
				}
			} else {
				if (jQuery.inArray(selectedGuestId, selectedGuests) > -1) {
					var indexOfToDelete = selectedGuests
							.indexOf(selectedGuestId);
					if (indexOfToDelete > -1) {
						selectedGuests.splice(indexOfToDelete, 1);
					}
				}
			}
		});
$(document).on(
		'click',
		'#guesttable > thead > tr > th',
		function() {
			guestTableSort = $(this).attr("data-cname") + '|';
			if ($(this).attr("data-cname") == guestTableSortLastCol
					&& guestTableSortLastSortDirection == 'A') {
				guestTableSort += 'D';
				guestTableSortLastSortDirection = 'D';
			} else {
				guestTableSort += 'A';
				guestTableSortLastSortDirection = 'A';
			}
			guestTableSortLastCol = $(this).attr("data-cname");
			reloadGuestTable();
		});
$(document).on('keyup', '#gsearch', function() {
	guestTableSE = $(this).val();
	reloadGuestTable();
});

$(document).on("click", "img[data-iconaction='true']", function() {
	handleIconClick(this);
});

$(window).resize(function() {
	resizeGuestTable(false);
});

function handleIconClick(obj) {
	var id = $(obj).data("id");
	var entity = $(obj).data("entity");
	var action = $(obj).data("action");
	if (action == "delete") {
		if (confirm('Wirklich löschen?')) {
			var url = '/api/events/' + fevent_id + '/' + entity + '/' + id;
			$.ajax({
				type : "DELETE",
				url : url,
				// data : data,
				success : function(data) {
					if (data.err && data.err.length > 0) {
						var errtxt = '';
						for (i = 0; i < data.err.length; i++) {
							errtxt += data.err[i].note + " ";
						}
						alert(errtxt);
					} else {
						if (entity == "regprocsteps") {
							reloadRegProcStepTable();
						}
					}
				},
				dataType : 'json'
			});
		}
	} else if (action == "editpopup") {
		var url = '/api/events/' + fevent_id + '/' + entity + '/' + id + '/';
		$.ajax({
			type : "GET",
			url : url,
			data : {
				'action' : 'editpopup'
			},
			success : function(data) {
				if (data.err && data.err.length > 0) {
					var errtxt = '';
					for (i = 0; i < data.err.length; i++) {
						errtxt += data.err[i].note + " ";
					}
					alert(errtxt);
				} else {
					if (entity == "regprocsteps") {
						// reloadRegProcStepTable();
						openLpopup(':-)', data.html);
						// alert(data.html);
					}
				}
			},
			dataType : 'json'
		});
	}
}

function openLpopup(headline, content) {
	var $newdiv1 = $('<div id="lpopup"><div id="lxpopup"><div class="closeLpopup"><a href="JavaScript:closeLpopup();">X</a></div><h2></h2><div class="content"></div></div></div>');
	$("body").prepend($newdiv1);
	$('#lxpopup .content').html(content);
}

function closeLpopup() {
	$('#lpopup').remove();
}

function resizeGuestTable(startFromScratch) {
	if (startFromScratch == true || $('#pane1').width() == 0) {
		lastWindowSize = 2000;
	}
	var securityCounter = 0;
	if ($('#guesttable')) {
		if ($(window).width() > lastWindowSize) {
			n = 0;
			while ($('#guesttable').width() <= $('#pane1').width()
					&& n < $('#guesttable thead th').length
					&& securityCounter < 60) {
				nextHiddenElement = $('#guesttable thead').find(':hidden')
						.index() + 1;
				$('#guesttable thead').find(
						':nth-child(' + nextHiddenElement + ')').show();
				$('#guesttable tbody').find(
						':nth-child(' + nextHiddenElement + ')').show();
				n += 1;
				securityCounter += 1;
			}
			if ($('#guesttable').width() > $('#pane1').width()) {
				$('#guesttable thead th:nth-child(' + nextHiddenElement + ')')
						.hide();
				$('#guesttable tbody td:nth-child(' + nextHiddenElement + ')')
						.hide();
			}

		} else if ($(window).width() < lastWindowSize
				&& $('#pane1').width() > 0) {
			var n = $('#guesttable thead th').length;
			while ($('#guesttable').width() > $('#pane1').width()
					&& securityCounter < 60) {
				$('#guesttable thead th:nth-child(' + n + ')').hide();
				$('#guesttable tbody td:nth-child(' + n + ')').hide();
				n -= 1;
				securityCounter += 1;
			}
		} else if ($('#pane1').width() == 0) {
		}
	}
	lastWindowSize = $(window).width();
}

function changeguesttableshowcount() {
	var selectedItem = $('#guesttableshowcount').val();
	guestTableShowCount = selectedItem;
	reloadGuestTable();
}

function showGuesttableNextPage() {
	guestTableShowStart += guestTableShowCount;
	reloadGuestTable();
}
function showGuesttablePreviousPage() {
	guestTableShowStart -= guestTableShowCount;
	if (guestTableShowStart < 0) {
		guestTableShowStart = 0;
	}
	reloadGuestTable();
}

function printAllTickets() {
	var id = $('#eventid').html();
	// remove all error messages
	$('.errtext').remove();
	var urlEvent = "/api/events/" + id + "/guests/?format=ticketsstore";
	var dataType = "json";
	startLoading();
	$.ajax({
		type : "GET",
		url : urlEvent,
		dataType : dataType
	}).done(function(data) {
		printAllTicketsF(data);
	}).always(function(data) {
		endLoading();
	});
	return false;
}
function printSelectedTickets() {
	var id = $('#eventid').html();
	// remove all error messages
	$('.errtext').remove();
	var urlEvent = "/api/events/" + id
			+ "/guests/?format=ticketsstore&records="
			+ encodeURIComponent(serializeSimpleArray(selectedGuests));
	var dataType = "json";
	startLoading();
	$.ajax({
		type : "GET",
		url : urlEvent,
		dataType : dataType
	}).done(function(data) {
		printAllTicketsF(data);
	}).always(function(data) {
		endLoading();
	});
	return false;
}
function printSingleTicket(ticketid) {
	var id = $('#eventid').html();
	// remove all error messages
	$('.errtext').remove();
	var urlEvent = "/api/events/" + id
			+ "/guests/?format=ticketsstore&records=" + ticketid;
	var dataType = "json";
	startLoading();
	$.ajax({
		type : "GET",
		url : urlEvent,
		dataType : dataType
	}).done(function(data) {
		printAllTicketsF(data);
	}).always(function(data) {
		endLoading();
	});
	return false;
}

function printAllTicketsF(data) {
	var ticketsurl = data.tempurl;
	var url = data.html2pdfurl;
	var pdfmargin = data.pdfmargin;
	if (url && data) {
		var inputs = '<input type="hidden" name="url" value="'
				+ decodeURI(ticketsurl)
				+ '" /><input type="hidden" name="filename" value="tickets.pdf" /><input type="hidden" name="margin" value="'
				+ pdfmargin + '" />';
		var entform = '<form data-ajax="false" class="nojssubmit specialForm realsubmit" target="_blank" action="'
				+ url + '" method="get">' + inputs + '</form>';
		jQuery(entform).appendTo('body').submit().remove();
	}
	;
};

function serializeSimpleArray(arr) {
	var myStr = '';
	$(arr).each(function(index) {
		myStr += arr[index] + ";";
	});
	return myStr;
}

function refreshEventStats() {
	var url = '/api/events/' + $('#eventid').html();
	$.ajax({
		type : "GET",
		url : url,
		success : function(data) {
			if (data.event) {
				$('.valEvinvited').html(data.event.invited);
				$('.valEvregistered').html(data.event.registered);
				$('.valEvcancelled').html(data.event.cancelled);
			} else {
				alert("Fehler beim Laden");
			}
		},
		dataType : 'json'
	});
	refreshEvents();
}

function reloadRegProcStepTable() {
	var url = '/api/events/' + $('#eventid').html() + '/regprocsteps';
	$
			.ajax({
				type : "GET",
				url : url,
				success : function(data) {
					if (data.err && data.err.length > 0) {
						var errtxt = '';
						for (i = 0; i < data.err.length; i++) {
							errtxt += data.err[i].note + " ";
						}
						alert(errtxt);
					} else {
						$('.valBookingProcessSteps tr').remove();
						html = '';
						for (var i = 0; i < data.steps.length; i++) {
							html += '<tr><td>' + data.steps[i].sequence
									+ '</td><td>';
							html += data.steps[i]['title'] + ' ('
									+ data.steps[i]['fname'] + ')</td><td>';
							html += data.steps[i]['description'] + '</td><td>'
									+ data.steps[i]['stepdetails']
									+ '</td><td>' + data.steps[i].active
									+ '</td><td>';
							html += '<img src="/pic/del.png" data-iconaction="true" data-entity="regprocsteps" data-action="delete" class="iconaction" data-id="'
									+ data.steps[i]['regprocstep_id'] + '" />';
							html += '<img src="/pic/edit.png" data-iconaction="true" data-entity="regprocsteps" data-action="editpopup" class="iconaction" data-id="'
									+ data.steps[i]['regprocstep_id'] + '" />';
							html += '</td></tr>';
						}
						$('.valBookingProcessSteps').html(html);
					}
				},
				dataType : 'json'
			});

	// $("#guestformfieldlisttables").trigger("create");
	// $("#guestformfieldlisttables").trigger("updatelayout");
	// $("#pageEvent").css("minHeight",
	// $("#guestformfieldlisttables").innerHeight()+"px");
	updatePageHeight();
}

function loading(action) {
	switch (action) {
	case "show":
		break;
	case "hide":
		break;
	}
}

/**
 * Navigation
 */
window.onload = function() {
	surfSupport();
}
window.onhashchange = function() {
	surfSupport();
}
function surfSupport() {
	var fulldestination = document.location.hash;
	var destination = fulldestination;

	var newEntity = null;
	var newEntityId = null;
	var newSubEntity = null;
	var newSubEntityId = null;
	var newSubSubEntity = null;
	var switchEntity = false;
	var switchEntityId = false;
	var switchSubEntity = false;
	var switchSubEntityId = false;
	var switchSubSubEntity = false;

	if (fulldestination == '') {
		destination = '#overview';
	} else {
		// count slashes
		var firstSlashPos = -1;
		var secondSlashPos = -1;
		var thirdSlashPos = -1;
		var fourthSlashPos = -1;

		firstSlashPos = fulldestination.indexOf("/");
		if (firstSlashPos > -1) {
			secondSlashPos = fulldestination.substring(firstSlashPos + 1)
					.indexOf("/");
			if (secondSlashPos > -1) {
				secondSlashPos += firstSlashPos + 1;
				thirdSlashPos = fulldestination.substring(secondSlashPos + 1)
						.indexOf("/");
				if (thirdSlashPos > -1) {
					thirdSlashPos += secondSlashPos + 1;
					fourthSlashPos = fulldestination.substring(
							thirdSlashPos + 1).indexOf("/");
					if (fourthSlashPos > -1) {
						fourthSlashPos += thirdSlashPos + 1;
					}
				}
			}
			// destination = destination.substring(0, occurs);

			// first level entity - before the first slash
			newEntity = destination.substring(1, firstSlashPos);

			// var id = fulldestination.substring(firstSlashPos + 1);
			// var endofeventid =
			// fulldestination.substring(beginOfEventid+1).indexOf("/");

			// if there is no second slash, the entity id is everything
			// after the first slash.
			if (secondSlashPos == -1) {
				newEntityId = fulldestination.substring(firstSlashPos + 1);
			} else {
				// otherwise, it is everything between the first
				// and the second slash.
				newEntityId = fulldestination.substring(firstSlashPos + 1,
						secondSlashPos);

			}

			// if there is no third slash, the sub-entity is everything
			// after the second slash.
			if (firstSlashPos != -1 && secondSlashPos != -1) {
				if (thirdSlashPos == -1) {
					newSubEntity = fulldestination
							.substring(secondSlashPos + 1);
				} else {
					// otherwise, it is everything between the second and the
					// third slash.
					newSubEntity = fulldestination.substring(
							secondSlashPos + 1, thirdSlashPos);
				}
			}

			// if there is no third slash, the sub-entity is everything
			// after the second slash.
			if (firstSlashPos != -1 && secondSlashPos != -1
					&& thirdSlashPos != -1) {
				if (fourthSlashPos == -1) {
					newSubEntityId = fulldestination
							.substring(thirdSlashPos + 1);
				} else {
					// otherwise, it is everything between the second and the
					// third slash.
					newSubEntityId = fulldestination.substring(
							thirdSlashPos + 1, fourthSlashPos);
				}
			}
			// goBooking($('#' + id), eventid);
		} else {
			// no slash, so we are on first entity root level
			newEntity = destination.substring(1);
			// $(destination + " .overview").show();
			// $(destination + " .single").hide();
		}
	}
	if (newEntity != currentEntity) {
		$('.page').hide();
		switchEntity = true;
	}
	if (newEntityId != currentEntityId) {
		switchEntityId = true;
	}
	if (newSubEntity != currentSubEntity) {
		switchSubEntity = true;
	}
	if (newSubEntityId != currentSubEntityId) {
		switchSubEntityId = true;
	}
	switch (newEntity) {
	case 'crm':
		if (switchEntity == true) {
			reloadCRMTable();
			$('#pageCRM').show();
			// goToEvent(newEntityId, newSubEntity);
			currentEntity = newEntity;
			currentEntityId = newEntityId;
			currentSubEntity = newSubEntity;
		} else if (switchEntityId == true) {
			goToCRM(newEntityId, newSubEntity);
			currentEntityId = newEntityId;
			/*
			 * } else if (switchSubEntity == true) {
			 * goToEventSubpart(newSubEntity); currentSubEntity = newSubEntity; }
			 * else if (switchSubEntityId == true) {
			 * goToEventSubpartId(newSubEntityId);
			 */
		}
		break;
	case 'emailtemplate':
		if (switchEntity == true) {
			$('#pageEmailtemplate').show();
			goToEmailtemplate(newEntityId);
			currentEntity = newEntity;
			currentEntityId = newEntityId;
			currentSubEntity = newSubEntity;
		} else if (switchEntityId == true) {
			goToEmailtemplate(newEntityId);
			currentEntityId = newEntityId;
		} else if (switchSubEntity == true) {
			// goToEventSubpart(newSubEntity);
			// currentSubEntity = newSubEntity;
		} else if (switchSubEntityId == true) {
			// goToEventSubpartId(newSubEntityId);
		}
		break;
	case 'fevent':
		if (switchEntity == true) {
			$('#pageEvent').show();
			goToEvent(newEntityId, newSubEntity);
			currentEntity = newEntity;
			currentEntityId = newEntityId;
			currentSubEntity = newSubEntity;
		} else if (switchEntityId == true) {
			goToEvent(newEntityId, newSubEntity);
			currentEntityId = newEntityId;
		} else if (switchSubEntity == true) {
			goToEventSubpart(newSubEntity);
			currentSubEntity = newSubEntity;
		} else if (switchSubEntityId == true) {
			goToEventSubpartId(newSubEntityId);
		}
		break;
	case 'filemanager':
		if (switchEntity == true) {
			// reloadCRMTable();
			$('#pageFilemanager').show();
			// goToEvent(newEntityId, newSubEntity);
			currentEntity = newEntity;
			currentEntityId = newEntityId;
			currentSubEntity = newSubEntity;
		} else if (switchEntityId == true) {
			goToFilemanager(newEntityId, newSubEntity);
			currentEntityId = newEntityId;
			/*
			 * } else if (switchSubEntity == true) {
			 * goToEventSubpart(newSubEntity); currentSubEntity = newSubEntity; }
			 * else if (switchSubEntityId == true) {
			 * goToEventSubpartId(newSubEntityId);
			 */
		}
		break;
	case null:
		$('#pageHome').show();
		currentEntity = null;
		break;
	}
}

/*
 * Toggle visibility of the account menu
 */
$(document).on('click', '.topmenu ul li', function() {
	$(this).find('ul').toggle();
});

/**
 * Toggle active state of mmenu
 */
$(document).on('click', 'nav.mmenu ul li a', function() {
	$(this).parent().parent().find('.active').removeClass('active');
	$(this).parent().addClass('active');
});

/**
 * switch vertical tabs
 */
$(document).on('click', 'nav.vtabs ul li a', function() {
	var tabclass = $(this).parent().parent().parent().data('tabclass');
	var tabno = $(this).parent().index() * 1 + 1;
	$(this).parent().parent().find('li').removeClass('active');
	$('.vtabs' + tabclass + ' div.tab').hide();
	$('.vtabs' + tabclass + ' div.tab:nth-child(' + tabno + ')').show();
	$(this).parent().toggleClass('active');
});

function showHalfDetailPage() {
	$('.subpage').css('width', '50%');
	$('.subpage').css('float', 'left');
	$('.halfsubpage').show();
}

$(document).on('click', '.toggler', function() {
	$(this).parent().find('div.toggled').toggle();
});

function saveemail() {
	var formData = $('#massmailsendform').serializeArray();
	$.ajax({
		url : '/api/emails',
		type : 'POST',
		data : formData,
		success : function(response) {
			handleFormResponse(response);
		}
	});
}
$('#massmailsendingtype_1').change(function() {
	updateMassMailSendingForm(1);
});
$('#massmailsendingtype_3').change(function() {
	updateMassMailSendingForm(3);
});
$('#massmailsendingtype_4').change(function() {
	updateMassMailSendingForm(4);
});
function updateMassMailSendingForm(id) {
	$('#massmailsendingtypeoptions1').hide();
	$('#massmailsendingtypeoptions3').hide();
	if (id == 1) {
		$('#massmailsendingtypeoptions1').show();
	}
	if (id == 3) {
		$('#massmailsendingtypeoptions3').show();
	}
}
function reloadPage() {
	document.location.reload();
}
$(document).on(
		'click',
		'.crmtable .deleteIcon',
		function() {
			confirm("Wirklich " + $(this).parent().parent().data('crm_id')
					+ ' löschen?');
		});
$(document).on('click', '.crmtable .editIcon', function() {
	var crm_id = $(this).parent().parent().data('crm_id');
	editCRMPerson(crm_id);
});

$(document).on('submit', 'form.formMoveCRMToEvent', function() {
	var additionalData = {};
	$(this).find('input[name="crm_ids"]').remove();
	var htm = '<input name="crm_ids" value="';
	var idshtm = '';
	$('input.crmchck:checked').each(function(key, val) {
		idshtm += $(this).parent().parent().data('crm_id') + ',';
	});
	idshtm = idshtm.substr(0, idshtm.length - 1);
	htm = htm + idshtm + '" type="hidden" />';
	$(this).append(htm);// = additionalData;
	handleFormSubmit(this);
	return false;
});
function printInvoiceTrigger(guestid) {
	var eventid = $('#eventid').html();
	// remove all error messages
	$('.errtext').remove();
	var urlEvent = "/api/events/" + eventid + "/guests/" + guestid
			+ "/sendPDFInvoice";
	var dataType = "json";
	// startLoading();
	$.ajax({
		type : "POST",
		url : urlEvent,
		dataType : dataType
	}).done(function(data) {
		printInvoice(data);
	}).always(function(data) {
		// endLoading();
	});
	return false;
}

function printInvoice(data) {
	var ticketsurl = data.tempurl;
	var url = data.html2pdfurl;
	var pdfmargin = data.pdfmargin;
	if (url && data) {
		var inputs = '<input type="hidden" name="url" value="'
				+ decodeURI(ticketsurl)
				+ '" /><input type="hidden" name="filename" value="tickets.pdf" /><input type="hidden" name="margin" value="'
				+ pdfmargin
				+ '" /><input type="hidden" name="format" value="A4" />';
		var entform = '<form data-ajax="false" class="nojssubmit specialForm realsubmit" target="_blank" action="'
				+ url + '" method="get">' + inputs + '</form>';
		// alert(entform);
		jQuery(entform).appendTo('body').submit().remove();
	}
	;
};

function generateInvoiceTrigger(guestid) {
	var eventid = $('#eventid').html();
	var that = $('.invoiceform');
	var callbck = null;
	// remove all error messages
	$('.errtext').remove();
	var urlEvent = "/api/events/" + eventid + "/guests/" + guestid
			+ "/generatePDFInvoice";
	var dataType = "json";
	// startLoading();
	$.ajax({
		type : "POST",
		url : urlEvent,
		dataType : dataType
	}).done(function(data) {
		handleFormResponse(data, that, callbck);
		// printInvoice(data);
		/*
		 * if (data.err && data.err.length > 0) { displayJsonErrors(data); }
		 * else { alert("Rechnung generiert."); }
		 */
	}).always(function(data) {
		// endLoading();
	});
	return false;
}

function displayJsonErrors(data) {
	if (data.err && data.err.length > 0) {
		alert("Fehler");
	}
}

function displayJsonErrorsInHtmlElement(data, destinationElement) {
	if (data.err && data.err.length > 0) {
		// show the list of errors above the form ...
		var errtxt = '<ul>';
		for (i = 0; i < data.err.length; i++) {
			var e = data.err[i];
			errtxt += '<li>' + e.message + '</li>';
		}
		errtxt += '</ul>';
		$(destinationElement).find('div').html(errtxt);
		$(destinationElement).finish().show().fadeIn(1).fadeOut(10000);
		
		// highlight the fields that we have discovered problems for
		for (i = 0; i < data.err.length; i++) {
			if ($(destinationElement).parent().find('input[name="' + data.err[i].field + '"]').size() > 0) {
				$(destinationElement).parent().find('input[name="' + data.err[i].field + '"]')
						.addClass('errorfield');
			}
		}
	}
}

function updateGuestInvoiceListTrigger() {
	$('.firstInvoiceCreation').hide();
	$('.guestinvoiceslist').each(
			function() {
				var url = "/api/events/" + fevent_id + "/guests/"
						+ $(this).data('guest_id') + "/invoices";
				var dataType = "json";
				var that = $(this);
				startLoading();
				$.ajax({
					type : "GET",
					url : url,
					dataType : dataType
				}).done(function(data) {
					fillGuestInvoiceListTable(that, data);
				}).always(function(data) {
					endLoading();
				});
			});
	$('.guestinvoiceselectbox').each(
			function() {
				var url = "/api/events/"
						+ fevent_id
						+ "/guests/"
						+ $(this).parentsUntil('.htabs').parent().data(
								'guest_id') + "/invoices";
				var dataType = "json";
				var that = $(this);
				startLoading();
				$.ajax({
					type : "GET",
					url : url,
					dataType : dataType
				}).done(function(data) {
					fillGuestInvoiceListSelectbox(that, data);
				}).always(function(data) {
					endLoading();
				});
			});
	$('.guestpaymentlist').each(
			function() {
				var url = "/api/events/" + fevent_id + "/guests/"
						+ $(this).data('guest_id') + "/payments";
				var dataType = "json";
				var that = $(this);
				startLoading();
				$.ajax({
					type : "GET",
					url : url,
					dataType : dataType
				}).done(function(data) {
					fillGuestPaymentListTable(that, data);
				}).always(function(data) {
					endLoading();
				});
			});
}

function fillGuestInvoiceListTable(t, data) {
	var html = '';
	if (data.records.length == 0) {
		$('.firstInvoiceCreation').show();
	} else {
		$('.firstInvoiceCreation').hide();
	}
	$
			.each(
					data.records,
					function(key, value) {
						html += '<tr data-actionid="' + value.id + '"';
						if (value.flg01 == 1) {
							html += ' class="invoicecancelled" ';
						}
						html += '><td>' + value.ts
								+ '</td><td><a href="/api/downloads/1/event/'
								+ fevent_id + '/invoices/' + value.suppdata
								+ '">' + value.suppdata + '</a></td>'
								+ '<td class="price">' + value.fdec6
								+ ' &euro;</td>' + '<td class="price">'
								+ value.fdec8 + ' &euro;</td>'
								+ '<td class="center">';
						if (value.paymentstatus == 16) {
							html += '<img src="/pic/icons/mark_green.svg" alt="paid" />';
						} else if (value.paymentstatus == null) {
							html += '<img src="/pic/icons/exclamation_red.svg" alt="unpaid" />';
						} else {
							html += value.paymentstatus + " ("
									+ value.paymentcomment + ")";
						}
						html += '</td><td>';
						html += '<img src="/pic/icons/email.svg" alt="Re-Send" class="buttonResendInvoice" />';
						html += '<img src="/pic/icons/remoteprint.svg" alt="print remotely" class="buttonRemoteprintInvoice" alt="{"Resend invoice via email"|gettext}" />';
						if (value.flg01 == 0) {
							html += '<img src="/pic/icons/delete.svg" alt="cancel" class="buttonCancelInvoice" alt="{"Cancel invoice"|gettext}" />';
						} else {
							html += '<img src="/pic/icons/delete.svg" alt="cancel" class="buttonUncancelInvoice" alt="{"Uncancel invoice"|gettext}" />';
						}
						html += '</td></tr>';
					});
	$(t).find('tbody').html(html);
}

function fillGuestPaymentListTable(t, data) {
	var html = '';
	$.each(data.records, function(key, value) {
		html += '<tr><td>' + value.ts + '</td>' + '<td>' + value.invoicename
				+ '</td>' + '<td>' + value.suppdata + '</td>' + '<td>'
				+ value.pstatus + '</td>';
		html += '</tr>';
	});
	$(t).find('tbody').html(html);
}
function fillGuestInvoiceListSelectbox(t, data) {
	var html = '';
	$.each(data.records, function(key, value) {
		html += '<option value="' + value.id + '">' + value.suppdata
				+ '</option>';
	});
	$(t).html(html);
}

function fillMultisettingsTables(evdata) {
	$('table.multisetting')
			.each(
					function() {
						var settingsname = $(this).data('multisetting');
						var html = '';
						var columns = null;
						var newdata = [];
						$
								.each(
										evdata.event,
										function(key, value) {
											if (key
													.substring(
															0,
															settingsname.length + 1 + 7) == settingsname
													+ "_columns") {
												columns = JSON.parse(value);
											} else if (key.substring(0,
													settingsname.length + 1) == settingsname
													+ "_") {
												var myKeyParts = key.split('_');
												if (newdata[myKeyParts[1]] == undefined) {
													newdata[myKeyParts[1]] = [];
												}
												newdata[myKeyParts[1]][myKeyParts[2]] = value;
											}
										});
						for (ix = 0; ix < Object.keys(newdata).length; ix++) {
							var row = newdata[Object.keys(newdata)[ix]];
							if (row != undefined) {
								html += '<tr data-seqid="'
										+ Object.keys(newdata)[ix]
										+ '" data-rowid="' + row.id + '">';//
								$(this)
										.find('thead tr th')
										.each(
												function() {
													if ($(this).data('col') == 'seqid') {
														html += '<td>'
																+ Object
																		.keys(newdata)[ix]
																+ '</td>';
													} else if ($(this).data(
															'col')) {
														if (row[$(this).data(
																'col')] == null) {
															html += '<td></td>';
														} else {
															html += '<td>'
																	+ row[$(
																			this)
																			.data(
																					'col')]
																	+ '</td>';
														}
													}
												});
								html += '<td>';
								html += '<img src="/pic/icons/edit.svg" alt="edit" class="multisettingEdit" />';
								html += '<img src="/pic/icons/delete.svg" alt="edit" class="multisettingDelete" />&nbsp;&nbsp;';
								html += '<img src="/pic/icons/arrowdown.svg" alt="edit" class="multisettingMove downwards" />';
								html += '<img src="/pic/icons/arrowup.svg" alt="edit" class="multisettingMove upwards" />';
								html += '</td>';
								html += '</tr>';
							} else {
								alert("newdata " + ix + " undefined");
							}
						}
						$(this).find('tbody').html(html);
					});
}
function showMultisettingsPopup(entityid, seqid) {
	showPopup('Multisetting');
	$('#popupPanelMultisetting').html(
			'One moment please, loading multisetting <em>' + entityid
					+ '</em>, nr ' + seqid + '...');

	var url = '/api/events/' + fevent_id
			+ '?form=editMultisettingsForm&multisetting=' + entityid
			+ '&seqid=' + seqid;
	$.ajax({
		type : "GET",
		url : url,
		success : function(data) {
			if (data.err != undefined && data.err.length > 0) {
				var errtxt = '';
				for (i = 0; i < data.err.length; i++) {
					errtxt += data.err[i].message + " ";
				}
				$('#popupPanelMultisetting').html(
						'<div class="error">' + errtxt + '</div>');
			} else {
				// reloadGuestTable();
				$('#popupPanelMultisetting').html(data.content);
			}
		},
		dataType : 'json'
	});

}
$(document).on(
		'click',
		'.multisettingEdit',
		function() {
			var entityid = $(this).parent().parent().parent().parent().data(
					'multisetting');
			var seqid = $(this).parent().parent().data('seqid');
			showMultisettingsPopup(entityid, seqid);

		});
$(document)
		.on(
				'click',
				'.multisettingDelete',
				function() {
					if (confirm("Wirklich löschen?")) {
						var entityid = $(this).parent().parent().parent()
								.parent().data('multisetting');
						var seqid = $(this).parent().parent().data('seqid');
						var rowid = $(this).parent().parent().data('rowid');

						var url = '/api/events/' + fevent_id + '/multisettings';// ?form=deleteMultisettings&multisetting='+entityid+'&seqid='+seqid+'&rowid='+rowid;
						var data = 'fevent_id='
								+ $('#eventid').html()
								+ '&multisetting='
								+ entityid
								+ '&seqid='
								+ seqid
								+ '&rowid='
								+ rowid
								+ '&form=deleteMultisettings&cb=reloadMultisettingsTables';// &class=event&type=ajax';
						$.ajax({
							type : "POST",
							url : url,
							data : data,
							success : function(data) {
								if (data.err != undefined
										&& data.err.length > 0) {
									var errtxt = '';
									for (i = 0; i < data.err.length; i++) {
										errtxt += data.err[i].message + " ";
									}
								} else {
									reloadMultisettingsTables();
								}
							},
							dataType : 'json'
						});
					}
				});
$(document).on(
		'click',
		'.multisettingSubDelete',
		function() {
			if (confirm("Wirklich löschen?")) {

				var pmultiname = $(this).parentsUntil('table').parent().data(
						'multisetting');
				var multiname = $(this).parentsUntil('table').parent().data(
						'n1entity');
				var pmultiid = $(this).parentsUntil('table').parent().data(
						'parentid');
				var pmultiseq = $(this).parentsUntil('table').parent().data(
						'parentseq');
				var multiid = $(this).parentsUntil('tr').parent().data(
						'multiid');

				var url = '/api/events/' + fevent_id + '/multisettings';
				var data = 'fevent_id=' + $('#eventid').html() + '&multiname='
						+ multiname + '&pmultiname=' + pmultiname + '&multiid='
						+ multiid + '&pmultiid=' + pmultiid
						+ '&form=removemultisetting1n';
				$.ajax({
					type : "POST",
					url : url,
					data : data,
					success : function(data) {
						if (data.err != undefined && data.err.length > 0) {
							var errtxt = '';
							for (i = 0; i < data.err.length; i++) {
								errtxt += data.err[i].message + " ";
							}
						} else {
							// reloadMultisettingsTables();
							showMultisettingsPopup(pmultiname, pmultiseq);
						}
					},
					dataType : 'json'
				});
			}
		});
$(document).on(
		'click',
		'.multisettingMove',
		function() {
			var entityid = $(this).parent().parent().parent().parent().data(
					'multisetting');
			var seqid = $(this).parent().parent().data('seqid');
			var rowid = $(this).parent().parent().data('rowid');
			var direction = 'down';
			if ($(this).hasClass('upwards')) {
				direction = 'up';
			}
			var url = '/api/events/' + fevent_id + '/multisettings';
			var data = 'fevent_id=' + $('#eventid').html() + '&multisetting='
					+ entityid + '&seqid=' + seqid + '&rowid=' + rowid
					+ '&direction=' + direction
					+ '&form=moveMultisettings&cb=reloadMultisettingsTables';
			$.ajax({
				type : "POST",
				url : url,
				data : data,
				success : function(data) {
					if (data.err != undefined && data.err.length > 0) {
						var errtxt = '';
						for (i = 0; i < data.err.length; i++) {
							errtxt += data.err[i].message + " ";
						}
					} else {
						reloadMultisettingsTables();
					}
				},
				dataType : 'json'
			});

		});

function reloadMultisettingsTables() {
	var urlEvent = "/api/events/" + fevent_id;
	var dataType = "json";
	startLoading();
	$.ajax({
		type : "GET",
		url : urlEvent,
		dataType : dataType
	}).done(function(data) {
		fillMultisettingsTables(data);
	}).always(function(data) {
		endLoading();
	});
}

function multisettingNew(entityid) {
	showPopup('Multisetting');
	$('#popupPanelMultisetting').html(
			'One moment please, loading multisetting <em>' + entityid
					+ '</em>, new ...');

	var url = '/api/events/' + fevent_id
			+ '?form=editMultisettingsForm&multisetting=' + entityid
			+ '&seqid=0';
	$.ajax({
		type : "GET",
		url : url,
		success : function(data) {
			if (data.err != undefined && data.err.length > 0) {
				var errtxt = '';
				for (i = 0; i < data.err.length; i++) {
					errtxt += data.err[i].message + " ";
				}
				$('#popupPanelMultisetting').html(
						'<div class="error">' + errtxt + '</div>');
			} else {
				$('#popupPanelMultisetting').html(data.content);
			}
		},
		dataType : 'json'
	});

};

$(document).on('click', '.htabs_menu li', function() {
	var nextHtab = $(this).data('htab');
	$(this).parent().find('li').removeClass('active');
	$(this).parent().parent().find('>div').hide();
	$(this).parent().find('[data-htab=' + nextHtab + ']').addClass('active');
	$(this).parent().parent().find('.htab_' + nextHtab).show();
});

$(document)
		.on(
				'click',
				'input[type="submit"]',
				function() {
					// find the parent form ...
					var parentForm = null;
					if ($(this).parent().is('form')) {
						parentForm = $(this).parent();
					} else {
						parentForm = $(this).parentsUntil('form').parent();
					}

					if (parentForm != null) {
						if ($(parentForm).find('[name="formsubmittedby"]').length == 1) {
							// if the field named "formsubmittedby" exists
							// already, modify its value
							$(parentForm).find('[name="formsubmittedby"]').val(
									$(this).attr("name"));
						} else {
							// if the field named "formsubmittedby" does not
							// exist yet, create it.
							var inputfield = '<input type="hidden" name="formsubmittedby" value="'
									+ $(this).attr("name") + '" />';
							$(parentForm).append(inputfield);
						}
					}
				});

function reloadGuestForm() {
	editGuest($('#popupGuest').data('guest_id'));
}

$(document).on('change', 'input.jfileup', prepareUpload);
function prepareUpload(event) {
	uploadfiles = event.target.files;
}

function uploadFiles(event) {
	event.stopPropagation(); // Stop stuff happening
	event.preventDefault(); // Totally stop stuff happening

	// START A LOADING SPINNER HERE

	// Create a formdata object and add the files
	var data = new FormData();
	$.each(uploadfiles, function(key, value) {
		data.append(key, value);
	});

	$.ajax({
		url : '/api/files',
		type : 'POST',
		data : data,
		cache : false,
		dataType : 'json',
		processData : false, // Don't process the files
		contentType : false, // Set content type to false as jQuery will tell
		// the server its a query string request
		success : function(data, textStatus, jqXHR) {
			if (typeof data.error === 'undefined') {
				// Success so call function to process the form
				// submitForm(event, data);
			} else {
				// Handle errors here
				console.log('ERRORS: ' + data.error);
			}
		},
		error : function(jqXHR, textStatus, errorThrown) {
			// Handle errors here
			console.log('ERRORS: ' + textStatus);
			// STOP LOADING SPINNER
		}
	});
}

function fileManagerShowDirectory(dirname, filemanagerobj) {
	var url = '/api/internalfiles';
	var data = {
		folder : dirname

	};
	// var thi = $(this);
	$.ajax({
		type : "GET",
		url : url,
		data : data,
		success : function(data) {
			if (data.err != undefined && data.err.length > 0) {
				var errtxt = '';
				for (i = 0; i < data.err.length; i++) {
					errtxt += data.err[i].message + " ";
				}
			} else {
				fillFilemanager(filemanagerobj, data);
			}
		},
		dataType : 'json'
	});

}
$(document).on(
		'click',
		'.folderlink',
		function() {
			var currentfolder = $(this).parentsUntil('.filemanager').parent()
					.data('folder');
			var subfolder = $(this).parent().data('subfolder');

			var showFolder = currentfolder;
			if (currentfolder != '/') {
				showFolder += '/';
			}
			showFolder += subfolder;
			var fm = $(this).parentsUntil('.filemanager').parent();
			fileManagerShowDirectory(showFolder, fm);
		});

function fillFilemanager(thi, data) {
	$(thi).data('folder', data.content.path);
	$(thi).find('.foldername').html(data.content.path);
	$(thi).find('input[name="foldername"]').val(data.content.path);

	var listtarget = $(thi).find('.filelistbody');
	$(listtarget).html('Loading...');

	var html = '';
	if (data.content.parentDirectoryPermitted !== undefined) {
		if (data.content.parentDirectoryPermitted == '1') {
			html += '<tr data-subfolder=".." class="upwards"><td colspan="5" class="folderlink"><img src="/pic/icons/folder_up.svg" alt="upwards" /></td></tr>';
		}
	}
	if (data.content.directories !== undefined) {
		for (i = 0; i < data.content.directories.length; i++) {
			html += '<tr data-subfolder="' + data.content.directories[i][0]
					+ '">';
			html += '<td class="folderlink"><img src="/pic/icons/folder.svg" alt="" title=""></td>';
			html += '<td class="folderlink">' + data.content.directories[i][0]
					+ '</td>';
			html += '<td> </td>';
			html += '<td>' + formatTimestamp(data.content.directories[i][2])
					+ '</td>';
			html += '<td><img class="folderdelete" src="/pic/icons/delete.svg" alt="Ordner löschen!" title="Ordner löschen!"></td>';
			html += '</tr>';
		}
	}
	if (data.content.files !== undefined) {
		for (i = 0; i < data.content.files.length; i++) {
			html += '<tr data-file="' + data.content.files[i][0] + '">';
			html += '<td class="filelink"><img src="/pic/icons/file.svg" alt="" title=""></td>';
			html += '<td class="filelink"><a href="' + data.content.files[i][3]
					+ '">' + data.content.files[i][0] + '</a></td>';
			html += '<td>' + data.content.files[i][1] + '</td>';
			html += '<td>' + formatTimestamp(data.content.files[i][2])
					+ '</td>';
			html += '<td><img class="filedelete" src="/pic/icons/delete.svg" alt="Ordner löschen!" title="Ordner löschen!"></td>';
			html += '</tr>';
		}
	}
	$(listtarget).html(html);
}
function reloadFileManager(thi) {
	var fm = $(thi).parentsUntil('.filemanagerobject').parent();

	if ($(fm).html() === undefined) {
		fm = $(thi).parent();
	}
	$(fm).find('.fineform').hide();
	$(fm).find('.menuicon').removeClass('active');
	$(fm).find('[name=newdirname]').val('');
	$(fm).find('[name=datei]').val('');

	var foldername = $(fm).find('.foldername').html();

	fileManagerShowDirectory(foldername, fm);
}

$(document).on(
		'click',
		'.folderdelete',
		function() {
			if (confirm('Ordner löschen?')) {
				var fm = $(this);
				var data = new FormData();
				// var data = {delete: 'folder', folder:
				// $(this).parent().parent().data('subfolder')};
				var mainfname = $(this).parentsUntil('[data-folder]').parent()
						.data('folder');

				var fname = mainfname;
				if (mainfname != '/') {
					fname += '/';
				}
				fname += $(this).parent().parent().data('subfolder');
				data.append('delete', 'folder');
				data.append('folder', fname);
				$.ajax({
					url : '/api/internalfiles',
					type : 'POST',
					data : data,
					cache : false,
					dataType : 'json',
					processData : false, // Don't process the files
					contentType : false, // Set content type to false as
					// jQuery
					// will tell
					// the server its a query string request
					success : function(data, textStatus, jqXHR) {
						if (typeof data.error === 'undefined') {
							// Success so call function to process the form
							// submitForm(event, data);
						} else {
							// Handle errors here
							console.log('ERRORS: ' + data.error);
						}
						;
						reloadFileManager(fm);
					},
					error : function(jqXHR, textStatus, errorThrown) {
						// Handle errors here
						console.log('ERRORS: ' + textStatus);
						// STOP LOADING SPINNER
					}
				});
			}
		});

$(document).on(
		'click',
		'.filedelete',
		function() {
			if (confirm('Datei löschen?')) {
				var fm = $(this);
				var data = new FormData();
				// var data = {delete: 'folder', folder:
				// $(this).parent().parent().data('subfolder')};
				var mainfname = $(this).parentsUntil('[data-folder]').parent()
						.data('folder');

				var fname = mainfname;
				if (mainfname != '/') {
					fname += '/';
				}
				fname += $(this).parent().parent().data('file');
				data.append('delete', 'file');
				data.append('file', fname);
				$.ajax({
					url : '/api/internalfiles',
					type : 'POST',
					data : data,
					cache : false,
					dataType : 'json',
					processData : false, // Don't process the files
					contentType : false, // Set content type to false as
					// jQuery
					// will tell
					// the server its a query string request
					success : function(data, textStatus, jqXHR) {
						if (typeof data.error === 'undefined') {
							// Success so call function to process the form
							// submitForm(event, data);
						} else {
							// Handle errors here
							console.log('ERRORS: ' + data.error);
						}
						;
						reloadFileManager(fm);
					},
					error : function(jqXHR, textStatus, errorThrown) {
						// Handle errors here
						console.log('ERRORS: ' + textStatus);
						// STOP LOADING SPINNER
					}
				});
			}
		});

function formatTimestamp(ts) {
	var dt = new Date(ts * 1000);
	var days = dt.getDate();
	var months = dt.getMonth() + 1;
	var years = dt.getFullYear();
	var hours = dt.getHours();
	var minutes = dt.getMinutes();
	var seconds = dt.getSeconds();

	// the above dt.get...() functions return a single digit
	// so I prepend the zero here when needed
	if (days < 10)
		days = '0' + days;
	if (months < 10)
		months = '0' + months;

	if (hours < 10)
		hours = '0' + hours;

	if (minutes < 10)
		minutes = '0' + minutes;

	if (seconds < 10)
		seconds = '0' + seconds;

	return days + "." + months + "." + years + " " + hours + ":" + minutes;
}
$(document).on('click', '.menuicon', function() {
	if ($(this).hasClass('active')) {
		$(this).parent().parent().find('.fineform').hide();
		$(this).parent().parent().find('.menuicon').removeClass('active');

	} else {
		$(this).parent().parent().find('.fineform').hide();
		$(this).parent().parent().find('.menuicon').removeClass('active');

		var toShow = $(this).data('on');

		$(this).parent().parent().find('.' + toShow).show();
		$(this).addClass('active');
	}
});
$(document).on(
		'click',
		// '.internalfileselector:not(".fileManagerOpen")',
		'.selectedfiledisplay',
		function() {
			var thi = $(this).parent().find('.internalfileselector');
			if ($(thi).hasClass('fileManagerOpen')) {
				$(thi).removeClass('fileManagerOpen');
				// $(thi).hide();
				$(thi).html('');
			} else {
				// load and display file manager
				var id = 10;
				var url = "/api/internalfiles/filemanager/" + id;
				var dataType = "json";

				var pdata = 'internalfile_id=10&select=1&selecttofield='
						+ $(this).data('belongsto');

				// startLoading();
				// clearEventData();
				// if (changePage == true) {
				// navigate('#pageEvent');
				// }

				$.ajax({
					type : "POST",
					url : url,
					data : pdata,
					dataType : dataType
				}).done(function(data) {
					fillFileselector(thi, data);
					eventLoaded = true;
				}).always(function(data) {
					endLoading();
				});
			}
		});
function fillFileselector(thi, data) {
	$(thi).html(data.html);
	$(thi).addClass("fileManagerOpen");
	$(thi).show();
}
$(document).on(
		'click',
		'.filelistbody tr',
		function() {
			if (!$(this).data('subfolder')) {
				if ($(this).parentsUntil('.filemanager').parent().data(
						'fdselect') == 1) {

					var filename = $(this).parentsUntil('.filemanager')
							.parent().find('.foldername').html()
							+ '/' + $(this).data('file');
					$(this).parentsUntil('.forminternalfileselect').parent()
							.find('input.selectedfile').val(filename);
					$(this).parentsUntil('.forminternalfileselect').parent()
							.find('.selectedfiledisplay').html(filename);
					$(this).parent().find('tr').removeClass('fileSelected');
					$(this).addClass('fileSelected');
					$(this).parentsUntil('.filemanager').parent().parent()
							.removeClass('fileManagerOpen');
					$(this).parentsUntil('.filemanager').parent().remove();
				}
			}

		});
$(document)
		.on(
				'click',
				'.buttonResendInvoice',
				function() {
					if (!$(this).hasClass('waitingforserver')) {
						$(this).addClass('waitingforserver');
						var that = $(this);
						var guest_id = $(this).parent().parent().parent()
								.parent().data('guest_id');
						var invoicefile = $(this).parent().parent().data(
								'invoicefile');
						var url = '/api/events/' + $('#eventid').html()
								+ '/guests/' + guest_id + '/resendinvoice';// index.php?type=ajax&class=guest';
						var data = 'fevent_id=' + $('#eventid').html()
								+ '&guest_id=' + guest_id
								+ '&form=resendInvoice&invoicefile='
								+ invoicefile;
						$.ajax({
							type : "POST",
							url : url,
							data : data,
							success : function(data) {
								if (data.err) {// }.length > 0) {
									/*
									 * var errtxt = ''; for (i = 0; i <
									 * data.err.length; i++) { errtxt +=
									 * data.err[i].note + " "; } alert(errtxt);
									 */
								} else {

									// reloadGuestTable();
								}
								$(that).removeClass('waitingforserver');
							},
							dataType : 'json'
						});
					}
				});
$(document)
		.on(
				'click',
				'.buttonRemoteprintInvoice',
				function() {
					if (!$(this).hasClass('waitingforserver')) {
						$(this).addClass('waitingforserver');
						var that = $(this);
						var guest_id = $(this).parentsUntil('.htabs').parent()
								.data('guest_id');
						var invoicefile = $(this).parent().parent().data(
								'invoicefile');
						var url = '/api/events/' + $('#eventid').html()
								+ '/guests/' + guest_id + '/remoteprintinvoice';// index.php?type=ajax&class=guest';
						var data = 'fevent_id=' + $('#eventid').html()
								+ '&guest_id=' + guest_id
								+ '&form=remoteprintInvoice&invoicefile='
								+ invoicefile;
						$.ajax({
							type : "POST",
							url : url,
							data : data,
							success : function(data) {
								if (data.err) {// }.length > 0) {
									/*
									 * var errtxt = ''; for (i = 0; i <
									 * data.err.length; i++) { errtxt +=
									 * data.err[i].note + " "; } alert(errtxt);
									 */
								} else {
									// alert("OK");
									// reloadGuestTable();
									updateGuestInvoiceListTrigger();
								}
								$(that).removeClass('waitingforserver');
							},
							dataType : 'json'
						});
					}
				});
$(document).on(
		'click',
		'.buttonCancelInvoice',
		function() {
			if (!$(this).hasClass('waitingforserver')) {
				$(this).addClass('waitingforserver');
				var that = $(this);
				var guest_id = $(this).parentsUntil('.htabs').parent().data(
						'guest_id');
				var invoiceid = $(this).parent().parent().data('actionid');
				var url = '/api/events/' + $('#eventid').html() + '/guests/'
						+ guest_id + '/cancelinvoice';// index.php?type=ajax&class=guest';
				var data = 'fevent_id=' + $('#eventid').html() + '&guest_id='
						+ guest_id + '&form=cancelinvoice&invoiceid='
						+ invoiceid;
				$.ajax({
					type : "POST",
					url : url,
					data : data,
					success : function(data) {
						if (data.err) {// }.length > 0) {
							/*
							 * var errtxt = ''; for (i = 0; i < data.err.length;
							 * i++) { errtxt += data.err[i].note + " "; }
							 * alert(errtxt);
							 */
						} else {
							// alert("OK");
							// reloadGuestTable();
							updateGuestInvoiceListTrigger();
							reloadGuestTable();
						}
						$(that).removeClass('waitingforserver');
					},
					dataType : 'json'
				});
			}
		});
$(document).on(
		'click',
		'.buttonUncancelInvoice',
		function() {
			if (!$(this).hasClass('waitingforserver')) {
				$(this).addClass('waitingforserver');
				var that = $(this);
				var guest_id = $(this).parentsUntil('.htabs').parent().data(
						'guest_id');
				var invoiceid = $(this).parent().parent().data('actionid');
				var url = '/api/events/' + $('#eventid').html() + '/guests/'
						+ guest_id + '/uncancelinvoice';// index.php?type=ajax&class=guest';
				var data = 'fevent_id=' + $('#eventid').html() + '&guest_id='
						+ guest_id + '&form=uncancelinvoice&invoiceid='
						+ invoiceid;
				$.ajax({
					type : "POST",
					url : url,
					data : data,
					success : function(data) {
						if (data.err) {// }.length > 0) {
							/*
							 * var errtxt = ''; for (i = 0; i < data.err.length;
							 * i++) { errtxt += data.err[i].note + " "; }
							 * alert(errtxt);
							 */
						} else {
							// alert("OK");
							// reloadGuestTable();
							updateGuestInvoiceListTrigger();
							reloadGuestTable();
						}
						$(that).removeClass('waitingforserver');
					},
					dataType : 'json'
				});
			}
		});
$(document).on(
		'click',
		'.buttonCheckInOut',
		function() {
			var guest_id = $(this).parentsUntil('.htabs').parent().data(
					'guest_id');
			var checkinout = 'checkin';
			var room_id = $(this).parent().parent().data('room_id');
			if ($(this).hasClass('buttonCheckout')) {
				checkinout = 'checkout';
			}

			var url = '/api/events/' + $('#eventid').html() + '/guests/'
					+ guest_id + '/checkinout';
			var data = 'fevent_id=' + $('#eventid').html() + '&guest_id='
					+ guest_id + '&room_id=' + room_id
					+ '&form=checkinout&checkinout=' + checkinout;
			// alert(url + " " + data);
			$
					.ajax({
						type : "POST",
						url : url,
						data : data,
						success : function(data) {
							if (data.err) {
								displayJsonErrorsInHtmlElement(data, $(that)
										.parentsUntil('.htab').find(
												'.checkinouterror'));
							} else {
								editGuest(guest_id);
							}
						},
						dataType : 'json'
					});
		});
$(document)
		.on(
				'click',
				'.buttonInvoiceCreateSave',
				function() {
					if (!$(this).hasClass('waitingforserver')) {
						$(this).addClass('waitingforserver');
						var that = $(this);

						var guest_id = $(this).parentsUntil('.htabs').parent()
								.data('guest_id');

						var url = '/api/events/' + $('#eventid').html()
								+ '/guests/' + guest_id + '/savePDFInvoice';
						var data = 'fevent_id='
								+ $('#eventid').html()
								+ '&guest_id='
								+ guest_id
								+ '&form=savePDFInvoice&cb=updateGuestInvoiceListTrigger';
						// alert(url + " " + data);
						$.ajax({
							type : "POST",
							url : url,
							data : data,
							success : function(data) {
								if (data.err) {// }.length > 0) {
									displayJsonErrorsInHtmlElement(data,
											$(that).parent().find(
													'.invoiceerror'));
								} else {
									updateGuestInvoiceListTrigger();
								}
								$(that).removeClass('waitingforserver');
							},
							dataType : 'json'
						});
					}
				});
$(document)
		.on(
				'click',
				'.buttonInvoiceCreateSaveSend',
				function() {
					if (!$(this).hasClass('waitingforserver')) {
						$(this).addClass('waitingforserver');
						var that = $(this);

						var guest_id = $(this).parentsUntil('.htabs').parent()
								.data('guest_id');

						var url = '/api/events/' + $('#eventid').html()
								+ '/guests/' + guest_id + '/generatePDFInvoice';
						var data = 'fevent_id='
								+ $('#eventid').html()
								+ '&guest_id='
								+ guest_id
								+ '&form=generatePDFInvoice&cb=updateGuestInvoiceListTrigger';
						$.ajax({
							type : "POST",
							url : url,
							data : data,
							success : function(data) {
								if (data.err) {
									displayJsonErrorsInHtmlElement(data,
											$(that).parent().find(
													'.invoiceerror'));
								} else {
									updateGuestInvoiceListTrigger();
								}
								$(that).removeClass('waitingforserver');
							},
							dataType : 'json'
						});
					}
				});
$(document)
		.on(
				'click',
				'.buttonInvoiceCreateSaveRemoteprint',
				function() {
					if (!$(this).hasClass('waitingforserver')) {
						$(this).addClass('waitingforserver');
						var that = $(this);

						var guest_id = $(this).parentsUntil('.htabs').parent()
								.data('guest_id');

						var url = '/api/events/' + $('#eventid').html()
								+ '/guests/' + guest_id
								+ '/generateAndRemotePrintPDFInvoice';
						var data = 'fevent_id='
								+ $('#eventid').html()
								+ '&guest_id='
								+ guest_id
								+ '&form=generateAndRemotePrintPDFInvoice&cb=updateGuestInvoiceListTrigger';
						$.ajax({
							type : "POST",
							url : url,
							data : data,
							success : function(data) {
								if (data.err) {
									displayJsonErrorsInHtmlElement(data,
											$(that).parent().find(
													'.invoiceerror'));
								} else {
									updateGuestInvoiceListTrigger();
								}
								$(that).removeClass('waitingforserver');
							},
							dataType : 'json'
						});
					}
				});
$(document).on(
		'click',
		'.buttonInvoicePreview',
		function() {
			if (!$(this).hasClass('waitingforserver')) {
				$(this).addClass('waitingforserver');
				var that = $(this);

				var guest_id = $(this).parentsUntil('.htabs').parent().data(
						'guest_id');
				printInvoiceTrigger(guest_id);
				$(that).removeClass('waitingforserver');
			}
		});

function serverstoreSingleTicket(guest_id, that) {
	var url = '/api/events/' + $('#eventid').html() + '/guests/' + guest_id
			+ '/serverstoreticket';
	var data = 'fevent_id=' + $('#eventid').html() + '&guest_id=' + guest_id
			+ '&form=serverstoreticket';
	$.ajax({
		type : "POST",
		url : url,
		data : data,
		success : function(data) {
			if (data.err) {// }.length > 0) {
				displayJsonErrorsInHtmlElement(data, $(that).parent().find(
						'.ticketerror'));
				var errtxt = '';
				for (i = 0; i < data.err.length; i++) {
					errtxt += data.err[i].note + " ";
				}
				alert(errtxt);

			} else {
				// alert("OK");
				// reloadGuestForm();
				// editGuest(guest_id);
				// reloadGuestTable();
				$(that).removeClass("waitingforserver");
			}
		},
		dataType : 'json'
	});

}
$(document).on(
		'click',
		'.buttonTicketPreview',
		function() {
			if (!$(this).hasClass('waitingforserver')) {
				$(this).addClass('waitingforserver');
				var that = $(this);

				var guest_id = $(this).parentsUntil('.htabs').parent().data(
						'guest_id');
				printSingleTicket(guest_id);
				$(that).removeClass('waitingforserver');
			}
		});
$(document).on(
		'click',
		'.buttonTicketCreateSaveRemoteprint',
		function() {
			if (!$(this).hasClass('waitingforserver')) {
				$(this).addClass('waitingforserver');
				var that = $(this);

				var guest_id = $(this).parentsUntil('.htabs').parent().data(
						'guest_id');
				serverstoreSingleTicket(guest_id, that);
			}
		});
function handleDataTakeover(that, step) {
	var server = $(that).parent().parent().find('select[name=url]').val();
	var username = $(that).parent().parent().find('input[name=username]').val();
	var password = $(that).parent().parent().find('input[name=password]').val();
	if (step == 1) {
		var server = $(that).parent().parent().find('select[name=url]').val();
		var username = $(that).parent().parent().find('input[name=username]')
				.val();
		var password = $(that).parent().parent().find('input[name=password]')
				.val();
		var urlEvent = "/api/remoteevents/?server=" + server + '&username='
				+ username + '&password=' + password;
		var dataType = "json";
		// hide the form...
		$(that).parent().hide();
		// show "loading" message ...
		$(that).parent().parent().find(':nth-child(2)').show();
		startLoading();
		$.ajax({
			type : "GET",
			url : urlEvent,
			dataType : dataType
		}).done(
				function(data) {
					var optionshtml = '';
					// hide the "loading" message ...
					$(that).parent().parent().find(':nth-child(2)').hide();
					if (data.err) {
						// show form ...
						$(that).parent().show();
						// show error messages ...
						displayJsonErrorsInHtmlElement(data, $(that).parent().parent().find('.error'));
					} else if (data.events) {
						for (i = 0; i < data.events.length; i++) {
							optionshtml += '<option value="' + data.events[i].id
								+ '">(' + data.events[i].id + ') '
								+ data.events[i].title + '</option>';
						}
						$(that).parent().parent().find('select[name=fevent_id]')
							.html(optionshtml);
						$(that).parent().hide();
						$(that).parent().parent().find(':nth-child(3)').show();
					}
				}).always(function(data) {
			endLoading();
		});
	} else if (step == 2) {
		try {
			var last_response_len = false;
			var takeoverEventId = $(that).find('select[name=fevent_id]').val();
			var url = '/api/remoteevents/takeoverfromserver';
			// var url='/atest.php';
			// $(that).parent().parent().find('select[name=url]').css('color',
			// '#00f');
			var data = 'ffevent_id=' + takeoverEventId + '&url=' + server
					+ '&password=' + password + '&username='
					+ username
					+ '&form=takeoverevent&class=event&type=ajax&lg=' + lg;
			$('#logstatusdisplay').html('');
			$('#logstatusdisplay').show();
			$
					.ajax(
							url,
							{
								type : "POST",
								data : data,
								xhrFields : {
									onprogress : function(e) {
										var this_response, response = e.currentTarget.response;
										if (last_response_len === false) {
											this_response = response;
											last_response_len = response.length;
										} else {
											this_response = response
													.substring(last_response_len);
											last_response_len = response.length;
										}
										// console.log(this_response);
										$('#logstatusdisplay').append(this_response);
									}
								}
							}).done(function(data) {
						// console.log('Complete response = ' + data);
					}).fail(function(data) {
						// console.log('Error: ', data);
					});
		} catch (e) {
			console.log(e);
		}

		console.log('Request Sent');
		return false;
	}
}

function handleDataPushover(that, step) {
	var server = $(that).parent().parent().find('select[name=url]').val();
	var username = $(that).parent().parent().find('input[name=username]').val();
	var password = $(that).parent().parent().find('input[name=password]').val();

	if (step == 1) {
		var server = $(that).parent().parent().find('select[name=url]').val();
		var username = $(that).parent().parent().find('input[name=username]')
				.val();
		var password = $(that).parent().parent().find('input[name=password]')
				.val();
		var urlEvent = "/api/remoteevents/?server=" + server + '&username='
				+ username + '&password=' + password;
		var dataType = "json";
		// hide the form...
		$(that).parent().hide();
		// show "loading" message ...
		$(that).parent().parent().find(':nth-child(2)').show();
		startLoading();
		$.ajax({
			type : "GET",
			url : urlEvent,
			dataType : dataType
		}).done(
				function(data) {
					// hide the "loading" message ...
					$(that).parent().parent().find(':nth-child(2)').hide();
					var optionshtml = '';
					if (data.err) {
						// show form ...
						$(that).parent().show();
						// show error messages ...
						displayJsonErrorsInHtmlElement(data, $(that).parent().parent().find('.error'));
					} else if (data.events) {
						for (i = 0; i < data.events.length; i++) {
							optionshtml += '<option value="' + data.events[i].id
								+ '">(' + data.events[i].id + ') '
								+ data.events[i].title + '</option>';
						}
						$(that).parent().parent().find('select[name=fevent_id]')
							.html(optionshtml);
						$(that).parent().hide();
						$(that).parent().parent().find(':nth-child(3)').show();
					}
				}
		).always(function(data) {
			endLoading();
		});
		loadEvents($(that).parent().parent().find('select[name=fevent_id_local]'));
	} else if (step == 2) {
		try {
			var last_response_len = false;
			var localEventId = $(that).find('select[name=fevent_id_local]').val();
			var remoteEventId = 1; // $(that).find('select[name=fevent_id_remote]').val();
			var url = '/api/remoteevents/pushovertoserver';
			var data = 'event_id_local=' + localEventId + '&event_id_remote=' 
					+ remoteEventId + '&url=' + server
					+ '&password=' + password + '&username='
					+ username
					+ '&form=pushoverevent&class=event&type=ajax&lg=' + lg;
			$('#outlogstatusdisplay').html('');
			$('#outlogstatusdisplay').show();
			$
					.ajax(
							url,
							{
								type : "POST",
								data : data,
								xhrFields : {
									onprogress : function(e) {
										var this_response, response = e.currentTarget.response;
										if (last_response_len === false) {
											this_response = response;
											last_response_len = response.length;
										} else {
											this_response = response
													.substring(last_response_len);
											last_response_len = response.length;
										}
										// console.log(this_response);
										$('#outlogstatusdisplay').append(this_response);
									}
								}
							}).done(function(data) {
						// console.log('Complete response = ' + data);
					}).fail(function(data) {
						// console.log('Error: ', data);
					});
		} catch (e) {
			console.log(e);
		}

		console.log('Request Sent');
		return false;
	}
}

function loadEvents(target) {
	var urlEvent = "/api/events/";
	var dataType = "json";
	$.ajax({
		type : "GET",
		url : urlEvent,
		dataType : dataType
	}).done(
		function(data) {
			var optionshtml = '';
			for (i = 0; i < data.events.length; i++) {
				optionshtml += '<option value="' + data.events[i].id
					+ '">(' + data.events[i].id + ') '
					+ data.events[i].title + '</option>';
			}
			$(target).html(optionshtml);
			/*
			 * $(that).parent().parent().find('select[name=fevent_id_remote]')
			 * .html(optionshtml); $(that).parent().hide();
			 * $(that).parent().parent().find(':nth-child(2)').show();
			 */
		}
	);
}

/**
 * loads the list of current and past event transfers from the server and fills
 * the related lists / tables in the template
 * 
 * @returns
 */
function loadTransferList() {
	var urlTransfers = "/api/eventtransfers";
	var dataType = "json";
	$.ajax({
		type : "GET",
		url : urlTransfers,
		dataType : dataType
	}).done(
		function(data) {
			var optionshtml = '';
			for (i = 0; i < data.transfers.in.length; i++) {
				var ti = data.transfers.in[i];
				optionshtml += '<tr data-fevent="' + ti.fevent + '" data-server="' + ti.server + '" '
					+ 'data-ts="' + ti.timestamp + '" data-rand="' + ti.rand + '" data-direction="in">'
					+ '<td>' + ti.fevent + '</td>' 
					+ '<td>' + ti.server + '</td>'
					+ '<td>' + ti.timestamp + '</td>'
					+ '<td>' + ti.rand + '</td>'
					+ '<td>' + ti.status + '</td>'
					+ '<td><img class="transferCancel" src="/pic/icons/delete.svg" alt="edit"></td>'
					+ '</tr>';
			}
			$('.transferinlist tbody').html(optionshtml);
			
			optionshtml = '';
			for (i = 0; i < data.transfers.out.length; i++) {
				var to = data.transfers.out[i];
				optionshtml += '<tr data-fevent="' + to.fevent + '" data-server="' + to.destination + '" '
				+ 'data-ts="' + to.timestamp + '" data-rand="' + to.rand + '" data-direction="out">'
					+ '<td>' + to.fevent + '</td>' 
					+ '<td>' + to.destination + '</td>'
					+ '<td>' + to.timestamp + '</td>'
					+ '<td>' + to.rand + '</td>'
					+ '<td>' + to.status + '</td>'
					+ '<td><img class="transferCancel" src="/pic/icons/delete.svg" alt="edit"></td>'
					+ '</tr>';
			}
			$('.transferoutlist tbody').html(optionshtml);
		}
	);
}

$(document).on(
		'click',
		'.transferCancel',
		function() {
			var f = $(this).parent().parent();
			cancelTransfer($(f).data('fevent'), $(f).data('direction'), $(f).data('ts'), $(f).data('rand'), $(f).data('server'));
		}
);

function cancelTransfer(fevent, direction, ts, code, server) {
	var urlTransfers = "/api/eventtransfers/cancel";
	var dataType = "json";
	var data = 'fevent_id=' + fevent + '&ts=' + ts
	+ '&direction=' + direction + '&code=' + code + '&server=' + server;
	$.ajax({
		type : "POST",
		url : urlTransfers,
		dataType : dataType,
		data : data
	}).done(
		function(data) {
			loadTransferList();
		}
	);
}