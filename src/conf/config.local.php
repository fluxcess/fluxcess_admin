<?php

/**
 * base installation directory (ends with a slash)
 */
$gs['BASEDIR'] = '';

/**
 * database server
 */
$gs['DB_HOST'] = 'localhost';

/**
 * database name
 */
$gs['DB_DB'] = '';

/**
 * database username
 */
$gs['DB_USER'] = '';

/**
 * database password
 */
$gs['DB_PW'] = '';

/**
 * determines if this is a testing environment
 */
$gs['ENV_TESTING'] = false;

/**
 * determines if this is a development environment
 */
$gs['ENV_DEV'] = false;

/**
 * determines if this is a live environment
 */
$gs['ENV_LIVE'] = false;

/**
 * determines the id of the user which is automatically logged in
 */
$gs['PRESET_USER_ID'] = null;

/**
 * determines the id of the current live-event
 */
$gs['LIVE_EVENT_ID'] = null;

/**
 * determines the url of the html2pdf converter
 * you can use your own server here, or the public
 * html2pdf.it server
 * default: https://html2pdf.line5.net
 */
$gs['HTML2PDF_SERVER'] = 'https://html2pdf.line5.net';

/**
 * determines the link to the according check-in application
 * default: https://check-in.fast-lane.org/
 */
$gs['CHECKIN_APP'] = 'https://check-in.fast-lane.org/';

/**
 * Determines the name of the environment.
 * Might be shown to end users.
 */
$gs['ENV_NAME'] = '';

/**
 * determines the basis template form
 */
$gs['BASETEMPLATE'] = 'fluxline';

/**
 * mail sender data
 */
$gs['mailsender'] = 'info@fluxcess.com';
$gs['mailsendername'] = 'fluxcess';
$gs['mailallbcc'] = 'info@fluxcess.com';
$gs['adminemail'] = 'info@fluxcess.com';

/**
 * session save path - has to be writable
 * required for generating wallet passes
 */
$gs['SESSION_SAVE_PATH'] = '';

/**
 * is it allowed to register as a new user?
 */
$gs['newuserregistrationallowed'] = false;

/**
 * mail imprint
 */
$gs['MAIL_IMPRINT_HTML'] = array(
    'default' => ''
);

/**
 * site link, for emails
 */
$gs['SITE_LINK'] = array(
    'default' => 'SITE_FLUXCESS_LINK'
);

/**
 * site name, for emails
 */
$gs['SITE_NAME'] = array(
    'default' => 'SITE_FLUXCESS_NAME'
);

/**
 * subtitle, also for the index template
 */
$gs['SITE_SUBTITLE'] = array(
    'default' => 'SITE_FLUXCESS_SUBTITLE'
);

/**
 * logo - can also be customized
 */
$gs['SITE_MINILOGO'] = array(
    'default' => 'SITE_FLUXCESS_MINILOGO'
);

/**
 * Page Title
 */
$gs['SITE_PAGETITLE'] = array(
    'default' => 'SITE_FLUXCESS_PAGETITLE'
);

/**
 * Page Description
 */
$gs['SITE_PAGEDESC'] = array(
    'default' => 'SITE_FLUXCESS_PAGEDESC'
);

/**
 * Page Favicon Path
 */
$gs['SITE_FAVICONPATH'] = array(
    'default' => 'SITE_FLUXCESS_FAVICONPATH'
);

/**
 * Page Author
 */
$gs['SITE_PAGEAUTHOR'] = array(
    'default' => 'SITE_FLUXCESS_PAGEAUTHOR'
);

/**
 * Page Author Link
 */
$gs['SITE_PAGEAUTHORLINK'] = array(
    'default' => 'SITE_FLUXCESS_PAGEAUTHORLINK'
);

/**
 * Page Footer Disclaimer
 */
$gs['SITE_PAGEFOOTDISCLAIMER'] = array(
    'default' => 'SITE_FLUXCESS_PAGEFOOTDISCLAIMER'
);

/**
 * Link contact
 */
$gs['SITE_LINKCONTACT'] = array(
    'default' => 'SITE_FLUXCESS_LINKCONTACT'
);
/**
 * Link data privacy
 */
$gs['SITE_LINKPRIVACY'] = array(
    'default' => 'SITE_FLUXCESS_LINKPRIVACY'
);
/**
 * Link forum
 */
$gs['SITE_LINKFORUM'] = array(
    'default' => 'SITE_FLUXCESS_LINKFORUM'
);
/**
 * Link about
 */
$gs['SITE_LINKABOUT'] = array(
    'default' => 'SITE_FLUXCESS_LINKABOUT'
);
/**
 * Link contact
 */
$gs['SITE_LINKDOCU'] = array(
    'default' => 'SITE_FLUXCESS_LINKDOCU'
);
/**
 * Link facebook
 */
$gs['SITE_SOCIALLINK_FACEBOOK'] = array(
    'default' => 'SITE_FLUXCESS_SOCIALLINK_FACEBOOK'
);
/**
 * Link twitter
 */
$gs['SITE_SOCIALLINK_TWITTER'] = array(
    'default' => 'SITE_FLUXCESS_SOCIALLINK_TWITTER'
);

/**
 * Tracking scripts
 */
$gs['SITE_TRACKINGSCRIPT'] = '';

/**
 * Direct incoming data transfer / import to the database allowed?
 * this should be disabled, unless the server is a private instance
 */
$gs['TRANSFER_IN_ALLOWED'] = false;

/**
 * Direct incoming data transfer / import to the database
 * source servers
 */
$gs['TRANSFER_IN_SERVERS'] = array();

/**
 * Debug mode (0/1) shows more error messages
 */
$gs['DEBUG_MODE'] = 0;

/**
 * ALL requests will be logged, if this is set to 1.
 */
$gs['LOG_ALL_REQUESTS'] = 0;

/**
 * Link to the privacy page (https://...)
 */
$gsLocal['SITE_LINKPRIVACY']['de'] = '';
$gsLocal['SITE_LINKPRIVACY']['en'] = '';

/**
 * Link to the contact page (https://...)
 */
$gsLocal['SITE_LINKCONTACT']['de'] = '';
$gsLocal['SITE_LINKCONTACT']['en'] = '';

/**
 * Link to the documentation (https://...)
 */
$gsLocal['SITE_LINKDOCU']['de'] = 'https://book.fluxcess.com/';
$gsLocal['SITE_LINKDOCU']['en'] = 'https://book.fluxcess.com/';

/**
 * Link to the facebook page (https://...)
 */
$gsLocal['SITE_SOCIALLINK_FACEBOOK']['de'] = 'https://www.facebook.com/fluxcess';
$gsLocal['SITE_SOCIALLINK_FACEBOOK']['en'] = 'https://www.facebook.com/fluxcess';

/**
 * Link to the twitter page (https://...)
 */
$gsLocal['SITE_SOCIALLINK_TWITTER']['de'] = 'https://twitter.com/fluxcess';
$gsLocal['SITE_SOCIALLINK_TWITTER']['en'] = 'https://twitter.com/fluxcess';

/**
 * Link to the "about" page (https://...); leave empty for none
 */
$gsLocal['SITE_LINKABOUT']['de'] = '';
$gsLocal['SITE_LINKABOUT']['en'] = '';

/**
 * Link to forums (https://...); leave empty for none
 */
$gsLocal['SITE_LINKFORUM']['de'] = '';
$gsLocal['SITE_LINKFORUM']['en'] = '';

/**
 * Text for the disclaimer written in the page footer
 */
$gsLocal['SITE_PAGEFOOTDISCLAIMER']['de'] = 'Willkommen bei fluxcess - weitere Informationen finden Sie unter <a href="http://fluxcess.com">http://fluxcess.com</a>.';
$gsLocal['SITE_PAGEFOOTDISCLAIMER']['en'] = 'Welcome to fluxcess - for more information, please have a look at <a href="http://fluxcess.com">http://fluxcess.com</a>.';

/**
 * Link to the page author
 */
$gsLocal['SITE_PAGEAUTHORLINK']['de'] = 'https://www.fluxcess.com';
$gsLocal['SITE_PAGEAUTHORLINK']['en'] = 'https://www.fluxcess.com';

/**
 * Name of the page author
 */
$gsLocal['SITE_PAGEAUTHOR']['de'] = 'fluxcess GmbH';
$gsLocal['SITE_PAGEAUTHOR']['en'] = 'fluxcess GmbH';

/**
 * Location of the favicon
 */
$gsLocal['SITE_FAVICONPATH']['de'] = '/pic/fluxcess_icon';
$gsLocal['SITE_FAVICONPATH']['en'] = '/pic/fluxcess_icon';

/**
 * Page description
 */
$gsLocal['SITE_PAGEDESC']['de'] = 'Die Software für Gästemanagement auf Ihrem Event - Gästelisten, Einladungen und der ultraschnelle FAST LANE Check-In';
$gsLocal['SITE_PAGEDESC']['en'] = 'The software for visitor management on your event - guest lists, invitations, and the lightning fast FAST LANE Check-In';

/**
 * Page Title
 */
$gsLocal['SITE_PAGETITLE']['de'] = 'fluxcess Besuchermanagement - Die Digitale Gästeliste';
$gsLocal['SITE_PAGETITLE']['en'] = 'fluxcess visitor management - The Digital Guest List';

/**
 * Page Subtitle
 */
$gsLocal['SITE_SUBTITLE']['de'] = 'Professionelle Software für Besuchermanagement';
$gsLocal['SITE_SUBTITLE']['en'] = 'Professional Visitor Management Software';

/**
 * Logo in the top left corner
 */
$gsLocal['SITE_MINILOGO']['de'] = '/pic/fluxcess_logo.svg';
$gsLocal['SITE_MINILOGO']['en'] = '/pic/fluxcess_logo.svg';

/**
 * Page name, as written in emails
 */
$gsLocal['SITE_NAME']['de'] = 'fluxcess';
$gsLocal['SITE_NAME']['en'] = 'fluxcess';

/**
 * Link to the Guestlist admin, as written in emails
 */
$gsLocal['SITE_LINK']['de'] = 'https://live.fluxcess.com/de';
$gsLocal['SITE_LINK']['en'] = 'https://live.fluxcess.com';
