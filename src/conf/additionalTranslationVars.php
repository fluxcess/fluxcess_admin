<?php
/**
 * This file contains some translation variables, enclosed in gettext calls.
 *
 * Weird, eh?
 * Well, when we generate the translation files, a script collects
 * all strings in 'gettext' calls from the entire project's source code.
 * Usually, this should cover all strings without exceptions.
 *
 * However, there are some strings we do not use within the code,
 * but should be translated. These are listed here, to be automatically
 * included in the translation file.
 *
 * It would be better to have these things configurable via a 'settings' option
 * in the GUI of the application. Maybe that's a nice ToDo for later...
 * ... a nice global site-admin area, where all these things can be configured.
 *
 * This file is never executed at runtime.
 *
 * @author fluxcess GmbH http://www.fluxcess.com
 */
gettext('SITE_FLUXDEV_NAME');
gettext('SITE_FLUXDEV_LINK');
gettext('SITE_FLUXDEV_SUBTITLE');
gettext('SITE_FLUXDEV_MINILOGO');
gettext('SITE_FLUXDEV_PAGETITLE');
gettext('SITE_FLUXDEV_PAGEDESC');
gettext('SITE_FLUXDEV_FAVICONPATH');
gettext('SITE_FLUXDEV_PAGEAUTHOR');
gettext('SITE_FLUXDEV_PAGEAUTHORLINK');
gettext('SITE_FLUXDEV_PAGEFOOTDISCLAIMER');
gettext('SITE_FLUXDEV_LINKABOUT');
gettext('SITE_FLUXDEV_LINKDOCU');
gettext('SITE_FLUXDEV_LINKFORUM');
gettext('SITE_FLUXDEV_SOCIALLINK_FACEBOOK');
gettext('SITE_FLUXDEV_SOCIALLINK_TWITTER');

gettext('SITE_FLUXCESS_NAME');
gettext('SITE_FLUXCESS_LINK');
gettext('SITE_FLUXCESS_SUBTITLE');
gettext('SITE_FLUXCESS_MINILOGO');
gettext('SITE_FLUXCESS_PAGETITLE');
gettext('SITE_FLUXCESS_PAGEDESC');
gettext('SITE_FLUXCESS_FAVICONPATH');
gettext('SITE_FLUXCESS_PAGEAUTHOR');
gettext('SITE_FLUXCESS_PAGEAUTHORLINK');
gettext('SITE_FLUXCESS_PAGEFOOTDISCLAIMER');
gettext('SITE_FLUXCESS_LINKABOUT');
gettext('SITE_FLUXCESS_LINKDOCU');
gettext('SITE_FLUXCESS_LINKFORUM');
gettext('SITE_FLUXCESS_SOCIALLINK_FACEBOOK');
gettext('SITE_FLUXCESS_SOCIALLINK_TWITTER');

gettext('SITE_GAESTELISTE_NAME');
gettext('SITE_GAESTELISTE_LINK');
gettext('SITE_GAESTELISTE_SUBTITLE');
gettext('SITE_GAESTELISTE_MINILOGO');
gettext('SITE_GAESTELISTE_PAGETITLE');
gettext('SITE_GAESTELISTE_PAGEDESC');
gettext('SITE_GAESTELISTE_FAVICONPATH');
gettext('SITE_GAESTELISTE_PAGEAUTHOR');
gettext('SITE_GAESTELISTE_PAGEAUTHORLINK');
gettext('SITE_GAESTELISTE_PAGEFOOTDISCLAIMER');
gettext('SITE_GAESTELISTE_LINKABOUT');
gettext('SITE_GAESTELISTE_LINKDOCU');
gettext('SITE_GAESTELISTE_LINKFORUM');
gettext('SITE_GAESTELISTE_SOCIALLINK_FACEBOOK');
gettext('SITE_GAESTELISTE_SOCIALLINK_TWITTER');

gettext('SITE_HEINZE_NAME');
gettext('SITE_HEINZE_LINK');
gettext('SITE_HEINZE_SUBTITLE');
gettext('SITE_HEINZE_MINILOGO');
gettext('SITE_HEINZE_PAGETITLE');
gettext('SITE_HEINZE_PAGEDESC');
gettext('SITE_HEINZE_FAVICONPATH');
gettext('SITE_HEINZE_PAGEAUTHOR');
gettext('SITE_HEINZE_PAGEAUTHORLINK');
gettext('SITE_HEINZE_PAGEFOOTDISCLAIMER');
gettext('SITE_HEINZE_LINKABOUT');
gettext('SITE_HEINZE_LINKDOCU');
gettext('SITE_HEINZE_LINKFORUM');
gettext('SITE_HEINZE_SOCIALLINK_FACEBOOK');
gettext('SITE_HEINZE_SOCIALLINK_TWITTER');