<?php
/**
 * Global and default settings, as well as the automatic module loader
 *
 * @author Line5
 */
error_reporting(E_ERROR);
ini_set('display_errors', 1);
date_default_timezone_set('CET');
$basedirtmp = null;

/* Figure out which environment we are using */
$docRoot = getenv('DOCUMENT_ROOT');
if ($docRoot == '') {
    $docRoot = __DIR__;
}

if (isset($gs['DEBUG_MODE']) && $gs['DEBUG_MODE'] === 1) {
    error_reporting(E_ALL);
}

/**
 * the database server is predefaultly set to localhost.
 */
if ($gs['DB_HOST'] == '') {
    $gs['DB_HOST'] = 'localhost';
}
$glob['DB_HOST'] = trim($gs['DB_HOST']);

if (! isset($gs['DB_DB']) || strlen(trim($gs['DB_DB'])) < 1) {
    die('Parameter DB_DB not set in conf/config.local.php. (Database Name).');
} else {
    $glob['DB_DB'] = trim($gs['DB_DB']);
}
if (! isset($gs['DB_USER']) || strlen(trim($gs['DB_USER'])) < 1) {
    die('Parameter DB_USER not set in conf/config.local.php. (Database Username).');
} else {
    $glob['DB_USER'] = trim($gs['DB_USER']);
}
if (isset($gs['DB_PW']) && strlen(trim($gs['DB_PW'])) > 0) {
    $glob['DB_PW'] = trim($gs['DB_PW']);
}

define('TESTING', $gs['ENV_TESTING']);
define('DEVENV', $gs['ENV_DEV']);
define('LIVEENV', $gs['ENV_LIVE']);

if (isset($gs['PRESET_USER_ID']) && is_numeric($gs['PRESET_USER_ID'])) {
    define('PRESETUSERID', $gs['PRESET_USER_ID']);
}
if (isset($gs['LIVE_EVENT_ID']) && is_numeric($gs['LIVE_EVENT_ID'])) {
    define('LIVEEVENTID', $gs['LIVE_EVENT_ID']);
}

if (! isset($gs['ENV_NAME'])) {
    $gs['ENV_NAME'] = getenv('HTTP_HOST');
}
define('ENV_NAME', $gs['ENV_NAME']);

/**
 * determines the directory where the code is located.
 * Last character is always a slash ("/").
 */
if (! isset($gs['BASEDIR']) || strlen($gs['BASEDIR']) < 1) {
    die('Parameter BASEDIR not set in conf/config.local.php. (Base directory)');
}
define('BASEDIR', $gs['BASEDIR']);

/**
 * set the session save path - required for generating wallet passes
 */
if (isset($gs['SESSION_SAVE_PATH'])) {
    ini_set('session.save_path', $gs['SESSION_SAVE_PATH']);
}

/**
 * The html2pdf server is based on the project html2pdf.it.
 * An external web service
 * is used in order to convert html to pdf.
 */
if (! isset($gs['HTML2PDF_SERVER']) || strlen($gs['HTML2PDF_SERVER']) < 1) {
    $gs['HTML2PDF_SERVER'] = 'https://html2pdf.line5.net';
}
define('HTML2PDF_SERVER', $gs['HTML2PDF_SERVER']);

/**
 * Link to the check-in app
 */
if (! isset($gs['CHECKIN_APP']) || strlen($gs['CHECKIN_APP']) < 1) {
    $gs['CHECKIN_APP'] = 'https://check-in.fast-lane.org/';
}
define('CHECKIN_APP', $gs['CHECKIN_APP']);

/**
 * determines smtp data, for DEV!
 */
if (! isset($gs['USESMTPRELAY'])) {
    $gs['USESMTPRELAY'] = false;
}
define('USESMTPRELAY', $gs['USESMTPRELAY']);
if (isset($gs['SMTP_HOST']) && strlen(trim($gs['SMTP_HOST'])) > 1) {
    define('SMTP_HOST', trim($gs['SMTP_HOST']));
}
if (isset($gs['SMTP_USER']) && strlen(trim($gs['SMTP_USER'])) > 1) {
    define('SMTP_USER', trim($gs['SMTP_USER']));
}
if (isset($gs['SMTP_PASS']) && strlen(trim($gs['SMTP_PASS'])) > 1) {
    define('SMTP_PASS', trim($gs['SMTP_PASS']));
}
if (isset($gs['BASETEMPLATE']) && strlen(trim($gs['BASETEMPLATE'])) > 1) {
    define('BASETEMPLATE', trim($gs['BASETEMPLATE']));
}

if (! isset($gs['INTERFACE_REMOTE_IPS'])) {
    $gs['INTERFACE_REMOTE_IPS'] = '';
}
define('INTERFACE_REMOTE_IPS', trim($gs['INTERFACE_REMOTE_IPS']));
if (! isset($gs['INTERFACE_AUTOLOGIN_USERID'])) {
    $gs['INTERFACE_AUTOLOGIN_USERID'] = '';
}
define('INTERFACE_AUTOLOGIN_USERID', trim($gs['INTERFACE_AUTOLOGIN_USERID']));
if (! isset($gs['INTERFACE_AUTOLOGIN_EMAILADDRESS'])) {
    $gs['INTERFACE_AUTOLOGIN_EMAILADDRESS'] = '';
}
define('INTERFACE_AUTOLOGIN_EMAILADDRESS', trim($gs['INTERFACE_AUTOLOGIN_EMAILADDRESS']));
if (! isset($gs['INTERFACE_AUTOLOGIN_CUSTOMERID'])) {
    $gs['INTERFACE_AUTOLOGIN_CUSTOMERID'] = '';
}
define('INTERFACE_AUTOLOGIN_CUSTOMERID', trim($gs['INTERFACE_AUTOLOGIN_CUSTOMERID']));

// Directory for temporary pre-pdf html file storage
define('DIR_STORAGE_PRE_PDF', BASEDIR . 'public/hidden/pre_pdf/');
// the html2pdf server is not able to use idn domains, so we use another one...
$myHost = getenv('HTTP_HOST');
if (isset($gs['HTTP_STORAGE_PRE_PDF'])) {
    $myHost = $gs['HTTP_STORAGE_PRE_PDF'];
} elseif ($myHost == 'admintest.xn--gsteliste-v2a.org') {
    $myHost = 'https://admintest.eventmanager.us';
} elseif ($myHost == 'admin.xn--gsteliste-v2a.org') {
    $myHost = 'https://admin.eventmanager.us';
} elseif ($myHost == 'gaesteliste') {
    $myHost = 'http://gaesteliste';
}
define('HTTP_STORAGE_PRE_PDF', $myHost . '/hidden/pre_pdf/');

/**
 * is new user registration allowed?
 */
if (! isset($gs['newuserregistrationallowed'])) {
    $gs['newuserregistrationallowed'] = false;
}
define('NEW_USER_REGISTRATION_ALLOWED', $gs['newuserregistrationallowed']);

/**
 * mail imprint
 */
$webhostname = getenv('HOST_NAME');
if (isset($gs['MAIL_IMPRINT_HTML'][$webhostname])) {
    $tmpvar = $gs['MAIL_IMPRINT_HTML'][$webhostname];
} else {
    $tmpvar = $gs['MAIL_IMPRINT_HTML']['default'];
}
define('MAIL_IMPRINT_HTML', $tmpvar);

/**
 * site name
 */
if (isset($gs['SITE_NAME'][$webhostname])) {
    $tmpvar = $gs['SITE_NAME'][$webhostname];
} else {
    $tmpvar = $gs['SITE_NAME']['default'];
}
define('SITE_NAME', $tmpvar);

/**
 * portal link
 */
if (isset($gs['SITE_LINK'][$webhostname])) {
    $tmpvar = $gs['SITE_LINK'][$webhostname];
} else {
    $tmpvar = $gs['SITE_LINK']['default'];
}
define('SITE_LINK', $tmpvar);

/**
 * site subtitle
 */
if (isset($gs['SITE_SUBTITLE'][$webhostname])) {
    $tmpvar = $gs['SITE_SUBTITLE'][$webhostname];
} else {
    $tmpvar = $gs['SITE_SUBTITLE']['default'];
}
define('SITE_SUBTITLE', $tmpvar);

/**
 * site subtitle
 */
if (isset($gs['SITE_MINILOGO'][$webhostname])) {
    $tmpvar = $gs['SITE_MINILOGO'][$webhostname];
} else {
    $tmpvar = $gs['SITE_MINILOGO']['default'];
}
define('SITE_MINILOGO', $tmpvar);

/**
 * site subtitle
 */
if (isset($gs['SITE_PAGETITLE'][$webhostname])) {
    $tmpvar = $gs['SITE_PAGETITLE'][$webhostname];
} else {
    $tmpvar = $gs['SITE_PAGETITLE']['default'];
}
define('SITE_PAGETITLE', $tmpvar);

/**
 * site subtitle
 */
if (isset($gs['SITE_PAGEFOOTDISCLAIMER'][$webhostname])) {
    $tmpvar = $gs['SITE_PAGEFOOTDISCLAIMER'][$webhostname];
} else {
    $tmpvar = $gs['SITE_PAGEFOOTDISCLAIMER']['default'];
}
define('SITE_PAGEFOOTDISCLAIMER', $tmpvar);

/**
 * site subtitle
 */
if (isset($gs['SITE_PAGEDESC'][$webhostname])) {
    $tmpvar = $gs['SITE_PAGEDESC'][$webhostname];
} else {
    $tmpvar = $gs['SITE_PAGEDESC']['default'];
}
define('SITE_PAGEDESC', $tmpvar);

/**
 * site author
 */
if (isset($gs['SITE_PAGEAUTHOR'][$webhostname])) {
    $tmpvar = $gs['SITE_PAGEAUTHOR'][$webhostname];
} else {
    $tmpvar = $gs['SITE_PAGEAUTHOR']['default'];
}
define('SITE_PAGEAUTHOR', $tmpvar);
/**
 * site author link
 */
if (isset($gs['SITE_PAGEAUTHORLINK'][$webhostname])) {
    $tmpvar = $gs['SITE_PAGEAUTHORLINK'][$webhostname];
} else {
    $tmpvar = $gs['SITE_PAGEAUTHORLINK']['default'];
}
define('SITE_PAGEAUTHORLINK', $tmpvar);

/**
 * site link: about
 */
if (isset($gs['SITE_LINKABOUT'][$webhostname])) {
    $tmpvar = $gs['SITE_LINKABOUT'][$webhostname];
} else {
    $tmpvar = $gs['SITE_LINKABOUT']['default'];
}
define('SITE_LINKABOUT', $tmpvar);
/**
 * site link: documentation
 */
if (isset($gs['SITE_LINKDOCU'][$webhostname])) {
    $tmpvar = $gs['SITE_LINKDOCU'][$webhostname];
} else {
    $tmpvar = $gs['SITE_LINKDOCU']['default'];
}
define('SITE_LINKDOCU', $tmpvar);
/**
 * site link: customer forums
 */
if (isset($gs['SITE_LINKFORUM'][$webhostname])) {
    $tmpvar = $gs['SITE_LINKFORUM'][$webhostname];
} else {
    $tmpvar = $gs['SITE_LINKFORUM']['default'];
}
define('SITE_LINKFORUM', $tmpvar);
/**
 * site link: contact
 */
if (isset($gs['SITE_LINKCONTACT'][$webhostname])) {
    $tmpvar = $gs['SITE_LINKCONTACT'][$webhostname];
} else {
    $tmpvar = $gs['SITE_LINKCONTACT']['default'];
}
define('SITE_LINKCONTACT', $tmpvar);

/**
 * site tracking script
 */
if (isset($gs['SITE_TRACKINGSCRIPT'][$webhostname])) {
    $tmpvar = $gs['SITE_TRACKINGSCRIPT'][$webhostname];
} else {
    $tmpvar = $gs['SITE_TRACKINGSCRIPT']['default'];
}
define('SITE_TRACKINGSCRIPT', $tmpvar);

/**
 * site favicon path
 */
if (isset($gs['SITE_FAVICONPATH'][$webhostname])) {
    $tmpvar = $gs['SITE_FAVICONPATH'][$webhostname];
} else {
    $tmpvar = $gs['SITE_FAVICONPATH']['default'];
}
define('SITE_FAVICONPATH', $tmpvar);

/**
 * site facebook link
 */
$tmpvar = '';
if (isset($gs['SITE_SOCIALLINK_FACEBOOK'][$webhostname])) {
    $tmpvar = $gs['SITE_SOCIALLINK_FACEBOOK'][$webhostname];
} else {
    $tmpvar = $gs['SITE_SOCIALLINK_FACEBOOK']['default'];
}
define('SITE_SOCIALLINK_FACEBOOK', $tmpvar);

/**
 * site twitter link
 */
$tmpvar = '';
if (isset($gs['SITE_SOCIALLINK_TWITTER'][$webhostname])) {
    $tmpvar = $gs['SITE_SOCIALLINK_TWITTER'][$webhostname];
} else {
    $tmpvar = $gs['SITE_SOCIALLINK_TWITTER']['default'];
}
define('SITE_SOCIALLINK_TWITTER', $tmpvar);

$tmpvar = false;
if (isset($gs['TRANSFER_IN_ALLOWED'])) {
    $tmpvar = $gs['TRANSFER_IN_ALLOWED'];
}
define('TRANSFER_IN_ALLOWED', $tmpvar);

$tmpvar = false;
if (isset($gs['TRANSFER_FOREIGNTRIGGERED_IN_ALLOWED'])) {
    $tmpvar = $gs['TRANSFER_FOREIGNTRIGGERED_IN_ALLOWED'];
}
define('TRANSFER_FOREIGNTRIGGERED_IN_ALLOWED', $tmpvar);

$tmpvar = false;
if (isset($gs['TRANSFER_OUT_ALLOWED'])) {
    $tmpvar = $gs['TRANSFER_OUT_ALLOWED'];
}
define('TRANSFER_OUT_ALLOWED', $tmpvar);

$tmpvar = false;
if (isset($gs['LOG_ALL_REQUESTS'])) {
    $tmpvar = $gs['LOG_ALL_REQUESTS'];
}
define('LOG_ALL_REQUESTS', $tmpvar);

$tmpvar = false;
if (isset($gs['BOOKING_DOMAINS'])) {
    $tmpvar = $gs['BOOKING_DOMAINS'];
}
define('BOOKING_DOMAINS', $tmpvar);

/**
 * reset the gs variable, which has been set by the user in config.local.php
 */
unset($gs);

define('SMARTY_DIR2', BASEDIR . 'tmp/');
if (! is_dir(SMARTY_DIR2)) {
    mkdir(SMARTY_DIR2);
}

// define (svn) revision number, which is written by deployment script
require_once (BASEDIR . 'conf/version');

/**
 * Set Mail params
 */
if (isset($gs['mailsender'])) {
    $glob['mailsender'] = $gs['mailsender'];
} else {
    $glob['mailsender'] = 'mail@line5.tv';
}
if (isset($gs['mailsendername'])) {
    $glob['mailsendername'] = $gs['malsendername'];
} else {
    $glob['mailsendername'] = 'Gästeliste.org - Fast Lane Check-In';
}
if (isset($gs['mailallbcc'])) {
    $glob['mailallbcc'] = $gs['mailallbcc'];
} else {
    $glob['mailallbcc'] = 'mail@line5.tv';
}
if (isset($gs['adminemail'])) {
    $glob['adminemail'] = $gs['adminemail'];
} else {
    $glob['adminemail'] = 'mail@line5.tv';
}
/**
 * Embed Libraries
 */
include_once (BASEDIR . 'lib/Smarty/Smarty.class.php');
include_once (BASEDIR . 'lib/phpmailer/class.phpmailer.php');
include_once (BASEDIR . 'lib/phpmailer/class.smtp.php');

/**
 * define PHPExcel root
 */
// define('PHPEXCEL_ROOT', BASEDIR.'lib/phpexcel/PHPExcel');

/**
 * important for apple wallet generation (zip temp dir!):
 */
define('PCLZIP_TEMPORARY_DIR', BASEDIR . '../../../tmp/');

/**
 * Determine if record changes should be logged
 */
define('LOGRECORDCHANGES', false);

/**
 * Define column labels
 */
$glob['columnlabels'] = array(
    'de' => array(
        'salutation' => 'Anrede',
        'firstname' => 'Vorname',
        'lastname' => 'Nachname',
        'company' => 'Firma',
        'emailaddress' => 'E-Mail',
        'invited' => 'eingeladen',
        'registered' => 'zugesagt',
        'cancelled' => 'abgesagt',
        'note' => 'Kommentar',
        'accesscode' => 'Zugangscode'
    ),
    'en' => array(
        'salutation' => 'Salutation',
        'firstname' => 'First Name',
        'lastname' => 'Last Name',
        'company' => 'Company',
        'emailaddress' => 'Email',
        'invited' => 'Invited',
        'registered' => 'Registered',
        'cancelled' => 'Cancelled',
        'note' => 'Notes',
        'accesscode' => 'Access Code'
    )
);

spl_autoload_register('autoload_line5_guestlist');
register_shutdown_function("fatal_handler");

/**
 * Ensure that missing files are automatically loaded
 *
 * @param string $class_name
 */
function autoload_line5_guestlist($class_name)
{
    $found = false;
    if (is_file(BASEDIR . 'inc/' . $class_name . '.php')) {
        include BASEDIR . 'inc/' . $class_name . '.php';
        $found = true;
    }
    
    if ($found == false) {
        $extname = substr($class_name, 4);
        $extensions = array(
            'persno'
        );
        if (array_search($extname, $extensions) !== false) {
            if (is_file(BASEDIR . 'ext/' . $extname . '/' . $class_name . '.php')) {
                include BASEDIR . 'ext/' . $extname . '/' . $class_name . '.php';
                $found = true;
            }
        }
    }
}

function fatal_handler()
{
    global $glob, $myHost;
    $errfile = "unknown file";
    $errstr = "shutdown";
    $errno = E_CORE_ERROR;
    $errline = 0;
    
    $error = error_get_last();
    
    if ($error !== NULL) {
        $b = "\n";
        $errno = $error["type"];
        $errfile = $error["file"];
        $errline = $error["line"];
        $errstr = $error["message"];
        
        if ($errno != 8) { // not for notices!
            $msg = $myHost . $b . $errno . $b . $errfile . $b . $errline . $b . $errstr;
            mail($glob['adminemail'], 'fluxcess fatal error ' . ENV_NAME . ', ' . FLUXLINE_VERSION, $msg);
        }
    }
}
