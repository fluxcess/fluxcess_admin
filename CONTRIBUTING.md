Welcome, nice to meet you!

Please create a dedicated branch for any issue you work on. 
Once you consider it "done", create a pull request. 
*Never merge anything to master without approval!*

Thank you!